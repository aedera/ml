#!/bin/csh

set epsilon = 1.0   #strength of dependencies. It is the logarithm of strength (i.e., order of magnitude). Larger value, stronger dependencies.
set EPSILON = 100
set M = 500         #keep it at 500
#set seed = 901234   #seed for generation of random network and sampler itself

set D = 200000   #dataset size
set seed = ( 23425 340987 234532 984  3984   9827593  2342 586 879847359 75943 )

set  machine = $1

set javaStr = "/usr/java/default/bin"

if($machine == "hal") set javaStr = "jdk1.5.0_13"
if($machine == "coffee") set javaStr = "jdk1.5.0_12"
if($machine == "cherry" || $machine == "peach" || $machine == "apple") set javaStr = "jdk1.5.0_06"

if($machine == "hal" || $machine == "peach" || $machine == "cherry" || $machine == "apple") set dir = "~/experiments/datasets/sampled/undirected/datasets/"
if($machine == "coffee") set dir = "/mnt/hal-home/bromberg/experiments/datasets/sampled/undirected/datasets/"

#set java_dir = /usr/java/$javaStr/bin/

set java_dir = "/usr/java/default/bin/"

set tau = $2

foreach n (20)
   #foreach tau (1)
           (1 2 3 4 5 6 7 8 9 10)
           
               "$java_dir"java -classpath ./code machineLearning.Graphs.gibbsSamplerMarkovNet -n $n -tau $tau -D $D -epsilon $epsilon -M $M -seed $seed[$R] -random true
               set outFile_base = sampledData_n"$n"_t"$tau"_E"$EPSILON"_M"$M"
               echo $outFile_base
               ./randomizeSampled   $outFile_base".csv"  $seed[$R] $n >&! $outFile_base"_R$R".csv
           end
    # end
end

