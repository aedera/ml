#!/bin/tcsh 

set output = $1   #tailName

#cat  testFaithfulness.csv   | sed '/^#/d'  >! T
cat  testFaithfulness_condSet.csv   | sed '/^#/d'  >! T

set input = T

    
set dollar = '$'

set title = ""
set x_label = ""
set key = ""
set dataset = ""
    
foreach version (weightedcost hamming accuracies)
     echo '\documentclass[twoside,11pt,letter]{article}' >! LATEX.tex
     echo '\usepackage{amsmath}' >> LATEX.tex
     echo '\usepackage{epsfig}' >> LATEX.tex
     
     echo '\\begin{document}' >> LATEX.tex

         
     set pdf_header = $version
     foreach n (6.000)  
               #echo "\\section {$dollar \\beta= $dollar $beta, n=$n, N=$N }" >> LATEX.tex
               set gnuplot_curves = ""
               #foreach E (0.000 75.000 100.000 120.000 150.000 175.000 200.000)
               foreach E (200.000)
                    #echo "\\Huge {n=$n, E=$E } \\\\" >> LATEX.tex
                               #OUTPUT FILE                   
                               #set M_factor_100 = `gawk -v MFACTOR="$M_factor" 'BEGIN{print MFACTOR*100}' /dev/null`
                               set output_eps = "E$E""_n$n"".eps"

                               foreach tau (1.000 2.000 4.000 8.000)
                                    set title = "{/=36 n = $n, E = $E}"
                                    set x_label = "|D|"
                                    set key = "{/=24 Accuracy}"
                                    set range = ""
                                    
                                    #FILTER DATA FILE                   
                                    set dataset = "T_DIFF_$tau"
                                    cat $input | gawk -F,  -v n="$n" -v TAU=$tau -v E="$E"  \
                                              '{ if($9==n && $11==TAU && $15==E) print $13,$7}' >! $dataset

                                    #cat $dataset                                         
                                    #exit
                                    
                                    if(!(-z "T_DIFF_$E")) then
                                        set gnuplot_curves = `echo "$gnuplot_curves " '"'"T_DIFF_$tau"'"' "with linespoints ps 1.5 lt $tau pt $tau, " # title " '"' "$key, t =$tau" '"'  `
                                        #echo $gnuplot_curves
                                    endif
                                    
                              end                              
                       echo $output_eps
                       set gnuplot_curves = `echo $gnuplot_curves "0"`
                            
                               #TITLE AND LABEL
                                #GNUPLOT HISTOGRAMS
                                if(!(-z T_DIFF_100.00)) then
                                 #echo plotting
                                  gnuplot <<EOF
                                      reset
                                      
                                      set terminal postscript enhanced 20
                                      set title "$title" 
                                      set bmargin 4
                                      set lmargin 8
                                   
                                      unset key
                                      #set key left top Left samplen 5.5 reverse
                                      set key spacing 1.5 width 0 bottom right box
                                      
                                      unset label
                                      set label 1 "Accuracy" at screen .005,.5 center rotate by 90 font "Helvetica,34"
                                      set label 2 "|D|" at screen .5,.04 center font "Helvetica,34"
               
                                      #set xtics ('10', 0, '40', 1, ) font "Helvetica,28"
                                      set ytics font "Helvetica,28"
                                      set output "$output_eps"
                                      set logscale x
               
                                      plot [45:55000][0.5:1] $gnuplot_curves
                                      exit
EOF
                                   echo $output_eps

                                   rm -f 'T_DIFF_*'
                              
                                   echo " \\epsfig { figure=$output_eps, width=7.5cm,angle=-90 } \\\\" >> LATEX.tex
                                endif
                       end #E

                  #echo "\\\\" >> LATEX.tex
              end #n
               echo '\\end{document}' >> LATEX.tex
               
               cat LATEX.tex
               latex LATEX.tex
               dvips -Ppdf -G0 -f LATEX.dvi -o LATEX.ps
               ps2pdf -dEmbedAllFonts=true -dCompatibilityLevel=1.4 LATEX.ps "testFaithfulness_taus.pdf"
               rm -f LATEX.*
               rm -f *.eps


