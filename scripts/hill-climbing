#!/bin/sh

DIR_LIB=../lib
CLASS="machineLearning.experiments.HillClimbingIBSB"
CLASSES=../bin/class
LIB=`ls $DIR_LIB/*.jar | tr '\n' ':'`
CLASSPATH=$LIB$CLASSES
OPT_JVM=""
OPTIONS=""
DELIMITER=":"

while [ "$#" -gt "0" ]; do
    case "$1" in
        -d|--dataset)
            shift
            DATASET=$1
            ;;
        -a|--accuracy)
            shift
            ACCURACY=$1
            ;;
        -Xms)
            shift
            XMS=$1
            ;;
        -Xmx)
            shift
            XMX=$1
            ;;
        -f|--file)
            shift
            FILE=$1
            ;;
        -o|--output)
            OUTPUT=1
            ;;
        -i|--iter)
            shift
            ITER=$1
            ;;
        -s|--samples)
            shift

            if [ -n "$SAMPLES" ]; then
                SAMPLES=$SAMPLES$DELIMITER$1
            else
                SAMPLES=$1
            fi

            ;;
        -r|--regularize)
            shift
            REGULARIZE=$1
            ;;
        *)
            help
            ;;
    esac

    shift
done

help() {
    echo "Usage: $0 -d dataset [-Xms xms] [-Xmx xmx] [-a accuracy] [-s x:xx:xxx] [-o] [-file output]"
    exit 2
}

set_options() {
    if [ "$XMS" ]; then
        OPT_JVM=$OPT_JVM" -Xms$XMS"
    fi

    if [ "$XMX" ]; then
        OPT_JVM=$OPT_JVM" -Xmx$XMX"
    fi

    OPT_JVM=$OPT_JVM" -cp $CLASSPATH -Djava.library.path=$DIR_LIB"
    OPTIONS="-dataset $DATASET"

    if [ "$ACCURACY" ]; then
        OPTIONS=$OPTIONS" -a $ACCURACY"
    fi

    if [ "$REGULARIZE" ]; then
        OPTIONS=$OPTIONS" -regularize $REGULARIZE"
    fi

    if [ "$ITER" ]; then
        OPTIONS=$OPTIONS" -iter $ITER"
    fi

    [ "$FILE" ] || FILE=`date +"%y%m%d-%H%M"`
}

call_java() {
    if [ "$OUTPUT" ]; then
        java $OPT_JVM $CLASS $OPT >&1
    else
        java $OPT_JVM $CLASS $OPT >> $FILE
    fi
}

[ "$DATASET" ] || help

set_options

if [ `echo $SAMPLES | grep $DELIMITER` ]; then
    COUNT=1
    SIZE=`echo $SAMPLES | cut -d: -f$COUNT`
    while [ -n "$SIZE" ]; do
        OPT=$OPTIONS" -samples $SIZE"

       call_java

        COUNT=`expr $COUNT + 1`
        SIZE=`echo $SAMPLES | cut -d: -f$COUNT`
    done
else
    OPT=$OPTIONS" -samples $SAMPLES"
   call_java
fi
