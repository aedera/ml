#!/bin/csh
#skip 6 + n , so argv[3] is n

set skip = `expr $argv[3]` 
set size = `wc -l $1 | gawk '{print $1}'`
set remain = `expr $size - $skip`

head -"$skip" $1 >! AUX_FILE
tail -"$remain" $1 | gawk -v SEED=$2 'BEGIN {srand(SEED)} {printf "%.15g %s\n", rand(),$0}' | sort -n | sed 's/^[^ ]*[ ]//' >! AUX_FILE2
cat AUX_FILE AUX_FILE2 | more
