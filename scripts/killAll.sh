#/bin/bash

for box in master nodo1 nodo2 nodo3 nodo4 nodo5 nodo6 nodo7 nodo9 nodo10
do
	(ssh $box ps -ef | grep sampleData | awk '{print $2}')  | xargs ssh $box kill
	(ssh $box ps -ef | grep java | awk '{print $2}')  | xargs ssh $box kill
done
