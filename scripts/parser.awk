#!/usr/bin/awk -f

BEGIN { print "DOG HAMMING IB SB" }

/^#/ {}

{
    FS = ","

    regularize[0] = "NORMAL"
    regularize[1] = "BIC"
    regularize[2] = "L1"
    regularize[3] = "L2"
    regularize[4] = "BLOCK_L1"

    for (i = 0; i < length(regularize); i++)
        if ($6 == regularize[i])
            printf "%s %3d %2d %d %f %f\n", $6, $10, $15, $17, $19, -$21
}