#/bin/bash

for box in master nodo1 nodo2 nodo3 nodo4 nodo5 nodo6 nodo7 nodo9 nodo10
do
	echo "----------------------------------------------"
	echo "$box"
	ssh $box ps -ef | grep sampleData 
	ssh $box ps -ef | grep java 
done
