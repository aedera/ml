#!/bin/bash

#master
echo "---------------------------------------------"
echo "Master"
ssh master <<EOF
	cd experiments/datasets/sampled/undirected/generateData
	./tcsh sampleData.sh cluster 75 1 1.5 150 '7 8 9 10' & 
EOF &

#nodo1
echo "---------------------------------------------"
echo "Nodo1"
ssh nodo1 <<EOF
        cd experiments/datasets/sampled/undirected/generateData
        ./tcsh sampleData.sh cluster 75 2 1.5 150 '6 7 8 9 10' &
EOF &


echo "---------------------------------------------"
echo "Nodo2"
ssh nodo2 <<EOF
        cd experiments/datasets/sampled/undirected/generateData
        ./tcsh sampleData.sh cluster 75 4 1.5 150 '5 6 7 8 9 10' &
EOF &

echo "---------------------------------------------"
echo "Nodo3"
ssh nodo3 <<EOF
        cd experiments/datasets/sampled/undirected/generateData
        ./tcsh sampleData.sh cluster 75 8 1.5 150 '6 7 8 9 10' &
EOF &

echo "---------------------------------------------"
echo "Nodo4"
ssh nodo4 <<EOF
        cd experiments/datasets/sampled/undirected/generateData
        ./tcsh sampleData.sh cluster 75 1 2.5 250 '8 9 10' &
EOF &

echo "---------------------------------------------"
echo "Nodo5"
ssh nodo5 <<EOF
        cd experiments/datasets/sampled/undirected/generateData
        ./tcsh sampleData.sh cluster 75 2 2.5 250 '5 6 7 8 9 10' &
EOF &

echo "---------------------------------------------"
echo "Nodo6"
ssh nodo6 <<EOF
	cd experiments/datasets/sampled/undirected/generateData
        ./tcsh sampleData.sh cluster 75 4 2.5 250 '6 7 8 9 10' &
EOF &

echo "---------------------------------------------"
echo "Nodo7"
ssh nodo7 <<EOF
        cd experiments/datasets/sampled/undirected/generateData
        ./tcsh sampleData.sh cluster 75 8 2.5 250 '5 6 7 8 9 10' &
EOF &

echo "---------------------------------------------"
echo "Nodo9"
ssh nodo9 <<EOF
        cd experiments/datasets/sampled/undirected/generateData
        ./tcsh sampleData.sh cluster 75 4 1.5 150 '10 9 8 7 6 5' &
EOF &

echo "---------------------------------------------"
echo "Nodo10"
ssh nodo10 <<EOF
        cd experiments/datasets/sampled/undirected/generateData
        ./tcsh sampleData.sh cluster 75 8 1.5 150 '10 9 8 7 6' & 
EOF &
