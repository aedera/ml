package org.utn.ia.ml.learners.deprec.eda;

import java.util.GregorianCalendar;

import org.utn.ia.ml.learners.gsmn.GSMN;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.BayesianTest;
import machineLearning.independenceBased.IndependenceTest;
import machineLearning.independenceBased.TripletSets;


/**
 * @author fschluter
 */
public class EdgesIBScoreStructureLearner {
	
	public UGraphSimple outputNetwork;
	private BayesianTest bt;
	private int n;
	public long runtime;
	
	public void run(FastDataset dataset) {
		long beginRuntime = new GregorianCalendar().getTimeInMillis();
		this.n = dataset.numberOfAttributes();
		this.bt = new BayesianTest(dataset, true, n); 
		outputNetwork = new UGraphSimple(n);
//		Double[][] CE = createMatrixOfEdgesIBScores();
//		for (int X = 0; X < n - 1; ++X) {
//			for (int Y = X; Y < n; ++Y) {
//				if (CE[X][Y] > CE[Y][X]) {  //triangular superior es dependencia, triangular inferior es independencia
//					outputNetwork.addEdge(X,Y);
//				}
//			}
//		}
		GSMN gsmn = new GSMN(bt);
		gsmn.run();
		outputNetwork = gsmn.outputNetwork;
		IBScoreBasedShrinkPhase();
		runtime = new GregorianCalendar().getTimeInMillis() - beginRuntime;
	}
	
	private void IBScoreBasedShrinkPhase() {
		for (int X = 0; X < n - 1; ++X) {
			for (int Y = X+1; Y < n ; ++Y) {
				FastSet blanketX = getBlanketOf(X, Y, outputNetwork);
				FastSet blanketY = getBlanketOf(Y, X, outputNetwork);
				TripletSets t1 = new TripletSets(X, Y, blanketX);
				TripletSets t2 = new TripletSets(Y, X, blanketY);
				double[] probT1 = bt.probs(bt.log_likelihoods(t1));
				double[] probT2 = bt.probs(bt.log_likelihoods(t2));
				double dependenceProb = probT1[IndependenceTest.D]*probT2[IndependenceTest.D];
				double independenceProb = probT1[IndependenceTest.I]*probT2[IndependenceTest.I];
				if(dependenceProb> independenceProb){
					outputNetwork.addEdge(X,Y);
				}else{
					outputNetwork.removeEdge(X,Y);
				}
			}
		}
	}

	private FastSet getBlanketOf(int x, int y, UGraphSimple outputNetwork) {
		FastSet blanketOfX =  outputNetwork.getBlanketOf(x);
		blanketOfX.remove(y);
		return blanketOfX;
	}

	/**
	 * 1. Create a matrix of edge-IB-scores, 
	 * @return
	 */
	@SuppressWarnings("unused")
	private Double[][] createMatrixOfEdgesIBScores() {
		Double[][] edgesIBScoreMatrix = new Double[n][n];
		for (int X = 0; X < n - 1; ++X) {
			for (int Y = X; Y < n ; ++Y) {
				FastSet blanketXY = getRestOfUniverse(n, X, Y);
				TripletSets t1 = new TripletSets(X, Y, blanketXY);
				double[] probT1 = bt.probs(bt.log_likelihoods(t1));
				edgesIBScoreMatrix[X][Y] = probT1[IndependenceTest.D];
				edgesIBScoreMatrix[Y][X] = probT1[IndependenceTest.I];
			}
		}
		return edgesIBScoreMatrix;
	}

	private FastSet getRestOfUniverse(int n, int x, int y) {
		FastSet restOfUniverse = new FastSet(n);
		for (int i = 0; i < n; ++i) {
			if (i != x && i != y) {
				restOfUniverse.add(i);
			}
		}
		return restOfUniverse;
	}
	
}
