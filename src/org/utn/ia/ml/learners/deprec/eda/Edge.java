package org.utn.ia.ml.learners.deprec.eda;

/**
 * @author fschluter
 *
 */
public class Edge implements Comparable<Edge>{

	public int X;
	public int Y;
	public Double crossEntropy;

	public Edge(int X, int Y, Double crossEntropy) {
		this.X = X;
		this.Y = Y;
		this.crossEntropy = crossEntropy;
	}
	
	@Override
	public int compareTo(Edge edge) {
		return (int) Math.signum(edge.crossEntropy - crossEntropy);
	}

}
