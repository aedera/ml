package org.utn.ia.ml.learners.deprec.eda;

import java.util.GregorianCalendar;
import java.util.TreeSet;

import machineLearning.Datasets.FastDataset;
import machineLearning.Datasets.FastInstance;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;


/**
 * @author fschluter
 * 1. Create a matrix of mutual information, M I, by estimating cross entropy for each pair of
 * variables in the solution. Cross entropy between two random variables, A and B, is given
 * by:
 *                                                           p(a, b)
 *                         CE(A, B) =  sum {  p(a, b) * log ------------ }
 *                                     a,b                   p(a) · p(b)
 *                                     
 *  where sum is over all possible combinations of A and B, and p(a, b) is the joint probability
 * of A = a and B = b computed from D
 * 
 * 2. Create an edge between two variables, if the mutual information between them is higher
 * than the given threshold. Here we compute the threshold, T R as T R = avg(M I) ∗ sig,
 *    where avg(M I) is the average of the elements of the MI matrix and sig is the significance
 *    parameter, which for the purpose of this paper is set to 1.5.
 *    
 * 3. If the number of neighbours to a variable is higher than the maximum number, M N , allowed,
 * only keep M N neighbours that have the highest mutual information.
 * 
 * @param population
 * @return
 */
public class MutualInformationStructureLearner {
	
	public UGraphSimple outputNetwork;
	public double sig = 1.5; // significance parameter
	public int MN = Integer.MAX_VALUE; // Maximum number of neighbors allowed
	private FastDataset dataset;
	private double[] domain = {0,1};
	private int n;
	public long runtime;
	public void run(FastDataset dataset, int MN, double sig) {
		long beginRuntime = new GregorianCalendar().getTimeInMillis();
		this.dataset = dataset;
		this.MN = MN;
		this.sig = sig;
		this.n = dataset.numberOfAttributes();
		outputNetwork = new UGraphSimple(n);
		Double[][] CE = createMatrixOfMutualInformation();
		double TR = computeThreshold(CE);
		for (int X = 0; X < n - 1; ++X) {
			TreeSet<Edge> edges = new TreeSet<Edge>();
			for (int Y = X; Y < n; ++Y) {
				if (CE[X][Y] > TR) {
					Edge edgeXY = new Edge(X,Y, CE[X][Y]);
					edges.add(edgeXY);
				}
			}
			int edgesAdded = 0;
			for (Edge edge: edges) {
				if(edgesAdded < MN){
					outputNetwork.addEdge(edge.X, edge.Y);
				}
			}
		}
		runtime = new GregorianCalendar().getTimeInMillis() - beginRuntime;
	}
	
	/**
	 * 1. Create a matrix of mutual information, M I, by estimating cross entropy for each pair of
	 * variables in the solution. Cross entropy between two random variables, A and B, is given
	 * by:
	 *                                                           p(a, b)
	 *                         CE(A, B) =  sum {  p(a, b) * log ------------ }
	 *                                     a,b                   p(a) · p(b)
	 *                                     
	 *  where sum is over all possible combinations of A and B, and p(a, b) is the joint probability
	 * of A = a and B = b computed from D
	 * @return
	 */
	private Double[][] createMatrixOfMutualInformation() {
		Double[][] CE = new Double[n][n];
		for (int X = 0; X < n - 1; ++X) {
			for (int Y = X; Y < n ; ++Y) {
				CE[X][Y] = 0d;
				int[][] configuration = getPossibleConfigurations();  //datasets are binary, for now
				for (int i = 0; i < configuration.length; ++i) {
					double[] probabilities = pr(X,configuration[i][0], Y, configuration[i][1]);
					double jointProbabilityXY = probabilities[0];
					double probabilityX = probabilities[1];
					double probabilityY = probabilities[2];
					CE[X][Y] +=  jointProbabilityXY * Math.log(jointProbabilityXY / probabilityX * probabilityY);
				}
			}
		}
		return CE;
	}

	/**
	 * Returns an array, 
	 * with the probability of (x=xValue && y=yValue), 
	 * the probability of (x=xValue && y=yValue), 
	 * and the probability of (x=xValue && y=yValue)
	 * @param x
	 * @param xValue
	 * @param y
	 * @param yValue
	 * @return
	 */
	private double[] pr(int x, int xValue, int y, int yValue) {
		int sumXY = 0;
		int sumX = 0;
		int sumY = 0;
		int D = dataset.instances.size();
		for (FastInstance instance : dataset.instances) {
			if (instance.values[x] == xValue && instance.values[y] == yValue) {
				++sumXY; 
				++sumX; 
				++sumY; 
			}else if(instance.values[x] == xValue ){
				++sumX; 
			}else if(instance.values[y] == yValue ){
				++sumY; 
			}
		}
		double prXY = sumXY / D; 
		double prX = sumX / D; 
		double prY = sumY / D; 
		double[] returnArray = {prXY,prX,prY};
		return returnArray;
	}


	/**
	 * Return all possible configuration between the variables of the Universe (0 to n). 
	 * The variable @variable domain contains the domain for all the variables (i.e., )
	 * Returns 
	 * @param n
	 * @param domainSize
	 */
	private int[][] getPossibleConfigurations() {
		int[][] binary = { { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 } };
		assert domain.length == 2;
		return binary;
	}

	/**
	 * MOA paper computes the threshold TR:
	 * 
	 *  TR as TR = avg(M I) ∗ sig,
	 *  
	 *  where avg(M I) is the average of the elements of the MI matrix and sig is the significance
	 *  parameter, which for the purpose of this paper is set to 1.5.
	 * @param CE
	 * @return
	 */
	private double computeThreshold(Double[][] CE) {
		double sum = 0;
		int times = 0;
		for (int X = 0; X < n - 1; ++X) {
			for (int Y = X; Y < n; ++Y) {
				sum += CE[X][Y];
				++times;
			}
		}
		assert times == n * (n-1) / 2;
		double avg = sum / times;  
		return avg * sig;
	}
}
