package org.utn.ia.ml.learners.deprec.eda;

public class Individual implements Comparable<Individual> {
	public String chromosome;
	public double fitnessValue;
	FitnessFunction fitnessFn;

	public Individual(String chromosome, FitnessFunction fitnessFn) {
		this.chromosome = chromosome;
		this.fitnessValue = fitnessFn.getValue(chromosome);
		this.fitnessFn = fitnessFn;
	}

	@Override
	public int compareTo(Individual individual) {
		return (int) Math.signum(individual.fitnessValue - fitnessValue);
	}

	@Override
	public String toString() {
		return "" + fitnessValue;
		// return chromosome + "(" + fitnessValue + ")";
	}

	@Override
	public int hashCode() {
		return chromosome.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return chromosome.equals(((Individual) obj).chromosome);
	}

	public int[] intArray() {
		char[] charArray = chromosome.toCharArray();
		int[] intArray = new int[chromosome.length()];
		for (int i = 0; i < chromosome.length(); ++i) {
			intArray[i] = new Integer(charArray[i]);
		}
		return intArray;
	}
}