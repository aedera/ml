/**
 * 
 */
package org.utn.ia.ml.learners.deprec.eda;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import machineLearning.Datasets.FastDataset;
import machineLearning.Datasets.FastInstance;

/**
 * @author fschluter
 * 
 */
public class Population {

	public int individualLenght;
	public List<Individual> individuals;

	public Population() {
		individuals = new ArrayList<Individual>();
	}

	public Population(Set<String> populationStrings, int individualLenght,
			FitnessFunction fitnessFn) {
		this.individualLenght = individualLenght;
		validatePopulation(populationStrings);
		individuals = new ArrayList<Individual>();
		for (String individualString : populationStrings) {
			Individual individual = new Individual(individualString, fitnessFn);
			individuals.add(individual);
		}
	}

	public Population(List<Individual> individuals) {
		this.individuals = new ArrayList<Individual>(individuals);
		individualLenght = !individuals.isEmpty() ? individuals.get(0).chromosome
				.length()
				: 0;
	}

	private void validatePopulation(Set<String> population) {

		// Require at least 1 individual in population in order
		// for algorithm to work
		if (population.size() < 1) {
			throw new IllegalArgumentException(
					"Must start with at least a population of size 1");
		}
		// String lengths are assumed to be of fixed size,
		// therefore ensure initial populations lengths correspond to this
		for (String individual : population) {
			if (individual.length() != this.individualLenght) {
				throw new IllegalArgumentException("Individual [" + individual
						+ "] in population is not the required length of "
						+ this.individualLenght);
			}
		}
	}

	public int size() {
		return individuals.size();
	}

	public Individual get(int individualIndex) {
		return individuals.get(individualIndex);
	}

	public List<Individual> subList(int fromIndex, int toIndex) {
		try {
			return individuals.subList(fromIndex, toIndex);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	public boolean contains(Individual child) {
		return individuals.contains(child);
	}

	public void add(Individual child) {
		individuals.add(child);
	}

	public void remove(Individual child) {
		individuals.remove(child);
	}

	/**
	 * Replace parent by child
	 **/
	public void replace(Individual parent, Individual child) {
		remove(parent);
		add(child);
	}

	public double getAvgFitness() {
		double totalFitness = 0;
		for (Individual individual : individuals) {
			totalFitness += individual.fitnessValue;
		}
		return totalFitness / individuals.size();
	}

	@Override
	public String toString() {
		return individuals.toString();
	}

	public FastDataset convertToDataset() {
		FastDataset dataset = new FastDataset();
		for (Individual individual : individuals) {
			FastInstance instance = new FastInstance(individual.intArray());
			dataset.addInstance(instance);
		}
		return dataset;
	}
}
