package org.utn.ia.ml.learners.deprec.eda;


import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;

public class EDA {

	protected final int individualLenght;
	private Random random;
	protected int M;  //population size
	private int N;    // size of selected population
	public double sig = 1.5; // significance parameter of the MUTUAL INFORMATION LEARNER
	public int MN = Integer.MAX_VALUE; // Maximum number of neighbors allowed by the MUTUAL INFORMATION LEARNER
	private int endCondition = 1000;
	protected FitnessFunction fitnessFn;
	private Individual bestIndividual;
	public long runtime = 0;
	private String bestFitnessEvolution;
	private int maxNumberOfIterations;

	public EDA(int individualLength, int M, int N, int maxNumberOfIterations, int endCondition, FitnessFunction fitnessFn, int seed) {
		this.individualLenght = individualLength;
		this.M = M;
		this.N = N;
		assert N <= M;
		this.maxNumberOfIterations = maxNumberOfIterations;
		this.endCondition = endCondition;
		this.fitnessFn = fitnessFn;
		this.random = new Random(seed);
	}

	public String run() {

		long beginRuntime = new GregorianCalendar().getTimeInMillis();
		
		
		bestIndividual = null;

		Population P = new Population(generateRandomPopulation(),
				individualLenght, fitnessFn);

		int noEvolution = 0;
		for (int i = 0; i < maxNumberOfIterations; i++) {
			P = eda(P);

			Individual better = P.get(0);
			if (bestIndividual == null || bestIndividual.equals(better)) {
				if (noEvolution == endCondition) {
					runtime = new GregorianCalendar().getTimeInMillis()
							- beginRuntime;
					return bestIndividual.chromosome;
				}
				if (bestIndividual == null) {
					bestIndividual = better;
				}
				++noEvolution;
			} else {
				noEvolution = 0;
				bestIndividual = better;
			}

			bestFitnessEvolution += i + " " + bestIndividual.fitnessValue + " "
					+ P.getAvgFitness() + "\n";

		}

		runtime = new GregorianCalendar().getTimeInMillis() - beginRuntime;
		return bestIndividual.chromosome;
	}


	private Set<String> generateRandomPopulation(){
		Set<String> population = new HashSet<String>();

		for (int i = 0; i < M; i++) {
			population.add(generateRandomIndividual());
		}

		return population;
	}
	
	private String generateRandomIndividual() {
		String randomIndividual = "";
		for (int i = 0; i < individualLenght; i++) {
			int bit = random.nextInt(2);
			randomIndividual += bit;
		}
		return randomIndividual;
	}

	/**
	 * @author fschluter
	 * Implementation of MOA algorithm
	 * 
	 * Markovianity based Optimisation Algorithm
	 * 1. Generate initial (parent) population P of size M
	 * 2. Select set D from P consisting of N solutions, where N <= M
	 * 3. Estimate structure of a Markov network from D
	 * 4. Estimate local Markov conditional probabilities, p(xi |Ni ), for each variable Xi as defined
	 *   by the undirected structure and sample them to generate new population
	 * 5. Replace old population by new one and go to step 2 until termination criteria are meet
	 * 
	 */
	protected Population eda(Population P) {
		Population D = selectIndividuals(P, N);
		UGraphSimple structure = generateStructure(D);
		Population nextPopulation = sample(structure);
		return nextPopulation;
	}

	/**
	 * 
	 * @param population
	 * @param populationChilds
	 * @param fitnessFn
	 * @return
	 */
	protected Population selectIndividuals(Population population, int N) {
		List<Individual> newPopulation = new ArrayList<Individual>();
		newPopulation.addAll(population.individuals);
		Collections.sort(newPopulation);
		return new Population(newPopulation.subList(0, N));
	}

	/**
	 * Esta estructura podría ser aprendida con el método de MOA (con cross over  y mutual information)
	 * ó con GSMN, o con iBMAPHC...  
	 * 
	 * @param population
	 * @return
	 */
	private UGraphSimple generateStructure(Population population) {
		return generateStructureByCrossEntropy(population);
	}

	/**
	 * 1. Create a matrix of mutual information, M I, by estimating cross entropy for each pair of
     * variables in the solution. Cross entropy between two random variables, A and B, is given
     * by:
     *                                                           p(a, b)
     *                         CE(A, B) =  SUM {  p(a, b) * log ------------ }
     *                                     a,b                   p(a) · p(b)
     *                                     
     *  where sum is over all possible combinations of A and B, and p(a, b) is the joint probability
     * of A = a and B = b computed from D
     * 
     * 2. Create an edge between two variables, if the mutual information between them is higher
     * than the given threshold. Here we compute the threshold, T R as T R = avg(M I) ∗ sig,
     *    where avg(M I) is the average of the elements of the MI matrix and sig is the significance
     *    parameter, which for the purpose of this paper is set to 1.5.
     * 3. If the number of neighbours to a variable is higher than the maximum number, M N , allowed,
     * only keep M N neighbours that have the highest mutual information.
     * 
	 * @param population
	 * @return
	 */
	private UGraphSimple generateStructureByCrossEntropy(Population population) {
		FastDataset dataset = population.convertToDataset();
		MutualInformationStructureLearner learner = new MutualInformationStructureLearner();
		learner.run(dataset, MN, sig);
		return learner.outputNetwork;
	}

	/**
	 * @param structure
	 * @return
	 */
	private Population sample(UGraphSimple structure) {
		return null;
	}
	

}
