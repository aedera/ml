package org.utn.ia.ml.learners.deprec.eda.moa;

import java.util.HashMap;

/**
 * @author fschluter
 * Implementation of MOA algorithm
 * 
 * Markovianity based Optimisation Algorithm
 * 1. Generate initial (parent) population P of size M
 * 2. Select set D from P consisting of N solutions, where N <= M
 * 3. Estimate structure of a Markov network from D
 * 4. Estimate local Markov conditional probabilities, p(xi |Ni ), for each variable Xi as defined
 *   by the undirected structure and sample them to generate new population
 * 5. Replace old population by new one and go to step 2 until termination criteria are meet
 * 
 */
public class MOA {

	public void run(int M, int N){
		Population P = generateInitialPopulation(M);
		assert N<=M;
		while(true){
			Population D = selectSolutions(P,N);
			MarkovNetwork mn = estimateStructure(D);
			HashMap<Integer,Double> conditionalProbabilities = estimateConditionalProbabilities(mn);
			P = sample(conditionalProbabilities); 
		}
	}


	private Population generateInitialPopulation(int m2) {
		return null;
	}

	private Population selectSolutions(Population p, int n2) {
		return null;
	}
	
	private MarkovNetwork estimateStructure(Population d) {
		return null;
	}

	private HashMap<Integer, Double> estimateConditionalProbabilities(
			MarkovNetwork mn) {
		return null;
	}

	private Population sample(HashMap<Integer, Double> conditionalProbabilities) {
		return null;
	}
}

