package org.utn.ia.ml.learners.gsmn;


import java.util.Arrays;
import java.util.GregorianCalendar;

import machineLearning.Datasets.Statistics;
import machineLearning.Graphs.GraphRVar;
import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FactSimpleGSIMN;
import machineLearning.Utils.FastSet;
import machineLearning.Utils.Output;
import machineLearning.Utils.Set;
import machineLearning.Utils.Utils;
import machineLearning.independenceBased.IBasedAlgorithm;
import machineLearning.independenceBased.IndependenceTest;
import machineLearning.independenceBased.Triplet;
import machineLearning.independenceBased.pFact;

/**
 * @author Facundo Bromberg
 *
 */
public class GSMN implements IBasedAlgorithm {
	static public Output output;
	
    protected boolean gsimn = false;
    protected boolean feedForward = false;
    protected boolean parcial = false;	//not all tests are known. It thus return some structure that is consistent with those known.

    public double logProbability = 0d;
    public double logpvalues [] = new double[2];
    
    double [][] p;
    int [] pi;// = new int[N];
    boolean [] visited;
    int [][] lambda;
    int [][] ordering;

    IndependenceTest IT;
    
    //Parameters
    static short MI_TYPE = Statistics.AVERAGE;
    
    /*static int default_test_type = Statistics.CHISQUARE;
    static double default_test_confidence = 0.05;
    boolean default_assume_independence = false;*/
	
    
	//public FastDataset data = null;
	//UGraph realMN;
//	public UGraphSimple outNet; 
	public UGraphSimple outputNetwork;
	public MarkovNet trueNet=null;		//generated by the GSMN, GSIMN, etc. algorithms

    //static boolean ScreenOutput = false;
	//boolean useMIordering = true;
    
    //Output
	//int testSavings = 0;
	int testsCost = 0;
	public int testsWeightedCost = 0;
    //int nTestsFailed = 0;
	//double accuracy;
    
    double numTestedD =0, numTestedI = 0;
    double Iaccuracy = 0, Daccuracy = 0;

	//Random r;
    int n;
	
    KBsimpleGSIMN KI, KF;

    int countProof1 = 0, countProof2=0, countProof3=0, countProof4=0;
    //forwardChainingSimple fch;//fch, fch;
	KBsimpleGSIMN TestedFacts;//GSMBTestedFacts, GSIMBTestedFactsTestedFacts, GSMBFCHTestedFacts, GSIMBFCHTestedFacts; 
	//Statistics Stats = new Statistics();

	//public Accuracy acc;

	FactSimpleGSIMN newFact, newFact2; 
	
	//public consistencyOracleBase co = null;
	//public static boolean fromConsistencyOracle;
	
	int numIncorrect = 0;

	private long runtime;

	private boolean[][] growAssignments;

	public  boolean print = false;

	public GSMN(){
		this.n = 0;
        this.IT = null;
        
        lambda = new int[n][];
        pi = new int[n];
        visited = new boolean[n];
        for(int i=0; i<n; i++) {
            pi[i] = i;
            lambda[i] = new int[n-1];
            for(int j=0; j<n; j++){
                if(i == j) continue;
                if(j<i) lambda[i][j] = j;
                else if(j>i) lambda[i][j-1] = j;
            }
        }
    }
    public GSMN(IndependenceTest IT){
		this.n = IT.numVars();
        this.IT = IT;
        
        lambda = new int[n][];
        pi = new int[n];
        visited = new boolean[n];
        for(int i=0; i<n; i++) {
            pi[i] = i;
            lambda[i] = new int[n-1];
            for(int j=0; j<n; j++){
                if(i == j) continue;
                if(j<i) lambda[i][j] = j;
                else if(j>i) lambda[i][j-1] = j;
            }
        }
    }
	protected boolean I(int x, int y, Set U, String axiom) {
	    return test(x,y,U, axiom);
	}
    
	protected boolean test(int x, int y, Set U, String axiom){
		//boolean [] testFailed = new boolean[1];
		//boolean ret=false, ret_data=false;
		pFact retFact = null;
		FastSet Z = new FastSet(U, n);
		Triplet triplet = new Triplet(x, y, Z);
		
		FactSimpleGSIMN f = new FactSimpleGSIMN(n, true, x, y, new Set(U));
		

		f.axiom(axiom);
		f.order(testsCost);
		
		testsWeightedCost += 2+U.size();
		testsCost++;
		
		retFact = IT.independent(triplet);
		if(print) System.out.print("\nTESTING " + triplet + " = " + Utils.myDoubleToString(Math.exp(retFact.logpvalue()),2));
		
		boolean trueNetSimpleIndependent = trueNet!=null? (new UGraphSimple(trueNet)).separates_nonRecursive2(triplet.X(), triplet.Y(), triplet.getCond()) : true;
		boolean correct = (retFact.truth_value == trueNetSimpleIndependent);
		if(!correct) numIncorrect++; 
		if(print) System.out.print("  " + (correct?"  CORRECT":"  INCORRECT")+ "(" + numIncorrect + ")  ");

		/** stores p_values in p vector */
        if(U.size() == 0) p[x][y] = p[y][x] = retFact.logpvalue();

        //short proofType = Argument.PROOF_COMPLETE;
    	if(print){
			String trueModelStr = "null";
			if(IT.hasTrueModel()){
				trueModelStr = IT.independentInTrueModel(triplet)?"I":"D";
			}
			String dataStr = "null";
			String dataOK = "null";  //in data different than in true model
			if(IT.hasData()){
				dataStr = IT.independentInData(triplet)?"I":"D";

				if(dataStr.equals(trueModelStr)) dataOK = "+";
				else dataOK = "-";
			}
			String correctedStr = "null";
			String correctedOK = "null"; //flipped by correction algorithm
			if(IT.hasTestCorrection()){
				correctedStr = IT.independentCorrected(triplet)?"I":"D";
				if(correctedStr.equals(trueModelStr)) correctedOK = "+";
				else correctedOK = "-";
			}
			
			//output.println(co.toStringProof(retFact));
			
			if(print) System.out.print("("+ dataOK + "/"+ correctedOK + ") " + retFact.toStringPValue());
			if(print) System.out.print(", (trueModel, Data, Correction) = (" + trueModelStr + ","+ dataStr + ","+ correctedStr + "), WC=" +testsWeightedCost);
			if(print) System.out.println();
			
		}

		
		f.I(retFact.truth_value);
		if(TestedFacts != null) TestedFacts.addFact(f);//TestedFacts.addFactWithSubSupsets(f2, V);
				
        logpvalues[IndependenceTest.D] = Math.log((-1)*Math.expm1(retFact.logpvalue()));
        logpvalues[IndependenceTest.I] = retFact.logpvalue(); 
//		double suma = Math.exp(logpvalues[IndependenceTest.I]) + Math.exp(logpvalues[IndependenceTest.D]); 

		
		return retFact.truth_value;
	}

	public void run() {

		long beginRuntime = new GregorianCalendar().getTimeInMillis();
		
        p = new double[n][];
        
        growAssignments = new boolean[n][n];
        
        //double [] logpvalue = new double[1];
        //double [] reliability = new double[1];
        //double pvalue;

       // if(!parcial) acc = new Accuracy(n, r, GSMN.default_test_type, GSMN.default_test_confidence);
        
        print  = false;

		testsCost = 0;
		testsWeightedCost = 0;

        outputNetwork =null;

        //Initialization
        if(!parcial ) {
        	this.outputNetwork = new UGraphSimple(n);
//			outNet = new UGraphSimple(n);
		}
        
		//FastDataset dataUnc = data;
		
		for(int i=0; i<n; i++) p[i] = new double[n]; 
		for(int i=0; i<n; i++){
			if(parcial) continue;
			
            for(int j=i+1; j<n; j++){
				boolean independent = false;
                //independent = test(i, j, new Set(), "U-TEST", logpvalue, reliability, dataUnc);
				independent = test(i, j, new Set(), "U-TEST");

                if(gsimn){
                    newFact = new FactSimpleGSIMN(n, true, i, j, new Set());
                    if(!parcial) newFact2 = new FactSimpleGSIMN(n, true, j, i, new Set());

                    if(independent){
                        newFact.I(true);
                        newFact2.I(true);
                        KI.addFact(newFact);
                        KI.addFact(newFact2);
                    }
                    else {
                        newFact.I(false);
                        newFact2.I(false);
                        
                        KF.addFact(newFact);
                        KF.addFact(newFact2);
                    }
                }

                
			}
		}
		int random [] = new int[pi.length];
		for(int i=0; i<n; i++) random[i] = i; 
        //if(false || data != null){
		if(true){
        	double [] avg = computeAverage(MI_TYPE);
            pi = auxiliarysort(avg);
        	if(print) System.out.print("pi: [");
            for(int i=0; i<n; i++){
            	if(print) System.out.print(pi[i] + ", ");
                lambda[pi[i]] = auxiliarysort(p[pi[i]]);
            }
        	if(print) System.out.print("]");
        }
		
        ordering  = new int[lambda[0].length][lambda[1].length-1];
        for(int i=0; i<n;i++){
        	System.arraycopy(lambda[i], 0, ordering[i], 0, lambda[i].length-1);
        }
        
//        System.arraycopy(lambda[0], 0, ordering[0], 0, lambda[0].length);
//        System.arraycopy(lambda[1], 0, ordering[0], 0, lambda[1].length);
		
        for(int i=0; i<n;i++) lambda[i] = this.removeFromQueue(lambda[i], i);
        while(pi.length > 0){
//            GraphRVar X = (GraphRVar) outNet.V().elementAt(pi[0]);
            int x = pi[0];
            visited[x] = true;

            Set T = new Set();
//            if(!parcial) T = computeTandRemoveFromLambda(x);
            //computeFandRemoveFromLambda(x);
            
            pi = removeFromQueue(pi, x);

            Set S = new Set();
            
            if(print) {
                System.out.println("-----------------------------------------------");
                System.out.println("X: "+ x + ", " + pi.length +"/"+n);
                System.out.println("T: " + T);

                System.out.print("lambda(X): [");
                
                if(lambda[x].length>0) System.out.print(""+lambda[x][0]);
                for(int i=1; i<lambda[x].length; i++) {
                    System.out.print(","+lambda[x][i]);
                }
                System.out.print("]\n");
                
                System.out.print("log_p_values(X): [");
                if(lambda[x].length>0) System.out.print(""+p[x][lambda[x][0]]);
                for(int i=1; i<lambda[x].length; i++) {
                    System.out.print(","+p[x][lambda[x][i]]);
                }
                System.out.print("]\n");
            }
            
            //Grow Phase
            if(print)System.out.println("ENTERING GROW PHASE");

            while(lambda[x].length > 0){
//                GraphRVar Y = (GraphRVar) outNet.V().elementAt(lambda[x][0]);
                int y = lambda[x][0];
                
                lambda[x] = removeFromQueue(lambda[x], y);

                if(x == y) {
                    Utils.FORCED_ASSERT(x != y, "Exception in GSMN, x==y");
                }

                Set Z;
                if(gsimn) Z = S;
                else Z = S;//Set.Union(S,T);
                
                if(print)System.out.print("Testing: " + x + "," + y +"|"+Z);
                
//                if( (p[x][y] < IT.threshold()) && !I(x,y,Z, "GROW")){
                if( !I(x,y,Z, "GROW")){
                    if(print)System.out.print("   DEPENDENT , WC=" +testsWeightedCost);
                    
                    S.add(y);
                    
                    //Update lambda[y]
//                    if(false && gsimn){
//                        changeposTo0(lambda[y], x);
//                        for(int k=S.size()-2; k>=0; k--){
//                            int S_k = ((GraphRVar)S.elementAt(k)).index(); 
//                            if(!changeposTo0(lambda[y], S_k)){
//    //                          System.out.println("ERROR in lambda change pos" );
//                            }
//                        }
//                    }
                    logProbability += logpvalues[IndependenceTest.D]; 
                }
                else {
                	logProbability += logpvalues[IndependenceTest.I]; 
                	if(print)System.out.print("   INDEPENDENT , WC=" +testsWeightedCost);
                }
			
                if(print) System.out.print("\n");
            }
            
            setGrowAssignments(x, S);
            
            //Update pi
            if(gsimn){
                for(int k=S.size()-1; k>=0; k--){
                    int S_k = ((GraphRVar)S.elementAt(k)).index();
                    if(changeposTo0(pi, S_k)) break;
                }
            }
            
//          Shrink Phase
            if(print)System.out.println("ENTERING SHRINKING PHASE");
            for(int i=S.size()-1; i>=0;i--){
                int y= (Integer) S.elementAt(i);
//                int y = Y.index();
                
                Set ST = S; //Set.Union(S,T);
                
                Set STMinusY = new Set(ST);
                STMinusY.remove(i);
                Set Z = STMinusY;
                
                if(print)System.out.print("Testing: " + x + "," + y +"|"+Z);

                if( I(x, y, Z, "SHRINK")){
                    if(print)System.out.print("   INDEPENDENT , WC=" +testsWeightedCost + "\n");
                    S.remove(i);
                    //lambda[y] = removeFromQueue(lambda[y],x);
                }
                else if(print)System.out.print("   DEPENDENT , WC=" +testsWeightedCost + "\n");
            }
            Set SUT = S; //Set.Union(S,T);
            if(print) System.out.print("\n");
            
            for(int i=0; i<SUT.size(); i++){
            	if(parcial) {
            	}
//            	else outNet.addEdge(x, ((Integer)SUT.elementAt(i)));
            	else outputNetwork.addEdge(x, ((Integer)SUT.elementAt(i)));
            }
        }
/*        if(this.ScreenOutput && true) {
            if(gsimn) System.out.println("Is Correct GSIMN? " + (outNet.hamming(trueNet) == 0));
            else System.out.println("Is Correct GSMN? " + (outNet.hamming(trueNet) == 0));
            
        }*/
        if(print)System.out.println("\n\n");
        
        runtime = new GregorianCalendar().getTimeInMillis() - beginRuntime;
	}
    

	private void setGrowAssignments(int x, Set s) {
		for (int i = 0; i < s.size(); i++) {
			int y = (Integer) s.get(i);
			growAssignments[x][y]=true;
		}
	}
//    private Set computeTandRemoveFromLambda(int x){
//    	Set T = new Set();
//        
//        for(int i=0; i<n; i++){
//        	if(i==x) continue;
//            
//            if(outNet.existEdge(x, i)){
//            	T.add(outNet.getVar(i));
//                lambda[x] = removeFromQueue(lambda[x], i);
//            }
//        }
//        
//        return T;
//    }
//    private void computeFandRemoveFromLambda(int x){
//        next_i: for(int i=0; i<n; i++){
//        	if(i==x) continue;
//            
//        	//Is in pi? (that is, not visited), then continue
//        	for(int j=0; j<pi.length;j++) {
//        		if(pi[j] == i) {
//        			continue next_i;
//        		}
//        	}
//        		
//        	//if reached here, then visited
//            if(!outNet.existEdge(x, i)){
//                lambda[x] = removeFromQueue(lambda[x], i);
//            }
//        }
//    }
    
    private double [] computeAverage(short TYPE){
        double avg [] = new double[n];
        if(TYPE == Statistics.MAX){
            for(int i=0; i<n; i++){
                avg[i] = 0;
                for(int j=0; j<n; j++) avg[i] = p[i][j]>avg[i]?p[i][j]:avg[i];
            }
        }
        if(TYPE == Statistics.MIN){
            for(int i=0; i<n; i++){
                avg[i] = 0;
                for(int j=0; j<n; j++) avg[i] = p[i][j]<avg[i]?p[i][j]:avg[i];
            }
        }
        else if(TYPE == Statistics.AVERAGE){
            for(int i=0; i<n; i++){
                avg[i] = 0;
                for(int j=0; j<n; j++) avg[i] += p[i][j];
                avg[i] /= (double)n;
            }
        }   
        return avg;
    }
	
    /**
	 * It returns queue such that i \leq i'<=> arrayToSort[queue[i]] \leq arrayToSort[queue[i']]
	 *  
	 *  
	 */    
    private int [] auxiliarysort(double [] pvalues){
		int queue [] =new int[pvalues.length];
		
		double arrayToSortCpy [] = new double[pvalues.length];
		System.arraycopy((Object) pvalues, 0, (Object)arrayToSortCpy, 0, pvalues.length);
		double arraySorted [] = new double[pvalues.length];
		System.arraycopy((Object) pvalues, 0, (Object)arraySorted, 0, pvalues.length);

		//A random array of integers to get random ordering in case of ties.
		int K[] =new int[pvalues.length];
		for(int i=0;i<K.length;i++) K[i] = i;
		//K = randomize(K);
		
		Arrays.sort(arraySorted);
		
		//Find a number not in arrayToSort
		double freshNumber = -1;
		if(Arrays.binarySearch(arraySorted, freshNumber) >=  0) {
			System.out.println("DANGER: not a fresh number in auxiliarysort");
			(new Exception()).printStackTrace();
		}
		for(int i=0; i<pvalues.length; i++){
			//Search positioin in original array avg of avgSorted[i].
			int j;
			for(j=0; j<pvalues.length;j++) {
				if(arrayToSortCpy[K[j]] == arraySorted[i]) {
					arrayToSortCpy[K[j]] = freshNumber;
					break;
				}
			}//when break, j=the position in original array of avgSorted[j]
			//queue[arrayToSort.length-i-1] = j;   //Arrays.sort sorts in ascending order, but we want in decreasing order.
            queue[i] = K[j];   //Ascending order.
		}
		//Check:
		for(int i=0; i<pvalues.length-1; i++){
			if(pvalues[queue[i]] > pvalues[queue[i+1]]){
				System.out.println("ERROR: iimproper sort in auxiliarySort");
				(new Exception()).printStackTrace();
			}
		}
		return queue;
	}

    /**
     * 
     * 
     * 
 * 	change move element S_k in queue to position 0. In place, i.e., does it on queue. Returns null if 
 *  S_k is not in queue.
 */
	private boolean changeposTo0(int [] queue, int S_k){
		if(queue.length <= 0 ) return false;
		int S_k_pos;
		
		//FIrst, First find position of S_k in lambda[y]
		for(S_k_pos=0; S_k_pos<queue.length; S_k_pos++){
			if(queue[S_k_pos] == S_k) break;
		}//At this point S_k_pos = position in lambda[y] of S_k
		
		if(S_k_pos == queue.length) {
			return false; //i.e. couldn't find S_k in queue.
		}
		
		for(int j=S_k_pos; j>0; j--){
			queue[j] = queue[j-1];
		}
		queue[0] = S_k;
		return true;
	}
	
    private int [] removeFromQueue(int [] queue, int S_k){
		if(queue.length <= 0 ) return queue;
		int S_k_pos;
		
		//FIrst, First find position of S_k in lambda[y]
		for(S_k_pos=0; S_k_pos<queue.length; S_k_pos++){
			if(queue[S_k_pos] == S_k) break;
		}//At this point S_k_pos = position in lambda[y] of S_k
		
		if(S_k_pos == queue.length) {
			return queue; //i.e. couldn't find S_k in queue.
		}

		for(int j=S_k_pos+1 ; j<queue.length; j++){
			queue[j-1] = queue[j];
		}
		//queue[queue.length-1] = S_k;
		
		int [] aux = new int[queue.length-1];
		System.arraycopy(queue, 0, aux, 0, aux.length);
		queue = aux;


		return queue;
	}
    
    public UGraphSimple getOutputNetwork(){
    	return outputNetwork;
//    	IndependenceStructure output = new IndependenceStructure(n, getOrdering(), getAssignments());
//		return output.getDirectedGraph();
    }
    
    /**
	 * @return the runtime
	 */
	public long getRuntime() {
		return runtime;
	}
	/**
	 * @param runtime the runtime to set
	 */
	public void setRuntime(long runtime) {
		this.runtime = runtime;
	}
	/**
	 * @return the ordering
	 */
	public int[][] getOrdering() {
		return ordering;
	}
	/**
	 * @param ordering the ordering to set
	 */
	public void setOrdering(int[][] sortedNodes) {
		this.ordering = sortedNodes;
	}
	public boolean[][] getAssignments() {
//		return getOutputNetwork().adjMatrix;
		return growAssignments;
	}

}
