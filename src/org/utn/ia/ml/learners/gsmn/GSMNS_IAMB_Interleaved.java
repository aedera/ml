package org.utn.ia.ml.learners.gsmn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import machineLearning.Graphs.GraphicalModelNode;
import machineLearning.Graphs.GraphsSimple.DirectedGraph;
import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.BayesianTest;
import machineLearning.independenceBased.IndependenceTest;
import machineLearning.independenceBased.Triplet;
import machineLearning.independenceBased.TripletSets;
import machineLearning.independenceBased.pFact;

/**
 * <p>
 * Grow Shrink Markov Network with Search method
 * <p>
 * 
 * @author fschluter
 */
public class GSMNS_IAMB_Interleaved {

	/**
	 * 
	 */
	private DirectedGraph outputNetwork;

	/**
	 * Number of variables
	 */
	private int n;

	private int totalNumNodes = 0;
	private IndependenceTest bt;
	private Double logProbability = 0d;
	public long runtime = 0;

	/**
	 * 
	 */
	private double threshold = 0.5;

	private int[][] ordering;

	private int currentBlanket;
	
	private Logger logger;
	
	/**
	 * Grow Shrink Markov Network with Search method
	 */
	public void run(int n, IndependenceTest bt) {
		long beginRuntime = new GregorianCalendar().getTimeInMillis();
		this.bt = bt;
		this.n = n;
		this.outputNetwork = new DirectedGraph(n);
		ordering = new int[n][n];

		initLogger();

		/*
		 * The gss method generates a blanketSet for each variable in the Markov
		 * network
		 */
		for (int i = 0; i < outputNetwork.size(); i++) {
			currentBlanket = i;
			GraphicalModelNode x = new GraphicalModelNode(n, currentBlanket); 
			gss(x);

			for (GraphicalModelNode y : x.getBlanket()) {
				outputNetwork.addEdge(x.index(), y.index());
			}
		}

		runtime = new GregorianCalendar().getTimeInMillis() - beginRuntime;
	}

	/**
	 * Grow Shrink with Search method
	 */
	private void gss(GraphicalModelNode x) {
		/* Grow phase */
		growS(x);

		/* Shrink phase */
//		shrink(x);

	}

	/**
	 * Grow phase in the gss method
	 */
	private void growS(GraphicalModelNode x) {
		List<GraphicalModelNode> solution;
		logger.log(Level.INFO, 		"\n------------------------------------------------------\n" +
									"\tBUILDING BLANKET FOR VARIABLE: " + x + 
									"\n------------------------------------------------------\n" );
		
		solution = gsGreedy(x, true, 0.5, null);
		
		logger.log(Level.INFO, "SOLUTION FOUND: " + solution);
		
		int i=n-1;
		
		x.setBlanket(new Vector<GraphicalModelNode>());
		for (GraphicalModelNode y : solution) {
			/*
			 * In this phase we add to the blanketSet the variables which are
			 * dependent, given a blanketSet
			 */
			if (!y.getTruthValue()) {
				x.getBlanket().add(y);
			}
			
			ordering[currentBlanket][i]=y.index();
			i--;
			
		}
	}

	/**
	 * Shrink phase in the gss method
	 */
	private void shrink(GraphicalModelNode x) {
		Vector<GraphicalModelNode> children = x.getBlanket();

		logger.log(Level.INFO, "\nSHRINK PHASE, variable: " + x + "\n" + 
				"BEFORE SHRINK PHASE - Blanket of " + x + ": " + x.getBlanket());

		for (GraphicalModelNode y : children) {
			/* creating a copy */
			Vector<GraphicalModelNode> s = new Vector<GraphicalModelNode>();
			s.addAll(x.getBlanket());
			/* S - {y} */
			s.remove(y);

			FastSet fastSet = new FastSet(s.size());
			for (GraphicalModelNode node : s) {
				fastSet.add(node.index());
			}

			Triplet triplet = new Triplet(x.index(), y.index(), fastSet);
			pFact test = bt.independent(triplet);
			if (test.truth_value()) { // is independendt?
				/* s <- s - {y} */
				x.setBlanket(s);
			}
		}

		logger.log(Level.INFO, "AFTER SHRINK PHASE - Blanket of " + x + ": " + x.getBlanket() + "\n\n");

	}


	/**
	 * @param currentNode
	 * @param root
	 * @param IT
	 * @param onlyMax
	 * @return
	 */
	private ArrayList<GraphicalModelNode> getSuccessors(GraphicalModelNode currentNode, GraphicalModelNode root, IndependenceTest IT, boolean onlyMax, double threshold) {

		FastSet state = currentNode.getState();
		ArrayList<GraphicalModelNode> successors = new ArrayList<GraphicalModelNode>();

		for (int y = 0; y < n; y++) {
				generateSuccessorsByAssignation(currentNode, root, IT, onlyMax, threshold, state, successors, y);
		}
		
		return successors;
	}

	/**
	 * @param currentNode
	 * @param root
	 * @param IT
	 * @param onlyMax
	 * @param threshold
	 * @param state
	 * @param successors
	 * @param y
	 */
	private void generateSuccessorsByAssignation(GraphicalModelNode currentNode, GraphicalModelNode root, IndependenceTest IT, boolean onlyMax, double threshold, FastSet state, ArrayList<GraphicalModelNode> successors, int successorIndex) {
		if (!state.isMember(successorIndex + n)) {
			double[] logLikelihoods = new double[2];

			FastSet condSet = new FastSet(n);
			for (int j = 0; j < n; j++) {
				if (state.isMember(j))
					condSet.add(j);
			}

			BayesianTest bt = (BayesianTest) IT;
			logLikelihoods = bt.log_likelihoods(new TripletSets(root.attr.index(), successorIndex, condSet));

			GraphicalModelNode successorI = new GraphicalModelNode(n, successorIndex);
			successorI.setTruthValue(true);
			successorI.getState().Union(currentNode.getState());
			successorI.setParent(currentNode);
			successorI.setLocalG((-1) * logLikelihoods[IndependenceTest.I]);
			successorI.setG(currentNode.getG() + successorI.getLocalG());

			GraphicalModelNode successorD = new GraphicalModelNode(n, successorIndex);
			successorD.setTruthValue(false, threshold == 0.5 ? false : bt.independent(logLikelihoods, threshold));
			successorD.getState().Union(currentNode.getState());
			successorD.setParent(currentNode);
			successorD.setLocalG((-1) * logLikelihoods[IndependenceTest.D]);
			successorD.setG(currentNode.getG() + successorD.getLocalG());

			if (!onlyMax) {
				successors.add(successorI);
				successors.add(successorD);
			} else {
				// successors.add((successorI.getLocalG() <
				// successorD.getLocalG())? successorI : successorD);
				if (bt.independent(logLikelihoods)) {
					successors.add(successorI);
				} else {
					successors.add(successorD);
				}
			}
		}
	}

	/**
	 * @param currentNode
	 * @param The
	 *            root of the solution. If you need to use all the tree, root
	 *            should be null.
	 * @return
	 */
	private List<GraphicalModelNode> buildSolution(GraphicalModelNode currentNode, GraphicalModelNode root) {
		List<GraphicalModelNode> solution = new ArrayList<GraphicalModelNode>();

		GraphicalModelNode parent = currentNode;
		do {
			solution.add(parent);
			parent = parent.getParent();
		}while (parent != root); 
		
		return solution;
	}


	private List<GraphicalModelNode> gsGreedy(GraphicalModelNode x, boolean byDependent, double threshold, double[] H) {
		double h = 0;
		GraphicalModelNode currentNode = x;
		
		while (true) {
			currentNode.setChildren(getSuccessors(currentNode, x, bt, true, threshold));

			logger.log(Level.INFO, "\tAll the candidates:"+ currentNode.getChildren());

			/* Break condition */
			if (currentNode.getChildren().size() == 0) {
				break;
			}

			/*
			 * Searching the dependent successor with the minimum localG value
			 */
			double minValue = Double.MAX_VALUE;
			GraphicalModelNode minSuccessor = currentNode.getChildren().get(0);
			for (GraphicalModelNode successor : currentNode.getChildren()) {
				if (successor.getLocalG() < minValue && (!byDependent || !successor.getTruthValue())) {
					minValue = successor.getLocalG();
					minSuccessor = successor;
				}
			}
			h += minSuccessor.getLocalG();
			
			currentNode = minSuccessor;
			
			if (!currentNode.getTruthValue()) {
				x.getBlanket().add(currentNode);
			}

			logger.log(Level.INFO, "\tBetter successor:"+ currentNode + ", parent: " + currentNode.getParent());

			/* Interleaved Shrink phase */
			shrink(x);
		}

		List<GraphicalModelNode> solution = buildSolution(currentNode, x);
		// solution.get(0).setG(h);

		if (H != null)
			H[0] = h;
		
		return solution;
	}

	/**
	 * @return the totalNumNodes
	 */
	public int getTotalNumNodes() {
		return totalNumNodes;
	}

	/**
	 * @param totalNumNodes
	 *            the totalNumNodes to set
	 */
	public void setTotalNumNodes(int totalNumNodes) {
		this.totalNumNodes = totalNumNodes;
	}

	/**
	 * @return the logProbability
	 */
	public Double getLogProbability() {
		return logProbability;
	}

	/**
	 * @param logProbability
	 *            the logProbability to set
	 */
	public void setLogProbability(Double logProbability) {
		this.logProbability = logProbability;
	}

	public DirectedGraph getOutputNetwork() {
		return outputNetwork;
	}

	/**
	 * @return the runtime
	 */
	public long getRuntime() {
		return runtime;
	}

	/**
	 * @param runtime
	 *            the runtime to set
	 */
	public void setRuntime(long runtime) {
		this.runtime = runtime;
	}

	/**
	 * @return the threshold
	 */
	public double getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold
	 *            the threshold to set
	 */
	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}
	
	public int[][] getOrdering() {
		return ordering;
	}

	/**
	 * 
	 */
	private void initLogger() {
		logger = Logger.getLogger("gsmnsLogger");
		logger.setLevel(Level.INFO);
		
	    FileHandler fh;
	    try {

	      // This block configure the logger with handler and formatter
	      fh = new FileHandler("~/gsmnsLogger.log", true);
	      logger.addHandler(fh);
	      SimpleFormatter formatter = new SimpleFormatter();
	      fh.setFormatter(formatter);

	    } catch (SecurityException e) {
//	      e.printStackTrace();
	    } catch (IOException e) {
//	      e.printStackTrace();
	    }
	}	
}
