/**
 * 
 */
package org.utn.ia.ml.learners.ibmaphc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.BayesianTest;
import machineLearning.independenceBased.IndependenceTest;
import machineLearning.independenceBased.TripletSets;

/**
 * @author fschluter
 * 
 */
public class HCState implements Comparable<HCState> {
	public BayesianTest bt;
	public UGraphSimple structure;
	public UGraphSimple oldStructure;
	public double score;
	public int n;
	public int weightedTestsCost = 0;
	public int tests;
	public static double threshold = 0.5;
	public static Logger logger;
	public List<TripletSets> closure = new ArrayList<TripletSets>();

	/**
	 * @param structure
	 * @param useTestsCache
	 * @param score
	 */
	public HCState(UGraphSimple structure,
			UGraphSimple oldStructure, 
			BayesianTest bt) {
		super();
		this.structure = structure;
		this.n = structure.getVariables().size();
		structure.structureScores = new double[n][n];
		this.bt = bt;
		this.oldStructure = oldStructure;

		if (oldStructure == null) {
			computeScore();
		} else {
			structure.blanketScores = new double[oldStructure.blanketScores.length];
			System.arraycopy(oldStructure.blanketScores, 0, structure.blanketScores, 0, oldStructure.blanketScores.length);  
			recomputeScore();
		}
	}

	private void computeScore() {
		double score = 0;
		for (Integer X : structure.getVariables()) {
			double blanketScore = 0;
			FastSet z = new FastSet(n);
			for (Integer Y : structure.getVariables()) {
				if (structure.existEdge(X, Y)) {
					z.add(Y);
				}
				FastSet s = new FastSet(z);
				s.remove(Y);
			}

			List<Integer> restOfUniverse = structure.getVariables();
			restOfUniverse.remove(X);
			for (Integer Y : restOfUniverse) {
				FastSet s = new FastSet(z);
				boolean exists = s.isMember(Y);
				s.remove(Y);
				TripletSets triplet = new TripletSets(X, Y, s);
				closure.add(triplet);				
				
				double[] logLikelihoods = bt
						.log_likelihoods(triplet, threshold);
				
				double[] scores;
				scores = bt.logProbs(logLikelihoods);
				tests++;

				double lastTestScore;
				lastTestScore = scores[exists ? IndependenceTest.D
						: IndependenceTest.I];
				blanketScore += lastTestScore;
				structure.structureScores[X][Y] = lastTestScore;

				weightedTestsCost += 2 + s.cardinality();

				bt.tripletsCache.put(triplet, scores);
			}
			
			structure.blanketScores[X] = blanketScore;
			score += structure.blanketScores[X];
		}

		structure.score = score;
		this.score = score;
	}
	

	private void recomputeScore() {
		structure.structureScores = new double[n][n];
		double score = 0;
		List<Integer> flippedVariables = getFlippedVariables();
		for (Integer X: structure.getVariables()) {
			if (flippedVariables.contains(X)) {
				recomputeBlanketScore(bt, X);
			} else {
				System
						.arraycopy(oldStructure.structureScores[X], 0,
								structure.structureScores[X], 0, n);
			}
			score += structure.blanketScores[X];
		}
		structure.score = score;
		this.score = score;
	}

	private void recomputeBlanketScore(BayesianTest bt, Integer X) {
		/* recalculating the score */
		double blanketScore = 0;
		FastSet z = new FastSet(n);

		for (Integer Y : structure.getVariables()) {
			if (structure.existEdge(X, Y)) {
				z.add(Y);
			}
			FastSet s = new FastSet(z);
			s.remove(Y);
		}
		
		List<Integer> restOfUniverse = structure.getVariables();
		restOfUniverse.remove(X);
		for (Integer Y : restOfUniverse) {
			FastSet s = new FastSet(z);
			boolean exists = s.isMember(Y);
			s.remove(Y);

			FastSet s2 = new FastSet(z);
			s2.remove(Y);

			TripletSets triplet = new TripletSets(X, Y, s);
			this.closure.add(triplet);
			double lastTestScore = 0;

			double[] scores = performTest(bt, triplet);
			lastTestScore = scores[exists ? IndependenceTest.D
					: IndependenceTest.I];
			structure.structureScores[X][Y] = lastTestScore;
			blanketScore += lastTestScore;
		}

		structure.blanketScores[X] = blanketScore;
		
	}

	private double[] performTest(BayesianTest bt, TripletSets triplet) {

		double[] logLikelihoods = bt.log_likelihoods(triplet);
		if (logLikelihoods[IndependenceTest.D] != logLikelihoods[IndependenceTest.D]
				|| logLikelihoods[IndependenceTest.I] != logLikelihoods[IndependenceTest.I]) {
			System.out.println("NaN logLikelihoods: "
					+ Arrays.toString(logLikelihoods));
		}
		double[] scores;
		scores = bt.logProbs(logLikelihoods);
		tests++;

		weightedTestsCost += 2 + triplet.FastZ.cardinality();

		return scores;
	}

	public List<Integer> getFlippedVariables() {
		List<Integer> flippedVariables = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if ((structure.adjMatrix[i][j] && oldStructure.adjMatrix[i][j])
						|| (!structure.adjMatrix[i][j] && !oldStructure.adjMatrix[i][j])) {
					;
				} else {
					flippedVariables.add(i);
					flippedVariables.add(j);
				}
			}
		}
		return flippedVariables;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(HCState state) {
		assert (state.score >= 0 && this.score >= 0);

		final int BEFORE = -1;
		final int EQUAL = 0;
		final int AFTER = 1;

		if (isBetterThan(state)) {
			return BEFORE;
		} else if (!isBetterThan(state)) {
			return AFTER;
		} else {
			if (this == state) {
				return EQUAL;
			} else
				return BEFORE;
		}
	}

	public boolean isBetterThan(HCState state) {
		return score < state.score;
	}

	@Override
	public String toString() {
		return "{" + structure.toString() + ", score: " + score + "}";
	}

}
