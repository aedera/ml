package org.utn.ia.ml.learners.ibmaphc;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class OptimizerAlgorithm {
	protected Logger logger;
	protected long runtime = 0;
	protected Level logLevel;
	private long beginRuntime;
	
	protected void initLogger() {
		logger = Logger.getLogger("gsmnsLogger");
		logger.setLevel(logLevel);

		FileHandler fh;
		try {
			fh = new FileHandler("./logs/gsmnsLogger.log", true);
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);

		} catch (SecurityException e) {
//			e.printStackTrace();
		} catch (IOException e) {
//			e.printStackTrace();
		}
	}	

	protected void initRuntime() {
		beginRuntime = new GregorianCalendar().getTimeInMillis();
	}
	
	protected void endRuntime() {
		runtime = new GregorianCalendar().getTimeInMillis() - beginRuntime;
	}
	
	public void log(String msg){
		if (logger.getLevel() != Level.OFF)
			logger.log(Level.INFO, msg);
	}

	public long getRuntime() {
		return runtime;
	}

	public void setRuntime(long runtime) {
		this.runtime = runtime;
	}
}
