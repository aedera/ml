package org.utn.ia.ml.learners.ibmaphc;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.GraphsSimple.DirectedGraph;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.BayesianTest;

import org.utn.ia.math.Combinatorics;

/**
 * @author fschluter
 * 
 */
public class IBMAPHC extends OptimizerAlgorithm {

	public int n;
	public BayesianTest bt;
	public HCState currentState;
	public UGraphSimple outputNetwork;

	public FastSet[] adjMatrix;

	public int testsCounts = 0;
	public int weightedTestsCost;
	public int climbs;
	public MarkovNet trueNet;
	public boolean incrementalScoreComputing;
	public int neighborsStrategy = NEIGHBORS_STRATEGY_ALL_NEIGHBORS;
	public static int NEIGHBORS_STRATEGY_ALL_NEIGHBORS = 0;
	public static int NEIGHBORS_STRATEGY_HEURISTICS = 1;

	public int maxClimbs = Integer.MAX_VALUE;
	public int MN = Integer.MAX_VALUE;
	public static double threshold = 0.5;

	public IBMAPHC(Level level, MarkovNet trueNet) {
		super();
		this.logLevel = level;
		this.trueNet = trueNet;
		initLogger();
		HCState.logger = logger;
	}

	public void run(int n, BayesianTest bt, 
			boolean[][] adjMatrix) {
		this.n = n;
		this.bt = bt;
		this.outputNetwork = new DirectedGraph(n);
		this.adjMatrix = new FastSet[adjMatrix.length];
		for (int i = 0; i < adjMatrix.length; i++) {
			this.adjMatrix[i] = new FastSet(adjMatrix[i]);
		}

		log("True network: " + trueNet + ", MN="+MN);

		initRuntime();

		UGraphSimple initialStructure = new UGraphSimple(n, adjMatrix); 

		currentState = new HCState(initialStructure, null, bt);
		
		log("Initial structure" + initialStructure + " - Score(initialStructure)=" + currentState.score);
		
		currentState = hillClimbing(currentState);

		log("Finished: \n     better state: "
				+ currentState.structure.toString() + ", score: "
				+ currentState.score);

		outputNetwork = currentState.structure;
		endRuntime();
	}

	/**
	 * @param state
	 * @return
	 */
	protected HCState hillClimbing(HCState state) {
		climbs = 0;
		HCState bestState = state;
		while (true) {
			HCState betterNeighborState = getBetterNeighborState(bestState);
			if (betterNeighborState == null
					|| betterNeighborState.score <= bestState.score) {
				log("####Local maximum found in state [" + climbs + "]: "
						+ bestState.score);
				return bestState;
			} else {
				bestState = betterNeighborState;
				if (climbs > maxClimbs) {
					return bestState;
				}
				climbs++;
				log("####Climbing structure [" + climbs + "]: "
							+ betterNeighborState.structure);
				log("####Climbing score[" + climbs + "]: "
						+ betterNeighborState.score);

			}
		}
	}

	protected HCState getBetterNeighborState(HCState state) {
		HCState bestNeighbor = null;
		UGraphSimple oldStructure = state.structure;
		List<FastSet> neighborsFastsets = new ArrayList<FastSet>();
		
		int localtestcounts = 0;

		if (neighborsStrategy == NEIGHBORS_STRATEGY_ALL_NEIGHBORS) {
			log("Neighbors Strategy: ALL NEIGHBORS");

			neighborsFastsets.addAll(Combinatorics.getAllCombinatorics(
					oldStructure.independenceAssignments, 1, n, MN));
			neighborsFastsets = new ArrayList<FastSet>(neighborsFastsets);

		} else if (neighborsStrategy == NEIGHBORS_STRATEGY_HEURISTICS) {
			log("Neighbors Strategy: BETTER NEIGHBORS");

			neighborsFastsets = new ArrayList<FastSet>();
			neighborsFastsets.add(getHeuristicBetterNeighbor(state));

		} else {
			throw new RuntimeException("wrong neighbors expansion strategy!");
		}

		for (FastSet fastSet : neighborsFastsets) {

			UGraphSimple newStructure = new UGraphSimple(fastSet,n);
			HCState newState = null;

			if (incrementalScoreComputing) {
				newState = new HCState(newStructure, oldStructure, bt);
			} else {
				newState = new HCState(newStructure, null, bt);
			}

			testsCounts += n - 1;
			weightedTestsCost += newState.weightedTestsCost;
			localtestcounts += newState.tests;

			if (bestNeighbor == null
					|| newState.score > bestNeighbor.score) {
				bestNeighbor = newState;
			}
		}

		return bestNeighbor;

	}

	private FastSet getHeuristicBetterNeighbor(HCState state) {
		int bestX = 0, bestY = 0;
		double minProduct = Integer.MAX_VALUE;
		UGraphSimple u = new UGraphSimple(state.structure);
		for (int i = 0; i < state.n - 1; ++i) {
			for (int j = i + 1; j < state.n; ++j) {
				u.flipEdge(i, j);
				if(u.getMaximumSizeOfBlanket()<=MN){
					double product = Math
							.exp(state.structure.structureScores[i][j])
							* Math.exp(state.structure.structureScores[j][i]);
					if (product < minProduct) {
						minProduct = product;
						bestX = i;
						bestY = j;
					}
				}
				u.flipEdge(i, j);
			}
		}
		u.flipEdge(bestX, bestY);
		return u.getFastSet();
	}



}
