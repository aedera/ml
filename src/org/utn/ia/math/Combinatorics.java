/**
 * 
 */
package org.utn.ia.math;

import java.util.ArrayList;
import java.util.List;

import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;

/**
 * @author fschluter
 * 
 */
public class Combinatorics {

	public static List<FastSet> getAllCombinatorics(FastSet bitset, int distance) {
		FastSet fs = new FastSet(bitset);
		fs.setAlltoOne();
		List<FastSet> fss = fs.subsets(distance);
		List<FastSet> newSets = new ArrayList<FastSet>();

		for (FastSet fsi : fss) {
			FastSet newSet = new FastSet(bitset);
			newSet.Xor(fsi);
			newSets.add(newSet);
		}

		return newSets;
	}

	/**
	 * @param bitset
	 * @param distance
	 * @param MN Maximum neighbors. Only returning
	 * @return
	 */
	public static List<FastSet> getAllCombinatorics(FastSet bitset, int distance, int n, int MN) {
		FastSet fs = new FastSet(bitset);
		fs.setAlltoOne();
		List<FastSet> fss = fs.subsets(distance);
		List<FastSet> newSets = new ArrayList<FastSet>();
		
		for (FastSet fsi : fss) {
			FastSet newSet = new FastSet(bitset);
			newSet.Xor(fsi);
			UGraphSimple u = new UGraphSimple(newSet, n);
			int maximumSizeOfBlanket = u.getMaximumSizeOfBlanket();
			if(maximumSizeOfBlanket <= MN){
				newSets.add(newSet);
			}
		}
		
		return newSets;
	}
	

}
