/**
 * 
 */
package org.utn.ia.math;

/**
 * @author fschluter
 * 
 */
public class Factorial {
	public static int calculate(int integer) {
		if (integer == 0) {
			return 0;
		}else if (integer == 1) {
			return 1;
		} else if(integer > 1){
			return (integer * (calculate(integer - 1)));
		} else {
			throw new RuntimeException("Negative parameter is invalid");
		}
	}
}
