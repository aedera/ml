package machineLearning;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import machineLearning.Utils.Utils;

/*
 * Created on May 11, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author Facundo Bromberg
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class main {
    GSIMN.experiment auxexp;

    public static String test(String [] args){
        String aux = args[0];
        int i=0;
        while(aux.equals("main")) i++;
        String ALG = args[i];
        //System.out.println(ALG);
        int N = (new Integer(args[i+1])).intValue();
        double tau = (new Double(args[i+2])).doubleValue();
        int randSeed = (new Integer(args[i+3])).intValue();
        //System.out.println(ALG + "  " + N + "  " +tau);

        GSIMN alg = new GSIMN(N,tau, new Random(randSeed));

        forwardChainingSimple fch = null;
        int memory=0;

        if(ALG.equals("GSMN")) {
            //System.out.println("REAL:"+ alg.realMN);
            alg.GSMBAlg(false);
            //System.out.println("EDGE SIMILARITY: gsmn " + alg.G.edgeSimilarity(alg.realMN));


            //ch = alg.fchGSMB;
        }
        if(ALG.equals("GSIMN")) {
            //System.out.println(ALG);
            alg.GSIMBPolishedAlg(false);
            //memory = alg.fchGSIMB.memoryComplexity();
            //System.out.println("\nEDGE SIMILARITY: gsimn " + alg.G.edgeSimilarity(alg.realMN));
            if(false){ //check similarity
                System.out.println("EDGE SIMILARITY: gsimn " + alg.G.edgeSimilarity(alg.realMN));
                if(alg.G.edgeSimilarity(alg.realMN) < 1.0){
                    System.out.println("REAL:"+ alg.realMN);
                    System.out.println("OUTPUT:"+ alg.G);
                    GSIMN.ScreenOutput = true;
                    alg.GSIMBPolishedAlg(false);
                    GSIMN.ScreenOutput = true;
                }
            }
        }
        if(ALG.equals("GSMN_FCH")) {
            //System.out.println("\n"+ ALG + "    GOT HERE"+"\n");
            alg.GSMBAlg(true, randSeed);
            memory = alg.fch.memoryComplexity();
        }
        if(ALG.equals("GSIMN_FCH")) {
            //System.out.println("\n"+ ALG + "    GOT HERE"+"\n");
            alg.GSIMBPolishedAlg(true);
            memory = alg.fch.memoryComplexity();
        }
        if(ALG.equals("PC")) alg.PCAlg();
        if(ALG.equals("PCI")) alg.PCInferenceAlg();

        String out = alg.testsCost + " " + alg.testsWeightedCost + " " + memory + " -1 -1";
        //System.out.println(out);
        return out;

    }
    public static void main(String[] args) {
        test(args);
    }
}
