package machineLearning;
/*
 * 
 * Created on Apr 18, 2005
 *
 */
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import machineLearning.Datasets.*;
import machineLearning.Utils.*;
import machineLearning.Graphs.*;
/**
 * @author Facundo Bromberg
 *
 */

public class forwardChainingSimpleMN extends forwardChainingSimple{
	
	forwardChainingSimpleMN(Set V, int numVars){
		super(V, numVars);
	}
	
	
	protected  void putInQueue(FactSimple f){
		FastSet VmA = new FastSet(FactSimple.convertToFastSet(V, V.size()));
		VmA.remove(f.X());
		VmA.remove(f.Y());
		
		if(f.axiom().equals("MERD") || f.axiom().equals("DATA")){
			int k=0;
		}
		//factQueue.offer(f);
		//factQueueH.put(f.toString(), f);
		
		Vector SS = null;
		
		if(!f.I()) SS = f.getCond().getAllSubsets(FactSimple.convertToFastSet(V, V.size()));
		if(f.I()) SS = f.getCond().getAllSupersets(VmA);
		
		int i;
		for(i=0; i<SS.size(); i++){
			FactSimple f2 = new FactSimple(f.I(), f.X(), f.Y(), (FastSet)SS.elementAt(i));
			
			if(FastSet.isEqual(f.getCond(), (FastSet)SS.elementAt(i))){
				f2.antecedents(f.antecedentOne(), f.antecedentTwo(), null);
				f2.axiom(f.axiom());
				if(f.antecedentOne()!= null && f.antecedentTwo()!= null){
					if(f.antecedentOne().axiom().equals("DATA") || f.antecedentTwo().axiom().equals("DATA")){
						int k=0;
					}
				}


			}
			else {
				f2.antecedents(f,null, null);
				f2.axiom("SU");
			}
			if((f2.I() && !Ikb.contains(f2)) || (!f2.I() && !Dkb.contains(f2))){
				
				factQueue.addLast(f2);
				factQueueH.put(f2.toString(), f2);
			}
			
		}
		//System.out.println((FastSet)SS.elementAt(i-1));
		//System.out.println(f + "  (toghether with its subsets/supersets)added to queue.");
	}
	
	
	
	//Ignores f3, used in forwardChaniningSimpleBN
	void feedForwardStep(FactSimple f1, FactSimple f2, FactSimple f3){
		FactSimple [] newFV;
		FactSimple newF;
		if((newFV = areMatchI1(f1, f2)) != null){
			this.inference("I1", f1, f2, newFV[0]);
			this.inference("I1", f1, f2, newFV[1]);
		}
		if((newFV = areMatchI2a(f1, f2)) != null){
			this.inference("I2a", f1, f2, newFV[0]);
			this.inference("I2a", f1, f2, newFV[1]);
		}
		
		if((newF = areMatchI2b(f1, f2)) != null){
			this.inference("I2b", f1, f2, newF);
		}
		if((newF = areMatchT2(f1, f2)) != null){
			this.inference("T2", f1, f2, newF);
		}
		if((newF = areMatchT3(f1, f2)) != null){
			this.inference("T3", f1, f2, newF);
		}
	}
	

	/**
	 * 
	 */
	private FactSimple [] areMatchI1NS(FactSimple f1, FactSimple f2){
		boolean discovered = false;
		int l1 = f1.X();
		int r1 = f1.Y();
		FastSet Z1 = f1.getCond();
		
		int l2 = f2.X();
		int r2 = f2.Y();
		FastSet Z2 = f2.getCond();
		
		FactSimple [] newF = null;
		if(f1.I() && f2.I() && Z1.isMember(r2)) {
			if(l1 == l2 &&  Z2.isMember(r1)){
				FastSet Z1aux = new FastSet(Z1); 
				FastSet Z2aux = new FastSet(Z2); 
				if(FastSet.isEqual(Z1aux.remove(r2), Z2aux.remove(r1)) ){
					if(!discovered) discovered = true; 
					else{
						System.out.println("\n\nERROR: f1 and f2 matched twice. ");
						Exception e = new Exception();
						e.printStackTrace();
						System.exit(0);
					}

					newF = new FactSimple[2];
					newF[0] = new FactSimple(true, l1, r1, Z1aux);
					newF[1] = new FactSimple(true, l1, f2.Y(), Z1aux);
				}
			}
		}
		return newF;

	}
	private FactSimple [] areMatchI1(FactSimple f1, FactSimple f2){
		FactSimple [] newF;
		if((newF = areMatchI1NS(f1, f2)) != null) return newF;
		if((newF = areMatchI1NS(f1.symmetric(), f2)) != null) return newF;
		if((newF = areMatchI1NS(f1, f2.symmetric())) != null) return newF;
		if((newF = areMatchI1NS(f1.symmetric(), f2.symmetric())) != null) return newF;
		
		return null;
	}
	/**
	 * Works only when the first fact is the independent, and second is the dependent
	 */
	private FactSimple [] areMatchI2aNS(final FactSimple f1, final FactSimple f2){
		boolean val1 = f1.I();
		int l1 = f1.X();
		int r1 = f1.Y();
		FastSet Z1 = f1.getCond();
		
		boolean val2 = f2.I();
		int l2 = f2.X();
		int r2 = f2.Y();
		FastSet Z2 = f2.getCond();
		
		FactSimple [] newF = null;
		if(val1 && !val2) {
			if(l1 == l2 &&  r1 == r2 && FastSet.isSubSet(Z2, Z1)){
				if(f1.toString().equals("I(0, {2,3,4,6}, 1).")){
					int k=0;
				}
				FastSet diff = FastSet.Minus(Z1, Z2);
				int w;
				if(diff.cardinality() == 1) {
					w = diff.getOn(0);
				}
				else return null;
					
				FastSet aux = new FastSet(Z2);
				newF = new FactSimple[2];
				aux.add(r1);
				newF[0] = new FactSimple(false, l1, w, aux);
				FastSet aux2 = new FastSet(Z2);
				aux2.add(l1);
				newF[1] = new FactSimple(false, r1, w, aux2);
			}	
		}
		return newF;
	}
	private FactSimple [] areMatchI2a(final FactSimple f1, final FactSimple f2){
		
		FactSimple [] newF;
		FactSimple I  = f1, D = f2;
		if(f2.I() && !f1.I()){
			I = f2; D = f1;
		}
		if((newF = areMatchI2aNS(I, D)) != null) return newF;
		if((newF = areMatchI2aNS(I.symmetric(), D)) != null) return newF;
		if((newF = areMatchI2aNS(I, D.symmetric())) != null) return newF;
		if((newF = areMatchI2aNS(I.symmetric(), D.symmetric())) != null) return newF;
		
		return null;
	}

	/**
	 * 
	 */
	private FactSimple areMatchI2b(FactSimple f1, FactSimple f2){
		FactSimple newF;
		FactSimple I  = f1, D = f2;
		if(f2.I() && !f1.I()){
			I = f2; D = f1;
		}
		if((newF = areMatchI2bNS(I, D)) != null) return newF;
		if((newF = areMatchI2bNS(I.symmetric(), D)) != null) return newF;
		if((newF = areMatchI2bNS(I, D.symmetric())) != null) return newF;
		if((newF = areMatchI2bNS(I.symmetric(), D.symmetric())) != null) return newF;
		
		return null;
	}
	private FactSimple areMatchI2bNS(FactSimple f1, FactSimple f2){
		boolean val1 = f1.I();
		int l1 = f1.X();
		int r1 = f1.Y();
		FastSet Z1 = f1.getCond();
		
		boolean val2 = f2.I();
		int l2 = f2.X();
		int r2 = f2.Y();
		FastSet Z2 = f2.getCond();
		
		FactSimple newF = null;
		
		if(val1 && !val2) {
			if(l1 == l2 && FastSet.isSubSet(Z2, Z1)){
				FastSet diff = FastSet.Minus(Z1, Z2);
				int w;
				if(diff.cardinality() == 1) { 
					w = diff.getOn(0);
					if(w == r2){
						FastSet aux = new FastSet(Z2); 
						aux.add(r1); 
						newF = new FactSimple(false, l1, w, aux);
					}
				}
			}
		}
		return newF;
}

	/**
	 * 
	 */
	private FactSimple areMatchT2(FactSimple f1, FactSimple f2){
		FactSimple newF;
		FactSimple I  = f1, D = f2;
		if(f2.I() && !f1.I()){
			I = f2; D = f1;
		}
		if((newF = areMatchT2NS(I, D)) != null) return newF;
		if((newF = areMatchT2NS(I.symmetric(), D)) != null) return newF;
		if((newF = areMatchT2NS(I, D.symmetric())) != null) return newF;
		if((newF = areMatchT2NS(I.symmetric(), D.symmetric())) != null) return newF;
		
		return null;
	}
	private FactSimple areMatchT2NS(FactSimple f1, FactSimple f2){
		int l1 = f1.X();
		int r1 = f1.Y();
		
		int l2 = f2.X();
		int r2 = f2.Y();
		
		FactSimple newF = null;
		if(f1.I() && !f2.I()) {
			if(l1 == l2 && r1 != r2 && FastSet.isEqual(f1.getCond(), f2.getCond())){
				newF = new FactSimple(true, r1, r2, f1.getCond());
			}
		}
		return newF;
	}

	/**
	 * 
	 */
	private FactSimple areMatchT3(FactSimple f1, FactSimple f2){
		FactSimple newF;
		if((newF = areMatchT3NS(f1, f2)) != null) return newF;
		if((newF = areMatchT3NS(f1.symmetric(), f2)) != null) return newF;
		if((newF = areMatchT3NS(f1, f2.symmetric())) != null) return newF;
		if((newF = areMatchT3NS(f1.symmetric(), f2.symmetric())) != null) return newF;
		
		return null;
	}
	private FactSimple areMatchT3NS(FactSimple f1, FactSimple f2){
		int l1 = f1.X();
		int r1 = f1.Y();
		
		int l2 = f2.X();
		int r2 = f2.Y();
		
		FactSimple newF = null;
		if(!f1.I() && !f2.I()) {
			if(l2 == r1 && r2 != l1 && FastSet.isEqual(f1.getCond(), f2.getCond())){
				newF = new FactSimple(false, l1, r2, f1.getCond());
			}
		}
		return newF;
	}
	
	
	public static void main(String[] args) {
	}
}


