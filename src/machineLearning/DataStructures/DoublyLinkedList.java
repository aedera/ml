// Implementation of lists, using doubly linked elements.
// (c) 1998 McGraw-Hill
package machineLearning.DataStructures;

import machineLearning.Utils.*;

public final class DoublyLinkedList //implements List
{

    //protected int count;
    public DoublyLinkedListElement head;
    protected DoublyLinkedListElement tail;

    public DoublyLinkedList()
    // post: constructs an empty list
    {
        head = null;
        tail = null;
        //count = 0;
    }

    public void add(int index, Object value)
    // post: adds value to beginning of list.
    {
        addToHead(index, value);
    }
    
    public void addToHead(int index, Object value)
    // pre: value is not null
    // post: adds element to head of list
    {
        // construct a new element, making it the head
        head = new DoublyLinkedListElement(index, value, head, null);
        // fix tail, if necessary
        if (tail == null) tail = head;
        //count++;
    }

    public Object removeFromHead()
    // pre: list is not empty
    // post: removes first value from list
    {
        if(Utils._ASSERT) Utils.ASSERT(!isEmpty(),"List is not empty.");
        DoublyLinkedListElement temp = head;
        head = head.next;
        if (head != null) {
            head.previous = null;
        } else {
            tail = null; // remove final value
        }
        temp.next = null;// helps clean things up; temp is free
        //count--;
        return temp.data;
    }

    public final DoublyLinkedListElement addToTail(int index, Object value)
    // pre: value is not null
    // post: adds new value to tail of list
    {
        // construct new element
        tail = new DoublyLinkedListElement(index, value, null, tail);
        // fix up head
        if (head == null) head = tail;
        //count++;
        
        return tail;
    }

    public Object removeFromTail()
    // pre: list is not empty
    // post: removes value from tail of list
    {
    	if(Utils._ASSERT) Utils.ASSERT(!isEmpty(),"List is not empty.");
        DoublyLinkedListElement temp = tail;
        tail = tail.previous;
        if (tail == null) {
            head = null;
        } else {
            tail.next = null;
        }
        //count--;
        return temp.value();
    }

    /**
     * Removes the sublist start-end (inclusive).
     */
    public void cut(DoublyLinkedListElement start, DoublyLinkedListElement end){
    	if(start != head) start.previous.next = end.next;
    	else head = end.next;
    	if(end != tail) end.next.previous = start.previous;
    	else tail = start.previous;
    }
	
    public Object peek()
    // pre: list is not empty
    // post: returns first value in list.
    {
        return head.value();
    }

    public Object tailPeek()
    // pre: list is not empty
    // post: returns last value in list.
    {
        return tail.value();
    }

    public boolean contains(Object value)
    // pre: value not null
    // post: returns true iff value is in the list
    {
        DoublyLinkedListElement finger = head;
        while ((finger != null) && (!finger.value().equals(value)))
        {
            finger = finger.next;
        }
        return finger != null;
    }

    public final Object remove(Object value)
    // pre: value is not null.  List can be empty.
    // post: first element matching value is removed from list
    {
        DoublyLinkedListElement finger = head;
        while (finger != null &&
               !finger.value().equals(value))
        {
            finger = finger.next;
        }
        if (finger != null)
        {
            // fix next field of element above
            if (finger.previous != null)
            {
                finger.previous.next = finger.next;
            } else {
                head = finger.next;
            }
            // fix previous field of element below
            if (finger.next != null)
            {
                finger.next.previous = finger.previous;
            } else {
                tail = finger.previous;
            }
            //count--;            // fewer elements
            return finger.value();
        }
        return null;
    }
    public Object remove(DoublyLinkedListElement finger)
    // pre: value is not null.  List can be empty.
    // post: first element matching value is removed from list
    {
    	 if (finger != null)
         {
             // fix next field of element above
             if (finger.previous != null)
             {
                 finger.previous.next = finger.next;
             } else {
                 head = finger.next;
             }
             // fix previous field of element below
             if (finger.next != null)
             {
                 finger.next.previous = finger.previous;
             } else {
                 tail = finger.previous;
             }
             //count--;            // fewer elements
             return finger.data;
         }
        return null;
    }

/*    public int size()
    // post: returns the number of elements in list
    {
        return count;
    }
*/
    public boolean isEmpty()
    // post: returns true iff the list has no elements.
    {
        //return size() == 0;
    	return (head == null && head == tail);
    }

    public void clear()
    // post: removes all the elements from the list
    {
        head = tail = null;
        //count = 0;
    }

    public Iterator elements()
    // post: returns iterator that allows the traversal of list.
    {
        return new Iterator(head);
    }

    public String toString()
    // post: returns a string representing list
    {
        StringBuffer s = new StringBuffer();
        s.append("<DoublyLinkedList:");
        Iterator li = elements();
        while (li.hasMoreElements())
        {
            s.append(" "+li.nextElement());
        }
        s.append(">");
        return s.toString();
    }
}
/*

class DoublyLinkedListIterator implements Iterator
{
    private DoublyLinkedListElement auxElement;

    protected DoublyLinkedListElement head;
    public DoublyLinkedListElement current;

    public DoublyLinkedListIterator(DoublyLinkedListElement h)
    // post: constructs an iterator rooted at list head, h
    {
        head = h;
        reset();
    }

    public void reset()
    // post: resets iterator to list head
    {
        current = head;
    }

    final public boolean hasMoreElements()
    // post: returns true iff current element is valid
    {
        return current != null;
    }

    final public DoublyLinkedListElement nextElement()
    // post: returns current element and increments iterator
    {
        auxElement = current;
        current = current.next;
        return auxElement;
    }

    public Object value()
    // pre: hasMoreElements
    // post: returns current element
    {
        return current.value();
    }
}
*/