package machineLearning.DataStructures;

import java.util.Collection;

import machineLearning.Graphs.GraphBase;
import machineLearning.Graphs.Factor;

public class Model {
    final private GraphBase structure_;
    final private Factor factors_;

    public Model(GraphBase structure, Factor factors) {
        structure_ = structure;
        factors_ = factors;
    }

    public GraphBase getStructure() {
        return structure_;
    }

    public Factor getFactors() {
        return factors_;
    }
}