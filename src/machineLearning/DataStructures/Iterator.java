// Abstract iteration class.  Like enumerable, but values are ordered.
// (c) 1998 McGraw-Hill

package machineLearning.DataStructures;
import java.util.Enumeration;

public class Iterator 
{
    private DoublyLinkedListElement auxElement;

    protected DoublyLinkedListElement head;
    public DoublyLinkedListElement current;

    public Iterator(DoublyLinkedListElement h)
    // post: constructs an iterator rooted at list head, h
    {
        head = h;
        reset();
    }

    public void reset()
    // post: resets iterator to list head
    {
        current = head;
    }

    final public boolean hasMoreElements()
    // post: returns true iff current element is valid
    {
        return current != null;
    }

    final public DoublyLinkedListElement nextElement()
    // post: returns current element and increments iterator
    {
        auxElement = current;
        current = current.next;
        return auxElement;
    }

    public Object value()
    // pre: hasMoreElements
    // post: returns current element
    {
        return current.value();
    }
}

