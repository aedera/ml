// Elements used in implementation of doubly linked lists.
// (c) 1998 McGraw-Hill
package machineLearning.DataStructures;

public final class DoublyLinkedListElement
{
	public int index;
    public Object data;
    public DoublyLinkedListElement next;
    protected DoublyLinkedListElement previous;

    public DoublyLinkedListElement(int index, Object v,
                            DoublyLinkedListElement next,
                            DoublyLinkedListElement previous)
    // post: constructs new element with list
    //       prefix referenced by previous and
    //       suffix referenced by next
    {
    	this.index = index;
        data = v;
        this.next = next;
        if (next != null)
        	next.previous = this;
        this.previous = previous;
        if (previous != null)
        	previous.next = this;
    }

    public DoublyLinkedListElement(Object v)
    // post: constructs a single element
    {
        this(-1, v,null,null);
    }
    public DoublyLinkedListElement(int index)
    // post: constructs a single element
    {
        this(index, null,null,null);
    }

    /*public DoublyLinkedListElement next()
    // post: returns the element that follows this
    {
        return nextElement;
    }

    public DoublyLinkedListElement previous()
    // post: returns element that precedes this
    {
        return previousElement;
    }
*/
    public Object value()
    // post: returns value stored here
    {
        return data;
    }

   /* public void setNext(DoublyLinkedListElement next)
    // post: sets value associated with this element
    {
        nextElement = next;
    }

    public void setPrevious(DoublyLinkedListElement previous)
    // post: establishes a new reference to a previous value
    {
        previousElement = previous;
    }
*/
    public void setValue(Object value)
    // post: sets a new value for this object
    {
        data = value;
    }

    public boolean equals(Object other)
    // post: returns true if this object and other are equal
    {
        DoublyLinkedListElement that = (DoublyLinkedListElement)other;
        if (that == null) return false;
        if (that.value() == null || value() == null)
        {
            return value() == that.value();
        } else {
            return value().equals(that.value());
        }
    }

    public int hashCode()
    // post: generates hash code for element
    {
        if (value() == null) return super.hashCode();
        else return value().hashCode();
    }

    public String toString()
    // post: returns string representation of element
    {
        return "<DoublyLinkedListElement: "+value()+">";
    }
}       

