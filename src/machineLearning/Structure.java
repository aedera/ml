package machineLearning;
import machineLearning.independenceBased.*;
import machineLearning.Utils.*;
import machineLearning.Datasets.IndependenceTestMargaritisUAI2001;
import machineLearning.Graphs.GraphsSimple.*;
/*
 * Created on Jan 29, 2006
 *
 */

/**
 * @author bromberg
 *
 */
public class Structure extends UGraphSimple{
	particleClasses.ClassInfo classInfo;
	structuresParticlesFilter s;
	int numParticles = 0;
	
	public Structure(structuresParticlesFilter s, UGraphSimple x){
		super(x);
		this.s = s;
		
	}
	public Structure(Structure np){
		//super(np);
		super(np.n);

		this.s = np.s;
		
		for(int i=0; i<np.n; i++){
			for(int j=i+1; j<n; j++){
				if(np.existEdge(i,j)) addEdge(i,j);
			}
		}
		//}	
	}
		
	public final void addParticle(){numParticles++;};
	public final void removeParticle(){numParticles--;};
	
	//public void normalizedPosterior(double np){normalizedPosterior = np;};
	public final double normalizedPosterior(){
//		return classInfo.particleProbability();
		return 0d;
	};

	public final void copyContentFrom(Structure spstar){
		super.copyContentFrom(spstar);
    }
	
	final public boolean separates(int x, int y, int test){
		return super.separates_nonRecursive2(x, y, s.tests[test].getCond());
	}
    
//----------------------------------------------------------
	public final double computeClassLogProb(){
		double val2=0;
		
		int index;
		
		for(int j=0 ;j<s.t; j++){
			index = (separates_nonRecursive(s.tests[j].X(), s.tests[j].Y(), s.tests[j].getCond()))?  	
								IndependenceTestMargaritisUAI2001.I : IndependenceTestMargaritisUAI2001.D;
			val2 += s.pds[index][j];
		}
		double aux = Math.exp(val2);
		return val2;
	}
	//------------------------------------------------------------
	public final String getClassStringSignature(){
		String ret = "";
		for(int i=0; i<s.t; i++){
			//if(separates(tests[i].X, tests[i].Y, tests[i].FastZ)) ret += "1";
			if(separates(s.tests[i].X(), s.tests[i].Y(), i)) ret += "1";
			else ret +="0";
		}
		
		return ret;
	}
	//------------------------------------------------------------
	public final String getClassStringSignature(gsTriplet [] tests, int t){
		String ret = "";
		for(int i=0; i<t; i++){
			//if(separates(tests[i].X, tests[i].Y, tests[i].FastZ)) ret += "1";
			if(separates(tests[i].X(), tests[i].Y(), i)) ret += "1";
			else ret +="0";
		}
		
		return ret;
	}
	//------------------------------------------------------------
	public final FastSet getClassSignature(gsTriplet [] tests, int t){
		FastSet sign = new FastSet(t);
		for(int i=0; i<t; i++){
			//if(separates(tests[i].X, tests[i].Y, tests[i].FastZ)) sign.add(i);
			if(separates(tests[i].X(), tests[i].Y(), i)) sign.add(i);
		}
		return sign;
	}
	
	public static void main(String[] args) {
	}
}
