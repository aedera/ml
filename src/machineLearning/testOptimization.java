package machineLearning;
import java.util.ArrayList;
import java.util.Vector;
import java.util.Random;
import java.util.Date;
import java.util.BitSet;


//import sun.security.krb5.internal.crypto.w;
import machineLearning.Utils.*;
import machineLearning.independenceBased.*;
import machineLearning.Graphs.*;
import machineLearning.Graphs.GraphsSimple.*;
import machineLearning.Datasets.*;
import machineLearning.DataStructures.*;

/**
 * @author bromberg
 *
 */
public class testOptimization {
	int n;
	structuresParticlesFilter s;
    posterior p;
    
    //for greedy search
    QElement [][] Q;
    FastSet [] C;
    QElement [] R;
    int Qsize [], Rsize;
    BitSet testsString [];  

    
    double avg = 0;

	//----------------------------------------------------------------
	public testOptimization(posterior p){
		this.n = p.n;
        this.s = p.s;
        this.p = p;
		doMemoryAllocation(n);
	}
	//-----------------------------------------------------------------
	//MUST SET PARAMETERS PRIOR TO CALLING THIS PROCEDURE
	public void doMemoryAllocation(int n){
		this.n = n;
		
//        Q = new QElement[p.K][];
//        C = new FastSet[n*n*n*n];
//        R = new QElement[C.length];
//        testsString  = new BitSet[C.length];
        Qsize = new int[p.K];
        for(int i=0; i<p.K; i++) {
 //           Q[i] = new QElement[C.length];
            Qsize[i] = 0;
        }
//        for(int i=0; i<C.length;i++){
//        	C[i] = new FastSet(n);
//            R[i] = new QElement();
//            testsString[i] = new java.util.BitSet();
//
//            for(int j=0; j<p.K; j++) {
//            	Q[j][i] = new QElement();
//            }
//        }
	}

	
    
//  --------------------------------------------------------
    class QElement{
        public int X, Y;
        public double h = 0, alpha = 0;
        public FastSet S = null;
        BitSet testsString;
        //public QElement prev = null;
        QElement(){
        	S = new FastSet(n);
            testsString = new BitSet();
          //  prev = new QElement();
        }
        QElement(double h, double alpha, int X, int Y, FastSet S){
        	this.alpha = alpha;
        	this.h = h;
            this.X = X;
            this.Y = Y;
            this.S = S;
        }
        final  void set(double h, double alpha, int X, int Y, FastSet S, java.util.BitSet testString){
        	//prev.copyFrom(this);
        	
            this.alpha = alpha;
            this.h = h;
            this.X = X;
            this.Y = Y;
            this.S.bitset = (BitSet) S.bitset.clone();
            
            if(false || Utils._ASSERT){
                java.util.BitSet aux = s.getTestsString(X, Y,S);
                Utils.FORCED_ASSERT(aux.equals(testString), "OOPS in getTestsString");
            }
            if(testString != null) this.testsString = (BitSet) testString.clone();
        }
        final void copyFrom(QElement Q){
             this.h = Q.h;
             this.alpha = Q.alpha;
             this.X = Q.X;
             this.Y = Q.Y;
             this.S.bitset = (BitSet) Q.S.bitset.clone();
             if(Q.testsString != null) this.testsString = (BitSet) Q.testsString.clone();
        }
       
    }
    //-------------------------------------------------------------
    private ArrayList<gsTriplet> getInitialTests(gsTriplet [] particles){
      ArrayList<gsTriplet> tests = new ArrayList<gsTriplet>(s.t + n*n);
      
      /** Tests done so far */
	  if(false){
		  for(int t=0; t<s.t; t++){
		      tests.add(new gsTriplet(s.tests[t]));
		  }
	  }
      /** Unconditional */
      if(true){
		  for(int i=0;i<n; i++) {
	          for(int j=i+1; j<n; j++) tests.add(new gsTriplet(i,j, new FastSet(n), true));
	      }
      }
      /** Conditioned on universe*/
      if(true){
		  for(int i=0;i<n; i++) {
	          for(int j=i+1; j<n; j++) {
				  FastSet U = new FastSet(n);
				  U.setAlltoOne();
				  U.remove(i);
				  U.remove(j);
				  tests.add(new gsTriplet(i,j, U, true));
	          }
	      }
      }
	  /** Random cond set */
      double alpha = 0.0; //if 0, then it's equivalent to unconditional
      if(false){
		  for(int i=0;i<n; i++) {
	          for(int j=i+1; j<n; j++) {
	        	  FastSet S = new FastSet(n);
	        	  for(int k = 0; k<n; k++){
	        		  if(k == i || k == j) continue;
	        		  if(p.r.nextDouble() < alpha) S.add(k);
	        	  }
	        	  tests.add(new gsTriplet(i,j, S, true));
	          }
	      }
      }
	  
	  /** Previous tests */
	  if(true && particles !=null){
		  for(int t=0; t<particles.length; t++){
		      tests.add(particles[t]);
		  }
	  }
	  return tests;
    }
 
    //-------------------------------------------------------------
    private gsTriplet searchOptimalCondSet(gsTriplet initial, double [] maxScore, int maxNumFlips){
        int X = initial.X();
        int Y = initial.Y();
        FastSet S = initial.getCond();
        double score;
        double [] alpha = new double[1], infogain = new double[1];
        //int ix_max = 0;
        FastSet flipSetMax = null;

        
		/** Initialization */
        maxScore[0] = Double.MIN_VALUE;//p.computeTestUnnormalizedPosterior(X, Y, S, alpha, infogain, s);
		boolean anyBetter = true;
		FastSet all = new FastSet(n);
		all.setAlltoOne();
		ArrayList<FastSet> flipSets = new ArrayList<FastSet>();
		
		
		for(int mnf = 0; mnf<=maxNumFlips; mnf++){
			flipSets.addAll(all.getAllSubsets(mnf));
		}
			
    	while(anyBetter){
    		/** Iterates over all bits in S set to false, i.e., over the complement of S */
    		anyBetter = false;
    		
    		/** Trying upward and downward together*/
    		//for(int ix=S.firstFalse(); ix>=0; ix=S.nextFalse()) {
    		//for(int ix=0; ix<n; ix++) {
    		for(FastSet flipSet : flipSets){
    			if(flipSet.isMember(X) || flipSet.isMember(Y)) continue;
        			//if(ix == X || ix == Y) continue;
	    			
				for(int ix=flipSet.first(); ix>=0; ix=flipSet.next()) S.flip(ix);
    			score = p.testScore(X, Y, S, alpha, infogain, s);
                for(int ix=flipSet.first(); ix>=0; ix=flipSet.next()) S.flip(ix);

                //Utils.FORCED_ASSERT(score >= 0, "Negative score");
    			//if(maxNumFlips > 5) System.out.println("(" + X + "," + Y + "|" + S + ") " + score);
                if(score > maxScore[0]){
                	///**FOR DEBUGGING*/ score = p.computeTestUnnormalizedPosterior(X, Y, S, alpha, infogain, s);
                    maxScore[0] = score;
                    //ix_max = ix;
                    flipSetMax = flipSet;
                    anyBetter = true;
                }
    		}
    		if(anyBetter) {
    			//S.flip(ix_max);
    			for(int ix=flipSetMax.first(); ix>=0; ix=flipSetMax.next()) S.flip(ix);
    		}
    	}
    	return maxScore[0] == 0.0? null : initial;
    }
    //-------------------------------------------------------------
    public gsTriplet getOptimalTestDeterministic(){
    	gsTriplet optimal = null, OPTIMAL = null;
    	ArrayList<gsTriplet> tests = getInitialTests(null);
    	
    	boolean print = p.exp.output.doPrint(coVL.MAINLOOP);
    	
    	int maxNumFlips = 1;
    	double maxScore = Double.MIN_VALUE;
    	double [] maxScoreSingle = new double[1];
	    while(OPTIMAL == null && maxNumFlips <= 4){
	    	for(gsTriplet triplet : tests){
	    		/*
	    		while(optimal == null) {
	    			optimal = searchOptimalCondSet(triplet, maxScoreSingle, numFlips);
	    			numFlips++;
	    		}*/
	    		optimal = searchOptimalCondSet(triplet, maxScoreSingle, maxNumFlips);
	    		if(maxScoreSingle[0] > maxScore){
	    			maxScore = maxScoreSingle[0];
	    		    OPTIMAL = optimal;
	    		    //if(maxNumFlips >= 4) break;
	    		}
	    	}
	    	maxNumFlips++;
	  	  	if(OPTIMAL == null ) {
		  		if(print) System.out.println("Increasing number of flips to " + maxNumFlips);
	    	}
    	}
    	//if(OPTIMAL == null) Utils.FORCED_ASSERT(OPTIMAL != null, "");
		return OPTIMAL;
    }
    
    //-------------------------------------------------------------
    int AA=0;
    public gsTriplet getOptimalTestGreedy(){
        long runtime = System.currentTimeMillis();
        FastSet S = new FastSet(n);
        String Sstring;
        gsTriplet [] tests = new gsTriplet[s.t + n*n];
        int X,Y, T=0; //c for current
        double h, hT, pp;
        double [] alpha = new double[1], infogain = new double[1];
        int Rsize = 0;
        int Csize = 0, i,k, j;
        boolean insertedAny;
        QElement Qel = new QElement();
        QElement R = new QElement();  //optimal
        boolean Rinitialized = false;
        FastSet Scomp = new FastSet(n);

        Integer hashCode = new Integer(0);
        int hashcode;
        boolean onlyDown = false;
        double maxInfoGain = 0d; //s.maxInfoGain();
        java.util.Hashtable Visitedhash = new java.util.Hashtable();
        //Collect tests to move: all tests done so far plus i,j|{} for pairs not done yet
        {
                boolean [][] ij = new boolean[n][];
                for( i=0;i<n; i++) {
                    ij[i] = new boolean[n];
                    for( j=0; j<n; j++) ij[i][j] = true;
                }

                if(true){
                    for(int t=0; t<s.t; t++){
                        tests[T] = s.tests[t];
                        ij[tests[T].X()][tests[T].Y()] = false;
                        ij[tests[T].Y()][tests[T].X()] = false;
                        T++;
                    }
                }
                for( i=0 ;i<n; i++){
                    for( j=i+1 ;j<n; j++){
                        if(true || ij[i][j]) {
                            tests[T] = new gsTriplet(i,j, new FastSet(n), true);
                            T++;
                        }
                    }
                }
        }
        
        
        
        
        
        
        
        
        
        
        
        //For each test in tests, find optimal increment for S.
        double A=0, B=0, CC=0, DD=0;
        double H;
        while(!Rinitialized){
	        for(int t=0; t<T; t++){
	            X = tests[t].X();
	            Y = tests[t].Y();
	            //FastSet SS = new FastSet(tests[t].FastZ);
	            int maxCard = 0, card;
	            if(tests[t].getCond().isEmpty() ) maxCard = (n)*1;
	            //for(int Scard=0; Scard<=maxCard; Scard++) {
	      	//INITIALIZE S
	            for(int Scard=0; Scard<=0; Scard++) {
		                //int Scard2 = r.nextInt(n+1);
		                pp = ((double)Scard/1)/((double)n);
		                //p=0.5;
		                //Reset
	                    for(i=0; i<p.K; i++) {
	                       // Q[i] = new QElement[n*n*n];
	                        Qsize[i] = 0;
	                    }
	                    Visitedhash.clear();
	                    insertedAny = true;
	                    onlyDown = false;
	    
	                    //Generate random subset of V-{X,Y}
	                    if(!tests[t].getCond().isEmpty()){
	                       S = new FastSet(tests[t].getCond());   
	                    }
	                    else {
	                        if(false){
	                            do{
	                                S.clear();
	                                for(i=0; i<n; i++){
	                                    if(i == X || i == Y) continue;
	                                    if(p.r.nextDouble() <= pp) S.add(i);
	                                }
	                            }while( false &&(S.cardinality() != Scard || S.isMember(X) || S.isMember(Y)));
	                        }
	                        if(true) S.clear();
	                        if(false) S.setAlltoOne();
	                        S.remove(X);
	                        S.remove(Y);
	                        
	                        double [] infoGain = new double[1];
	                        h = p.testScore(X, Y, S, alpha, infoGain, s);
	    //                    System.out.println("score("+ X +","+ Y+ "|"+ S +") = " + h);
	                        
	                        Scomp.setAlltoOne();
	                        Scomp.remove(X);
	                        Scomp.remove(Y);
	                        Scomp = FastSet.Minus(Scomp, S);
	                        FastSet Smax= new FastSet(S);
	                        double info_max=infoGain[0];
	                        while((true || alpha[0] == 0) && Scomp.cardinality() > 0) {
	                            int aux = Scomp.getOn(p.r.nextInt(Scomp.cardinality())); 
	                            S.add(aux);
	                            Scomp.remove(aux);
	                            //h = s.infoGain(X, Y, S, alpha);
	                            h = p.testScore(X, Y, S, alpha, infoGain, s);
	                            if(infoGain[0] > info_max){
	                                info_max = infoGain[0];
	                                Smax = new FastSet(S);
	  //                              System.out.println("score("+ X +","+ Y+ "|"+ Smax +") = " + h);
	                            }
	                       }
	                        while((true || alpha[0] == 1) && S.cardinality() > 0){
	                            int aux = S.getOn(p.r.nextInt(S.cardinality()));
	                            S.remove(aux);
	                           Scomp.add(aux);
	                            //h = s.infoGain(X, Y, S, alpha);
	                            h = p.testScore(X, Y, S, alpha, infoGain, s);
	                            if(infoGain[0] > info_max){
	                                info_max = infoGain[0];
	                                Smax = new FastSet(S);
	//                                System.out.println("score("+ X +","+ Y+ "|"+ Smax +") = " + h);
	                            }
	                            
	                        }
	                        if(true) S = Smax;
	                        
	/*                        S.clear();
	                        DoublyLinkedListElement current = adjList[u].head;
	                        while(current != null){
	//                        Iterator it = adjList[u].elements();
	//                          while(it.hasMoreElements()){
	                            auxElement = current;
	                            current = current.next;
	                            w= auxElement.index;//adjList[u][i];
	                        }
	*/                        
	                        //System.out.print("a_init  = " + alpha[0] + " ");
	                    }
		                 h = p.testScore(X, Y, S, alpha, null, s);
		                 Qel.set(h, alpha[0], X, Y, S, null);
		                //Add first element to Queue
	                
		                Visitedhash.put(S.bitset, S.bitset);
		                
		                //FastSet C = new FastSet(n);
		                insertedAny = true;
		                int I=0, J=0;
	
		          //GROW S WHILE ...    
		                while(insertedAny){
		                    insertedAny = false;
		
		                    //FIXME Qel.alpha <= 0.5 !!!. Note however that we always go up because (true || ...)
		                    if(!onlyDown && (true|| Qel.alpha <= 0.5)) { //UP 
		                        Scomp.setAlltoOne();
		                        Scomp.remove(X);
		                        Scomp.remove(Y);
		                        Scomp = FastSet.Minus(Scomp, Qel.S);
		                        //i=Scomp.bitset.nextSetBit(0);
		                        FastSet C = new FastSet(n);
		                        C.bitset = (BitSet) Qel.S.bitset.clone();
		                        //while(i >= 0){
		                        while(!Scomp.isEmpty()){
		                            i = p.r.nextInt(Scomp.cardinality());
		                            I = Scomp.getOn(i);
		                            Scomp.remove(I);
		                            A++;
		                            C.add(I);
		                            
		                            if(Visitedhash.contains(C.bitset)){
		                                int askljdf;
		                            }
		                            else{
		                                Visitedhash.put(C.bitset, C.bitset);
		                                h = p.testScore(X, Y, C, alpha, infogain, s);
		                                
		                                if( h > Qel.h  ||  (h == Qel.h  && C.cardinality() < Qel.S.cardinality())){
		                                    Qel.set(h, alpha[0], X, Y, C, null);
		                                    insertedAny = true;
		      //                              System.out.println("score("+ X +","+ Y+ "|"+ C +") = " + h);
		                                    CC++;
		                                    break;
		                                }
		                            }   
		                            
		                            C.remove(I);
		                            //i = Scomp.bitset.nextSetBit(i+1);
		                        }
		                    }
		                    //FIXME Qel.alpha >= 0.5 !!!. Note however that we always go down because (true || ...)
		                    if(true || Qel.alpha >= 0.5) { //DOWN 
		                        A++;
		                        //i = Qel.S.bitset.nextSetBit(0);
		                        FastSet C = new FastSet(n);
		                        FastSet SS = new FastSet(Qel.S);
		                        C.bitset = (BitSet) Qel.S.bitset.clone();
		                        //while(i >= 0){
		                        while(!SS.isEmpty()){
		                            i = p.r.nextInt(SS.cardinality());
		                            I = SS.getOn(i);
		                            SS.remove(I);
		                            C.remove(I);
		                            
		                            if(!Visitedhash.contains(C.bitset)) {
		                                Visitedhash.put(C.bitset, C.bitset);
		                                h = p.testScore(X, Y, C, alpha, infogain, s);
		                                
		                                if( h > Qel.h  ||  (h == Qel.h  && C.cardinality() < Qel.S.cardinality())){
		                                    Qel.set(h, alpha[0], X, Y, C, null);
		                                    insertedAny = true;
		        //                            System.out.println("score("+ X +","+ Y+ "|"+ C +") = " + h);
		                                    CC++;
		                                    break;
		                                }
		                            }   
		                            
		                            C.add(I);
		                            //i = Qel.S.bitset.nextSetBit(i+1);
		                        }
		                        
		                    }
		                    if(true){ //in-cardinality moves
		                        j=0;
		                        Scomp.setAlltoOne();
		                        Scomp.remove(X);
		                        Scomp.remove(Y);
		                        Scomp = FastSet.Minus(Scomp, Qel.S);
		                        FastSet C = new FastSet(n);
		                        C.bitset = (BitSet) Qel.S.bitset.clone();
		                        FastSet SS[] = new FastSet[n];
		                        for(k=0; k<n; k++) {
		                            SS[k] = new FastSet(C);
		                            SS[k].add(k);
		                        }
		                        //System.out.println(Qel.S);
		                        ///System.out.println(Scomp);
		                        //while(i >= 0){
		                        while(!Scomp.isEmpty()){
		                            i = p.r.nextInt(Scomp.cardinality());
		                            I = Scomp.getOn(i);
		                            if(SS[I].isEmpty()) {
		                                Scomp.remove(I);
		                                continue;
		                            }
		                            A++;
		                            C.add(I);
		                            
		                            j = p.r.nextInt(SS[I].cardinality());
		                            J = SS[I].getOn(j);
		                            SS[I].remove(J);
		                            C.remove(J);
		
		                            //System.out.println(I + ", " + J);
		                            if(!Visitedhash.contains(C.bitset)) {
		                                Visitedhash.put(C.bitset, C.bitset);
		                                h = p.testScore(X, Y, C, alpha, infogain, s);
		                                
		                                if( h > Qel.h  ||  (h == Qel.h  && C.cardinality() < Qel.S.cardinality())){
		                                    Qel.set(h, alpha[0], X, Y, C, null);
		                                    insertedAny = true;
		                                    CC++;
		          //                          System.out.println("score("+ X +","+ Y+ "|"+ C +") = " + h);
		                                    break;
		                                }
		                            }
		                            C.add(J);
		                            C.remove(I);
		                        }
		                    }
		                    if(true && Math.abs(infogain[0]-maxInfoGain) < 0.1*maxInfoGain){
		                        onlyDown = true;
		                        if(true && Qel.S.cardinality() == 0){
		                            return new gsTriplet(X, Y, Qel.S, true);
		                        }
		                    }
		                    if( Qel.h >= R.h  ||  (Qel.h == R.h  && Qel.S.cardinality() < R.S.cardinality())){
		                        R.copyFrom(Qel);
		                        Rinitialized = true;
		                    }
		                }
	            }
	        }
        }
        
        gsTriplet test = (R == null)? null : new gsTriplet(R.X, R.Y, R.S, true);
        
        //System.out.println(test);
        runtime = System.currentTimeMillis() - runtime;
        if(false || Utils._ASSERT){
            h = p.testScore(R.X, R.Y, R.S, alpha, null, s);
            avg += alpha[0];
//            System.out.println("RT = " + runtime + "  a= " + alpha[0] + "   WC = " + p.gs.weightedTestsCost + " AA = " + AA + "    A = " + A + "  CC = "+ CC + "  DD = "+ DD);
        }
        AA += A;

        return test;
    }
    
    

    //-------------------------------------------------------------
    public gsTriplet getOptimalTestGreedy__(){
        long runtime = System.currentTimeMillis();
        FastSet S = new FastSet(n);
        String Sstring;
        gsTriplet [] tests = new gsTriplet[s.t + n*n];
        int X,Y, T=0; //c for current
        double h, hT, pp;
        double [] alpha = new double[1], infogain = new double[1];
        int Rsize = 0;
        int Csize = 0, i,k, j;
        boolean insertedAny;
        QElement Qel = new QElement();
        QElement R = new QElement();  //optimal
        FastSet Scomp = new FastSet(n);

        Integer hashCode = new Integer(0);
        int hashcode;
        boolean onlyDown = false;
//        double maxInfoGain = s.maxInfoGain();
        
        java.util.Hashtable Visitedhash = new java.util.Hashtable();
        
        
        if(s.t == 12 || s.t==16){
         int asdfj=0;   
        }
        //Collect tests to move: all tests done so far plus i,j|{} for pairs not done yet
        {
                boolean [][] ij = new boolean[n][];
                for( i=0;i<n; i++) {
                    ij[i] = new boolean[n];
                    for( j=0; j<n; j++) ij[i][j] = true;
                }

                if(false){
                    for(int t=0; t<s.t; t++){
                        tests[T] = s.tests[t];
                        ij[tests[T].X()][tests[T].Y()] = false;
                        ij[tests[T].Y()][tests[T].X()] = false;
                        T++;
                    }
                }
                for( i=0 ;i<n; i++){
                    for( j=i+1 ;j<n; j++){
                        if(true || ij[i][j]) {
                            tests[T] = new gsTriplet(i,j, new FastSet(n), true);
                            T++;
                        }
                    }
                }
        }
        
        //Find optimal increment for S.
        double A=0, B=0, CC=0, DD=0;
        double H;
        for(int t=0; t<T; t++){
            X = tests[t].X();
            Y = tests[t].Y();
            //FastSet SS = new FastSet(tests[t].FastZ);
            int maxCard = 0, card;
            if(tests[t].getCond().isEmpty() ) maxCard = (n)*1;
            //for(int Scard=0; Scard<=maxCard; Scard++) {
            for(int Scard=0; Scard<=0; Scard++) {
                //int Scard2 = r.nextInt(n+1);
                pp = ((double)Scard/1)/((double)n);
                //p=0.5;

                //Reset
                    for(i=0; i<p.K; i++) {
                       // Q[i] = new QElement[n*n*n];
                        Qsize[i] = 0;
                    }
                    Visitedhash.clear();
                    insertedAny = true;
                    onlyDown = false;
    
                //Generate random subset of V-{X,Y}
                    if(!tests[t].getCond().isEmpty()){
                       S = new FastSet(tests[t].getCond());   
                    }
                    else {
                        if(false){
                            do{
                                S.clear();
                                for(i=0; i<n; i++){
                                    if(i == X || i == Y) continue;
                                    if(p.r.nextDouble() <= pp) S.add(i);
                                }
                            }while( false &&(S.cardinality() != Scard || S.isMember(X) || S.isMember(Y)));
                        }
                        if(true) S.clear();
                        if(false) S.setAlltoOne();
                        S.remove(X);
                        S.remove(Y);
                        
                        double [] infoGain = new double[1];
                        h = p.testScore(X, Y, S, alpha, infoGain, s);
    //                    System.out.println("score("+ X +","+ Y+ "|"+ S +") = " + h);
                        
                        Scomp.setAlltoOne();
                        Scomp.remove(X);
                        Scomp.remove(Y);
                        Scomp = FastSet.Minus(Scomp, S);
                        FastSet Smax= new FastSet(S);
                        double info_max=infoGain[0];
                        while((false || alpha[0] == 0) && Scomp.cardinality() > 0) {
                            int aux = Scomp.getOn(p.r.nextInt(Scomp.cardinality())); 
                        	S.add(aux);
                            Scomp.remove(aux);
                            //h = s.infoGain(X, Y, S, alpha);
                            h = p.testScore(X, Y, S, alpha, infoGain, s);
                            if(infoGain[0] > info_max){
                            	info_max = infoGain[0];
                                Smax = new FastSet(S);
  //                              System.out.println("score("+ X +","+ Y+ "|"+ Smax +") = " + h);
                            }
                       }
                        while(alpha[0] == 1 && S.cardinality() > 0){
                            int aux = S.getOn(p.r.nextInt(S.cardinality()));
                        	S.remove(aux);
                           Scomp.add(aux);
                            //h = s.infoGain(X, Y, S, alpha);
                            h = p.testScore(X, Y, S, alpha, infoGain, s);
                            if(infoGain[0] > info_max){
                                info_max = infoGain[0];
                                Smax = new FastSet(S);
//                                System.out.println("score("+ X +","+ Y+ "|"+ Smax +") = " + h);
                            }
                            
                        }
                        if(false) S = Smax;
                        
                        //System.out.print("a_init  = " + alpha[0] + " ");
                    }
                 h = p.testScore(X, Y, S, alpha, null, s);
                 Qel.set(h, alpha[0], X, Y, S, null);
                //Add first element to Queue
                
                Visitedhash.put(S.bitset, S.bitset);
                
                //FastSet C = new FastSet(n);
                insertedAny = true;
                int I=0, J=0;
                while(insertedAny){
                    insertedAny = false;

                    if(!onlyDown && (true|| Qel.alpha <= 0.5)) { //UP 
                        Scomp.setAlltoOne();
                        Scomp.remove(X);
                        Scomp.remove(Y);
                        Scomp = FastSet.Minus(Scomp, Qel.S);
                        //i=Scomp.bitset.nextSetBit(0);
                        FastSet C = new FastSet(n);
                        C.bitset = (BitSet) Qel.S.bitset.clone();
                        //while(i >= 0){
                        while(!Scomp.isEmpty()){
                            i = p.r.nextInt(Scomp.cardinality());
                            I = Scomp.getOn(i);
                            Scomp.remove(I);
                            A++;
                            C.add(I);
                            
                            if(Visitedhash.contains(C.bitset)){
                            	int askljdf;
                            }
                            else{
                                Visitedhash.put(C.bitset, C.bitset);
                                h = p.testScore(X, Y, C, alpha, infogain, s);
                                
                                if( h > Qel.h  ||  (h == Qel.h  && C.cardinality() < Qel.S.cardinality())){
                                    Qel.set(h, alpha[0], X, Y, C, null);
                                    insertedAny = true;
      //                              System.out.println("score("+ X +","+ Y+ "|"+ C +") = " + h);
                                    CC++;
                                    break;
                                }
                            }   
                            
                            C.remove(I);
                            //i = Scomp.bitset.nextSetBit(i+1);
                        }
                    }
                    if(true || Qel.alpha >= 0.5) { //DOWN 
                        A++;
                        //i = Qel.S.bitset.nextSetBit(0);
                        FastSet C = new FastSet(n);
                        FastSet SS = new FastSet(Qel.S);
                        C.bitset = (BitSet) Qel.S.bitset.clone();
                        //while(i >= 0){
                        while(!SS.isEmpty()){
                            i = p.r.nextInt(SS.cardinality());
                            I = SS.getOn(i);
                            SS.remove(I);
                            C.remove(I);
                            
                            if(!Visitedhash.contains(C.bitset)) {
                                Visitedhash.put(C.bitset, C.bitset);
                                h = p.testScore(X, Y, C, alpha, infogain, s);
                                
                                if( h > Qel.h  ||  (h == Qel.h  && C.cardinality() < Qel.S.cardinality())){
                                    Qel.set(h, alpha[0], X, Y, C, null);
                                    insertedAny = true;
        //                            System.out.println("score("+ X +","+ Y+ "|"+ C +") = " + h);
                                    CC++;
                                    break;
                                }
                            }   
                            
                            C.add(I);
                            //i = Qel.S.bitset.nextSetBit(i+1);
                        }
                        
                    }
                    if(true){ //in-cardinality moves
                        j=0;
                        Scomp.setAlltoOne();
                        Scomp.remove(X);
                        Scomp.remove(Y);
                        Scomp = FastSet.Minus(Scomp, Qel.S);
                        FastSet C = new FastSet(n);
                        C.bitset = (BitSet) Qel.S.bitset.clone();
                        FastSet SS[] = new FastSet[n];
                        for(k=0; k<n; k++) {
                        	SS[k] = new FastSet(C);
                            SS[k].add(k);
                        }
                        //System.out.println(Qel.S);
                        ///System.out.println(Scomp);
                        //while(i >= 0){
                        while(!Scomp.isEmpty()){
                            i = p.r.nextInt(Scomp.cardinality());
                            I = Scomp.getOn(i);
                            if(SS[I].isEmpty()) {
                                Scomp.remove(I);
                                continue;
                            }
                            A++;
                            C.add(I);
                            
                            j = p.r.nextInt(SS[I].cardinality());
                            J = SS[I].getOn(j);
                            SS[I].remove(J);
                            C.remove(J);

                            //System.out.println(I + ", " + J);
                            if(!Visitedhash.contains(C.bitset)) {
                                Visitedhash.put(C.bitset, C.bitset);
                                h = p.testScore(X, Y, C, alpha, infogain, s);
                                
                                if( h > Qel.h  ||  (h == Qel.h  && C.cardinality() < Qel.S.cardinality())){
                                    Qel.set(h, alpha[0], X, Y, C, null);
                                    insertedAny = true;
                                    CC++;
          //                          System.out.println("score("+ X +","+ Y+ "|"+ C +") = " + h);
                                    break;
                                }
                            }
                            C.add(J);
                            C.remove(I);
                        }
                    }
//                    if(true && Math.abs(infogain[0]-maxInfoGain) < 0.1*maxInfoGain){
//                        onlyDown = true;
//                        if(true && Qel.S.cardinality() == 0){
//                            return new gsTriplet(X, Y, Qel.S, true);
//                        }
//                    }
                    if( Qel.h > R.h  ||  (Qel.h == R.h  && Qel.S.cardinality() < R.S.cardinality())){
                    	R.copyFrom(Qel);
                    }
                        
                }
            }
        }
        
        gsTriplet test = R == null? null : new gsTriplet(R.X, R.Y, R.S, true);
        
        //System.out.println(test);
        runtime = System.currentTimeMillis() - runtime;
        if(true || Utils._ASSERT){
        	h = p.testScore(R.X, R.Y, R.S, alpha, null, s);
            avg += alpha[0];
//            System.out.println("RT = " + runtime + "  a= " + alpha[0] + "   WC = " + p.gs.weightedTestsCost + " AA = " + AA + "    A = " + A + "  CC = "+ CC + "  DD = "+ DD);
        }
        AA += A;

        return test;
    }
    
    
    //-------------------------------------------------------------
    double aaa=0, alphaAvg=0;
    /**
     * Pseudo code in posterior/write-up/afterAAAI.dvi 
    */
    public gsTriplet getOptimalTestGreedy_(){
        long runtime = System.currentTimeMillis();
        FastSet S = new FastSet(n);
        String Sstring;
        gsTriplet [] tests = new gsTriplet[s.t + n*n];
        int X,Y, T=0; //c for current
        double h, hT, pp;
        double [] alpha = new double[1], infogain = new double[1];
        int Csize = 0, i,k, j;
        boolean insertedAny;
        QElement Qel;
        FastSet Scomp = new FastSet(n);
        
        Integer hashCode = new Integer(0);
        int hashcode;
        boolean onlyDown = false;
        double maxInfoGain = 0d; // s.maxInfoGain();
        
        java.util.Hashtable Visitedhash = new java.util.Hashtable();
        
        
        Rsize = 0;
        
        if(s.t == 12 || s.t==16){
         int asdfj=0;   
        }
        //Collect tests to move: all tests done so far plus i,j|{} for pairs not done yet
        {
                boolean [][] ij = new boolean[n][];
                for( i=0;i<n; i++) {
                    ij[i] = new boolean[n];
                    for( j=0; j<n; j++) ij[i][j] = true;
                }

                if(false){
                    for(int t=0; t<s.t; t++){
                        tests[T] = s.tests[t];
                        ij[tests[T].X()][tests[T].Y()] = false;
                        ij[tests[T].Y()][tests[T].X()] = false;
                        T++;
                    }
                }
                for( i=0 ;i<n; i++){
                    for( j=i+1 ;j<n; j++){
                        if(true || ij[i][j]) {
                            tests[T] = new gsTriplet(i,j, new FastSet(n), true);
                            T++;
                        }
                    }
                }
        }
        //Find optimal pair
        int optI=0, optJ=1;
        if(false){
            double optH=0;
            for( i=0 ;i<n; i++){
                for( j=i+1 ;j<n; j++){
                    FastSet SS = new FastSet(n);
                    SS.setAlltoOne();
                    SS.remove(i);
                    SS.remove(j);
                    h = s.infoGain(i, j, SS, null, null);
                    if(h > optH) {
                        optH = h;
                        optI = i;
                        optJ = j;
                    }
                    
                    //h = p.computeTestUnnormalizedPosterior(i, j, SS, null, null, s);
                }
            }
        }        
        
        //Find optimal increment for S.
        double A=0, B=0, CC=0;
        for(int t=0; t<T; t++){
            X = tests[t].X();
            Y = tests[t].Y();
            if(false && !((X == optI && Y == optJ) || (X == optJ && Y == optI))) continue;
            
            //FastSet SS = new FastSet(tests[t].FastZ);
            int maxCard = 0;
            //if(tests[t].FastZ.isEmpty() ) maxCard = (n)*1;
            //for(int Scard=0; Scard<=n*1; Scard++) {
            for(int Scard=0; Scard<=0; Scard++) {
                //int Scard2 = r.nextInt(n+1);
                pp = ((double)Scard/1)/((double)n);
                //p=0.5;

                //Reset
                    for(i=0; i<p.K; i++) {
                       // Q[i] = new QElement[n*n*n];
                        Qsize[i] = 0;
                    }
                    Visitedhash.clear();
                    insertedAny = true;
                    onlyDown = false;
    
                //Generate random subset of V-{X,Y}
                    if(!tests[t].getCond().isEmpty()){
                       S = new FastSet(tests[t].getCond());   
                    }
                    else {
                        if(true){
                            do{
                                S.clear();
                                for(i=0; i<n; i++){
                                    if(i == X || i == Y) continue;
                                    if(p.r.nextDouble() <= pp) S.add(i);
                                }
                            }while( false &&(S.cardinality() != Scard || S.isMember(X) || S.isMember(Y)));
                        }
                        if(true) S.clear();
                        if(false) S.setAlltoOne();
                        S.remove(X);
                        S.remove(Y);
                        
                        //0.5 \notin [delta, gamma]?
//                            FastSet SS = new FastSet(n);
//                            h = p.computeTestUnnormalizedPosterior(X, Y, S, alpha, null, s);
                            
                        
                        
                        double [] infoGain = new double[1];
                        h = p.testScore(X, Y, S, alpha, infoGain, s);
                        
                        Scomp.setAlltoOne();
                        Scomp.remove(X);
                        Scomp.remove(Y);
                        Scomp = FastSet.Minus(Scomp, S);
                        FastSet Smax= new FastSet(S);
                        double info_max=infoGain[0];
                        while((false || alpha[0] == 0) && Scomp.cardinality() > 0) {
                            int aux = Scomp.getOn(p.r.nextInt(Scomp.cardinality())); 
                            S.add(aux);
                            Scomp.remove(aux);
                            //h = s.infoGain(X, Y, S, alpha);
                            h = p.testScore(X, Y, S, alpha, infoGain, s);
                            if(infoGain[0] > info_max){
                                info_max = infoGain[0];
                                Smax = new FastSet(S);
                            }
                       }
                        while(alpha[0] == 1 && S.cardinality() > 0){
                            int aux = S.getOn(p.r.nextInt(S.cardinality()));
                            S.remove(aux);
                           Scomp.add(aux);
                            //h = s.infoGain(X, Y, S, alpha);
                            h = p.testScore(X, Y, S, alpha, infoGain, s);
                            if(infoGain[0] > info_max){
                                info_max = infoGain[0];
                                Smax = new FastSet(S);
                            }
                            
                        }
                        if(false) S = Smax;
                    }
                 h = p.testScore(X, Y, S, alpha, null, s);

                //Add first element to Queue
                //Q[0][0].set(h, alpha[0], X, Y, S, s.getTestsString(X, Y, S));
                Q[0][0] = new QElement(h, alpha[0], X, Y, S);
                Q[0][0].testsString = s.getTestsString(X, Y, S);
                Qsize[0]++;
             
                Visitedhash.put(S.bitset, S.bitset);
                
                int aaa = 0;
                while(insertedAny){
                    aaa++;
                    if(Rsize > 0 && Q[0][0].h > R[0].h) Rsize = 0;
                    if(Rsize == 0 || Q[0][0].h >= R[0].h) {
                        for(i=0; i<Qsize[0]; i++){
                            //R[Rsize+i] = new QElement(Q[0][i].h, Q[0][i].alpha, Q[0][i].X, Q[0][i].Y, new FastSet(Q[0][i].S));
                            R[Rsize+i].copyFrom(Q[0][i]);
                        }
                        Rsize += Qsize[0];
                    }
               
                    
                    Csize = 0;
                    //C = new FastSet[n*n*n*n];
    
                    //int aux = r.nextInt(Qsize[0]);
                    for(k=0; k<p.K; k++){
                        for(j=0; j<Qsize[k]; j++){
                            //if(j != aux) continue;
                            Qel = Q[k][j];
                            if(!onlyDown && (true|| Qel.alpha <= 0.5)) { //UP 
                                Scomp.setAlltoOne();
                                Scomp.remove(X);
                                Scomp.remove(Y);
                                Scomp = FastSet.Minus(Scomp, Qel.S);
                                i=Scomp.bitset.nextSetBit(0);
                                while(i >= 0){
                                    C[Csize].bitset = (BitSet) Qel.S.bitset.clone();
                                    C[Csize].add(i);
                                    //s.getTestsString2(Qel.X, Qel.Y, C[Csize], Qel.testsString, testsString[Csize], true);
                                    Csize++;
                                    i = Scomp.bitset.nextSetBit(i+1);
                                }
                            }
                            if(true || Qel.alpha >= 0.5) { //DOWN 
                                i = Qel.S.bitset.nextSetBit(0);
                                while(i >= 0){
                                    C[Csize].bitset = (BitSet) Qel.S.bitset.clone();
                                    C[Csize].remove(i);
                                    //s.getTestsString2(Qel.X, Qel.Y, C[Csize], Qel.testsString, testsString[Csize], false);

                                    Csize++;
                                    i = Scomp.bitset.nextSetBit(i+1);
                                }
                                
                            }
                            if(false){ //in-cardinality moves
                                //Utils.FORCED_ASSERT(false, "FIX UPs for SIDE MOVES!!!");
                                Scomp.setAlltoOne();
                                Scomp.remove(X);
                                Scomp.remove(Y);
                                Scomp = FastSet.Minus(Scomp, Qel.S);
                                i=Scomp.bitset.nextSetBit(0);
                                while(i >= 0){
                                    int kk;
                                    kk = S.bitset.nextSetBit(0);
                                    while(kk >= 0){
                                        C[Csize] = new FastSet(Qel.S);
                                        //C[Csize].bitset = (BitSet) Qel.S.bitset.clone();
                                        C[Csize].add(i);
                                        //s.getTestsString2(Qel.X, Qel.Y, C[Csize], Qel.testsString, testsString[Csize], true);

                                        C[Csize].remove(kk);
                                        //s.getTestsString2(Qel.X, Qel.Y, C[Csize], testsString[Csize], testsString[Csize], false);

                                        Csize++;
                                        kk = Scomp.bitset.nextSetBit(kk+1);
                                    }
                                    i = Scomp.bitset.nextSetBit(i+1);
                                }
                            }
                         }
                    }
                     for(i=0; i<p.K; i++) Qsize[i] = 0;
                    
                    //for all T \in C
                    insertedAny = false;
                    C: for(j=0 ; j<Csize; j++){
                        //A++;
                        if(Visitedhash.contains(C[j].bitset)) {
                            continue;
                        }
                        A++;
                        Visitedhash.put(C[j].bitset, C[j].bitset);
                        h = p.testScore(X, Y, C[j], alpha, infogain, s, null);//testsString[j]);
                        
                        if(true && Math.abs(infogain[0]-maxInfoGain) < 0.1*maxInfoGain){
                            onlyDown = true;
                            if(true && C[j].cardinality() == 0){
                                return new gsTriplet(X, Y, C[j], true);
                            }
                        }
                        //FIND POSITION IN Q
                        //First, check if there's any array in Q with (entropy, |S|) equal to (h,|S|).
                        for(i=0; i<p.K; i++){ 
                            //if(Qsize[i] > 0 && Q[i][0].h == h) {
                            if(Qsize[i] > 0 && Q[i][0].h == h && Q[i][0].S.cardinality() == C[j].cardinality()) {
                                //Q[i][Qsize[i]] = new QElement(h, alpha[0], X, Y, new FastSet(C[j]));
                                Q[i][Qsize[i]].set(h, alpha[0], X, Y, C[j], testsString[j]);
                                CC++;
                                Qsize[i]++;
                                insertedAny = true;
                                continue C;
                            }
                        }
                        i = p.K;  //i is the position where new one should be added
                        //while(i > 0 && (Qsize[i-1] == 0 || h > Q[i-1][0].h )) {
                        while(i > 0 && (Qsize[i-1] == 0 || h > Q[i-1][0].h  ||  (h == Q[i-1][0].h  && C[j].cardinality() < Q[i-1][0].S.cardinality())  )) {
                            Qsize[i-1] = 0;
                            i--;
                        }
                        if(i < p.K && (/*i > 0 && */h > 0 && h < 1.0))  {    //add
                            //Q[i][Qsize[i]] = new QElement(h, alpha[0], X, Y, new FastSet(C[j]));
                            Q[i][Qsize[i]].set(h, alpha[0], X, Y, C[j], testsString[j]);
                            CC++;
                            Qsize[i]++;
                            insertedAny = true;
                        }
                    }
                }
            }
        }
        
        //Break ties among survivors (elements in R). We re-use Q[0] here.
        Qsize[0] = 0;
        int min = R[0] == null? 0:R[0].S.cardinality();
        int min_index = 0;
        if(R[0] != null && R[0].S.isEmpty()){
            int aslkjfasdlk=0;   
        }
        for(i = 1; i<Rsize; i++){
                if(R[i].S.cardinality() < min){
                min = R[i].S.cardinality();
                min_index = i;
            }
        }
        for(i = 0; i<Rsize; i++){
            if(R[i].S.cardinality() == min){
                Q[0][Qsize[0]] = R[i];
                Qsize[0]++;
            }
        }
        Qel = Qsize[0] == 0? null : Q[0][p.r.nextInt(Qsize[0])];
        gsTriplet test = Qel == null? null : new gsTriplet(Qel.X, Qel.Y, Qel.S, true);
        
        //System.out.println(test);
        if(false || Utils._ASSERT){
            h = p.testScore(Qel.X, Qel.Y, Qel.S, alpha, null, s);
            avg += alpha[0];
            System.out.println(test+ "   alpha = " + alpha[0] + "  Avg = "+ ((double)avg) / ((double)s.t) );
        }
        AA += A;
        alphaAvg += Math.abs(Qel.alpha-0.5);
        aaa++;
        runtime = System.currentTimeMillis() - runtime;

//        System.out.println("RT = " + runtime + " a = " + Utils.myDoubleToString(Qel.alpha,2) + " E[a] = " + Utils.myDoubleToString(alphaAvg/aaa,4)  + "   WC = " + " AA = " + AA + "    A = " + A  + "  CC = "+ CC );
        return test;
    }
    
	// --------------------------------------------------------
	public gsTriplet getOptimalTestsBruteForce() {
		long prev;

		gsTriplet yp;

		double p_max = Double.MIN_VALUE, score, hn = -1;
		int card_min = n, card;
		gsTriplet y_min = null;

		boolean print = Experiment2.output.doPrint(coVL.MAINLOOP);
		
		
		prev = System.currentTimeMillis();
		if(print) System.out.print("FINDING OPTIMAL TEST: ");

		// Utils.println(pfmnVL.MAINLOOP | pfmnVL.LOG, "entropy = "+ s.computeEntropy());

		boolean skip = false;
		for(int x = 0; x<n; x++){
			for(int y=x+1; y<n; y++){
				FastSet all = new FastSet(n);
				all.setAlltoOne();
				all.remove(x);
				all.remove(y);
				ArrayList<FastSet> subsets = all.subsetsEq();
				for(FastSet S : subsets){
					yp = new gsTriplet(x, y, S, false);
		
					score = p.testScore(yp.X(), yp.Y(), yp.getCond(), null, null, s);
					card = yp.getCond().cardinality();
					if ((score == p_max && card < card_min) || score > p_max) {
						// hn = s.computeInfo_Gain_forCandidate(yp);
						p_max = score;
						card_min = card;
						y_min = yp;
					}
				}
			}
		}
		return y_min;
	}

}
