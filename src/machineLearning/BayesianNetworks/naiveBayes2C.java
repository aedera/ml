/*
 * Created on Jun 30, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package machineLearning.BayesianNetworks;

import machineLearning.Datasets.*;
import machineLearning.Datasets.Schema;

/**
 * Class of a Bayesian network with a fixed structure, one where 
 * the last node is the parent of all the rest (as Naive Bayes) and
 * the one previous to the last one is also parent of all the rest.
 * 
 * 
 * @author bromberg
 */
public class naiveBayes2C extends BayesianGraph {



	/**
	 * @param _name
	 * @param schema
	 * @param ess
	 */
	public naiveBayes2C(String _name, Schema schema, int ess, int index) {
		super(_name, schema, ess);
		buildStructure( index);
	}

	protected void buildStructure(int index)
	{
		if(index == 0){
			for(int i=0; i<size()-1; i++){
				addEdge(size()-1, i, false);
			}
			for(int i=0; i<size()-2; i++){
				addEdge(size()-2, i, false);
			}
		}
		else if(index == 1){
			for(int i=0; i<size()-2; i++){
				addEdge(size()-1, i, false);
			}
			for(int i=0; i<size()-2; i++){
				addEdge(size()-2, i, false);
			}
			addEdge(size()-2, size()-1);
		}
	}
	
	/**
	 * An input instance is composed by <x,c, C>.<!-- --> It 
	 * classifies according to max P(C|x,c) ALWAYS!.<!-- -->
	 * 
	 * It computes: 
	 * P(C|X,c) = P(C,X,c)/P(X,c) = \alpha x P(X|C,c) x P(c|C) x P(C), where
	 * \alpha is a normalization factor independent of the value of C.  
	 * 
	 */
	private double getClassProbability(Instance inst, int classIndex, int class2predict){
		Attribute classAttr = schema.getAttribute(class2predict);
		
		//Computing P(C)
		double P_C = getBNNode(class2predict).getTable().getProbability(inst);
		
		//Computing P(c|C)               
		double P_c_C = 	getBNNode(classIndex).getTable().getProbability(inst);
		
		//Computing Prod_i[ P(X_i|C,c) ]
		double Prod1=1;
		for(int i=0; i<inst.size()-2; i++){
			Prod1 *= getBNNode(i).getTable().getProbability(inst);
		}
		
		return P_C * P_c_C * Prod1;
	}
	public double [] distributionForInstance(Instance inst, int classIndex, int class2predict){
		Attribute classAttr = schema.getAttribute(class2predict);
		double [] dist = new double[classAttr.size()];
		
		Instance instaux = inst.makeCopy();
		//double sum = 0;
		double max = 0;
		int maxIndex = 0;
		for(int j=0; j<classAttr.size(); j++){
			instaux.setValue(classAttr.getValueName(j), class2predict);
			
			//It may happen that C2's value is missing, for which we need to de-marginalize.
			if(!inst.getValue(classIndex).equals(Instance.missingValue))
					dist[j] = getClassProbability(instaux, classIndex, class2predict);
			else{
				Attribute class2Attr = schema.getAttribute(classIndex);
				dist[j] = 0;
				for(int k=0; k<class2Attr.size(); k++){
					instaux.setValue(class2Attr.getValueName(k), classIndex);
					dist[j] += getClassProbability(instaux, classIndex, class2predict);;
				}
			}
		}
		//Normalize
		double norm = 0;
		for(int i=0; i<dist.length; i++) norm += dist[i];
		;
		//String out = "";
		for(int i=0; i<dist.length; i++) {
			dist[i] = dist[i]/norm;
		//	out += dist[i] + ",";
			
		}
		//System.out.println(out);
		for(int i=0; i<dist.length; i++) dist[i] = dist[i]/norm;
		
		return dist;
		
	}
	public int getMostProbableClassIndex(Instance inst, int classIndex, int class2predict){
		double [] dist = distributionForInstance(inst, classIndex, class2predict);
		
		double max = 0;
		int maxIndex = 0;
		for(int j=0; j<dist.length; j++){
			if(dist[j] > max) {
				max = dist[j];
				maxIndex = j;
			}
		}
		return maxIndex;
		
	}
	/**
	 * Classify the whole dataset for two classes, sets the classValue
	 * of classIndex class (index of the class in the schema, e.g. size()-1)
	 * to the most probable one.<!-- --> return the difference percentage.
	 */
	public double classifyDataset(Dataset data, int agentIndex)
	{
		int incorrect=0;
		int class2predict=0, classIndex=0;
		if(agentIndex == 0){
			classIndex = data.schema().size()-1;
			class2predict = data.schema().size()-2;
		}
		else if(agentIndex == 1){
			classIndex = data.schema().size()-2;
			class2predict = data.schema().size()-1;
		}
		Attribute classAttr = schema.getAttribute(class2predict);
		
		machineLearning.Datasets.Instance inst = data.getFirstInstance();
		
		while (inst != null)
		{
			String prev = inst.getValue(class2predict);
			
			int aux1 = getMostProbableClassIndex(inst, classIndex, class2predict);
			String aux2 = classAttr.getValueName(aux1);
			
			if(!aux2.equals(prev)) {
				incorrect++; 
			}
			
			inst.setValue(aux2, class2predict);
			
			inst = data.getNextInstance();
		}
		return (double)incorrect/(double)data.size();
	}
	
}
