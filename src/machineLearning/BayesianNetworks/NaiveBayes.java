/*
 * Created on Jul 5, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package machineLearning.BayesianNetworks;

import machineLearning.Datasets.*;

/**
 * @author bromberg
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class NaiveBayes extends BayesianGraph {


	/**
	 * @param _name
	 * @param schema
	 * @param ess
	 */
	public NaiveBayes(String _name, Schema schema, int ess) {
		super(_name, schema, ess);
		buildStructure();
	}

	//We assume the class is the last attribute.
	protected void buildStructure()
	{
			for(int i=0; i<size()-1; i++){
				addEdge(size()-1, i, false);
			}
	}
	
	/**
	 * An input instance is composed by <x,c, C>.<!-- --> It 
	 * classifies according to max P(C|x,c) ALWAYS!.<!-- -->
	 * 
	 * It computes: 
	 * P(C|X,c) = P(C,X,c)/P(X,c) = \alpha x P(X|C,c) x P(c|C) x P(C), where
	 * \alpha is a normalization factor independent of the value of C.  
	 * 
	 */
	private double getClassProbability(Instance inst){
		Attribute classAttr = schema.getAttribute(size()-1);
		
		//Computing P(C)
		double P_C = getBNNode(size()-1).getTable().getProbability(inst);
		
		//Computing Prod_i[ P(X_i|C,c) ]
		double Prod1=1;
		for(int i=0; i<inst.size()-1; i++){
			Prod1 *= getBNNode(i).getTable().getProbability(inst);
		}
		
		return P_C * Prod1;
	}
	public double [] distributionForInstance(Instance inst){
		Attribute classAttr = schema.getAttribute(size()-1);
		double [] dist = new double[classAttr.size()];
		
		Instance instaux = inst.makeCopy();
		//double sum = 0;
		double max = 0;
		int maxIndex = 0;
		for(int j=0; j<classAttr.size(); j++){
			instaux.setValue(classAttr.getValueName(j), size()-1);
			
			dist[j] = getClassProbability(instaux);
		}
		//Normalize
		double norm = 0;
		for(int i=0; i<dist.length; i++) norm += dist[i];
		;
		//String out = "";
//		for(int i=0; i<dist.length; i++) dist[i] = dist[i]/norm;
//		for(int i=0; i<dist.length; i++) dist[i] = dist[i]/norm;
		
		return dist;
		
	}
	public int getMostProbableClassIndex(Instance inst){
		double [] dist = distributionForInstance(inst);
		
		double max = 0;
		int maxIndex = 0;
		for(int j=0; j<dist.length; j++){
			if(dist[j] > max) {
				max = dist[j];
				maxIndex = j;
			}
		}
		return maxIndex;
		
	}
	/**
	 * Classify the whole dataset for two classes, sets the classValue
	 * of classIndex class (index of the class in the schema, e.g. size()-1)
	 * to the most probable one.<!-- --> return the difference percentage.
	 */
	public double classifyDataset(Dataset data)
	{
		int incorrect=0;
		Attribute classAttr = schema.getAttribute(size()-1);
		
		machineLearning.Datasets.Instance inst = data.getFirstInstance();
		
		while (inst != null)
		{
			String prev = inst.getValue(size()-1);
			
			int aux1 = getMostProbableClassIndex(inst);
			String aux2 = classAttr.getValueName(aux1);
			
			if(!aux2.equals(prev)) {
				incorrect++; 
			}
			
			inst.setValue(aux2, size()-1);
			
			inst = data.getNextInstance();
		}
		return (double)incorrect/(double)data.size();
	}
	
}
