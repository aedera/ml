package machineLearning.BayesianNetworks;

import machineLearning.Datasets.*;
import java.util.*;
import java.io.*;


/**
 * Class cptSet
 *
 * Implements a set of conditional probability tables over the
 * Vector class to answer questions about the cpts required by
 * an incremental learning procedure (specifically, that of
 * Friedman and Goldszmidt (1994) with Lam and Bacchus' (1994)
 * score updating).  Uses the Dataset object to track attribute info.
 *
 * @author Brian Patterson
*/

public class cptSet extends Vector {
    protected Schema attributes;

    protected dataSourceARTF datasource;

//    protected Dataset data;
/**
* @return the Schema of the cptSet (obtained from dataSource on creation).
*/
 //   public Schema getSchema(){return attributes;};
/*---------------------------------------------------*/
/**
 * Initializes what is in the complete attribute set and adds marginal tables
 * @param attr_names = array of initial attribute names
 * @param num_cat = number of catagories for each of the attributes
 * @param cat = string of the catagory labels in comma-and-double-quote delim (ie "x","y",...) format
 * @param ds = the data Source that feeds this instance of cptSet.
 */
	public void initializeAttributes(String[] attr_names, int[] num_cat, String[] cat, int ess, dataSourceARTF _ds) {
          datasource= _ds;
          attributes = datasource.schema();
          String[] temp;
          Attribute temp_attr;
          for( int i = 0; i < attr_names.length; i++) {
            if( num_cat[i] > 0 ) {
              temp_attr = new Attribute(attr_names[i], i);
              temp = cat[i].split("\"");
              for( int j = 1; j < temp.length; j=j+2) {
                temp_attr.addValue(temp[j]);
              }
              attributes.addAttribute(temp_attr);
            }
          }
          condProbabilityTable cpt;
          for( int i = 0; i < attributes.numberOfAttributes(); i++) {
            //Also adds marginal tables:
            cpt = addCPT(i, new int[]{}, ess);
            cpt.counter = Integer.MAX_VALUE;
          }
        }

        /*---------------------------------------------------*/
/**
 * Creates an empty CPT for P( attr | c_attr) in the set.
 * @param attr - name of the attribute were conditioning from
 * @param c_attr - name of the attributes we're conditioning on
 * @return The CPT added (as a condProbabilityTable)
 */
 	public condProbabilityTable addCPT( String attr, String[] c_attr, int ess) {
 		//Find the indexes of the requested attributes
 		int[] attr_idx_arr = nameToAttrIndex(new String[]{ attr });
 		int attr_idx = attr_idx_arr[0];
 		int[] c_attr_idx = nameToAttrIndex(c_attr);

 		return addCPT(attr_idx, c_attr_idx, ess);
 	}


/*---------------------------------------------------*/
/**
 * Creates an empty CPT for P( attr | c_attr) in the set.
 * @param attr - index of the attribute were conditioning from
 * @param c_attr - index of the attributes we're conditioning on
 * @return The CPT added (as a condProbabilityTable)
 */
	public condProbabilityTable addCPT( int attr, int[] c_attr, int ess) {
		//See if the CPT is already present
		condProbabilityTable cpt = findCPT(attr, c_attr);
                if( cpt == null) {
			//Generate the cpt using the attributes extracted
			cpt = new condProbabilityTable(datasource.schema(), attr, c_attr, ess);
			add(cpt);
			cpt.counter = 0;
		}
		cpt.counter++;
		return cpt;
	}

/*---------------------------------------------------*/
/**
 * Deletes the CPT for P( attr | c_attr) from the set.
 * @param attr - name of the attribute conditioned from
 * @param c_attr - name of the attributes conditioned on
 * @return The CPT removed (as a condProbabilityTable)
 */
	public condProbabilityTable removeCPT( String attr, String[] c_attr) {
		//Find the indexes of the requested attributes
 		int[] attr_idx_arr = nameToAttrIndex(new String[]{ attr });
 		int attr_idx = attr_idx_arr[0];
 		int[] c_attr_idx = nameToAttrIndex(c_attr);

		return removeCPT(attr_idx, c_attr_idx);
	}


/*---------------------------------------------------*/
/**
 * Deletes the CPT for P( attr | c_attr) from the set.
 * @param attr - index of the attribute conditioned from
 * @param c_attr - index of the attributes conditioned on
 * @return The CPT removed (as a condProbabilityTable)
 */
        public condProbabilityTable removeCPT(condProbabilityTable cpt)
       {
         return removeCPT(cpt.m_attribute_index, cpt.m_condAttributeSet);
       }
	public condProbabilityTable removeCPT( int attr, int[] c_attr) {
		//Find the CPT with the matching attributes
		condProbabilityTable cpt = findCPT(attr, c_attr);
        if(cpt == null) return null;
		cpt.counter--;
		if( cpt.counter < 1){
			removeElement(cpt);
		}
		return cpt;
	}

/*---------------------------------------------------*/
/**
 * Finds the CPT for P( attr | c_attr), returns it (null if not found)
 * @param attr - name of the attribute conditioned from
 * @param c_attr - name of the attributes conditioned on
 * @return The CPT found (as a condProbabilityTable), null if not found
 */
 	public condProbabilityTable findCPT( int attr, int[] c_attr) {
 		condProbabilityTable cpt;

 		//Find the right CPT
 		boolean found, found_c;
 		for(int i = 0; i < size(); i++) {
 			cpt = (condProbabilityTable)elementAt(i);
 			if( (cpt.m_attribute_index == attr) && (cpt.m_condAttributeSet.length == c_attr.length) ) {
 				found = true;
 				for(int j = 0; j < c_attr.length; j++) {
 					found_c = false;
 					for(int k = 0; k < cpt.m_condAttributeSet.length; k++) {
 						if( cpt.m_condAttributeSet[k] == c_attr[j] ) found_c = true;
 					}
 					if( !found_c ) found = false;
 				}
 				if( found ) {
 					return cpt;
 				}
 			}
 		}
 		return null; //Could not find specified CPT
 	}

/*---------------------------------------------------*/
/**
 * Reads buffersize records from the database, updates this cptSet appropriately
 *  (returns number of records found)
 * @param buffersize - number of records to update the cptSet with
 * @return The actual number of data instances read (usually buffersize unless EOF from dataSource)
 */
 public int readNextDataFrom(int buffersize, char separator) {
	datasource.read(buffersize, separator);
	if( buffersize > datasource.dataBuffer().size()) buffersize = datasource.dataBuffer().size();
	if( buffersize == 0) return 0;

//	lastData = data; //Don't remember empty returns as our last data
	condProbabilityTable cpt;
	//Updates CPT i with all buffersize data units
	for( int i = 0; i < size(); i++) {
		cpt = (condProbabilityTable)elementAt(i);
		for( int j = 0; j < buffersize; j++ ) {
			cpt.updateInstance(datasource.dataBuffer().getInstance(j));
		}
	}
	return buffersize;
 }

/*---------------------------------------------------*/
/**
 * Uses records from the database to update this cpt
 * @return The cpt updated
 */
 public condProbabilityTable updateCPT(condProbabilityTable cpt) {
	//Updates CPT with all data units
        if(datasource.dataBuffer() == null) return cpt;
	int buffersize = datasource.dataBuffer().size();
	for( int j = 0; j < buffersize; j++ ) {
		cpt.updateInstance(datasource.dataBuffer().getInstance(j));
	}
	return cpt;
 }

/*---------------------------------------------------*/
/**
 * Reads the entire database, updates all CPTs in the data set
 * @return Number of records found
 */
 public int readAllDataFrom(char separator) {
	datasource.readAll(separator);
	int buffersize = datasource.dataBuffer().size();
	if( buffersize == 0) return buffersize;

	condProbabilityTable cpt;
	for( int i = 0; i < size(); i++) {
		cpt = (condProbabilityTable)elementAt(i);
		//Updates CPT i with all the data units
		for( int j = 0; j < buffersize; j++ ) {
			cpt.updateInstance(datasource.dataBuffer().getInstance(j));
		}
	}
	return buffersize;
 }

/*---------------------------------------------------*/
/**
 * Returns the index of the given attribute name
 * @param attr - name of an attribute
 * @return The index of the named attribute.
 */
 	private int attrToIndex(String attr) throws Exception {
		for( int i = 0; i < attributes.numberOfAttributes(); i++) {
			if( attr.equalsIgnoreCase(attributes.getAttribute(i).name) ) return i;
		}
		throw new Exception();
	}

/*---------------------------------------------------*/
/**
 * Returns the attribute at the given index, accessor function
 * @param attrIndex - index of the attribute requested
 * @return Attribute object.
 */
	private Attribute getAttribute(int attrIndex) {
		return attributes.getAttribute(attrIndex);
	}

/*---------------------------------------------------*/
/**
 * @param attr - name of the attribute requested
 * @return The Attribute with the given name
 */
	public Attribute getAttribute(String attr) {
		try {
			return (attributes.getAttribute(attrToIndex(attr)));
		} catch( Exception e ) {
			System.out.println("Cannot find attribute " + attr + ".");
			e.printStackTrace();
		}
		System.out.println("Cannot find attribute " + attr + ".");
		return null;
	}

/*---------------------------------------------------*/
/**
 * Converts an array of strings that are names of attributes to the attribute's indices in the Dataset
 * @param attr - name of the attributes
 * @return Array of integers
 */
 	public int[] nameToAttrIndex( String[] attr) {
		int[] attr_idx = new int[attr.length];
		try {
			for(int i = 0; i < attr.length; i++) {
				attr_idx[i] = attrToIndex(attr[i]);
			}
		} catch( Exception e ) {
			System.out.println("One or more of the named attributes does not appear in the data set.");
			e.printStackTrace();
		}
		return attr_idx;
	}

/*---------------------------------------------------*/
/**
 * Prints all the CPTs currently in the cptSet (useful for debugging)
 */
 	public void printAllCPTs() {
 		System.out.print( "CPTs over the following attributes: " );
 		for( int i = 0; i < attributes.numberOfAttributes(); i++) {
 			if( i < attributes.numberOfAttributes() - 1 ) {
	 			System.out.print( (attributes.getAttribute(i)).name + ", " );
 			} else {
 				System.out.println( (attributes.getAttribute(i)).name);
 			}
 		}
 		System.out.println( "Possible attribute values: " );
 		for( int i = 0; i < attributes.numberOfAttributes(); i++) {
 			System.out.println( (attributes.getAttribute(i)).name + ": " + (attributes.getAttribute(i)) );
 		}

 		System.out.println( "\nCPTs: " );
 		condProbabilityTable cpt;
 		for( int i = 0; i < size(); i++) {
 			cpt = (condProbabilityTable)elementAt(i);
 			if( cpt.m_condAttributeSet.length == 0) {
 				System.out.println( "CPT " + i + ": P(" + (attributes.getAttribute(cpt.m_attribute_index)).name + ")" );
 			} else {
				System.out.print( "CPT " + i + ": P(" );
				System.out.print( (attributes.getAttribute(cpt.m_attribute_index)).name + " | " );
				for( int j = 0; j < cpt.m_condAttributeSet.length; j++) {
					if( j < cpt.m_condAttributeSet.length - 1) {
						System.out.print( (attributes.getAttribute(cpt.m_condAttributeSet[j])).name + ", " );
					} else {
						System.out.println( (attributes.getAttribute(cpt.m_condAttributeSet[j])).name + ")" );
					}
				}
			}
 			System.out.println();
 		}
 	}

        /** Retrieves a string with the Ns for each CPT.<!-- --> The N field contains the number of rows used
         * for computing the cpt so far (N is incremented by by each call of updateInstace.)*/
         public String getNString()
        {
          String out = "";
          condProbabilityTable cpt;
          for( int i = 0; i < size(); i++) {
                  cpt = (condProbabilityTable)elementAt(i);
                  out += cpt.N + " ";
           }
          return out;
        }
}





