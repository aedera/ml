package machineLearning.BayesianNetworks;

/**
 * Class: BNNode (Bayesian Network Node)
 * Extends Graph Node with extra functionality to maintain condicional Probability Tables.
 *
 * @author Facundo Bromberg
 * @version 1.0 Final Project CS572 AI, Fall 2002.
 * @date Nov 2002
 */

import java.util.Vector;

import machineLearning.Datasets.Attribute;
import machineLearning.Datasets.Dataset;
import machineLearning.Datasets.Instance;
import machineLearning.Datasets.Schema;
import machineLearning.Datasets.discreteProbabilityDistribution;
import machineLearning.Graphs.GraphicalModelNode;
import machineLearning.Utils.Utils;


/**
 * Extends a graph node and add also functionality of Attribute; recall that a Random Variable is a node in the
 * graph and also an attribute in the datset; This class links these two aspects of a random variable<!-- -->
 * This node maintains its own adjacency list (Vector children) and a parents Vector <!-- -->
 * The calling class should maintain these vectors; i.e. Taking care that if u is a child of v,
 * then v appears in the parent vector of u.<!-- --> Also, it maintains the conditional Probability Tables for the
 * node, which are updated by means of two possible data sources: a Dataset object or through S, the sufficient
 * statistics container which in turns is in contact of some external datasource (probably a database).
 *
 * @author Facundo Bromberg
 * @version 1.0 Final Project CS572 AI, Fall 2002.
 * @date Nov 2002
 */
public class BNNode extends GraphicalModelNode
{

    /**
     * Conditional Probability Table of the node with its parents.
     */
    protected condProbabilityTable table;

    /** Used in try transpose.*/
    boolean tryTranposeVisited;
    /**
     * Index, of the variable represented in the node, in the attributes of the dataset.<!-- -->
     * This way we connect the BNNode and the Attribute.<!-- -->
     * Recall that  both represents different aspects of the same random variable,
     *
     */
    //protected int Attribute_index;

    /** We keep a local vector of the value names of the associated attribute.  */
    protected Vector valuesNames;

    /** Hold the equivalent sample size */
    protected int m_equivalent_sample_size;

    /** lambda for use in Inference.*/
    protected discreteProbabilityDistribution lambda;

    /** pi for use in Inference.*/
    protected discreteProbabilityDistribution pi;

    /** Belief distribution for use in Inference.*/
    protected discreteProbabilityDistribution BEL;

    /** Stores the lambda messages sent by its children.*/
    protected  discreteProbabilityDistribution [] lambdaMsgs;

    /** Stores the pi messages sent by its parents.*/
    protected discreteProbabilityDistribution [] piMsgs;

    /** Produce extra output useful for debugging.*/
    static boolean verboseOutput = true;

    /** A reference to the sufficient statistics container S.<!-- --> If it's null, then it is assumed the data comes
     * from dataset, otherwise S takes care of contacting the datasource.  */
    protected cptSet S;

    /** Keeps the value of the description length, to avoid repeated computations of it, works in combination
     * with table.notUsedSinceLastUpdate . */
    public double DL;

/**
   *
   * Constructor for a node with probability estimation capabilites
   */
  public BNNode(Schema schema, int attr_index, int ess)
  {
      super(schema.getAttribute(attr_index).name);
      initNode(schema, attr_index, ess, null, null);
  }

  /**
     *
     * Constructor for a node with probability estimation capabilites
     */
    public BNNode(Schema schema, int attr_index, int ess, cptSet _S)
    {
	super(schema.getAttribute(attr_index).name);
	initNode(schema, attr_index, ess, _S, null);
    }

    public BNNode(Attribute attr){
		super(attr);
	}
  /**
   * Constructor for a node without probability estimation capabilites
   */
  public BNNode(condProbabilityTable cpt, Schema schema)
  {
      super("");
      initNode(schema, cpt.m_attribute_index, cpt.m_equivalent_sample_size, null, cpt);
  }

  /**
   * Helping procedure for the constructor which actually initialize the node.
    * @param S Sufficient Statistics Container S.<!-- --> The node requests him the creation of a new table.<!-- -->
    * On the next data read, the new table will be considered and grown with new data.
    */
  private void initNode(Schema schema, int attr_index, int ess, cptSet _S, condProbabilityTable cpt)
  {
      DL = Double.MAX_VALUE;

      m_equivalent_sample_size = ess;

      attr = schema.getAttribute(attr_index);
      //Attribute_index = attr_index;
      S = _S;

      valuesNames = new Vector(1,2);
      //Copy valueNames vector;
      for(int i=0;i<attr.size(); i++)
      {
	  valuesNames.addElement(attr.getValueName(i));
      }
      //Creates the condProbabilityTable
      if(cpt!= null) table = cpt;
      else if(S == null) table = new condProbabilityTable(schema, attr.index(), getParentAttrIndexList(), ess);
           else table = S.addCPT(attr.index(), getParentAttrIndexList(),  ess);

      lambda = null;
      pi = null;
      BEL = null;
  }

  /** Creates a new node with equal characteristics. */
  public BNNode makeSimilar()
  {
      return new BNNode(table.schema, attr.index(), m_equivalent_sample_size, S);
  }


   /** Creates a new node with equal characteristics. <!-- --> It does not copy the children nor the parent
    * vector. */
   public void makeCopyFrom(BNNode old)
    {
      super.makeCopyFrom(old);
//        BNNode newNode = new BNNode(table.data, Attribute_index, m_equivalent_sample_size, S);
      table = old.table;
      attr = old.attr;
      lambda = null;
      pi = null;
      BEL = null;
      lambdaMsgs = null;
      piMsgs = null;
      S = old.S;
      DL = old.DL;
    }

  /**
   * sets the value of verbose Output, which will provoke exactly that, verbose output.
   */
  public void setVerboseOutput(boolean t)
  {
      verboseOutput = t;
  }

  /**
   * Retrieves the BNNode stored at the index-th position in the parents vector
   */
  public int numValues() { return valuesNames.size(); }

  /**
   * Retrieves the BNNode stored at the index-th position in the parents vector
   */
  public BNNode getBNParent(int index) { return (BNNode) parents.elementAt(index); }

  /**
   * Retrieves the BNNode stored at the index-th position in the children vector
   */
  public BNNode getBNChild(int index) { return (BNNode) children.get(index); }


  /**
   * Returns the index-th value.
   */
  public String getValue(int index) {return (String) valuesNames.elementAt(index);}

  /**
   * Retruns the index of the attribute associated to this node.
   */
  public int getAttributeIndex() {return attr.index();}

  /**
   * Retrieves the conditional probability table of the node.
   */
  public condProbabilityTable getTable()
  {
	 return table;
  }

  /**
   * Sets the conditional probability table of the node.
   */
  public void setTable(condProbabilityTable  cpt)
  {
    table = cpt;
  }

  /**
   * Retrieves an array with the attribute indexes of the parents.
   */
  public int [] getParentAttrIndexList()
  {
//    if(table != null) return table.m_condAttributeSet;
//    else {
      int[] intParents = new int[inDegree()];

      for (int i = 0; i < inDegree(); i++) {
        BNNode parent = (BNNode) getParent(i);
        intParents[i] = parent.getAttributeIndex();
      }
      return intParents;
    }
  //}

  /**
   * Used after any change produced on the graph (i.e. addition or remotion of edges).<!-- -->
   * It replaces the old table with a new one in accordance to the new dimensions required
   * (ie new parents added), at this point it is necessary to recompute the descriptionLength.
   *
   */
  public void replaceTable(Schema schema, cptSet S)
  {
    replaceTable(schema, S, true);
  }

  /** Same as above but with the option of not removing the table from S.*/
  public void replaceTable(Schema schema, cptSet S, boolean removeTable)
  {
      DL = Double.MAX_VALUE;  //when MAX_VALUE, descriptionLength recomputes.

      if(S == null){
      	table = null;//frees the table from memory;
      	table = new condProbabilityTable(schema, attr.index(), getParentAttrIndexList(), m_equivalent_sample_size);
      }
      else {
        //The following is wrong, it should remove the table BEFORE the change (either removeEdge or addEdge)
//	  if(removeTable) S.removeCPT(Attribute_index, getParentAttrIndexList());
          table = S.addCPT(attr.index(), getParentAttrIndexList(), m_equivalent_sample_size);
      }
  }

  /**
   * Used for externally attaching the table.
   */
 /* public void setTable(condProbabilityTable cpt)
  {
      table = null;
      table = cpt;
  }

  /**
   * replaceTable only re-allocates space for the new table.<1-- --> This method instead re-computes the values
   * (counts)for the table, given the Dataset stored in data;
   *
   */
  public void computeTable(Dataset data)
  {
  	// Compute counts
  	Instance inst = data.getFirstInstance();
  	
  	while (inst != null) {
  		
  		table.updateInstance(inst);
  		inst = data.getNextInstance();
  	}
  	
  }
/*---------------------------------------------------*/
    /**
   * Display a representation of this probabilityTable
   */
  public String toString(int tab) {

    String result = "\n\nProbability Tables of node:" + name + "\n\n";

    String line = "";
    String aux;
    for(int j=0; j<parents.size() ;j++)
    {
	aux = getBNParent(j).name;
	if(aux.length() > tab) aux = aux.substring(0, tab);
	else {
	    for(int i=0; i< tab - aux.length(); i++) aux += " ";
	}
	line += aux + " | ";
    }
    aux = name;
    if(aux.length() > tab) aux = aux.substring(0, tab);
    else {
	for(int i=0; i< tab - aux.length(); i++) aux += " ";
    }
    line += "|" + aux + "||" + "  P\n";

    result += line;



    //int [] coord = new int[parents.size()];
    //for(int i= 0; i<coord.length; i++) coord[i] = 0;
    int [] coord = table.getInitialCoord();
    while(coord != null)
    {
	for(int i=0; i<valuesNames.size() ; i++)
	{
	    result += getLine(coord, i, tab);
	}
	coord = table.getNextCoord(coord);
    }
    return result;
  }

  /**
   * Builds one line of values for the table.
   */
  private String getLine(int [] coord, int valueIndex, int tab)
  {
      String line = "";
      String aux;
      for(int i=0; i<parents.size(); i++)
      {
	  aux = getBNParent(i).getValue(coord[i]);
	  if(aux.length() > tab) aux = aux.substring(0, tab);
	  else {
	      for(int j=0; j< tab - aux.length(); j++) aux += " ";
	  }


	  line += aux + " | ";
      }
      aux = getValue(valueIndex);
      if(aux.length() > tab) aux = aux.substring(0, tab);
      else {
	  for(int i=0; i< tab - aux.length()+8; i++) aux += " ";
      }

      line += "|" + aux + "||"  ;

      aux = Utils.myDoubleToString(table.getProbability(coord, valueIndex), 2);

      line += aux +"\n";
      return line;
  }


  /***********************************************************/
  /*              LEARING METHODS                          */
  /***********************************************************/
   /**
   * Procedure that returns a copy of the BNNode but with the parent given as input.
   */
  public BNNode getPAIR(BNNode parent, cptSet S)
  {
      int [] parentIndex = new int[1];

      BNNode newnode = makeSimilar();
      BNNode parentNode = parent.makeSimilar();

      newnode.addParent(parentNode);
      parentIndex[0] = parentNode.attr.index();

      //newnode.replaceTable(S.addCPT(table.m_attribute_index, parentIndex, m_equivalent_sample_size), S);
      newnode.replaceTable(table.schema, S);

      return newnode;
  }

  /**
   * See Lam&Bacchus 93 for details.<!-- --> When this value needs to be recomputed is mananged internally
   * by condProbabilityTable and BNNode through the value of DL and notUsedSinceLAstUpdate.<!-- --> The first
   * one is set to Double.MAX_VALUE whenevery the BNNode's table changed (adding/deleting/transposing an edge).
   * <!-- --> The second is set to true whenever the table changed because of new data coming in (more specifically
   * in condProbabilityTable.updateInstance).
   */
  public double descriptionLength(int sizeOfNet){return descriptionLength(false, sizeOfNet);};

  public double descriptionLength(boolean forceComputation, int sizeOfNet)
  {
    //When equal to MAX_VALUE, recomputes. This is set up by replaceTable.
    if (!table.notUsedSinceLastUpdate && DL != Double.MAX_VALUE && !forceComputation) {
      return DL;
    }
    else {
      table.notUsedSinceLastUpdate = false;

      int sumOfCounts = 0;

      if(table.N == 0)
      {
        //First try to obtain some info from the last read;
        S.updateCPT(table);
        if(table.N == 0) {
          return DL = Double.MAX_VALUE;
        }
      }
      else DL = parents.size() * Math.log(sizeOfNet);
      int product = (numValues() - 1) * Utils.d; //d = 64 since it takes 8bytes to represent a double in Java.

      //Product of #(values) of the parents
      for (int i = 0; i < parents.size(); i++) {
        BNNode par = (BNNode) getParent(i);
        product *= par.numValues();
      }

      DL += product;

      //Computation of W for the node, given it's parents. Note W=0 if no parents.
      double W = 0;
      int[] coord = table.getInitialCoord();

      double joint, cond, P_X, log; //auxiliary variables

      do{
        for (int i = 0; i < numValues(); i++) {
          cond = table.getProbability(coord, i);
          condProbabilityTable prior = S.findCPT(attr.index(), new int[] {});
          P_X = prior.getProbability(null, i);

          log = Math.log(cond / P_X);

          joint = table.getJointProbability(coord, i);
          sumOfCounts += table.getN(coord, i);

          W += joint * log; //i = valueIndexx;
        }
        coord = table.getNextCoord(coord);
      }while(coord != null);

      DL -= table.N * W;
      DL = DL/sumOfCounts;
    }
    return DL;
  }


  /***********************************************************/
  /*              INFERENCE METHODS                          */
  /***********************************************************/

  /**
   * getBelief returns BEL
   */
  public discreteProbabilityDistribution getBelief()
  {
      return BEL;
  }
  /**
   * Returns the most probable value according to BELIEF.
   */
  public String getMostProbableValue()
  {
      if(BEL != null) return getValue(BEL.getMostProbableIndex());
      else return "Belief was not yet computed.";
  }

  /**
   * This means that the node's value is assigned to the index-th value in the value list; ie X=x_index
   */
  public void setEvidence(int index)
  {
      lambda = new discreteProbabilityDistribution(numValues());

      for(int i=0 ; i<numValues() ;i++) lambda.setProbability(i, 0);
      lambda.setProbability(index, 1);
  }

  /**
   * Set evidence through the lambda vector (see p.181 PRIS).<!-- -->
   */
  public void setEvidence(discreteProbabilityDistribution lam)
  {
      lambda = lam.makeCopy();

      if(verboseOutput)
      {
	  System.out.println("Evidence set for Node " + name + ":" + lambda.toString());
      }
  }

  /**
   * Resets the node to equilibrium state.<!-- --> It leaves the node ready for the next belief propagation
   */
  public void resetInferenceInfo()
  {
      lambda = null;
      pi = null;
      BEL = null;

      lambdaMsgs = null;
      piMsgs = null;

      lambdaMsgs = new discreteProbabilityDistribution[outDegree()];
      piMsgs = new discreteProbabilityDistribution[inDegree()];

      for(int i=0; i<outDegree(); i++) lambdaMsgs[i] = null;
      for(int i=0; i<inDegree(); i++) piMsgs[i] = null;

      if(verboseOutput)
      {
	  System.out.println("Node " + name + " reseted");
      }
  }


  /**
   * Computes the belief given the current state of the network and the new evidence arrived.<!-- -->
   * Note that this method is recursive through the method requesteLambdaMsg in computeLambda, which in
   * turns called computeLambda for each of the children.<!-- -->
   * This means that once we call computeLambda for a node X, this will trigger
   * the computation of the lambdas of  all the nodes in the subtree rooted at X.<!-- -->
   * Moreover, the requestLambdaMsg will in turn request for Pi Messages to its parent in other subtrees, propagating
   * the computation of Beliefs across those nodes.<!-- --> The propagation triggered by this call of computeBelief
   * will not be spread along all the nodes of the poly-tree, reason why we have to call explicitelly for
   * the computation of belief in those nodes.
   */
  public void computeBelief()
  {
  	if(BEL != null) return;
  	else BEL = new discreteProbabilityDistribution(numValues());
  	
  	if(verboseOutput)
  	{
  		System.out.println("Computing BELIEF of Node " + name);
  	}
  	computeLambda();
  	computePi();
  	BEL.copyFrom(lambda);
  	BEL = BEL.product(pi);
  	BEL.normalize();
  	if(verboseOutput)
  	{
  		System.out.println("New BELIEF of node " + name + " is:" + BEL.toString());
  	}
  }

  /**
   * Computes the current lambda given the current state of the network and the new evidence arrived.<!-- -->
   * Note that this method is recursive through the method requesteLambdaMsg as explained in computeBelief.
   */
  private void computeLambda()
  {
      if(lambda != null) return;
      else lambda = new discreteProbabilityDistribution(numValues());

      if(verboseOutput)
      {
	  System.out.println("Computing Lambda of Node " + name);
      }
      lambda.setAllProbabilities(1);

      //The termination condition for the recursive calls to requesteLambdaMsg is implicit. If there is
      //no childs, it will return a lambda with all 1's, exactly what it should be for a leaf.
      for(int i=0 ;i<outDegree() ;i++)
      {
	  if(lambdaMsgs[i] == null)
	  {
	       BNNode child = getBNChild(i);
	       lambdaMsgs[i] = child.requestLambdaMsg(this);

          }
	  lambda = lambda.product(lambdaMsgs[i]);
      }
      if(verboseOutput)
      {
	  if(outDegree() == 0)
	  {
	      System.out.println("Node " + name + " is a terminal leaf node. Lambda set to all 1's:  " + lambda.toString());
	  }
	  else System.out.println("New Lambda of Node " + name + ":" + lambda.toString());
      }
  }

  /**
   * Computes the lambda message to be sent to the callingParent.<!-- -->
   */
  public discreteProbabilityDistribution requestLambdaMsg(BNNode callingParent)
  {
      return computeLambdaMsg(parents.indexOf(callingParent));
  }

  /**
   * Computes the lambda msgs to send to the parent parentIndex.
   */
  private discreteProbabilityDistribution computeLambdaMsg(int parentIndex)
  {
      computeLambda();  //This will trigger a recursive call to requesteLambdaMsg.

      if(verboseOutput)
      {
	  System.out.println("Computing Lambda Message from Node " + name + " to Node " + getBNParent(parentIndex).name);
      }
      //Request of PiMsgs of all the parents except the parentIndex-th one.
      //In the same loop we compute the product of all this vectors.
//      discreteProbabilityDistribution piProd = new discreteProbabilityDistribution(numValues());
  //    piProd.setAllProbabilities(1);
      for(int i=0 ;i<inDegree() ;i++)
      {
	  if(i == parentIndex) continue;

	  if(piMsgs[i] == null)
	  {
	      BNNode parent = getBNParent(i);
	      piMsgs[i] = parent.requestPiMsg(this);
	  }
//	  piProd = piProd.product(piMsgs[i]);
      }

      //Computation of lambda msg
      discreteProbabilityDistribution newMsg = new discreteProbabilityDistribution(getBNParent(parentIndex).numValues());

      for(int j=0; j<getBNParent(parentIndex).numValues(); j++)
      {
	  double val = 0;
	  for(int x=0; x<numValues(); x++)
	  {
	      int [] coord = table.getInitialCoord();
	      if(coord != null)coord[parentIndex] = j;
	      double val2 = 0;
	      while(coord != null)
	      {
		  double val3 = table.getProbability(coord, x);
		  for(int k=0; k<coord.length; k++)
		  {
		      if(k == parentIndex) continue;
		      val3 *= piMsgs[k].getProbability(coord[k]);
		  }
		  //gets the next coordinate but fixing the parentIntex-th coordinate.
		  coord = table.getNextCoord(coord, parentIndex);
		  val2 = val3;
	      }
	      val += lambda.getProbability(x) * val2;
	  }
	  newMsg.setProbability(j, val);
      }
      if(verboseOutput)
      {
	  System.out.println("New Lambda Message from Node " + name + " to Node " + getBNParent(parentIndex).name + " is: "+ newMsg.toString());
      }
      return newMsg;
  }

  /**
   * Computes Pi of the node for the current state of the network.<!-- --> Note that it request pi messages
   * to ALL the parents, which triggers a belief computation across many other nodes in the network.
   */
  private void computePi()
  {
      if(pi != null) return;
      else pi = new discreteProbabilityDistribution(numValues());

      if(verboseOutput)
      {
	  System.out.println("Computing Pi of Node " + name);
      }
      //termination condition, set for the root node: pi(x) = P(x), the prior probability of node X.
      if(inDegree() == 0)
      {
      	for(int i=0; i<numValues() ;i++)
      	{
	      pi.setProbability(i, table.getProbability(null, i));
      	}
	  if(verboseOutput)
	  {
	      System.out.println("Node " + name + " is a terminal root node. Pi set to the prior probability of node:  " + pi.toString());
	  }
	  return;
      }

      //If it's not a root node.
//      pi.setAllProbabilities(1);

      for(int i=0 ;i<inDegree() ;i++)
      {
	  if(piMsgs[i] == null)
	  {
	       BNNode parent = getBNParent(i);
	       piMsgs[i] = parent.requestPiMsg(this);

	  }
//	  pi = pi.product(piMsgs[i]);
      }
      //
      for(int x=0; x<numValues(); x++)
      {
	  int [] coord = table.getInitialCoord();
	  double val = 0;
	  while(coord != null)
	  {
	      double val3 = table.getProbability(coord, x);
	      for(int i=0; i<coord.length; i++)
	      {
		  val3 *= piMsgs[i].getProbability(coord[i]);
	      }
	      val += val3;
	      coord = table.getNextCoord(coord);
	  }
	  pi.setProbability(x, val);
      }
      pi.normalize();
      if(verboseOutput)
      {
	  System.out.println("New Pi of Node " + name + ":" + pi.toString());
      }
  }

  /**
   * computes the pi message from the current node to the calling Child who requested it.
   */
  public discreteProbabilityDistribution requestPiMsg(BNNode callingChild)
  {
      return computePiMsg(children.indexOf(callingChild));
  }

  /**
   * Computes the pi msgs to send to the child childrenIndex.
   */
  private discreteProbabilityDistribution computePiMsg(int childIndex)
  {
      computeBelief();  //This will trigger a recursive call to requesteLambdaMsg and requestPiMsgs.

      if(verboseOutput)
      {
	  System.out.println("Computing Pi Message from Node " + name + " to Node " + getBNChild(childIndex).name);
      }

      //At this point then, we should have the lambdaMsgs from all the childs.
      discreteProbabilityDistribution newMsg = new discreteProbabilityDistribution(numValues());

      for(int x=0; x<numValues(); x++)
      {
	  if(lambdaMsgs[childIndex] == null)
	  {
	       BNNode child = getBNChild(childIndex);
	       lambdaMsgs[childIndex] = child.requestLambdaMsg(this);

	  }
	  newMsg.setProbability(x, BEL.getProbability(x)/ lambdaMsgs[childIndex].getProbability(x) );
      }
      newMsg.normalize();
      if(verboseOutput)
      {
	  System.out.println("New Pi Message from Node " + name + " to Node " + getBNChild(childIndex).name + " is: "+ newMsg.toString());
      }
      return newMsg;
  }
}

