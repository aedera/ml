package machineLearning.BayesianNetworks;

/*
/*
 *    BayesPolyTree.java
 *    @author Facundo Bromberg. 2002.
 *
 *
 */

//import weka.core.*;

import java.io.*;
import java.util.Vector;
import java.util.Random;

import machineLearning.Graphs.*;
import machineLearning.Datasets.*;
import machineLearning.Utils.*;

/**
 * Class derived from Graph (a DAG without cycles).<!-- --> It adds some functionalities relevant to
 * Bayesian Graphs.<!-- --> Also, it interprets its nodes as BNNodes.<!-- --> If S is equal to null (set at construction time)
 * it uses Datset from dataset package to populate the graph and the conditional proability tables.
 * Otherwise, is S in charge of managing the tables and getting the data externaly to update them (e.g. when
 * the datasource is the same for many number of bayesian networks, like in final project CS561).
 *
 * @author Facundo Bromberg
 * @version 1.0 Final Project CS572 AI, Fall 2002.
 * @version 1.1 Final Project CS561 AI, Spring 2003.
 * @version 1.2 MAL, Oct 2003.
 */
public class BayesianGraph extends Graph
{
    /** Name. */
    protected String name;

    /** A reference to the dataset used to generate the graph and conditional prob tables. */
    protected Dataset data;

    /** A reference to the schema */
    protected Schema schema;

    /** A reference to the sufficient statistics container S. */
    cptSet S;

    /** Used for transpose commitment.*/
    private int [] targetCond = null, sourceCond = null;

    /** Used for transpose commitment.*/
    private int auxSource, sourceIndex;

    /** Used for transpose commitment.*/
    private int auxTarget, targetIndex;


    /*-------------------------------*/
      public BayesianGraph(){super();};
   
      public BayesianGraph(BayesianGraph G){super(G);   };
     
  /*-------------------------------*/
  /**
   * Initializes the Bayesian Graph without probability estimation capabilities.
   *
   * @param inst Dataset used to populate the conditional probability tables.
   * @param ess equivalent sample size.
   */
  public BayesianGraph(String _name, Dataset _data, Schema schema )
  {
      initTree(_name, _data, schema,  -1, null);
  }

  /*-------------------------------*/
  /**
   * Initializes the Bayesian Graph with probability estimation capabilities.
   *
   * @param inst Dataset used to populate the conditional probability tables.
   * @param ess equivalent sample size.
   */
  public BayesianGraph(String _name, Dataset _data, Schema schema, int ess)
  {
      initTree(_name, _data, schema, ess, null);
  }

  /*-------------------------------*/
  /**
   * Initializes the Bayesian Graph with probability estimation capabilities.
   *
   * @param inst Dataset used to populate the conditional probability tables.
   * @param ess equivalent sample size.
   */
  public BayesianGraph(String _name, Schema schema, int ess)
  {
     initTree(_name, null, schema, ess, null);
  }

  /*-------------------------------*/
  /**
   * Initializes the Bayesian Graph with probability estimation capabilities and a container for Sufficient
   * Statistics.
   *
   * @param inst Dataset used to populate the conditional probability tables.
   * @param inst Dataset used to populate the conditional probability tables.
   * @param ess equivalent sample size.
   * @param S Sufficient Statistics Container S.<!-- --> The node requests him the creation of a new table.<!-- -->
   * On the next data read, the new table will be considered and grown with new data.
   */
  public BayesianGraph(String _name, Dataset _data, Schema schema, int ess, cptSet S)
  {
      initTree(_name, _data, schema, ess, S);
  }


  public BayesianGraph(String name)
  {
      this.name = name;
  }
  /*-------------------------------*/
  /**
   * Initializes the Bayesian Graph with probability estimation capabilities.
   *
   * @param inst Dataset used to populate the conditional probability tables.
   * @param ess equivalent sample size.
   */
  private void initTree(String _name, Dataset data, Schema schema, int ess, cptSet _S)
  {
     this.data = null;
      this.schema = schema;

     name =_name;
      S = _S;

     for(int i=0; i<schema.numberOfAttributes() ; i++)
     {
         if(schema.getAttribute(i).ignore()) continue;

         BNNode newNode;
         if(ess == -1) newNode = new BNNode(schema, i, ess);
         else newNode = new BNNode(schema, i, ess, S);
         if(S == null && data != null) newNode.computeTable(data);
         addNode(newNode);
     }
  }

   //////////////////////////////////////////
   ////////// RANDOM MARKOV NETWORKS
   //////////////////////////////////////////
   /**
    * @param tau threshold for connectivity. That is, if p(edge)<tau then add the edge.
    */
   public BayesianGraph generateRandomStructure(double tau, Random rand, int N, int numValues, int ess){
      //UGraph G = new UGraph();
      schema = new Schema();
      if(size() !=0){
         System.out.println("ERROR tried to randomized a graph that is not empty.");
         (new Exception()).printStackTrace();
         System.exit(0);
      }
      for(int i=0; i<N; i++){
         Attribute attr = new Attribute(""+i, i, 2);
         schema.addAttribute(attr);
         BNNode nn = new BNNode(schema, attr.index(), ess);
         
         addNode(nn);
      }
      for(int i=0; i<N; i++){
         for(int j=i+1; j<N; j++){
            double p = rand.nextDouble();
            double orientation = rand.nextDouble();
            
            if(p<tau && orientation < 0.5) addEdge(i,j);
            else if(p<tau && orientation >= 0.5) addEdge(j,i);
         }
      }
      return this;
   }

   private machineLearning.Utils.Set findAllPaths(int x, int y){
      Set paths = new Set();
      BNNode X = getBNNode(x);
      BNNode Y = getBNNode(y);

      //Termination condition. Successful path
      if(x == y) {
         Vector path = new Vector();
         path.add(Y);
         paths.add(path);
         
         return paths;
      }
      for(int i=0; i<X.numChildren(); i++ ){
         BNNode child = X.getBNChild(i);
         Set childPaths = findAllPaths(child.index(), y);
         
         //For path returned by the children, I need to add X at its beggining, and merge the sets of paths into a single one: paths. 
         for(int j=0; j<childPaths.cardinality(); j++){
            Vector path = (Vector) childPaths.elementAt(j);
            path.add(0, X);
         }
         paths = Set.Union(paths, childPaths);
      }
      return paths;
   }
   /**
    * In the context of Bayesian networks, separates means d-separation (Pearl p 117, or notebook 5 (Margaritis-2) p. 6)
    */
   public boolean separates(int x, int y, Vector Z){
      Set paths = findAllPaths(x, y);
      
      for(int i=0; i<paths.cardinality(); i++){
         Vector path = (Vector) paths.elementAt(i);
         
         boolean allPathsOk = true;
         for(int j=0; j<path.size() ; j++){
            boolean pathOk = false;
            BNNode W = (BNNode) path.elementAt(j);
            
            //do not have converging arrows
            if(W.numParents() <= 1 && Z.contains(W)) pathOk = true;
            
            //has converging arrows
            else if(W.numParents() > 1 && !Z.contains(W)){
               boolean allNotInZ = true;
               for(int k=j+1; k<path.size(); k++){
                  BNNode Wdescendent = (BNNode) path.elementAt(k);
                  
                  if(Z.contains(Wdescendent)) {
                     allNotInZ = false;
                     break;
                  }
               }
               if(allNotInZ) pathOk = true;
            }
            
            if(!pathOk) return false; //because, ALL paths must be ok, one that fails, we're done.
         }
      }
      
      return false;
   }
   
  public BayesianGraph makeCopy()
  {
      BayesianGraph newN = new BayesianGraph(name, data, schema, getBNNode(0).m_equivalent_sample_size, S);

      for(int i=0; i<size() ;i++)
      {
        BNNode old = getBNNode(i);
        BNNode newNode = newN.getBNNode(i);
        newNode.makeCopyFrom(old);
        for(int j=0; j<old.outDegree(); j++)
        {
          int pos = old.getBNChild(j).getAttributeIndex();
          newNode.addChild(newN.getBNNode(pos));
        }
        for(int j=0; j<old.inDegree(); j++)
        {
          int pos = old.getBNParent(j).getAttributeIndex();
          newNode.addParent(newN.getBNNode(pos));
        }
      }
//      System.out.println("OLD: " + toString());
  //    System.out.println("NEW: " + newN.toString());
      return newN;
  }

  /**
   * sets the value of verbose Output, which will provoke exactly that, verbose output.
   */
  public void setVerboseOutput(boolean t)
  {
      for(int i=0; i<size() ; i++) getBNNode(i).setVerboseOutput(t);
  }

  /**
   * Overrides the super class (PolyTree) get Node;
   */
  public BNNode getBNNode(int i) {return (BNNode) getNode(i);}

  /** retrieve the BN name.  */
  public String getName() {return name;}

  /** sets the BN name.  */
  public void setName(String _name) {name = _name;}

   /**
   * Adds a node to the graph.
   */
  public void addNode(BNNode newNode) { V.add(newNode); }

  /**
  * Cretes an edge from source to target vertices.<!-- -->
  * It override the addEdge of Graph but it calls i.<!-- --> It only adds an extra
  * line which extends the table given that the target node has one more parent.
  */
 public boolean addEdge(int source, int target)
 {
   return addEdge(source, target, true);
 }
 /**
  * The same as above but allows to decide whether we want to replace the table or not.<!-- -->
  * Specially implemented for the external data source case, which requires more flexibility to
  * when actually update the tables.
  */
 public boolean addEdge(int source, int target, boolean removeTable)
 {
     if(!super.addEdge(source, target)) return false;

     if(existCycle()) {
       super.removeEdge(source, target);
//       System.out.println(toString());
       return false;
   }

     BNNode v = getBNNode(target);
     v.replaceTable(schema, S, removeTable);

     return true;
 }

 /**
  * Cretes an edge from source to target vertices.<!-- -->
  * It override the addEdge of polyTree but it calls it at.<!-- --> It only adds an extra
  * line which extends the table given that the target node has one more parent.
  */
 public boolean removeEdge(int source, int target)
 {
   return removeEdge(source, target, true);
 }
 /**
  * The same as above but allows to decide whether we want to replace the table or not.<!-- -->
  * Specially implemented for the external data source case, which requires more flexibility to
  * when actually update the tables.
  */
 public boolean removeEdge(int source, int target, boolean removeTable)
 {
     if(!super.removeEdge(source, target)) return false;

     BNNode v = getBNNode(target);
     v.replaceTable(schema, S, removeTable);

     return true;
 }

 /*-------------------------------*/
    /**
    * Tranpose the edge from source to target vertices.<!-- --> That is, if (source, target) existed, it
    * removes it and adds the edge (target, source).
    */
   public boolean transposeEdge(int source, int target)
   {
     return transposeEdge(source, target, true);
   }
   /**
   * Tranpose the edge from source to target vertices.<!-- --> That is, if (source, target) existed, it
   * removes it and adds the edge (target, source).<!-- -->IF commitTranspose is se to true, it will
   * replace the CPT (Note that if using cptSet to store the CPTs, this allows to keep it there until
   * a posterior commitment with commitTranspose().
   */
   public boolean transposeEdge(int source, int target, boolean commitTranspose)
   {
       targetCond = new int[getBNNode(target).table.m_condAttributeSet.length];
       sourceCond = new int[getBNNode(source).table.m_condAttributeSet.length];
       for(int i=0;i<targetCond.length; i++) targetCond[i] = getBNNode(target).table.m_condAttributeSet[i];
       for(int i=0;i<sourceCond.length; i++) sourceCond[i] = getBNNode(source).table.m_condAttributeSet[i];

       BNNode v = getBNNode(target);
       targetIndex = v.table.m_attribute_index;
       v = getBNNode(source);
       sourceIndex = v.table.m_attribute_index;

       auxSource = source;
       auxTarget = target;

       if(!removeEdge(source, target, commitTranspose)) return false;
       if(!addEdge(target, source, commitTranspose))
         {
           //If tranpose is not commited, we should rollback the removeEdge, and invalidate future rollback
           if(!commitTranspose){
             addEdge(source, target, true);
//             S.removeCPT(auxiliaryTable.m_attribute_index, auxiliaryTable.m_condAttributeSet);
             targetCond = null;
           }
           return false;
         }

       if(commitTranspose) targetCond = null; //used to tell lastTransposeRollBack that the last trasnpose was actually
                                    //commited so it should not rollback;
       return true;
   }
   /** Used to commit the last transpose, i.e. replace the CPT tables.*/
   public void commmitLastTranspose()
   {
     if(targetCond == null) return;  //The last transpose was actually commited.

     //Commit means to remove the table of target from S.
//     S.removeCPT(targetIndex, targetCond);
//     S.removeCPT(sourceIndex, sourceCond);
   }

   /** Used to rollback last transpose.*/
   public void lastTranposeRollBack()
   {
     if(targetCond == null) return;  //The last transpose was actually commited.

     //We commit this rollback (remove tables).
     if(removeEdge(auxTarget, auxSource, true))  //Remove the newly generated table.
     {
       addEdge(auxSource, auxTarget, true);
     }
     //Since here we add a new count for the new added tables, which BTW are auxiliaryTable and
     //AuxiliaryTableII, we need to remove them once here.
//     S.removeCPT(targetIndex, targetCond);
//     S.removeCPT(sourceIndex, sourceCond);
   }


  /*-------------------------------*/
  /**
   * Used after any change produced on the graph (i.e. addition or remotion of edges).<!-- --> It
   * replaces the old table with a new one in accordance to the new dimensions required
   * (i.e. new parents added).
   *
   * @param j node for which to replace the table.
   */
  private void replaceTable(int j)
  {
      if(S == null) {
     getBNNode(j).replaceTable(schema, S);
      }

  }
  /*-------------------------------*/
  /**
   * Computes all the the tables (incrementaly or not) given the 
   * data given as input.
   */
  public void computeTables(Dataset data, boolean incremental)
  {
     for(int i=0; i<size(); i++){
        if(!incremental) getBNNode(i).replaceTable(data.schema(), null);
        getBNNode(i).computeTable(data);
     }
  }
  /*-------------------------------*/
  /**
   * replaceTable only re-allocates space for the new table.<!-- --> This method re-computes the values (counts)
   * for the table, given the dataset stored in m_Instances; used only when the data is stored in a dataset object;
   * otherwise, if S is not null, the data is obtained externally by S and this method is not required.
   *
   * @param j node for which to compute the table.
   */
  private void computeTable(int j)
  {
      if(S != null) return;

      getBNNode(j).computeTable(data);
  }

  /**
   * Retrieves the joint probability distribution for the instance.<!-- --> That is, the probability
   * that such an instance occurs.
   *
   * @param inst Instance.
   */
  public double getJointProbabilityFromInstance(Instance inst)
  {
     double prob = 1, aux;
     for(int i=0; i<size(); i++){
         aux = getBNNode(i).getTable().getProbability(inst);
         if(aux > 0) prob *= aux;
     }
     return prob;
  }
  /*-------------------------------*/
  /**
   * Retrieves the sum of the total number of entries of each table in the graph.
   *
   */
  public int totalEntries()
  {
      int tot = 1, aux;

      for(int i=0; i<size(); i++) {
     BNNode u = getBNNode(i);
     aux = u.getTable().totalEntries();
     tot = tot + aux;
      }

      return tot;
  }

  /*------------------------------*/
  /**
   * Display a representation of the Bayesian Poly Tree; indicating, for each
   * node, a list of its parents.
   */
  public String toString() {
    return toString(false);
  }
  /*------------------------------*/
  /**
   * Display a representation of the Bayesian Poly Tree; indicating, for each
   * node, a list of its parents.
   */
  public String toString(boolean verbose) {

      int val;

      String result = "\n\nNetwork complexity (total entries): " + totalEntries() + "\n" +
            "Total entries per node (average): " + totalEntries() / size() + "\n";
//            "Equivalent Sample size for probability estimation: " +  "\n";
      if(S != null) result += "\nDescription length: " + descriptionLength();
      result += super.toString();


      if(verbose) {
        for (int i = 0; i < size(); i++) {
          result += getBNNode(i).toString(10);
        }
      }
    return result;
  }
  /*******************************************************************/
   /*              Lam&Bacchus METHODS                               */
   /******************************************************************/
   /**
     * Procedure that returns a vector of BNNodes as PAIRS and LAM&BACCHUS 93.
   */
/*  public BNNode [] getPAIRS()
  {
      int numOfPairs = 0;
      for(int i=0; i<size(); i++)
      {
     numOfPairs += this.getBNNode(i).inDegree();
      }
      BNNode [] PAIRS = new BNNode[numOfPairs];
      int k=0;
      for(int i=0; i<size(); i++)
      {
     BNNode aux = getBNNode(i);
     for(int j=0; j<aux.inDegree(); j++)
     {
         PAIRS[k] = aux.getPAIR(j);
         k++;
     }
      }
      return PAIRS;

  }

  /*******************************************************************/
   /*              TRAINING METHODS                                  */
   /*                                                                */
   /* There are two methods implemented: Entropy based and MDL based */
   /*                                                                */
   /******************************************************************/
   /** Sets all visited flags to false and then calls internal_tryTranpose. */
   public boolean tryTranspose(BNNode curr)
   {
     for(int i=0; i<size(); i++)
     {
       getBNNode(i).tryTranposeVisited = false;
     }
     return internal_tryTranspose(curr);
   }
   /** Procedure that try if reversing any of the edges incident in the current node decreses the
     * description length. */
    public boolean internal_tryTranspose(BNNode curr)
    {
        double oldcurrDL, parentOldDL, oldDL, newDL;
        int target, source;
        int n = size();
        boolean didTranspose = false;

        if(curr.tryTranposeVisited) return false;
        curr.tryTranposeVisited = true;

        int [] parentList = curr.getParentAttrIndexList();

        for(int i=0; i<parentList.length; i++)
        {
          BNNode parent = getBNNode(i);

          oldcurrDL = curr.descriptionLength( true, n);

          parentOldDL = parent.descriptionLength(true,  n);
          oldDL = descriptionLength();

          target = curr.getAttributeIndex();
          source = parent.getAttributeIndex();

//          System.out.println("Before Transpose: " + toString());

          //We do not commit the transpose until we know it decreased the DL.
//          System.out.println("Before Transpose: " + toString());
          if(!transposeEdge(source, target, false)) {

          }
//          System.out.println("After Transpose: " + toString());

          newDL = oldDL - oldcurrDL - parentOldDL +
              curr.descriptionLength(true, n) +
              parent.descriptionLength(true, n);
          if(newDL - oldDL < -0.00001)
          {
            didTranspose = true;
            //we now commit the tranpose
//            System.out.println("Before Commit Transpose: " + toString());
            commmitLastTranspose();
            internal_tryTranspose(parent);
//            System.out.println("After recuriseve Transpose: " + toString());
          }
          else
          {
            lastTranposeRollBack();
//            System.out.println("After Transpose: " + toString());
            curr.descriptionLength(true, n);
            parent.descriptionLength(true, n);
          }

        }
        return didTranspose;
    }


   /**
    * Computes the score for the current state of the network.<!-- --> This method is used by the
    * Lam&Bacchus learning algorithm which uses a localized version of the MDL principle for scoring the
    * network.
    *
    */
  public double descriptionLength()
  {
      double DL = 0;
      int n=size();

      for(int i=0; i<size() ; i++)
      {
          double aux = getBNNode(i).descriptionLength(n);
     DL += aux;
      }

      return DL;
  }

/**
 * Computes the score for the current state of the network.<!-- --> This method is used by the
 * method "train_Margaritis" which construct the network from empirical data.
 */
  private double computeScore_Margaritis(double penaltyWeight)
  {
      double p, entropy = 0, penalty;

      Instance instance = data.getFirstInstance();
      while (instance != null) {
     p = getJointProbabilityFromInstance(instance);
     entropy -= p * Math.log(p);

     instance = data.getNextInstance();
      }
//      double pw = penaltyWeight;
  //    if(penaltyWeight == -1) pw = 0.0001;

      penalty = penaltyWeight*(totalEntries()/2)*Math.log(data.size());
      return entropy - penalty ;
  }
  /*--------------------------------------------------*/
  /**
   * This method construct the network according to empirical data.<!-- --> the algorithm used was
   * introduced by D.<!-- --> Margaritis, C.<!-- --> Faloutsos and S.<!-- --> Thrun.<!-- -->
   * NetCube: A Scalable Tool for Fast Data Mining and Compression.<!-- -->
   *  27th Conference on Very Large Databases (VLDB), Roma, Italy.<!-- --> September 2001.<!-- -->
   *
   * @param k Is the maximum number of parents allowed for a node. <!-- -->
   * A k value of -1 means no limit in the order statistics
   * @param penaltyWeight is a parameter that penalize too complex networks.
   */
  public void train_Margaritis(int k, double penaltyWeight)
  {
      double maxscore, newscore, score, aux;
      boolean change = false;
      String output;
      int tot=0;

      score = computeScore_Margaritis(penaltyWeight);
      maxscore = score - 1;
      while (true){
     if(score == maxscore) {
         return;
     }
     maxscore = score;
     //for each attribute pair
     for(int i=0; i<size(); i++) {
         BNNode I = getBNNode(i);
         for(int j=0; j<size()-1; j++) {
        BNNode J = getBNNode(j);
        //Maximum k parents per node or no limit if k == -1
        if(J.getParentAttrIndexList().length < k || k == -1) {
            if(addEdge(i,j)){
           J.computeTable(data);
           newscore = computeScore_Margaritis(penaltyWeight);
           if(newscore > score) {
               score = newscore;
           }
           else {
               boolean kk = removeEdge(i,j);
               J.computeTable(data);
           }
            }
        }
        if(removeEdge(i,j)){
            J.computeTable(data);
            newscore = computeScore_Margaritis(penaltyWeight);
            if(newscore > score) {
           score = newscore;
            }
            else {
           addEdge(i,j);
           J.computeTable(data);
            }
        }
        if(transposeEdge(i,j)){
            J.computeTable(data);
            I.computeTable(data);
            newscore = computeScore_Margaritis(penaltyWeight);
            if(newscore > score) {
           score = newscore;
            }
            else{
           transposeEdge(j,i);
           I.computeTable(data);
           J.computeTable(data);
            }
        }
         }
     }
     String output1 = toString();
//         Utils.writeStringToFile(output1, "graph.dat");
     if(BNNode.verboseOutput) {
         System.out.println(output1);
     }
      }
  }
  /***********************************************************/
   /*              INFERENCE METHODS                          */
   /***********************************************************/


//APPLICATION OF INFERENCE FOR CLASSIFICATION
/**
 * Sets the values of the variables to those of the instance.<!-- -->
 * If any of the values is the missing values, it ignores it.
 */
  public void setEvidenceFromInstance(Instance inst)
  {
      for(int i=0; i<inst.size()-1 ;i++)
      {
         //if(inst.getValue(i) == Instance.missingValue) continue;
         
         int valueIndex = schema.getAttribute(i).getValueIndex(inst.getValue(i));
         getBNNode(i).setEvidence(valueIndex);
      }
  }

  /**
   * Get the i-th variable's value with highest probability.
   */
  public String getMostProbableValue(Instance inst, int i)
  {
      resetInferenceInfo();
      setEvidenceFromInstance(inst, i);
      doInference();

      return getBNNode(i).getMostProbableValue();
  }


// PERFORMANCE MEASURING METHODS
/**
 * setEvidenceFromInstance sets the values of all the variables equal to those coming in the input instance.<!-- -->
 * Excepting the value of one of the variables, which it leaves it as before.<!-- --> This is for computing
 * P( Xj | X1=v1 X2=v2 ... Xj-1=vj-1 Xj+1=vj-2 .... Xn=vn)
 */
  public void setEvidenceFromInstance(Instance inst, int except)
  {
      for(int i=0; i<inst.size();i++)
      {
         if(except == i) continue;
         int valueIndex = schema.getAttribute(i).getValueIndex(inst.getValue(i));
         getBNNode(i).setEvidence(valueIndex);
      }
  }

  /**
   * Since it is not feasible to test performance for every possible attribute (variables) values, since that
   * may grow exponentially (Product of #(values) of each attributes ), we randomly sample N possible instances.
   *
   * @param testData is the test dataset for comparison
   * @param numberOfInstances is the N mentioned above.
   *
   * @return a String with information of the performance results
   */
  public String performanceMeasure(Dataset testData, int numberOfInstances)
  {
      double averageTrain = 0, averageTest = 0;
      int count = 0;
      String pString = "\nPerformance Measurement of dataset: " + testData.name + "\n";
      pString += "\ni: Conditioned attribute index";
      pString += "\nj: Conditionin attribute index";
      pString += "\nk: Value index of Conditioned attribute index";
      pString += "\nThat is: P(Xi | Xj=Vk)";
      pString += "(i,j,k)" + "\t\tBN distance to training set" + "\t\tBN distance to test set" ;
      Random rand = new Random();
      for(int i=0; i<schema.numberOfAttributes(); i++)
      {
     for(int j=0; j<schema.numberOfAttributes(); j++)
     {
         if(i==j) continue;
         Attribute attr = schema.getAttribute(j);
         for(int k=0; k<attr.size(); k++)
         {

        /*     //We obtain randomly the attribute index of the conditioned variable.
        int condIndex = rand.nextInt(data.numberOfAttributes());

        //We obtain randomly the attribute of the conditioning variable.
        int attrIndex = rand.nextInt(data.numberOfAttributes());

        //We obtain randomly the value of the conditioning attribute.
        Attribute attr = data.getAttribute(condIndex);
        int randValue = rand.nextInt(attr.size());
        */
        resetInferenceInfo();
        getBNNode(j).setEvidence(k);
        doInference();

        discreteProbabilityDistribution networkP = getBNNode(i).getBelief();
        discreteProbabilityDistribution dataP = data.getDirectProbability(j, attr.getValueName(k), i);
        discreteProbabilityDistribution testDataP = testData.getDirectProbability(j, attr.getValueName(k), i);

        double dataD = dataP.distance(networkP);
        averageTrain += dataD;
        double testDataD = testDataP.distance(networkP);
        averageTest += testDataD;

        pString += "\n(" + i +"," + j + "," + k + ")\t\t\t " +  Utils.myDoubleToString(dataD, 3) + "\t\t\t\t " +  Utils.myDoubleToString(testDataD, 3);
        count++;
         }
     }
      }

      pString += "\n---------------------------------------------------------------";
      pString += "\nAverage distance for training set: " + averageTrain/count;
      pString += "\nAverage distance for test set: " + averageTest/count;
      pString += "\nMaximum distance: 1.0";
      return pString;
  }


   /**
    * Before any inference, first we need to call this method to reset the network for the next belief propagation. <!-- -->
    * After calling resetInferenceInfor, we should set the existent evidence, not before.
    */
  public void resetInferenceInfo()
  {
      if(BNNode.verboseOutput) {
     System.out.println("");
     System.out.println("");
      }

      for(int i=0; i<size(); i++)
      {
     getBNNode(i).resetInferenceInfo();
      }
  }

  /**
   * Propagate the just setted new evidence across the network.
   */
  public void doInference()
  {
      //       resetInferenceInfo();
      for(int i=0; i<size(); i++)
      {
     if(BNNode.verboseOutput) {
         System.out.println("");
     }
     getBNNode(i).computeBelief();
      }
  }
}


