package machineLearning;
import java.util.BitSet;
import java.util.Vector;
import machineLearning.independenceBased.*;
import machineLearning.Utils.*;
import machineLearning.Datasets.IndependenceTestMargaritisUAI2001;
import machineLearning.Graphs.EulerNode;
import machineLearning.Graphs.GraphsSimple.*;
import machineLearning.Graphs.*;
/*
 * Created on Jan 29, 2006
 *
 */

/**
 * @author bromberg
 *
 */
public class Particle extends UGraphSimple{
	static long uniqueNumber = 0;
	long myUniqueNumber;
	//UGraphSimple x;
	int index;
	String signature = "";
	double normalizedProbMass;
	//int indexInClass;
	//double logPrior = 0;
	//boolean move = false;
	
	int numParticles = 0;
	
	particleClasses.ClassInfo classInfo;
	
	FastSet AUP = null, ADOWN = null;
	//double logPartPosterior;
	//private double prevIterationPosterior;//normalizedPosterior;
	double w;

	structuresParticlesFilter s;
	
	public Particle(structuresParticlesFilter s, UGraphSimple x){
		super(x);
		this.index = -1;
		ADOWN = AUP = null;
		this.s = s;
		
		myUniqueNumber = uniqueNumber++;
	}
	public Particle(structuresParticlesFilter s, UGraphSimple x, int index){
		super(x);
		//this.x = x;
		this.index = index;
		ADOWN = AUP = null;
		this.s = s;
		myUniqueNumber = uniqueNumber++;
	}
	public Particle(Particle np, int i){
		this(np);
		this.index = i;
		myUniqueNumber = uniqueNumber++;
	}
		
	/**
	 * very expensive, reconstructs the forests (dynamicGraphs) for each test
	 */
	public Particle(Particle np){
		//super(np);
		super(np.n);
		this.index = np.index;
		//this.classInfo = np.classInfo;
		//this.logPartPosterior = np.logPartPosterior;
		if(np.AUP != null) AUP = new FastSet(np.AUP);
		if(np.ADOWN != null) ADOWN = new FastSet(np.ADOWN);
		//this.prevIterationPosterior = np.prevIterationPosterior;
		this.w = np.w;

		this.s = np.s;
		
		for(int i=0; i<np.n; i++){
			for(int j=i+1; j<n; j++){
				if(np.existEdge(i,j)) addEdge(i,j);
			}
		}
		myUniqueNumber = uniqueNumber++;	
	}

	public final String hashStringParticle(){
		return super.hashString()+myUniqueNumber;
	}

	public final void addParticle(){numParticles++;};
	public final void removeParticle(){numParticles--;};
	
	//public void normalizedPosterior(double np){normalizedPosterior = np;};
	//public double prevIterationPosterior(){return prevIterationPosterior;};
/*	public double updatePosterior(){
		normalizedPosterior = classInfo.particleProbability();
		return normalizedPosterior;
	};*/
	
	public final void copyContentFrom(Particle spstar){
		super.copyContentFrom(spstar);
		//this.classInfo = spstar.classInfo;
        this.index = spstar.index;
        //this.myUniqueNumber = spstar.myUniqueNumber;
        
    }
	
	final public boolean separates(int x, int y, int test){
		return super.separates_nonRecursive2(x, y, s.tests[test].getCond());
	}
    
//	----------------------------------------------------------
	public static final double computeLogConditional_dt_yt(structuresParticlesFilter s, String signature){
		int index;
		int j = s.t-1; //last index, that is, the one corresponding to time t.

		if(j < 0) return 0;
		
		index = (signature.charAt(j) == '1')? IndependenceTestMargaritisUAI2001.I : IndependenceTestMargaritisUAI2001.D;

		return s.pds[index][j];
	}
//	----------------------------------------------------------
	public static final double computeClassLogProb(structuresParticlesFilter s, String signature){
		double val2=0;
		
		int index;
		for(int j=0 ;j<s.t; j++){
			index = (signature.charAt(j) == '1')? IndependenceTestMargaritisUAI2001.I : IndependenceTestMargaritisUAI2001.D;

			val2 += s.pds[index][j];
		}
		double aux = Math.exp(val2);
		return val2;
	}
//----------------------------------------------------------
	public final double computeClassLogProb(){
		double val2=0;
		
		int index;
		
		for(int j=0 ;j<s.t; j++){
			index = (separates_nonRecursive(s.tests[j].X(), s.tests[j].Y(), s.tests[j].getCond()))?  	
								IndependenceTestMargaritisUAI2001.I : IndependenceTestMargaritisUAI2001.D;
			val2 += s.pds[index][j];
		}
		double aux = Math.exp(val2);
		return val2;
	}
//	------------
	public final double particleNormalizedProbability(){
		return normalizedProbMass;
	}

	//------------------------------------------------------------
	public final void recomputeStringSignature(){
		signature = "";
		getClassStringSignature();
	}
	public final String getClassStringSignature(){
		if(signature == "" || signature.length() != s.t){
			StringBuffer ret = new StringBuffer();
			for(int i=0; i<s.t; i++){
				//if(separates(tests[i].X, tests[i].Y, tests[i].FastZ)) ret += "1";
				if(separates(s.tests[i].X(), s.tests[i].Y(), i)) ret.append("1");
				else ret.append("0");
			}
			
			signature = ret.toString();
			return signature;
		}
		else{
			return signature;
		}
	}
/*	public final FastSet getClassSignature(gsTriplet [] tests, int t){
		FastSet sign = new FastSet(t);
		for(int i=0; i<t; i++){
			//if(separates(tests[i].X, tests[i].Y, tests[i].FastZ)) sign.add(i);
			if(separates(tests[i].X(), tests[i].Y(), i)) sign.add(i);
		}
		return sign;
	}
	public static final boolean areEquivalent(FastSet sign1, FastSet sign2){
		
		return FastSet.isEqual(sign1, sign2);
	}*/

/*	public boolean assertConsistency(){
		if(!dynamic) return true;
		
		if(Utils._ASSERT){
			unconditional.assertConsistency();
			for(int i=0; i<s.t; i++){
				if(!dgs[i].assertConsistency()) return false;
			}
		}
		return true;
	}*/
	
	//public final String toString(){return toString();};
	
	public static void main(String[] args) {
	}
}
