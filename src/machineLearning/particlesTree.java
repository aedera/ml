package machineLearning;
import machineLearning.Utils.*;
import machineLearning.Graphs.GraphsSimple.*;
import java.util.BitSet;

/*
 * Created on Jan 29, 2006
 *
 */

/**
 * @author bromberg
 *
 */
public class particlesTree {
	int currI, currJ;	//Denotes the roots edge
	particlesTree leftBranch=null, rightBranch=null, parent = null;
	Set particlesSet;
	posterior p;
	
	BitSet unique;

	boolean amIroot = false;
	//double normalization=0;
	
//	-------------------------------------------------------------------
	//If created from outside, it sets it as root and create unique bitset
	public particlesTree(posterior p){
		particlesSet = new Set();
		
		this.p = p;
		
		currI = 0;
		currJ = 1;
	
		unique = new BitSet(p.N);
		amIroot = true;
	}
//	-------------------------------------------------------------------
	private particlesTree(posterior p, int currI, int currJ, BitSet unique){
		particlesSet = new Set();
		
		this.currI = currI;
		this.currJ = currJ;
		this.p = p;
		
		this.unique = unique;
	}
	//-------------------------------------------------------------------
	public void removeParticle(Particle part){
		if(leftBranch == null && rightBranch == null){	//is a leaf
			removeParticleFromNode(part);
		}
		else{
			if(!part.existEdge(currI, currJ)) {
				leftBranch.removeParticle(part);
			}
			else{
				rightBranch.removeParticle(part);
			}
		}
	}
	
	//	-------------------------------------------------------------------
	/*public void setAsRoot(){
		amIroot = true;
		unique = new BitSet(p.N);
	}*/
	//-------------------------------------------------------------------
	public void addParticle(Particle part){
		if(leftBranch == null && rightBranch == null){	//is a leaf
			particlesSet.add(part);
			/*normalization = */doSplit();
			
		}
		else{
			if(!part.existEdge(currI, currJ)) {
				leftBranch.addParticle(part);
			}
			else{
				rightBranch.addParticle(part);
			}
		}
	}

//	-------------------------------------------------------------------
	public boolean ASSERT_CORRECTNESS()
	{
		if(!Utils._ASSERT) return true;
		
		//Construction of an alternative unique bitset, more costly but easier to construct and test for correctnes.
		BitSet uniqueComp = new BitSet();
		
		for(int i=0; i<p.N; i++) uniqueComp.set(i);
			
		for(int i=0; i<p.N; i++){
			if(!uniqueComp.get(i)) continue;
			
			for(int j=i+1; j<p.N; j++){
				if(p.s.particles[i].hamming(p.s.particles[j]) == 0) {
					uniqueComp.clear(j);
				}
			}
		}

		//Actual comparison
		for(int i=0; i<p.N; i++) if(unique.get(i) != uniqueComp.get(i)) return false;
		
		return true;
	}
//	-------------------------------------------------------------------
	//Return normalization of the subbranch
	void doSplit(){
		boolean isLeaf = false;
		
		int index;
		//particle with lower index in the particleSet gets the 1, the rest are set to 0.
		
		int min_index=-1;
		if(particlesSet.cardinality() > 0) min_index= ((Particle) particlesSet.elementAt(0)).index;
		if(particlesSet.cardinality() == 1){
			int kk=0;
		}
		for(int i=1; i<particlesSet.cardinality(); i++){
			index = ((Particle) particlesSet.elementAt(i)).index;
			if(index < min_index) min_index = index;
		}
		for(int i=0; i<particlesSet.cardinality(); i++){
			index = ((Particle) particlesSet.elementAt(i)).index;
			if(index == 0){
				int kk=0;
			}
			if(index==min_index) unique.set(index);	//all the rest stays as zero;
			else unique.clear(index);
		}

		
		if((particlesSet.cardinality() <= 1 && leftBranch == null && rightBranch == null) || (currJ == p.n-1 && currI == p.n-1)){
			if((currJ == p.n-1 && currI == p.n-1)){
				int kk=0;
			}
			return;
		}
		

		
		
		//Splits particlesSet according to (currI,currJ) edge
		Particle sp;
		Set left = new Set(), right = new Set();
		for(int i=0; i<particlesSet.cardinality(); i++){
			sp = (Particle) particlesSet.elementAt(i);
			
			if(!sp.existEdge(currI, currJ)) {
				left.add(sp);
			}
			else{
				right.add(sp);
			}
		}

		//compute next indexes
		int nextI=currI, nextJ;

		if(currJ == p.n-1){
			nextI = currI +1;
			nextJ = currI +1;
		}
		else nextJ = currJ+1;

		//Do the split
		//if(left.cardinality() >= 1){ 
			leftBranch = new particlesTree(p, nextI, nextJ, unique);
			leftBranch.particlesSet = Set.Union(leftBranch.particlesSet, left);
			leftBranch.parent = this;
			leftBranch.doSplit();
		//}
		//if(right.cardinality() >= 1){ 
			rightBranch = new particlesTree(p, nextI, nextJ, unique);
			rightBranch.particlesSet = Set.Union(rightBranch.particlesSet, right);
			rightBranch.parent = this;
			rightBranch.doSplit();
		//}
		particlesSet.clear();
	}
	//-------------------------------------------------------------------	
	void removeParticleFromNode(Particle part){
		if(false && Utils._ASSERT){
			if(!particlesSet.contains(part)){
				System.out.println("ERRROR in particlesTree.removeParticleFromNode, trying to remove a non-existing particle");
		  		(new Exception()).printStackTrace();
		  		System.exit(1);
			}
		}
		unique.clear(part.index);
		
		particlesSet.remove(part);
		if(particlesSet.cardinality() > 0) {
			unique.set(    ((Particle) particlesSet.elementAt(0)).index      );	
		}
		
	}
	public static void main(String[] args) {
	}
}
