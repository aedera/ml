package machineLearning;
import machineLearning.independenceBased.*;
import machineLearning.Datasets.*;
import machineLearning.Graphs.*;
import machineLearning.Graphs.GraphsSimple.*;
import machineLearning.Utils.*;

import java.util.Random;
import java.util.Vector;
import java.util.LinkedList;
//import java.util.Queue;

/**
 * @author bromberg
 *
 */

public class GSOnDemand {
	Triplet k;
	//Initialization
	int n;
	FastDataset data = null;
	GraphRVar [] Varray;
	Vector V;
	boolean growing = true, shrinking;
	
	//State
	LinkedList [] grown;	
	Set [] dependent; //vector of GraphRVars	
	Set [] notGrown;
	//KBsimple kb;
	MarkovNet trueNet = null;
	
	String shrinkString [] ;
	String growString [] ;
	
	posterior p;
	
	//Statistics
	//Statistics stats = new Statistics();
	//int default_test_type = Statistics.MARGARITIS_UAI2001;
	int default_test_type = Statistics.CHISQUARE;
	double default_test_confidence = 0.05;
	boolean default_assume_independence = true;

	//Number of tests
	int weightedTestsCost = 0;
	int numberOfRecordsCost = 0;
	int testsCost = 0;

	
	GSOnDemand(FastDataset data, posterior p){
		this.p = p;
		this.data = data;
		this.n= data.schema().size();

		GSOnDemandInternal(n);
	}
	public GSOnDemand(int n, double tau, Random r, posterior p){
		this.p = p;
		trueNet = new MarkovNet();
		this.n = n;
		//trueNet.generateRandomStructure(tau, r, n, 0);
		int kk = (n*(n-1)/2);
		kk = (int)(((double)tau)*kk);
		trueNet.generateRandomStructure(tau, r, n, 0);
		
		GSOnDemandInternal(n);
	}

	private void GSOnDemandInternal(int n){
		//kb = new KBsimple(n);
		
		MarkovNet aux;
		if(data != null) {
			aux = new MarkovNet(data.schema());
		}
		else aux = trueNet;

		V = aux.V();
		Varray = new GraphRVar[n];
		notGrown = new Set[n];
		dependent = new Set[n];
		shrinkString = new String[n];
		growString = new String[n];
		
		for(int i=0; i<n; i++){
			Varray[i] = aux.getVar(i);
			notGrown[i] = new Set(V);
			notGrown[i].remove(V.elementAt(i));
			shrinkString[i] = "";
			growString[i] = "";
			dependent[i] = new Set(n);
		}		
	}
////////////////////////////////////////////////////////////////////////////////////////////
	//-------------------------------------------------------------------------------------------
/*	*//**
	 * Returns a Vector of Triplets containing all tests NOT yet grown by variables in V.
	 *//*
	final public Vector getFringe(){
		Vector fringe = new Vector();
		int Y;
		gsTriplet trip;
		boolean output = false;
		//Set dependent_i; 
		Set S_i;
		
		if(output) System.out.println("\n\nFRINGE OUTPUT");
		
		for(int i=0; i<n; i++){
			if(output) System.out.print("\n");
			//Grow
			if(output) System.out.print(i + ": [GROW] ");
			
			for(int j=0; j<notGrown[i].size(); j++){
				Y = ((GraphRVar)notGrown[i].get(j)).index(); 
				if(output) System.out.print(Y + "|" + dependent[i]+ " ");
				
				gsTriplet newTriplet = new gsTriplet(i, Y,  dependent[i], n, true);
				fringe.add(newTriplet);
			}
			
			//Shrink
			if(output) System.out.print(i + " [SHRINK] ");
			if(dependent[i].size() > 1) //IAMB
			//if(dependent[i].size() > 1 && notGrown[i].size() == 0) //GSMN
			{
				for(int j=0; j<dependent[i].size()-1; j++){
					S_i = new Set(dependent[i]);
					S_i.remove(((GraphRVar)dependent[i].elementAt(j)));

					Y = ((GraphRVar)dependent[i].elementAt(j)).index();
					
					if(output) System.out.print(Y + "|" + S_i + " ");

					trip = new gsTriplet(i, Y, S_i, n, false); 
					
					if( !kb.contains(new FactSimple(false, trip))) fringe.add( trip);
				}
			}
		}
		return fringe;
	}*/
	//-------------------------------------------------------------------------------------------
	/*final public void update(boolean I, gsTriplet trip){
		kb.addFact(new FactSimple(I, trip));
//		if(trip.grown){
		String bool;
		if(I) bool = "T";
		else bool = "F";

		notGrown[trip.X()].remove(Varray[trip.Y()]);

		//Is trip.Y() independent of trip.X(), i.e. if X \notin dependent[Y] AND X \notin notGrown[Y]
		//if(!dependent[trip.Y()].contains(Varray[trip.X()])  && !notGrown[trip.Y()].contains(Varray[trip.X()]) ) {
		if(I){
			//update(true, trip);
			notGrown[trip.Y()].remove(Varray[trip.X()]);
		}

		if(trip.grown) growString[trip.X()] += "" + Varray[trip.Y()].index() + "|" + trip.getCond() + "=" + bool +", ";
		else shrinkString[trip.X()] += "" + Varray[trip.Y()].index() + "|" + trip.getCond() + "=" + bool +", ";
		
		if(!I) dependent[trip.X()].add(Varray[trip.Y()]);
		else if(!trip.grown) {
			dependent[trip.X()].remove(Varray[trip.Y()]);
			//dependent[trip.Y()].remove(Varray[trip.X()]);
		}
	}
	*/

	//-----------------------------------------------------------------
	//if random != null, then it generates log-data likelihood from a gaussian, see notebook7 p.4
	final public double [] doTest(Triplet trip){
		return doTest(trip, -1, null);
	}
	//returns log data likelihoods, where [I] is the one for independence and [D] is the one for dependence
	final public double [] doTest(Triplet trip, int tripletIndex, Statistics stats){
		double ret [] = new double[2];
		boolean independent;
		//boolean [] testFailed = new boolean[1];
		double log_dl_ratio_true, log_dl_ratio_false;
		double mean_log_dl_true, mean_log_dl_false;
		
		weightedTestsCost += 2+trip.getCond().cardinality();
		testsCost ++;

		if(trip.getCondNotSimple() == null){
//			trip.setCondNotSimple( FactSimple.convertToSet(trip.getCond(), new Set(V)) );
		}

		if(data == null) {
			if(stats != null) ret = stats.sampledLogDataLikelihoods(p.r, new MarkovNet(trueNet), trip.X(),trip.Y(),(Vector)trip.getCondNotSimple());
			else{
//				independent=trueNet.separates(trip.X(),trip.Y(),(Vector)trip.getCondNotSimple());
//                //independent=p.trueNetSimple.separates_nonRecursive(trip.X,trip.Y,trip.FastZ);
//				
//				ret[IndependenceTestMargaritisUAI2001.I] = independent?Math.log(1.0/p.e):Math.log(p.e);
//				ret[IndependenceTestMargaritisUAI2001.D] = independent?Math.log(p.e):Math.log(1.0/p.e);
			}
		}
		else {
			int numOfRecords = p.deltaD * p.s.testChunksInfo[tripletIndex];
			
			numberOfRecordsCost += (2+trip.getCond().cardinality()) * numOfRecords; 
			//FastDataset dataChunk = data.subsetHorizontal(0, numOfRecords);
			FastDataset dataChunk = data;
			
			ret = IndependenceTestMargaritisUAI2001.log_likelihoods(dataChunk, trip.X(), trip.Y(), trip.getCondNotSimple(), p.prior);
		}
		return ret;
	}

	/**
	 * Given a set of tests (FactSimple) y, how many are satisfied by x? This is k_t(x)
	 */

	public String toString(){
		String ret = "", bool;
		int X, Y;
		Set cond;
		boolean I;
		
		for(int i=0; i<Varray.length; i++){
			X = Varray[i].index();
			ret += "" + X + ": [GROW] ";
			ret += growString [i];
			//Shrink
			ret += "   [SHRINK] ";
			ret += shrinkString [i]+ "\n";
		}
		
		
		return ret;
	}

	public static void main(String[] args) {
	}
}


