package machineLearning;
/*
 * 
 * Created on Apr 18, 2005
 *
 */
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import machineLearning.Datasets.*;
import machineLearning.Utils.*;
import machineLearning.Graphs.*;
/**
 * @author Facundo Bromberg
 *
 */

public abstract class forwardChainingSimple {
	protected KBsimple Ikb, Dkb;	//since (now) KBsimple stores facts (not condSets), we no longer need two kbs two distinguish indpendent vs dependent facts.
	protected LinkedList factQueue;
	protected Hashtable factQueueH;
	protected int infered=0;
	protected Set V; //universe of variables.
	
	public KBsimple inferedFacts;	//stores those facts that were infered.
	
	
	protected forwardChainingSimple(Set V, int numVars){
		Ikb = new KBsimple(numVars, true);
		Dkb = new KBsimple(numVars, false);
		factQueue = new LinkedList();
		factQueueH = new Hashtable();
		inferedFacts = new KBsimple(numVars);
		this.V = V;
	}
	
	public int memoryComplexity(){
		return Ikb.memoryComplexity();// + Dkb.memoryComplexity();
	}
	public FactSimple get(FactSimple f){
		if(f.I()) return Ikb.get(f);
		else return Dkb.get(f);
		
	}
	public boolean contains(FactSimple f){
		if(f.I()) return Ikb.contains(f);
		else return Dkb.contains(f);
		
	}
	abstract protected void putInQueue(FactSimple f);
	
	protected void putInKB(FactSimple f){
		if(f.I()) Ikb.addFact(f);
		else Dkb.addFact(f);

		//System.out.println(f + "(and its subsets/supersets) added to kb.");
	}
	//public Fact get(String key, boolean I){return kb.get(key, I);};

	protected String QueueToString(){
		Iterator li = factQueue.iterator();
		FactSimple f ;
		String ret ="";
		while(li.hasNext()){
			f=null;
			f = (FactSimple) li.next();
			ret += f + "\n";
		}
		return ret;
	}
	
	protected void inference(String axiom, FactSimple f1, FactSimple f2, FactSimple newf){
		inference(axiom, f1, f2, null, newf);
	}
		
	protected void inference(String axiom, FactSimple f1, FactSimple f2, FactSimple f3, FactSimple newf){
		if((newf.I() && !Ikb.contains(newf)) || (!newf.I() && !Dkb.contains(newf))){
			
			newf.antecedents(f1, f2, f3);
			newf.axiom(axiom);
			if(newf.I()){
				int k=0;
			}
			//this.inferedFacts.addFactWithSubSupsets(newf, V, Ikb, Dkb);

			if(!factQueueH.containsKey(newf.toString())){
				//System.out.println("\n\nINFERENCE( "+axiom+" ):  " + f1 + " AND " + f2 + " => " + newf+"\n\n");
				putInQueue(newf);
				infered++;
			}
		}
	}
	
	
	abstract void feedForwardStep(FactSimple f1, FactSimple f2, FactSimple f3);

	void feedForward(final FactSimple f){
		feedForward(f, false);
	}
	void feedForwardThree(final FactSimple f){
		feedForward(f, true);
	}
	/**
	 * For each fact f1 in Queue, and each fact f2 in KB, it tries to infere something using the axioms.
	 */
	void feedForward(final FactSimple f, boolean threeAntecedents){
		if((f.I() && Ikb.contains(f)) || (!f.I() && Dkb.contains(f))){
			return;
		}
		FactSimple f1;
		
		putInQueue(f);
		//System.out.println(f + "  is being forward chained.");

		while(!factQueue.isEmpty()){
			f1 = (FactSimple) factQueue.removeFirst();
			factQueueH.remove(f1);
			
			Hashtable condSets;
			Enumeration en, en2;
			FastSet Z2, Z3;
			FactSimple f2, f3 = null;
			KBsimple kb = Ikb, kb2 = Ikb;
			boolean f2I = true, f3I = true;
			int N = V.size();
			for(int k=0; k<2; k++) {
				if(k==1) {
					kb = Dkb;
					f2I = false;
				}
				for(int x=0; x<N; x++){
					for(int y=x+1; y<V.size(); y++){
						en = kb.getFacts(x, y);
						while(en.hasMoreElements()){
							Z2 = null; f2 = null;
							Z2 = ((FactSimple)en.nextElement()).getCond();
							f2 = new FactSimple(f2I, x, y, Z2);
							f2 = get(f2);
							
							//For the case of Weak Transitivity and Chordality axioms, which has three antecedents
							if(!threeAntecedents) {
								for(int t=0; t<2; t++) {
									if(t==1) {
										kb2 = Dkb;
										f3I = false;
									}
									for(int xx=0; xx<N; xx++){
										for(int yy=xx+1; yy<V.size(); yy++){
											en2 = kb.getFacts(xx, yy);
											while(en.hasMoreElements()){
												Z3 = null; f3 = null;
												Z3 = ((FactSimple)en.nextElement()).getCond();
												f3 = new FactSimple(f3I, xx, yy, Z3);
												f3 = get(f3);
												
												feedForwardStep(f1, f2, f3);
											}
										}
									}
								}
							}
							else feedForwardStep(f1, f2, f3);
						
						}
					}
				}
			}
			
			
			//According to the claim (in notebook, Forward chaining notes), it is now safe to put the fact in kb
			putInKB(f1);
/*			machineLearning.Utils.Utils.writeStringToFile("Queue\n", "kb.dat", false, false);
			machineLearning.Utils.Utils.writeStringToFile("-----\n", "kb.dat", true, false);
//			machineLearning.Utils.Utils.writeStringToFile(this.QueueToString(), "kb.dat", true, false);
			machineLearning.Utils.Utils.writeStringToFile("\nIndependent Facts\n", "kb.dat", true, false);
			machineLearning.Utils.Utils.writeStringToFile("-----------------\n", "kb.dat", true, false);
			//machineLearning.Utils.Utils.writeStringToFile(this.Ikb.toString(), "kb.dat", true, false);
			machineLearning.Utils.Utils.writeStringToFile("\nDependent Facts\n", "kb.dat", true, false);
			machineLearning.Utils.Utils.writeStringToFile("---------------\n", "kb.dat", true, false);
			//machineLearning.Utils.Utils.writeStringToFile(this.Dkb.toString(), "kb.dat", true, false);
*/			int k=1;
		}
	}
	
	
	public static void main(String[] args) {
	}
}


