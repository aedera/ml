package machineLearning.Graphs;

import java.util.Collection;

public abstract class Feature extends Potencial {
    protected Feature() { super(); }
    protected Feature(Collection<Variable> variables) { super(variables); }
    protected Feature(Variable[] variables) { super(variables); }
    public Feature(int[] scope) { super(scope); }

    public abstract double value(int... indices);
}