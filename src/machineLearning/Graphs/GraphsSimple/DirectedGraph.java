/**
 * 
 */
package machineLearning.Graphs.GraphsSimple;

import machineLearning.Utils.Utils;

/**
 * @author fschluter
 *
 */
public class DirectedGraph extends UGraphSimple {

	/**
	 * @param G
	 */
	public DirectedGraph(int n) {
		super(n);
	}

	
	/**
	 * Cretes an edge from source to target vertices.
	 * 
	 * @return False if the edge already exists.
	 */
	
	public final boolean addEdge(int s, int t) {
		hashString = null;
		if (!adjMatrix[s][t]) {
			numberOfEdges++;
		} else
			return false;
		Ebar.bitset.clear(e(s, t));// s*n+t);
		E.bitset.set(e(s, t));

		adjMatrix[s][t] = true;// adjMatrix[s].set(t);
		adjListPointers[s][t] = adjList[s].addToTail(t, null);

		if (Utils._ASSERT)
			ASSERT();

		return true;
	}

//	/**
//	 * It checks all the elements in the adjacency matrix to detect blankets
//	 * inconsistencies. If an edge is consistent it sums 1, else it sums 0.5.
//	 * 
//	 * @param G
//	 *            An UGraphSimple instance to compare the hamming distance
//	 * @return The hamming distance, considering inconsitencies. If an edge is
//	 *         consistent it sums 1, else it sums 0.5.
//	 */
//	public double hamming(UGraphSimple G) {
//		double ret = 0;
//		if (n != G.n) {
//			return 0;
//		}
//
//		for (int i = 0; i < n; i++) {
//			for (int j = 0; j < n; j++) {
//				if ((adjMatrix[i][j] && G.adjMatrix[i][j]) || (!adjMatrix[i][j] && !G.adjMatrix[i][j])) {
//					;
//				} else {
//					ret++;
//				}
//			}
//		}
//		return ret / 2d;
//	}
	
}
