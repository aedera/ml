package machineLearning.Graphs.GraphsSimple;
/*
/*
 *    Graph.java
 *    @author Facundo Bromberg. 2002.
 *
 *
 */

import java.io.*;
import java.util.BitSet;

import machineLearning.Graphs.GraphNode;
import machineLearning.Graphs.UGraph;
import machineLearning.Utils.*;

/**
 */
public class UGraphSimple_ArrayAdjList{
  public static boolean debug = false;

  public int n, m;

  protected int numberOfEdges = 0;
  
  FastSet E, Ebar; //used in metropolisHastingsStructures
  
  /**
   * Bitset that determines if an edge exist or not. adjMatrix[i] is the adjacency list
   * of node i. 
   * adjMatrix represents the adjancy matrix, and is square.
   */
  //private BitSet adjMatrix[];
  protected boolean adjMatrix[][];
  public int  adjList[][];
  public boolean visited[];
  public int degrees[];
  //private BitSet visited;
  
  private int [] emptyStack, stack;
  int stackSize;
  public BitSet visitedBits, stackBits;
  protected BitSet cc;
  protected UGraphSimple ST = null;	//constructed when connectedComponent is called
  
  
  /**
   * Initializes the graph.
   */
  public UGraphSimple_ArrayAdjList(int n) {
  	UGraphSimpleInternal(n);
  }
  public UGraphSimple_ArrayAdjList(UGraph G) {
  	this.n = G.size();
  	UGraphSimpleInternal(n);
  	
  	for(int i=0; i<n; i++){
  		for(int j=i+1; j<n; j++){
  			if(G.existEdge(i,j)) {
  				addEdge(i,j);
  			}
  		}
  	}
  	
  }
  public UGraphSimple_ArrayAdjList(UGraphSimple_ArrayAdjList G) {
  	this.n = G.n;
  	this.adjMatrix = new boolean[n][];//new BitSet[n];
  	this.adjList = new int[n][];
    this.degrees = new int[n];
    this.visited = new boolean[n];//(BitSet) G.visited.clone();
  	System.arraycopy(G.visited, 0, this.visited, 0, n);
  	System.arraycopy(G.degrees, 0, this.degrees, 0, n);
  	
    emptyStack = new int[n];
    stack = new int[n];
    visitedBits = new BitSet(n);
    stackBits = new BitSet(n);
    cc = new BitSet(n);
    
    
  	//visited.clear();
	for(int  i=0; i<n; i++){
		//this.adjMatrix[i] = new BitSet(n);//(BitSet) G.adjMatrix[i].clone();
		//this.adjMatrix[i].or(G.adjMatrix[i]);
		this.adjMatrix[i] = new boolean[n];
		this.adjList[i] = new int[n];
		System.arraycopy(G.adjMatrix[i], 0, this.adjMatrix[i], 0, n);
		System.arraycopy(G.adjList[i], 0, this.adjList[i], 0, n);
	  		
	}
	numberOfEdges = G.numberOfEdges;
  }

  public void UGraphSimpleInternal(int n) {
  	this.n = n;
    m = n*(n-1)/2;
    visited = new boolean[n];//new BitSet(n);
    adjMatrix = new boolean[n][];//new BitSet[n];
    adjList = new int[n][];//new BitSet[n];
    degrees = new int[n];
    
    emptyStack = new int[n];
    stack = new int[n];
    visitedBits = new BitSet(n);
    stackBits = new BitSet(n);
    cc = new BitSet(n);
    
    //E = new FastSet(n * (n-1));
    //Ebar = new FastSet(n * (n-1));

    for(int i=0; i<n; i++){
    	adjMatrix[i] = new boolean[n];//new BitSet(n);
    	adjList[i] = new int[n];
    	degrees[i] = 0;
    	for(int j=0; j<n; j++) adjList[i][j] = -1;
    }
}
  public void copyContentFrom(UGraphSimple_ArrayAdjList G) {
  	this.n = G.n;
  	System.arraycopy(G.visited, 0, this.visited, 0, n);
  	System.arraycopy(G.degrees, 0, this.degrees, 0, n);
  	
  	//visited.clear();
	for(int  i=0; i<n; i++){
		System.arraycopy(G.adjMatrix[i], 0, this.adjMatrix[i], 0, n);
		System.arraycopy(G.adjList[i], 0, this.adjList[i], 0, n);
	}
	numberOfEdges = G.numberOfEdges;
  }
 
  /**
   * returns the edge's index: 
   * The index of (a,b) is  min(a,b)*n + max(a,b)
   */
  final public int e(int a, int b){
  	return ((a<=b) ? a : b)*n + ((a>b) ? a : b);  //min(a,b)*n + max(a,b)
  }
  /*-------------------------------*/
final public int size(){return n;};
  /*-------------------------------*/
  /*final public int degree(int x){
  	int degree=0;
  	for(int i=0; i<n; i++) if(adjMatrix[x][i]) degree++;
  	return degree;
  }*/
  /*-------------------------------*/
    /**
   * Returns the number of edges.
   */
  public int numberOfEdges()
  {
  	
  	int noe = 0;

    if(Utils._ASSERT){
    	for(int i=0; i<n; i++)
        {
	      noe += degrees[i];//adjMatrix[i].cardinality();;
	    }
	    
	    if(numberOfEdges/2 != noe/2){
	    	int kk=0;
	    }
	    Utils.ASSERT(numberOfEdges == noe/2, "invalid number Of edges");
	    
	    return noe/2;
    }
    else return numberOfEdges;
	
  }

  /*-------------------------------*/
    /**
   * Returns true if there is an edge from the source-th node in V  to the target-th node in V.
   */
  public boolean existEdge(int s, int t)
  {
  	return adjMatrix[s][t];//adjMatrix[s].get(t);
  }
  
  /*-------------------------------*/
  //Used for ASSERTION. Returns true if adjList[i] contains element j
  private boolean contains(int i, int j){
  	for(int k=0; k<n; k++) if(adjList[i][k] == j) return true;
  	return false;
  }
  private int countDegree(boolean [] adjMatrix){
  	int sum=0;
  	for(int k=0; k<n; k++) if(adjMatrix[k]) sum++;
  	return sum;
  }
  private void ASSERT(){
  	if(Utils._ASSERT){
  		for(int i=0; i<n; i++){
  			for(int j=0; j<n; j++){
  				if(!Utils.ASSERT(adjMatrix[i][j] == contains(i,j), "Inconsistent adjList and adjMatrix in UGraphSimple")){
  					int kk=0;
  				}
  			}
  			if(!Utils.ASSERT(countDegree(adjMatrix[i]) == degrees[i], "Inconsistent adjList and adjMatrix in UGraphSimple")){
  				int kk=0;
  			}
  		}
  	}
  }
   /**
   * Cretes an edge from source to target vertices.
   * @return False if the edge already exists.
   */
  public boolean  addEdge(int s, int t)
  {
  	//TODO make more efficient
  	if(!existEdge(s, t)) numberOfEdges++;
  	else return false;
  	
  	adjMatrix[s][t] = true;//adjMatrix[s].set(t);
  	adjMatrix[t][s] = true;  	//adjMatrix[t].set(s);
  	
  	
  	//Add them in sorted order
  	int i, j=0;
	for(i=0; i<degrees[s];i++){
		if(adjList[s][i] > t) {
			for(j=degrees[s]; j>i;j--){
				adjList[s][j] = adjList[s][j-1];
			}
			adjList[s][j] = t;
			break;
		}
	}
	if(degrees[s] == i) adjList[s][i] = t;
	//if(i==degrees[s]) adjList[s][i] = t;
	//else adjList[s][i-1] = t;
	
	for(i=0; i<degrees[t];i++){
		if(adjList[t][i] > s) {
			for(j=degrees[t]; j>i;j--){
				adjList[t][j] = adjList[t][j-1];
			}
			adjList[t][j] = s;
			break;
		}
	}
	if(degrees[t] == i) adjList[t][i] = s;
	//if(i==degrees[t]) adjList[t][i] = s;
	//else adjList[t][i-1] = s;

	degrees[s]++;
	degrees[t]++;

  	/*adjList[s][degrees[s]] = t;
  	adjList[t][degrees[t]] = s;
  	degrees[s]++;
  	degrees[t]++;
  	*/
  	if(Utils._ASSERT) ASSERT();
  	
  	return true;
  }
  
  /*-------------------------------*/
  final private int nextSetBit(boolean [] b, int k){
  	for(int i=k; i<b.length; i++) if(b[i]) return i;
  	return b.length;
  }
  /*-------------------------------*/
/*  final public void addEdge(int edgeToAdd){
  	int k = edgeToAdd;
  	for(int i=0; i<adjMatrix.length; i++){
  		int node = -1;
  	  	for(int j=0; j<degrees[i];j++){//adjMatrix[i].cardinality(); j++){
  			node = nextSetBit(adjMatrix[i], node+1);//adjMatrix[i].nextSetBit(node+1);
  			if(node < i) continue;		//Because it is an undirected graph.
  			if(k == 0){
  				addEdge(i,node);
  				return;
  			}
  			k--;
  		}
  	}
  }
*/
  /*-------------------------------*/
   /**
   * Removes the edge from source to target vertices.
   * @return True if node v existed in adjacency list (children) of u AND node u existed in parents list of v.
   */
  public boolean removeEdge(int s, int t)
  {
  		if(existEdge(s, t)) numberOfEdges--;
  		else return false;
  		
  		adjMatrix[s][t] = false;//adjMatrix[s].clear(t);
  		adjMatrix[t][s] = false;  		//adjMatrix[t].clear(s);
  		
  		
  		for(int i=0; i<degrees[s];i++){
  			if(adjList[s][i] == t) {
  				for(int j=i; j<degrees[s];j++){
  					adjList[s][j] = adjList[s][j+1];
  				}
  				degrees[s]--;
  				break;
  			}
  		}
  		for(int i=0; i<degrees[t];i++){
  			if(adjList[t][i] == s) {
  				for(int j=i; j<degrees[t];j++){
  					adjList[t][j] = adjList[t][j+1];
  				}
  				degrees[t]--;
  				break;
  			}
  		}
  		
  		if(Utils._ASSERT) ASSERT();
  		return true;
  }
  
  final public boolean removeEdge(int edgeToRemove){
  	int k = edgeToRemove;
  	/*int kk=0;
  	for(int i=0; i<adjList.length;i++){
  		if(kk+degrees[i] > edgeToRemove) break;
  		else kk += degrees[i];
  	}
  	*/
  	for(int i=0; i<adjList.length;i++){
  	//	for(int i=0; i<adjMatrix.length; i++){
  		int node = -1;
  	  	for(int j=0; j<degrees[i]; j++){
  	  		int kkk = nextSetBit(adjMatrix[i], node+1);
  	  		int kk2 = adjList[i][j]; 
  	  		if(adjList[i][j] != nextSetBit(adjMatrix[i], node+1)){
  	  			int kk=0;
  	  		}
  			node = adjList[i][j];//nextSetBit(adjMatrix[i], node+1);//adjMatrix[i].nextSetBit(node+1);
  			if(node < i) continue;
  			if(k == 0){
  				return removeEdge(i,node);
  			}
  			k--;
  		}
  	}
  	return false;
  }
  /*-------------------------------*/
   /**
   * Tranpose the edge from source to target vertices.<!-- --> That is, if (source, target) existed, it
   * removes it and adds the edge (target, source)
   */
  public int flipEdge(int s, int t)
  {
  		if(adjMatrix[s][t]){//if(adjMatrix[s].get(t)) {
  			if(removeEdge(s, t)) return 0;
  			else return -1;
  		}
  		else{
  			if(addEdge(s,t)) return 0;
  			else return 1;
  		}
  }
  
  /*------------------------------------*/
  final  public boolean separates_recursive(int x, int y, FastSet S){
  	if(Utils._ASSERT){
	  	if(S.isMember(x) || S.isMember(y)) {
	  		System.out.println("S cannot contain x nor y in Graph.separates");
	  		(new Exception()).printStackTrace();
	  		System.exit(1);
	  	}
  	}
  	//visited.clear();
  	for(int i=0; i<visited.length;i++) visited[i] = false; 
  	int node;
  	int cardS = S.cardinality();
  	
  	for(int i=0; i<cardS; i++){
  		node = S.getOn(i);
  		//visited.set(node);
  		visited[node] = true;
  	}
  	boolean ret = !connectedInternal(x,y);
  	return ret;
  }
  private final boolean connectedInternal(int x, int y){
  	//visited.set(x);
  	visited[x] = true;
  	int xdegree = degrees[x];
  	
  	int k=-1;
  	for(int i=0; i<xdegree; i++){
  		k= adjList[x][i];//*/nextSetBit(adjMatrix[x], k+1);//adjMatrix[x].nextSetBit(k+1);
  	
  		if(visited[k]) continue;//if(visited.get(k)) continue;
  		
  		if(y == k) return true;
  		
  		if(connectedInternal(k, y)) return true;
  	}
  	return false;
  }

  
  /**
   * dfs(v)
   * s = empty stack
   * s.push(v)
   * while s is not empty
   *   u = s.top()
   *   s.pop()
   *   visit u
   *   for each neighbor w of u
   *     if w has not been visited and is not in s then
   *       s.push(w)
   */
  public boolean separates_nonDynamic(int x, int y, FastSet S){
  	if(Utils._ASSERT){
	  	if(S.isMember(x) || S.isMember(y)) {
	  		System.out.println("S cannot contain x nor y in Graph.separates");
	  		(new Exception()).printStackTrace();
	  		System.exit(1);
	  	}
  	}
  	int u, udegree,w, i;
  	boolean ret = true;
  	
  	//Initialization
  	visitedBits.clear();
  	stackBits.clear();
  	//System.arraycopy(G.visited, 0, this.visited, 0, n);
  	System.arraycopy(emptyStack, 0, stack, 0, n);
  	stackSize = 0;
  	
  	stack[stackSize] = x; //push
  	stackSize++;
  	stackBits.set(x);
  	
  	
  	int card = S.cardinality();
  	
  	whileLoop: while(!(stackSize == 0)){
  		u = stack[stackSize-1];	//top stack
  		stackSize--;	//pop
  		stackBits.clear(u);
  		
  		if(S.bitset.get(u)) continue;
  		
  		//System.out.println("visiting node: "+ u);
  		
  		visitedBits.set(u);
  		udegree = degrees[u];
  	  	
  		i=udegree-1;
  		for(; i>=0; i--){
  	  		w= adjList[u][i];
  	  		
  	  		if(w == y) {
  	  			ret = false;//i.e., x and y are connected
  	  			break whileLoop;
  	  			
  	  		}
  	  		
  	  		if(/*S.isMember(w) || */visitedBits.get(w) || stackBits.get(w)) continue;	//not visited and not in stack
  	  		
  	  		stack[stackSize] = w;
  	  		stackSize++;
  	  		stackBits.set(w);
  	  	}
  	}
  	if(Utils._ASSERT){
  		boolean ret2 = separates_recursive(x, y, S);
  		if(!Utils.ASSERT(ret2 == ret, "UGraphSimple: separates failed")){
  			int kk=0;
  		}
  	}
  	return ret;
  	
  }
  //------------------------------------------------------------------------
  
  //ST is assumed to be empty
  final public FastSet connectedComponent_nonDynamic(int x, FastSet S, UGraphSimple ST){
  	//ST = new UGraphSimple(n);
  	
	if(Utils._ASSERT){
	  	if(S != null && S.isMember(x) ) {
	  		System.out.println("S cannot contain x in UGraphSimple.connectedComponent");
	  		(new Exception()).printStackTrace();
	  		System.exit(1);
	  	}
  	}
  	int u, udegree,w, i;
  	boolean ret = false;
  	
  	//Initialization
	  	cc = new BitSet(n);
	  	
	  	visitedBits.clear();
	  	stackBits.clear();
	  	//System.arraycopy(G.visited, 0, this.visited, 0, n);
	  	System.arraycopy(emptyStack, 0, stack, 0, n);
	  	stackSize = 0;
	//END:initialization
  	
  	stack[stackSize] = x; //push
  	stackSize++;
  	stackBits.set(x);
  	cc.set(x);
  	whileLoop: while(!(stackSize == 0)){
  		u = stack[stackSize-1];	//top stack
  		stackSize--;	//pop
  		stackBits.clear(u);
  		
  		visitedBits.set(u);
  		udegree = degrees[u];
  	  	
  		i=udegree-1;
  		for(; i>=0; i--){
  	  		w= adjList[u][i];
  	  		
  	  		if(visitedBits.get(w) || stackBits.get(w) || (S != null && S.isMember(w))) continue;	//not visited and not in stack
  	  		
  	  		stack[stackSize] = w;
  	  		stackSize++;
  	  		stackBits.set(w);
  	  		
  	  		cc.set(w);
  	  		if(ST != null) ST.addEdge(u, w);
  	  	}
  	}
  	if(Utils._ASSERT){
  		FastSet kk = connectedComponent_recursive(x, S);
  		if(!Utils.ASSERT(FastSet.isEqual(kk,new FastSet(cc,n)), "UGraphSimple: connected component failed")){
  			int kkk=0;;
  			Utils.ASSERT(FastSet.isEqual(kk,new FastSet(cc,n)), "UGraphSimple: connected component failed");
  		}
  	}
  	return new FastSet(cc, n);
 }  	

  /*-------------------------------*/
  /**
   * Returns a set Set containing Integers I=i*n+j representing edges (i,j). Indexes i,j can be recovered from I by i=I/n, j=I%n.
   */
  public Set edgesSet(){
  	Set edges = new Set(n*(n-1)/2);
  	for(int i=0; i<n; i++){
  		for(int j=i+1; j<n; j++){
  			if(adjMatrix[i][j]) {//if(adjMatrix[i].get(j)){
  				edges.add(new Integer(i*n+j));
  			}
  		}
  	}
  	return edges;
  }
  /*-------------------------------*/
  /**
   * Returns a set Set containing Integers I=i*n+j representing non-edges (i,j) (i.e. pairs of nodes (i,j) not in E(x)). 
   * Indexes i,j can be recovered from I by i=I/n, j=I%n.
   */
  public Set edgesComplementSet(){
  	Set edges = new Set(n*(n-1)/2);
  	for(int i=0; i<n; i++){
  		for(int j=i+1; j<n; j++){
  			if(!adjMatrix[i][j]){//if(!adjMatrix[i].get(j)){
  				edges.add(new Integer(i*n+j));
  			}
  		}
  	}
  	return edges;
  }
  /*-------------------------------*/
  final public int hamming(UGraphSimple G){
  	int ret = 0;
  	if(n != G.n) return 0;
  	
  	for(int i=0; i<n; i++){
  		for(int j=i+1; j<n; j++){
  			if((existEdge(i,j) && G.existEdge(i,j))  || (!existEdge(i,j) && !G.existEdge(i,j))) {
  				; 
  			}
  			else ret++;
  		}
  	}
  	return ret;
  }
  /*-------------------------------*/
  public FastSet connectedComponent_recursive(int x, FastSet S){
  	FastSet cc = new FastSet(n);
  	cc.add(x);
  	
  	if(Utils._ASSERT){
	  	if(S != null && S.isMember(x)) {
	  		System.out.println("S cannot contain x in UGraphSimple.connectedComponent");
	  		(new Exception()).printStackTrace();
	  		System.exit(1);
	  	}
  	}
  	//visited.clear();
	for(int i=0; i<visited.length;i++) visited[i] = false; 
  	int node;
  	for(int i=0; S!=null && i<S.cardinality(); i++){
  		node = S.getOn(i);
  		//visited.set(node);
  		visited[node] = true;
  	}
  	cc = connectedCompInternal(x, cc);
  	return cc;
  }
  
  private final FastSet connectedCompInternal(int x, FastSet cc){
  	//visited.set(x);
  	visited[x] = true;
  	int xdegree = degrees[x];
  	
  	int k=-1;
  	for(int i=0; i<xdegree; i++){
  		k= adjList[x][i];//*/nextSetBit(adjMatrix[x], k+1);//adjMatrix[x].nextSetBit(k+1);
  	
  		if(visited[k]) continue;//if(visited.get(k)) continue;
  		
  		cc.add(k);
  		
  		cc.Union(connectedCompInternal(k, cc));
  		//cc = FastSet.Union(cc, connectedCompInternal(k, cc));
  	}
  	return cc;
  }

  /*-------------------------------*/
  public String toString(){
	  int TAB = 10;
	
	  //String out = "Adjacency lists of the graph: \n\n";
	  String out = "\nVAR                             VAR CHILDREN\n\n";
	  for(int i=0; i<size(); i++) {
	  	//out += adjMatrix[i].toString() + "\n";
	  	int k = adjList[i][0];//nextSetBit(adjMatrix[i], 0);//adjMatrix[i].nextSetBit(0);
	  	out += "{";
	  	if(k>=i) out += "" + k;
	  	for(int j=1; j<degrees[i]; j++){
	  		if(k >= i) out += ", ";
	  		k = adjList[i][j];//nextSetBit(adjMatrix[i], k+1);//adjMatrix[i].nextSetBit(k+1);
	  		if(k >= i) out += ""+ k;
	  	}
	  	out += "}\n";
	  }
	  return out;

  }

}


