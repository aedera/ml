package machineLearning.Graphs.GraphsSimple;

/*
 /*
 *    Graph.java
 *    @author Facundo Bromberg. 2002.
 *
 *
 */

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Random;

import machineLearning.DataStructures.DoublyLinkedList;
import machineLearning.DataStructures.DoublyLinkedListElement;
import machineLearning.DataStructures.Iterator;
import machineLearning.Graphs.BN;
import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.UGraph;
import machineLearning.Utils.FastSet;
import machineLearning.Utils.Set;
import machineLearning.Utils.Utils;

/**
 */
public class UGraphSimple {
	public static boolean debug = false;
	String hashString = null;

	public int n, m;

	protected int numberOfEdges = 0;

	public FastSet E, Ebar; // used in metropolisHastingsStructures

	public boolean adjMatrix[][];
	public double blanketScores[];
	public double[][] structureScores;
	
	DoublyLinkedListElement adjListPointers[][]; // each node has an array with
	public DoublyLinkedList adjList[];

	static DoublyLinkedListElement auxElement, current;
	static public boolean visited[], stackB[], emptyB[];
	static private int[] emptyStack, stack;
	static int stackSize;
	static private BitSet visitedBits;

	protected BitSet cc;
	protected UGraphSimple ST = null; 

	UGraphSimple A;
	protected StringBuffer sb = new StringBuffer(), sbaux;
	int i, j;
	public double logScore;
	public double score;
	public FastSet independenceAssignments;

	// protected linkedListElements [] = new
	// DoublyLinkedListElement[n];//(index, value, null, tail);

	/**
	 * Initializes the graph.
	 */
	public UGraphSimple(int n) {
		UGraphSimpleInternal(n);
		blanketScores = new double[n];
	}

	public UGraphSimple(UGraph G) {
		this.n = G.size();
		blanketScores = new double[n];
		UGraphSimpleInternal(n);

		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (G.existEdge(i, j)) {
					addEdge(i, j);
				}
			}
		}

	}

	public UGraphSimple(UGraphSimple G) {
		this.n = G.size();
		UGraphSimpleInternal(n);

		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (G.existEdge(i, j)) {
					addEdge(i, j);
				}
			}
		}
		blanketScores = new double[n];
		System.arraycopy(G.blanketScores, 0, blanketScores, 0, n);
	}
	
	public UGraphSimple(int[][] structure) {
		this.n = structure.length;
		UGraphSimpleInternal(n);
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < structure[i].length; j++) {
//				if(structure[i][j]<n){
					addEdge(i, structure[i][j]);
//				}
			}
		}
	}

	public UGraphSimple(BN G) {
		this.n = G.size();
		blanketScores = new double[n];
		UGraphSimpleInternal(n);

		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (G.isParent(i, j) || G.isParent(j, i)) {
					addEdge(i, j);
				}
			}
		}

	}

	public UGraphSimple(int n, boolean[][] adjMatrix) {
		this.n = n;
		blanketScores = new double[n];
		UGraphSimpleInternal(n);
		independenceAssignments = new FastSet(n * (n - 1) / 2);
		int k = 0;
		for (int i=0; i<n-1; ++i) {
			for (int j=i+1; j<n; ++j) {
				boolean isDependent = adjMatrix[i][j] || adjMatrix[j][i];
				if(isDependent){
					addEdge(i, j);
					independenceAssignments.flip(k);
				}
				++k;
			}
		}
	}
	
	/** Generates a random structure with exactly m edges */
	public UGraphSimple(int n, int m, Random r) {
		UGraphSimpleInternal(n);
		blanketScores = new double[n];
		// We first obtain a random permutation of the (n choose 2) pairs.
		int M = n * (n - 1) / 2;
		int[] Ms = Utils.randomIntArray(M, r);

		// We then select the first m once and use each to add the corresponding
		// edge.
		for (int i = 0; i < m; i++) {
			addEdge(a(Ms[i]), b(Ms[i]));
		}
	}

	public UGraphSimple(MarkovNet G) {
		this.n = G.size();
		blanketScores = new double[n];
		UGraphSimpleInternal(n);

		for (int i = 0; i < n-1; i++) {
			for (int j = i+1; j < n; j++) {
				if (G.existEdge(i, j)) {// G.existEdge(i,j)) {
					addEdge(i, j);
				}
			}
		}

	}

	public UGraphSimple(String bog, int n) {
		this(new UGraph(bog, n));
	}

	public UGraphSimple(Boolean[] bog, int n) {
		this.n = n;
		UGraphSimpleInternal(n);

		/*
		 * -i is the index in the bitstring, j is the index of rows in the
		 * adyascence matrix, k is the index of columns in the adyascence matrix
		 */
		int i = 0;
		int j = 0;
		int k = 1;
		int nextDBreak = n - 1;
		int times = 0;
		for (Boolean bit : bog) {
			++times;
			if (bit) {
				addEdge(j, k);
			}

			if (times == nextDBreak) {
				--nextDBreak;
				k = n - nextDBreak - 1;
				++j;
				times = 0;
			}
			++k;
			++i;
		}
	}
	
	public UGraphSimple(FastSet bog, int n) {
		this.n = n;
		UGraphSimpleInternal(n);
		this.independenceAssignments = bog;
		
		/*
		 * -i is the index in the bitstring, j is the index of rows in the
		 * adyascence matrix, k is the index of columns in the adyascence matrix
		 */
		int i = 0;
		int j = 0;
		int k = 1;
		int nextDBreak = n - 1;
		int times = 0;
		for (int bit=0; bit<bog.capacity; ++bit) {
			++times;
			if (bog.isMember(bit)) {
				addEdge(j, k);
			}
			
			if (times == nextDBreak) {
				--nextDBreak;
				k = n - nextDBreak - 1;
				++j;
				times = 0;
			}
			++k;
			++i;
		}
	}
	
	

	public void UGraphSimpleInternal(int n) {
		this.n = n;
		m = n * (n - 1) / 2;
		visited = new boolean[n];// new BitSet(n);
		adjMatrix = new boolean[n][];// new BitSet[n];
		// adjList = new int[n][];//new BitSet[n];
		adjListPointers = new DoublyLinkedListElement[n][];// new BitSet[n];
		adjList = new DoublyLinkedList[n];
		// degrees = new int[n];

		emptyStack = new int[n];
		stack = new int[n];

		visited = new boolean[n];
		stackB = new boolean[n];
		emptyB = new boolean[n];

		visitedBits = new BitSet(n);
		cc = new BitSet(n);

		E = new FastSet(n * (n - 1));
		Ebar = new FastSet(n * (n - 1));

		for (int i = 0; i < n; i++) {
			adjMatrix[i] = new boolean[n];// new BitSet(n);
			adjListPointers[i] = new DoublyLinkedListElement[n];
			adjList[i] = new DoublyLinkedList();
			// adjList[i] = new int[n];
			// degrees[i] = 0;
			for (int j = i + 1; j < n; j++)
				Ebar.add(e(i, j));
		}

		hashString = hashString();
	}

	public final boolean equals(Object O) {
		A = (UGraphSimple) O;
		for (i = 0; i < n; i++) {
			for (j = i + 1; j < n; j++) {
				if (this.adjMatrix[i][j] != A.adjMatrix[i][j])
					return false;
			}
		}
		return true;
	}

	public void resetHashString() {
		hashString = null;
	};

	public String hashString() {
		if (hashString == null) {
			sb.delete(0, (n * (n - 1)) / 2);
			for (i = 0; i < n; i++) {
				for (j = i + 1; j < n; j++) {
					sbaux = adjMatrix[i][j] ? sb.append('1') : sb.append('0');
				}
			}
			hashString = sb.toString();
		}
		return hashString;
	}

	public int hashCode() {
		return hashString().hashCode();
	}

	@SuppressWarnings("static-access")
	public void copyContentFrom(UGraphSimple G) {
		this.n = G.n;
		System.arraycopy(G.visited, 0, this.visited, 0, n);
		System.arraycopy(G.blanketScores, 0, this.blanketScores, 0, n);

		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (G.adjMatrix[i][j]) {
					addEdge(i, j);
				} else {
					removeEdge(i, j);
				}
			}
		}
		// visited.clear();
		/*
		 * for(int i=0; i<n; i++){ System.arraycopy(G.adjMatrix[i], 0,
		 * this.adjMatrix[i], 0, n); System.arraycopy(G.adjList[i], 0,
		 * this.adjList[i], 0, n); } numberOfEdges = G.numberOfEdges;
		 */
	}

	/**
	 * returns the edge's index: The index of (a,b) is min(a,b)*n + max(a,b)
	 */
	static final public int e(int a, int b, int n) {
		return ((a <= b) ? a : b) * n + ((a > b) ? a : b); // min(a,b)*n +
		// max(a,b)
	}

	final public int e(int a, int b) {
		return ((a <= b) ? a : b) * n + ((a > b) ? a : b); // min(a,b)*n +
		// max(a,b)
	}

	/** Given the edge's index m, it returns the nodes of the edge */
	static final public int a(int n, int m) {
		return m % n;
	};

	static final public int b(int n, int m) {
		return m / n;
	};

	public int a(int m) {
		return m % n;
	};

	public int b(int m) {
		return m / n;
	};

	/*-------------------------------*/
	final public int size() {
		return n;
	};

	/*-------------------------------*/
	/*
	 * final public int degree(int x){ int degree=0; for(int i=0; i<n; i++)
	 * if(adjMatrix[x][i]) degree++; return degree; }
	 */
	/*-------------------------------*/
	/**
	 * Returns the number of edges.
	 */
	public int numberOfEdges() {
		return numberOfEdges;
	}

	/*-------------------------------*/
	public int countEdges() {
		int numberOfEdges = 0;
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++)
				if (adjMatrix[i][j])
					numberOfEdges++;
		}
		return numberOfEdges;
	}

	/*-------------------------------*/
	/**
	 * Returns true if there is an edge from the source-th node in V to the
	 * target-th node in V.
	 */
	final public boolean existEdge(int s, int t) {
		return adjMatrix[s][t];// adjMatrix[s].get(t);
	}

	/*-------------------------------*/
	// Used for ASSERTION. Returns true if adjList[i] contains element j
	private boolean contains(int i, int j) {
		Iterator it = adjList[i].elements();
		while (it.hasMoreElements()) {
			if (it.nextElement().index == j) {
				return true;
			}
		}
		return false;
	}

	protected void ASSERT() {
		if (Utils._ASSERT) {
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					if (!Utils
							.ASSERT(adjMatrix[i][j] == contains(i, j),
									"Inconsistent adjList and adjMatrix in UGraphSimple")) {
						;
					}
				}

				/*
				 * if(!Utils.ASSERT(countDegree(adjMatrix[i]) == degrees[i],
				 * "Inconsistent adjList and adjMatrix in UGraphSimple")){ int
				 * kk=0; }
				 */
			}

			FastSet Ebar2 = new FastSet(n * (n - 1));
			for (int j = 0; j < n; j++) { // Initialize containing the
				// complement of E(xNew).
				for (int k = j + 1; k < n; k++) {
					if (!adjMatrix[j][k])
						Ebar2.add(j * n + k);
				}
			}
			Utils.ASSERT(Ebar2.bitset.equals(Ebar.bitset),
					"Error in Ebar in UGraphSImple");

			FastSet E2 = new FastSet(n * (n - 1));
			for (int j = 0; j < n; j++) { // Initialize containing the
				// complement of E(xNew).
				for (int k = j + 1; k < n; k++) {
					if (adjMatrix[j][k])
						E2.add(j * n + k);
					// Ebar.add(j*xNew.size()+i);
				}
			}
			Utils.ASSERT(E.bitset.equals(E.bitset),
					"Error in E in UGraphSImple");

		}
	}

	/**
	 * Cretes an edge from source to target vertices.
	 * 
	 * @return False if the edge already exists.
	 */
	public boolean addEdge(int s, int t) {
		hashString = null;
		// TODO make more efficient
		if (!adjMatrix[s][t]) {
			numberOfEdges++;
		} else
			return false;
		Ebar.bitset.clear(e(s, t));// s*n+t);
		E.bitset.set(e(s, t));

		adjMatrix[s][t] = true;// adjMatrix[s].set(t);
		adjMatrix[t][s] = true; // adjMatrix[t].set(s);

		adjListPointers[s][t] = adjList[s].addToTail(t, null);
		adjListPointers[t][s] = adjList[t].addToTail(s, null);

		if (Utils._ASSERT)
			ASSERT();

		return true;
	}

	/*-------------------------------*/
	/**
	 * Removes the edge from source to target vertices.
	 * 
	 * @return True if node v existed in adjacency list (children) of u AND node
	 *         u existed in parents list of v.
	 */
	public final boolean removeEdge(int s, int t) {
		hashString = null;

		if (adjMatrix[s][t]) {
			numberOfEdges--;
		} else
			return false;
		Ebar.bitset.set(e(s, t));
		E.bitset.clear(e(s, t));

		adjMatrix[s][t] = false;// adjMatrix[s].clear(t);
		adjMatrix[t][s] = false; // adjMatrix[t].clear(s);

		adjList[s].remove(adjListPointers[s][t]);
		adjListPointers[s][t] = null;

		adjList[t].remove(adjListPointers[t][s]);
		adjListPointers[t][s] = null;

		if (Utils._ASSERT)
			ASSERT();

		return true;
	}

	final public boolean removeEdge(int edgeToRemove) {
		hashString = null;
		;

		int currEdge = 0;
		for (i = 0; i < n; i++) {
			for (j = i + 1; j < n; j++) {
				if (adjMatrix[i][j]) {
					if (currEdge == edgeToRemove) {
						removeEdge(i, j);
						return true;
					}
					currEdge++;
				}
			}
		}
		return false;
	}

	/*-------------------------------*/
	/**
	 * Tranpose the edge from source to target vertices.<!-- --> That is, if
	 * (source, target) existed, it removes it and adds the edge (target,
	 * source)
	 */
	public int flipEdge(int s, int t) {
		if (adjMatrix[s][t]) {// if(adjMatrix[s].get(t)) {
			if (removeEdge(s, t))
				return 0;
			else
				return -1;
		} else {
			if (addEdge(s, t))
				return 0;
			else
				return 1;
		}
	}

	/*------------------------------------*/
	public final double shortestPathLengthConditional(int s, int t, FastSet S) {
		UGraphSimple G = new UGraphSimple(this);
		// remove all edges in and out of nodes in S
		for (int u = 0; u < n; u++) {
			if (S.isMember(u)) {
				DoublyLinkedListElement v = adjList[u].head;
				while (v != null) {
					G.removeEdge(u, v.index);
					v = v.next;
				}
			}
		}
		return G.shortestPathLength(s, t);
	}

	/*------------------------------------*/
	final double shortestPathLength(int s, int t) {
		double w[][] = new double[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++)
				w[i][j] = 1;
		}
		double spl_v[] = dijkstra(s, w);
		double spl = spl_v[t];

		return spl;
	}

	/*------------------------------------*/
	/**
	 * Dijkstra's algorithm for shortest path weight. Given a matrix of edge
	 * weights, it returns an array d[t], containing the weight of the shortest
	 * path between input node s and t. It also stores pi[t], the predecessor of
	 * each node t. Starting at target t, pi[t] can be used to reconstruct the
	 * actual shortest path to s.
	 */
	final double[] dijkstra(int s, double[][] w) {
		double d[] = new double[n];
		boolean visited[] = new boolean[n];
		int pi[] = new int[n];

		// Initialize single source
		for (int i = 0; i < n; i++) {
			d[i] = Double.MAX_VALUE;
			pi[i] = -1;
			visited[i] = false;
		}
		d[s] = 0;
		// visited[s] = true;

		while (true) {
			// Extract-min from Queue. Here this is implemented
			// as finding the node with minimum d that has not
			// been yet visited.
			double min = Double.MAX_VALUE;
			int u = -1;
			for (int i = 0; i < n; i++) {
				if (visited[i])
					continue;

				if (d[i] <= min) {
					min = d[i];
					u = i;
				}
			}
			if (u == -1)
				break;

			visited[u] = true;

			DoublyLinkedListElement v = adjList[u].head;
			while (v != null) {
				// relax(u,v,w)
				if (d[v.index] > d[u] + w[u][v.index]) {
					d[v.index] = d[u] + w[u][v.index];
					pi[v.index] = u;
				}

				v = v.next;
			}
		}
		return d;
	}

	/*------------------------------------*/
	final public boolean separates_recursive(int x, int y, FastSet S) {
		if (Utils._ASSERT) {
			if (S.isMember(x) || S.isMember(y)) {
				System.out
						.println("S cannot contain x nor y in Graph.separates");
				(new Exception()).printStackTrace();
				System.exit(1);
			}
		}
		// visited.clear();
		for (int i = 0; i < visited.length; i++)
			visited[i] = false;
		int node;
		int cardS = S.cardinality();

		for (int i = 0; i < cardS; i++) {
			node = S.getOn(i);
			// visited.set(node);
			visited[node] = true;
		}
		boolean ret = !connectedInternal(x, y);
		return ret;
	}

	/*------------------------------------*/
	private final boolean connectedInternal(int x, int y) {
		// visited.set(x);
		visited[x] = true;
		// int xdegree = degrees[x];

		int k = -1;
		Iterator it = adjList[x].elements();
		// for(int i=0; i<xdegree; i++){
		while (it.hasMoreElements()) {
			k = it.nextElement().index;// k=
			// adjList[x][i];//*/nextSetBit(adjMatrix[x],
			// k+1);//adjMatrix[x].nextSetBit(k+1);

			if (visited[k])
				continue;// if(visited.get(k)) continue;

			if (y == k)
				return true;

			if (connectedInternal(k, y))
				return true;
		}
		return false;
	}

	/*------------------------------------*/
	/**
	 * dfs(v) s = empty stack s.push(v) while s is not empty u = s.top() s.pop()
	 * visit u for each neighbor w of u if w has not been visited and is not in
	 * s then s.push(w)
	 */
	public boolean separates_nonRecursive2(int x, int y, FastSet S) {
		if (Utils._ASSERT) {
			if (S.isMember(x) || S.isMember(y)) {
				System.out
						.println("S cannot contain x nor y in Graph.separates");
				(new Exception()).printStackTrace();
				System.exit(1);
			}
		}
		int u, w;
		boolean ret = true;
		DoublyLinkedListElement auxElement, current;

		// Initialization
		// visitedBits.clear();
		// stackBits.clear();

		System.arraycopy(emptyB, 0, stackB, 0, n);
		System.arraycopy(emptyB, 0, visited, 0, n);
		System.arraycopy(emptyStack, 0, stack, 0, n);
		stackSize = 0;

		stack[stackSize] = x; // push
		stackSize++;
		stackB[x] = true;

		// int card = S.cardinality();

		whileLoop: while (!(stackSize == 0)) {
			u = stack[stackSize - 1]; // top stack
			stackSize--; // pop
			stackB[u] = false;

			if (S.bitset.get(u))
				continue;

			visitedBits.set(u);
			visited[u] = true;

			current = adjList[u].head;
			while (current != null) {
				// Iterator it = adjList[u].elements();
				// while(it.hasMoreElements()){
				auxElement = current;
				current = current.next;
				w = auxElement.index;// adjList[u][i];

				// w= it.nextElement().index;//adjList[u][i];

				if (w == y) {
					ret = false;// i.e., x and y are connected
					break whileLoop;

				}

				if (/* S.isMember(w) || */visited[w] || stackB[w])
					continue; // not visited and not in stack

				stack[stackSize] = w;
				stackSize++;
				stackB[w] = true;
			}
		}
		if (Utils._ASSERT) {
			boolean ret2 = separates_recursive(x, y, S);
			if (!Utils.ASSERT(ret2 == ret, "UGraphSimple: separates failed")) {
				;
			}
		}
		return ret;

	}

	/*------------------------------------*/
	/**
	 * dfs(v) s = empty stack s.push(v) while s is not empty u = s.top() s.pop()
	 * visit u for each neighbor w of u if w has not been visited and is not in
	 * s then s.push(w)
	 */
	public boolean separates_nonRecursive(int x, int y, FastSet Z) {
		if (Utils._ASSERT) {
			if (Z.isMember(x) || Z.isMember(y)) {
				System.out
						.println("S cannot contain x nor y in Graph.separates");
				(new Exception()).printStackTrace();
				System.exit(1);
			}
		}
		int u, w;
		boolean ret = true;
		DoublyLinkedListElement auxElement, current;

		// Initialization
		// visitedBits.clear();
		// stackBits.clear();

		System.arraycopy(emptyB, 0, stackB, 0, n);
		System.arraycopy(emptyB, 0, visited, 0, n);
		System.arraycopy(emptyStack, 0, stack, 0, n);
		stackSize = 0;

		stack[stackSize] = x; // push
		stackSize++;
		stackB[x] = true;

		// int card = S.cardinality();

		whileLoop: while (!(stackSize == 0)) {
			u = stack[stackSize - 1]; // top stack
			stackSize--; // pop
			stackB[u] = false;

			if (Z.bitset.get(u))
				continue;

			visitedBits.set(u);
			visited[u] = true;

			current = adjList[u].head;
			while (current != null) {
				// Iterator it = adjList[u].elements();
				// while(it.hasMoreElements()){
				auxElement = current;
				current = current.next;
				w = auxElement.index;// adjList[u][i];

				// w= it.nextElement().index;//adjList[u][i];

				if (w == y) {
					ret = false;// i.e., x and y are connected
					break whileLoop;

				}

				if (/* S.isMember(w) || */visited[w] || stackB[w])
					continue; // not visited and not in stack

				stack[stackSize] = w;
				stackSize++;
				stackB[w] = true;
			}
		}
		if (Utils._ASSERT) {
			boolean ret2 = separates_recursive(x, y, Z);
			if (!Utils.ASSERT(ret2 == ret, "UGraphSimple: separates failed")) {
				;
			}
		}
		return ret;

	}

	// ------------------------------------------------------------------------
	// final public FastSet connectedComponent_nonDynamic(int x, FastSet S,
	// UGraphSimple ST, BitSet cc){
	// }
	// ST is assumed to be empty
	final public BitSet connectedComponent_nonDynamic(int x, FastSet S,
			UGraphSimple ST, BitSet cc) {
		// ST = new UGraphSimple(n);

		if (Utils._ASSERT) {
			if (S != null && S.isMember(x)) {
				System.out
						.println("S cannot contain x in UGraphSimple.connectedComponent");
				(new Exception()).printStackTrace();
				System.exit(1);
			}
		}
		int u, w;
		DoublyLinkedListElement auxElement, current;

		// Initialization
		// cc = new BitSet(n);

		cc.clear();
		// stackBits.clear();
		System.arraycopy(emptyB, 0, stackB, 0, n);
		System.arraycopy(emptyB, 0, visited, 0, n);
		// System.arraycopy(emptyB, 0, cc, 0, n);
		System.arraycopy(emptyStack, 0, stack, 0, n);

		stackSize = 0;
		// END:initialization

		stack[stackSize] = x; // push
		stackSize++;
		stackB[x] = true;
		cc.set(x);
		while (!(stackSize == 0)) {
			u = stack[stackSize - 1]; // top stack
			stackSize--; // pop
			stackB[u] = false;

			// if(S.bitset.get(u)) continue;

			visited[u] = true;

			current = adjList[u].head;
			while (current != null) {
				// Iterator it = adjList[u].elements();
				// while(it.hasMoreElements()){
				auxElement = current;
				current = current.next;
				w = auxElement.index;// adjList[u][i];

				// w= it.nextElement().index;//adjList[u][i];

				if (visited[w] || stackB[w] || (S != null && S.bitset.get(w)))
					continue; // not visited and not in stack

				stack[stackSize] = w;
				stackSize++;
				stackB[w] = true;

				cc.set(w);
			}
		}
		if (Utils._ASSERT) {
			FastSet kk = connectedComponent_recursive(x, S);
			if (!Utils.ASSERT(FastSet.isEqual(kk, new FastSet(cc, n)),
					"UGraphSimple: connected component failed")) {
				;
				Utils.ASSERT(FastSet.isEqual(kk, new FastSet(cc, n)),
						"UGraphSimple: connected component failed");
			}
		}
		return cc;
	}

	// ------------------------------------------------------------------------
	// final public FastSet connectedComponent_nonDynamic(int x, FastSet S,
	// UGraphSimple ST, BitSet cc){
	// }
	// ST is assumed to be empty
	final public BitSet connectedComponent_nonDynamic2(int x, FastSet S,
			UGraphSimple ST, BitSet cc) {
		// ST = new UGraphSimple(n);

		if (Utils._ASSERT) {
			if (S != null && S.isMember(x)) {
				System.out
						.println("S cannot contain x in UGraphSimple.connectedComponent");
				(new Exception()).printStackTrace();
				System.exit(1);
			}
		}
		int u, w;
		DoublyLinkedListElement auxElement, current;

		// Initialization
		// cc = new BitSet(n);

		cc.clear();
		// stackBits.clear();
		System.arraycopy(emptyB, 0, stackB, 0, n);
		System.arraycopy(emptyB, 0, visited, 0, n);
		// System.arraycopy(emptyB, 0, cc, 0, n);
		System.arraycopy(emptyStack, 0, stack, 0, n);

		stackSize = 0;
		// END:initialization

		stack[stackSize] = x; // push
		stackSize++;
		stackB[x] = true;
		cc.set(x);

		while (!(stackSize == 0)) {
			u = stack[stackSize - 1]; // top stack
			stackSize--; // pop
			stackB[u] = false;

			// if(S.bitset.get(u)) continue;

			visited[u] = true;

			current = adjList[u].head;
			while (current != null) {
				// Iterator it = adjList[u].elements();
				// while(it.hasMoreElements()){
				auxElement = current;
				current = current.next;
				w = auxElement.index;// adjList[u][i];

				// w= it.nextElement().index;//adjList[u][i];

				if (visited[w] || stackB[w] || (S != null && S.bitset.get(w)))
					continue; // not visited and not in stack

				stack[stackSize] = w;
				stackSize++;
				stackB[w] = true;

				cc.set(w);
				// if(ST != null) ST.addEdge(u, w);
			}
		}
		if (Utils._ASSERT) {
			FastSet kk = connectedComponent_recursive(x, S);
			if (!Utils.ASSERT(FastSet.isEqual(kk, new FastSet(cc, n)),
					"UGraphSimple: connected component failed")) {
				Utils.ASSERT(FastSet.isEqual(kk, new FastSet(cc, n)),
						"UGraphSimple: connected component failed");
			}
		}
		// return new FastSet(cc, n);
		return cc;
	}

	/*-------------------------------*/
	/**
	 * Returns a set Set containing Integers I=i*n+j representing edges (i,j).
	 * Indexes i,j can be recovered from I by i=I/n, j=I%n.
	 */
	public Set edgesSet() {
		Set edges = new Set(n * (n - 1) / 2);
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (adjMatrix[i][j]) {// if(adjMatrix[i].get(j)){
					edges.add(new Integer(i * n + j));
				}
			}
		}
		return edges;
	}

	/*-------------------------------*/
	/**
	 * Returns a set Set containing Integers I=i*n+j representing non-edges
	 * (i,j) (i.e. pairs of nodes (i,j) not in E(x)). Indexes i,j can be
	 * recovered from I by i=I/n, j=I%n.
	 */
	public Set edgesComplementSet() {
		Set edges = new Set(n * (n - 1) / 2);
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (!adjMatrix[i][j]) {// if(!adjMatrix[i].get(j)){
					edges.add(new Integer(i * n + j));
				}
			}
		}
		return edges;
	}

	// /*-------------------------------*/
	// public double hamming(UGraphSimple G){
	// double ret = 0;
	// if(n != G.n) return 0;
	//	  	
	// for(int i=0; i<n; i++){
	// for(int j=i+1; j<n; j++){
	// if((adjMatrix[i][j] && G.adjMatrix[i][j]) || (!adjMatrix[i][j] &&
	// !G.adjMatrix[i][j])) {
	// ;
	// }else{
	// ret++;
	// }
	// }
	// }
	// System.out.println("____________");
	// System.out.println("CALCULATING HD: ");
	// System.out.println("G1: " + toString());
	// System.out.println("G2: " + G.toString());
	// System.out.println("HD: " + ret);
	// System.out.println("____________");
	// return ret;
	// }

	/**
	 * It checks all the elements in the adjacency matrix to detect blankets
	 * inconsistencies. If an edge is consistent it sums 1, else it sums 0.5.
	 * 
	 * @param G
	 *            An UGraphSimple instance to compare the hamming distance
	 * @return The hamming distance, considering inconsitencies. If an edge is
	 *         consistent it sums 1, else it sums 0.5.
	 */
	public double hamming(UGraphSimple G) {
		double ret = 0;
		if (n != G.n) {
			return 0;
		}

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if ((adjMatrix[i][j] && G.adjMatrix[i][j])
						|| (!adjMatrix[i][j] && !G.adjMatrix[i][j])) {
					;
				} else {
					ret++;
				}
			}
		}
		// System.out.println("____________");
		// System.out.println("CALCULATING HD: ");
		// System.out.println("G1: " + toString());
		// System.out.println("G2: " + G.toString());
		// System.out.println("HD: " + ret);
		// System.out.println("____________");
		return ret / 2d;
	}

	public double falsePositives(UGraphSimple trueGraph) {
		double ret = 0;
		if (n != trueGraph.n) {
			return 0;
		}
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if ((adjMatrix[i][j] && !trueGraph.adjMatrix[i][j])) {
					ret++;
				}
			}
		}
		return ret / 2d;
	}
	
	public double falseNegatives(UGraphSimple trueGraph) {
		double ret = 0;
		if (n != trueGraph.n) {
			return 0;
		}
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if ((!adjMatrix[i][j] && trueGraph.adjMatrix[i][j])) {
					ret++;
				}
			}
		}
		return ret / 2d;
	}

	/*-------------------------------*/
	public FastSet connectedComponent_recursive(int x, FastSet S) {
		FastSet cc = new FastSet(n);
		cc.add(x);

		if (Utils._ASSERT) {
			if (S != null && S.isMember(x)) {
				System.out
						.println("S cannot contain x in UGraphSimple.connectedComponent");
				(new Exception()).printStackTrace();
				System.exit(1);
			}
		}
		// visited.clear();
		for (int i = 0; i < visited.length; i++)
			visited[i] = false;
		int node;
		for (int i = 0; S != null && i < S.cardinality(); i++) {
			node = S.getOn(i);
			// visited.set(node);
			visited[node] = true;
		}
		cc = connectedCompInternal(x, cc);
		return cc;
	}

	private final FastSet connectedCompInternal(int x, FastSet cc) {
		// visited.set(x);
		visited[x] = true;
		// int xdegree = degrees[x];
		//  	
		// int k=-1;
		// for(int i=0; i<xdegree; i++){
		// k= adjList[x][i];//*/nextSetBit(adjMatrix[x],
		// k+1);//adjMatrix[x].nextSetBit(k+1);
		//  	
		Iterator it = adjList[x].elements();
		int k;
		while (it.hasMoreElements()) {
			k = it.nextElement().index;
			if (visited[k])
				continue;// if(visited.get(k)) continue;

			cc.add(k);

			cc.Union(connectedCompInternal(k, cc));
			// cc = FastSet.Union(cc, connectedCompInternal(k, cc));
		}
		return cc;
	}

	/*-------------------------------*/
	public String toString() {
		String out = "\nVAR                             VAR CHILDREN\n\n";
		for (int i = 0; i < n; i++) {
			Iterator it = adjList[i].elements();
			int k = -1;
			if (it.hasMoreElements())
				k = it.nextElement().index;
			out += "{";
			if (k >= i)
				out += "" + k;
			while (it.hasMoreElements()) {
				if (k >= i)
					out += ", ";
				k = it.nextElement().index;
				if (k >= i)
					out += "" + k;
			}
			out += "}\n";
		}
		return out;

	}

	public static void main(String[] args) {
		Utils._ASSERT = true;

		UGraphSimple G = new UGraphSimple(7);
		G.toString();
		UGraphSimple.debug = true;
		G.addEdge(0, 3);
		G.addEdge(2, 3);
		G.addEdge(1, 3);
		G.addEdge(0, 4);
		G.addEdge(4, 5);
		G.addEdge(5, 6);
		G.addEdge(4, 6);
		G.addEdge(2, 6);
		G.addEdge(1, 5);

		// G.removeEdge(2,6);
		G.removeEdge(0, 3);
		G.removeEdge(1, 5);
		G.addEdge(1, 6);
		G.addEdge(1, 4);
		G.addEdge(1, 0);
		G.addEdge(4, 6);
		G.addEdge(5, 2);
		G.addEdge(0, 2);
		G.removeEdge(1, 5);
		G.removeEdge(1, 3);
		G.removeEdge(2, 6);
		G.removeEdge(4, 1);
	}

	public int getCRC() {
		String crc = "";
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (adjMatrix[i][j]) {
					crc += "1";
				} else {
					crc += "0";
				}
			}
		}
		return crc.hashCode();
	}

	public List<Integer> getBlanketOf2(Integer X) {
		List<Integer> blanketOfX = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			if (adjMatrix[X][i]) {
				blanketOfX.add(i);
			}
		}
		return blanketOfX;
	}
	
	public FastSet getBlanketOf(Integer X) {
		FastSet blanketOfX = new FastSet(n);
		for (int i = 0; i < n; i++) {
			if (adjMatrix[X][i]) {
				blanketOfX.add(i);
			}
		}
		return blanketOfX;
	}

	public int getBlanketSizeOf(Integer X) {
		int blanketSizeOfX = 0;
		for (int i = 0; i < n; i++) {
			if (adjMatrix[X][i]) {
				++blanketSizeOfX;
			}
		}
		return blanketSizeOfX;
	}
	

	public int[][] intArray() {
		int[][] intArray = new int[n][];
		for (int i = 0; i < n; i++) {
			List<Integer> blanketOfX = new ArrayList<Integer>();
			for (int j = 0; j < n; j++) {
				if (adjMatrix[i][j]) {
					blanketOfX.add(j);
				}
			}
			intArray[i] = new int[blanketOfX.size()];
			j=0;
			for(Integer Y: blanketOfX){
				intArray[i][j] = Y;
				j++;
			}
		}	
		return intArray;
	}

	public List<Integer> getVariables() {
		List<Integer> variables = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			variables.add(i);
		}	
		return variables;
	}
	
	public Boolean[] getBOG() {
		int k=0;
		Boolean[] bog = new Boolean[n*(n-1)/2];
		for (int i = 0; i < size(); i++) {
			for (int j = i + 1; j < size(); j++) {
				bog[k]= existEdge(i, j)? true : false;
				++k;
			}
		}
		return bog;
	}

	public FastSet getFastSet() {
		int k=0;
		FastSet bog = new FastSet(n*(n-1)/2);
		for (int i = 0; i < size(); i++) {
			for (int j = i + 1; j < size(); j++) {
				if(existEdge(i, j)){
					bog.flip(k);
				}
				++k;
			}
		}
		return bog;
	}

	public List<Integer> getFlippedVariables(UGraphSimple graphSimple) {
			List<Integer> flippedVariables = new ArrayList<Integer>();
			for (int i = 0; i < n; i++) {
				for (int j = i + 1; j < n; j++) {
					if ((adjMatrix[i][j] && graphSimple.adjMatrix[i][j]) || (!adjMatrix[i][j] && !graphSimple.adjMatrix[i][j])) {
						;
					} else {
						flippedVariables.add(i);
						flippedVariables.add(j);
					}
				}
			}
			return flippedVariables;
		}

	public int getMaximumSizeOfBlanket() {
		int maximumSizeOfBlanket = 0;
		for (int X = 0; X < size(); X++) {
			int sizeOfBlanket = getBlanketSizeOf(X);
			if(sizeOfBlanket > maximumSizeOfBlanket){
				maximumSizeOfBlanket = sizeOfBlanket;
			}
		}
		return maximumSizeOfBlanket;
	}

		

}
