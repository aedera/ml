package machineLearning.Graphs;

import java.util.Collection;

import machineLearning.Graphs.VarSet;


public class ConstantFactor implements Factor {
    private double value_;

    public ConstantFactor() { }

    public ConstantFactor(double value) {
        setValue(value);
    }

    public ConstantFactor(Factor factor) {
        if (factor instanceof ConstantFactor)
            value_ = ((ConstantFactor)factor).value();
        else {
            Assigment ass = new Assigment(factor.scope());
            value_ = factor.value(ass);
        }
    }
    public double value(machineLearning.Datasets.FastInstance instance) {
	throw new UnsupportedOperationException ("Commig soon");
    }
    public double value(int... instance) {
	throw new UnsupportedOperationException ("Commig soon");
    }
    public double value(Assigment ass) { return value(); }

    public double value() { return value_; }

    public void setValue(Assigment assigment, double value) { setValue(value); }

    public void setValue(double value) { value_ = value; }

    public Factor multiply(Factor factor) throws IllegalArgumentException {
        return null;
    }

    public Collection<Variable> scope() { return new VarSet(); }

    public boolean isAnyInScope(Collection<Variable> collection) {
        return false;
    }

    public boolean isAllInScope(Collection<Variable> collection) {
        return false;
    }

    public boolean isInScope(Variable variable) { return false; }

    public Collection<Variable> getVariables() { return new VarSet(); }

    public Factor marginalize(Variable... variables)
        throws IllegalArgumentException {
        return this;
    }

    public Factor marginalize(Variable variable)
        throws IllegalArgumentException {
        return this;
    }

    public void normalize() { }

    public int numParameters() { return 1; }

    public boolean isZero() { return value_ == 0; }

    public Factor clone() { return new ConstantFactor(value_); }
}