package machineLearning.Graphs;

import java.util.Collection;
import machineLearning.Datasets.FastInstance;

public interface Factor extends Cloneable {
    /**
     * @return the factor value for the assignation <code>ass</code>.
     */
    public double value(Assigment ass);
    public double value(int[] ass);

    public double value(FastInstance instance);

    /**
     * Set value <code>value</code> in assigment <code>ass</code> of factor.
     */
    public void setValue(Assigment ass, double value);

    /**
     * @return the product between this factor and <code>factor</code>.
     */
    public Factor multiply(Factor factor) throws IllegalArgumentException;

    /**
     * @return variables in the factor scope.
     */
    public Collection<Variable> scope();

    /**
     * @return if any variable in <code>collection</code> is in the factor
     * scope.
     */
    public boolean isAnyInScope(Collection<Variable> collection);

    /**
     * @return if all the variable in <code>collection</code> is in the factor
     * scope.
     */
    public boolean isAllInScope(Collection<Variable> collection);

    /**
     * @return if variable is in the factor scope.
     */
    public boolean isInScope(Variable variable);

    /**
     * @return variables of this factor.
     */
    public Collection<Variable> getVariables();

    /**
     * @return true thereas all parameters in factor are zeros.
     */
    public boolean isZero();

    /**
     * @return the marginal distribution getted by summing out
     * <code>var</code>.
     */
    public Factor
        marginalize(Variable... variables) throws IllegalArgumentException;
    public Factor
        marginalize(Variable variable) throws IllegalArgumentException;

    /**
     * Normalize factor
     */
    public void normalize();

    /**
     * @return the number of parameters for this factor.
     */
    public int numParameters();

    public Factor clone();
}