package machineLearning.Graphs;

public class Variable implements Comparable, Cloneable {
    private String label_;
    private int index_;
    // Number of values for variable
    private int outcomes_;
    private static int INDEX = -1;

    public Variable() {
        // Variable continuos
        outcomes_ = -1;
        index_ = ++INDEX;
    }

    public Variable(int outcomes) {
        this();
        outcomes_ = outcomes;
        setLabel();
    }

    public Variable(int outcomes, String label) {
        this(outcomes);
        setLabel(label);
    }

    public Variable(int outcomes, int index) {
        this(outcomes);
        index_ = index;
        setLabel();
    }

    // TODO INDEX++
    public Variable(int outcomes, String label, int index) {
        outcomes_ = outcomes;
        label_ = label;
        index_ = index;
    }

    public void setLabel() { label_ = ""+index_; }
    public void setLabel(String label) { label_ = label; }
    public String getLabel() { return label_; }

    public int compareTo(Object o) {
        int index = this.index_;
        int indexO = ((Variable)o).index_;

        if (index == indexO) return 0;
        else if (index < indexO) return -1;
        else return 1;
    }

    public String toString() { return getLabel(); }

    public int getNumOutcomes() { return getOutcomes(); }
    public int getOutcomes() { return outcomes_; }

    public int index() { return getIndex(); }
    public int getIndex() { return index_; }

    public Variable clone() throws CloneNotSupportedException {
        return new Variable(outcomes_, label_, index_);
    }

    public boolean equals(Object o) {
        return index_ == ((Variable)o).index_;
    }
}