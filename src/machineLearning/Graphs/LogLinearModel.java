package machineLearning.Graphs;

public class LogLinearModel extends FactorGraph {
    private double[] params_;

    public LogLinearModel(Factor[] factors, double[] parameters) {
	super(factors);
	params_ = parameters;
    }

    public double value(int... ass) {
	return unNormValue(ass) * constNorm_;
    }

    protected double unNormValue(int... ass) {
        double unNormMeasure = 0;
	int k = 0;

	for (Factor f : factors_) unNormMeasure += params_[k] * f.value(ass);

	return Math.exp(unNormMeasure);
    }
}