/*
 * Created on Mar 16, 2005
 *
 */
package machineLearning.Graphs;
import java.util.ArrayList;
import java.util.Vector;

import machineLearning.Datasets.*;
import machineLearning.Utils.*;

/**
 * @author Facundo Bromberg
 *
 */
public class GraphRVar extends GraphicalModelNode{


    public GraphRVar(int index){
        super(index);
    }
    public GraphRVar(Attribute attr){
        super(attr);
    }

    public GraphRVar(GraphRVar node){
        super(node);
    }

    public GraphRVar(GraphNode node, Attribute attr){
        super(node, attr);
    }

    public GraphRVar(String _name, int _index)
    {
        children = new ArrayList<GraphicalModelNode>();
        parents = new Vector(0, 2);

        name = _name;
        index = _index;
        visited = false;
    }

    public boolean smaller(Object a, Object b){
        return ((GraphRVar) a).index() < ((GraphRVar) b).index();
    }

    public static void main(String[] args) {
    }
}
