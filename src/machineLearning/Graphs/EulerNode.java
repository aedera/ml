/*
 *	 How to make more efficient:
 *	maintain pointers to first and last element. Used (as far as I remember) in join, and 
 *  obviously in treeMaximum and treeMinimum.
 * 
 * 
 */
package machineLearning.Graphs;

import machineLearning.Graphs.GraphsSimple.*;
import machineLearning.Utils.*;
import java.util.BitSet;
import machineLearning.DataStructures.*;

/**
 * @author bromberg
 *
 */
public final class EulerNode extends RBNode{
	//UGraphSimple spanningT;
	//int n;	//number of nodes in ST
	static boolean debug = true;
	
	public int STindex;		//pointer to the node in the MN (or ST or G).
	//public DoublyLinkedListElement eulerSequenceElement;	//pointer to its location in the DlinkedList eulerSequence
	//EulerNode leftOcc, rightOcc;
	int leftOcc, rightOcc;
	boolean active = true;

	static EulerNode nil = null;
	
	dynamicGraph dg;

	

///////////////////////////////////////////////////////
///////////  CONSTRUCTOR/S //////////////
/////////////////////////////////////////////////////
/*	EulerNode(){
		STindex = -1;
		leftOcc = null;
		rightOcc = null;
	}
*/	EulerNode(int index, dynamicGraph dyn_graph){
		super();
		super.debug = debug;
		
		this.dg = dyn_graph;
		
		STindex = index;
		//leftOcc = null;//T.new RBNode(null);
		//rightOcc = null;//T.new RBNode(null);
		leftOcc = -1;//nil;
		rightOcc = -1;//nil;
		active = false;
		
		
	}
	EulerNode(EulerNode node){
		super(node.content, node.key);

		dg = node.dg;

		active = false;
		STindex = node.STindex;
		leftOcc = -1;//nil;
		rightOcc = -1;//nil;
		
	}
	
	/*public EulerTree(RBTree T, eulerNode [][] edgeOcc, eulerNode [] activeOcc){
		this.setRoot(T.root);
		this.treeOcc = edgeOcc;
		this.activeOcc = activeOcc;
		//this.anyOcc = anyOcc;
	}*/

	public static EulerNode construct(UGraphSimple ST, int rootIndex, dynamicGraph dyn_graph){
		//creates occ of root node
		EulerNode singleton = new EulerNode(rootIndex, dyn_graph);
		dyn_graph.activeOcc[rootIndex] = singleton;
		singleton.active = true;
		
		//toString();
//		dyn_graph.visitedBits.clear();
		return constructInternal(singleton, ST, rootIndex);
	}

	public String toString(){return toString(printContent);};
	public String toString(boolean printContent){
		return toStringInternal("", printContent);
	}

///////////////////////////////////////////////////////
///////////  PUBLIC METHODS  //////////////
/////////////////////////////////////////////////////
	public final  static void createNil(){
		if(nil == null) {
			nil = new EulerNode(-2, null);
			nil.red = false;
			nil.key = -2;
			nil.left = null;
			nil.right = null;
			nil.p = null;
		}
		RBNode.nil = nil;
	}
	/**
	 * deletes edge (a,b) from T: Let T1 and T2 be the two trees which result,
	 * where a \in T1 and b \in T2. Let oa1, ob1, oa2, ob2 represent the ocurrences 
	 * encountered in the two traversals of (a,b). If oa1<ob1 and ob1<ob2, then oa1 <
	 * ob1 < ob2 < oa2. Thus ET(T2) is given by the interval of ET(T) ob1, ...., ob2
	 * and ET(T1) is given by splicing out of ET(T) the sequence ob1, ...., ob2. 
	 */
	public final static RBNode [] cutST(dynamicGraph dg, int a, int b){
		int e = dg.e(a, b);
		
		RBNode root_a=null, root_b=null;
		if(debug){
			root_a = getRoot(dg.activeOcc[a]);
			root_b = getRoot(dg.activeOcc[b]);
		}
		root_a = root_a;
		root_b = root_b;
		//get the nodes representing edge (a,b)
		EulerNode oa1 = dg.treeOcc[e*4+0];
		EulerNode oa2 = dg.treeOcc[e*4+1];
		EulerNode ob1 = dg.treeOcc[e*4+2];
		EulerNode ob2 = dg.treeOcc[e*4+3];
		
		//set the treeOcc to nil
		dg.treeOcc[e*4+0] = nil;
		dg.treeOcc[e*4+1] = nil;
		dg.treeOcc[e*4+2] = nil;
		dg.treeOcc[e*4+3] = nil;
		
		//sort oa1, oa2, ob1, and ob2 s.t. oa1 < ob1 < ob2 < oa2 in in-order
		//if they're not nil.
		//eb1 may b nil
		EulerNode aux = nil;
		if(oa1 != nil && oa2 != nil){
			if(smaller(oa2, oa1)){
				aux = oa1; oa1 = oa2; oa2 = aux;
			}
		}
		else{		//either oa1 or oa2 are nil
			if(oa1 != nil) {//.. it is oa2
				oa2 = oa1; oa1 = nil;
			}
		}
		if(ob1 != nil && ob2 != nil){
			if(smaller(ob2, ob1)){
				aux = ob1; ob1 = ob2; ob2 = aux;
			}
		}
		else{
					//either ob1 or ob2 is nil
			if(ob1 != nil){
				ob2 = ob1; ob1 = nil;
			}
		}
		//now oa2 and ob2 are non-nil
		if(smaller(oa2, ob2) ){
			aux = ob1; ob1 = oa1; oa1 = aux;
			aux = ob2; ob2 = oa2; oa2 = aux;
		}
		
		//update trees
		RBNode [] TT = (RBNode [])split(oa1, true);	///{.....,oa1}{ob1,......}
		RBNode [] TT2 = (RBNode [])split(oa2, true);	///{.....,oa1}{ob1,......}
		TT[0] = (EulerNode) join(TT[0], TT2[1]);
		
		RBNode [] TT3 = (RBNode [])split(ob2, true);	///{.....,oa1}{ob1,......}
		TT[1] = TT3[0];
		
		/*RBNode [] TT = (RBNode [])split(oa2, true);	///{.....,oa1}{ob1,......}
		RBNode [] TT2 = (RBNode []) split(ob2, true); //{ob1,.....,ob2}{oa2,.....}
		RBNode  [] TT3 = (RBNode []) split(ob1, false); 
			
		TT[0] = (EulerNode) join(TT3[0], TT[1]);
		TT[1] = TT3[1];
		RBNode [] TT = (RBNode [])split(ob1, false);	///{.....,oa1}{ob1,......}
		RBNode [] TT2 = (RBNode []) split(oa2, false); //{ob1,.....,ob2}{oa2,.....}
		RBNode  [] TT3 = (RBNode []) split(oa2, true); 
			
		TT[0] = (EulerNode) join(TT[0], TT3[1]);
		TT[1] = TT2[0];
*/		
		/*RBNode [] TT = split(oa1, false);
		RBNode [] TT2 = split(oa2, false);
		TT[0] = (EulerNode) join(TT[0], TT2[1]);
		TT2 =  split(ob2, false);
		TT[1] = TT2[1];*/
		
		//update active occurences
		if(oa2.active) oa2.passActivityTo(oa1);
		
		// update treeOcc
		int k;
		int eAfter = oa2.rightOcc;
		if(eAfter != -1){
			//int eAfter = oa2.rightEdge();
			//if(oa1.leftOcc != eAfter_node){
			if(oa1.leftOcc != eAfter){
				//replace oa2 by oa1
				for(k=0 ; oa2 != dg.treeOcc[eAfter*4+k]; k++);
				dg.treeOcc[eAfter*4+k] = oa1;
			}
			else{
				//replae oa2 by nil
				for(k=0 ; oa2 != dg.treeOcc[eAfter*4+k]; k++);
				dg.treeOcc[eAfter*4+k] = nil;
			}
		}
		//update edge occurences
		oa1.rightOcc = oa2.rightOcc;
		if(ob1 != nil) ob1.leftOcc = -1;
		else		   ob2.leftOcc = -1;
		ob2.rightOcc = -1;
		return TT;
	}
	
	/**
	 * Let s = from, t = to. Then, let os denote any occurence of s (first here).
	 * Splice out the first part of the sequence ending with the occurence before os, 
	 * remove its first occurence (or), and tack this on to the end of the 
	 * sequence which now begins with os. Add a new occurence os to the end.
	 */
	public final static EulerNode changeSTRoot(EulerNode et_root, EulerNode to){
		dynamicGraph dg = to.dg;
		int k;
		
		if(et_root == nil) return et_root;
		
//FIRST: update tree occurences		
		EulerNode first = (EulerNode) treeMinimum(et_root);
		EulerNode last = (EulerNode) treeMaximum(et_root);
		
		if(to == first) return et_root;
		
		EulerNode new_occ = new EulerNode(to);
		
		if(first.active) first.passActivityTo(last);
			
		if(to.leftOcc == to.rightOcc){  //to is a leaf in  ST
			for(k=0; nil != dg.treeOcc[to.leftOcc*4+k]; k++);
			dg.treeOcc[to.leftOcc*4+k] = new_occ;
		}
		else{
			for(k=0; to != dg.treeOcc[to.leftOcc*4+k]; k++);
			dg.treeOcc[to.leftOcc*4+k] = new_occ;
		}
		int firstEdge = first.rightOcc;
		if(firstEdge != last.leftOcc   ||  to == last){
			for(k=0; first != dg.treeOcc[firstEdge*4+k]; k++);
			dg.treeOcc[firstEdge*4+k] = last;
		}
		else{
			for(k=0; first != dg.treeOcc[firstEdge*4+k]; k++);
			dg.treeOcc[firstEdge*4+k] = nil;
		}
		
//SECOND: update edge occurences		
		last.rightOcc = firstEdge;
		new_occ.leftOcc = to.leftOcc;
		to.leftOcc = -1;
		
		//split off first occurence and delete it
		RBNode [] TT1 = split(first,true);
		first = null;
		
		//split immediately before to
		RBNode [] TT2 = split(to, false);
		
		return (EulerNode) insertRight(join(TT2[1], TT2[0]), new_occ);
		
	}
	
	/**
	 * Links euler trees ETa and ETb by edge (a,b) in ST,
	 * where a and b are indexes of nodes in the ST.
	 * 
	 * According to Henzinger&King:
	 * Given occurences oa and ob, reroot ETb at b, create
	 * a new occurence oan and splice the sequence ETb:oan into ETa, 
	 * inmediately after oa.
	 * 
	 * @return it returns nothing, but ETa stores the joined tree.
	 */
	public final static EulerNode linkST(EulerNode root_a, EulerNode root_b, int a, int b){
		dynamicGraph dg = root_a.dg;
		
		if(root_a == nil) return root_b;
		if(root_b == nil) return root_a;
		
		
		//Find active occurences of u and v, and create new occ of u
		EulerNode a_act = dg.activeOcc[a];
		EulerNode b_act = dg.activeOcc[b];
		EulerNode new_a_occ = new EulerNode(a_act);
		
		//Change root to v_act
		root_b = changeSTRoot(root_b, b_act);
		
		//Initialize tree_occs of new node (u,v)
		int e = dg.e(a, b);
		dg.treeOcc[e*4+0] = a_act;
		dg.treeOcc[e*4+1] = new_a_occ;
		
		//the first and the last node of ETb are treeOcc[edgeIndex][2 and 3] if
		//they are different, otherwise treeOcc[edgeIndex][2] is nil
		EulerNode etb_last = (EulerNode) treeMaximum(root_b);
		dg.treeOcc[e*4+3] = etb_last;
		if(etb_last != b_act) 	dg.treeOcc[e*4+2] = b_act;
		else 					dg.treeOcc[e*4+2] = nil;//nil;
		 
		//Update tree occurences of edge following e if it exists
		int eAfter = a_act.rightOcc;
		if(eAfter != -1){
			if(a_act.leftOcc != eAfter){
				//replace u_act by new_u_occ
				int k;
				for(k=0; a_act != dg.treeOcc[eAfter*4+k]; k++);
				dg.treeOcc[eAfter*4+k] = new_a_occ;
			}
			else{
//				replace nil pointer by new_u_occ
				int k;
				for(k=0; nil != dg.treeOcc[eAfter*4+k]; k++);
				dg.treeOcc[eAfter*4+k] = new_a_occ;
			}
		}
		
		//update edge occurences
		new_a_occ.rightOcc = a_act.rightOcc;
		new_a_occ.leftOcc = e;
		a_act.rightOcc = e;
		b_act.leftOcc = e;
		etb_last.rightOcc = e;
		
		//update trees
		//ETb.insert(new_a_occ, ETb.treeMaximum(ETb.root), true);
		root_b = (EulerNode) insertRight(root_b, new_a_occ);
		RBNode [] TT1 = split(a_act, true);
		//concatenate the pieces
		return (EulerNode) join(TT1[0], join(root_b, TT1[1]));
	}

	public boolean assertConsistency(FastSet cc, UGraphSimple ST){
		if(!super.assertConsistency()) return false;
		
		FastSet [] visitedNeighbors = new FastSet[ST.n];
		for(int i=0; i<ST.n; i++){
			visitedNeighbors[i] = new FastSet(ST.n);
		}
//		assertConsistencyInternal(ST, visitedNeighbors, treeMinimum(this));
		EulerNode x = (EulerNode) treeMinimum(this); 
		EulerNode y = (EulerNode) treeSuccessor(x);
		int indexX = x.STindex, indexY = y.STindex;
		while(y != nil){
			if(!Utils.ASSERT(ST.existEdge(indexX, indexY), "Error type 1 in EulerNode")) {
				return false;
			}
			visitedNeighbors[indexX].add(indexY);
			
			x = y;
			indexX = indexY;
			y = (EulerNode) treeSuccessor(x);
			indexY = y.STindex;
		}
		for(int i=0; i<ST.n; i++){
			if(!cc.isMember(i)) continue;
			FastSet adjListBS = new FastSet(ST.n);
			Iterator it = ST.adjList[i].elements();
			while(it.hasMoreElements()){
			//for(int j=0; j<ST.degrees[i]; j++){
				//adjListBS.add(ST.adjList[i][j]);
				adjListBS.add(it.nextElement().index);
			}
			if(!Utils.ASSERT(adjListBS.isEqual(visitedNeighbors[i]), "Error type 2 in EulerNode")) {
				return false;
			}
		}
		return true;
	}
	
///////////////////////////////////////////////////////
///////////  PRIVATE METHODS  //////////////
/////////////////////////////////////////////////////
	private final void passActivityTo(EulerNode to){
		to.active = true;
		active = false;
		dg.activeOcc[to.STindex] = to;
	}
	
	/**
	 * Populates the tree with each visit of each node in an Euler Tour of a spanning tree ST:
	 * 
	 * ET(x)
	 * 	visit x;  (or insert)
	 * 	for each child c of x do
	 * 		ET(c);
	 * 		visit x;	(or insert)
	 * 
	 */

	public static UGraphSimple reconstructST(EulerNode root, int n){
		UGraphSimple G = new UGraphSimple(n);
		
		EulerNode x = (EulerNode) treeMinimum(root);
		EulerNode y = nil;
		if(x != nil) y = (EulerNode) treeSuccessor(x);
		
		int xIndex, yIndex;
		while(y != nil){
			xIndex = x.STindex;
			yIndex = y.STindex;
		
			G.addEdge(xIndex, yIndex);
			x = y;
			y = (EulerNode) treeSuccessor(x);
		}
		
		return G;
	}

	/**
	 * Return the root of the new tree
	 */
	private static EulerNode constructInternal(EulerNode root, UGraphSimple ST, int a){
//		root.dg.visitedBits.set(a);
		
		//int card = ST.degrees[a];
		int b;
		
		//Only go from a to b if a<b
//		for(int i=0; i<card; i++){
		Iterator it = ST.adjList[a].elements();
		while(it.hasMoreElements()){
			//b = ST.adjList[a][i];
			b = it.nextElement().index;
//			if(root.dg.visitedBits.get(b)) continue;
				
			EulerNode singleton = new EulerNode(b, root.dg);
			singleton.active = true;
			root.dg.activeOcc[b] = singleton;
			
			root = linkST(root, singleton, a, b);
			
			root = constructInternal(root, ST, b);
		}
		return root;
	}
	
	/**
	 * returns the output of the branch of x.
	 */
	private String toStringInternal(String offset, boolean printContent){
		if(this == nil) return offset;
		
		String outLeft = ((EulerNode)left).toStringInternal(offset + "     ", printContent);
		String outRight = ((EulerNode)right).toStringInternal(offset + "     ", printContent);
		
		String label = label();
		if(red && offset.length()>0) {
			offset = offset.substring(0, offset.length()-1);
		}
		
		String out="";
		out += outRight; 
		out += "\n" + offset + label;
		out += outLeft;
		
		return out;
	}
	private String label(){
		String label = "";//+key;
		//label += "("+ STindex +")";
		label += STindex;
		//else label = ;
		
		if(red) label = "."+label+".";
		return label;
	};

///////////////////////////////////////////////////////
///////////  MAIN  //////////////
/////////////////////////////////////////////////////
	
	public static void main(String[] args) {
		
	}
}
