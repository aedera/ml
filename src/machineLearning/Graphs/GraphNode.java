package machineLearning.Graphs;

/**
 * Class: GraphNode
 * Implements a graph node that can have any number of children and parents.
 *
 * @author Facundo Bromberg
 * @version 1.0 Final Project CS572 AI, Fall 2002.
 * @date Nov 2002
 */

import java.util.ArrayList;
import java.util.Vector;
import java.util.Iterator;

import machineLearning.Utils.FastSet;
import machineLearning.Utils.Set;
/**
 * Implements a graph node. This node maintains its own adjacency list (Vector
 * children) and a parents Vector <!-- --> The calling class should maintain
 * these vectors; i.e. Taking care that if u is a child of v, then v appears in
 * the parent vector of u.
 *
 * @author Facundo Bromberg
 * @version 1.0 Final Project CS572 AI, Fall 2002.
 * @date Nov 2002
 */
@SuppressWarnings("unchecked")
public class GraphNode implements GraphNodeI, Comparable<GraphNode> {

    public static final int COMPARE_BY_G = 0;

    public static final int COMPARE_BY_LOCAL_G = 1;

    public static final int COMPARE_BY_PRIORITY = 2;

    public static final int COMPARE_BY_AVERAGE_G = 3;

    /**
     * List of childrens; i.e. the adjacency list.
     */
    protected ArrayList<GraphicalModelNode> children;

    /**
     * Parents list.
     */
    protected Vector<GraphicalModelNode> parents = new Vector<GraphicalModelNode>();

    // FIXME;correrlo a una clase TreeNode
    protected Vector<GraphicalModelNode> blanket = new Vector<GraphicalModelNode>();

    // protected FastSet blanketSet;
    /**
     * Bitsets menos significativos (DERECHA) son los predecesores, y los ms
     * significativos son los del blanket (o conditioning set) Son 2*n bits (n
     * predecesores, y n del blanket), donde n es la cantidad de variables.
     */
    protected FastSet state;
    protected GraphicalModelNode parent = null;
    protected double priority = 0;
    private boolean truth_value;
    private int depth = 0;

    /**
     * A String name for the node.
     */
    protected String name = "";

    /**
     * A index for the node.
     */
    protected int index;

    /**
     * Use to check for double paths between nodes.
     */
    public boolean visited;

    /**
     * G cost
     */
    private Double g = 0d;

    /**
     * G local cost
     */
    private Double localG = 0d;
    private Double avgG = 0d;

    /**
     * How to compare in the compareTo method implementation
     */
    public int compareMode = COMPARE_BY_G;

    public int n;

    /**
     * String Constructor
     */
    public GraphNode(String _name) {
        children = new ArrayList<GraphicalModelNode>();
        parents = new Vector(0, 2);

        name = _name;
        visited = false;
    }

    /**
     * String Constructor
     */
    public GraphNode(String _name, int _index) {
        children = new ArrayList<GraphicalModelNode>();
        parents = new Vector(0, 2);

        name = _name;
        index = _index;
        visited = false;
    }

    public GraphNode() {
    }

    /**
     * String Constructor
     */
    public GraphNode(int _index) {
        children = new ArrayList<GraphicalModelNode>();
        parents = new Vector(0, 2);

        name = "" + _index;
        index = _index;
        visited = false;
    }

    public GraphNode(int n, int index) {
        this.n = n;
        children = new ArrayList<GraphicalModelNode>();
        parents = new Vector(0, 2);

        name = "" + index;
        this.index = index;
        visited = false;
        // if(node.state != null) this.state = new FastSet(node.state);
        state = new FastSet(n * 2);
        state.add(n + index()); // Agregamos el nodo a la lista de sus
        // predecesores.
    }

    public GraphNode(int n, int index, int compareMode) {
        children = new ArrayList<GraphicalModelNode>();
        parents = new Vector(0, 2);
        name = "" + index;
        this.n = n;
        this.index = index;
        visited = false;
        // if(node.state != null) this.state = new FastSet(node.state);
        state = new FastSet(n * 2);
        state.add(index()); // Agregamos el nodo a la lista de sus
        // predecesores.
        if (compareMode == COMPARE_BY_G || compareMode == COMPARE_BY_LOCAL_G || compareMode == COMPARE_BY_AVERAGE_G || compareMode == COMPARE_BY_PRIORITY) {
            this.compareMode = compareMode;
        } else {
            throw new RuntimeException("Invalid compare method. It must be on of GraphNode constants!");
        }

    }

    public GraphNode(GraphNode node) {
        this.n = node.n;
        this.name = node.name;
        this.index = node.index;
        this.visited = node.visited;
        this.children = new ArrayList<GraphicalModelNode>(node.children);
        this.parents = new Vector(node.parents);
        this.localG = node.localG;
        this.g = node.g;
        this.truth_value = node.truth_value;
    }

    public GraphNode(GraphNode node, int numberOfAttributes) {
        this.name = node.name;
        this.index = node.index;
        this.visited = node.visited;
        this.children = new ArrayList<GraphicalModelNode>(node.children);
        this.parents = new Vector(node.parents);
        this.localG = node.localG;
        this.g = node.g;

        state = new FastSet(numberOfAttributes * 2);
        state.add(numberOfAttributes + index()); // Agregamos el nodo a la lista
        // de sus predecesores.
    }

    public String toString() {
        if(parent != null){
            return "{[" + index + (truth_value ? "I" : "D") + "], priority: " + getPriority() + ", parent: " + parent.index + (parent.getTruthValue() ? "I" : "D") + " }";
        }else{
            return "{[" + index + (truth_value ? "I" : "D") + "], priority: " + getPriority() + ", parent: null }";
        }
    }

    /**
     * Retrieves the name of the node with a comma separated list of the
     * children names;
     */
    public String toStringWithNeighbors() {
        String aux;
        int TAB = 15;

        if (name.length() > TAB)
            aux = name.substring(0, TAB);
        else {
            aux = name;
            for (int i = 0; i < TAB - name.length(); i++)
                aux += " ";
        }
        String out = toString() + " : ";
        for (int i = 0; i < outDegree(); i++) {
            aux = "" + getChild(i).index();
            if (aux.length() > TAB)
                aux = aux.substring(0, TAB);
            else {
                for (int j = 0; j < TAB - aux.length(); j++)
                    aux += " ";
            }
            out += aux + " ";
        }
        return out;
    }

    /**
     * Number of children.
     */
    public int outDegree() {
        return children.size();
    }

    public int numChildren() {
        return children.size();
    }

    /**
     * Number of parents.
     */
    public int inDegree() {
        return parents.size();
    }

    public int numParents() {
        return parents.size();
    }

    public int degree() {
        return parents.size() + children.size();
    }

    final public ArrayList<GraphicalModelNode> adjacencyList() {
        return children;
    };


    /**
     * @return A instance of Set with the neighborshood of node
     */
    public final Set adjacencySet() {
        // Get children and to tranform it at a instance of Vector. Later to
        // tranform it at a instance of Set.
        Vector vector = new Vector();
        vector.addAll(children);

        //Set neighbors;
        return new Set(vector);
    }

    /**
     * @return A instance of Set with the neighborshood of node
     */
    public final Set neighbors() {
        return adjacencySet();
    }

    /**
     * Does not maintain the parent list of input node "child". <!-- --> This
     * should be done externally.
     */
    public void addChild(GraphicalModelNode child) {
        boolean exists = false;
        for (int i = 0; i < children.size(); i++) {
            if (child.index == ((GraphNode) children.get(i)).index) {
                exists = true;
            }
        }

        if (!exists) {
            children.add(child);
        }
    }

    public void myAddChild(GraphicalModelNode child) {
        children.add(child);
    }

    /**
     * Does not maintain the parent list of input node "child". <!-- --> This
     * should be done externally.
     */
    public boolean removeChild(GraphNode child) {
        return children.remove(child);
    }

    /**
     * Does not maintain the child list of input node "parent". <!-- --> This
     * should be done externally.
     */
    public void addParent(GraphicalModelNode parent) {
        parents.addElement(parent);
    }

    /**
     * Does not maintain the child list of input node "parent". <!-- --> This
     * should be done externally.
     */
    public boolean removeParent(GraphNode parent) {
        return parents.remove(parent);
    }

    /**
     * Retrieves the GraphNode stored at the index-th position in the children
     * vector
     */
    public GraphicalModelNode getChild(int index) {
        return children.get(index);
    }

    /**
     * Retrieves the name of the node;
     */
    public String name() {
        return name;
    }

    public int index() {
        return index;
    };

    /**
     * Set the name
     */
    public void name(String newName) {
        name = newName;
    }

    /**
     * Retrieves the GraphNode stored at the index-th position in the parents
     * vector
     */
    public GraphNode getParent(int index) {
        return (GraphNode) parents.elementAt(index);
    }

    /**
     * Returns the first appearence of node u in the children list.<!-- --> If
     * it is not a child, it will return -1;
     */
    public int indexOfChild(GraphNode u) {
        return children.indexOf(u);
    }

    /**
     * Returns the first appearence of node u in the parent list.<!-- --> If it
     * is not a parent, it will return -1;
     */
    public int indexOfParent(GraphNode u) {
        return parents.indexOf(u);
    }

    /**
     * Returns true if there is an edge from the node to input node u.
     */
    final public boolean existEdge(GraphNode u) {
        if (equals(u))
            return true;

        for (int i = 0; i < children.size(); i++) {
            if (getChild(i).index == u.index)
                return true;
        }
        // return (indexOfChild(u) != -1);
        return false;
    }

    /**
     * @return the parents
     */
    public FastSet getState() {
        return state;
    }

    public FastSet getS() {
        FastSet condSet = new FastSet(n);
        for (int j = 0; j < n; j++) {
            if (state.isMember(j + n))
                condSet.add(j);
        }
        // condSet.remove(index);
        return condSet;
    }

    /**
     * @param parents
     *            the parents to set
     */
    public void setState(FastSet predecessors) {
        this.state = predecessors;
    }

    /**
     * It goes upward the graph (in the direction of the parents) in a DFS
     * fashion.<!-- --> If an ancestor is visited twice, then it returns
     * false.<!-- --> Before calling this method, the calling function should
     * reset all the visited flags of the ancestors using
     * ancestorsVisitedReset().
     */

    public boolean existDoublePath() {
        if (visited)
            return true;
        visited = true;

        for (int i = 0; i < inDegree(); i++) {
            GraphNode p = getParent(i);
            if (p.existDoublePath())
                return true;
        }
        return false;
    }

    /** Creates a new node with equal characteristics. */
    public void makeCopyFrom(GraphNode old) {
        name = old.name;
        visited = false;
    }

    /**
     * @return the children
     */
    public ArrayList<GraphicalModelNode> getChildren() {
        return children;
    }

    /**
     * Find the children in clique of node caller. This assumption
     * that at the clique is node caller
     *
     * @param clique Clique to find the children of node
     * @return the children into <code>clique</code>
     */
    public ArrayList<GraphicalModelNode> getChildrenIntoClique(Set clique) {
        ArrayList<GraphicalModelNode> childrenInClique =
            new ArrayList<GraphicalModelNode>();

        for (Iterator i = clique.iterator(); i.hasNext();)
            for (Iterator j = ((Set)i.next()).iterator(); j.hasNext();) {
                GraphicalModelNode node = (GraphicalModelNode) j.next();
                // If child is equal a this node then continue
                if (this.compareTo(node) == 0) continue;
                // node in clique is a child at this node?
                if (this.children.contains(node)) childrenInClique.add(node);
            }

        return childrenInClique;
    }

    /**
     * @param children
     *            the children to set
     */
    public void setChildren(ArrayList<GraphicalModelNode> children) {
        this.children = children;
    }

    /**
     * @return the g
     */
    public Double getG() {
        return g;
    }

    /**
     * @param g
     *            the g to set
     */
    public void setG(Double g) {
        this.g = g;
    }

    /**
     * @return the parent
     */
    public GraphicalModelNode getParent() {
        return parent;
    }

    /**
     * @param parent
     *            the parent to set
     */
    public void setParent(GraphicalModelNode parent) {
        this.depth = parent.getDepth() + 1;
        this.parent = parent;
    }

    /**
     * @return the priority
     */
    public double getPriority() {
        return priority;
    }

    /**
     * @param priority
     *            the priority to set
     */
    public void setPriority(double priority) {
        this.priority = priority;
    }

    /**
     * @param test
     */
    public void setTruthValue(boolean test) {
        setTruthValue(test, false);
    }

    /**
     * @param test
     */
    public void setTruthValue(boolean test, boolean notAddToBlanket) {
        this.truth_value = test;
        if (!notAddToBlanket) {
            // if(test && this.state.isMember(index+n)){
            if (test) {
                this.state.remove(index() + n); // si lo seteamos independiente,
                // y estaba como dependiente,
                // removemos del blanket
            } else {
                this.state.add(index() + n); // si lo seteamos dependiente
                // encendemos el
            }
            // bit correspondiente en el blanket.
        }
    }

    /**
     * @param test
     */
    public boolean getTruthValue() {
        return truth_value;
    }

    /**
     * @return the blanket
     */
    public Vector<GraphicalModelNode> getBlanket() {
        return blanket;
    }

    /**
     * @param blanket
     *            the blanket to set
     */
    public void setBlanket(Vector<GraphicalModelNode> blanket) {
        this.blanket = blanket;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(GraphNode graphNode) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        if (compareMode == COMPARE_BY_G) {
            if (this.getG() < graphNode.getG()) {
                return BEFORE;
            } else if (this.getG() > graphNode.getG()) {
                return AFTER;
            } else {
                if (this == graphNode) {
                    return EQUAL;
                } else
                    return BEFORE;
            }
        } else if (compareMode == COMPARE_BY_LOCAL_G) {
            if (this.getLocalG() < graphNode.getLocalG()) {
                return BEFORE;
            } else if (this.getLocalG() > graphNode.getLocalG()) {
                return AFTER;
            } else {
                if (this == graphNode) {
                    return EQUAL;
                } else
                    return BEFORE;
            }
        } else if (compareMode == COMPARE_BY_AVERAGE_G) {
            if (this.getAvgG() < graphNode.getAvgG()) {
                return BEFORE;
            } else if (this.getAvgG() > graphNode.getAvgG()) {
                return AFTER;
            } else {
                if (this == graphNode) {
                    return EQUAL;
                } else
                    return BEFORE;
            }
        } else if (compareMode == COMPARE_BY_PRIORITY) {
            if (this.getPriority() < graphNode.getPriority()) {
                return BEFORE;
            } else if (this.getPriority() > graphNode.getPriority()) {
                return AFTER;
            } else {
                if (this == graphNode) {
                    return EQUAL;
                } else
                    return BEFORE;
            }
        } else {
            throw new RuntimeException("Invalid value in compareMethod field");
        }
    }

    /**
     * @return the localG
     */
    public Double getLocalG() {
        return localG;
    }

    /**
     * @param localG
     *            the localG to set
     */
    public void setLocalG(Double localG) {
        this.localG = localG;
    }

    /**
     * @return the depth
     */
    public int getDepth() {
        return depth;
    }

    /**
     * @param depth
     *            the depth to set
     */
    public void setDepth(int depth) {
        this.depth = depth;
    }

    /**
     * @return
     */
    public Double getAvgG() {
        return avgG;
    }

    /**
     * @param avgG
     */
    public void setAvgG(Double avgG) {
        this.avgG = avgG;
    }

}