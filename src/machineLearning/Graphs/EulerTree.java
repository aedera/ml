/*
 *	 How to make more efficient:
 *	maintain pointers to first and last element. Used (as far as I remember) in join, and 
 *  obviously in treeMaximum and treeMinimum.
 * 
 * 
 */
package machineLearning.Graphs;

import java.util.BitSet;
import machineLearning.Graphs.GraphsSimple.*;
//import machineLearning.Graphs.RBTree.RBNode;
import machineLearning.Graphs.RBTree;
//import machineLearning.DataStructures.*;

/**
 * @author bromberg
 *
 */
public final class EulerTree extends RBTree{
	UGraphSimple spanningT;
	static int n;	//number of nodes in ST
	
	static eulerNode nil = null;
	BitSet visited = new BitSet();
	
	//occ[index of (a,b)][0] is first occurence of a in edge
	//occ[index of (a,b)][1] is first occurence of b in edge
	//occ[index of (a,b)][2] is second occurence of a in edge
	//occ[index of (a,b)][3] is second occurence of b in edge
	//occurences of b are consecutive to those of a.
	//The index of (a,b) is  min(a,b)*n + max(a,b)
	eulerNode [][] treeOcc;	 
	eulerNode [] activeOcc;
	
///////////////////////////////////////////////////////
///////////  CONSTRUCTOR/S //////////////
/////////////////////////////////////////////////////
	public EulerTree(int n){
		super(true); 
		if(nil == null) {
			nil = new eulerNode();
			nil.red = false;
			nil.key = -2;
			nil.left = null;
			nil.right = null;
			nil.p = null;
		}
		RBTree.nil = nil; 
			
		int m = n*(n-1);
		activeOcc = new eulerNode[n];
		
		treeOcc = new eulerNode[m][];
		for(int i=0; i<m; i++){
			treeOcc[i] = new eulerNode[4];
		}
	}
	public EulerTree(UGraphSimple ST, int rootIndex, EulerTree forest){
		super(true); 
		if(nil == null) {
			nil = new eulerNode();
			nil.red = false;
			nil.key = -2;
			nil.left = null;
			nil.right = null;
			nil.p = null;
		}
		RBTree.nil = nil; 
			
		this.activeOcc = forest.activeOcc;
		this.treeOcc = forest.treeOcc;
		
		//creates occ of root node
		RBTree singleton = new RBTree();
		eulerNode new_occ = new eulerNode(rootIndex);
		new_occ.active = true;
		activeOcc[rootIndex] = new_occ;
		singleton.insert(new_occ);
		this.setRoot(new_occ);
		
		//toString();
		visited.clear();
		eulerTreeConstruction(ST, rootIndex);
	}

	public EulerTree(RBTree T, eulerNode [][] edgeOcc, eulerNode [] activeOcc){
		this.setRoot(T.root);
		this.treeOcc = edgeOcc;
		this.activeOcc = activeOcc;
		//this.anyOcc = anyOcc;
	}
		
///////////////////////////////////////////////////////
///////////  PUBLIC METHODS  //////////////
/////////////////////////////////////////////////////
	/**
	 * deletes edge (a,b) from T: Let T1 and T2 be the two trees which result,
	 * where a \in T1 and b \in T2. Let oa1, ob1, oa2, ob2 represent the ocurrences 
	 * encountered in the two traversals of (a,b). If oa1<ob1 and ob1<ob2, then oa1 <
	 * ob1 < ob2 < oa2. Thus ET(T2) is given by the interval of ET(T) ob1, ...., ob2
	 * and ET(T1) is given by splicing out of ET(T) the sequence ob1, ...., ob2. 
	 */
	public final EulerTree [] cutST(int a, int b){
		int e = ((a<=b) ? a : b)*n + ((a>b) ? a : b);  //min(a,b)*n + max(a,b)
		
		//get the nodes representing edge (a,b)
		eulerNode oa1 = treeOcc[e][0];
		eulerNode oa2 = treeOcc[e][1];
		eulerNode ob1 = treeOcc[e][2];
		eulerNode ob2 = treeOcc[e][3];
		
		//set the treeOcc to nil
		treeOcc[e][0] = null;
		treeOcc[e][1] = null;
		treeOcc[e][2] = null;
		treeOcc[e][3] = null;
		
		//sort oa1, oa2, ob1, and ob2 s.t. oa1 < ob1 < ob2 < oa2 in in-order
		//if they're not nil.
		//eb1 may b nil
		eulerNode aux;
		if(oa1 != null && oa2 != null){
			if(smaller(oa2, oa1)){
				aux = oa1; oa1 = oa2; oa2 = aux;
			}
		}
		else{		//either oa1 or oa2 are nil
			if(oa1 != null) {//.. it is oa2
				oa2 = oa1; oa1 = null;
			}
		}
		if(ob1 != null && ob2 != null){
			if(smaller(ob2, ob1)){
				aux = ob1; ob1 = ob2; ob2 = aux;
			}
		}
		else{
					//either ob1 or ob2 is nil
			if(ob1 != null){
				ob2 = ob1; ob1 = null;
			}
		}
		//now oa2 and ob2 are non-nil
		if(smaller(oa2, ob2)){
			aux = ob1; ob1 = oa1; oa1 = aux;
			aux = ob2; ob2 = oa2; oa2 = aux;
		}
		
		//update trees
		RBTree [] TT = split(oa1, true);	///{.....,oa1}{ob1,......}
		RBTree [] TT2 = TT[1].split(oa2, false); //{ob1,.....,ob2}{oa2,.....}
		RBTree [] TT3 = TT2[1].split(oa2, true); 
			
		TT[0].join(TT3[1]);
		TT[1] = TT2[0];
		
		//update active occurences
		if(oa2.active) passActivity(oa2, oa1);
		
		// update treeOcc
		//update treeOcc of the edge following e if it exists
		int k;
		eulerNode eAfter_node = oa2.rightOcc;
		int aAfter = oa2.STindex;
		if(eAfter_node != null){
			int bAfter = eAfter_node.STindex;
			int eAfter = ((aAfter<=bAfter)?aAfter : bAfter)*n + ((aAfter>bAfter) ? aAfter : bAfter); 
			if(oa1.leftOcc != eAfter_node){
				//replace oa2 by oa1
				for(k=0 ; oa2 != treeOcc[eAfter][k]; k++);
				treeOcc[eAfter][k] = oa1;
			}
			else{
				//replae oa2 by nil
				for(k=0 ; oa2 != treeOcc[eAfter][k]; k++);
				treeOcc[eAfter][k] = null;
			}
		}
		//update edge occurences
		oa1.rightOcc = oa2.rightOcc;
		if(oa2.rightOcc != null) oa2.rightOcc.leftOcc = oa1;
		
		if(ob1 != null) ob1.leftOcc = null;
		else           ob2.leftOcc = null;
		
		if(ob2 != null) ob2.rightOcc = null;
		else           ob1.rightOcc = null;
		
		
		EulerTree [] ET = new EulerTree[2];
		ET[0] = new EulerTree(TT[0], treeOcc, activeOcc);
		ET[1] = new EulerTree(TT[1], treeOcc, activeOcc);
		
		return ET;
	}
	
	/**
	 * Let s = from, t = to. Then, let os denote any occurence of s (first here).
	 * Splice out the first part of the sequence ending with the occurence before os, 
	 * remove its first occurence (or), and tack this on to the end of the 
	 * sequence which now begins with os. Add a new occurence os to the end.
	 */
	public final void changeSTRoot(eulerNode to){
		int k;
		
//FIRST: update tree occurences		
		eulerNode first = (eulerNode) treeMinimum(root);
		eulerNode last = (eulerNode) treeMaximum(root);
		
		if(to == first) return;
		
		eulerNode new_occ = new eulerNode(to);
		
		if(first.active) passActivity(first, last);
			
		int leftIndex = to.leftOcc.STindex;
		if(to.leftOcc == to.rightOcc){  //to is a leaf in  ST
			//to.STindex;
			for(k=0; null != treeOcc[leftIndex][k]; k++){
				int kasdlfja = 0;
			}
			treeOcc[leftIndex][k] = new_occ;
		}
		else{
			for(k=0; to != treeOcc[leftIndex][k]; k++){
				int kasdlfja = 0;
			}
			treeOcc[leftIndex][k] = new_occ;
		}
		eulerNode firstEdge = first.rightOcc;
		if(firstEdge != last.rightOcc   ||  to == last){
			for(k=0; first != treeOcc[firstEdge.STindex][k]; k++){
				int kasdlfja = 0;
			}
			treeOcc[firstEdge.STindex][k] = last;
		}
		else{
			for(k=0; first != treeOcc[firstEdge.STindex][k]; k++){
				int kasdlfja = 0;
			}
			treeOcc[firstEdge.STindex][k] = null;
		}
		
//SECOND: update edge occurences		
		last.rightOcc = first.rightOcc;
		first.rightOcc.leftOcc = last;
		
		new_occ.leftOcc = to.leftOcc;
		to.leftOcc.rightOcc = new_occ;
		
		to.leftOcc = null;
		
		//split off first occurence and delete it
		RBTree [] TT1 = split(first,true);
		first = null;
		
		//split immediately before to
		RBTree [] TT2 = TT1[1].split(to, false);
		
		TT2[1].join(TT2[0]);
		
		this.setRoot(TT2[1].root);
		
		insert(new_occ, treeMaximum(root), true);
	}
	
	/**
	 * Links euler trees ETa and ETb by edge (a,b) in ST,
	 * where a and b are indexes of nodes in the ST.
	 * 
	 * According to Henzinger&King:
	 * Given occurences oa and ob, reroot ETb at b, create
	 * a new occurence oan and splice the sequence ETb:oan into ETa, 
	 * inmediately after oa.
	 * 
	 * @return it returns nothing, but ETa stores the joined tree.
	 */
	public static final  void linkST(EulerTree ETa, EulerTree ETb, int a, int b){
		EulerTree global = ETa;	//ETa and ETb has same activeOcc and treeOcc pointers
		
		//Find active occurences of u and v, and create new occ of u
		eulerNode a_act = global.activeOcc[a];
		eulerNode b_act = global.activeOcc[b];
		eulerNode new_a_occ = global.new eulerNode(a_act);
		
		//Change root to v_act
		ETb.changeSTRoot(b_act);
		
		//Initialize tree_occs of new node (u,v)
		int e = ((a<=b) ? a : b)*n + ((a>b) ? a : b);  //min(a,b)*n + max(a,b)
		global.treeOcc[e][0] = a_act;
		global.treeOcc[e][1] = new_a_occ;
		
		//the first and the last node of ETb are treeOcc[edgeIndex][2 and 3] if
		//they are different, otherwise treeOcc[edgeIndex][2] is nil
		eulerNode etb_last = (eulerNode) ETb.treeMaximum(ETb.root);
		global.treeOcc[e][3] = etb_last;
		if(etb_last != b_act) 	global.treeOcc[e][2] = b_act;
		else 					global.treeOcc[e][2] = null;//nil;
		 
		//Update tree occurences of edge following e if it exists
		eulerNode edgeAfter_e = a_act.rightOcc;
		int aAfter = a;
		int bAfter;
		if(edgeAfter_e != null){
			bAfter = edgeAfter_e.STindex;
			int eAfter = ((aAfter<=bAfter)?aAfter : bAfter)*n + ((aAfter>bAfter) ? aAfter : bAfter); 
			if(a_act.leftOcc != edgeAfter_e){
				//replace u_act by new_u_occ
				int k;
				for(k=0; a_act != global.treeOcc[eAfter][k]; k++);
				global.treeOcc[eAfter][k] = new_a_occ;
			}
			else{
//				replace nil pointer by new_u_occ
				int k;
				for(k=0; null != global.treeOcc[eAfter][k]; k++){
					int kksadf=0;
				}
				global.treeOcc[eAfter][k] = new_a_occ;
			}
		}
		
		//update edge occurences
		new_a_occ.rightOcc = a_act.rightOcc;
		if(a_act.rightOcc != null) a_act.rightOcc.leftOcc = new_a_occ;
		
		new_a_occ.leftOcc = etb_last;
		etb_last.rightOcc = new_a_occ;
		
		a_act.rightOcc = b_act;
		if(b_act != null) b_act.leftOcc = a_act;
		
		//update trees
		//ETb.changeSTRoot(b_act); //done above
		ETb.insert(new_a_occ, ETb.treeMaximum(ETb.root), true);
		RBTree [] TT1 = ETa.split(a_act, true);
		//concatenate the pieces
		TT1[0].join(ETb.join(TT1[1]));
		
		ETa.setRoot(TT1[0].root);
		
	}

	//Can be called from any tree in the forest
	public final boolean separated(int a, int b){
		return getRoot(activeOcc[a]) == getRoot(activeOcc[b]);
	}
	public String toString(){return super.toString();};
///////////////////////////////////////////////////////
///////////  PRIVATE METHODS  //////////////
/////////////////////////////////////////////////////
	protected class eulerNode extends RBNode{
		public int STindex;		//pointer to the node in the MN (or ST or G).
		//public DoublyLinkedListElement eulerSequenceElement;	//pointer to its location in the DlinkedList eulerSequence
		eulerNode leftOcc, rightOcc;
		boolean active = true;
		
		eulerNode(){
			STindex = -1;
			leftOcc = null;
			rightOcc = null;
		}
		eulerNode(int index){
			super();
			
			STindex = index;
			//edgeOcc = new RBTree.RBNode[2];
			leftOcc = null;//T.new RBNode(null);
			rightOcc = null;//T.new RBNode(null);
		}
		eulerNode(eulerNode node){
			super();
			
			active = false;
			STindex = node.STindex;
			leftOcc = null;
			rightOcc = null;
			boolean aaa = nil.active;
		}
		
		public String toString(){
			String label = "";//+key;
			//label += "("+ STindex +")";
			label += STindex;
			//else label = ;
			
			if(red) label = "."+label+".";
			return label;
		};

	}

	private final void passActivity(eulerNode from, eulerNode to){
		to.active = true;
		from.active = false;
		activeOcc[to.STindex] = to;
	}
	
	/**
	 * Populates the tree with each visit of each node in an Euler Tour of a spanning tree ST:
	 * 
	 * ET(x)
	 * 	visit x;  (or insert)
	 * 	for each child c of x do
	 * 		ET(c);
	 * 		visit x;	(or insert)
	 * 
	 */
	private void eulerTreeConstruction(UGraphSimple ST, int a){
		visited.set(a);
		
//		int card = ST.degrees[a];
		int b;
		
		//Only go from a to b if a<b
//		for(int i=0; i<card; i++){
//			b = ST.adjList[a][i];
//			if(visited.get(b)) continue;
//				
//			RBTree singleton = new RBTree();
//			eulerNode new_occ = new eulerNode(b);
//			new_occ.active = true;
//			activeOcc[b] = new_occ;
//			singleton.insert(new_occ);
//			
//			EulerTree singleton_et = new EulerTree(singleton, treeOcc, activeOcc);
//			
//			linkST(this, singleton_et, a, b);
//			
//			eulerTreeConstruction(ST, b);
//		}
	}

	public UGraphSimple reconstructST(int n){
		UGraphSimple G = new UGraphSimple(n);
		
		eulerNode x = (eulerNode) treeMinimum(root);
		eulerNode y = nil;
		if(x != nil) y = (eulerNode) treeSuccessor(x);
		
		int xIndex, yIndex;
		while(y != RBTree.nil){
			xIndex = x.STindex;
			yIndex = y.STindex;
		
			G.addEdge(xIndex, yIndex);
			x = y;
			y = (eulerNode) treeSuccessor(x);
		}
		
		return G;
	}

///////////////////////////////////////////////////////
///////////  MAIN  //////////////
/////////////////////////////////////////////////////
	
	public static void main(String[] args) {
		UGraphSimple G = new UGraphSimple(7);
		G.addEdge(0,3);
		G.addEdge(2,3);
		G.addEdge(1,3);
		G.addEdge(0,4);
		G.addEdge(4,5);
		G.addEdge(4,6);
		
		EulerTree forest = new EulerTree(G.size());
		EulerTree et = new EulerTree(G, 0, forest);
		et.assertConsistency(false);
		System.out.println(et + "\n ------------------------------ \n");

		EulerTree [] ets = et.cutST(0,4);
		ets[0].assertConsistency(false);
		ets[1].assertConsistency(false);
		System.out.println(ets[0] + "\n ------------------------------ \n");
		System.out.println(ets[1] + "\n ------------------------------ \n");
		
		EulerTree.linkST(ets[0], ets[1], 3, 4); 
		ets[0].assertConsistency(false);
		EulerTree [] ets2 = ets[0].cutST(0,3); 

		UGraphSimple Gp = ets[0].reconstructST(G.size());
		System.out.println(Gp + "\n ------------------------------ \n");
		Gp = ets[1].reconstructST(G.size());
		System.out.println(Gp + "\n ------------------------------ \n");

		EulerTree.linkST(ets[0],ets[1], 0, 1);
		
//		UGraphSimple Gp = et.reconstructST(G.size());

	}
}
