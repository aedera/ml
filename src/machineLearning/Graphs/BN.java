package machineLearning.Graphs;

/*******************************************************************************
 * 
 * Abstract Bayesian network class.
 * 
 * (C) 2006 Dimitris Margaritis, All Rights Reserved.
 * 
 ******************************************************************************/

import java.io.*;
import java.util.*;

//import weka.classifiers.bayes.net.BayesNetGenerator;

import machineLearning.Datasets.Statistics;
//import machineLearning.Utils.Utils.IntegerDoublePair;

/*-------------------------------------------------------------------------*/

public class BN {
	static public boolean print = false;

	/* Number of nodes in the BN. */
	protected int nvars;

	/* Parents of each node. */
	protected int[][] parents;

	/* Number of states per node */
	int nvals[];

	/* Positions in a 2D area */
	int positions[][];

	/* Sampled Data File Name */
	public String sampledDataFileName;

	/*
	 * array of hashtables, one for each node. Each hashtable is indexed by a
	 * List of values, one for each parent, and returns an array of doubles
	 * representing the conditional probability table for this node and this
	 * parent configuration.
	 */
	protected ArrayList<HashMap<List<Integer>, double[]>> probs;

	/*
	 * Data set associated with this BN. Elements in the table are integers
	 * corresponding to words enouuntered in the data file.
	 */
	protected int[][] dataset;

	/*
	 * Size of "dataset" i.e., number of tuples in it.
	 */
	protected int ndata;

	/*
	 * nvals[var] is the number of values of variable "var".
	 */
	//protected int[] nvals;

	/*
	 * Bayesian test class used for all access to data.
	 */
	// protected StatisticalTest st;
	public boolean verbose; /*
							 * Whether to print informational messages or not.
							 */

	enum StatisticalTestType {
		CHISQUARE, BAYESIAN, GSQUARE
	};

//	public BN(int n) {
//		nvars = n;
//		parents = new boolean[nvars][nvars];
//		probs = new ArrayList<HashMap<List<Integer>, double[]>>(nvars);
//		
//		verbose = false;
//	}
	public BN(BN bn) {
		nvars = bn.size();
		parents = new int[nvars][];
		probs = new ArrayList<HashMap<List<Integer>, double[]>>(nvars);
		
		verbose = false;
		
		for(int x=0; x<nvars; x++){
			parents[x] = new int[bn.parents[x].length];
			for(int y=0; y<nvars;y++){
				if(x==y) continue;
				if(bn.isParent(y, x)) makeParent(y, x);
			}
		}
	}

	public BN(String bnfile) {
		readFromFile(bnfile);
		verbose = false;
	}

	/**
	 * Save a BN to a file.
	 */

	public void saveToFile(String fileName) {
		try {
			PrintStream outputFile = new PrintStream(new FileOutputStream(
					fileName), true);

			outputFile.println("# Bayesian Network.\n");
			outputFile.println("#\t Format:\n");
			outputFile.println("#\t<!-- Number of Variables -->");
			outputFile.println("#\t\t number_of_variables\n");
			outputFile.println("#\t<!-- DAG -->");
			outputFile
					.println("#\t\t var_name of var 1 : list of parent's var_name separated by spaces");
			outputFile
					.println("#\t\t var_name of var 2 : list of parent's var_name separated by spaces");
			outputFile.println("#\t\t ...");
			outputFile
					.println("#\t\t var_name of var n : list of parent's var_name separated by spaces\n");
			outputFile.println("#\t<!-- Number of States -->");
			outputFile
					.println("#\t\t number_of_states : num_states_var_1 number_states_var_2 ... number_states_var_n\n");
			outputFile.println("#\t<!-- Positions -->");
			outputFile
					.println("#\t\t positions : pos_x_1 pos_y_1 pos_x_2 pos_y_2 ... pos_x_n pos_y_n\n");
			outputFile.println("#\t<!-- Probability Distributions -->");
			outputFile
					.println("#\t\t var_name of var 1 : 1st parents_configuration : distribution");
			outputFile.println("#\t\t ...");
			outputFile
					.println("#\t\t var_name of var 1 : last parents_configuration : distribution");
			outputFile.println("#\t\t ... \n");
			outputFile
					.println("#\t\t var_name of var n : 1st parents_configuration : distribution");
			outputFile.println("#\t\t ...");
			outputFile
					.println("#\t\t var_name of var n : last parents_configuration : distribution");

			double x, y;
			//int nParents[] = new int[nvars];

			outputFile.println("#<!-- Number of Variables -->");
			outputFile.println(nvars + "\n");

			outputFile.println("#<!-- DAG -->");
			for (int i = 0; i < nvars; i++) {
				outputFile.print(i + " : ");

				for (int j = 0; j < parents[i].length; j++) {
					//if (parents[i][j]) {
						outputFile.print(parents[i][j] + " ");
						//nParents[i] = nParents[i] * nvals[j];
					//}
				}
				outputFile.print("\n");
			} // end of for DAG
			outputFile.println("");

			outputFile.println("#<!-- Number of States -->");
			outputFile.print("number_of_states : ");
			for (int j = 0; j < nvars; j++) {
				outputFile.print(nvals[j] + " ");
			}
			outputFile.println("\n");

			if(positions!= null){
				outputFile.println("#<!-- Positions -->");
				outputFile.print("positions : ");
				for (int j = 0; j < nvars; j++) {
					if(positions[j] != null) outputFile.print(positions[j][0] + " " + positions[j][1] + " ");
				}
			}
			outputFile.println("\n");

			outputFile.println("#<!-- Probability Distributions -->");
			ArrayList<Integer> cond = new ArrayList<Integer>(nvars);
			for (int i = 0; i < nvars; i++) {
				HashMap<List<Integer>, double[]> probs_i = probs.get(i);

				int m = 0;
				int parents_i[] = new int[nvars];

				for (int j = 0; j < parents[i].length; j++) {
					//if (parents[i][j]) {
						parents_i[m] = parents[i][j];
						m++;
					//}
				}
				int counter[] = new int[m];
				outerWhile: while (true) {
					cond.clear();
					outputFile.print(i + " : ");
					for (int k = 0; k < m; k++) {
						cond.add(counter[k]);
						outputFile.print(counter[k] + " ");
					}
					outputFile.print(": ");

					double distr[] = probs_i.get(cond); // df.generateUniformDistribution(numberStates[i],"emFloat");
					for (int k = 0; k < nvals[i]; k++) {
						outputFile.print(distr[k] + " ");
						// distribution[cont]=distr[k];
						// cont++;
					}
					outputFile.println("");

					int j = 0;
					do {
						if (j == m)
							break outerWhile;
						counter[j]++;
						int asdf = parents_i[j];
						counter[j] = counter[j] % nvals[parents_i[j]];
						j++;
					} while (counter[j - 1] == 0);
				}
				outputFile.println("#");
			} // end of for
			outputFile.println("");

			outputFile.println("#<!-- Sampled Data File Name -->");
			outputFile.println("sampled_data_file_name : "
					+ sampledDataFileName);
			outputFile.println("\n");
		} catch (IOException e) {
		}
	}
   /** ---------------------------------------------*/
	/**
	 * Read a BN from a file.
	 */
	public void readFromFile(String bn_filename) {
		//double parentsInts [][] = null;  //needed because the other parents forgets the ordering of the parents.
		
		StringTokenizer st;

		Scanner sc = null;
		try {
			sc = new Scanner(new File(bn_filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		String line, header="", token;

		while(sc.hasNextLine()){
			//-------------------------------------------------------
			/** Read header and skip lines with comments*/
			if(header.isEmpty()){
				line = sc.nextLine();
				if(line.isEmpty()) continue;
				
				Scanner scl = new Scanner(line);
				
				/* Parsing  header #<!-- bla bla bla -->  */
				//Initialized after the content between headers has been processed
				if(scl.next().equals("#<!--")) {
					token = scl.next();
					while(!token.equals("-->")) {
						header += token + " ";
						token = scl.next();
					}
					header = header.substring(0, header.length()-1); //removes trailing space
				}
			}
			//-------------------------------------------------------
			/** Read Number of Variables */ 
			else if(header.equalsIgnoreCase("Number of Variables")){
				line = sc.nextLine();

				nvars = Integer.parseInt(line);
				if (print)
					System.out.println("Number of vars = " + nvars);
				parents = new int[nvars][];
				//parentsInts = new double[nvars][];
				//for(int i=0; i<nvars;i++)
					//for(int j=0; j<nvars;j++) parentsInts[i][j] = -1;
				probs = new ArrayList<HashMap<List<Integer>, double[]>>(nvars);
				for (int X = 0; X < nvars; X++) {
					probs.add(new HashMap<List<Integer>, double[]>());
				}
	
				header = "";
				continue;
			}
			//-------------------------------------------------------
			/** Read DAG (vars and parents) */
			else if(header.equalsIgnoreCase("DAG")){
				for (int i = 0; i < nvars; i++) {
					line = sc.nextLine();
					// Skip comments and empty lines
					if (line.length() == 0 || line.charAt(0) == '#') {
						i--;
						continue;
					}

					st = new StringTokenizer(line);
					int X = Integer.parseInt(st.nextToken()); // Vars names start
																// from 0.
					st.nextToken(); // Skip ":".
					if (print) System.out.print(X + " : ");
					int n=0;
					int aux [] = new int[nvars];
					if(X == 15){
						int asdfads=0;
					}
					while (st.hasMoreTokens()) {
						int Y = Integer.parseInt(st.nextToken());
						if (Y > nvars) {
							System.err.println("Error: variable " + Y + " out of range " + "[1, " + nvars + "].");
							System.exit(1);
						}
						if (print) System.out.print(Y + " ");
						aux[n++] = Y;
						//parents[X][Y] = true;
					}
					parents[i] = new int[n];
					for(int j=0; j<n; j++) parents[X][j] = aux[j];
					
					if (print)
						System.out.println();
				} // end for (X)
				if (print)
					System.out.println();
				
				header = "";
				continue;
			}
			//-------------------------------------------------------
			/** Read number of states */
			else if(header.equalsIgnoreCase("Number of States")){
				nvals = new int[nvars];
				line = sc.nextLine();

				st = new StringTokenizer(line);
				st.nextToken(); // Skip "number_of_states"
				st.nextToken(); // Skip ":".
				if (print)
					System.out.print("number_of_states : ");
				for (int i = 0; i < nvars; i++) {
					nvals[i] = Integer.parseInt(st.nextToken());
					if (print)
						System.out.print(nvals[i] + " ");
				}
				if (print)
					System.out.println("\n");

				header = "";
				continue;
			}
			//-------------------------------------------------------
			/** Read positions */
			else if(header.equalsIgnoreCase("Positions")){
				positions = new int[nvars][2];
				// Skip comments and empty lines
				while ((line = sc.nextLine()).length() == 0 || line.charAt(0) == '#') continue;
				
				st = new StringTokenizer(line);
				st.nextToken(); // Skip "positions"
				st.nextToken(); // Skip ":".
				if (print)
					System.out.print("positions : ");
				for (int i = 0; i < nvars; i++) {
					positions[i][0] = Integer.parseInt(st.nextToken());
					positions[i][1] = Integer.parseInt(st.nextToken());
					if (print)
						System.out.print(positions[i][0] + " " + positions[i][1]
								+ " ");
				}
				if (print)
					System.out.println("\n");

				header = "";
				continue;
			}
			//-------------------------------------------------------
			/** Read distributions */
			else if(header.equalsIgnoreCase("Probability Distributions")){
				token = "";
				line = sc.nextLine();
				while (sc.hasNextLine() && line != null && line.length() != 0) {
					st = new StringTokenizer(line);
					int X = Integer.parseInt(st.nextToken()); // Vars name
					st.nextToken(); // Skip ":".
					if (print) System.out.print(X + " : ");
					
					// Read configuration list 
					ArrayList<Integer> config = new ArrayList<Integer>(nvars);
					while (!(token = st.nextToken()).equals(":")) {
						int state = Integer.parseInt(token);
						config.add(state);
						if (print) System.out.print(state + " ");
					}
					if (print) System.out.print(": ");
					//Re-sort configuration list in ascending order of parents' number
					//int order  [] = sortIntArray(parentsInts[X]);
					
					
					// Read distribution for that configuration
					double[] distr = new double[nvals[X]];
					int i = 0;
					while (st.hasMoreTokens()) {
						distr[i] = Double.parseDouble(st.nextToken());
						if (print) System.out.print(distr[i] + " ");
						i++;
					}
					probs.get(X).put(config, distr);

					if (print) System.out.println("");
					
					if(sc.hasNextLine()) line = sc.nextLine();
					while(line.length() != 0 && line.charAt(0)=='#') line = sc.nextLine();
				}
				
				header = "";
				continue;
			}
			//-------------------------------------------------------
			/** Read Sampled Data File Name */
			else if(header.equalsIgnoreCase("Sampled Data File Name")){
				line = sc.nextLine(); 
				if (line != null) {
					st = new StringTokenizer(line);
					st.nextToken(); // Skip "sampled_data_file_name"
					st.nextToken(); // Skip ":".
					sampledDataFileName = st.nextToken();
					if (print)
						System.out.print("\nsampled_data_file_name : "
								+ sampledDataFileName);
					if (print)
						System.out.println("\n");
				}

				header = "";
				continue;
			}
		}
	}
	 static class IntegerDoublePair implements Comparable<IntegerDoublePair>{
		  int int_;
		  double double_;
		  public int compareTo(IntegerDoublePair x){
			  int aux = (int)((double_ - x.double_)*10E12);
			  return aux;
		  };
	  }	 
	  /**
	   * weights is overwritten with weights in proper order
	   * Sorts in ASCENDING ORDER
	   */
	  public static int [] sortIntArray(double [] w){
		  double [] weights = new double[w.length];
		  for(int i=0 ; i<w.length; i++) weights[i] = w[i];
		  
		  int length = weights.length;
		  IntegerDoublePair x [] = new IntegerDoublePair[length];
		  
		  for(int i=0; i<length; i++){
			  x[i] = new IntegerDoublePair();
			  x[i].int_ = i;
			  x[i].double_ = weights[i];
		  }
		  Arrays.sort(x);
		  
		  int ints [] = new int[length];
		  //double newWeights[] = new double[length];
		  for(int i=0; i<length; i++) {
			  ints[i] = x[i].int_;  
			  weights[i] = x[i].double_;
		  }
		  
		  return ints;
	  }

		
	/**
	 * Variable to String.
	 */
	public static String varname(int X) {
		return "" + (X);
	}

	/**
	 * Return the number of variables in the BN.
	 */
	public int numVars() {
		return nvars;
	}
	final public int size() {
		return nvars;
	}

	/**
	 * Return the array containing the number values for every variable in the
	 * BN.
	 */
	public int[] numVals() {
		return nvals;
	}

	/**
	 * Check if Y is parent of X in BN structure.
	 */
	public boolean isParent(int Y, int X) {
		for(int i=0; i<parents[X].length; i++) if(parents[X][i] == Y) return true;
		return false;
	}

	/**
	 * Make a node Y a parent of another node X.
	 */
	public void makeParent(int Y, int X) {
		int i;
		int [] auxparents = new int[parents[X].length + 1]; 
		for(i=0; i<parents[X].length; i++) auxparents[i] = Y;
		auxparents[i] = Y;
		parents[X] = auxparents;
	}
	/**
	 * Remove Y as a parent of X
	 */
//	public void removeParent(int Y, int X) {
//		for(i=0; i<parents[X].length; i++) auxparents[i] = Y;
//		auxparents[i] = Y;
//		parents[X] = auxparents;
//	}

	private int [] degrees(){
		int degrees[] = new int[nvars];
		
		//compute degrees
		for(int i=0; i<nvars;i++){
			for(int j=0; j<nvars;j++){
				if(i == j) continue;
				if(isParent(j, i)) degrees[i]++;
				if(isParent(i, j)) degrees[i]++;
			}
		}
		return degrees;
	}
	/** returns the total degree (in + out degree) of the edge with maximum total degree
 	 */
	public int maxDegree(){
		int degrees [] = degrees();
		int max = 0;
		for(int i=0; i<nvars;i++) if(degrees[i] > max) max = degrees[i];
		
		return max;
	}
	
	/** returns the average of the total degree (in + out degree) 
 	 */
	public double avgDegree(){
		int degrees [] = degrees();
		double avg = 0;
		for(int i=0; i<nvars;i++) avg += degrees[i];
		
		return avg/nvars;
	}
	
	/** returns the total number of arcs 
 	 */
	public double numEdges(){
		int arcs=0;
//		compute degrees
		for(int i=0; i<nvars;i++){
			for(int j=0; j<nvars;j++){
				if(i == j) continue;
				if(isParent(j, i)) arcs++;
			}
		}
		return arcs;
	}

	/**
	 * If Y was a parent of X, make node Y NOT a parent of X, and X the parent of Y.
	 * Else, do nothing.
	 */
	public void flip(int Y, int X) {
		if(isParent(Y, X)) {
			removeEdge(Y,X);
			makeParent(Y,X);
		}
	}
	/**
	 * Remove directed edge between Y and X, if such edge exist,
	 * do nothing otherwise
	 */
	public void removeEdge(int Y, int X) {
		int indexY;
		for(indexY=0; indexY<parents[X].length; indexY++) if(parents[X][indexY] == Y) break;
		
		int auxparents [] = new int[parents[X].length-1];
		for(int i=0; i<parents[X].length;i++) if(i != indexY) auxparents[i] = parents[X][i];
		
		parents[X]=auxparents;
	}

	/**
	 * Check if there exists a (directed) path in the BN structure between node
	 * X and ANY of the nodes in the boolean[] array Y.
	 */
	public boolean pathExists(int X, boolean[] Y) {
		return pathExistsDFS(X, Y, new boolean[nvars]);
	}

	/**
	 * Auxiliary function for pathExists(). Does a DFS search of the BN graph,
	 * remembering the visited nodes.
	 */

	private boolean pathExistsDFS(int X, boolean[] Y, boolean[] visited) {
		// Y reached. return true;
		if (Y[X] == true)
			return true; // X is already in Y.

		visited[X] = true;
		// Search for Y through X's children. */
		for (int W = 0; W < nvars; W++) {
			if (isParent(X, W) && !visited[W]) {
				if (pathExistsDFS(W, Y, visited))
					return true;
			}
		}
		return false;
	}

	/**
	 * Check if there exists a (directed) path in the BN structure between node
	 * X and SINGLE node Y.
	 */

	public boolean pathExists(int X, int Y) {
		boolean[] Yset = new boolean[nvars];
		Yset[Y] = true;
		return pathExists(X, Yset);
	}

	/**
	 * Check if there exists a (directed) cycle in the BN structure 
	 * starting at node X.
	 * It proceeds by checking if there is a (directed) path between any
	 * of X's children and X.
	 */

	public boolean cycleExists(int X) {
//		 Search for Y through X's children. */
		for (int W = 0; W < nvars; W++) {
			if (isParent(X, W)) {
				if (pathExists(W, X))
					return true;
			}
		}
		return false;
	}
	/**
	 * Print a BN to stdout.
	 */

	public void print() {
		System.out.println("Number of variables: " + nvars);
		for (int X = 0; X < nvars; X++) {
			System.out.print("Parents of cariable " + varname(X) + " : ");
			for (int Y = 0; Y < nvars; Y++)
				if (isParent(Y, X))
					System.out.print(varname(Y) + " ");
			System.out.println();
		}
	}
	/**
	 * Print a BN to stdout.
	 */

	public String toString() {
		return toString("#");
	}
	public String toString(String lineHeader) {
		String str = lineHeader+"Number of variables: " + nvars + "\n";
		for (int X = 0; X < nvars; X++) {
			str += lineHeader+varname(X) + " : ";
			for (int Y = 0; Y < nvars; Y++)
				if (isParent(Y, X))
					str += varname(Y) + " ";
			str += "\n";
		}
		return str;
	}
	/**
	 * Check if X is dependent with Y given set Z using the rules of
	 * d-separation.
	 */
	public boolean areIndep(int X, int Y, boolean[] Z) {
		boolean are_indep = !areDependentDFS(X, X, Y, Z,new boolean[nvars][nvars]);

		if (verbose)
			System.out.println("INDEP( " + varname(X) + " , " + varname(Y)
					+ " | " + setToString(Z) + " ) = "
					+ (are_indep ? "independent" : "dependent"));

		return are_indep;
	}

	/**
	 * Check if X is dependent with Y unconditionally using the rules of
	 * d-separation.
	 */
	public boolean areIndep(int X, int Y) {
		return areIndep(X, Y, new boolean[nvars]);
	}

	/**
	 * Depth-first search for a d-path from X to Y, given Z.
	 */
	private boolean areDependentDFS(int from, int X, int Y, boolean[] Z,
			boolean[][] visited) {
		if (X == Y) {
			// System.out.println("From " + varname(from) + " to " +
			// varname(X) + " : FOUND");
			return true;
		}

		// System.out.println("Visiting " + varname(X) + " coming from " +
		// varname(from));

		visited[from][X] = true;
		if (isParent(from, X)) { // Coming through edge "from" -> X.

			if (pathExists(X, Z)) { // Path X -> Z, follow parents.
				for (int W = 0; W < nvars; W++) {
					if (isParent(W, X) && W != from && !visited[X][W])
						if (areDependentDFS(X, W, Y, Z, visited)) {
							// System.out.println("From " + varname(from) + " to
							// " +
							// varname(X) + " : FOUND");
							return true;
						}
				} // end for (W)
			}

			if (!Z[X]) { // X is not in Z, follow (possibly also) the
							// children.
				for (int W = 0; W < nvars; W++) {
					if (isParent(X, W) && !visited[X][W])
						if (areDependentDFS(X, W, Y, Z, visited)) {
							// System.out.println("From " + varname(from) + " to
							// " +
							// varname(X) + " : FOUND");
							return true;
						}
				} // end for (W)
			}

		} else { // Else coming through edge X -> "from".
			if (Z[X]) { // Node X in Z => blocked.
				// System.out.println("From " + varname(from) + " to " +
				// varname(X) + " is BLOCKED");
				return false;
			} else { // X not blocked, follow both parents and children.
				for (int W = 0; W < nvars; W++) {
					if ((isParent(W, X) && !visited[X][W])
							|| (isParent(X, W) && W != from && !visited[X][W]))
						if (areDependentDFS(X, W, Y, Z, visited)) {
							// System.out.println("From " + varname(from) + " to
							// " +
							// varname(X) + " : FOUND");
							return true;
						}
				} // end for (W)
			}
		}

		// System.out.println("From " + varname(from) + " to " +
		// varname(X) + " is BLOCKED");
		return false;
	}

	/**
	 * Computes joint probability of an input configuration
	 */
	public double jointProbability(int[] configuration) {
		double p = 1;

		for (int i = 0; i < nvars; i++) {
			p *= condProbability(i, configuration);
		}
		return p;
	}

	/**
	 * Computes the probability the i-th variable, conditioned on its parents,
	 * when the variables takes the state given by configuration.
	 */
	public double condProbability(int var, int[] state) {
		ArrayList<Integer> stateOfParents = new ArrayList<Integer>(nvars); // configuration
																			// of
																			// the
																			// parents

		String cond = "";
		for (int i = 0; i < nvars; i++) {
			if(isParent(i, var)) { // i-th var is parent of var
				if (print)
					cond += i + "=" + state[i] + ", ";
				stateOfParents.add(state[i]);
			}
		}
		double[] dist = probs.get(var).get(stateOfParents);

		if (print) {
			System.out.println("Pr(" + var + "=" + state[var] + "|" + cond
					+ ") = " + dist[state[var]]);
		}
		return dist[state[var]];
	}

	/**
	 * Randomly samples a data point (a value for each random variable)
	 * according to its joint probability determined by the BN.
	 */
	public int[] sampleDataPoint(Random r) {
		boolean hasParent;
		double[] dist;
		int datapoint[] = new int[nvars];
		boolean visited[] = new boolean[nvars];

		for (int i = 0; i < nvars; i++)
			datapoint[i] = -1;

		/* iterate over queue until empty */
		while (true) {
			/**
			 * Collect in 'visit' all those nodes whose parent has been all
			 * visited
			 */
			ArrayList<Integer> visit = new ArrayList<Integer>(nvars);
			for (int i = 0; i < nvars; i++) {
				/** check if all parents has been visited */
				boolean allParentsVisisted = true;
				for (int j = 0; j < parents[i].length; j++) {
					if(!visited[parents[i][j]]) {
						allParentsVisisted = false;
						break;
					}
				}
				if (allParentsVisisted)
					visit.add(i);
			}

			boolean allvisited = true;
			for (Integer head : visit) {
				if (visited[head])
					continue;
				visited[head] = true;
				allvisited = false;

				/** Get corresponding CPT */
				ArrayList<Integer> cond = new ArrayList<Integer>();
				for (int i = 0; i < parents[head].length; i++)
					//if (parents[head][i]) // iterate over parents
						cond.add(datapoint[parents[head][i]]); // get i-th parent's value in
												// current datapoint
				dist = probs.get(head).get(cond); // get CPT

				/** Sample value randomly */
				if(dist == null){
					int asdfsadd=0;
				}
				datapoint[head] = Statistics.sampleFromUnivariate(dist, r); // sample
																			// from
																			// CPT
			}
			if (allvisited)
				break;
		}
		return datapoint;
	}

	/**
	 * Print a set represented by a boolean[] array.
	 */
	protected static String setToString(boolean[] S) {
		boolean first_var = true;
		String str = "{";
		for (int X = 0; X < S.length; X++) {
			if (S[X]) {
				if (first_var)
					first_var = false;
				else
					str += ",";
				str += " " + varname(X);
			}
		}
		str += " }";
		return str;
	}

	public String getSampledDataFileName(){
		int lastIndex = sampledDataFileName.lastIndexOf('/');
		return  sampledDataFileName.substring(lastIndex==-1?0:lastIndex);
	}
		// ///////////////////////////////////////////////////////////////////////////////////////
	public static void main(String args[]) {
		

		
		String folder = "/home/fbromberg/research/experiments/datasets/sampled/BNs-MNs/";
		//String [] datasets = {"alarm", "Barley", "carpo", "Diabetes", "hailfinder", "insurance", "Link", "Mildew", "Munin1", "Munin2", "Munin3", "Munin4", "Pigs", "Water"};
		String [] datasets = {"Barley"};
		//String [] datasets = {"alarmTest"};
		
		for(int k=0 ; k<datasets.length; k++){
		//for(int k=0 ; k<1; k++){
			String bn_filename = folder+datasets[k]+".facundo";
			String mn_filename_read = folder+datasets[k]+".mn";
			String mn_filename_write = folder+datasets[k]+".mn.data";
			
			//Create and read bn
			BN bn = new BN(bn_filename);
			//bn.sampledDataFileName = folder+datasets[k]+".data";
			//bn.saveToFile(bn_filename);

			//Read mn header
			ArrayList<String> mn_lines = new ArrayList<String>();
			
			Scanner sc=null;
			try {  sc = new Scanner(new File(mn_filename_read));
			} catch (FileNotFoundException e) {
				e.printStackTrace();		}
			while(sc.hasNextLine()){
				String  line = sc.nextLine();
				mn_lines.add(line);
				
				if(line.isEmpty()) break;
			}
			
	
			//empties BN and MN files
			machineLearning.Utils.Utils.writeStringToFile("", bn.sampledDataFileName, false, false);
			machineLearning.Utils.Utils.writeStringToFile("", mn_filename_write, false, false);
			
			//Writes header into MN file
			for(String line : mn_lines) machineLearning.Utils.Utils.writeStringToFile(line + "\n", mn_filename_write, true, false);
			
			System.out.println(bn.sampledDataFileName);
			
			Random r = new Random(123456);
			for (int i = 0; i < 32000; i++) {
				int[] datapoint = bn.sampleDataPoint(r);
				String aux="";
				for (int j = 0; j < datapoint.length; j++)
					aux += datapoint[j] + ",";
				aux = aux.substring(0, aux.length()-1);
				//System.out.println(aux);
				machineLearning.Utils.Utils.writeStringToFile(aux+"\n", bn.sampledDataFileName, true, false);
				machineLearning.Utils.Utils.writeStringToFile(aux+"\n", mn_filename_write, true, false);
				
			}
		}
	}
}

/** ************************************************************************ */
