/*
 * Created on Jun 27, 2006
 *
 */
package machineLearning.Graphs;

import java.util.Random;

import machineLearning.Datasets.*;
import machineLearning.Utils.Utils;

/**
 * @author bromberg
 *
 */
public class gibbsSamplerMarkovNet {

    static boolean screenOutput;
    static public void generateArtificialData(String filename, int n, int D, int M, double tau, Random rand, boolean randomInit){

        final int K=100; //flush interval
        final int numValues = 2;

        MarkovNet MN = new MarkovNet();

        MN.generateRandomStructure_cardinality(((int)tau)*n/2, rand, n, numValues);

	MN.generateRandomParameters(rand);

	System.out.println(MN.toString());

        Utils.writeStringToFile("", filename, false, false);
        Utils.writeStringToFile("n = " +n +", tau = " + tau + ", D = "+D+", M = "+ M + ", epsilon = " + MarkovNet.epsilon + "\n" + MN.toString() + "\n", filename, true, false);

        Utils.writeStringToFile(""+0, filename, true, false);
        for(int i=1; i<n; i++) Utils.writeStringToFile(","+i, filename, true, false);

        Utils.writeStringToFile("\n", filename, true, false);

        FastInstance inst = MN.getRandomInstance(rand);
        for(int i = 0; i < D; i += K) {
            long runtime = System.currentTimeMillis();
            FastDataset sample = MN.gibbsSample(K, M, numValues, inst, rand, randomInit);

            if(screenOutput) System.out.println("Independent sample #"+(i+K)+":  "+ inst);

            Utils.writeStringToFile(sample.toARFF(true, ","), filename, true, false);

            if(screenOutput) System.out.println("Estimated time for " + 100*K +
                                                " data points is " +
                                                (System.currentTimeMillis()-runtime)*100/1000/60 +
                                                " min ");
         }

	try {
	    MN.writeParameters((int)tau);
	} catch (Exception e) { e.printStackTrace(System.err); }
    }

        public static void main(String[] args) {
        int seed = 348957234, R = 0;
        int n = 0, D=0, M=0;
        double tau = 0.0;
        boolean randomInit = false;
        String outputFile = "";

        screenOutput = (args.length == 0);

        if(args.length == 0){
            String args2 [] = {"-n","16", "-tau", "2",   "-D","32000", "-epsilon",  "0.0", "-M","500", "-seed", "123456", "-R", "1", "-random ", "true" };
            args = args2;
        }

        for(int i=0; i<args.length; i+=2){
                if(args[i].equals("-n")){
                        n = (new Integer(args[i+1])).intValue();
            }
            if(args[i].equals("-tau")){
                tau = (new Double(args[i+1])).doubleValue();
            }
//            if(args[i].equals("-out")){
//                outputFile = args[i+1];
//            }
            if(args[i].equals("-D")){
                D = (new Integer(args[i+1])).intValue();
            }
            if(args[i].equals("-epsilon")){
                MarkovNet.epsilon = (new Double(args[i+1])).doubleValue();
            }
            if(args[i].equals("-random")){
                randomInit = args[i+1].equalsIgnoreCase("true");
            }
            if(args[i].equals("-M")){
                M = (new Integer(args[i+1])).intValue();
            }
            if(args[i].equalsIgnoreCase("-seed")){
                seed = (new Integer(args[i+1])).intValue();
            }
            if(args[i].equalsIgnoreCase("-R")){
                R = (new Integer(args[i+1])).intValue();
            }
        }

        outputFile = "sampledData_n"+n+"_t"+((int)(tau))+"_E"+((int)(MarkovNet.epsilon*100))+"_M"+M+"_R"+R+".csv";
        System.out.println("\n\nData will be output to file "+ outputFile + "\n\n");

        generateArtificialData(outputFile, n, D, M, tau, new Random(seed), randomInit);
        }
}
