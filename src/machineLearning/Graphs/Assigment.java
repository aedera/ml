package machineLearning.Graphs;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;

public class Assigment implements Cloneable, Iterable {
    private VarSet variables_;
    private int[] values_;

    public Assigment(Collection<Variable> variables) {
        variables_ = new VarSet(variables);
        values_ = new int[variables_.size()];
    }

    public Assigment(Collection<Variable> variables, int... values) {
        this(new VarSet(variables), values);
    }

    public Assigment(VarSet variables, int... values) {
        variables_ = variables;
        values_ = values;
    }

    public Assigment(Variable[] vars) {
        this(new VarSet(vars));
    }

    public Assigment(Variable[] var, int... values) {
        this(new VarSet(var));
        setValueOf(var, values);
    }

    /**
     * @return the value in this assignation of the variable <code>var</code>.
     */
    public int getValueOf(Variable var) {
        return getValueAt(variables_.indexOf(var));
    }

    /**
     * Return the variables values of <code>variables</code> in this
     * assignation. If <code>variables</code> have a scope different is
     * retained the variables that are in this assignation.
     *
     * @return the values, in this assignation, of the variables
     * <code>variables</code>.
     */
    public int[] getValueOf(Variable... variables) {
        Assigment dummy = new Assigment(variables);
        // retains variables that are in this assignation.
        dummy.retainWithVariables(this);
        int[] values = new int[dummy.size()];

        int i = 0;
        for (Variable var : dummy.variables_) values[i++] = getValueOf(var);

        return values;
    }

    public int[] getValueOf(Collection<Variable> variables) {
        return getValueOf(variables.toArray(new Variable[0]));
    }

    public int getValueAt(int i) { return values_[i]; }

    /**
     * @todo Exception in place of assert
     */
    public void setValueAt(int index, int value) {
        // Value not must be greater that number of values to variable
        assert (variables_.get(index).getOutcomes() < value);
        values_[index] = value;
    }

    public void setValueOf(Variable variable, int value) {
        setValueAt(variables_.indexOf(variable), value);
    }

    public void setValueOf(Variable[] variables, int... values) {
        int i = 0;
        for (Variable variable : variables) setValueOf(variable, values[i++]);
    }

    public Variable getVariableAt(int index) {  return variables_.get(index); }

    public Collection<Variable> getVariables() { return variables_; }

    /**
     * Assigment values must be ordened it according to variables
     */
    public void put(int... values) throws Exception {
        if (values.length > values_.length) throw new Exception();

        for (int i = 0; i < values.length; i++) setValueAt(i, values[i]);
    }

    public String toString() {
        String out = "[ ";
        for (Variable var : variables_)
            out += var.toString() + " = " + getValueOf(var) + " ";
        return out + "]";
    }

    public Assigment clone() {
        int[] copyValues = new int[values_.length];
        System.arraycopy(values_, 0, copyValues, 0, values_.length);
        return new Assigment(variables_.clone(), copyValues);
    }

    /**
     * Lookup if there exist a value in <code>values_</code> that equals to
     * <code>-1</code>.
     */
    private boolean hasWildcard() {
        boolean result;

        result = (wildcardIndexOf(this) == -1) ? false : true;

        return result;
    }

    /**
     * Return the index, in <code>assigment</code>, of the first occurrence of
     * a wildcard (<code>-1</code> in a variable value).
     *
     * @return the index in <code>assigment</code> of the first occurrence of
     * a wildcard, or -1 if not contains wildcards.
     */
    public static int wildcardIndexOf(Assigment assigment) {
        int index = -1;

        for (int i = 0; i < assigment.values_.length; i++)
            if (assigment.getValueAt(i) == -1) {
                index = i;
                break;
            }

        return index;
    }

    /**
     * Return all possibles combinations of the variables values that have
     * wildcard. For example:
     * Assigment with values: <tt>[0, -1]</tt>. The method return:
     * <tt>
     * [0, 0]
     * [0, 1]
     * </tt>
     */
    public Collection<Assigment> getAssigmentsPossible() {
        Collection<Assigment> assigments = new ArrayList<Assigment>();
        assigments.add(this);

        return getAssigmentsPossible(assigments);
    }

    public Collection<Assigment>
        getAssigmentsPossible(Collection<Assigment> assigments) {
        // there is recurrent?
        if (assigments.size() == 0) return assigments;

        // Duplicate can to eliminate and add elements
        Collection<Assigment> possibles = new ArrayList<Assigment>(assigments);

        for (Assigment assigment : assigments) {
            possibles.remove(assigment);
            possibles.addAll(getAssigmentsPossible(assigment));
        }

        // There exist element with wildcard?
        Collection<Assigment> morePossibles = getAssigmentsPossible(possibles);
        if (morePossibles.size() > 1) possibles = morePossibles;

        return possibles;
    }

    public Collection<Assigment> getAssigmentsPossible(Assigment assigment) {
        Collection<Assigment> possibles = new ArrayList<Assigment>();

        if (wildcardIndexOf(assigment) != -1)
            possibles.addAll(getAssigmentsPossible(wildcardIndexOf(assigment),
                                                   assigment));

        return possibles;
    }

    public static Collection<Assigment>
        getAssigmentsPossible(int index, Assigment assigment) {
        Collection<Assigment> possibles = new ArrayList<Assigment>();
        int outcomes = assigment.getVariableAt(index).getOutcomes();

        for (int i = 0; i < outcomes; i++) {
            Assigment assigmentCloned = assigment.clone();
            assigmentCloned.setValueAt(index, i);
            possibles.add(assigmentCloned);
        }

        return possibles;
    }

    public boolean equals(Object o) {
        if (o instanceof Assigment)
            if (variables_.equals(((Assigment)o).variables_))
                return java.util.Arrays.equals(values_,
                                               ((Assigment)o).values_);

        return false;
    }

    public int size() { return variables_.size(); }


    public boolean retainWithVariables(Variable... variables) {
        return retainWithVariables(new VarSet(variables));
    }

    public boolean retainWithVariables(VarSet variables) {
        Assigment thisAssigment = this.clone();

        if (!thisAssigment.variables_.retainVariables(variables)) return false;

        int[] newValues = new int[thisAssigment.size()];
        int i = 0;
        for (Variable var : thisAssigment.variables_) newValues[i++] = getValueOf(var);

        values_ = newValues;
        variables_ = thisAssigment.variables_;

        return true;
    }

    /**
     * Retains only the variables in this assigment that are contains in
     * <code>assigment</code>.
     *
     * @return True if this assigment retains variables of
     * <code>assigments</code>.
     *
     */
    public boolean retainWithVariables(Assigment assigment) {
        return retainWithVariables(assigment.variables_);
    }

    public int[] valuesToArray() {
        return values_;
    }

    public Iterator iterator() {
        return null;
    }
}