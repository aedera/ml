package machineLearning.Graphs;

/*
 /*
 *    Directed Graph.java
 *    @author Facundo Bromberg. 2002.
 *
 *
 */

import java.util.Vector;
import java.util.Collection;
import java.util.ArrayList;

import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Graphs.VarSet;
import machineLearning.Utils.FastSet;
import machineLearning.Utils.Set;

/**
 * Class implementing a graph. It includes various methods for adding and
 * removing edges and vertices. <!-- --> This class only considers the children,
 * which is equivalent to the adjacencyList.
 *
 * @author Facundo Bromberg
 * @version 1.0 Final Project CS572 AI, Fall 2002.
 * @date Nov 2002
 */
public class UGraph extends GraphBase {

	public static boolean screenOutput;
	GraphNode dummy, dummy_1, dummy_2;
	private String bog;

	/**
	 * Initializes the graph.
	 */
	public UGraph() {
		super();
	}

	public UGraph(UGraph G) {
		V = new Vector(0, G.size());
		for (int i = 0; i < G.size(); i++) {
			addNode(new GraphRVar(G.getNode(i).name(), G.getNode(i).index()));
		}
	}

	public UGraph(UGraphSimple G) {
		this.n = G.size();
		V = new Vector(0, G.size());
		for (int i = 0; i < G.size(); i++) {
			addNode(new GraphRVar("" + i, i));
		}

		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (G.existEdge(i, j)) {
					addEdge(i, j);
				}
			}
		}

	}

    public UGraph(Collection<Factor> factors, int n) {
	Collection<Variable> variables = new FactorGraph(factors).scope();
	this.n = variables.size();
	V = new Vector(0, this.n);

	// Add nodes
	for (int i = 0; i < n; i++) addNode(new GraphRVar(""+i, i));

	// Add edges
	ArrayList<Variable> scope;
	for (Factor factor : factors) {
	    scope = new ArrayList<Variable>(factor.scope());
	    for (int i = 0; i < scope.size(); i++)
		for (int j = i+1; j < scope.size(); j++)
		    if (!this.existEdge(i, j))
			addEdge(scope.get(i).index(), scope.get(j).index());
	}
    }

	/**
	 * @param bog
	 *            binary representation
	 * @param n
	 *            number of variables
	 */
	@SuppressWarnings("unchecked")
	public UGraph(String bog, int n) {
		this.bog = bog;
		this.n = n;
		V = new Vector(0, n);
		for (int i = 0; i < n; i++) {
			addNode(new GraphRVar("" + i, i));
		}

		/*
		 *
		 * -i is the index in the bitstring, j is the index of rows in the
		 * adyascence matrix, k is the index of columns in the adyascence matrix
		 */
		int i = 0;
		int j = 0;
		int k = 1;
		int nextDBreak = n - 1;
		int times = 0;
		for (char bit : bog.toCharArray()) {
			++times;
			if (bit == '1') {
				addEdge(j, k);
			}

			if (times == nextDBreak) {
				--nextDBreak;
				k = n - nextDBreak - 1;
				++j;
				times = 0;
			}
			++k;
			++i;
		}
	}

    public UGraph(int numNodes) {
        super();
        addNodes(numNodes);
    }
    private void addNodes(int numNodes) {
        for (int i = 0; i < numNodes; i++) addNode(new GraphRVar("", i));
    }

	/*-------------------------------*/
	/**
	 * Returns the number of edges.
	 */
	public int numberOfEdges() {
		int noe = 0;
		for (int i = 0; i < size(); i++) {
			noe += getNode(i).degree();
		}
		return noe;
	}

	/*-------------------------------*/
	/**
	 * Adds a node to the graph.
	 */
	public void addNode(GraphRVar newNode) {
		V.add(newNode);
	}

	/*-------------------------------*/
	/**
	 * Returns true if there is an edge from the source-th node in V to the
	 * target-th node in V.
	 */
	final public boolean existEdge(int source, int target) {
		dummy_1 = getNode(source);
		dummy_2 = getNode(target);

		return dummy_1.existEdge(dummy_2) || dummy_2.existEdge(dummy_1);
	}

	/*-------------------------------*/
	/**
	 * Cretes an edge from source to target vertices.<!-- --> children is the
	 * adjacency list, parents are ignored for undirected graphs.
	 *
	 * @return False if the edge already exists.
	 */
	public boolean addEdge(int source, int target) {
		GraphicalModelNode u = getNode(source);
		GraphicalModelNode v = getNode(target);

		if (!u.existEdge(v)) {
			u.addChild(v);
			v.addChild(u);
			return true;
		}
		return false;
	}

	/*-------------------------------*/
	/**
	 * Removes the edge from source to target vertices.
	 *
	 * @return True if node v existed in adjacency list (children) of u AND node
	 *         u existed in parents list of v.
	 */
	public boolean removeEdge(int source, int target) {
		if (!existEdge(source, target))
			return false;

		GraphNode u = getNode(source);
		GraphNode v = getNode(target);

		boolean ret = u.removeChild(v);
		boolean ret2 = v.removeChild(u);

		return ret && ret2;
	}

	/*-------------------------------*/
	public void connectAll() {
		for (int i = 0; i < size(); i++) {
			for (int j = i + 1; j < size(); j++) {
				addEdge(i, j);
			}
		}
	}

	/*-------------------------------*/
	final public double edgeSimilarity(UGraph G) {
		double ret = 0;
		if (size() != G.size())
			return 0.0;

		double N = size();

		for (int i = 0; i < N; i++) {
			for (int j = i + 1; j < N; j++) {
				if ((existEdge(i, j) && G.existEdge(i, j))
						|| (!existEdge(i, j) && !G.existEdge(i, j))) {
					ret++;
				}
			}
		}
		return ret / (N * (N - 1) / 2.0);
	}

	/*-------------------------------*/
	public int hamming(UGraph G) {
		int ret = 0;
		if (size() != G.size())
			return 0;

		double N = size();

		for (int i = 0; i < N; i++) {
			for (int j = i + 1; j < N; j++) {
				boolean aux = existEdge(i, j);
				boolean aux2 = G.existEdge(i, j);
				if ((existEdge(i, j) && G.existEdge(i, j))
						|| (!existEdge(i, j) && !G.existEdge(i, j))) {
					;
				} else
					ret++;
				aux = existEdge(i, j);
				aux2 = G.existEdge(i, j);

			}
		}
		return ret;
	}

	/*-------------------------------*/
	// returns [TI, TD, FP, FN]
	// Assumes this network is the correct one
	public double[] hammingMatrix(UGraph G) {
		double TP = 0;
		double FP = 0;
		double TN = 0;
		double FN = 0;

		if (size() != G.size())
			return null;

		double ret[] = new double[4];

		double N = size();

		for (int i = 0; i < N; i++) {
			for (int j = i + 1; j < N; j++) {
				if (screenOutput)
					System.out.print("\n(" + i + "," + j + ")");
				if (existEdge(i, j)) {
					if (G.existEdge(i, j)) {
						TP++;
						if (screenOutput)
							System.out.print("true  true  TI " + TP);
					} else {
						FN++;
						if (screenOutput)
							System.out.print("true  false  FN " + FN);
					}
				} else {
					if (!G.existEdge(i, j)) {
						TN++;
						if (screenOutput)
							System.out.print("false  false  TD " + TN);
					} else {
						FP++;
						if (screenOutput)
							System.out.print("false  true  FP " + FP);
					}
				}
				if (screenOutput)
					System.out
							.print("  Accuracy "
									+ "("
									+ (TP + TN)
									+ "/"
									+ (TN + TP + FN + FP)
									+ ") = "
									+ ((double) (TP + TN) / (double) (TP + FP
											+ TN + FN)));
			}
		}
		ret[0] = TP;
		ret[1] = TN;
		ret[2] = FP;
		ret[3] = FN;
		return ret;
	}

	/*-------------------------------*/
	/**
	 * first element in the output array is the specificity, the second is the
	 * sensitivity
	 */
	public double[] specificityPlusSensitivity(UGraph trueNet) {
		if (size() != trueNet.size()) {
			System.out
					.println("ERROR: size of networks mistmatch in specificityPlusSensitivity");
			(new Exception()).printStackTrace();
		}

		double N = size();
		double TP = 0;
		double TN = 0;
		double FP = 0;
		double FN = 0;
		for (int i = 0; i < N; i++) {
			for (int j = i + 1; j < N; j++) {
				if (existEdge(i, j) && trueNet.existEdge(i, j)) {
					TP++;
				}
				if (!existEdge(i, j) && !trueNet.existEdge(i, j)) {
					TN++;
				}
				if (existEdge(i, j) && !trueNet.existEdge(i, j)) {
					FP++;
				}
				if (!existEdge(i, j) && trueNet.existEdge(i, j)) {
					FN++;
				}
			}
		}
		double ret[] = new double[2];
		ret[0] = TP / (TP + FP);
		ret[1] = TP / (TP + FN);
		return ret;
	}

	/**
	 * Return a Vector of Sets. Each of the sets in the Vector is a clique in
	 * the network.
	 *
	 * @return
	 */

	/**
	 * Return the maximum clique in G, that is a super-set of the input clique.
	 *
	 * @return
	 */
	public Set maxCliques(Set C) {
		System.out.println(C);
		Set maxClique = new Set(C);
		Set subs = Set.minus(new Set(V), C);

		for (int i = 0; i < subs.size(); i++) {
			GraphNode Y = (GraphNode) subs.elementAt(i);

			// Is Y linked to all variables in C?
			boolean linked2all = true;
			for (int j = 0; j < C.size(); j++) {
				GraphNode X = (GraphNode) C.elementAt(j);

				if (!Y.adjacencyList().contains(X)) {
					linked2all = false;
					break;
				}
			}

			if (linked2all) {
				Set Cp = new Set(C);
				Cp.add(Y);
				Set aux = maxCliques(Cp);

				if (aux.size() > maxClique.size()) {
					maxClique = null;
					maxClique = aux;
				}
			}
		}
		return maxClique;
	}

	/**
	 * Return a Vector of Sets. Each of the sets in the Vector is a clique in
	 * the network.
	 *
	 * @return
	 */
	public Vector MaxCliques() {
		Vector cliques = new Vector();

		Set Vp = new Set(V);

		while (!Vp.isEmpty()) {
			GraphNode X = (GraphNode) Vp.elementAt(0);

			Set Cx = new Set();
			Cx.add(X);
			Set maxCliqueX = maxCliques(Cx);

			Vp = Set.minus(Vp, maxCliqueX);
			cliques.add(maxCliqueX);
		}

		return cliques;
	}

	/**
	 * The input C is a candidate maximalClique. If there a pair of nodes in C
	 * that does not has an edge, then it's not a clique.
	 *
	 * @return
	 */
	public Set maxCliques3(Set C) {
		boolean allConnected = true;
		Set cliques = new Set();
		Set Cz;
		setSizesComparator ssc = new setSizesComparator();

		for (int i = 0; i < C.size(); i++) {
			GraphNode X = (GraphNode) C.elementAt(i);

			Set Cx = new Set(C);
			Cx.remove(X);
			for (int j = i + 1; j < C.size(); j++) {
				GraphNode Y = (GraphNode) C.elementAt(j);
				Set Cy = new Set(C);

				Cy.remove(Y);
				if (!existEdge(X.index(), Y.index())) {
					allConnected = false;

					Set aux1 = maxCliques3(Cx);
					Set aux2 = maxCliques3(Cy);
					cliques = Set.Union(cliques, aux1);
					cliques = Set.Union(cliques, aux2);

					break;
				}
			}
			if (!allConnected)
				break;
		}
		if (allConnected) {
			cliques.add(C);
		}

		return cliques;
	}

	GraphNode X, Y;
	FastSet Cy, Cx, aux;

	/*    *//**
	 * Returns false if C is a subset of any set in cliques
	 */
	/*
	 * boolean isMaximal(Set C, Set cliques){ for(int i=0; i<cliques.size();
	 * i++){ aux = (Set) cliques.elementAt(i); if(Set.subSet(C, aux)) return
	 * false; } return true; }
	 */

	public Set maxCliques3Fast(Set C, final boolean[][] adjMatrix) {
		boolean allConnected = true;
		Set cliques = new Set();
		int m = C.cardinality();

		for (int i = 0; i < m; i++) {
			X = (GraphNode) C.elementAt(i);
			int x = X.index();
			for (int j = i + 1; j < m; j++) {
				Y = (GraphNode) C.elementAt(j);
				if (!adjMatrix[x][Y.index()]) {
					Set Cx = new Set(C);
					Cx.remove(X);

					Set Cy = new Set(C);
					Cy.remove(Y);

					allConnected = false;

					Set aux1 = maxCliques3Fast(Cx, adjMatrix);
					cliques = Set.Union(cliques, aux1);

					Set aux2 = maxCliques3Fast(Cy, adjMatrix);
					cliques = Set.Union(cliques, aux2);

					break;
				}
			}
			if (!allConnected)
				break;
		}
		// if(allConnected && !Set.subSet(C, F)){
		if (allConnected) {
			cliques.add(C);
		}

		return cliques;
	}

	public Set MinCliques() {
		Set cliques = new Set();
		int n = V.size();

		for (int i = 0; i < n; i++) {
			X = (GraphNode) V.elementAt(i);
			int x = X.index();
			for (int j = i + 1; j < n; j++) {
				Y = (GraphNode) V.elementAt(j);
				if (existEdge(X.index(), Y.index())) {
					Set C = new Set();
					C.add(X);
					C.add(Y);

					cliques.add(C);
				}
			}
		}
		return cliques;
	}

	/**
	 * Return a Vector of Sets. Each of the sets in the Vector is a clique in
	 * the network.
	 *
	 * @return
	 */
	public Set MaxCliques3() {
		// Set cliques = new Set();

		Set Vp = new Set(V);

		// long time = System.currentTimeMillis();
		boolean adjMatrix[][] = new boolean[Vp.size()][];
		for (int i = 0; i < Vp.size(); i++)
			adjMatrix[i] = new boolean[Vp.size()];
		for (int i = 0; i < Vp.size(); i++) {
			GraphNode X = (GraphNode) Vp.elementAt(i);
			for (int j = 0; j < Vp.size(); j++) {
				GraphNode Y = (GraphNode) Vp.elementAt(j);
				if (X.index() == Y.index())
					continue;

				if (existEdge(X.index(), Y.index()))
					adjMatrix[i][j] = true;
				else
					adjMatrix[i][j] = false;
			}
		}
		Set cliques = maxCliques3Fast(Vp, adjMatrix);
		// System.out.println("time = " + (System.currentTimeMillis() -
		// time)/1000.0);

		/*
		 * time = System.currentTimeMillis(); Set cliques2 = maxCliques3(Vp);
		 * System.out.println("time = " + (System.currentTimeMillis() -
		 * time)/1000.0);
		 */

		// Clean up subsets
		for (int i = 0; i < cliques.size(); i++) {
			Set Ci = (Set) cliques.elementAt(i);
			for (int j = i + 1; j < cliques.size(); j++) {
				Set Cj = (Set) cliques.elementAt(j);

				if (Set.subSet(Ci, Cj)) {
					if (j >= i)
						j--;
					cliques.remove(i);
					i--;
					break;
				}
				if (Set.subSet(Cj, Ci)) {
					if (i >= j)
						i--;
					cliques.remove(j);
					j--;
				}
			}
		}
		return cliques;

	}

	// //////////////////////////
	// /// Comparator methods
	// ///////////////////////////
	class setSizesComparator implements machineLearning.Utils.Comparator {
		public boolean equal(Object O1, Object O2) {
			return ((Set) O1).cardinality() == ((Set) O2).cardinality();
		}

		public boolean smaller(Object O1, Object O2) {
			return ((Set) O1).cardinality() < ((Set) O2).cardinality();
		}
	};

	/*------------------------------*/
	/**
	 * Display a representation of the graph as an adjency list.
	 */
	public String toString() {
	    //String out = "Adjacency lists of the graph: \n\n";
		String out = "\nVAR                             VAR CHILDREN\n\n";
		for (int i = 0; i < size(); i++) {
			out += getNode(i).toStringWithNeighbors() + "\n";
		}
		return out;
		// return new UGraphSimple(this).toString();
	}

	public String getBOG() {
		String bog = "";
		for (int i = 0; i < size(); i++) {
			for (int j = i + 1; j < size(); j++) {
				if (existEdge(i, j)) {
					bog += "1";
				}else{
					bog += "0";
				}
			}
		}
		return bog;
	}

@Override
public int hashCode() {
	return getBOG().hashCode();
}

}