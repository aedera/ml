package machineLearning.Graphs;

import java.util.Iterator;
import java.util.Random;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.DumperOptions;

import machineLearning.Datasets.*;
import machineLearning.Graphs.UGraph;
import machineLearning.Graphs.GraphNode;
import machineLearning.Utils.Utils;


public class gibbsSamplerMarkovNetNew {
    public static void
        generateArtificialData(String filename, int n, int D, int M, int tau,
                               Random rand, boolean randomInit) {
        int numValues = 2;
        MarkovNet net = new MarkovNet();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String, Object> options = new HashMap<String, Object>();

        //        net.generateRandomStructure_cardinality(tau *n/2, rand, n, numValues);
        net.generateRandomLattice(n);
        net.allocateParams();
        net.generateRandomParameters(rand);

        options.put("D", D);
        options.put("n", n);
        options.put("M", M);
        data.put("options", options);

        data.put("structure", structureToMap(net));
        data.put("variables", variablesToArray(net));
        data.put("parameters", parametersToMap(net));
        data.put("dataset", datasetToMap(net, rand, D, M, randomInit));

        Yaml yaml = new Yaml();
        String output = yaml.dump(data);

        Utils.writeStringToFile(output, filename, true, false);
    }

    private static Integer[] variablesToArray(UGraph net) {
        Collection<GraphNode> nodes = net.V();
        Integer[] variables = new Integer[nodes.size()];
        int i = 0;

        for (GraphNode node : nodes) variables[i++] = new Integer(node.index());

        return variables;
    }

    private static Map<Integer, Collection<Integer>>
        structureToMap(UGraph graph) {
        Map<Integer, Collection<Integer>> structure =
            new HashMap<Integer, Collection<Integer>>();

        for (Iterator<GraphNode> iter = graph.iterator(); iter.hasNext();) {
            GraphNode node = iter.next();
            Collection<GraphNode> neighbors = node.adjacencySet();

            structure.put(new Integer(node.index()),
                          neighborsToIntegers(neighbors));
        }

        return structure;
    }

    private static Collection<Integer>
        neighborsToIntegers(Collection<GraphNode> neighbors) {
        Collection<Integer> indicesOfNeighbors = new ArrayList<Integer>();

        int i = 0;
        for (GraphNode nodeNeighbor : neighbors)
            indicesOfNeighbors.add(new Integer(nodeNeighbor.index()));

        return indicesOfNeighbors;
    }

    private static Map<Collection<Integer>, Collection<Double>>
        parametersToMap(MarkovNet graph) {
        Map<Collection<Integer>, Collection<Double>> parameters =
            new HashMap<Collection<Integer>, Collection<Double>>();

        Collection<Integer> clique;
        int n = graph.size();

        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                if (graph.existEdge(i, j)) {
                    clique = Arrays.asList(new Integer[] {
                            new Integer(i),
                            new Integer(j) });

                    parameters.put(clique, getParametersBetween(graph, i, j));
                }

        return parameters;
    }

    private static Collection<Double>
        getParametersBetween(MarkovNet net, int varIndexA, int varIndexB) {
        Collection<Double> configurations;

        configurations = Arrays.asList(new Double[] {
                new Double(net.getParams()[varIndexA][varIndexB][0][0]),
                new Double(net.getParams()[varIndexA][varIndexB][0][1]),
                new Double(net.getParams()[varIndexA][varIndexB][1][0]),
                new Double(net.getParams()[varIndexA][varIndexB][1][1])
            });

        return configurations;
    }

    private static Collection<Collection<Integer>>
        datasetToMap(MarkovNet graph, Random rand,
                     int sizeDataSet, int sizeSample, boolean randomInit) {
        Collection<Collection<Integer>> instances = new ArrayList();
        int flush = 100;
        int numValues = 2;

        FastInstance inst = graph.getRandomInstance(rand);
        for(int i = 0; i < sizeDataSet; i += flush) {
            FastDataset sample =
                graph.gibbsSample(flush, sizeSample, numValues, inst,
                                  rand, randomInit);

            for (Iterator<FastInstance> iter = sample.iterator(); iter.hasNext(); )
                instances.add(iter.next().getValues());
        }

        return instances;
    }

    public static void main(String[] args) {
        int seed = 348957234, R = 0;
        int n = 0, D=0, M=0;
        int tau = 0;
        boolean randomInit = false;
        String outputFile = "";
        boolean screenOutput = (args.length == 0);

        if(args.length == 0){
            System.err.println("Empty arguments.");
            System.err.println("For example: " +
                               "-n 16 -tau 2 -D 32000 -epsilon 0.0 -M 500 " +
                               "-seed 123456 -R 1 -random true");
            System.exit(0);
        }

        for(int i=0; i<args.length; i+=2){
            if(args[i].equals("-n")){
                n = (new Integer(args[i+1])).intValue();
            }
            if(args[i].equals("-tau")){
                tau = (new Integer(args[i+1])).intValue();
            }

            if(args[i].equals("-D")){
                D = (new Integer(args[i+1])).intValue();
            }
            if(args[i].equals("-epsilon")){
                MarkovNet.epsilon = (new Double(args[i+1])).doubleValue();
            }
            if(args[i].equals("-random")){
                randomInit = args[i+1].equalsIgnoreCase("true");
            }
            if(args[i].equals("-M")){
                M = (new Integer(args[i+1])).intValue();
            }
            if(args[i].equalsIgnoreCase("-seed")){
                seed = (new Integer(args[i+1])).intValue();
            }
            if(args[i].equalsIgnoreCase("-R")){
                R = (new Integer(args[i+1])).intValue();
            }
        }

        outputFile = "sampledData_n"+n+"_t"+ tau +"_E"+((int)(MarkovNet.epsilon*100))+"_M"+M+"_R"+R+".csv";
        System.out.println("\n\nData will be output to file "+ outputFile + "\n\n");

        generateArtificialData(outputFile, n, D, M, tau, new Random(seed), randomInit);
    }
}
