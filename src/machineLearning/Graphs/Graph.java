package machineLearning.Graphs;
/*
/*
 *    Graph.java
 *    @author Facundo Bromberg. 2002.
 *
 *
 */

import java.io.*;
import java.util.*;

/**
 * Class implementing a DIRECTED graph. It includes various methods for adding and removing edges and vertices. <!-- -->
 *
 * @author Facundo Bromberg
 * @version 1.0 Final Project CS572 AI, Fall 2002.
 * @date Nov 2002
 */
public abstract class Graph extends GraphBase {

  /** Vector of vertices (or nodes). */
  protected Vector V;

  /**
   * Initializes the graph.
   */
  public Graph() {
      V = new Vector(0, 5);
  }
  public Graph(Graph G) {super(G);}
  /*-------------------------------*/
    /**
   * Returns the number of edges.
   */
  public int numberOfEdges()
  {
    int noe = 0;
    for(int i=0; i<size(); i++)
    {
      noe += getNode(i).inDegree();
    }
    return noe;
  }

  /*-------------------------------*/
    /**
   * Returns true if there is an edge from the source-th node in V  to the target-th node in V.
   */
  public boolean existEdge(int source, int target)
  {
      GraphNode u = getNode(source);
      GraphNode v = getNode(target);

      return u.existEdge(v);
  }
  
  /*-------------------------------*/
   /**
   * Cretes an edge from source to target vertices.
   * @return False if the edge already exists.
   */
  public boolean addEdge(int source, int target)
  {
	  GraphicalModelNode u = getNode(source);
	  GraphicalModelNode v = getNode(target);

      if(!u.existEdge(v)) {
	  u.addChild(v);
	  v.addParent(u);
	  return true;
      }
      return false;
  }
  /*-------------------------------*/
   /**
   * Removes the edge from source to target vertices.
   * @return True if node v existed in adjacency list (children) of u AND node u existed in parents list of v.
   */
  public boolean removeEdge(int source, int target)
  {
      GraphNode u = getNode(source);
      GraphNode v = getNode(target);

      boolean ret =  u.removeChild(v);
      boolean ret2 = v.removeParent(u);

      return ret && ret2;
  }
  /*-------------------------------*/
   /**
   * Tranpose the edge from source to target vertices.<!-- --> That is, if (source, target) existed, it
   * removes it and adds the edge (target, source)
   */
  public boolean transposeEdge(int source, int target)
  {
		if(!removeEdge(source, target)) return false;
		if(!addEdge(target, source)) return false;

	  return true;
  }
  
  /*------------------------------------*/
  public abstract boolean separates(int x, int y, Vector Z);
	

}


