package machineLearning.Graphs;

/**
 * Class: GraphNode
 * Implements a graph node that can have any number of children and parents.
 *
 * @author Facundo Bromberg
 * @version 1.0 Final Project CS572 AI, Fall 2002.
 * @date Nov 2002
 */

import machineLearning.Datasets.Attribute;
import machineLearning.Utils.FastSet;

/**
 * Implements a graph node. This node maintains its own adjacency list (Vector
 * children) and a parents Vector <!-- --> The calling class should maintain
 * these vectors; i.e. Taking care that if u is a child of v, then v appears in
 * the parent vector of u.
 *
 * @author Facundo Bromberg
 * @version 1.0 Final Project CS572 AI, Fall 2002.
 * @date Nov 2002
 */
public class GraphicalModelNode extends GraphNode {
    public Attribute attr;

    /**
     * String Constructor
     */
    public GraphicalModelNode(String _name) {
        super(_name);
    }

    public GraphicalModelNode(Attribute attr) {
        super(attr.name(), attr.index());
        this.attr = attr;
    }
    public GraphicalModelNode(int n, int index) {
        super(n, index);
        this.parent = null;
        this.attr = new Attribute(this.name, this.index);
    }

    public GraphicalModelNode(int n, int index, int compareMode) {
        super(n, index, compareMode);
        this.parent = null;
        this.attr = new Attribute(this.name, this.index);
    }

    public GraphicalModelNode(GraphicalModelNode node) {
        super(node);
        this.attr = new Attribute(node.attr);
        this.state = new FastSet(node.state);
    }


    public GraphicalModelNode(GraphicalModelNode node, int n) {
        super(node, n);
        this.attr = new Attribute(node.attr);
    }

    public GraphicalModelNode(GraphNode node, Attribute attr) {
        super(node);
        this.attr = new Attribute(attr);
    }
    public GraphicalModelNode(GraphNode node, Attribute attr, int numberOfAttributes) {
        super(node, numberOfAttributes);
        this.attr = new Attribute(attr);
    }
    /**
     * String Constructor
     */
    public GraphicalModelNode(String _name, int _index) {
        super(_name, _index);
    }

    public GraphicalModelNode() {
    }

    /**
     * String Constructor
     */
    public GraphicalModelNode(int _index) {
        super(_index);
    }

}