package machineLearning.Graphs;

import java.util.Iterator;
import java.util.Collection;

import machineLearning.Utils.Matrix;

public class DiscreteFactor extends Potencial {
    protected Matrix parameters_;

    public DiscreteFactor() {
        super();
        initializeParameters();
    }

    public DiscreteFactor(Collection<Variable> variables) {
        super(variables);
        initializeParameters();
    }

    public DiscreteFactor(Variable[] variables) {
        super(variables);
        initializeParameters();
    }

    public DiscreteFactor(Variable[] variables, double... parameters) {
        this(variables);
        setParameters(parameters);
    }

    public DiscreteFactor(Variable[] variables, Matrix parameters) {
        this(variables);
        parameters_ = parameters;
    }

    public DiscreteFactor(VarSet variables, Matrix parameters) {
        super(variables);
        parameters_ = parameters;
    }

    public DiscreteFactor(int[] scope, double... parameters) {
	super(scope);

        int[] sizes = new int[scope.length];
	java.util.Arrays.fill(sizes, 2);

        parameters_ = new Matrix(sizes, parameters);
    }

    public double value(Assigment ass) {
        // index to query table of parameters
        int[] assignationToIndex = ass.getValueOf(getVariables());

        return value(assignationToIndex);
    }

    public double value(int... indices) {
	int[] ind;
	int k = 0;
	if (indices.length > scope_.length) {
	    ind = new int[scope_.length];

	    for (int i = 0; i < scope_.length; i++)
		ind[k++] = indices[scope_[i]];

	} else
	    ind = indices;

	return parameters_.get(ind);
    }

    public double value(machineLearning.Datasets.FastInstance instance) {
	return -999999999;
    }

    public void setValue(int[] index, double value) {
        parameters_.set(value, index);
    }

    // TODO: Should not call numParameters?
    public int getNumAssigments() {
        int outcome = 1;

        for (int size : ((VarSet)getVariables()).getDimensions())
            outcome *= size;

        return outcome;
    }

    public void setParameters(Matrix parameters) {
        if (!java.util.Arrays.equals(parameters_.dimensions(),
                                     parameters.dimensions()))
            new IllegalArgumentException("Dimensions not compatibles with factor's variables.");
        else parameters_ = parameters;
    }

    public void setParameters(double... parameters) {
        // Get sizes of dimensions each variable in factor's scope.
        int[] sizes = new int[scope().size()];
        int i = 0;
        for (Variable variable : scope())
            sizes[i++] = variable.getOutcomes();

        setParameters(new Matrix(sizes, parameters));
    }

    public void fillRandomDouble() { parameters_.fillRandomDouble(); }

    private void initializeParameters() {
        parameters_ = new Matrix(((VarSet)getVariables()).getDimensions());
    }

    public Factor clone() {
        return new DiscreteFactor(((VarSet)scope()).clone(),
                                  parameters_.clone());
    }

    public Factor multiply(Factor factor) throws IllegalArgumentException {
        if (!isAnyInScope(factor.getVariables()))
            throw new IllegalArgumentException("Not variables in scope.");

        Collection<Variable> join = (VarSet)VarSet.join(this.getVariables(),
                                                        factor.getVariables());

        Factor newFactor = new DiscreteFactor(join);
        double value;
        for (Iterator i = new Assigment(join).iterator(); i.hasNext(); ) {
            Assigment assigment = (Assigment)i.next();
            value = value(assigment) * factor.value(assigment);
            newFactor.setValue(assigment, value);
        }

        return newFactor;
    }

    public double[] getParameters() {
	return parameters_.toArray();
    }

    protected Factor computeMarginalize(Variable variable) {
        VarSet subSetVar = variables_.clone();
        subSetVar.remove(variable);

        Factor factorMarginalized = new DiscreteFactor(subSetVar);

        Assigment assigment = new Assigment(variables_);

        //Iterate about all assigment and set value in new factor
        for (Iterator i = assigment.iterator(); i.hasNext(); ) {
            Assigment ass = (Assigment)i.next();
            double value = factorMarginalized.value(ass) + value(ass);
            ((Potencial)factorMarginalized).setValue(ass, value);
        }

        return factorMarginalized;
    }

    public boolean equals(Factor factor) {
        return equals(((Potencial)factor).scope()) &&
            equals(((Potencial)factor).getParameters());
    }

    public boolean equals(Matrix parameters) {
        return parameters_.equals(parameters);
    }

    public boolean equals(Collection<Variable> variables) {
        return variables_.equals(variables);
    }

    public void normalize() { parameters_.normalize(); }

    public int numParameters() { return parameters_.size(); }

    public boolean isZero() {
	return parameters_.isZero();
    }

   public String toString() {
       return java.util.Arrays.toString(scope_) + java.util.Arrays.toString(getParameters());
   }
}