package machineLearning.Graphs;

import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Arrays;

public class VarSet implements List<Variable>, Cloneable {
    private List<Variable> variables_;

    public VarSet() { variables_ = new ArrayList<Variable>(); }

    public VarSet(Variable... variables) { this(Arrays.asList(variables)); }

    public VarSet(Collection<Variable> variables) {
        TreeSet<Variable> set = new TreeSet<Variable>();
        set.addAll(variables);
        variables_ = new ArrayList<Variable>(set);
    }

    public VarSet(Factor... factors) {
        TreeSet<Variable> set = new TreeSet<Variable>();
        for (Factor factor : factors) set.addAll(factor.scope());
        variables_ = new ArrayList<Variable>(set);
    }

    /**
     * Join the variables in <code>setA</code> and <code>setB</code>.
     *
     * @return A collection with join the variables of parameters passed. Not
     * contains variables duplicated.
     */
    public static Collection<Variable> join(Collection<Variable> setA,
                              Collection<Variable> setB) {
        TreeSet<Variable> set = new TreeSet<Variable>(setA);
        set.addAll(setB);

        return new VarSet(set);
    }

    /**
     * Join the variables in <code>variables</code>.
     *
     * @return A collection with join the variables of parameters passed. Not
     * contains variables duplicated.
     */
    public static Collection<Variable> join(Variable... variables) {
        return new VarSet(new TreeSet<Variable>(Arrays.asList(variables)));
    }

    /**
     * Sort the variables in this set, according the ordering implements the
     * <code>Comparable</code> interface in the class <code>Variable</code>.
     *
     * Is responsability of user sorts the set each add a element in it. In
     * the unique moment that is sorted the variables is when call the
     * constructors.
     */
    public void sort() { java.util.Collections.sort(variables_); }

    public boolean add(Variable o) { return variables_.add(o); }

    public void add(int index, Variable v) { variables_.add(index, v); }

    public boolean addAll(Collection c) { return variables_.addAll(c); }

    public boolean addAll(int index, Collection c) {
        return variables_.addAll(index, c);
    }

    // TODO: add singles return boolean
    public boolean addAll(Variable... variables) {
        boolean outcome = true;
        for (Variable variable : variables) add(variable);
        return outcome;
    }

    public void clear() { variables_.clear(); }

    public boolean contains(Object o) { return variables_.contains(o); }

    public boolean containsAll(Collection c) {
        return variables_.containsAll(c);
    }

    public boolean containsAny(Collection<Variable> c) {
        for (Variable var : c)
            if (contains(var)) return true;

        return false;
    }

    public boolean equals(Object o) {
        if (o instanceof Collection)
            return variables_.equals(((VarSet)o).variables_);

        return false;
    }

    public int hashCode() { return variables_.hashCode(); }

    public boolean isEmpty() { return variables_.isEmpty(); }

    public java.util.Iterator iterator() { return variables_.iterator(); }

    public java.util.ListIterator listIterator() {
        return variables_.listIterator();
    }

    public java.util.ListIterator listIterator(int index) {
        return variables_.listIterator(index);
    }

    public boolean remove(Object o) { return variables_.remove(o); }

    public Variable remove(int i) { return variables_.remove(i); }

    public int size() { return variables_.size(); }

    public boolean removeAll(Collection c) { return variables_.removeAll(c); }

    public boolean retainAll(Collection c) { return variables_.retainAll(c); }

    /**
     * Remove all the variables in this set that are not contained in
     * <code>variables</code>.
     *
     * @return true if this set is changed.
     */
    public boolean retainVariables(VarSet variables) {
        return variables_.retainAll(variables.variables_);
    }

    public Variable set(int index, Variable element) {
        return variables_.set(index, element);
    }

    public Variable get(int index) { return variables_.get(index); }

    public List<Variable> subList(int fromIndex, int toIndex) {
        return variables_.subList(fromIndex, toIndex);
    }

    public int lastIndexOf(Object o) { return variables_.lastIndexOf(o); }

    public Object[] toArray() { return variables_.toArray(); }

    public Object[] toArray(Object[] object) {
        return variables_.toArray(object);
    }

    public Variable[] toArray(Variable[] v) {
        return variables_.toArray(v);
    }

    /**
     * Return an array with dimension sizes of the variables in the set
     */
    public int[] getDimensions() {
        return getDimensions(this);
    }

    public static int[] getDimensions(Collection<Variable> variables) {
        int[] dim = new int[variables.size()];
        int i = 0;

        for (Variable variable : variables)
            dim[i++] = variable.getNumOutcomes();

        return dim;
    }

    public int[] indicesToArray() {
        int[] indices = new int[size()];
        int i = 0;

        for (Variable variable : this) indices[i++] = variable.index();

        return indices;
    }

    public int indexOf(Object o) { return variables_.indexOf(o); }

    public VarSet clone() {
        return new VarSet(toArray(new Variable[0]));
    }

    public String toString() {
        String out = "{ ";

        for (Variable var : this) out += var.toString() + " ";

        return out + "}";
    }
}