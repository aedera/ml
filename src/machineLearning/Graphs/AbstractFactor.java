package machineLearning.Graphs;

import java.util.Collection;

public abstract class AbstractFactor implements Factor {
    protected VarSet variables_;
    public int[] scope_;

    protected AbstractFactor() { variables_ = new VarSet(); }
    protected AbstractFactor(Variable... variables) {
        this(new VarSet(variables));
    }
    protected AbstractFactor(Collection<Variable> variables) {
        variables_ = new VarSet(variables);
	scope_ = new int[variables_.size()];
	int i = 0;
	for (Variable var : variables_) scope_[i++] = var.index();
    }

    public AbstractFactor(int[] scope) {
	scope_ = scope;
	variables_ = new VarSet();
	for (int var : scope) variables_.add(new Variable(2, "",var));
    }

    public Collection<Variable> getVariables() { return scope(); }

    public Collection<Variable> scope() { return variables_; }

    public boolean isAllInScope(Collection<Variable> varset) {
        return variables_.containsAll(varset);
    }

    public boolean isAnyInScope(Collection<Variable> varset) {
        return variables_.containsAny(varset);
    }

    public boolean isInScope(Variable variable) {
	return java.util.Arrays.binarySearch(scope_, variable.index()) >= 0 ? true : false;
    }

    protected Factor computeMarginalize(Variable... variables) {
        Factor factor = marginalize(variables[0]);

        for (int i = 1; i < variables.length; i++)
            factor = factor.marginalize(variables[i]);

        return factor;
    }

    protected abstract Factor computeMarginalize(Variable variable);

    public Factor marginalize(Variable... variables)
        throws IllegalArgumentException {
        if (!isAllInScope(java.util.Arrays.asList(variables)))
            throw new IllegalArgumentException();

        return computeMarginalize(variables);
    }

    public Factor marginalize(Variable variable)
        throws IllegalArgumentException {
        if (!isInScope(variable)) throw new IllegalArgumentException();

        return computeMarginalize(variable);
    }

    public abstract Factor clone();
}
