package machineLearning.Graphs;

import java.util.Iterator;

import machineLearning.Utils.Matrix;


public class UGraphIterator implements Iterator<GraphBase> {
    private final int nNodes_;
    private final int NUM_STRUCTURES;
    private final int[] DIMENSIONS;
    private int currentIndex_;
    private int[] edges_;

    public UGraphIterator(int numNodes) {
        nNodes_ = numNodes;
        edges_ = new int[numNodes * (numNodes - 1) / 2];
        NUM_STRUCTURES = (int)Math.pow(2.0, edges_.length) - 1;
        DIMENSIONS = new int[edges_.length];
        java.util.Arrays.fill(DIMENSIONS, 2);
        currentIndex_ = 0;
    }

    public boolean hasNext() {
        return (currentIndex_ <= NUM_STRUCTURES) ? true : false;
    }

    public GraphBase next() throws java.util.NoSuchElementException {
        UGraph graph = new UGraph(nNodes_);

        int[] edges = indexToEdges(currentIndex_);
        for (int i = 0; i < edges.length; i++) {
            if (!hasEdgeAt(i)) continue;

            int[] edge = indexAtUpperTriangularnMatrix(i);
            graph.addEdge(edge[0], edge[1]);
        }

        currentIndex_++;
        incrementStatus();

        return graph;
    }

    public void remove() throws java.lang.UnsupportedOperationException {
        throw new java.lang.UnsupportedOperationException();
    }

    private boolean hasEdgeAt(int index) {
        return (edges_[index] == 1) ? true : false;
    }

    /**
     * Transform edges array index one-dimensional to a index two-dimensional,
     * that represent a index in the upper triangular matrix of graphs's edges.
     *
     * @todo very, very, very ugly
     */
    private int[] indexAtUpperTriangularnMatrix(int index) {
        int[] edge = new int[2];
        int count = 0;

        for (int i = 0; i < nNodes_; i++)
            for (int j = i + 1; j < nNodes_; j++) {
                edge[0] = i;
                edge[1] = j;

                if (count == index) return edge;

                count++;
            }

        return null;
    }

    private int[] indexToEdges(int index) {
        return Matrix.indexToIndices(index, DIMENSIONS);
        //        edge[1] += edge[0] + 1; // by triangulate upper matrix

        //        return edge;
    }

    private void incrementStatus() {
        incrementStatus(edges_.length - 1);
    }

    private void incrementStatus(int index) {
        edges_[index] = (edges_[index] + 1) % 2;

        if (edges_[index] == 0) // If equal cero then carry
            if ((index - 1) >= 0) // Is index valid (not negative)?
                incrementStatus(index - 1);
    }
}