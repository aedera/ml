/*
 * Created on Mar 16, 2005
 *
 * @author Facundo Bromberg
 */
package machineLearning.Graphs;

import java.io.IOException;
import java.util.Vector;
import java.util.Random;
import java.util.Hashtable;
import java.util.BitSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.*;

import machineLearning.Graphs.GraphsSimple.*;
import machineLearning.Datasets.*;
import machineLearning.Utils.*;


public class MarkovNet extends UGraph /*implements FullConditionalProposal*/{
    static double epsilon = -10.0;   //for generating random parameters

    public Schema schema;
    /**
     * @deprecated variable name isn't standard
     */
    Set Cliques = null;

    /**
     * For each clique we have a hashtable that will store the
     * parameters for different configurations of the variables in
     * the clique. This way we can retrieve the parameter for a
     * given configuration.
     */
    Vector paramsHashtables = new Vector();

    int [] dim;  //for each pair of nodes x,y, dimensions[x][y][0] and
                 //dimensions[x][y][0] are the number of values x and
                 //y has, respectively
    public double [][][][] params; //params[x][y] stores the table of
                            //parameters for sub-clique (edge) x-y.
    Random rand;    //uniform random generator;
    boolean adjMatrix[][];

    StringBuffer strbuff = new StringBuffer();
    double p_i [];

    double[] parameters;

    public MarkovNet(){super();};
    public MarkovNet(MarkovNet G){
        V = new Vector(0, G.size());
        for(int i=0; i<G.size(); i++){
            GraphRVar ar0 = G.getVar(i);
            GraphRVar ar = new GraphRVar(ar0);
            addNode(ar);
        }
        for(int i=0; i<G.size(); i++){
            for(int j=i+1; j<G.size(); j++){
                if(G.existEdge(i,j)) {
                    addEdge(i,j);
                }
            }
        }
    };

    public MarkovNet(UGraph G){ super(G); };

    public MarkovNet(GraphRVar... variables) {
        this();
        for (GraphRVar variable : variables) addNode(variable);
    }

    public void setEdges(UGraphSimple G){
        for(int i=0; i<G.size(); i++){
            for(int j=i+1; j<G.size(); j++){
                removeEdge(i,j);
                if(G.existEdge(i,j)) {
                    addEdge(i,j);
                }
            }
        }
    };

    public GraphRVar getVar(int i){
        GraphRVar g = (GraphRVar) V.elementAt(i);
        return g;
    };

    public FastInstance getRandomInstance(Random rand){
        FastInstance inst = new FastInstance(schema, "random", size());

        for(int i=0; i<size(); i++){
            Attribute attr = getVar(i).attr;
            inst.setValue(rand.nextInt(attr.size()), i);
        }

        return inst;
    }

    public void read(FastDataset data, String FileName, int startLine){
        read(data, FileName, startLine, -1);
    }
    public void read(FastDataset data, String FileName, int startLine, int D){
        MarkovNet dummy = new MarkovNet(this);

        int line = dummy.readBase(FileName, startLine);
        if(D == -1) data.readAllWSchema(FileName, line , ',');
        else data.readAllWSchema(FileName, line , D+line, ',');

        int n = dummy.size();

        for(int i=0; i<n; i++){
            GraphRVar aux =new GraphRVar(dummy.getNode(i), data.schema().getAttribute(i));
            addNode(aux);
            int size = aux.adjacencyList().size();
            for(int j=0; j<size; j++)       aux.removeChild(aux.getChild(0));
        }
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                if(dummy.existEdge(i, j)) addEdge(i, j);
            }
        }
    }
    public MarkovNet(Schema schema){

        this.schema = schema;
        for(int i=0; i<schema.size(); i++){
            this.addNode(new GraphRVar(schema.getAttribute(i)));
        }
    }

    /**
     * See Notebook Margaritis 5, page 29'
     * @param inst
     */
    double sum;



    /*-------------------------------*/
    /**
     * Returns the number of edges.
     */
    public int numberOfEdges()
    {
        return super.numberOfEdges()/2;
    }
    //---------------------------------------------------------------
    //  convert UTF-8 to String
    static private String UTFtoString2(byte b[]) {
        int maxlen = b.length;
        char str[] = new char[maxlen*2];
        int outlen = 0;
        for (int i = 0; i < maxlen; i++) {
            byte c = b[i];
            if ((c & 0x80) == 0) {
                str[outlen++] = (char) c;
            } else if ((c & 0xf0) == 0xe0) {
                str[outlen++] = (char) (((c & 0xf) << 12)
                                        | ((b[i + 1] & 0x3f) << 6) | (b[i + 2] & 0x3f));
                i += 2;
            } else {
                str[outlen++] = (char) (((c & 0x1f) << 6) | (b[i + 1] & 0x3f));
                i++;
            }
            str[outlen++] = ',';
        }
        //String output =  new String(str, 0, outlen);
        return new String(str, 0, outlen);
    }
    final static private String intToString(int b[], char str2 [], int maxlen) {
        //int maxlen = b.length;
        char str [];
        if(str2 == null) str = new char[maxlen*2];
        else str = str2;

        outlen = 0;
        for (i = 0; i < maxlen; i++) {
            str[outlen++] = (char) b[i];
            str[outlen++] = ',';
        }
        //String output =  new String(str, 0, outlen);
        return new String(str, 0, outlen);
    }

    //-----------------------------------------------
    /**
     * It does not compute the partition function.
     * @param inst
     */
    Double w;
    attributesSet cc;
    int [] instC;
    Hashtable hashC;
    FastSet [] fastCliques;
    int numCliques;
    int [][] fastfastCliques;
    int cliquesCard[];
    static int i,j, outlen;
    String aux;
    final public double UnNormalizedProbability_slow(FastInstance inst){
        sum = 0;

        if(Cliques == null) {
            Cliques = MinCliques();
            //Cliques = MaxCliques3();

            numCliques = Cliques.size();
            fastCliques = new FastSet[Cliques.cardinality()];
            for(i=0; i<Cliques.cardinality(); i++) {
                fastCliques[i] = new FastSet(((Set)Cliques.elementAt(i)), n);
            }
        }
        if(fastfastCliques == null){
            cliquesCard = new int[fastCliques.length];
            fastfastCliques = new int [fastCliques.length][];

            for(int i=0; i<fastCliques.length;i++){
                cliquesCard[i] = fastCliques[i].cardinality();
                fastfastCliques[i] = new int[fastCliques[i].cardinality()];
                int k= fastCliques[i].bitset.nextSetBit(0);
                j=0;
                while(k>=0){
                    fastfastCliques[i][j++] = k;
                    k = fastCliques[i].bitset.nextSetBit(k+1);
                }

            }

        }
        int n=inst.size();
        instC = new int[n];
        char [] chars = new char[n*2];
        for(int i=0; i<numCliques; i++){

            hashC = (Hashtable) paramsHashtables.elementAt(i);

            for(j=0; j<cliquesCard[i]; j++){
                instC[j] = inst.values[fastfastCliques[i][j]];
            }
            //Convert instC into a string.

            aux = intToString(instC, chars, cliquesCard[i]);
            sum += ((Double) hashC.get(aux)).doubleValue();
            aux = null;
        }

        return Math.exp(sum);
    }

    final public double UnNormalizedProbability(FastInstance inst){
        return Math.exp(UnNormalizedLogProbability(inst));
    }


    final public double UnNormalizedLogProbability(FastInstance inst){
        sum = 0;
        int n= V.size();

        if(adjMatrix == null){
            adjMatrix = new boolean[V.size()][];
            for(int i=0; i<V.size(); i++) adjMatrix[i] = new boolean[V.size()];
            for(int i=0; i<V.size(); i++) {
                GraphNode X = (GraphNode) V.elementAt(i);
                for(int j=0; j<V.size(); j++){
                    GraphNode Y = (GraphNode) V.elementAt(j);
                    if(X.index() == Y.index()) continue;

                    adjMatrix[i][j] = existEdge(X.index(), Y.index());
                }
            }
        }
        int valXIndex, valYIndex;
        for(int i=0; i<n; i++){
            for(int j=i+1; j<n; j++){
                if(adjMatrix[i][j]){
                    valXIndex = inst.values[i];
                    valYIndex = inst.values[j];

                    sum += params[i][j][valXIndex][valYIndex];
                }
            }
        }
        return sum;
    }

    //////////////////////////////////////////
    ////////// RANDOM MARKOV NETWORKS
    //////////////////////////////////////////
    public UGraph generateChain(int N, int numValues){
        //UGraph G = new UGraph();
        schema = new Schema();
        if(size() !=0){
            System.out.println("ERROR tried to randomized a graph that is not empty.");
            (new Exception()).printStackTrace();
            System.exit(0);
        }
        for(int i=0; i<N; i++){
            Attribute attr = new Attribute(""+i, i, 2);
            schema.addAttribute(attr);
            GraphRVar nn = new GraphRVar(attr);

            addNode(nn);
        }
        for(int i=0; i<N-1; i++){
            addEdge(i,i+1);
        }
        System.out.println(this);
        return this;
    }


    /**
     * Each node will have tau randomly selected edges.
     */
    public UGraph generateRandomStructure(double tau, Random rand, int n, int numValues){
        //for backward compatibility
        double aux = tau%1;

        schema = new Schema();
        if(size() !=0){
            System.out.println("ERROR tried to randomized a graph that is not empty.");
            (new Exception()).printStackTrace();
            System.exit(0);
        }

        for(int i=0; i<n; i++){
            Attribute attr = new Attribute(""+i, i, 2);

            schema.addAttribute(attr);
            GraphRVar nn = new GraphRVar(attr);

            addNode(nn);
        }
        if(tau == 2){
            int aklsd=0;
        }
        //For each node
        for(int i=0; i<n; i++){
            //sample ~[0,1] for each other node
            double [] probs = new double[n];
            for(int j=0; j<n; j++) {
                probs[j] = rand.nextDouble();
                if(i==j) probs[j] = -2; //self-edge never selected
            }
            //sort nodes according to probs and select top tau
            for(int k=0; k<tau; k++){
                double maxP = 0.0;
                int maxIndex=0;
                for(int l=0; l<n; l++){
                    if(probs[l] > maxP){
                        maxP = probs[l];
                        maxIndex = l;
                    }
                }

                addEdge(i,maxIndex);
                probs[maxIndex] = -2;
            }
        }
        return this;
    }

    /**
     * @todo DRY
     */
    public GraphBase generateRandomLattice(int numNodes) {
        Random rand = new Random();
        schema = new Schema();

        for(int i = 0; i < numNodes; i++) {
            Attribute attr = new Attribute(""+i, i, 2);
            schema.addAttribute(attr);
            GraphRVar nn = new GraphRVar(attr);
            addNode(nn);
        }

        if (!Utils.isEven(numNodes)) System.exit(0);

        Pairwise[] pairs = new Pairwise[numNodes];
        for (int i = 0; i < numNodes; i++)
            pairs[i] = new Pairwise(i, rand.nextDouble());

        java.util.Arrays.sort(pairs);

        addEdgeBetweenPairs(pairs);

        return this;
    }

    private void addEdgeBetweenPairs(Pairwise... pairs) {
        for (int i = 0; i < pairs.length - 1; i++)
            addEdge(pairs[i].index_, pairs[i + 1].index_);

        addEdge(pairs[0].index_, pairs[pairs.length - 1].index_);
    }

    private class Pairwise implements Comparable<Pairwise> {
        public int index_;
        public double value_;
        public Pairwise(int index, double value) { add(index, value); }
        public void add(int index, double value) {
            index_ = index; value_ = value;
        }

        public int compareTo(Pairwise par) {
            return (int) ((value_ - par.value_) * 10E12);
        }
    }


    /**
     * @param tau is neighborhood size, i.e., the output network will have tau edges
     */
    public UGraph generateRandomStructure_cardinality(int tau, Random rand,
                                                      int n, int numValues) {
        int m = n*(n-1)/2;
        int card = (int) (tau);
        schema = new Schema();
        if(size() !=0){
            System.out.println("ERROR tried to randomized a graph that is not empty.");
            (new Exception()).printStackTrace();
            System.exit(0);
        }
        for(int i = 0; i < n; i++) {
            Attribute attr = new Attribute(""+i, i, 2);
            schema.addAttribute(attr);
            GraphRVar nn = new GraphRVar(attr);
            addNode(nn);
        }
        //Sample ~[0,1] for each edge.
        int k=0;
        double [] probs = new double[n*n];
        for(int i=0; i<n; i++)
            for(int j=i+1; j<n; j++)
                while(probs[UGraphSimple.e(i,j,n)] == 0)
                    probs[UGraphSimple.e(i,j,n)] = rand.nextDouble();

        //Sort by probs
        for(k=0; k<card; k++){
            double maxP = 0.0;
            int maxIndex=0;
            for(int i=0; i<n*n; i++){
                if(probs[i] > maxP){
                    maxP = probs[i];
                    maxIndex = i;
                }
            }
            int a = maxIndex/n;
            int b = maxIndex%n;
            if(!Utils.FORCED_ASSERT(!existEdge(a,b),
                                    "OOPS MORE NEIGHBORS THAN NODES IN MarkovNet.generateRandomStructure_cardinality")){
                int aslkdja=0;
            }

            addEdge(a,b);
            probs[maxIndex] = -2;
        }

        return this;
    }

    /**
     * @param tau threshold for connectivity. That is, if p(edge)<tau then add the edge.
     */
    public UGraph generateRandomStructureBinomial(double tau, Random rand, int n, int numValues){
        //UGraph G = new UGraph();
        schema = new Schema();
        if(size() !=0){
            System.out.println("ERROR tried to randomized a graph that is not empty.");
            (new Exception()).printStackTrace();
            System.exit(0);
        }
        for(int i=0; i<n; i++){
            Attribute attr = new Attribute(""+i, i, 2);
            schema.addAttribute(attr);
            GraphRVar nn = new GraphRVar(attr);

            addNode(nn);
        }
        for(int i=0; i<n; i++){
            for(int j=i+1; j<n; j++){
                double c = 0, p;
                //for(int k=0; k<100000; k++){
                p = rand.nextDouble();
                if(p<tau) {
                    addEdge(i,j);
                    c++;
                }
                //    }
                //  if(((double)c)/100000.0 < 0.5) addEdge(i,j);
            }
        }
        return this;
    }

    /**
     * There is a parameter for each each configuration of the variables in each maximal clique.
     * @param rand
     */
    public void generateRandomParameters_slow_erase(Random rand){
        if(Cliques == null) {
            //Cliques = MaxCliques3();
            Cliques = MinCliques();
            numCliques = Cliques.size();
            fastCliques = new FastSet[Cliques.size()];

            for(int i=0; i<Cliques.size(); i++) {
                fastCliques[i] = new FastSet(((Set)Cliques.elementAt(i)), n);
            }
        }
        Set c;
        int [] curr;
        GraphRVar var;
        int numParam = 0;

        paramsHashtables.clear();
        for(int i=0; i<Cliques.cardinality(); i++){
            Hashtable hashC = new Hashtable();

            c = (Set) Cliques.elementAt(i);
            attributesSet cc = new attributesSet(c);

            //Pick as many random numbers from [0,1] as configurations of the clique c;
            //Compute number of configurations
            int numConf = 1;
            for(int k=0; k<c.size(); k++) numConf *= ((Attribute) cc.elementAt(k)).size();

            //Obtain a sorted array of size numConf of random numbers from [0,1]
            Vector Pr = new Vector();
            double p_curr;
            int l=0;
            for(int k=0; k< numConf; k++) {
                double p_new = rand.nextDouble();
                for(l=0; l< k; l++) {
                    p_curr = ((Double)Pr.elementAt(l)).doubleValue();
                    if(p_new < p_curr) break;
                }
                Pr.add(l, new Double(p_new));
            }


            //Iterates over all possible configurations of the clique.
            cc.initIterator();
            int k=0;
            double p = ((Double)Pr.elementAt(0)).doubleValue();
            while(cc.hasNext()){
                curr = cc.currentAsIntArray();

                //                              Convert instC into a string.
                String key ="";
                if(curr.length > 0 ) key=""+curr[0];
                for(int j=1; j<curr.length;j++) key += "," + curr[j];

                // Pr[k]-Pr[k-1]
                if(k>0) p = ((Double)Pr.elementAt(k)).doubleValue() - ((Double)Pr.elementAt(k-1)).doubleValue();

                double mult = 1.0;
                //hashC.put(key, new Double(rand.nextDouble()*mult));
                //                byte [] byteCurr = new byte[curr.length];
                //                for( i=0; i<curr.length; i++) byteCurr[i] = (byte) curr[i];
                hashC.put(intToString(curr, null, curr.length), new Double(rand.nextDouble()*mult));

                //System.out.println(key);
                cc.next();
                k++;
            }
            paramsHashtables.add(hashC);    //appends hashC at the end of Vector.
        }
    }

    /**
     * There is a parameter for each each configuration of the variables in each maximal clique.
     * @param rand
     */
    public void generateRandomParameters_slow(Random rand){
        if(Cliques == null) {
            //Cliques = MaxCliques3();
            Cliques = MinCliques();
            numCliques = Cliques.size();
            fastCliques = new FastSet[Cliques.size()];
            for(int i=0; i<Cliques.size(); i++) {
                fastCliques[i] = new FastSet(((Set)Cliques.elementAt(i)), n);
            }
        }
        Set c;
        int [] curr;

        paramsHashtables.clear();
        for(int i=0; i<Cliques.cardinality(); i++){
            Hashtable hashC = new Hashtable();

            c = (Set) Cliques.elementAt(i);
            attributesSet cc = new attributesSet(c);


            //Iterates over all possible configurations of the clique.
            cc.initIterator();
            int k=0;
            while(cc.hasNext()){
                curr = cc.currentAsIntArray();

                //              Convert instC into a string.
                String key ="";
                if(curr.length > 0 ) key=""+curr[0];
                for(int j=1; j<curr.length;j++) key += "," + curr[j];

                double mult = 1.0;
                hashC.put(intToString(curr, null, curr.length), new Double(rand.nextDouble()*mult));

                //System.out.println(key);
                cc.next();
                k++;
            }
            paramsHashtables.add(hashC);    //appends hashC at the end of Vector.
        }
    }

    //double [][][][] params; //params[x][y] stores the table of parameters for sub-clique (edge) x-y.
    /**
     * @todo Supposed that dim is binary... See code comment
     */
    public void allocateParams() { allocateParams(V.size()); }

    public void allocateParams(int n){
        params = new double[n][][][];
        dim = new int[n];

        for(int i=0; i<n; i++)
            dim[i] = 2; //((GraphRVar)V.elementAt(i)).attr.size();

        for(int i=0; i<n; i++) {
            params[i] = new double[n][][];
            for(int j=i+1; j<n; j++){
                if(this.existEdge(i,j)){
                    params[i][j] = new double [dim[i]][];
                    for(int k=0; k<dim[i]; k++){
                        params[i][j][k] = new double[dim[j]];
                    }
                }
            }
        }

    }
    /**
     * There is a parameter for each each configuration of the variables in each maximal clique.
     * @param rand
     */
    /**
     * @param rand
     */
    public void generateRandomParameters(Random rand){
        int n = V.size();

        double r;

        double a00, a01, a11, a10, alpha;

        allocateParams();
        for(int i=0; i<n; i++){
            for(int j=i+1; j<n; j++){
                if(existEdge(i,j)){
                    Utils.FORCED_ASSERT(dim[i] == 2 && dim[j] == 2,
                                        "Error in MarkovNetwork.generateRandomParameters: NON-BINARY VARIABLES");
                    if(epsilon > 0){
                        double sigma = 0.2, mean = 1.0;
                        a00 = rand.nextGaussian()*sigma+epsilon/2.0+mean;
                        a11 = rand.nextGaussian()*sigma+epsilon/2.0+mean;
                        a01 = rand.nextGaussian()*sigma+mean;
                        a10 = a00+a11-a01-epsilon;
                    }
                    else{ //understood as totally random
                        a00 = rand.nextDouble();
                        a11 = rand.nextDouble();
                        a01 = rand.nextDouble();
                        a10 = rand.nextDouble();
                    }

                    params[i][j][0][0] = a00;
                    params[i][j][1][0] = a10;
                    params[i][j][0][1] = a01;
                    params[i][j][1][1] = a11;
                }
            }
        }
    }

    public String paramsToString() {
        String output = "";
        int n = V.size();

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (existEdge(i, j)) {
                    output += "" + i + "," + j + ",";
                    output += "" + params[i][j][0][0] + ",";
                    output += "" + params[i][j][0][1] + ",";
                    output += "" + params[i][j][1][0] + ",";
                    output += "" + params[i][j][1][1] + System.getProperty("line.separator");
                }
            }
        }

        return output;
    }

    public String writeParameters(int tau) throws Exception {
	String parametersS = paramsToString();

	java.io.FileWriter fstream =
	    new FileWriter("parameters_n"+V.size()+"_t"+tau+"_E"+(int)epsilon*100+".csv");
	java.io.BufferedWriter out = new BufferedWriter(fstream);

	out.write(parametersS);

	out.close();

	return parametersS;
    }

    public void readParameters(String filename) throws Exception {
	String[] lineSplit;
	String line;
	int i,j, k = 0;
        java.io.BufferedReader reader = null;

	int n = Utils.getNumVariablesFromFilename(filename);
	allocateParams(n);

	reader = new BufferedReader(new FileReader(filename));

	while (true) {
	    line = reader.readLine();

	    if (line == null) break;

	    lineSplit = line.split(",");

	    i = new Integer(lineSplit[k++]);
	    j = new Integer(lineSplit[k]);

	    params[i][j][0][0] = new Double(lineSplit[++k]);
	    params[i][j][0][1] = new Double(lineSplit[++k]);
	    params[i][j][1][0] = new Double(lineSplit[++k]);
	    params[i][j][1][1] = new Double(lineSplit[++k]);

	    k = 0;
	}
    }

    /**
     * Friedman&Koller's prior  Being Bayesian about Network Structure
     */
    public double kollerPrior2(){
        double p=1;
        int n = size();
        int k;

        for(int i=0; i<n; i++){
            k = getNode(i).degree();
            double aux = 1.0/(double)Utils.choose(n-1, k);
            p *= aux;
        }

        return p;
    }


    ////////////////////////////////////////////////////////
    ////////// GIBBS SAMPLING AND HELPING FUNCTIONS
    //////////////////////////////////////////////////////////
    //Given an instance X and a variable index j, it returns the distribution p(X_j | X_1, X_2, ..., X_j-1, X_j+1, ...., X_n))
    // See Jordan03MCMC p. 22
    //    final private double [] conditionalDistribution(int j, final FastInstance origInst){
    //      conditionalDistribution(j, origInst, null);
    //    }

    /**
     * WARNING: Destroys values in origInst;
     */
    final private double []  conditionalDistribution(int i, final FastInstance origInst, double p []){
        int origValue;
        int size = schema.getAttribute(i).size();
        double marginal = 0, sum = 0;
        double p_aux [] = p;//new double[p.length];
        double p_aux2 [] = new double[p.length];
        int j,k;

        //Compute Joints
        for(j=0; j<size; j++){
            //for(j=size-1; j!=0; j--){
            origInst.setValue(j, i);
            p_aux[j] = UnNormalizedLogProbability(origInst);;
            //p[j] = UnNormalizedProbability(origInst);
            //marginal += p[j];
        }

        //Compute Marginals
        for(j=0; j<size; j++){
            //for(j=size-1; j!=0; j--){
            sum=0;
            for(k=0; k<size; k++){
                //for(k=size-1; k!=0; k--){
                sum += Math.exp(p_aux[k] - p_aux[j]);
            }
            p[j] = p_aux2[j] = 1.0/sum;
        }

        //Normalize joints by the marginal
        /*              for(int j=0; j<size; j++){
                        p[j] /= marginal;
                        }*/

        return p;
    }
    //Notebook Margaritis 1 (4), p. 49

    final private FastInstance nextGibbsSample(FastInstance Xinst){
        int val;

        //double [] p_i = new double[Xinst.size()+1]; //the plus 1 is necessary inside conditionalDistribution.
        //To sample from a univariate distribution (the conditionals) I use rejection sampling with a uniform proposed distribution.
        for(int i=0; i<Xinst.size(); i++){
            conditionalDistribution(i, Xinst, p_i);
            val = Statistics.sampleFromUnivariateDestructive(p_i, rand);
            Xinst.setValue(val, i);
        }

        return Xinst;
    }

    /**
     * If outputFile != null, then it saves instance by instance (or maybe every 100 of them) @return
     */
    public FastDataset gibbsSample(int D, int independenceInterval,
                                   int numValues, FastInstance init,
                                   Random r, boolean randInit) {
        FastDataset sample = new FastDataset("Sampled using Gibbs Sampling MCMC");
        int j=0;
        FastInstance pInst = init;
        String str;

        p_i = new double[numValues +1 ];//the plus 1 is necessary inside conditionalDistribution.
        if (r == null) rand = new Random(1234);
        else rand = r;

        //System.out.println("Sampling of n= "+D+ " samples
        //initiated, and independenceInterval = " +
        //independenceInterval);

        int i=0;

        if(randInit)  pInst = getRandomInstance(rand);
        //System.out.println("RANDOM INITIAL INST(:"+ j + ")" + pInst);
        while(true){
            pInst = nextGibbsSample(pInst);

            if((i+1) % independenceInterval == 0){
                sample.addInstance(pInst.makeCopy());
                //  System.out.println("FINAL(:"+ j + ")" + pInst);
                j++;
                if (randInit)  pInst = getRandomInstance(rand);
                //System.out.println("RANDOM INITIAL INST(:"+ j + ")" + pInst);
                if (j >= D) break;
            }
            i++;
        }
        return sample;
    }

    public double[][][][] getParams() { return params; }
    public double getParamsAt(int i, int j, int k, int z) { return params[i][j][k][z]; }
    public double[] getParamsOf(int indexVarA, int indexVarB) {
        return new double[] {
            params[indexVarA][indexVarB][0][0],
            params[indexVarA][indexVarB][0][1],
            params[indexVarA][indexVarB][1][0],
            params[indexVarA][indexVarB][1][1]
        };
    }
    public double setParamsAt(int i, int j, int k, int z, double value) {
        return params[i][j][k][z] = value;
    }
    public void setEpsilon(double e) { epsilon = e; }
}
