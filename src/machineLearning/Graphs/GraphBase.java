package machineLearning.Graphs;
/*
  /*
  *    Directed Graph.java
  *    @author Facundo Bromberg. 2002.
  *
  *
  */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.Vector;
import java.util.Random;

import machineLearning.Utils.FastSet;
import machineLearning.Utils.Set;
import machineLearning.Utils.Utils;

import machineLearning.Datasets.Schema;

/**
 * Base class for graph. <!-- -->
 *
 * @author Facundo Bromberg
 * @version 1.0 Spring 2005.
 */
abstract public class GraphBase implements Iterable<GraphNode> {
    int n;

    /** Vector of vertices (or nodes). */
    protected Vector V;

    /**
     * Initializes the graph.
     */
    public GraphBase() {
        V = new Vector(0, 5);
    }
    /**
     * Initializes the graph with the same number of nodes as in input G, but no edges.
     */
    public GraphBase(GraphBase G) {
        n = G.size();
        V = new Vector(0, G.size());
        for(int i=0; i<G.size(); i++){
            addNode(new GraphicalModelNode(G.getNode(i).name(), G.getNode(i).index()));
        }
    }

    /**
     * @return true if there exist a node with index equal to <code>index</code>.
     */
    public boolean containsIndex(int index) {
        for (GraphNode node : this) if (node.index() == index) return true;
        return false;
    }

    public Vector V(){return V;};

    public FastSet Vset(){
        FastSet cond = new FastSet(V.size());
        for(int i=0; i<V.size(); i++){
            cond.add(((GraphicalModelNode)V.elementAt(i)).index());
        }
        return cond;
    }

    /*------------------------------*/
    /**
     * Display a representation of the graph as an adjency list.
     */
    public String toString() {

        int TAB = 10;

        //String out = "Adjacency lists of the graph: \n\n";
        String out = "\nVAR                             VAR CHILDREN\n\n";
        for(int i=0; i<size(); i++) {
            out += getNode(i).toStringWithNeighbors() + "\n";
        }
        return out;

    }

    /*-------------------------------*/
    public String toStringHydrology()
    {
        int TAB = 10;

        //String out = "Adjacency lists of the graph: \n\n";
        //String out = "\nVAR                             VAR CHILDREN\n\n";
        String out = "";
        for(int i=0; i<size(); i++) {
            GraphicalModelNode ni = getNode(i);
            out += ni.name + "\n\n";
            for(int j=0; j<ni.degree(); j++){
                out += ni.name + "\n" + ni.getChild(j).name + "\n\n";
            }
        }
        return out;
    }
    /*-------------------------------*/
    /**
     * Returns the number of vertices (nodes) in the graph.
     */
    public int size() { return V.size(); }

    /*-------------------------------*/
    /**
     * Returns the number of edges.
     */
    abstract public int numberOfEdges();

    /*-------------------------------*/
    /**
     * Returns true if there is an edge from the source-th node in V  to the target-th node in V.
     */
    abstract public boolean existEdge(int source, int target);
    /*-------------------------------*/
    /**
     * Adds a node to the graph.
     */
    public void addNode(GraphicalModelNode newNode)
    {
        V.add(newNode);
    }
    /*-------------------------------*/
    /**
     * Returns the i-th GraphicalModelNode in the nodes vector V.
     */
    final public GraphicalModelNode getNode(int i)
    {
        return (GraphicalModelNode) V.elementAt(i);
    }
    /*-------------------------------*/
    /**
     * Cretes an edge from source to target vertices.
     * @return False if the edge already exists.
     */
    abstract public boolean addEdge(int source, int target);

    /*-------------------------------*/
    /**
     * Removes the edge from source to target vertices.
     * @return True if node v existed in adjacency list (children) of u AND node u existed in parents list of v.
     */
    abstract public boolean removeEdge(int source, int target);

    /*-------------------------------*/
    /**
     * Checks the graph and returns true if there is any cycle in the graph. It uses the DFS
     * algorithm to clasify the edges as tree, back, forward or cross and returns true if there
     * is any back edge. It runs in O(V+E).
     */
    public boolean existCycle()
    {
        int n = size();
        int  [] d = new int[n];
        int [] f = new int[n];

        for(int i=0; i<n; i++){
            d[i] = -1;
            f[i] = -1;
        }

        int time = 0;

        for(int i=0; i<n; i++){
            GraphicalModelNode u = (GraphicalModelNode) V.elementAt(i);

            if(d[i] == -1)
                {
                    if(visit(u, i, time, d, f)) return true;
                }
        }
        return false;
    }
    /*------------------------------*/
    private boolean visit(GraphicalModelNode u, int pos_u, int time, int [] d, int [] f)
    {
        time += 1;

        d[pos_u] = time;
        for(int i=0; i<u.outDegree(); i++)
            {
                GraphicalModelNode v = (GraphicalModelNode) u.getChild(i);
                int pos_v = V.indexOf(v);
                if(d[pos_v] == -1) {
                    if(visit(v, pos_v, time, d, f)) return true;
                }
                else {
                    if(f[pos_v] == -1) {
                        //                  System.out.println(toString());
                        return true; //there is a cycle!
                    }
                }
            }
        time += 1;
        f[pos_u] = time;
        return false;
    }


    /**
     * @param S Set of GraphicalModelNodes
     * @return
     */
    public static FastSet convertToFastSet(Set S, int capacity){
        FastSet cond = new FastSet(capacity);
        for(int i=0; i<S.size(); i++){
            cond.add(((GraphicalModelNode)S.elementAt(i)).index());
        }
        return cond;
    }
    /**
     * @param S FastSet
     * @return
     */
    public Set convertToSet(FastSet FS){
        Set S = new Set();
        for(int i=0; i<FS.cardinality(); i++){
            S.add(V.elementAt(FS.getOn(i)));
        }
        return S;
    }


    private GraphicalModelNode parseLine(String line, Vector neighbors){
        if(line == null) return null;
        int index;

        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new StringReader(line));
        }
        catch(Exception ioe){}

        StreamTokenizer tokenizer = new StreamTokenizer(reader);

        tokenizer.resetSyntax();
        //tokenizer.whitespaceChars(':',':');
        tokenizer.whitespaceChars(' ', ' ');
        tokenizer.wordChars(' '+1,'\u00FF');
        tokenizer.wordChars(':',':');

        /* tokenizer.wordChars('A','FastZ');
           tokenizer.wordChars('a','z');
           tokenizer.wordChars('0','9');
           tokenizer.wordChars('>','>');
           tokenizer.wordChars('-','-');
        */tokenizer.parseNumbers();
        tokenizer.eolIsSignificant(true);

        int TTNUMBER = StreamTokenizer.TT_NUMBER;
        int TTWORD = StreamTokenizer.TT_WORD;
        int TTEOF = StreamTokenizer.TT_EOF;
        int TTEOL= StreamTokenizer.TT_EOL;

        try{tokenizer.nextToken();}
        catch (Exception te){}

        //      First Token must be a number
        if(tokenizer.ttype != StreamTokenizer.TT_NUMBER) return null;
        index = (int)tokenizer.nval;

        try{tokenizer.nextToken();}
        catch (Exception te){}

        //Second token must be -->
        if(tokenizer.ttype != StreamTokenizer.TT_WORD) return null;
        if(!tokenizer.sval.equals(":")) return null;


        //We have a valid node
        GraphicalModelNode newNode = new GraphicalModelNode(""+index, index);

        try{tokenizer.nextToken();}
        catch (Exception te){}
        while(tokenizer.ttype != StreamTokenizer.TT_EOL && tokenizer.ttype != StreamTokenizer.TT_EOF)
            {
                if(tokenizer.ttype != StreamTokenizer.TT_NUMBER) {
                    System.out.println("ERROR: there seems to be a misformed structure in line: \n" + line);
                    (new Exception()).printStackTrace();
                    return null;
                }
                neighbors.add(new Integer((int)tokenizer.nval));

                try{tokenizer.nextToken();}
                catch (Exception te){}
            }

        return newNode;
    }

    public void save(String fileName){
        //Utils.writeStringToFile(fileName, , true, false);
        Utils.writeStringToFile(toString(), fileName, false, false);
    }

    public int readBaseNth(String fileName, int n){
        int line=0;
        for(int i=0; i<n; i++){
            line=readBase(fileName, line);
        }
        return readBase(fileName, line);
    }
    //----------------------------------------------------------------------------
    public int readBase(String fileName, int startAtLine){
        V.clear();

        String line="" ;
        int lineCounter = startAtLine;

        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new FileReader(fileName));
        }
        catch(Exception ioe){
            throw new RuntimeException("Cannot access to the following file: " + fileName + ". \n" + ioe.getMessage());
        }

        //Skips startAtLine first lines.
        for(int i=0; i<startAtLine-1; i++) {
            try {
                line = reader.readLine();
            }catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        //Find (next) start of network description
        if(!reader.markSupported()){
            System.out.println("ERROR: stream does not support marks.");
            (new Exception()).printStackTrace();
        }
        try{reader.mark(10);}
        catch(Exception ie){};

        Vector dummy = new Vector();
        try { line = reader.readLine(); lineCounter++;}
        catch (IOException ex) { }
        while(parseLine(line, dummy) == null){
            try{reader.mark(10);}
            catch(Exception ie){};

            try { line = reader.readLine(); lineCounter++;}
            catch (IOException ex) { }
        }

        //Start reading network structure
        try{reader.reset();}
        catch(Exception ie){};

        GraphicalModelNode newNode;
        Vector neighbors = new Vector();
        Vector AdjacencyMatrix = new Vector();
        try { line = reader.readLine(); lineCounter++;}
        catch (IOException ex) { }
        while((newNode=parseLine(line, neighbors)) != null){
            AdjacencyMatrix.add(neighbors);
            addNode(newNode);

            try { line = reader.readLine(); lineCounter++;}
            catch (IOException ex) { }
            neighbors = null;
            neighbors = new Vector();
        }
        lineCounter--;

        for(int i=0; i<V.size(); i++){
            neighbors = (Vector) AdjacencyMatrix.elementAt(i);

            for(int j=0; j<neighbors.size(); j++){
                Integer neighborIndex = (Integer) neighbors.elementAt(j);
                addEdge(i, neighborIndex.intValue());
            }
        }
        // System.out.println(this);
        return lineCounter;
    }


    public java.util.Iterator<GraphNode> iterator() {
        return new java.util.Iterator<GraphNode>() {
            int index;
            public boolean hasNext() {
                return (index < size()) ? true : false;
            }

            public GraphNode next() {
                return getNode(index++);
            }
            public void remove()
                throws java.lang.UnsupportedOperationException {
                throw new java.lang.UnsupportedOperationException();
            }
        };
    }
}


