package machineLearning.Graphs;

import java.util.BitSet;
import machineLearning.Graphs.EulerNode;
import machineLearning.Graphs.GraphsSimple.*;
import machineLearning.Utils.*;
/*
 * Created on Jan 29, 2006
 *
 */

/**
 * @author bromberg
 *
 */
public class dynamicGraph extends UGraphSimple{
	//occ[index of (a,b)][0] is first occurence of a in edge
	//occ[index of (a,b)][1] is first occurence of b in edge
	//occ[index of (a,b)][2] is second occurence of a in edge
	//occ[index of (a,b)][3] is second occurence of b in edge
	//occurences of b are consecutive to those of a.
	protected EulerNode [] treeOcc;	 
	protected EulerNode [] activeOcc;
	protected FastSet treeEdges;
	
	FastSet S;
	
	UGraphSimple ST;

	//AUXILIARY
	static int [] ccEmpty = null;
	static int [] ccA, ccB;

/////////////////////////////////////////////////////////////////	
//////////////////////CONSTRUCTORS/////////////////////////
///////////////////////////////////////////////////////////////
	
	public dynamicGraph(int n, FastSet S){
		super(n);
 		EulerNode.debug = debug;
		
		EulerNode.createNil();
		
		if(S != null) this.S = new FastSet(S);
		
		activeOcc = new EulerNode[n];
 		treeOcc = new EulerNode[m*2*4];
 		treeEdges = new FastSet(m*2);
 		for(int i=0; i<n; i++){
 			activeOcc[i] = new EulerNode(i, this);
 			activeOcc[i].active = true;
 		}
 		
 		constructForest(S);
 		
		if(ccEmpty == null) {
			ccEmpty = new int [n];
			ccA = new int[n];
			ccB = new int[n];
		}
	}
	
	/**
	 * very expensive, reconstruct the while tree.
	 */
	/*public dynamicGraph(dynamicGraph np){
		super(np);
		
		if(S != null) this.S = new FastSet(S);
		
		activeOcc = new EulerNode[n];
 		treeOcc = new EulerNode[m*2*4];
 		treeEdges = new FastSet(m*2);
 		for(int i=0; i<n; i++){
 			activeOcc[i] = new EulerNode(i, this);
 			activeOcc[i].active = true;
 		}
 		
		constructForest(np.S);
	}*/
/////////////////////////////////////////////////////////////////	
//////////////////////INTERFACE /////////////////////////
///////////////////////////////////////////////////////////////
	
	final public boolean addEdge(int a, int b){
		if(Utils._ASSERT) super.addEdge(a,b);
		else{
			if(!existEdge(a, b)) numberOfEdges++;
		  	else return false;
		  	
		  	adjMatrix[a][b] = true;//adjMatrix[s].set(t);
		  	adjMatrix[b][a] = true;  	//adjMatrix[t].set(s);
		}
	  	
	  	
	  	//we link if neither a nor b are in S.
	  	if(S != null && (S.isMember(a) || S.isMember(b))) return true;
	  	
		EulerNode root_a = getRoot(a), root_b = getRoot(b);
		if(root_a == root_b) return true;
		
		EulerNode et = EulerNode.linkST(root_a, root_b, a, b);
		treeEdges.add(e(a,b));
		ST.addEdge(a,b);
		
		if(Utils._ASSERT) {
//			if(!assertConsistency()){
				if(debug) {
					System.out.println(this + "\n ------------------------------ \n");
					System.out.println(et + "\n ------------------------------ \n");
				}
//			}
		}
		return true;
	}
	

	final public int flipEdge(int s, int t)
	  {
	  		if(adjMatrix[s][t]){
	  			if(!removeEdge(s, t)) return 0;
	  			return -1;
	  		}
	  		else{
	  			if(!addEdge(s,t)) return 0;
	  			return 1;
	  		}
	  }
	  
	final public boolean separates(int a, int b){
		return getRoot(a) != getRoot(b);
	}
	
	public final int findReplacement(EulerNode root_a, EulerNode root_b){
		int i,j,k, e;
		boolean ccBempty = true;
		EulerNode root;
		int [] ccA2 = ccA, ccB2 = ccB;
		ccA[2] = 9;
		System.arraycopy(ccEmpty, 0, ccA, 0, n);
		System.arraycopy(ccEmpty, 0, ccB, 0, n);
		int ccBsize=0, ccAsize = 0;
		
		for(i=0; i<n; i++ ){
			root = getRoot(i);
			
			if(root == root_a) {
				ccA[ccAsize++] = i;
				for(j = 0; j<ccBsize; j++){
					if(existEdge(i, ccB[j])) {
						return e(i,ccB[j]);
					}
				}
			}
			if(root == root_b) {
				ccB[ccBsize++] = i;
				for(j = 0; j<ccAsize; j++){
					if(existEdge(i, ccA[j])) {
						return e(i,ccA[j]);
					}
				}
			}
		}
		return -1;	//replacement not found
	}
	
	/**
	 * @param returnSet assumed empty and of capacity 2*m (i.e. n*(n-1))
	 */
	public final int replacementSet(EulerNode root_a, EulerNode root_b, FastSet  replacementSet){
		int i,j,k, e;
		boolean ccBempty = true;
		EulerNode root;
		int [] ccA2 = ccA, ccB2 = ccB;
		ccA[2] = 9;
		System.arraycopy(ccEmpty, 0, ccA, 0, n);
		System.arraycopy(ccEmpty, 0, ccB, 0, n);
		int ccBsize=0, ccAsize = 0;
		
		int setSize = 0;
		for(i=0; i<n; i++ ){
			root = getRoot(i);
			
			if(root == root_a) {
				ccA[ccAsize++] = i;
				for(j = 0; j<ccBsize; j++){
					/*if(existEdge(i, ccB[j]))*/ replacementSet.add(e(i,ccB[j]));
				}
			}
			if(root == root_b) {
				ccB[ccBsize++] = i;
				for(j = 0; j<ccAsize; j++){
					/*if(existEdge(i, ccA[j]))*/ replacementSet.add (e(i,ccA[j]));
				}
			}
		}
		return setSize;
	}
	


	private void constructForest(FastSet S){
		this.S = S;//new FastSet(S);
		
		FastSet cc;
		FastSet all = new FastSet(n);
		all.setAlltoOne();
		if(S != null) all = FastSet.Minus(all, S);
//		int z;

		ST = new UGraphSimple(n);
//		EulerTree forestGlobal = new EulerTree(n);
		while(!all.isEmpty()) {
//			z = all.getOn(0);
//			cc = connectedComponent_nonDynamic(z, S, ST);
//			EulerNode et = EulerNode.construct(ST, z, this);
			//forests[numTrees++] = et;
			
//			all = FastSet.Minus(all, cc);
		}
	}
/////////////////////////////////////////////////////////////////	
////////////////////// PRIVATE METHODS /////////////////////////
///////////////////////////////////////////////////////////////
	final public EulerNode getRoot(int a){
		return (EulerNode) EulerNode.getRoot(activeOcc[a]);
	}
	
/////////////////////////////////////////////////////////////////	
//////////////////////STATIC  METHODS /////////////////////////
///////////////////////////////////////////////////////////////
	public static void main(String[] args) {
		Utils._ASSERT = true;
		
		dynamicGraph G = new dynamicGraph(7, null);
		dynamicGraph.debug = true;
		G.addEdge(0,3);
		G.addEdge(2,3);
		G.addEdge(1,3);
		G.addEdge(0,4);
		G.addEdge(4,5);
		G.addEdge(5,6);
		G.addEdge(4,6);
		G.addEdge(2,6);
		G.addEdge(1,5);
		
		//G.removeEdge(2,6);
		G.removeEdge(0,3);
		G.removeEdge(1,5);
		G.addEdge(1,6);
		G.addEdge(1,4);
		G.addEdge(1,0);
		G.addEdge(4,6);
		G.addEdge(5,2);
		G.addEdge(0,2);
		G.removeEdge(1,5);
		G.removeEdge(1,3);
		G.removeEdge(2,6);
		G.removeEdge(4,1);
		
		
/*		EulerTree [] ets = et.cutST(0,4);
		ets[0].assertConsistency(false);
		ets[1].assertConsistency(false);
		System.out.println(ets[0] + "\n ------------------------------ \n");
		System.out.println(ets[1] + "\n ------------------------------ \n");
		
		EulerTree.linkST(ets[0], ets[1], 3, 4); 
		ets[0].assertConsistency(false);
		EulerTree [] ets2 = ets[0].cutST(0,3); 

		UGraphSimple Gp = ets[0].reconstructST(G.size());
		System.out.println(Gp + "\n ------------------------------ \n");
		Gp = ets[1].reconstructST(G.size());
		System.out.println(Gp + "\n ------------------------------ \n");

		EulerTree.linkST(ets[0],ets[1], 0, 1);
		
//		UGraphSimple Gp = et.reconstructST(G.size());

*/	}
}
