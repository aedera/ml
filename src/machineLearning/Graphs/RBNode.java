/*
 * Created on Mar 6, 2006
 *
 */
package machineLearning.Graphs;

import machineLearning.Utils.Utils;
import machineLearning.DataStructures.*;

/**
 * @author bromberg
 *
 * Implements a Red Black Tree which is a balanced binary search tree with a slight
 * modification necessary for efficient join of two RBTrees (see RBTree.join).
 * This modification sets the keys of all nodes to be its original key plus a constant
 * RBNode.treeKey;
 * This way, we can add a constant to all keys in the tree in constant time; 
 *  
 */
public class RBNode {
	int bh = 0;	//make sense only if this is the root, which is true
				// if its parent is nil
	
	static boolean debug = false;
	
	static RBNode nil = null;

	protected Object content;
	protected boolean red; //if true color is red, otherwise its black.
	protected int key;
	protected RBNode left;
	protected RBNode right;
	protected RBNode p;	//parent

	//RBNode root;

	static public boolean printContent = true;
	//public int masterKey = 0;
	
///////////////////////////////////////////////////////
///////////  CONSTRUCTOR/S //////////////
/////////////////////////////////////////////////////
	public RBNode(Object content, int key){
		this.content = content;
		red = true; //if true color is red, otherwise its black.
		this.key = key;
		left = nil;
		right = nil;
		p = nil;	//parent
	}
	public RBNode(){
		content = null;
		red = false; //if true color is red, otherwise its black.
		key = -1;
		left = nil;
		right = nil;
		p = nil;	//parent
		
		bh = 1;
	}
	
	/**
	 * Does not clone pointers left, right, and p;
	 */
	public RBNode my_clone(){
		if(this == nil) return nil;
		
		RBNode newNode = new RBNode();
		
		newNode.content = content;
		newNode.red = red;
		newNode.key = key;
		
		return newNode;
	}

	public RBNode cloneTree(){
		RBNode newTree = my_clone();
		
		if(newTree != nil){
			newTree.left = left.cloneTree();
			newTree.right = right.cloneTree();
		}
		return newTree;
	}
///////////////////////////////////////////////////////
///////////  INTERFACE //////////////
/////////////////////////////////////////////////////
	static public void createNil(){
		if(nil == null){
			nil = new RBNode();
			nil.red = false;
			nil.key = -2;
			nil.left = null;
			nil.right = null;
			nil.p = null;
		}
	}
	/**
	 *  Return root. 
	 */
	public static final RBNode insert(RBNode root, RBNode z){
	
		//Here we copy algorithm from book
		RBNode x = root;
		RBNode y = nil;
		while(x != nil){
			y = x;
			if(z.key < x.key) x = x.left;
			else x = x.right;
		}
		return insertInternal(root, z, y, false, false);
	}
	public static final RBNode insertRight(RBNode root, RBNode z){
			return insertInternal(root,z , treeMaximum(root), true, false);
		}
	public static final RBNode insertLeft(RBNode root, RBNode z){
		return insertInternal(root, z, treeMinimum(root), true, true);
	}
	/**
	 * Deletes node z. If y is provided (!=null) is taken to 
	 * be the successor of z, otherwise we call treeSuccessor(z).
	 * This functionality is used when keys are inconsistent
	 * but ordering is known externally (e.g. Euler trees).
	 * 
	 * Return the new root if changed.
	 */
	public static RBNode delete(RBNode root, RBNode z){
		//RBNode z = treeSearch(k);
		RBNode y;
		
		if(z == nil) return root;
		
		RBNode x;
		if(z.left == nil || z.right == nil)  y = z;
		else  y = treeSuccessor(z);
		
		if(y.left != nil) x = y.left;
		else x = y.right;
		
		x.p = y.p;
		if (y.p == nil) {
			x.bh = root.bh;
			root = x;
		}
		else{
			if(y == y.p.left) y.p.left = x;
			else y.p.right = x;
		}
		if(y != z) {
			z.key = y.key;
			z.content = y.content;
		}
		if(!y.red) {	
			root = deleteFixup(root, x);
		}
		return root;
	}
	
	
	/*public boolean separated(int key_x, int key_y)	{//equivalent to !connected
		RBNode x = treeSearch(key_x);
		RBNode y = treeSearch(key_y);
		
		return (getRoot(x).key != getRoot(y).key); 
	}
	*/
	/**
	 * Used to get the root of the tree. We provide
	 * a static and a non-static version
	 */
	static final public RBNode getRoot(RBNode z){
		if(z == nil) return nil;
		RBNode x = z;
		while(x.p != nil) x = x.p;
		
		return x;
	}
	final public RBNode getRoot(){
		RBNode x = this;
		while(x.p != nil) x = x.p;
		
		return x;
	}

	/**
	 * It tests that the red black tree and binary search tree properties hold.
	 * Also, it checkes that tree is actually balanced by testing that
	 * bh<2lg(n+1) (where n is output of testBinarySearchProperty(),
	 * and bh is the black height and is the output of testRedBlackProperties(). 
	 */
	public boolean assertConsistency(){
		RBNode root = getRoot();
		if(root == nil) return true;
		
		int bh = testRBProperties(root)-1;
		int n = testBinarySearchProperty(root, true);
		
		double aux = 2*(Math.log10(n+1) / Math.log10(2));
		
		if(debug){
			System.out.println("n="+ n+ ", bh=" + bh + " <= 2*lg(n+1)=" + aux);
		}
		return Utils.ASSERT(bh == this.bh && bh >= 0 && n >= 0 && bh <= aux, "Red Black tree is inconsistent");
	}

	/**
	 * Implements the solution of problem 13.2 (RB-JOIN) of Introduction to Algorithms of Cormen.
	 * See also ass3.tex in \Research\Projects\ongoing\posterior.
	 * Given an input tree T2 (i.e., its root), it merges with this in O(lg n) time, resulting
	 * in a balanced tree.
	 * 
	 * WARNING: The outcome tree fulfills the binary search property
	 * only if all keys in T1 are smaller than all keys in T2. Used to cut and merge euler trees.
	 * 
	 * WARNING 2: it destroys the meaning of keys. The may become repeated if
	 * the same key was in left and right.
	 */
	public static final RBNode join(RBNode T1, RBNode T2){
		int h;
		RBNode y;
		
//Now do problem 13-2 which joins T1 and T2 with x as root.
		if(T1 == nil) return T2;
		if(T2 == nil) return T1;
		if(T1.bh >= T2.bh){
			int bh = T1.bh;
			
			//First find node with largest key and remove it from T1
			RBNode x = treeMaximum(T1);
			T1 = delete(T1, x);

			if(T1.bh < bh){
				T2 = insertLeft(T2, x);
				return join(T1, T2);
			}
			if(T1 == nil){
 				T2 = insertLeft(T2, x);
				return T2;
			}
			//Find largest key in T1 whose bh equals bh[T2]
			y = T1;
			h = T1.bh;
			while(true){
				if(h == T2.bh) break;
				//y.right.p = y;	//for case in which y.right is nil
				y = y.right;
				if(!y.red) h = h-1;
			}
						
//Set x in the position of y and set its left child to be y and right child to be T2
			if(y != nil && y.p == nil) {
				x.bh = T1.bh;
				T1 = x;
			}
			
//			edge p[y]-x
			x.p = y.p;
			y.p.right = x;
			
			//edge x-y
			y.p = x;
			x.left = y;
			
			//edge x-T2
			T2.p = x;
			x.right = T2;
			
			x.red = true;
			
			T1 =  insertFixup(T1,x);
			
			return T1;
		}
		else{
			int bh = T2.bh;
			//First find node with smallest key and remove it from T2
			RBNode x = treeMinimum(T2);
			T2 = delete(T2, x);
	
			if(T2.bh < bh){
				T1 = insertRight(T1, x);
				return join(T1, T2);
			}

			if(T2 == nil){
 				T1 = insertRight(T1, x);
 				return T1;
			}

			//Find smallest key in T2 whose bh equals bh[T1]
			y = T2;
			h = T2.bh;
			while(true){
				if(h == T1.bh) break;
				y = y.left;
				if(!y.red) h = h-1;
			}
			
//Set x in the position of y and set its right child to be y and left child to be T1
			if(y != nil && y.p == nil) {
				x.bh = T2.bh;
				T2 = x;
			}
			
			//edge p[y]-x
			x.p = y.p;
			y.p.left = x;
				
			//edge x-y
			y.p = x;
			x.right = y;
			
			//edge x-T1
			T1.p = x;
			x.left = T1;
			
			x.red = true;
			
			T2 = insertFixup(T2, x);
			
			return T2;
		}
	}
	/**
	 * Given a valid red-black tree and a node "a", this procedure
	 * returns two valid (i.e. balanced) red-black trees,
	 * one containing all nodes smaller than a, and the other containing
	 * all nodes larger than a. Node a is included in one or the other 
	 * depending on "inclusive".
	 * 
	 * The approach is similar to the procedure split of binary search trees:
	 * move a up to the root through left and right rotations (that maintain the 
	 * binary search property) and then return left and right sub-trees. 
	 * However, here we must maintain the left and right trees as valid red-black trees. 
	 * The algorithms has a run time complexity of O(log^2 n).
	 * 
	 * Note this tree is not a valid red-black tree at the output of the procedure.
	 */
	public static final RBNode [] split(RBNode a, boolean inclusive){
		RBNode [] T = new RBNode[2];	
		T[0] = nil;
		T[1] = nil;
		
		RBNode Tpaux, Tsaux;// = new RBNode(), Tsaux = new RBNode();
		
		RBNode x = a, y, xp;

		if(a == nil) return T;
		
		if(inclusive) {
			T[1] = x.right;
			setAsRoot(T[1]);
		}
		else {
			T[0] = x.left;
			setAsRoot(T[0]);
		}
		do
		{
			if(inclusive){
				y = x.p;
				inclusive = (x == x.p.right);
				
				//splice out x;
				if(x.p.left == x) x.p.left = x.right;
				else x.p.right = x.right;
				
				Tpaux = x.left;
				setAsRoot(Tpaux);
				Tpaux = insertRight(Tpaux, x);
				
				T[0] = join(Tpaux, T[0]);		//because all keys in Tpaux are smaller than all those in Tp.
				x = y;
			}
			else{
				y = x.p;
				inclusive = (x == x.p.right);
				
				//splice out x;
				//x.left.p = x.p;
				if(x.p.left == x) x.p.left = x.left;
				else x.p.right = x.left;

				Tsaux = x.right;
				setAsRoot(Tsaux);
				Tsaux = insertLeft(Tsaux, x);

				//if(Ts.root == nil) Ts.setRoot(Tsaux.root);
				//else Ts.join(Tsaux);		//because all keys in Ts are smaller than all those in Tsaux.
				T[1] = join(T[1], Tsaux);
			}
			x = y;
		}while(x != nil);

		return T;
	}

	/**
	 * This implements ITERATIVE-TREE-SEARCH(x,k) of CS511's book, pp.257
	 * It finds node z s.t. key[z]=k;
	 */
	final public static RBNode treeSearch(RBNode root, int k){
		RBNode x = root;
		while(x != nil && k != x.key){
			if(k < x.key) x = x.left;
			else x = x.right;
		}
		return x;
	}


	public String toString(){return toString(printContent);};
	public String toString(boolean printContent){
		return label() + "\n" +  toStringInternal("", printContent);
	}

///////////////////////////////////////////////////////
///////////  PROTECTED METHODS AND CLASSES //////////////
/////////////////////////////////////////////////////
	final static protected RBNode treeSuccessor(RBNode x){	//double checked
		if(x.right != nil){
			return RBNode.treeMinimum(x.right);
		}
		RBNode y = x.p;
		while(y != nil && x == y.right){
			x = y;
			y = y.p;
		}
		return y;
	}
	final static protected  RBNode treePredecessor(RBNode x){	//double checked
		if(x.left != nil){
			return RBNode.treeMaximum(x.left);
		}
		RBNode y = x.p;
		while(y != nil && x == y.left){
			x = y;
			y = y.p;
		}
		return y;
	}

	final static protected  RBNode treeMinimum(RBNode x){
		if(x == nil) return nil;
		RBNode xx = x;
		while(xx.left != nil) xx = xx.left;
		return xx;
	}
	final static protected  RBNode treeMaximum(RBNode x){
		if(x == nil) return nil;
		RBNode xx = x;
		while(xx.right != nil) xx = xx.right;
		return xx;
	}
	
	/*final static protected void changeRoot(RBNode old_root, RBNode new_root){
		new_root.p = nil;
		//new_root.red = false;
		new_root.bh = old_root.bh;
		
	}*/
	final static protected void setAsRoot(RBNode x){
		x.p = nil;
		x.red = false;
		x.bh = recomputeBH(x);
	}
	final protected static int recomputeBH(RBNode root){
		//recompute bh
		int bh = 0;
		RBNode y = root;
		while(y != nil){
			if(!y.red) bh++;
			y = y.right;
		}
		return bh;
	}

	/**
	 * Return true if node u is before in the in-order than v
	 * It works by comparing left-right paths from nodes to root.
	 */
	final static protected boolean smaller(RBNode u, RBNode v){
		if(u == nil || v == nil) return false;
		if(u == v) return false;
		
		//determine the eight of u and v
		RBNode aux_u = u;
		RBNode aux_v = v;
		int u_height, v_height;
		for(u_height=0; aux_u.p != nil; aux_u = aux_u.p, u_height++);
		for(v_height=0; aux_v.p != nil; aux_v = aux_v.p, v_height++);
		
		//if u and v have different roots they are incomparable and we return false
		if(aux_u != aux_v) return false;
		
		//we represent the paths from u and v to their roots by arrays
		//create arrays. false:left true:right
		boolean [] u_path = new boolean[u_height];
		boolean [] v_path = new boolean[v_height];
		
		//insert left and right moves
		int u_i = u_height-1;
		for(aux_u = u; aux_u.p != nil; aux_u = aux_u.p, u_i--){
			if(aux_u.p.left == aux_u) u_path[u_i] = false;
			else					  u_path[u_i] = true;
		}
		int v_i = v_height-1;
		for(aux_v = v; aux_v.p != nil; aux_v = aux_v.p, v_i--){
			if(aux_v.p.left == aux_v) v_path[v_i] = false;
			else					  v_path[v_i] = true;
		}
		
		//compare the paths
		//skip identical prefix
		int i;
		for(i=0; ((i < u_height) && (i<v_height)) && (u_path[i] == v_path[i]); i++ );
		
		//at least one path is not completely scanned because u!=v
		//but u's root = v's root.
		//at i they are different
		boolean result;
		if( (i<u_height) && (u_path[i] == false))  result = true;
		else if( (i<v_height) && (v_path[i] == true) ) result = true;
			else result = false;
		
		u_path = null;
		v_path = null;
		
		return result;
	}
	
///////////////////////////////////////////////////////
///////////  PRIVATE METHODS AND CLASSES //////////////
/////////////////////////////////////////////////////
	private static final RBNode insertInternal(RBNode root, RBNode z, RBNode y, boolean leftOrRight, boolean left){
		z.p = y;
		if(y == nil) {
			z.bh = root.bh;
			root = z;
		}
		else{
			if(!leftOrRight) left = z.key < y.key;  
			
			if(left) y.left = z;
			else y.right = z;
		}
		
		z.left = nil;
		z.right = nil;
		z.red = true;
		
		root = insertFixup(root, z);
		
		return root;
	}
		
	final static private RBNode insertFixup(RBNode root, RBNode z){
		RBNode y;
		while(z.p.red){
			if(z.p == z.p.p.left){
				y = z.p.p.right;
				if(y.red){
					z.p.red = false;
					y.red = false;
					z.p.p.red = true;
					z = z.p.p;
				}
				else{
					if(z == z.p.right){
						z = z.p;
						root = leftRotate(root, z);
						
					}
					z.p.red = false;
					z.p.p.red = true;
					root = rightRotate(root, z.p.p);
				}
			}
			else{	//same as "then" clause with "right"and "left" exchanged
				y = z.p.p.left;
				if(y.red){
					z.p.red = false;
					y.red = false;
					z.p.p.red = true;
					z = z.p.p;
				}
				else{
					if(z == z.p.left){
						z = z.p;
						root = rightRotate(root, z);
					}
					z.p.red = false;
					z.p.p.red = true;
					root = leftRotate(root, z.p.p);
				}
			}
		}
		if(root.red) root.bh++;
		
		root.red = false;
		
		return root;
	}
	final static private RBNode deleteFixup(RBNode root, RBNode x){
		RBNode w;
		while(x != root && !x.red){
			if(x == x.p.left){
				w = x.p.right;
				if(w.red) {
					w.red = false;
					x.p.red = true;
					root = leftRotate(root, x.p);
					w = x.p.right;
				}
				if(!w.left.red && !w.right.red){
					w.red = true;
					x = x.p;
				}
				else {
					if(!w.right.red){
						w.left.red = false;
						w.red = true;
						root = rightRotate(root, w);
						w = x.p.right;
					}
					w.red = x.p.red;
					x.p.red = false;
					w.right.red = false;
					root = leftRotate(root, x.p);
					break;
				}
			}
			else {//repeat with "right"and "left" exchanged.
				w = x.p.left;
				if(w.red) {
					w.red = false;
					x.p.red = true;
					root = rightRotate(root, x.p);
					w = x.p.left;
				}
				if(!w.right.red && !w.left.red){
					w.red = true;
					x = x.p;
				}
				else {
					if(!w.left.red){
						w.right.red = false;
						w.red = true;
						root = leftRotate(root, w);
						w = x.p.left;
					}
					w.red = x.p.red;
					x.p.red = false;
					w.left.red = false;
					root = rightRotate(root, x.p);
					break;
				}
			}
		}
		if(x == root && !x.red) root.bh--;	
		
		x.red = false;
		
		return root;
	}
	final  static private RBNode leftRotate(RBNode root, RBNode x){
		RBNode y = x.right;
		x.right = y.left;
		if(y.left != nil) y.left.p = x;
		y.p = x.p;
		if(x == root) {
			y.bh = root.bh;
			root = y;
		}
		else{
			if(x == x.p.left)  x.p.left = y;
			else x.p.right = y;
		}
		y.left = x;
		x.p = y;
		
		return root;
	}
	final static private RBNode rightRotate(RBNode root, RBNode y){
		RBNode x = y.left;
		y.left = x.right;
		if(x.right != nil) x.right.p = y;
		x.p = y.p;
		if(y.p == nil) {
			x.bh = root.bh;
			root = x;
		}
		else{
			if(y == y.p.left)  y.p.left = x;
			else y.p.right = x;
		}
		x.right = y;
		y.p = x;
		
		return root;
	}

	/**
	 * Checks if 5 properties of red black trees hold:
	 * 1. Every node is either red or black  
	 * 2. There root is black.
	 * 3. Every leaf (NIL) is black.
	 * 4. If a node is red, then both its children are black.
	 * 5. For each node, all paths from the node to descendent leaves 
	 * 	  contain the same number of black nodes.
	 * 
	 *  @return if properties failed return -1, otherwise it returns
	 * 			the black height (i.e., number of black nodes
	 * 			from root to (any) leaf).
	 */
	private int testRBProperties(RBNode root){
		// 1. Hold trivially since we have red to be a boolean, so only two states, red or not red (i.e. black)
		
		// 2.
		if(root.red) return -1;
		
		// 3., 4., 5.
		return testRBPropertiesInternal(root);
	}
	private int testRBPropertiesInternal(RBNode x){
		if(x == nil) {
			if(x.red) {
				return -1;	//3.
			}
			else return 1;
		}
		
		int addThis = 0;
		if(!x.red) addThis++;
		else { //4.
			if(x.left.red || x.right.red) {
				return -1;
			}
		}
		
		int bh = testRBPropertiesInternal(x.left);	//don't count the root
		if(testRBPropertiesInternal(x.right) == bh) return bh+addThis;
		
		else {
			return -1;
		}
	}
	
	/**
	 * Tests binary-search-property:
	 * Let x be a node in a binary search tree. If y is a node in the left 
	 * subtree of x, then key[y] <= key[x]. If y is a node in the right subtree
	 * of x, then key[x] <= key[y].
	 * 
	 * @return if property holds it returns the number of nodes in the tree,
	 * 		   otherwise it return -1. 
	 */
	private int testBinarySearchProperty(RBNode root, boolean justCount){
		return testBinarySearchPropertyInternal(root, justCount);
	}
	private int testBinarySearchPropertyInternal(RBNode x, boolean justCount){
		int numNodes = 1, aux;
		if(x.left != nil){
			aux = testBinarySearchPropertyInternal(x.left, justCount);
			if(aux < 0) return -1;
			else numNodes += aux;
		}
		if(x.right != nil){
			aux = testBinarySearchPropertyInternal(x.right, justCount); 
			if(aux < 0) return -1;
			else numNodes += aux;
		}
		return numNodes;
	}

	/**
	 * returns the output of the branch of x.
	 */
	private String toStringInternal(String offset, boolean printContent){
		if(this == nil) return offset;
		
		String outLeft = left.toStringInternal(offset + "     ", printContent);
		String outRight = right.toStringInternal(offset + "     ", printContent);
		
		String label = label();
		if(red && offset.length()>0) {
			offset = offset.substring(0, offset.length()-1);
		}
		
		String out="";
		out += outRight; 
		out += "\n" + offset + label;
		out += outLeft;
		
		return out;
	}
	private String label(){
		String label = ""+key;
		if(printContent && content != null) label += "("+ content.toString() +")";
		
		if(red) label  = "." + label + ".";
		return label;
	}
	
///////////////////////////////////////////////////////
///////////  MAIN //////////////
/////////////////////////////////////////////////////
	public static void main(String[] args) {
		Utils._ASSERT = true;
		
		createNil();
		EulerNode.debug = true;
		
		RBNode T = nil;
		RBNode x1=null, x2=null;
		for(int i=0; i<30; i++){
			RBNode aux = new RBNode(null, i);  
			T = insert(T, aux);
			if(i == 5) x1 = aux;
			if(i ==12) x2 = aux;
		}
		T.assertConsistency();
		
		/*T = delete(T, treeSearch(T, 12));
		T.assertConsistency(true);
		System.out.println(T + "\n---------------------------\n");

		T = delete(T, treeSearch(T, 17));
		T.assertConsistency(true);
		System.out.println(T + "\n---------------------------\n");

		T = delete(T, treeSearch(T, 5));
		T.assertConsistency(true);
		System.out.println(T + "\n---------------------------\n");

		T = delete(T, treeSearch(T, 3));
		T.assertConsistency(true);
		System.out.println(T + "\n---------------------------\n");

		T = delete(T, treeSearch(T, 4));
		T.assertConsistency(true);
		System.out.println(T + "\n---------------------------\n");
*/
		
		
		boolean ans = smaller(treeSearch(T, 7), treeSearch(T, 20));
		ans = smaller(treeSearch(T, 20), treeSearch(T, 20));
		ans = smaller(treeSearch(T, 21), treeSearch(T, 20));
		ans = smaller(treeSearch(T, 0), treeSearch(T, 20));
		ans = smaller(treeSearch(T, 25), treeSearch(T, 20));
		
		System.out.println(T + "\n---------------------------\n");
		T.assertConsistency();
		
		RBNode [] TT = RBNode.split(RBNode.treeSearch(T, 23), true);
		System.out.println(TT[0] + "\n---------------------------\n");
		System.out.println(TT[1] + "\n---------------------------\n");
		TT[0].assertConsistency();
		TT[1].assertConsistency();
		
		
		RBNode T2 = RBNode.nil;
		//T2.masterKey = tree.key(tree.treeMaximum(tree.root))+1;
		for(int i=10; i<29; i++){
			T2 = RBNode.insert(T2, new RBNode(null, i));
		}
		System.out.println(T2+ "\n---------------------------\n");
		
		T = RBNode.join(T, T2);
		T.assertConsistency();
		//tree.cut(x1, x2);
		System.out.println(T + "\n---------------------------\n");


		
	}

}
