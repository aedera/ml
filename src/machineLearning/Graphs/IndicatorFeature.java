package machineLearning.Graphs;

import java.util.Collection;

import machineLearning.Datasets.FastInstance;

public class IndicatorFeature extends Feature {
    private int[] indicatorIndex_;
    public double parameter_ = 1;
    public double[][][] assigmentsToValues;
    public double count;

    public IndicatorFeature() { super(); }
    public IndicatorFeature(Collection<Variable> variables) { super(variables); }
    public IndicatorFeature(Variable[] variables) { super(variables); }
    public IndicatorFeature(Assigment indicatorIndex) {
	this(indicatorIndex.getVariables());
	indicatorIndex_ = indicatorIndex.valuesToArray();
    }
    public IndicatorFeature(int[] scope, int[] assigment) {
	super(scope);
	indicatorIndex_ = assigment;
    }
    public IndicatorFeature(Assigment indicatorIndex, double parameter) {
	this(indicatorIndex);
	setValue(parameter);
    }

    public double value(FastInstance instance) {
	return value(instance.values);
    }

    public double value(int... indices) {
	for (int i = 0; i < scope_.length; i++)
	    if (indicatorIndex_[i] != indices[scope_[i]])
		return 0;

	return parameter_;
    }

    public void setValue(double value) { parameter_ = value; }
    public void setValue(final int[] indices, double value) {
	if (isIndicator(indices)) setValue(value);
    }
    public Factor clone() {
    	IndicatorFeature dummy = new IndicatorFeature(scope());
    	dummy.parameter_ = parameter_;

    	return dummy;
    }
    public boolean isZero() { return parameter_ == 0; }

    public double[] getParameters() {
	return new double[] { parameter_ };
    }

    public int numParameters() { return 1; }

    public void normalize() { setValue(1); }

    public void setParameters(double... parameters) { setValue(parameters[0]); }

    public Factor computeMarginalize(Variable variable) {
        throw new UnsupportedOperationException ();
    }
    public Factor multiply(Factor factor) {
        throw new UnsupportedOperationException ();
    }

    public int[] assigment() { return indicatorIndex_;}

    private boolean isIndicator(int... indices) {
	return java.util.Arrays.equals(indicatorIndex_, indices);
    }
}