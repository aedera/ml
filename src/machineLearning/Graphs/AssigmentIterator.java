package machineLearning.Graphs;

import java.util.Iterator;
import java.util.Collection;

import machineLearning.Utils.Matrix;
import machineLearning.Datasets.Schema;
import machineLearning.Datasets.Attribute;
import machineLearning.Datasets.FastInstance;

public class AssigmentIterator implements Iterator<int[]> {
    private int currentIndex;
    private int[] sizesDomainsOfVars;
    private int max;

    public AssigmentIterator(int nVars) {
	this(nVars, new int[nVars]);
    }

    public AssigmentIterator(int nVars, int[] assigment) {
        sizesDomainsOfVars = new int[assigment.length];
	java.util.Arrays.fill(sizesDomainsOfVars, 2);

        currentIndex = Matrix.indicesToIndex(sizesDomainsOfVars, assigment);

        max = Matrix.numValues(sizesDomainsOfVars);
    }

    public boolean hasNext() {
        return (currentIndex + 1 <= max) ? true : false;
    }

    public int[] next() {
        try {
	    return Matrix.indexToIndices(currentIndex++,
					 sizesDomainsOfVars);
        } catch(Exception e) { e.printStackTrace(System.err); return null; }
    }

    public void remove() throws java.lang.UnsupportedOperationException {
        throw new java.lang.UnsupportedOperationException();
    }
}