package machineLearning.Graphs;

import java.util.Collection;
import java.util.List;
import java.util.ArrayList;

import machineLearning.Utils.Factors;

public class FactorGraph implements Factor, List<Factor> {
    protected double constNorm_;
    protected List<Factor> factors_;
    private Factor constantNormalization;
    public int nVar;

    public FactorGraph() {
        factors_ = new ArrayList<Factor>();
        constantNormalization = new ConstantFactor(1);
    }

    public FactorGraph(Factor factor) {
        this();
        addFactor(factor);
    }

    public FactorGraph(Factor... factors) {
        this();
        addFactor(factors);
    }

    public FactorGraph(Collection<Factor> factors) {
        this();
        addAll(factors);
    }

    public FactorGraph(Variable[]... factorVariables) {
        this();
        for (Variable[] variables : factorVariables) addFactor(variables);
    }

    public double value(machineLearning.Datasets.FastInstance instance) {
	throw new UnsupportedOperationException ("Commig soon");
    }

    public double value(Assigment ass) {
        return constantNormalization.value(ass) * product(ass);
    }

    public double value(int... ass) {
	return unNormValue(ass) * constNorm_;
    }

    protected double unNormValue(int... ass) {
        double unNormMeasure = 1;

	for (Factor f : factors_) unNormMeasure *= f.value(ass);

	return unNormMeasure;
    }

    public double logValue(Assigment ass) {
        return constantNormalization.value(ass) * summation(ass);
    }

    public Factor multiply(Factor factor) throws IllegalArgumentException {
        FactorGraph clone = (FactorGraph)clone();
        clone.addFactor(factor);
        return clone;
    }

    public void addFactor(Factor factor) { factors_.add(factor); }
    public void addFactor(Factor... factors) {
        for (Factor factor : factors) addFactor(factor);
    }
    public void addFactor(Variable... factorVariables) {
        addFactor(new DiscreteFactor(factorVariables));
    }
    public List<Factor> getFactors() { return factors_; }

    public java.util.Iterator<Factor> iterator() { return factors_.iterator(); }

    public void add(int index, Factor element) { factors_.add(index, element); }
    public boolean add(Factor obj) { return factors_.add(obj); }
    public boolean addAll(Collection c) { return factors_.addAll(c); }
    public boolean addAll(int index, Collection c) {
        return factors_.addAll(index, c);
    }
    public void clear() { factors_.clear(); }
    public boolean contains(Object o) { return factors_.contains(o); }
    public boolean containsAll(Collection c) { return factors_.containsAll(c); }
    public boolean equals(Object o) { return factors_.equals(o); }
    public Factor get(int index) { return factors_.get(index); }
    public int hashCode() { return factors_.hashCode(); }
    public int indexOf(Object o) { return factors_.indexOf(o); }
    public boolean isEmpty() { return factors_.isEmpty(); }
    public int lastIndexOf(Object o) { return factors_.lastIndexOf(o); }
    public java.util.ListIterator listIterator() {
        return factors_.listIterator();
    }
    public java.util.ListIterator listIterator(int index) {
        return factors_.listIterator(index);
    }
    public Factor remove(int i) { return factors_.remove(i); }
    public boolean remove(Object o) { return factors_.remove(o); }
    public boolean removeAll(Collection c) { return factors_.removeAll(c); }
    public boolean retainAll(Collection c) { return factors_.retainAll(c); }
    public Factor set(int index, Factor element) {
        return factors_.set(index, element);
    }
    public int size() { return factors_.size(); }
    public List subList(int fromIndex, int toIndex) {
        return factors_.subList(fromIndex, toIndex);
    }
    public Object[] toArray() { return factors_.toArray(); }
    public Object[] toArray(Object[] o) { return factors_.toArray(o); }

    public Factor clone() {
        List<Factor> clone = new ArrayList<Factor>();
        java.util.Collections.copy(clone, factors_);
        return new FactorGraph(clone);
    }

    public void normalize() {
	double z = 0;

	for (AssigmentIterator it = new AssigmentIterator(nVar); it.hasNext();)
	    z += unNormValue(it.next());

	constNorm_ = 1/z;

    }

    public double summation(Assigment assigment) {
        double summation = 0.0;

        for(Factor factor : this) summation += factor.value(assigment);

        return summation;
    }

    public double product(Assigment assigment) {
        double product = 1.0;

        for(Factor factor : this) product *= factor.value(assigment);

        return product;
    }

    public Factor partitionFunction() { return constantNormalization; }

    public void setValue(Assigment assigment, double value) {
        throw new UnsupportedOperationException ("Commig soon");
    }

    // TODO: cache for scope...

    public Collection<Variable> getVariables() { return scope(); }

    public Collection<Variable> scope() {
        return new VarSet(factors_.toArray(new Factor[0]));
    }

    public boolean isAnyInScope(Collection<Variable> variables) {
        throw new UnsupportedOperationException ("Commig soon");
    }

    public boolean isAllInScope(Collection<Variable> variables) {
        throw new UnsupportedOperationException ("Commig soon");
    }

    public boolean isInScope(Variable variable) {
        throw new UnsupportedOperationException ("Commig soon");
    }

    public Factor marginalize(Variable variable) {
        throw new UnsupportedOperationException();
    }

    public Factor marginalize(Variable... variables) {
        throw new UnsupportedOperationException();
    }

    public double[] parametersToArray() {
        double[] parameters;
        double[] tmp;
        int offset = 0;

        parameters = new double[numParameters()];

        for (Factor factor : this) {
            tmp = ((Potencial)factor).getParameters();
            System.arraycopy(tmp, 0, parameters, offset, tmp.length);
            offset += tmp.length;
        }

        return parameters;
    }

    public int numParameters() {
        int num = 0;

        for (Factor factor : this) num += factor.numParameters();

        return num;
    }

    public void setParameters(double... parameters) {
        double[] array;
        int offset = 0;
        int length = 0;

        for (Factor factor : this) {
            length = ((Potencial)factor).getParameters().length;
            array = new double[length];
            System.arraycopy(parameters, offset, array, 0, length);
            ((Potencial)factor).setParameters(array);
            offset += length;
        }
    }

    public boolean isZero() {
	for (Factor f : factors_)
	    if (!f.isZero()) return false;

	return true;
    }
}