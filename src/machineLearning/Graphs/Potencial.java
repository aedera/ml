package machineLearning.Graphs;

import java.util.Collection;
import machineLearning.Datasets.FastInstance;

public abstract class Potencial extends AbstractFactor {
    protected Potencial() { super(); }
    protected Potencial(Collection<Variable> variables) { super(variables); }
    protected Potencial(Variable[] variables) { super(variables); }
    public Potencial(int[] scope) { super(scope); }

    public double value(int[] variables, int[] assigment) {
	int[] assig = new int[scope_.length];

	for (int i = 0; i < variables.length; i++)
	    for (int j = 0; j < scope_.length; j++)
		if (variables[i] == scope_[j])
		    assig[j] = assigment[i];

	return value(assig);
    }

    // FIXME see when assignationToIndex is empty must to return zero.
    public double value(Assigment assigment) {
	// index to query table of parameters
        int[] assignationToIndex = assigment.getValueOf(scope());

        return value(assignationToIndex);
    };
    public abstract double value(int... indices);
    public void setValue(Assigment assigment, double value) {
	int[] assignationToIndex = assigment.getValueOf(scope());

        setValue(assignationToIndex, value);
    }
    public abstract void setValue(int[] indices, double value);
    public abstract double[] getParameters();
    public abstract void setParameters(double... parameters);
}