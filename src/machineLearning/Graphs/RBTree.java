/*
 * Created on Mar 6, 2006
 *
 */
package machineLearning.Graphs;

import machineLearning.Utils.Utils;
import machineLearning.DataStructures.*;

/**
 * @author bromberg
 *
 * Implements a Red Black Tree which is a balanced binary search tree with a slight
 * modification necessary for efficient join of two RBTrees (see RBTree.join).
 * This modification sets the keys of all nodes to be its original key plus a constant
 * RBNode.treeKey;
 * This way, we can add a constant to all keys in the tree in constant time; 
 *  
 */
public class RBTree {
	int bh = 0;
	
	static RBNode nil = null;
	RBNode root;

	static public boolean printContent = true;
	public int masterKey = 0;
	
///////////////////////////////////////////////////////
///////////  CONSTRUCTOR/S //////////////
/////////////////////////////////////////////////////
	public RBTree(){
		//if(nil == null)	nil = new RBNode();
		
		if(nil == null){
			nil = new RBNode();
			nil.red = false;
			nil.key = -1;
			nil.left = null;
			nil.right = null;
			nil.p = null;
		}

		root = nil;
		
		
	}
	public RBTree(boolean dummy){
		root = nil;
	}


///////////////////////////////////////////////////////
///////////  INTERFACE //////////////
/////////////////////////////////////////////////////
	public RBNode insert(Object content, RBNode y, boolean right){
		RBNode z = new RBNode();
		z.content = content;
		z.key = -1;

		insert(z, y, right);
		
		return z;
	}
	public RBNode insert(Object content, int key){
		//creation of new node
		RBNode z = new RBNode();
		z.content = content;
		z.key = key;
		
		insert(z);
		
		return z;
	}
	public void insert(RBNode z){
		insert(z, null, true);
	}

	/**
	 *  If y is provided, it doesn't look for it.
	 *  Note that y will be the the parent of z, with z its 
	 *  left child if right = false, or right child ow.
	 */
	
	public void insert(RBNode z, RBNode y, boolean right){
	
		//Here we copy algorithm from book
		if(y == null){
			y = nil;
			RBNode x = root;
			while(x != nil){
				y = x;
				if(key(z) < key(x)) x = x.left;
				else x = x.right;
			}
		}
		z.p = y;
		if(y == nil) root = z;
		
		/*else if(key(z) < key(y)) y.left = z;
			 else y.right = z;
		*/
		if(y == null) right = (key(z) >= key(y));
		if(right) y.right = z;
		else y.left = z;
		
		z.left = nil;
		z.right = nil;
		z.red = true;
		
		insertFixup(z);
	}
	
	/**
	 * Deletes node z. If y is provided (!=null) is taken to 
	 * be the successor of z, otherwise we call treeSuccessor(z).
	 * This functionality is used when keys are inconsistent
	 * but ordering is known externally (e.g. Euler trees).
	 */
	public void delete(RBNode z, RBNode y){
		//RBNode z = treeSearch(k);
		if(z == nil) return;
		
		RBNode x;
		if(z.left == nil || z.right == nil)  y = z;
		else if(y == null) y = treeSuccessor(z);
		
		if(y.left != nil) x = y.left;
		else x = y.right;
		
		x.p = y.p;
		if (y.p == nil) root = x;
		else {
			if(y == y.p.left) y.p.left = x;
			else y.p.right = x;
		}
		if(y != z) {
			z.key = y.key;
			z.content = y.content;
		}
		if(!y.red) {	
			deleteFixup(x);
		}
	}
	
	public boolean separated(int key_x, int key_y)	{//equivalent to !connected
		RBNode x = treeSearch(key_x);
		RBNode y = treeSearch(key_y);
		
		return (key(getRoot(x)) != key(getRoot(y))); 
	}
	
	/**
	 * Used to get the root of the tree. Note this is a static
	 * method.
	 */
	static final public RBNode getRoot(RBNode z){
		RBNode x = z;
		while(x.p != nil) x = x.p;
		
		return x;
	}

	/**
	 * make node b root the root of the tree through a sequence of rotations
	public void makeRoot(RBNode b){
		RBNode x = b;
		while(x.p != nil){
			if(x == x.p.left){
				rightRotate(x.p);
			}
			else{
				leftRotate(x.p);
			}
			//x = x.p;
		}
	}
	 */

	
	/**
	 * It tests that the red black tree and binary search tree properties hold.
	 * Also, it checkes that tree is actually balanced by testing that
	 * bh<2lg(n+1) (where n is output of testBinarySearchProperty(),
	 * and bh is the black height and is the output of testRedBlackProperties(). 
	 */
	public boolean assertConsistency(boolean screenOutput){
		if(root == nil) return true;
		
		int bh = testRBProperties()-1;
		int n = testBinarySearchProperty(true);
		
		double aux = 2*(Math.log10(n+1) / Math.log10(2));
		
		if(screenOutput){
			System.out.println("n="+ n+ ", bh=" + bh + " <= 2*lg(n+1)=" + aux);
		}
		return Utils.ASSERT(bh == this.bh && bh >= 0 && n >= 0 && bh <= aux, "Red Black tree is inconsistent");
	}

	/**
	 * Implements the solution of problem 13.2 (RB-JOIN) of Introduction to Algorithms of Cormen.
	 * See also ass3.tex in \Research\Projects\ongoing\posterior.
	 * Given an input tree T2, it merges with this in O(lg n) time, resulting
	 * in a balanced tree.
	 * 
	 * WARNING: The outcome tree fulfills the binary search property
	 * only if all keys in T1 are smaller than all keys in T2. Used to cut and merge euler trees.
	 * 
	 * WARNING 2: it destroys the meaning of keys. The may become repeated if
	 * the same key was in left and right.
	 */
	public final RBTree join(RBTree T2){
		int h;
		RBNode y;
		RBTree T1 = this;
		
//Now do problem 13-2 which joins T1 and T2 with x as root.
		
		if(T1.bh >= T2.bh){
			//First find node with largest key and remove it from T1
			RBNode x = T1.root;
			while(x.right != nil){
				x = x.right;
			}
			T1.delete(x, null);

			if(T1.root == nil){
 				T2.insert(x, T2.treeMinimum(T2.root), false);
				T1.root = T2.root;
				T1.bh = T2.bh;
				return this;
			}
			//Find largest key in T1 whose bh equals bh[T2]
			y = T1.root;
			h = T1.bh;
			while(true){
				if(h == T2.bh) break;
				//y.right.p = y;	//for case in which y.right is nil
				y = y.right;
				if(!y.red) h = h-1;
			}
						
//Set x in the position of y and set its left child to be y and right child to be T2
			if(y == T1.root) T1.root = x;
			
//			edge p[y]-x
			x.p = y.p;
			y.p.right = x;
			
			//edge x-y
			y.p = x;
			x.left = y;
			
			//edge x-T2
			T2.root.p = x;
			x.right = T2.root;
			
			x.red = true;
			
			insertFixup(x);
		}
		else{
			//First find node with smallest key and remove it from T2
			RBNode x = T2.root;
			while(x.left != nil){
				x = x.left;
			}
			T2.delete(x, null);
			/*if(x == x.p.left) x.p.left = nil;
			else x.p.right = nil;
			nil.p = x.p;
			T2.deleteFixup(nil);
*/
			if(T2.root == nil){
 				T1.insert(x, T1.treeMaximum(T1.root), true);
 				return this;
			}

			//Find smallest key in T2 whose bh equals bh[T1]
			y = T2.root;
			h = T2.bh;
			while(true){
				if(h == T1.bh) break;
				//y.left.p = y;	//for case in which y.right is nil
				y = y.left;
				if(!y.red) h = h-1;
			}
			
//Set x in the position of y and set its right child to be y and left child to be T1
			if(y == T2.root) T2.root = x;
			
			//edge p[y]-x
			x.p = y.p;
			y.p.left = x;
				
			//edge x-y
			y.p = x;
			x.right = y;
			
			//edge x-T1
			T1.root.p = x;
			x.left = T1.root;
			
			x.red = true;
			
			T2.insertFixup(x);
			
				
			T1.root = T2.root;
			T1.bh = T2.bh;
		}
		return this;
	}
	/**
	 * Given a valid red-black tree and a node "a", this procedure
	 * returns two valid (i.e. balanced) red-black trees,
	 * one containing all nodes smaller than a, and the other containing
	 * all nodes larger than a. Node a is included in one or the other 
	 * depending on "inclusive".
	 * 
	 * The approach is similar to the procedure split of binary search trees:
	 * move a up to the root through left and right rotations (that maintain the 
	 * binary search property) and then return left and right sub-trees. 
	 * However, here we must maintain the left and right trees as valid red-black trees. 
	 * The algorithms has a run time complexity of O(log^2 n).
	 * 
	 * Note this tree is not a valid red-black tree at the output of the procedure.
	 */
	public final RBTree [] split(RBNode a, boolean inclusive){
		RBTree [] T = new RBTree[2];	
		RBTree Tp = T[0] = new RBTree();
		RBTree Ts = T[1] = new RBTree();
		
		RBTree Tpaux = new RBTree(), Tsaux = new RBTree();
		
		RBNode x = a, y, xp;

		if(inclusive) Ts.setRoot(x.right);
		else Tp.setRoot(x.left);
		do
		{
			if(inclusive){
				y = x.p;
				inclusive = (x == x.p.right);
				
				//splice out x;
				//x.right.p = x.p;
				if(x.p.left == x) x.p.left = x.right;
				else x.p.right = x.right;
				
				Tpaux.setRoot(x.left);
				Tpaux.insert(x, Tpaux.treeMaximum(Tpaux.root), true);	//since we know x is larger than all elements in its left subtree.
				
				Tpaux.join(Tp);		//because all keys in Tpaux are smaller than all those in Tp.
				Tp.setRoot(Tpaux.root);
				//Tp.join(Tpaux);
				x = y;
			}
			else{
				y = x.p;
				inclusive = (x == x.p.right);
				
				//splice out x;
				//x.left.p = x.p;
				if(x.p.left == x) x.p.left = x.left;
				else x.p.right = x.left;

				Tsaux.setRoot(x.right);
				Tsaux.insert(x, Tsaux.treeMinimum(Tsaux.root), false);	//since we know x is larger than all elements in its left subtree.

				//Ts.root.p = nil;
				if(Ts.root == nil) Ts.setRoot(Tsaux.root);
				else Ts.join(Tsaux);		//because all keys in Ts are smaller than all those in Tsaux.
				
			}
			x = y;
		}while(x != nil);

		return T;
	}
	
	public String toString(){return toString(printContent);};
	public String toString(boolean printContent){
		return toStringInternal(root, "", printContent);
	}

///////////////////////////////////////////////////////
///////////  PROTECTED METHODS AND CLASSES //////////////
/////////////////////////////////////////////////////
	protected class RBNode{
		protected Object content;
		protected boolean red; //if true color is red, otherwise its black.
		protected int key;
		protected RBNode left;
		protected RBNode right;
		protected RBNode p;	//parent
		
		RBNode(Object content){
			this.content = content;
		}
		RBNode(){
			content = null;
			red = true; //if true color is red, otherwise its black.
			key = -1;
			left = nil;
			right = nil;
			p = nil;	//parent
		}

		public String toString(){
			String label = ""+key;
			if(printContent && content != null) label += "("+ content.toString() +")";
			//else label = ;
			
			if(red) label = "."+label+".";
			return label;
		};
	}

	final protected RBNode treeSuccessor(RBNode x){	//double checked
		if(x.right != nil){
			return treeMinimum(x.right);
		}
		RBNode y = x.p;
		while(y != nil && x == y.right){
			x = y;
			y = y.p;
		}
		return y;
	}
	final protected  RBNode treePredecessor(RBNode x){	//double checked
		if(x.left != nil){
			return treeMaximum(x.left);
		}
		RBNode y = x.p;
		while(y != nil && x == y.left){
			x = y;
			y = y.p;
		}
		return y;
	}

	final protected  RBNode treeMinimum(RBNode x){
		if(x == nil) return nil;
		RBNode xx = x;
		while(xx.left != nil) xx = xx.left;
		return xx;
	}
	final protected  RBNode treeMaximum(RBNode x){
		if(x == nil) return nil;
		RBNode xx = x;
		while(xx.right != nil) xx = xx.right;
		return xx;
	}
	
	final protected void setRoot(RBNode x){
		root = x;
		root.p = nil;
		x.red = false;
		
		//recompute bh
		bh = 0;
		RBNode y = x;
		while(y != nil){
			if(!y.red) bh++;
			y = y.right;
		}
	}

	/**
	 * Return true if node u is before in the in-order than v
	 * It works by comparing left-right paths from nodes to root.
	 */
	final protected boolean smaller(RBNode u, RBNode v){
		if(u == nil || v == nil) return false;
		if(u == v) return false;
		
		//determine the eight of u and v
		RBNode aux_u = u;
		RBNode aux_v = v;
		int u_height, v_height;
		for(u_height=0; aux_u.p != nil; aux_u = aux_u.p, u_height++);
		for(v_height=0; aux_v.p != nil; aux_v = aux_v.p, v_height++);
		
		//if u and v have different roots they are incomparable and we return false
		if(aux_u != aux_v) return false;
		
		//we represent the paths from u and v to their roots by arrays
		//create arrays. false:left true:right
		boolean [] u_path = new boolean[u_height];
		boolean [] v_path = new boolean[v_height];
		
		//insert left and right moves
		int u_i = u_height-1;
		for(aux_u = u; aux_u.p != nil; aux_u = aux_u.p, u_i--){
			if(aux_u.p.left == aux_u) u_path[u_i] = false;
			else					  u_path[u_i] = true;
		}
		int v_i = v_height-1;
		for(aux_v = v; aux_v.p != nil; aux_v = aux_v.p, v_i--){
			if(aux_v.p.left == aux_v) v_path[v_i] = false;
			else					  v_path[v_i] = true;
		}
		
		//compare the paths
		//skip identical prefix
		int i;
		for(i=0; ((i < u_height) && (i<v_height)) && (u_path[i] == v_path[i]); i++ );
		
		//at least one path is not completely scanned because u!=v
		//but u's root = v's root.
		//at i they are different
		boolean result;
		if( (i<u_height) && (u_path[i] == false))  result = true;
		else if( (i<v_height) && (v_path[i] == true) ) result = true;
			else result = false;
		
		u_path = null;
		v_path = null;
		
		return result;
	}
	
///////////////////////////////////////////////////////
///////////  PRIVATE METHODS AND CLASSES //////////////
/////////////////////////////////////////////////////
	
	private int key(RBNode x){
		return x.key + masterKey;
	}
	/**
	 * This implements ITERATIVE-TREE-SEARCH(x,k) of CS511's book, pp.257
	 * It finds node z s.t. key[z]=k;
	 */
	final private RBNode treeSearch(int k){
		k += masterKey;
		RBNode x = root;
		while(x != nil && k != key(x)){
			if(k < key(x)) x = x.left;
			else x = x.right;
		}
		return x;
	}

	final private void insertFixup(RBNode z){
		RBNode y;
		while(z.p.red){
			if(z.p == z.p.p.left){
				y = z.p.p.right;
				if(y.red){
					z.p.red = false;
					y.red = false;
					z.p.p.red = true;
					z = z.p.p;
				}
				else{
					if(z == z.p.right){
						z = z.p;
						leftRotate(z);
					}
					z.p.red = false;
					z.p.p.red = true;
					rightRotate(z.p.p);
				}
			}
			else{	//same as "then" clause with "right"and "left" exchanged
				y = z.p.p.left;
				if(y.red){
					z.p.red = false;
					y.red = false;
					z.p.p.red = true;
					z = z.p.p;
				}
				else{
					if(z == z.p.left){
						z = z.p;
						rightRotate(z);
					}
					z.p.red = false;
					z.p.p.red = true;
					leftRotate(z.p.p);
				}
			}
		}
		if(root.red) bh++;
		
		root.red = false;
	}
	final private void deleteFixup(RBNode x){
		RBNode w;
		while(x != root && !x.red){
			if(x == x.p.left){
				w = x.p.right;
				if(w.red) {
					w.red = false;
					x.p.red = true;
					leftRotate(x.p);
					w = x.p.right;
				}
				if(!w.left.red && !w.right.red){
					w.red = true;
					x = x.p;
				}
				else {
					if(!w.right.red){
						w.left.red = false;
						w.red = true;
						rightRotate(w);
						w = x.p.right;
					}
					w.red = x.p.red;
					x.p.red = false;
					w.right.red = false;
					leftRotate(x.p);
					break;
				}
			}
			else {//repeat with "right"and "left" exchanged.
				w = x.p.left;
				if(w.red) {
					w.red = false;
					x.p.red = true;
					rightRotate(x.p);
					w = x.p.left;
				}
				if(!w.right.red && !w.left.red){
					w.red = true;
					x = x.p;
				}
				else {
					if(!w.left.red){
						w.right.red = false;
						w.red = true;
						leftRotate(w);
						w = x.p.left;
					}
					w.red = x.p.red;
					x.p.red = false;
					w.left.red = false;
					rightRotate(x.p);
					break;
				}
			}
		}
		if(x == root && !x.red) bh--;	
		
		x.red = false;
	}
	final private void leftRotate(RBNode x){
		RBNode y = x.right;
		x.right = y.left;
		if(y.left != nil) y.left.p = x;
		y.p = x.p;
		if(x == root) root = y;
		else{
			if(x == x.p.left)  x.p.left = y;
			else x.p.right = y;
		}
		y.left = x;
		x.p = y;
	}
	final private void rightRotate(RBNode y){
		RBNode x = y.left;
		y.left = x.right;
		if(x.right != nil) x.right.p = y;
		x.p = y.p;
		if(y.p == nil) root = x;
		else{
			if(y == y.p.left)  y.p.left = x;
			else y.p.right = x;
		}
		x.right = y;
		y.p = x;
	}

	/**
	 * Checks if 5 properties of red black trees hold:
	 * 1. Every node is either red or black  
	 * 2. There root is black.
	 * 3. Every leaf (NIL) is black.
	 * 4. If a node is red, then both its children are black.
	 * 5. For each node, all paths from the node to descendent leaves 
	 * 	  contain the same number of black nodes.
	 * 
	 *  @return if properties failed return -1, otherwise it returns
	 * 			the black height (i.e., number of black nodes
	 * 			from root to (any) leaf).
	 */
	private int testRBProperties(){
		// 1. Hold trivially since we have red to be a boolean, so only two states, red or not red (i.e. black)
		
		// 2.
		if(root.red) return -1;
		
		// 3., 4., 5.
		return testRBPropertiesInternal(root);
	}
	private int testRBPropertiesInternal(RBNode x){
		if(x == nil) {
			if(x.red) {
				return -1;	//3.
			}
			else return 1;
		}
		
		int addThis = 0;
		if(!x.red) addThis++;
		else { //4.
			if(x.left.red || x.right.red) {
				return -1;
			}
		}
		
		int bh = testRBPropertiesInternal(x.left);	//don't count the root
		if(testRBPropertiesInternal(x.right) == bh) return bh+addThis;
		
		else {
			return -1;
		}
	}
	
	/**
	 * Tests binary-search-property:
	 * Let x be a node in a binary search tree. If y is a node in the left 
	 * subtree of x, then key[y] <= key[x]. If y is a node in the right subtree
	 * of x, then key[x] <= key[y].
	 * 
	 * @return if property holds it returns the number of nodes in the tree,
	 * 		   otherwise it return -1. 
	 */
	private int testBinarySearchProperty(boolean justCount){
		return testBinarySearchPropertyInternal(root, justCount);
	}
	private int testBinarySearchPropertyInternal(RBNode x, boolean justCount){
		int numNodes = 1, aux;
		if(x.left != nil){
			aux = testBinarySearchPropertyInternal(x.left, justCount);
			if(aux < 0) return -1;
			else numNodes += aux;
		}
		if(x.right != nil){
			aux = testBinarySearchPropertyInternal(x.right, justCount); 
			if(aux < 0) return -1;
			else numNodes += aux;
		}
		return numNodes;
	}

	/**
	 * returns the output of the branch of x.
	 */
	private String toStringInternal(RBNode x, String offset, boolean printContent){
		if(x == nil) return offset;
		
		String outLeft = toStringInternal(x.left, offset + "     ", printContent);
		String outRight = toStringInternal(x.right, offset + "     ", printContent);
		
		String label = x.toString();
		if(x.red && offset.length()>0) {
//			coloring = ".";
			offset = offset.substring(0, offset.length()-1);
		}
		
		String out="";
		out += outRight; 
		out += "\n" + offset + label;
		out += outLeft;
		
		return out;
	}
	
	
///////////////////////////////////////////////////////
///////////  MAIN //////////////
/////////////////////////////////////////////////////
	public static void main(String[] args) {
		Utils._ASSERT = true;
		
		RBTree tree = new RBTree();
		tree.masterKey = 0;
		RBTree.RBNode x1=null, x2=null;
		for(int i=0; i<30; i++){
			RBTree.RBNode aux =	tree.insert(null, i);
			if(i == 5) x1 = aux;
			if(i ==12) x2 = aux;
		}
		boolean ans = tree.smaller(tree.treeSearch(4), tree.treeSearch(20));
		
		System.out.println(tree + "\n---------------------------\n");
		tree.assertConsistency(true);
		
		RBTree [] TT = tree.split(tree.treeSearch(23), true);
		System.out.println(TT[0] + "\n---------------------------\n");
		System.out.println(TT[1] + "\n---------------------------\n");
		TT[0].assertConsistency(true);
		TT[1].assertConsistency(true);
		
		
		RBTree T2 = new RBTree();
		//T2.masterKey = tree.key(tree.treeMaximum(tree.root))+1;
		for(int i=10; i<29; i++){
			T2.insert(null, i);
		}
		System.out.println(T2+ "\n---------------------------\n");
		
		tree.join(T2);
		tree.assertConsistency(true);
		//tree.cut(x1, x2);
		System.out.println(tree + "\n---------------------------\n");


		
		
		tree.insert(null, 1);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.insert(null, 5);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.insert(null, 13);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.insert(null, 4);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.insert(null, 27);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.insert(null, 3);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.insert(null, 17);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.assertConsistency(true);
		tree.insert(null, 7);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.insert(null, 0);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.insert(null, 12);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");

		int pred = (tree.treePredecessor(tree.treeSearch(7))).key;
		
		
		
		tree.delete(tree.treeSearch(12), null);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.delete(tree.treeSearch(17), null);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.delete(tree.treeSearch(5), null);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.delete(tree.treeSearch(3), null);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
		
		tree.delete(tree.treeSearch(4), null);
		tree.assertConsistency(true);
		System.out.println(tree + "\n---------------------------\n");
	}

}
