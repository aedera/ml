package machineLearning.Utils;
import machineLearning.Graphs.GraphRVar;
import machineLearning.Utils.*;
import machineLearning.independenceBased.Triplet;

/**
 * Class Fact
 * 
 * @author Facundo Bromberg
 */
public class FactSimpleGSIMN{
	/**
	 * sets type of fact: independence (I=true) or dependence (I=false)fact.
	 */
	private boolean I;	
	//private GraphRVar L, R;
	public int X,Y, TH;  //Left, Right, Third (for weak transitivity and chordality)
	public  FastSet cond;
	protected Set condNotFast;
	protected int order;
	public Triplet triplet;
	
	//private int N;
	/**
	 * Used for recovering the proof path.
	 */
	private FactSimpleGSIMN antecedentOne=null, antecedentTwo=null, antecedentThree=null;
	private String axiom = "DATA";
	static final String separator = "@"; 
	
	//Note, cond is refering to an externally created object. Hopefully nobody frees or changes it.
	public FactSimpleGSIMN(){};
	public FactSimpleGSIMN(FactSimpleGSIMN f){
		FactSimpleInternal(f.I(), f.X(), f.Y(), f.getCond(), f.getCondNotFast());
	}
	public FactSimpleGSIMN(int N, boolean Independent, int X, int Y, final Set cond){
		FastSet aux = convertToFastSet(cond, N);
		FactSimpleInternal(Independent, X, Y, aux, cond);
	}
	
	/**
	 * If V==null it leaves the condNonFast as null.
	 */
	public FactSimpleGSIMN(boolean Independent, int  X, int Y, final FastSet cond){
		FactSimpleInternal(Independent, X, Y, cond, null);
	}
	public FactSimpleGSIMN(boolean Independent, int  X, int Y, final FastSet cond, final Set V){
		//CONVERT TO NOT FAST
		if(V != null){
			condNotFast = new Set();
			int n= V.size();
			for(int i=0; i<n; i++){
				if(cond.isMember(i)) condNotFast.add(V.elementAt(i));
			}
		}
		FactSimpleInternal(Independent, X, Y, cond, null);
	}
	public FactSimpleGSIMN(boolean Independent, Triplet trip) {
		this.triplet = trip;
		FactSimpleInternal(Independent, trip.X, trip.Y, trip.FastZ, null);
		this.triplet = trip;
	}
	public void FactSimpleInternal(boolean Independent, int  X, int Y, final FastSet cond, final Set condNotFast){
		
		//this.N = N;
		I = Independent;
		this.X = X<Y?X:Y;
		this.Y = X<Y?Y:X;
		
		if(condNotFast != null) this.condNotFast = new Set(condNotFast);
		if(cond != null) this.cond  = cond;
	}

	
	public final Triplet  triplet(){return triplet;};
	public final int  X(){return X;};
	public final int  Y(){return Y;};
	public final void X(int x){X = x;};
	public final void Y(int y){Y = y;};
	public final FastSet getCond(){return cond;};
	public final Set getCondNotFast(){return condNotFast;};

	
	public void set(int N, boolean Independent, int  X, int  Y, final Set cond){
		FastSet aux = convertToFastSet(cond, N);
		set(Independent, X,Y, aux);
	}
	public void set(boolean Independent, int  X, int  Y, final FastSet cond){
		
		//this.N = N;
		I = Independent;
		this.X = X;
		this.Y = Y;
		this.cond = cond;
	}
	public boolean I(){return I;};
	public FactSimpleGSIMN I(boolean val){
		I = val;
		return this;
	}
	
	public boolean isSUEquivalentTo(FactSimpleGSIMN fact){
		//Assume X.index()  always smaller than Y.index()
		if(I && fact.I) return FastSet.isSubSet(cond, fact.cond);
		if( (I != fact.I) ) return false;  //r = true and r'= false or viceversa
		if(!I && !fact.I) return FastSet.isSubSet(fact.cond, cond);
		
		//shouldn't get here! the above exhaust all possibilities
		Utils.FORCED_ASSERT(false, "shouldn't get here! the above exhaust all possibilities");
		return true;
	}
	public int order(){return order;};
	public void order(int order){this.order = order;};
	
	public FactSimpleGSIMN antecedentOne(){return antecedentOne;};
	public FactSimpleGSIMN antecedentTwo(){return antecedentTwo;};
	public FactSimpleGSIMN antecedentThree(){return antecedentThree;};
	public FactSimpleGSIMN antecedents(FactSimpleGSIMN antecedent1, FactSimpleGSIMN antecedent2, FactSimpleGSIMN antecedent3){
		this.antecedentOne = antecedent1;
		this.antecedentTwo = antecedent2;
		this.antecedentThree = antecedent3;
		
		return this;
	}
	public String axiom(){return axiom;};
	public FactSimpleGSIMN axiom(String axiom){
		this.axiom = axiom;
		return this;
	}
	//Used to sort it. We put the variable with smaller name on the left. And then we sort the cond set similarly.
	//Note that it implements Insertion sort, which runs in linear time if input (i.e. cond) is already sorted.
	static Set sort(Set cond){
		GraphRVar key;
		int key2;
		int i;
		
		//Insertion sort
		for(int j=1; j<cond.size(); j++){
			key = (GraphRVar)cond.elementAt(j);
			i = j-1;
			while(i>=0 && (key2= ((GraphRVar)cond.elementAt(i)).index()) > (key.index())  ){
				cond.setElementAt(cond.elementAt(i), i+1);
				i = i-1;
			}
			//cond.setElementAt(key2, i+1);
			cond.setElementAt(key, i+1);
		}
		//Test if it works
		if(cond.size() > 3){
			int k=0;
		}
		for(int j=1; j<cond.size(); j++){
			GraphRVar key1 = (GraphRVar)cond.elementAt(j-1);
			GraphRVar key3 = (GraphRVar)cond.elementAt(j);
			if(key1.index() > key3.index()){
				System.out.println("ERROR: Sorting is not working.");
			}
		}
		return cond;
	};
	
	public FactSimpleGSIMN symmetric(){
		return new FactSimpleGSIMN(I, Y,X, cond, null);
	}
	public static FastSet convertToFastSet(Set S, int numVarsInV){
		FastSet cond = new FastSet(numVarsInV);
		for(int i=0; i<S.size(); i++){
			cond.add((Integer)S.elementAt(i)); 
		}
		return cond;
	}
	
	static public Set convertTotSet(FastSet S, Set V){
		Set cond = new Set();
		for(int i=0; i<V.size(); i++){
			if(S.isMember(i)) cond.addElement(V.elementAt(i));
		}
		return cond;
	}
	
	public String proof(){
		return "\n"+proofInt("                                                   ")+"";
	}
	public String proofInt(String tab){
		String proof="";
		if(this.antecedentOne != null){
			String tabint = tab;
			if(tabint.length() > 10) tabint = tabint.substring(0, tabint.length()-10);
			proof += antecedentOne.proofInt(tabint) + "\n";
			if(antecedentTwo != null) proof += antecedentTwo.proofInt(tabint) + "\n";
			if(antecedentThree != null) proof += antecedentThree.proofInt(tabint) + "\n";
		}
		proof += tab+"("+ axiom +") => " + this.toString();
		return proof;
	}
	
	public String toString(){
		int i1 = X, j1 = Y;
		String ret = I?"":"!"; 
		String ret2;
		if(cond != null) ret2 =  ret+ "I(" + X + "," + Y +  "|" + cond + ")";
		else ret2 =  ret+ "I(" + X +"," + Y +  "|" + condNotFast + ")";
		
		return ret2;
	}
}
