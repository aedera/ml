/*
 * Created on Mar 4, 2005
 *
 */
package machineLearning.Utils;

//import java.util.BitSet;
import java.util.Vector;
import java.util.*;

/**
 * @author Facundo Bromberg
 *
 */
public class powerSet implements Iterator{
	private boolean allowHasnext=false;
	Set set;
	int [] indexes;
	//BitSet bs, bsones;
	int n, N;
	
	public powerSet(Set _set, int _n){
		set = _set;
		N = set.size();
		n = _n;
		indexes = new int[n];
		initIterator();
	}
	public void initIterator(){
		for(int i=0; i<n;i++){
			indexes[i]=i;
			if(i==n-1)indexes[i]--;
		}
		
	}
	public Set current(){
		Set S = new Set(new Vector(0,n));
		for(int i=0; i<n ;i++){
			S.add(set.elementAt(indexes[i]));
		}
		return S;
	}
	
	boolean aux=true; //used for special case of n=0;
	public boolean hasNext(){
		if(!allowHasnext){
			System.out.println("\n\nERROR: hasNext shouldn't be used for powerSet. See this.test() for an example of usage.");
			Exception e = new Exception();
			e.printStackTrace();
	
			return false;
		}
		if(n==0 && aux) {
			aux=false;
			return true;
		}
		else if(n==0 && !aux) return false;
		
		for(int i=0; i<n;i++) {
			int k = N-1-(n-i-1);
			if(indexes[i] < N-1-(n-i-1)) {
				return true; 
			}
		}
		return false;
	}
	public Object next(){
		Set curr = null;
		allowHasnext=true;
		if(hasNext()){  
			for(int i=n-1;i>=0;i--){
				if(i==n-1){
					if(indexes[i] < N-1){ 
						indexes[i]++;
						break;
					}
				}
				else if(indexes[i] < indexes[i+1]-1){
					indexes[i]++;
					for(int j=i+1, k=0; j<n;j++, k++){
						indexes[j]=indexes[i]+k+1;
					}
					break;
				}
			}
			curr = current();
		}
		allowHasnext=false;
		return curr;
	}
	public void remove(){;};
	static public void test(){
		int k=5;
		Set S = new Set(4);
		S.add(new Integer(5));
		S.add(new Integer(3));
		S.add(new Integer(7));
		S.add(new Integer(1));
		powerSet ps = new powerSet(S, 2);
		
		ps.initIterator();
		while(ps.next() != null){
			Set Ss = new Set(ps.current());
			System.out.println(Ss);
		}
	}
	public static void main(String[] args) {
		
	}
}
