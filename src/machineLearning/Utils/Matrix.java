package machineLearning.Utils;

/**
 * Matrix represent a valued-real multidimensional matrix implemented through
 * of a unidimensional array.
 */
public class Matrix implements Cloneable {
    private int[] sizesDimensions_;
    private double[] values_;

    public Matrix(int... sizes) {
        setSizesOfDimensions(sizes);
    }

    public Matrix(int[] sizes, double... values) {
        this(sizes);
        fill(values);
    }

    public void fill(double... values) {
        System.arraycopy(values, 0, values_, 0, values.length);
    }

    public void set(double value, int... indices) {
        set(value, indicesToIndex(indices));
    }

    public void set(double value, int index) { values_[index] = value; }

    public double get(int... indices) { return get(indicesToIndex(indices)); }

    public double get(int index) { return values_[index]; }

    public int size() { return values_.length; }
    public int[] dimensions() { return sizesDimensions_; }

    public int indicesToIndex(int... indices) {
        return indicesToIndex(sizesDimensions_, indices);
    }

    public static int indicesToIndex(int[] sizes, int... indices) {
        int idx = 0;

        for (int dim = 0; dim < indices.length; dim++)
            idx = (idx * sizes[dim]) + indices[dim];

        return idx;
    }

    public int[] indexToIndices(int index) {
        return indexToIndices(index, sizesDimensions_);
    }

    public static int[] indexToIndices(int index, int... sizes) {
        int nDimensions = totalDimensions(sizes);
        int[] indices = new int[sizes.length];

        for (int i = 0; i < sizes.length; i++) {
            nDimensions /= sizes[i];
            indices[i] = index / nDimensions;
            index = index % nDimensions;
        }

        return indices;
    }

    public void fillRandomDouble() { fillRandomDouble(this); }

    public static void fillRandomDouble(Matrix matrix) {
        java.util.Random random = new java.util.Random();

        for (int i = 0; i < matrix.size(); i++)
            matrix.values_[i] = random.nextDouble();
    }

    private static int totalDimensions(int... sizes) {
        Integer total = 1;
        for (int size : sizes) {
        	total *= size;
        }
        return total;
    }

    public static int numValues(int... sizes) { return totalDimensions(sizes); }

    private void setSizesOfDimensions(int... sizes) {
        int length = totalDimensions(sizes);
        values_ = new double[length];
        sizesDimensions_ = sizes;
    }

    public Matrix clone() {
        return new Matrix(sizesDimensions_, values_.clone());
    }

    public void multiplyBy(double... values) {
        assert (values.length == values_.length);
        int i = 0;
        for (double value : values) values_[i++] = value;
    }

    public boolean equals(Object o) {
        if (o instanceof Matrix)
            return java.util.Arrays.equals(values_, ((Matrix)o).values_);

        return false;
    }

    public double[] toArray() { return values_; }

    public void normalize() {
        double total = 0.0;

        for (double value : values_ ) total += value;

        for (int i = 0; i < values_.length; i++) values_[i] /= total;
    }

    public boolean isZero() {
	for (double v : values_)
	    if (v != 0) return false;

	return true;
    }
}