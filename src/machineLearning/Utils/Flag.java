package machineLearning.Utils;

import java.util.ArrayList;

public class Flag extends Parameter<Boolean>{
	//-----------------------------------------------------
	public Flag(String name, String description){
		super(name, description, false);
	}
	
//	-----------------------------------------------------
	public void setValues(ArrayList<String> args) throws IllegalAccessException{
		if(args.size() > 0) throw new IllegalAccessException("");
		
		args.add("true");
		super.setValues(args);
	}
	//-----------------------------------------------------
	public boolean getBooleanValue() throws IllegalAccessException{
		if(!type.equalsIgnoreCase("Boolean")) throw new IllegalAccessException("");
		return ((Boolean) values.get(0)).booleanValue();
	}
//	-----------------------------------------------------
	public String toString(){
		return name;
	}

}
