/*
 * Created on Mar 5, 2005
 *
 * Implements a set using a BitSet
 */
package machineLearning.Utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Random;
import java.util.Vector;

/**
 * @author Facundo Bromberg
 *
 */
public final class FastSet {
    int iteratorIndex = 0, iteratorFalseIndex = 0;
    public java.util.BitSet bitset;
    public int capacity;

    // int hashcode, i, k;
    // FastSet A;

    // private final BitSet bitset(){return bitset;};

    public FastSet(FastSet V) {
        capacity = V.capacity;
        // bitset = (BitSet) V.bitset().clone();
        bitset = new BitSet(V.capacity);
        bitset.or(V.bitset);
    }

    public FastSet(int capacity) {
        this.capacity = capacity;
        bitset = new BitSet(capacity);
    }

    public FastSet(int capacity, int... initialMembers) {
        this.capacity = capacity;
        bitset = new BitSet(capacity);
        for (int m : initialMembers) {
            add(m);
        }
    }

    // Special For Sets of GraphVars
    public FastSet(Set S, int numVarsInV) {
        this.capacity = numVarsInV;
        bitset = new BitSet(capacity);

        for (int i = 0; i < S.size(); i++) {
            add((Integer) S.elementAt(i));
        }
    }

    public FastSet(java.util.BitSet bitset, int capacity) {
        this.capacity = capacity;
        this.bitset = bitset;
    }

    public FastSet(boolean[] array) {
        this.capacity = array.length;
        this.bitset = new BitSet(capacity);
        for (int i = 0; i < array.length; i++) {
            if (array[i])
                bitset.flip(i);
        }
    }

    public FastSet(int[] array) {
        this.capacity = array.length;
        this.bitset = new BitSet(capacity);

        for (int i = 0; i < array.length; i++)
            if (array[i] == 1) bitset.flip(i);
    }

    final public void setAlltoOne() {
        bitset.set(0, capacity);
    }


    /**
     * <code>setAlltoOneExcept</code> set all bits in one, except the bits
     * indicates by <code>index</code>. <code>index</code> is a array of
     * integer that indicate the position of bits to set it to zero.
     *
     */
    public final void setAlltoOneExcept(int... index) {
        setAlltoOne();
        for (int i = 0; i < index.length; i++)
            this.flip(index[i]);
    }

    final public int cardinality() {
        return bitset.cardinality();
    }

    /*
     * public int capacity(){ return capacity; }
     *//*
        * public int length(){ return bitset.length(); }
        */
    final public FastSet remove(int i) {
        bitset.clear(i);
        return this;
    };

    public FastSet flip(int i) {
        bitset.flip(i);
        return this;
    }

    final public boolean[] toBooleanArray() {
        boolean[] ret = new boolean[capacity];
        int first = first();

        // if fastset empty return ret empty
        if (first == -1) return ret;

        ret[first] = true;
        int index;
        while ((index = next()) >= 0)
            ret[index] = true;

        return ret;
    }
    
    final public String toStringArray() {
    	return Arrays.toString(toBooleanArray()).replace("true", "1").replace("false", "0")
        .replaceAll("[^01]", "");
    }

    final public int first() {
        return iteratorIndex = bitset.nextSetBit(0);
    }

    final public int next() {
        return iteratorIndex = bitset.nextSetBit(iteratorIndex + 1);
    }

    /**
     * Returns the index of the first index that is set to false. It is
     * persistent (in memory), i.e., it stores this value and works in
     * conjunction with nextFalse().
     */
    final public int firstFalse() {
        return iteratorFalseIndex = bitset.nextClearBit(0);
    }

    /**
     * Gets the index of the next bit that is False.
     */
    final public int nextFalse() {
        iteratorFalseIndex = bitset.nextClearBit(iteratorFalseIndex + 1);
        return iteratorFalseIndex > capacity ? -1 : iteratorFalseIndex;
    }

    public boolean isEmpty() {
        return bitset.isEmpty();
    }

    // public boolean contains(Object O){return S.contains(O);}
    public final FastSet add(int i) {
        bitset.set(i);
        return this;
    }

    final public boolean isMember(int i) {
        return bitset.get(i);
    }

    /**
     * return the position of the i-th bit that is on
     *
     */
    public int getOn(int i) {
        int k = 0;
        for (int j = 0; j <= i; j++) {
            k = bitset.nextSetBit(k);
            k++;
        }
        return k - 1;
    }

    static public FastSet emptySet(int setsize) {
        FastSet S = new FastSet(setsize);
        S.bitset.clear();

        return S;
    }

    public void clear() {
        bitset.clear();
    }

    static public final FastSet Union(final FastSet A, final FastSet B) {
        FastSet C = new FastSet(A);
        C.bitset.or(B.bitset);
        return C;
    }

    public final void Union(final FastSet B) {
        // FastSet C = new FastSet(A);
        this.bitset.or(B.bitset);
    }

    static final public FastSet Intersection(final FastSet A, final FastSet B) {
        FastSet C = new FastSet(A);
        C.bitset.and(B.bitset);
        return C;
    }

    public final void Intersection(final FastSet B) {
        this.bitset.and(B.bitset);
    }

    public final void Xor(final FastSet Flipper) {
        this.bitset.xor(Flipper.bitset);
    }


    public static final FastSet complement(FastSet f) {
        f.bitset.flip(0, f.capacity);
        return f;
    }

    public final FastSet complement() {
        this.bitset.flip(0, capacity);
        return this;
    }

    static final public FastSet Minus(final FastSet A, final FastSet B) {
        FastSet C = new FastSet(B);
        C.bitset.flip(0, C.capacity);
        C = Intersection(C, A);
        return C;
    }

    /*
     * public void Minus(final FastSet B){ FastSet C = new FastSet(B);
     * C.bitset().flip(0,C.capacity()); this.Intersection(B); }
     *//**
        * @param V
        *            universe of variables (in some cases this DOES NOT include the
        *            variables of the fact, e.g. x, y) DEPRECATED, USE ONE BELOW
        *            INSTEAD
        * @return
        */
    public Vector getAllSupersets(FastSet V) {
        int newSize = (int) Math.pow(2, V.cardinality() - 1);
        Vector ret = new Vector(newSize);
        V = FastSet.Minus(V, this);

        Vector SS = V.getAllSubsets(V);
        for (int i = 0; i < SS.size(); i++) {
            ret.add(Union(this, (FastSet) SS.elementAt(i)));
        }
        // ret.add(Union(this,V));
        return ret;
    }

    public ArrayList<FastSet> getAllSupersets() {
        int newSize = (int) Math.pow(2, capacity - 1);
        ArrayList<FastSet> ret = new ArrayList<FastSet>(newSize);
        FastSet V = new FastSet(capacity);
        V.setAlltoOne();
        V = FastSet.Minus(V, this);

        ArrayList<FastSet> SS = V.getAllSubsets();
        for (FastSet SS_current : SS) {
            ret.add(Union(this, SS_current));
        }
        // ret.add(Union(this,V));
        ret.remove(this);
        return ret;
    }

    /**
     * DEPRECATED, USE ONE BELOW INSTEAD.
     */
    public Vector getAllSubsets(FastSet V) {
        int newSize = (int) Math.pow(2, cardinality() - 1);
        Vector ret = new Vector(newSize);

        FastSet S = null;
        for (int n = 0; n < cardinality(); n++) {
            FastPowerSet ps = new FastPowerSet(this, n, V.capacity);
            ps.initIterator();
            int i = 0;
            while (ps.next() != null) {
                S = new FastSet(ps.current());
                ret.add(S);
                i++;
            }
        }
        ret.add(this);
        return ret;
    }

    /**
     * DEPRECATED, Use subsets and subsetsEq below. get all proper substs, i.e.,
     * this is not included
     *
     * @return
     */
    final public ArrayList<FastSet> getAllSubsets() {
        return subsets();
    }

    /**
     * DEPRECATED, USE subsets() Gets all subsets of size m
     */
    final public ArrayList<FastSet> getAllSubsets(int m) {
        return subsets(m);
    }

    /**
     * get all substs, i.e., this IS included
     *
     * @return
     */
    final public ArrayList<FastSet> subsetsEq() {
        ArrayList<FastSet> ret = subsets();
        ret.add(this);
        return ret;
    }

    /**
     * get all proper substs, i.e., this is not included
     *
     * @return
     */
    final public ArrayList<FastSet> subsets() {
        FastPowerSet ps;
        int i, n;
        int card = cardinality();
        ArrayList<FastSet> ret = new ArrayList<FastSet>((int) Math.pow(2, card - 1));

        FastSet S = null;
        for (n = 0; n < card; n++) {
            ret.addAll(subsets(n));
        }
        // ret.add(this);
        return ret;
    }

    // --------------------------------------------------
    final public ArrayList<FastSet> subsetsEq(int m) {
        if (m == -1)
            return subsetsEq();

        return subsets(m);
    }

    // --------------------------------------------------
    /**
     * Gets all subsets of size m
     */
    final public ArrayList<FastSet> subsets(int m) {
        if (m == -1)
            return subsets();

        FastPowerSet ps;
        int i;
        int card = cardinality();
        int num = (int) Utils.logChoose(card, m);
        ArrayList<FastSet> ret = new ArrayList<FastSet>(num);

        FastSet S = null;
        ps = new FastPowerSet(this, m, capacity);
        ps.initIterator();
        i = 0;
        while (ps.next() != null) {
            S = new FastSet(ps.current());
            ret.add(S);
            i++;
            //                  if(i==num){
            //                          break;
            //                  }
        }
        return ret;
    }

    /**
     * Gets all subsets of size m
     */
    final public ArrayList<FastSet> subsets(int m, int max) {
        if (m == -1)
            return subsets();

        FastPowerSet ps;
        int i;
        int card = cardinality();
        int num = (int) Utils.logChoose(card, m);
        ArrayList<FastSet> ret;
        if (num > 0 && num < max) {
            ret = new ArrayList<FastSet>(num);
        } else {
            ret = new ArrayList<FastSet>(max);
        }

        FastSet S = null;
        ps = new FastPowerSet(this, m, capacity);
        ps.initIterator();
        i = 0;
        while (ps.next() != null && i < max) {
            S = new FastSet(ps.current());
            ret.add(S);
            i++;
        }
        return ret;
    }

    /**
     * returns A & ~B = ALL ZEROS
     */
    static public boolean isSubSet(FastSet A, FastSet B) {
        FastSet C = new FastSet(A);
        C.bitset.andNot(B.bitset);

        return C.bitset.isEmpty();
    }

    /**
     * returns A XOR B = ALL ZEROS
     */
    static public boolean isEqual(FastSet A, FastSet B) {
        FastSet C = new FastSet(A);
        C.bitset.xor(B.bitset);
        return C.bitset.isEmpty();
    }

    public boolean isEqual(FastSet A) {
        FastSet C = new FastSet(A);
        C.bitset.xor(bitset);
        return C.bitset.isEmpty();
    }

    final public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if ((object == null) || (object.getClass() != this.getClass())) {
            return false;
        }

        return bitset.equals(((FastSet) object).bitset);
    }

    final public int hashCode() {
        return bitset.hashCode();
        /*
         * hashcode = 0; k=bitset.nextSetBit(0); while(k >= 0){ hashcode += k;
         * k=bitset.nextSetBit(k+1); } //for(i=0; i<n ;i++) hashcode +=
         * S.bitset.get(i)?i:0; //hashCode = new Integer(hashcode); return
         * hashcode;
         */}

    public String toString() {
        return toStringBuffer().toString();

    }

    public StringBuffer toStringBuffer() {
        StringBuffer ret = new StringBuffer("{");
        for (int i = 0; i < bitset.size(); i++) {
            if (bitset.get(i)) {
                ret.append(i);
                ret.append(",");
            }
        }
        if (ret.length() > 1) {
            ret = ret.deleteCharAt(ret.length() - 1);
        }
        ret.append("}");
        return ret;

    }

    public int toInt(boolean isMSB) { return toInt(this, isMSB); }

    public static int toInt(boolean[] bits, boolean isMSB) {
        int outcome = 0;
        boolean[] clone = bits.clone();
        boolean tmp = false;

        if (isMSB == false)
            for (int i = 0; i < clone.length / 2; i++) {
                tmp = clone[i];
                clone[i] = clone[clone.length - i - 1];
                clone[clone.length - i - 1] = tmp;
            }

        for (int i = 0; i < clone.length; i++)
            if (clone[i] == true)
                outcome += Math.pow(2.0, i);

        return outcome;

    }

    public static int toInt(FastSet bits, boolean isMSB) {
        return toInt(bits.toBooleanArray(), isMSB);
    }

    public static FastSet getRandomFastSet(int capacity) {
        FastSet randomAssignment = new FastSet(capacity);
        Random random = new Random();
        int trueAssignments = random.nextInt(capacity);
        for (int i = 0; i < trueAssignments; i++) {
            randomAssignment.flip(random.nextInt(capacity));
        }
        return randomAssignment;
    }

    public static void main(String[] args) {
        FastSet fs = new FastSet(9);
        fs.setAlltoOne();

        System.out.println("Testing all subsets of set " + fs);
        ArrayList<FastSet> subsets = fs.subsets(2);
        for (FastSet subset : subsets)
            System.out.println(subset);

    }
}
