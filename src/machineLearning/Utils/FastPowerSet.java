/*
 * Created on Mar 4, 2005
 *
 */
package machineLearning.Utils;

//import java.util.BitSet;
import java.util.*;

/**
 * @author Facundo Bromberg
 *
 */
public class FastPowerSet implements Iterator{
	private boolean allowHasnext=false;
	FastSet set;
	int [] indexes;
	//BitSet bs, bsones;
	final int n, N;
	int numOfVarsInV;
	
	public FastPowerSet(FastSet _set, int _n, int numOfVarsInV){
		set = new FastSet(_set);
		this.numOfVarsInV = numOfVarsInV;
		this.N = _set.cardinality();
		n = _n;
		indexes = new int[n];
		initIterator();
	}
	public void initIterator(){
		for(int i=0; i<n;i++){
			indexes[i]=i;
			if(i==n-1)indexes[i]--;
		}
		
	}
	public FastSet current(){
		FastSet S = new FastSet(numOfVarsInV);
		int k;
		for(int i=0; i<n ;i++){
			S.add(set.getOn(indexes[i]));
		}
		return S;
	}
	
	boolean aux=true; //used for special case of n=0;
	public boolean hasNext(){
		if(!allowHasnext){
			System.out.println("\n\nERROR: hasNext shouldn't be used for powerSet. See this.test() for an example of usage.");
			Exception e = new Exception();
			e.printStackTrace();
	
			return false;
		}
		if(n==0 && aux) {
			aux=false;
			return true;
		}
		else if(n==0 && !aux) return false;
		
		for(int i=0; i<n;i++) {
			int k = N-1-(n-i-1);
			if(indexes[i] < N-1-(n-i-1)) {
				return true; 
			}
		}
		return false;
	}
	public Object next(){
		Object curr = null;
		allowHasnext=true;
		if(hasNext()){  
			for(int i=n-1;i>=0;i--){
				if(i==n-1){
					if(indexes[i] < N-1){ 
						indexes[i]++;
						break;
					}
				}
				else if(indexes[i] < indexes[i+1]-1){
					indexes[i]++;
					for(int j=i+1, k=0; j<n;j++, k++){
						indexes[j]=indexes[i]+k+1;
					}
					break;
				}
			}
			curr = current();
		}
		allowHasnext=false;
		return curr;
	}
	public void remove(){;};
	static public void test(){
		int k=5;
		Set S = new Set(4);
		S.add(new Integer(5));
		S.add(new Integer(3));
		S.add(new Integer(7));
		S.add(new Integer(1));
		powerSet ps = new powerSet(S, 2);
		
		ps.initIterator();
		while(ps.next() != null){
			Set Ss = new Set(ps.current());
			System.out.println(Ss);
		}
	}
	
	public static void main(String[] args) {
		//Prints all subsets of the universe {0,1,2,3,...,domainSize}
		
		int domainSize = 4;
		FastSet S = new FastSet(domainSize);
		S.setAlltoOne();
		
		int i=0;
		for(int size = 0; size<=domainSize; size++){
			FastPowerSet ps = new FastPowerSet(S, size, domainSize);
			
			ps.initIterator();
			while(ps.next() != null){
				System.out.println(i + ": " + ps.current());
				i++;
			}
		}
	}
}
