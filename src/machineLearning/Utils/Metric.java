package machineLearning.Utils;

import static java.lang.Math.*;

import java.util.List;

import machineLearning.Graphs.AssigmentIterator;
import machineLearning.Graphs.Factor;

public class Metric {
    public static double
	kLDivergence(int nVars, final Factor p, final Factor q) {
	double summation = 0.0;
	double p_value = 0.0, q_value = 0.0;
	int[] assigment;

	for (AssigmentIterator it = new AssigmentIterator(nVars); it.hasNext();) {
	    assigment = it.next();
	    p_value = p.value(assigment);
	    q_value = q.value(assigment);
	    //System.out.println("p: " + p_value + " q: " + q_value);
	    // 0 log 0 -> 0
	    if (p_value == 0 && q_value == 0) continue;

	    summation += p_value * log(p_value / q_value);
	}

	return summation;
    }
}