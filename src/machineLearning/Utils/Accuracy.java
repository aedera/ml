package machineLearning.Utils;

//import FactSimpleGSIMN;
//import GSMN;
//import KBsimpleGSIMN;

import java.util.Random;
import java.util.Vector;

import machineLearning.Datasets.FastDataset;
import machineLearning.Datasets.attributesSet;
import machineLearning.Datasets.Statistics;
import machineLearning.Graphs.GraphNode;
import machineLearning.Graphs.GraphNodeI;
import machineLearning.Graphs.GraphRVar;
import machineLearning.Graphs.MarkovNet;

/*
 * Created on Sep 20, 2006
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author bromberg
 *
 */
public class Accuracy {
    static final boolean screenOutput = false;
    
    static final short ROC_TPR = 0;   //true positive rate = #(true positives)/#(positives)
    static final short ROC_FPR = 1;     //false positive rate = #(false positives)/#(negatives)
    static final short ROC_POS = 2;       //class ratio = #(negatives)/#(positives)
    static final short ROC_NEG = 3;       //total number of counts
    //static String ROC_OUTPUT;

    //int maxCondSize = 8;
    //int numTests = 800;
    protected int testType;
    protected double testThreshold;
    protected Random r;
    protected MarkovNet outNet;
    
    double ROC [][] = new double [4][];
    protected FactSimpleGSIMN [][] kbs;
    int size[];
    int n;
    
    public Accuracy(Random r, int testType, double testThreshold){
        this.r = r;
        this.testType = testType;
        this.testThreshold = testThreshold;
        
        outNet = null;
    }

    public Accuracy(int n, Random r, int testType, double testThreshold){
        this.r = r;
        this.testType = testType;
        this.testThreshold = testThreshold;
        this.n = n;
        size = new int[n-2];
        kbs = new FactSimpleGSIMN[n-2][];
        for(int i=0; i<n-2;i++) kbs[i] = new FactSimpleGSIMN[10];
    }

    public Accuracy(MarkovNet outNet, int maxCondSize, int numTests, Random r, int testType, double testThreshold){
        this.r = r;
        this.testType = testType;
        this.testThreshold = testThreshold;
        this.outNet = outNet;
        
        kbs = sampleRandomTests(new Set(outNet.V()), maxCondSize, numTests);
    }

    public Accuracy(MarkovNet outNet, FactSimpleGSIMN [][] kbs, Random r, int testType, double testThreshold){
        this.r = r;
        this.testType = testType;
        this.testThreshold = testThreshold;
        this.outNet = outNet;
        
        this.kbs = kbs;
    }

    public Accuracy(MarkovNet outNet, Accuracy acc){
        this.r = acc.r;
        this.testType = acc.testType;
        this.testThreshold = acc.testThreshold;
        this.kbs = acc.kbs;

        this.outNet = outNet;
    }

    public void addTest(FactSimpleGSIMN f){
    	if(f == null) return;
    	int card = f.cond.cardinality();
    	if(card >= n-2) return;
    	
    	kbs[card][size[card]] = f;
    	          
    	//next will overflow
    	if(size[card]+1 == kbs[card].length){
    		FactSimpleGSIMN aux []= new FactSimpleGSIMN[kbs[card].length * 2];
    		for(int i=0; i<kbs[card].length; i++){
    			aux[i] = kbs[card][i]; 
    		}
    		kbs[card] = null;
    		kbs[card] = aux;
    	}
    	size[card]++;
    }
    /**
     * It performs ALL statistical tests from data that can be done for conditioning sets of cardinality lower (or equal) than maxCardCondSet. 
     * <!-- --> Each of them is compared with the test done on the output network (this.G). <!-- --> If origNet is 
     * provided, it compares the two networks (G and orig) on the same tests. It returns the ratio of
     * comparisons that match.
     * @return
     */
    public double accuracy(FastDataset data){
        evaluate(null, data);
        return accuracy(this.ROC);
    }
    public double accuracy(MarkovNet trueNet){
        evaluate(trueNet, null);
        return accuracy(this.ROC);
    }
    public double accuracy(MarkovNet trueNet, FastDataset data, FactSimpleGSIMN [][] kbs){
    	this.kbs = kbs;
        evaluate(trueNet, data);
        return accuracy(this.ROC);
    }
    public double accuracy(){
        return accuracy(this.ROC);
    }
    //DEPRECATED. Now call evaluate and then confusionMatrix().
/*    public double [] confusionMatrix(MarkovNet trueNet){
        evaluate(trueNet, null);
        return confusionMatrix();
    }
    public double [] confusionMatrix(MarkovNet trueNet, FastDataset data){
        evaluate(trueNet, data);
        return confusionMatrix();
    }*/

    
    // outputmatrix = [TI TD FP FN]
    public double [] confusionMatrix(){
        double [] matrix= new double[4];
        int TP=0, N=0, TN=0, POS=0, NEG=0;
        
        for(int k=0;k<ROC[0].length;k++){
            N += ROC[ROC_POS][k]+ROC[ROC_NEG][k];
            POS += ROC[ROC_POS][k];
            NEG += ROC[ROC_NEG][k];
            TP +=  ROC[ROC_TPR][k] * ROC[ROC_POS][k];
            TN +=  ROC[ROC_NEG][k] * (1.0-ROC[ROC_FPR][k]) ;
         }
        matrix[0] = TP;
        matrix[1] = TN;
        matrix[2] = POS-TP;	//FP
        matrix[3] = NEG-TN; //FN
    
   
        return matrix;
    }

    public double N(){
    	double N=0;
	   for(int k =0; k<ROC[0].length; k++){
           N += ROC[ROC_POS][k]+ROC[ROC_NEG][k];
       }
       return N;
    }
    private static double accuracy(double [][] ROC){
        //Accuracy = (TI + TD)/N;
        //tpr = TI/NUM_IND;
        //fpr = FP/NUM_DEP;
        //TI = \sum_k tpr_k*POS_k 
        //TD = \sum_k (NEG_k - FP_k) = \sum_k (NEG_k - fpr_k*NEG_k) =   
        double TP =0, TN =0, N =0;
        for(int k =0; k<ROC[0].length; k++){
            N += ROC[ROC_POS][k]+ROC[ROC_NEG][k];
            TP +=  ROC[ROC_TPR][k] * ROC[ROC_POS][k];
            TN +=  ROC[ROC_NEG][k] * (1.0-ROC[ROC_FPR][k]) ;
        }
        return (TP+TN)/N;
    }

    public FactSimpleGSIMN [][] sampleRandomTests(Set V, int maxCondSize, int numTests){
        FastSet cond=null;
        Set Vxy;
        GraphNode X=null, Y=null;
        //KBsimpleGSIMN  kb = new KBsimpleGSIMN(V.size());
        FactSimpleGSIMN facts [][] = new FactSimpleGSIMN[maxCondSize+1][];
        
        int N = V.size();
        //if(maxCondSetSize > N-2) return null;
        
        int ix=0, iy=0;
      
        for(int k=0; k<maxCondSize+1; k++){
            if(k > N-2) break;
            
            facts[k] = new FactSimpleGSIMN[numTests/(maxCondSize+1)];
            int count=0;
            int maxnumtests = (int) (Math.exp((int)Utils.logChoose((int)N-2, k)) *  N*(N-1)/2.0);
            while(count <numTests/(maxCondSize+1) && count < maxnumtests){
                Vxy = null;
                Vxy = new Set(V);
    
                //Select X
                ix = r.nextInt(V.size());
                X = null;
                X = (GraphRVar) V.elementAt(ix);
                Vxy.remove(X);
    
                //Select Y
                iy = ix;
                Y = null;
                while(iy == ix){
                    iy = r.nextInt(V.size());
                }
                Y = (GraphRVar) V.elementAt(iy);
                Vxy.remove(Y);
                
                //Select condSet
                Set VxyCopy = new Set(Vxy);
                cond = null;
                cond = new FastSet(V.size());
                for(int j=0; j<k; j++){
                    int condVar = r.nextInt(VxyCopy.size());
                    //as.add(  ((GraphRVar)(VxyCopy.elementAt(condVar))).attr);
                    cond.add(((GraphRVar)VxyCopy.elementAt(condVar)).index());
                    VxyCopy.remove(condVar);
                }
                if(Utils._ASSERT) Utils.ASSERT(cond.cardinality() == k, "GSIMN.Accuracy: invalid cond size.");
                
                facts[k][count] = new FactSimpleGSIMN(true, X.index(), Y.index(), cond, V);
                //System.out.println(fact);
                
                count++;
            }
        }
        return facts;
    }

    /**
     * Given the true network as input, for each node X add a test for each neighbor Y given all other neighbors N_X except of Y, i.e.
     * X,Y | {N_X - Y}
     */
    public FactSimpleGSIMN [][] sampleNeighborsTests(MarkovNet trueNet){
        FastSet cond=null;
        Set V = new Set(trueNet.V());
        Set N_X; //Neighbors of X
        GraphNode X=null, Y=null;
        //KBsimpleGSIMN  kb = new KBsimpleGSIMN(V.size());
        FactSimpleGSIMN facts [][] = new FactSimpleGSIMN[1][];
        
        int N = V.size();

		//Experiment.output.println(pfmnVL.SAMPLE_NEIGHBORS_TESTS| pfmnVL.HIGHER, trueNet.toString());
        Experiment.output.println(pfmnVL.SAMPLE_NEIGHBORS_TESTS | pfmnVL.HIGHER, trueNet.toString());

        int numTests = 0;
        for(int x=0; x<N; x++){
        	numTests +=  ((GraphRVar)V.elementAt(x)).degree();
        }
    	facts[0] = new FactSimpleGSIMN[numTests];

    	int count=0;
        for(int x=0; x<N; x++){
        	X = null;
        	X = (GraphRVar) V.elementAt(x);

        	N_X = null;
        	N_X = new Set();
        	N_X.addAll(X.adjacencyList());
        	
        	for(int y=0; y<N_X.size(); y++){
        		Y = null;
        		Y = (GraphRVar) N_X.elementAt(y);

        		//Select condSet
        		Set N_XMinusY = new Set(N_X);
        		cond = null;
        		cond = new FastSet(V.size());
        		for(int j=0; j<N_XMinusY.size(); j++){
        			if(j == y) continue;
        			cond.add(((GraphRVar)N_XMinusY.elementAt(j)).index());
        		}
        		facts[0][count] = new FactSimpleGSIMN(true, X.index(), Y.index(), cond, V);
        		//System.out.println(fact);
        		Experiment.output.println(pfmnVL.SAMPLE_NEIGHBORS_TESTS | pfmnVL.HIGHER, facts[0][count].toString());
        		count++;
        	}
        }
        return facts;
    }

    public boolean evaluate(FastDataset data){
        return evaluate(null, data);
    }
    public boolean evaluate(MarkovNet trueNet){
        return evaluate(trueNet, null);
    }
    public boolean  evaluate(MarkovNet trueNet, FastDataset data){
        //MarkovNet origNet
       
        attributesSet as = null;
        Vector subsets;
        boolean a=true,b=true;
        Set cond, Vxy;
        int X,Y;
        
        int TOT_POS=0, TOT_NEG=0;
        int TOT_TP=0, TOT_FN=0;
        
        int maxK=0;
        for(int k = 0; k<kbs.length; k++) {
            if(kbs[k] == null) break;
            maxK++; 
        }
            
        for(int i=0; i<4; i++) ROC[i] = new double[maxK];
    
        //numOfTests = 0;
        
        
        Experiment.output.println(pfmnVL.ACCURACY_EVAL, "\n\nACCURACY ");
        Experiment.output.println(pfmnVL.ACCURACY_EVAL, "-------");
        //grows a KB with tests to be performed
        for(int k = 0; k<maxK; k++){
            Experiment.output.println(pfmnVL.ACCURACY_EVAL, "\n Cardinality = " + k);
            
            double FN = 0, POS = 0, NEG = 0, TP = 0;
            int i=0;
            while(i < kbs[k].length && kbs[k][i] != null ){
                X = kbs[k][i].X();
                Y = kbs[k][i].Y();
                cond = kbs[k][i].getCondNotFast();
                
                
                boolean testDone = false;
                try{
                    if(data != null && trueNet != null){
                        //To test quality of sampled data. I will compare tests done on this data with the true network, instead of the
                        //one generated from the data.
                        boolean [] testFailed = new boolean[1];
                        a = Statistics.independent(data, X, Y, cond, testThreshold, true, testType, testFailed);
                              
                        if(!testFailed[0]) {//that is, last test did not fail, then
//                            b=trueNet.separates(X,Y,(Vector)cond);
                            testDone=true;
                            //if(a==b) if(GSMN.ScreenOutput)System.out.print("  CORRECT");
                            //else if(GSMN.ScreenOutput) System.out.print("  INCORRECT");
                            //if(GSMN.ScreenOutput) System.out.print("\n");
                        }
                    }
                    //Compares finalNet with the tests done from the data
                    else if(data != null){
                        boolean [] testFailed = new boolean[1];
                        a = Statistics.independent(data,  X, Y, cond, testThreshold, true, testType, testFailed);
                        if(!testFailed[0]) {//that is, last test did not fail, then
                            testDone=true;
//                            b=outNet.separates(X,Y,(Vector)cond);
                        }
                    }
                    //Compares finalNet with the tests done from the original network (meaningful only for artificial Data).
                    else if(trueNet != null){
                        testDone=true;
//                        b=trueNet.separates(X,Y,(Vector)cond);
//                        a=outNet.separates(X,Y,(Vector)cond);
                    }
                }
                catch(Exception e){
                    System.out.print(e);
                    e.printStackTrace();
                }
                
                if(testDone){
                	Experiment.output.print(pfmnVL.ACCURACY_EVAL, "\n" + X + "," + Y + " | " + cond + "  " + b + "  " + a + "  ");
                    if(a){
                        POS++;
                        TOT_POS++;
                        if(b)  {
                        	TP++;  //TI
                        	TOT_TP++;
                        	Experiment.output.print(pfmnVL.ACCURACY_EVAL, "TI " + TOT_TP);
                        }
                        else Experiment.output.print(pfmnVL.ACCURACY_EVAL, "FP " + (TOT_POS-TOT_TP));
                    }
                    else{
                        NEG++;
                        TOT_NEG++;
                        if(b) {
                        	FN++; //FN
                        	TOT_FN++;
                        	Experiment.output.print(pfmnVL.ACCURACY_EVAL, "FN " + TOT_FN);
                        }
                        else Experiment.output.print(pfmnVL.ACCURACY_EVAL, "TD " + (TOT_NEG-TOT_FN));
                    }
                    Experiment.output.print(pfmnVL.ACCURACY_EVAL, "  Accuracy = " + "(" + TOT_TP + "+" + (TOT_NEG-TOT_FN) + ")/" + (TOT_POS+TOT_NEG) + "= " + ((double)(TOT_TP+TOT_NEG-TOT_FN)/(double)(TOT_POS+TOT_NEG)));
                }
                i++;

            }
            ROC[ROC_TPR][k] = POS == 0? 0.0: TP / POS;
            ROC[ROC_FPR][k] = NEG == 0? 0.0:FN / NEG;
            ROC[ROC_POS][k] = POS;
            ROC[ROC_NEG][k] = NEG;

            Experiment.output.println(pfmnVL.ACCURACY_EVAL, "\nCardinality   " + k + " Accuracy ="  + ((double)(TP+NEG-FN)/(double)(POS+NEG)));

            //TOT_POS += NUM_IND;
            //TOT_TP += TI;
        }
        //System.out.println(i + ":("+ ix + ","+ iy + ") |" + cond + " accuracy=" + accuracy/numOfTests);
        //if(i%100 == 0) System.out.print(".");
            
        if(data != null) {
            if(screenOutput) System.out.println(outNet);
            if(screenOutput) System.out.println(trueNet);
        }
        
        return true;//accuracy/numOfTests;
    }


	public static void main(String[] args) {
	}
}
