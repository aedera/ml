package machineLearning.Utils;

public class VerboseLevels {
	public static final int LOG = 1;
	public static final int UP = 2, HIGHER = 2, SUPER = 2, INCLUSIVE = 2;
	public static final int RESULT = 4, RESULTS = 4;
	public static final int SIMPLE = 8;
//	public static final int SIMPLE = 8;
//	public static final int MAINLOOP = 16;
//	public static final int VERBOSE = 32;
}
