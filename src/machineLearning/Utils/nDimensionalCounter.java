package machineLearning.Utils;

public class nDimensionalCounter {
	private int ndim;
	private int [] counter;
	private int [] numStates;
	boolean starting;
	
	public nDimensionalCounter(int ndim, int ... numStates){
		this.ndim = ndim;
		this.numStates = numStates;
		
		counter = new int[ndim];
		starting = true;
	}
	//----------------------------------------------------
	public nDimensionalCounter(int ndim, int numStatesPerDim){
		this.ndim = ndim;
		this.numStates = new int[ndim];
		
		for(int i=0; i<ndim; i++) numStates[i] = numStatesPerDim;
		
		counter = new int[ndim];
		starting = true;
	}
	//----------------------------------------------------
	public void reset(){
		for(int i=0; i<ndim; i++){
			counter[i] = 0;
		}
		starting = true;
	}

	//----------------------------------------------------
	public int [] current(){
		return counter;
	}
	
	//----------------------------------------------------
	public boolean hasNext(){
		int dim=0;
		do{
			if(dim==ndim) return false;
			dim++;
		}while(!starting && counter[dim-1]%numStates[dim-1] == 0);
		return true;
	}	
	//----------------------------------------------------
	public int [] next(){
		int dim=0;
		do{
			if(dim==ndim) return null;
			counter[dim]++;
			counter[dim] = counter[dim]%numStates[dim];
			dim++;
		}while(counter[dim-1] == 0);
		
		starting = false;
		return counter;
	}
	//----------------------------------------------------
	public String toString(){
		String out="["+counter[0];
		for(int i=1; i<ndim; i++) out += " " + counter[i];
		out += "]";
		
		return out;
	}
//	----------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		nDimensionalCounter nc = new nDimensionalCounter(2, 2, 2);
		
		while(nc.hasNext()){
			System.out.println(""+nc);
			nc.next();
		}

		nc.reset();
		while(nc.hasNext()){
			System.out.println(""+nc);
			nc.next();
		}
	}

}
