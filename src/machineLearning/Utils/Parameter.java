package machineLearning.Utils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Scanner;

public class Parameter<T> {
        private T dummy;
        String name;
        String description;

        protected ArrayList<T> values;
        protected String type = null;
        boolean definesNumRuns = false;

        /**
         * Initially true for all parameters. It is set to false once the setValue method has been called.
         */
        boolean _default = true;

        //-----------------------------------------------------
        public Parameter(String name, String description, T ... values){
                //super(name, description);
                this.name = name;
                this.description = description;

                this.values = new ArrayList<T>();

                if(values != null) for(T v : values) this.values.add(v);


                /**get type */
                if(values != null && values.length > 0) type = values[0].getClass().getSimpleName();
        }
        //-----------------------------------------------------
        public Parameter(String name, T ... values){
                //super(name, description);
                this.name = name;
                this.description = "";
                this.values = new ArrayList<T>();

                for(T v : values) this.values.add(v);

                /**get type */
                if(values.length > 0) type = values[0].getClass().getSimpleName();
        }


//      --------------------------------------------------------------------
          //Separates a space separated input into an Strings[]
        static public String []  processMultiOptions(String option){
                  if(option == null || option.length() == 0) {
                          String rett [] = new String [1];
                          rett[0] = "";
                          return rett;
                  }

                  java.util.ArrayList<String> ret = new java.util.ArrayList<String>();

                  //Strip quotes
                  if(option.charAt(0) == '\"'){
                          option = option.substring(1,option.length());
                  }
                  if(option.charAt(option.length()-1) == '\"'){
                          option = option.substring(0,option.length()-1);
                  }
                  Scanner sc = new Scanner(option);
                  while(sc.hasNext()){
                          ret.add(sc.next());
                  }
                  String rett [] = new String[ret.size()];
                  int i=0;
                  for(String r : ret) rett[i++] = r;

                  return rett;
          }
//      -----------------------------------------------------
        public int hashCode(){return name.hashCode();};

//      -----------------------------------------------------
        public String description(){
                return "-"+name+"     "+description;
        }
//      -----------------------------------------------------
        public String toString(){
                String out = name;
                if(values.size() > 0 ) out += " = ";
                for(T t : values){
                        out += "" + t.toString();
                }

                return out;
        }

//      -----------------------------------------------------
        public void setValues(ArrayList<String> args)throws IllegalAccessException {
                values.clear();
                int i=0;
                for(String s : args){
                        values.add(i++, convertToTyped(s));
                        //setValue(convertToTyped(s), i++);
                }
        }
        //-----------------------------------------------------
        public T getValue(int index)throws IllegalAccessException {
                return values.get(index);
        }
        //-----------------------------------------------------
        public T getValue(){
                return values.get(0);
        }
        //      -----------------------------------------------------
        protected T convertToTyped(String valueStr){
                if(type.equalsIgnoreCase("Boolean")) return (T) new Boolean(valueStr);
                else if(type.equalsIgnoreCase("Integer")) return (T) new Integer(valueStr);
                else if(type.equalsIgnoreCase("Double")) return (T) new Double(valueStr);
                else if(type.equalsIgnoreCase("String")) return(T) valueStr;

                return null;
        }
        //-----------------------------------------------------
        public boolean getBooleanValue(int index) throws IllegalAccessException{
                if(!type.equalsIgnoreCase("Boolean")) throw new IllegalAccessException("");
                return (Boolean) values.get(index);
        }
        //-----------------------------------------------------
        public int getIntValue(int index) throws IllegalAccessException{
                if(!type.equalsIgnoreCase("Integer")) throw new IllegalAccessException("");
                return (Integer) values.get(index);
        }
        //-----------------------------------------------------
        public double getDoubleValue(int index) throws IllegalAccessException{
                if(!type.equalsIgnoreCase("Double")) throw new IllegalAccessException("");
                return ((Double) values.get(index));
        }
        //-----------------------------------------------------
        public String getStringValue(int index) throws IllegalAccessException{
                if(!type.equalsIgnoreCase("String")) throw new IllegalAccessException("");
                return (String) values.get(index);
        }
//      -----------------------------------------------------
        public String getValueAsString(int index) throws IllegalAccessException{
                return (String) (""+values.get(index));
        }
//      -----------------------------------------------------
        public boolean getBooleanValue() throws IllegalAccessException{
                if(!type.equalsIgnoreCase("Boolean")) throw new IllegalAccessException("");
                return ((Boolean) values.get(0)).booleanValue();
        }
        //-----------------------------------------------------
        public int getIntValue() throws IllegalAccessException{
                if(!type.equalsIgnoreCase("Integer") && !type.equalsIgnoreCase("Double") ) throw new IllegalAccessException("");
                if(type.equalsIgnoreCase("Double")) return (int) ((Double) values.get(0)).doubleValue();
                return (Integer) values.get(0);
        }
        //-----------------------------------------------------
        public void setIntValue(T val) throws IllegalAccessException{
                if(!type.equalsIgnoreCase("Integer") && !type.equalsIgnoreCase("Double") ) throw new IllegalAccessException("");
                values.set(0, val);
        }
        //-----------------------------------------------------
        public double getDoubleValue() throws IllegalAccessException{
                if(!type.equalsIgnoreCase("Integer") && !type.equalsIgnoreCase("Double") ) throw new IllegalAccessException("");
                if(type.equalsIgnoreCase("Integer")) return (double) ((Integer) values.get(0)).intValue();
                return (Double) values.get(0);
        }
        //-----------------------------------------------------
        public String getStringValue() throws IllegalAccessException{
                if(!type.equalsIgnoreCase("String")) throw new IllegalAccessException("");
                return (String) values.get(0);
        }
//      -----------------------------------------------------
        public String getValueAsString() throws IllegalAccessException{
                return (String) (""+values.get(0));
        }
        //-----------------------------------------------------

        //-----------------------------------------------------
        public int numValues(){return values.size();};
        //-----------------------------------------------------
}
