/*
 * Created on Apr 19, 2005 by Facundo Bromberg
 *
package machineLearning.Utils;

/**
 * @author Facundo Bromberg
 *
 */
package machineLearning.Utils;

public interface Comparator {
	boolean smaller(Object a, Object b);
	boolean equal(Object a, Object b);
}
