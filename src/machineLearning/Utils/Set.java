/*
 * Created on Mar 5, 2005
 *
 */
package machineLearning.Utils;

import java.util.Vector;
import java.util.*;

/**
 * @author Facundo Bromberg
 */
public class Set extends Vector implements Iterator{
    int iteratorIndex = 0;
    boolean left [];
    Set aux;
    Random r = new Random(1234346435);

    public Set(){
        ;
    }
    public Set(Vector V){
        super(0, V.size());
        for(int i=0; i<V.size(); i++){
            if(!contains(V.elementAt(i))) {
                super.addElement(V.elementAt(i));
            }
        }
    }
    public Set(Set V){
        super(0,V.size());
        for(int i=0; i<V.size(); i++){
            if(!contains(V.elementAt(i))) super.addElement(V.elementAt(i));
        }
    }
    public Set(int initCapacity){
        super(0, initCapacity);
    }
    public int cardinality(){
        return size();
    }
    public void initIterator(){
        iteratorIndex = -1;
        left = new boolean[size()];
        aux = new Set(this);
    }
    public Object current(){
        return elementAt(iteratorIndex);
    }
    public boolean hasNext(){
        return iteratorIndex+1<size();
    }
    public Object nextRandom(){
        if(aux.size() == 0) return null;
        int index=(int)(r.nextDouble()*(double)aux.size());
        Object current = aux.elementAt(index);
        aux.remove(index);

        return current;
    }
    public Object next(){
        Object curr = null;
        if(hasNext())  {
            iteratorIndex++;
            curr = current();
        }
        return curr;
    }
    public void remove(){
        removeElementAt(iteratorIndex);
    };
    //public boolean contains(Object O){return S.contains(O);}
    public boolean add(Object O)
    {
        if (!contains(O))
            {
                super.addElement(O);
            }
        return true;
    }
    public void addElement(Object O){
        System.out.println("\n\nERROR: Attempt to use addElement ("+ O +") in Set " +this+"\n\n");
        Exception e = new Exception();
        e.printStackTrace();
    };
    public boolean addSorted(Object O, Comparator C)
    {
        for(int i=0; i<size(); i++){
            Object curr = elementAt(i);
            if(C.smaller(curr, O))continue;
            else{
                if(C.equal(curr, O)){
                    System.out.println("\nWARNING: tried to add an existing element " + O + " into SET " + this + "\n");
                }
                this.add(i, O);
                break;
            }
        }
        if(size()==0) this.add(0, O);
        return true;
    }

    public void UnionDesctructive (Set B){
        B.initIterator();
        while(B.next() != null){
            add(B.current());
        }
    }

    static public Set UnionSorted(final Set A, final Set B, Comparator Comp){return Union(A, B, Comp);};
    static public Set Union(final Set A, final Set B){return Union(A, B, null);};
    public static Set union(final Set A, final Set B){ return Union(A, B, null); }
    static public Set Union(final Set A, final Set B, Comparator Comp){
        Set C = new Set(A);
        B.initIterator();
        while(B.next() != null){
            if(Comp == null)C.add(B.current());
            else C.addSorted(B.current(), Comp);
        }
        return C;
    }
    public void minusDestructive(Set B){
        B.initIterator();
        while(B.next() != null){
            if(contains(B.current())) remove(B.current());
        }
    }
    static public Set minus(final Set A, final Set B){
        Set C = new Set(A);
        B.initIterator();
        while(B.next() != null){
            if(C.contains(B.current())) C.remove(B.current());
        }
        return C;
    }

    static public Set Intersection(final Set A, final Set B){
        return Intersection(A,B, null);
    }

    public static Set intersection(final Set a, final Set b) {
        return Intersection(a, b, null);
    }

    static public Set Intersection(final Set A, final Set B, Comparator Comp){
        Set C = new Set();
        for(int i=0; i<A.size(); i++){
            for(int j=0; j<B.size(); j++){
                if(Comp == null){
                    if(A.elementAt(i) == B.elementAt(j)) C.add(A.elementAt(i));
                }
                else{
                    if(Comp.equal(A.elementAt(i), B.elementAt(j)) ) C.add(A.elementAt(i));
                }
            }
        }
        return C;
    }

    public static Set complement(final Set a, final Set b) {
        Set c = new Set(a);
        b.initIterator();
        Object x = b.next();

        while (x != null) {
            c.remove(x);
            x = b.next();
        }

        return c;
    }

    static public boolean subSet(Set A, Set B){
        A.initIterator();
        while(A.next() != null){
            if(!B.contains(A.current())) return false;
        }
        return true;

    }
    /** Works ONLY when A and B are sorted in ascending order*/
    static public boolean FastEqual(Set A, Set B, Comparator C){
        int i=0;
        Object aPrev=null, bPrev=null;
        Object a,b;
        int sizeA = A.size();
        int sizeB = B.size();

        if(sizeA != sizeB) return false;

        for(int j=0; j<sizeA; j++){
            a = A.elementAt(j);
            b = B.elementAt(i);
            if(aPrev!= null) if(!C.smaller(aPrev, a)) {
                    System.out.println("ERROR: input to FastSubSet not sorted in ascending order" + A);
                }
            if(bPrev!= null) if(!C.smaller(bPrev, b)) {
                    System.out.println("ERROR: input to FastSubSet not sorted in ascending order" + B);
                }
            if(a != b) return false;

            bPrev = b;
            aPrev = a;
        }
        return true;
    }
    /** Works ONLY when A and B are sorted in ascending order*/
    static public boolean equals(Set A, Set B){
        Object aPrev=null, bPrev=null;
        Object a,b;
        int sizeA = A.size();
        int sizeB = B.size();

        if(sizeA != sizeB) return false;

        for(int i=0; i<sizeA; i++){
            a = A.elementAt(i);

            boolean containsA = false;
            for(int j=0; j<sizeB; j++){
                b = B.elementAt(j);
                if(a == b) containsA = true;
            }
            if(!containsA) return false;

        }
        return true;
    }
    static public boolean properSubSet(Set A, Set B){
        //if(A.isEmpty()) return true;
        //if(A.isEmpty()) return true;
        A.initIterator();
        while(A.next() != null){
            if(!B.contains(A.current())) return false;
        }
        B.initIterator();
        while(B.next() != null){
            if(!A.contains(B.current())) return true;
        }
        return false;
    }

    /** Works ONLY when A and B are sorted in ascending order*/
    /** Works ONLY when A and B are sorted in ascending order*/
    static public boolean FastSubSet(Set A, Set B, Comparator C){
        int i=0;
        Object aPrev=null, bPrev=null;
        Object a,b;

        for(int j=0; j<A.size(); j++){
            a = A.elementAt(j);
            if(aPrev!= null) if(!C.smaller(aPrev, a)) {
                    System.out.println("ERROR: input to FastSubSet not sorted in ascending order");
                }
            while(i<B.size()) {
                b = B.elementAt(i);
                if(bPrev!= null) if(!C.smaller(bPrev, b)) {
                        System.out.println("ERROR: input to FastSubSet not sorted in ascending order");
                    }

                if(a == b) break;
                i++;

                bPrev = b;
            }
            if(i==B.size()) return false;

            aPrev = a;
        }
        return true;
    }
    static public Set FastIntersection(Set A, Set B, Comparator C){
        int i=0, j=0;
        Set inter = new Set(B.size());
        Object aPrev=null, bPrev=null;
        Object a,b;

        //if(A.size() == 0 || B.size() == 0) return inter;
        while(true){
            if(i<A.size() && j<B.size()){
                while(C.smaller(A.elementAt(i), B.elementAt(j))) {
                    if(aPrev!= null) if(!C.smaller(aPrev, A.elementAt(i))) {
                            System.out.println("ERROR: input to FastIntersection not sorted in ascending order: " + A);
                        }
                    aPrev = A.elementAt(i);
                    i++;
                }
                while(C.smaller(B.elementAt(j), A.elementAt(i))) {
                    if(bPrev!= null) if(!C.smaller(bPrev, B.elementAt(j))) {
                            System.out.println("ERROR: input to FastIntersectionnot sorted in ascending order" + B);
                        }
                    bPrev = B.elementAt(j);
                    j++;
                }
            }
            if(i == A.size() || j == B.size()) break;

            inter.add(A.elementAt(i));
            i++;
            j++;
        }
        return inter;
    }

    public Vector getAllSubsets(){
        return getAllSubsets(0, cardinality());
    }
    public Vector getAllSubsets(int maxCardinality){
        return getAllSubsets(0, maxCardinality);
    }

    public Vector getAllSubsets(int minCardinality, int maxCardinality){
        //              it is actually (n M) + (n M-1) + .... + (n m), with
        //              n=cardinality(), M=maxCardinality and m=minCardinality
        //              and (n m) being choose.
        int newSize = (int) (Utils.choose(cardinality(), maxCardinality) * 2.0);
        Vector ret = new Vector(newSize);

        Set S=null;
        for(int n=minCardinality; n<=maxCardinality; n++){
            powerSet ps = new powerSet(this, n);
            ps.initIterator();
            int i=0;
            Set curr;
            while((curr=(Set)ps.next()) != null){
                S = new Set(curr);
                ret.add(S);
                i++;
            }
        }
        return ret;
    }


    /**
     * This is a weak comparison, since it checks the references of the
     * elements, not if they are internally equal
     */
    static public boolean isEqual(Set A, Set B){
        if(A.size() != B.size()) return false;
        for(int i=0; i<A.size(); i++) if(A.elementAt(i) != B.elementAt(i)) return false;

        return true;
    }


    /**
     * Compare the sets <code>a</code> and <code>b</code>. Two sets are equals
     * if have same size and the same elements (not necesarily same order).
     */
    public static boolean isEqualElements(Set a, Set b) {
        if (!Set.isEqualSize(a, b)) return false;

        boolean result = false;
        for (int i = 0; i < a.size(); i++)
            if (b.contains(a.elementAt(i))) result = true;
            else result = false;

        return result;
    }

    public static boolean isEqualSize(Set a, Set b) {
        if (a.size() == b.size()) return true;
        return false;
    }

    public String toString(){
        String ret = "{";
        if(size()>0) ret+=elementAt(0);
        for(int i=1;i<size();i++){
            ret += ","+elementAt(i);
        }
        ret += "}";
        return ret;

    }

    /**
     * @return true if set not is empty
     */
    public boolean thereIsAny() { return !isEmpty(); }
}
