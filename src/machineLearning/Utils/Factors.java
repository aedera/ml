package machineLearning.Utils;

import java.util.Collection;
import java.util.List;
import java.util.Stack;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Iterator;

import machineLearning.Datasets.FastDataset;
import machineLearning.Datasets.FastInstance;

import machineLearning.Graphs.GraphBase;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.AbstractFactor;
import machineLearning.Graphs.GraphRVar;
import machineLearning.Graphs.GraphNode;
import machineLearning.Graphs.UGraph;
import machineLearning.Graphs.Variable;
import machineLearning.Graphs.Factor;
import machineLearning.Graphs.DiscreteFactor;
import machineLearning.Graphs.IndicatorFeature;
import machineLearning.Graphs.FactorGraph;
import machineLearning.Graphs.Assigment;
import machineLearning.Graphs.VarSet;

public class Factors {
    public static Factor eliminationVariables(Collection<Factor> factors,
                                              Collection<Variable> variables) {
        Collection<Factor> phis = new ArrayList<Factor>(factors);

        for (Variable variable : variables) {
            phis = sumProduct(phis, variable);
        }

        return Factors.products(phis);
    }

    public static Collection<Factor> sumProduct(Collection<Factor> factors,
                                                Variable variable) {
        // The Factors with varible in scope
        Collection<Factor> inScope = new ArrayList<Factor>();
        // The Factors without variable in scope
        Collection<Factor> outScope =
            (Collection)new ArrayList<Factor>(factors).clone();
        // Product of factors with variable
        Factor psi;

        // Calculate factors with variable in scope
        for (Factor factor : factors)
            if (factor.isInScope(variable))
                inScope.add(factor);

        // Calculate factors without variable in scope
        outScope.removeAll(inScope);

        psi = Factors.products(inScope);

        outScope.add(psi.marginalize(variable));
        return outScope;
    }

    public static Factor products(Collection<Factor> factors) {
        Stack<Factor> stack = new Stack();
        stack.addAll((Collection)new ArrayList<Factor>(factors).clone());

        for (int i = 0; i < factors.size() - 1; i++) {
            Factor a = stack.pop();
            Factor b = stack.pop();
            Factor outcome = a.multiply(b);
            stack.push(outcome);
        }

        return stack.pop();
    }

    private static Factor graphNodesToFactor(Collection<GraphNode> nodes) {
        Variable[] variables = new Variable[nodes.size()];
        int i = 0;

        for (GraphNode node : nodes)
            variables[i++] = new Variable(2, node.index());

        return new DiscreteFactor(variables);

    }

     /**
     * Finding maximal clique in graph by using Bron-Kerbosh Algorithm version 1.
     *
     * @param clique     Set whose elements are maximal cliques in the graph
     * @param compsub    Potential clique between calls
     * @param candidates Candidates to be part of a clique
     * @param not        Nodes not used between calls
     *
     * @todo improve selection to pivot by selectioning a node with many
     * neighborshood. The improve in the algorithm become it to version 2.
     */
    public static void
        bronKerboschAlgorithm(Collection<Factor> cliques,
                              List<GraphNode> compsub,
                              List<GraphNode> candidates,
                              List<GraphNode> not) {
        // Is found a clique?
        if (candidates.isEmpty() && not.isEmpty()) {
            cliques.add(graphNodesToFactor(compsub));
            return;
        }

        List<GraphNode> nodesToSelect = new ArrayList<GraphNode>(candidates);
        nodesToSelect.addAll(not);
        GraphNode pivot = nodesToSelect.get(0);
        List<GraphNode> neighborsOfPivot = pivot.neighbors();
        List<GraphNode> inspect = new ArrayList<GraphNode>(candidates);
        inspect.removeAll(neighborsOfPivot);

        for (int i = 0; i < inspect.size(); i++) {
            GraphNode selectedNode = inspect.get(i);

            List<GraphNode> newCompsub = new ArrayList<GraphNode>(compsub);
            newCompsub.add(selectedNode);
            candidates.remove(selectedNode);
            List<GraphNode> neighborsOfSelected = selectedNode.neighbors();

            List<GraphNode> newCandidates = new ArrayList<GraphNode>(candidates);
            newCandidates.retainAll(neighborsOfSelected);

            List<GraphNode> newNot = new ArrayList<GraphNode>(not);
            newNot.retainAll(neighborsOfSelected);

            bronKerboschAlgorithm(cliques, newCompsub, newCandidates, newNot);

            not.add(selectedNode);
        }
    }

    public static Collection<Factor> factorizeToCompleteSubgraphs(GraphBase net) {
        Collection<Factor> factors = new FactorGraph();

        bronKerboschAlgorithm(factors, new ArrayList(), new ArrayList(net.V()),
                              new ArrayList());

        return factors;
    }

    // TODO: do calculate parameter without to use parameter of graph...
    // TODO: validate that variables at factors are binaries.
    public static Collection<Factor>
        setParametersOfGraphInFactors(Collection<Factor> factors, MarkovNet graph) {
        VarSet scope;
        Assigment assigment;
        int[] indicesOfVars;

        for (Factor factor : factors) {
            scope = (VarSet)factor.scope();
            indicesOfVars = new int[scope.size()];
            indicesOfVars = scope.indicesToArray();

            for (Iterator iter = new Assigment(scope).iterator();
                 iter.hasNext();) {
                assigment = (Assigment)iter.next();

                double product =
                    productOfFactorsBinaries(indicesOfVars, assigment, graph);

                factor.setValue(assigment, product);
            }
        }

        return factors;
    }

    private static double
        productOfFactorsBinaries(int[] indicesOfVariables, Assigment assig,
                                 MarkovNet graph) {
        double outcome = 1.0;

        for (int i = 0; i < indicesOfVariables.length - 1; i++)
            for (int j = i + 1; j < indicesOfVariables.length; j++) {
                if (!graph.existEdge(i, j)) continue;

                outcome *= graph.getParamsAt(i, j,
                                             assig.getValueAt(i),
                                             assig.getValueAt(j));

            }

        return outcome;
    }

    public static void makeCache(final FastDataset dataset,
				 final Factor[] factors) {
	int[] scope = dataset.scope();
	int sizeDataset = dataset.size();

	for (Factor f : factors)
	    makeCache(dataset, sizeDataset, scope, (IndicatorFeature)f);
    }

    public static void makeCache(final FastDataset dataset,
				 final int sizeDataset,
				 final int[] scope,
				 final IndicatorFeature f) {
	f.assigmentsToValues = new double[sizeDataset][scope.length + 1][2];

	int value1, value2;
	for (FastInstance i : dataset) {
	    f.assigmentsToValues[i.index][scope.length][0] = f.value(i);
	    f.count += f.assigmentsToValues[i.index][scope.length][0];

	    for (int index : scope) {
		// first value
		value1 = i.values[index];
		f.assigmentsToValues[i.index][index][value1] =
		    f.assigmentsToValues[i.index][scope.length][0];

		// last value
		value2 = (value1 + 1) % 2;
		i.values[index] = value2;
		f.assigmentsToValues[i.index][index][value2] = f.value(i);

		i.values[index] = value1;
	    }
	}
   }

    public static Factor[] graphToFeatures(UGraph graph) {
	List<Factor> features = new ArrayList<Factor>();

	for (int i = 0; i < graph.size(); i++)
	    for (int j = i; j < graph.size(); j++)
		if (graph.existEdge(i, j)) {
		    features.add(new IndicatorFeature(new int[] {i, j},
						      new int[] { 0, 0 }));
		    features.add(new IndicatorFeature(new int[] {i, j},
						      new int[] { 0, 1 }));
		    features.add(new IndicatorFeature(new int[] {i, j},
						      new int[] { 1, 0 }));
		    features.add(new IndicatorFeature(new int[] {i, j},
						      new int[] { 1, 1 }));
		}

	return features.toArray(new Factor[0]);
    }

   public static Factor[] graphToFeatures(UGraphSimple graph) {
	List<Factor> features = new ArrayList<Factor>();

	for (int i = 0; i < graph.size(); i++)
	    for (int j = i; j < graph.size(); j++)
		if (graph.existEdge(i, j)) {
		    features.add(new IndicatorFeature(new int[] {i, j},
						      new int[] { 0, 0 }));
		    features.add(new IndicatorFeature(new int[] {i, j},
						      new int[] { 0, 1 }));
		    features.add(new IndicatorFeature(new int[] {i, j},
						      new int[] { 1, 0 }));
		    features.add(new IndicatorFeature(new int[] {i, j},
						      new int[] { 1, 1 }));
		}

	return features.toArray(new Factor[0]);
    }

    public static Factor[] generateInitial(int[] variables, java.util.Random r) {
	Collection<Factor> factors = new ArrayList<Factor>();

	ArrayList<int[]> scopes = new ArrayList<int[]>();

	int i = r.nextInt(variables.length);
	int j = i;

	while (i == j) j = r.nextInt(variables.length);

	scopes.add(new int[] { variables[i], variables[j] });

	return generateIndicators(scopes);
    }

    public static Factor[] generateIndicators(List<int[]> scopes) {
	Factor[] factors = new Factor[scopes.size() * 4];
	int i = -1;

	for (int[] scope : scopes) {
	    factors[++i] = new IndicatorFeature(scope, new int[] { 0, 0 });
	    factors[++i] = new IndicatorFeature(scope, new int[] { 0, 1 });
	    factors[++i] = new IndicatorFeature(scope, new int[] { 1, 1 });
	    factors[++i] = new IndicatorFeature(scope, new int[] { 1, 1 });
	}

	return factors;
    }

    public static Factor[] generateFactors(int[] variables, Factor[] initials) {
	ArrayList<Integer> init = new ArrayList<Integer>();

	for (Factor f : initials)
	    for (int i : ((AbstractFactor)f).scope_)
		if (!init.contains(i)) init.add(i);

	ArrayList<int[]> scopes = new ArrayList<int[]>();

	for (int i = 0; i < variables.length; i++)
	    for (int j = i+1; j < variables.length; j++) {
		if (variables[i] == init.get(0) && variables[j] == init.get(1))
		    continue;

		scopes.add(new int[] { variables[i], variables[j] });
	    }

	return generateIndicators(scopes);
    }

    public static Factor[] factorizeToPairwise(MarkovNet net) {
	List<Factor> factors = new ArrayList<Factor>();

	for (int i = 0; i < net.size(); i++)
	    for (int j = i + 1; j < net.size(); j++)
		if (net.existEdge(i, j))
		    factors.add(new DiscreteFactor(new int[] {i, j},
						   net.params[i][j][0][0],
						   net.params[i][j][0][1],
						   net.params[i][j][1][0],
						   net.params[i][j][1][1]));

	return factors.toArray(new Factor[0]);
    }
}