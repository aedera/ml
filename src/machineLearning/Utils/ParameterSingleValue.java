package machineLearning.Utils;

import java.util.ArrayList;

public class ParameterSingleValue<T>  extends Parameter<T>{
	//-----------------------------------------------------
	public ParameterSingleValue(String name, T value, String description){
		super(name, description, value);
	}
	//-----------------------------------------------------
	public ParameterSingleValue(String name, T value) throws IllegalAccessException{
		super(name, value);
		//if(value.length == 0)  throw new IllegalAccessException();
	}
	
//	-----------------------------------------------------
	public void setValueFromString(String valueStr)throws IllegalAccessException {
		values.add(0, convertToTyped(valueStr));
		
	}
//	-----------------------------------------------------
	public void setValues(ArrayList<String> args) throws IllegalAccessException{
		if(args.size() > 1) throw new IllegalAccessException("");
		super.setValues(args);
		
	}
	//-----------------------------------------------------
	public T getValue(int index) throws IllegalAccessException{
		if(true) throw new IllegalAccessException();
		return null;
	}
	//-----------------------------------------------------
	public T getValue(){
		return values.get(0);
	}

	
//	-----------------------------------------------------
	public boolean getBooleanValue(int index) throws IllegalAccessException{
		if(true) throw new IllegalAccessException("");
		return false;
	}
//	-----------------------------------------------------
	public int getIntValue(int index) throws IllegalAccessException{
		if(true) throw new IllegalAccessException("");
		return -1;
	}
	//-----------------------------------------------------
	public double getDoubleValue(int index ) throws IllegalAccessException{
		if(true) throw new IllegalAccessException("");
		return -1;
	}
	//-----------------------------------------------------
	public String getStringValue(int index) throws IllegalAccessException{
		if(true) throw new IllegalAccessException("");
		return null;
	}//-----------------------------------------------------
	//-----------------------------------------------------
	public boolean getBooleanValue() throws IllegalAccessException{
		if(!type.equalsIgnoreCase("Boolean")) throw new IllegalAccessException("");
		return ((Boolean) values.get(0)).booleanValue();
	}
	//-----------------------------------------------------
	public int getIntValue() throws IllegalAccessException{
		if(!type.equalsIgnoreCase("Integer") && !type.equalsIgnoreCase("Double") ) throw new IllegalAccessException("");
		if(type.equalsIgnoreCase("Double")) return (int) ((Double) values.get(0)).doubleValue();
		return (Integer) values.get(0);
	}
	//-----------------------------------------------------
	public double getDoubleValue() throws IllegalAccessException{
		if(!type.equalsIgnoreCase("Integer") && !type.equalsIgnoreCase("Double") ) throw new IllegalAccessException("Not a valid Type: " + type.toString());
		if(type.equalsIgnoreCase("Integer")) return (double) ((Integer) values.get(0)).intValue();
		return (Double) values.get(0);
	}
	//-----------------------------------------------------
	public String getStringValue() throws IllegalAccessException{
		if(!type.equalsIgnoreCase("String")) throw new IllegalAccessException("");
		return (String) values.get(0);
	}
}
