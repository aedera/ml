package machineLearning.Utils;

/**
 *
 * <p>Title: Used for screen output management and logging.</p>
 * <p>Description: Initially user defines as many levels of verbosenes they need (e.g. verbose, detailed, other).
 *                 Also, it initially defines a log file on which the screen output will be logged.
 *                 Finally, the user sets which verbose_level it wants in this run.
 *    It then use println or print with verbose_level that sets that it will be printed if verbose_level match or is lower.</p>
 *    An alternative, and more efficient way to use this class is to start a session on a certain level of verboseness using
 *    output.startSession(LOG | MAINLOOP) for instance. Then, user can use member variables print, or getter print() to check if
 *    we should print or not. This can be used as follows:  System.out.println(output.print?<String to output>:"");
 *    This is more efficient than output.println(<String to output>); as it evaluates the string, usually very slow and inneficient. 
 * @author Facundo Bromberg
 * @version 1.0
 */
public class Output
{
  //HashMap verbose_levels = new HashMap(10);
  //String [] current_v_levels;
  int currentVLevels;
  String logFileName;
  private boolean silentMode = false, temporary_silentMode = false;
  private boolean print = false, log = false;
  
  //VerboseLevels verboseLevels;
  
  boolean orHigher;
  //--------------------------------------------------------------
  public Output(String logFileName){
	  this.logFileName = logFileName;
  }
  //--------------------------------------------------------------
  public Output(){
	  this.logFileName = null;
	  currentVLevels = 0;
  }
//--------------------------------------------------------------
  public void setLogFileName(String lfn){logFileName = lfn;};
  //--------------------------------------------------------------
  public void clearLogFile(){
	  Utils.emptyFile(logFileName);
  }
  //--------------------------------------------------------------
  public void clearLogFile(String logFileName2){
	  if(this.logFileName == null) Utils.emptyFile(logFileName2);
	  else Utils.emptyFile(logFileName);
  }
  //--------------------------------------------------------------
  public void silentMode(){
	  silentMode = true;
  }
  //--------------------------------------------------------------
  public void silentMode(boolean val){
	  silentMode = val;
  }
  //--------------------------------------------------------------
  //For instance, if level_name is VERBOSE, and subLevels contains DETAILED, then 
  //everytime a println level is set to DETAILED it will be printed whenever verbose level is set to VERBOSE.
  /*public void addVerboseLevel(String level_name){
	  int size = verbose_levels.size();
	  verbose_levels.put(level_name, new Integer(size));
  }
  *///--------------------------------------------------------------
  //Example log.println("Print this line", "FILE+VERBOSE");
/*  public void setVerboseLevel(String vLevels){
	  current_v_levels= parseLevelsString(vLevels);
  }
*/  ///--------------------------------------------------------------
  //Example log.println("Print this line", "VerboseLevels.FILE || VerboseLevels.VERBOSE");
  public void setVerboseLevel(int vLevels){
	  currentVLevels= vLevels;
  }
  //--------------------------------------------------------------
  public boolean doPrint(int vLevels){
	  computePrintBoolean(vLevels);
	  
	  return !temporary_silentMode && (print);
  }
  //--------------------------------------------------------------
  public boolean printResult(int vLevels, String content){
	  //printingResult = true;
	  return print(vLevels | VerboseLevels.RESULT, content);
	  //printingResult = false;
  }
  public boolean printlnResult(int vLevels, String content){
		//printingResult = true;
	    return println(vLevels | VerboseLevels.RESULT, content);
    	//printingResult = false;
}
  //--------------------------------------------------------------
  //Example log.println("Print this line", "FILE+VERBOSE");
/*  public void println(String vLevels, String content){
	  if(!printingResult && silentMode) return;
	    
	  print(vLevels, content+"\n");
  }*/
  //--------------------------------------------------------------
  //Example log.println("Print this line", "FILE+VERBOSE");
  public final void println(String ... content){
	  for(String s : content)print(s);
	  print("\n");
  }
  //--------------------------------------------------------------
  /**
   * SUPER, HIGHER, INCLUSIVE says should print if any level (or higher) of vLevels is current (a.k.a. set).
   * For instance, if vLevels is "VERBOSE+HIGHER", then will print even though current level is NONVERBOSE.
   */
	  public final void print(String content){
		  if((VerboseLevels.LOG&currentVLevels) != 0) {
			  Utils.writeStringToFile(content, logFileName, true, false);
			  }
		  
		  System.out.print(content);
	  }

  //--------------------------------------------------------------
  //Example log.println("Print this line", "FILE+VERBOSE");
  public final boolean println(int vLevels, String content){
	  return print(vLevels, content+"\n");
  }
  //--------------------------------------------------------------
  /**
   * SUPER, HIGHER, INCLUSIVE says should print if any level (or higher) of vLevels is current (a.k.a. set).
   * For instance, if vLevels is "VERBOSE+HIGHER", then will print even though current level is NONVERBOSE.
   */
	  public final boolean print(int vLevels, String content){
		  computePrintBoolean(vLevels);
		  //boolean loc_log = computeLogBoolean();
		  
		  if(!temporary_silentMode && print) System.out.print(content);
		  if(!temporary_silentMode && log) Utils.writeStringToFile(content, logFileName, true, false);
		  
		  return !temporary_silentMode && (print || log);
	  }

//	--------------------------------------------------------------
/*	  final private boolean computeLogBoolean(){
		  if((VerboseLevels.LOG&currentVLevels) != 0) {
			  return true;
		  }
		  return false;
	  }*/
		//--------------------------------------------------------------
	  /**
	   * Returns the value of print, which says whether to print or not (by comparing
	   * vLevels with currentVLevels;
	   */
	  private boolean computePrintBoolean(int vLevels){
		  if(  ((vLevels&VerboseLevels.RESULT)!=0)) {
			  print = true;
			  temporary_silentMode = false;
			  return true;
		  }
		  if(silentMode) {
			  temporary_silentMode = true;
			  print = false;
			  return false;
		  }
		  if((VerboseLevels.LOG&currentVLevels) != 0) {
			 log = true;
		  }
		  if((vLevels&VerboseLevels.LOG) != 0){
				vLevels -= (VerboseLevels.LOG);
		  }
		  //boolean orHigher = containsLevel(vl, "SUPER") || containsLevel(vl, "HIGHER") || containsLevel(vl, "INCLUSIVE")|| containsLevel(vl, "UP");
		  orHigher = ((vLevels&VerboseLevels.SUPER)!=0) ||
		  			((vLevels&VerboseLevels.HIGHER)!=0) ||
		  			((vLevels&VerboseLevels.INCLUSIVE)!=0) ||
		  			((vLevels&VerboseLevels.UP)!=0);
		  //for(int i=0; i<vl.length; i++){
		  if(  ((vLevels&currentVLevels)!=0 )) {
			  print = true;
			  return true;
		  }
		  if(orHigher) {
			  vLevels -= (VerboseLevels.HIGHER);
			  vLevels = ((int)Math.pow(2,((int)Utils.log2(vLevels)+1))-1);
			  if(  ((vLevels&(currentVLevels-VerboseLevels.LOG))==0 )) {
				  print = true;
				  return true;
			  }
		  }
		  print = false;
		  
		  return (print);
	  }

  //--------------------------------------------------------------
  public static void main(String[] args) {
/*	  Output output = new Output("eraseme.output", new VerboseLevels());
	  output.setVerboseLevel(VerboseLevels.MAINLOOP);
	  
	  output.println(VerboseLevels.MAINLOOP | VerboseLevels.LOG, "YES");
	  output.println(VerboseLevels.SIMPLE| VerboseLevels.LOG, "NO"); //shouldn't print it;
	  output.println(VerboseLevels.SIMPLE | VerboseLevels.LOG | VerboseLevels.HIGHER, "YES 2"); 
	  output.println(VerboseLevels.SIMPLE | VerboseLevels.HIGHER, "YES 3"); 
	  output.println(VerboseLevels.VERBOSE | VerboseLevels.LOG | VerboseLevels.HIGHER, "NO -1"); 
	  
	  output.silentMode();
	  output.println(VerboseLevels.SIMPLE | VerboseLevels.LOG, "NO"); //shouldn't print;
	  output.printlnResult(0, "YES 4");*/

  }
}
