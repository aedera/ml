package machineLearning.Utils;

import java.util.Collection;
import java.util.Map;
import java.util.ArrayList;
import java.util.Iterator;

import java.io.InputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.yaml.snakeyaml.Yaml;

import org.apache.commons.lang.ArrayUtils;

import machineLearning.Graphs.GraphBase;
import machineLearning.Graphs.UGraph;
import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.UGraphIterator;
import machineLearning.Graphs.GraphRVar;
import machineLearning.Graphs.Factor;
import machineLearning.Graphs.DiscreteFactor;
import machineLearning.Graphs.FactorGraph;
import machineLearning.Graphs.Variable;

import machineLearning.DataStructures.Model;

public class Graphs {
    public static Collection<GraphBase> getAllStructuresBy(int numNodes) {
        Collection<GraphBase> graphs = new ArrayList();

        for (UGraphIterator iter = new UGraphIterator(numNodes);
             iter.hasNext();)
            graphs.add(iter.next());

        return graphs;
    }

    /**
     * @return representation of graph at bitset. It represent the triangulate
     * upper matrix of the edges. The bitset is ordered in little-endian
     */
    public static boolean[] graphToBitSet(UGraph graph) {
        boolean[] edges = new boolean[(graph.size() * (graph.size() - 1)) / 2];

        for (int i = 0; i < graph.size(); i++)
            for (int j = i + 1; j < graph.size(); j++)
                if (graph.existEdge(i, j))
                    edges[indexBitSet(graph.size(), i, j)] = true;

        return edges;
    }

    private static int indexBitSet(int size, int... indices) {
        int dim = size - 1;
        int offsetX = 0;
        for (int i = 0; i < indices[0]; i++)
            offsetX += dim - i;
        int offsetY = indices[1] - indices[0] - 1;

        return offsetX + offsetY;
    }

    /**
     * @param numGraph number decimal that represent the edges in a graph.
     * @param numNodes number of nodes
     *
     * @return return a instance of <code>UGraph</code> with number of node
     * equals a <code>numNodes</code> and edges equals a
     * <code>numGraph</code>.
     *
     * @todo hard-code. Improve code.
     */
    public static UGraph getUGraphBy(int numGraph, int numNodes)
        throws Exception {
        int currentNum = 0;
        UGraph graph = null;

        for (UGraphIterator iter = new UGraphIterator(numNodes);
             iter.hasNext();) {
            graph = (UGraph)iter.next();
            if (currentNum++ == numGraph) return graph;
        }

        throw new Exception("Number of graph not valid by number of nodes.");
    }

    public static Model getUGraphFrom(String path)
        throws FileNotFoundException {
        Map<String, Object> hash = (Map<String, Object>)loadFromStream(path);

        MarkovNet structure = new MarkovNet();
        setNodes(structure, hash);
        FactorGraph factors = setParameters(hash);

        return new Model(structure, factors);
    }

    private static void setNodes(UGraph graph, Map<String, Object> hash) {
        Collection<Integer> variables = (Collection<Integer>)hash.get("variables");
        Map<Integer, Collection<Integer>> structure =
            (Map<Integer, Collection<Integer>>)hash.get("structure");

        for (Integer index : variables)
            addNode(index.intValue(), graph);

        for (Iterator<Map.Entry<Integer, Collection<Integer>>> it = structure.entrySet().iterator();
             it.hasNext();) {
            Map.Entry<Integer, Collection<Integer>> pair = it.next();

            int key = pair.getKey().intValue();
            for (Integer index : pair.getValue())
                graph.addEdge(key, index.intValue());

        }
    }

    private static FactorGraph
        setParameters(Map<String, Object> hash) {
        Map<Collection<Integer>, Collection<Double>> parameters =
            (Map<Collection<Integer>, Collection<Double>>)hash.get("parameters");
        FactorGraph factors = new FactorGraph();

        for (Iterator<Map.Entry<Collection<Integer>, Collection<Double>>> it =
                 parameters.entrySet().iterator();
             it.hasNext();) {
            Map.Entry<Collection<Integer>, Collection<Double>> pair = it.next();
            int[] edge =
                ArrayUtils.toPrimitive(pair.getKey().toArray(new Integer[pair.getKey().size()]));
            double[] values =
                ArrayUtils.toPrimitive(pair.getValue().toArray(new Double[pair.getValue().size()]));

            Variable[] variables = new Variable[edge.length];

            for (int i = 0; i < variables.length; i++)
                variables[i] = new Variable(2, edge[i]);

            DiscreteFactor factor = new DiscreteFactor(variables, values);

            factors.add(factor);
        }

        return factors;
    }

    private static void addNode(int index, UGraph graph) {
        if (!graph.containsIndex(index))
            graph.addNode(new GraphRVar("" + index, index));
    }

    private static Object loadFromStream(String path)
        throws FileNotFoundException {
        InputStream input = new FileInputStream(new File(path));
        Yaml yaml = new Yaml();

        return yaml.load(input);
    }
}