package machineLearning.Utils;


public class Profiler extends ProfilerJNI {
  String me;
  long cputime, time;
  static long staticTime;
  
  public Profiler(String name) {
	me = name;
  }
  
  public void start() {
    if(!failed) cputime = getCurrentThreadCpuTime();
    else time = System.currentTimeMillis();
  }
  
  static public void staticStart() {
	    if(!failed) staticTime = getCurrentThreadCpuTime();
	    else staticTime = System.currentTimeMillis();
	  }

  static public  long staticStop() {
	    if(!failed) staticTime = getCurrentThreadCpuTime() - staticTime;
	    staticTime = System.currentTimeMillis() - staticTime;
	    
	    return staticTime;
	  }

  public  long stop() {
    if(!failed) cputime = getCurrentThreadCpuTime() - cputime;
    time = System.currentTimeMillis() - time;
    
    return failed?time:cputime;
  }
  
  public long time(){return failed?time:cputime;};
  static public long staticTime(){return staticTime;};
  
  public void print() {
    System.out.println(me + " time: " + time + " ms"
      + " cputime: " + cputime/1000000 + " ms");
  }
  
}