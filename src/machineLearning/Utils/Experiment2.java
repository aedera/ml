package machineLearning.Utils;

//import GSMN;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import machineLearning.independenceBased.IndependenceTest;


//First numIndepVars variables are independent and their values can be set.
abstract public class Experiment2 {
    public IndependenceTest IT = null;

    static protected Random r;
    static public Output output;

    protected HashMap<String, Flag> flags = new HashMap<String,Flag>();
    protected HashMap<String, Parameter> controlParamsHash = new HashMap<String, Parameter>();

    protected ArrayList<Parameter> controlParamsArray = new ArrayList<Parameter>();
    protected ArrayList<ParameterSingleValue> outputParams = new ArrayList<ParameterSingleValue>();

    protected int repetition=0;
    protected int numRepetitions;



    //------------------------------------------------------------------
    //  -------------------------------------------------------
    public Experiment2(){
        output = new Output();

        //Default controls that every experiment has
        addFlag(new Flag("header", "Prints a header with all input and output parameters and their column in the CSV output."));
        addFlag(new Flag("help", "Outputs this help."));
        //addFlag(new Flag("clearLog", "Empties the log file"));

        addControl(new ParameterSingleValue<Integer>("seed", 123456, "Seed for random number generator. Same seed for all runs of the experiment."));
        addControl(new ParameterSingleValue<String>("log", "default.log", "Log file name"));
        addControl(new ParameterSingleValue<Integer>("verbose", 2, "Sets the output level. 0 is \n"+
                                                     "equivalent to output silent mode, i.e., only ouputs CVS string. 1 includes a human readable description. \n" +
                                                     "Programs can have up to level 4. If verbose is not used, the default value is 1. If verbose is used without parameters the default value is 2.\n" +
                                                     " As a general rule, level 0 is for reporting events that occur approximately every 5 to more minutes. Level 1 is a verbose version of level\n" +
                                                     "0, with the output in comment format (first character is #). Level 2 is for events that shows the progression of the \n" +
                                                     "software, either to System.err, or with a leading #. Level 3 is for debugging purposes at a high level (e.g., output \n" +
                                                     "of algorithms main loop, and level 4 is very detail and would considerably slow down the execution.\n"));

        //readValues(args, NameThatDefinesNumRuns);
    }
    //    System.out.println(CVSheader());
    //  ------------------------------------------------------------------
    public void readValues(String [] args) throws IllegalAccessException{
        readValues(args, null);
    }
    public void readValues(String [] args, String NameThatDefinesNumRuns) throws IllegalAccessException{
        String arg;
        Parameter param = null;

        if(args.length == 0){
            System.out.println(description());
            System.exit(0);
        }

        System.out.print("#");

        ArrayList<String> argsSingle = new ArrayList<String>();
        for(int i=0;i<args.length;i++){
            System.out.print(i+"]"+ args[i]+ " ");

            //Detects parameter name
            arg = args[i].trim();
            if(arg.charAt(0) == '-'){
                param = controlParamsHash.get(arg.substring(1));
                String str = arg.substring(1);
                if(param == null) param = flags.get(arg.substring(1));

                if(param == null) throw new IllegalArgumentException("Invalid argument " + str);

            }
            else{ //remaining args are values of param (until another '-' is found)
                if(param == null) throw new IllegalArgumentException("Invalid argument");
                argsSingle.add(arg);
            }
            if(i+1 == args.length || args[i+1].trim().charAt(0)=='-'){//next is '-' or end of args
                if(param == null) throw new IllegalArgumentException("Invalid argument");

                param.setValues(argsSingle);
                argsSingle.clear();
            }

        }

        processInputs(NameThatDefinesNumRuns);
    }
    //  ------------------------------------------------------------------
    //  -------------------------------------------------------
    protected void processInputs(String NameThatDefinesNumRuns) throws IllegalAccessException{
        Parameter param;

        /** Help number*/
        if(flags.get("help").getBooleanValue()){
            System.out.println(description());
            System.exit(1);
        }

        /** Header number*/
        if(flags.get("header").getBooleanValue()){
            System.out.println(CVSheader());
            System.exit(1);
        }

        /** Clear Log file *//*
            if(flags.get("clearLog").getBooleanValue()){
            output.clearLogFile();
            }*/

        /** Number of repetitions */
        numRepetitions = NameThatDefinesNumRuns==null? 1 : controlParamsHash.get(NameThatDefinesNumRuns).numValues();

        /** Random number*/
        r = new Random(controlParamsHash.get("seed").getIntValue());

        /** Log file */
        output.setLogFileName(controlParamsHash.get("log").getStringValue());


        //      Experiment.output.silentMode(false);
        //              if(args.length!=0) Experiment.output.silentMode(true);
        /** Verbose file */
        param = controlParamsHash.get("verbose");
        int level = 1;
        if(param.numValues() == 0) level = 2;
        else if(param.numValues() == 1) level = param.getIntValue();
        else throw new IllegalArgumentException();

        if(level == 0) output.setVerboseLevel(coVL.LEVEL0 );
        if(level == 1) output.setVerboseLevel(coVL.LEVEL0 | coVL.LEVEL1);
        if(level == 2) output.setVerboseLevel(coVL.LEVEL0 | coVL.LEVEL1 | coVL.LEVEL2);
        if(level == 3) output.setVerboseLevel(coVL.LEVEL0 | coVL.LEVEL1 | coVL.LEVEL2 | coVL.LEVEL3);
        if(level == 4) output.setVerboseLevel(coVL.LEVEL0 | coVL.LEVEL1 | coVL.LEVEL2 | coVL.LEVEL3 | coVL.LEVEL4);
    }

    //------------------------------------------------------------------
    public String description() throws IllegalAccessException{
        String out = "java Experiment {-flag -parameter  parameterValue}\n\n";
        out += "Flags:\n";
        for(Flag p : flags.values()){
            out += "\t" + p.description() + "\n\n";
        }
        out += "Parameters:\n";
        for(Parameter p : controlParamsArray){
            out += "\t" + p.description() + " Default value = " + p.getValueAsString() + "\n\n";
        }
        return out;
    }

    //  ------------------------------------------------------------------
    abstract protected void computeValuesForIteration() throws IllegalAccessException;

    //-------------------------------------------------------
    protected boolean getBooleanValue(String controlParamKey, int ... index) throws IllegalAccessException{
        if(index.length == 0) return controlParamsHash.get(controlParamKey).getBooleanValue();
        else return controlParamsHash.get(controlParamKey).getBooleanValue(index[0]);
    }
    //-------------------------------------------------------
    protected int getIntValue(String controlParamKey, int ... index) throws IllegalAccessException{
        if(index.length == 0) return controlParamsHash.get(controlParamKey).getIntValue();
        else return controlParamsHash.get(controlParamKey).getIntValue(index[0]);
    }
    //-------------------------------------------------------
    protected void setIntValue(String controlParamKey, int value) throws IllegalAccessException{
        controlParamsHash.get(controlParamKey).setIntValue(value);
    }
    //-------------------------------------------------------
    protected double getDoubleValue(String controlParamKey, int ... index) throws IllegalAccessException{
        if(index.length == 0 ) return controlParamsHash.get(controlParamKey).getDoubleValue();
        else return controlParamsHash.get(controlParamKey).getDoubleValue(index[0]);
    }
    //-------------------------------------------------------
    protected String getStringValue(String controlParamKey, int ...index) throws IllegalAccessException{
        if(index.length == 0) return controlParamsHash.get(controlParamKey).getStringValue();
        else return controlParamsHash.get(controlParamKey).getStringValue(index[0]);

    }
    //-------------------------------------------------------
    protected void addFlag(Flag val){
        flags.put(val.name, val);
    }//-------------------------------------------------------
    protected void addControl(Parameter val){
        controlParamsHash.put(val.name, val);
        controlParamsArray.add(val);
    }

    public void setControl(String key, Object value) {
        Parameter param = controlParamsHash.get(key);

        for (int i = 0; i < controlParamsArray.size(); i++)
            if (controlParamsArray.get(i) == param)
                controlParamsArray.remove(i);

        controlParamsHash.remove(param);

        addControl(new ParameterSingleValue<String>(key, value.toString(), ""));
    }

    //  -------------------------------------------------------
    public void addOutput(ParameterSingleValue val){
        outputParams.add(val); //NOTE. If there was a value for val, it simply replace it.
    }
    //  -------------------------------------------------------
    public void setDoubleOutput(String key, double value) throws IllegalAccessException{
        for(ParameterSingleValue p : outputParams){
            if(p.name.equals(key)) p.setValueFromString(String.format("%1.20f", value));
        }
        //      outputParams.add(val); //NOTE. If there was a value for val, it simply replace it.
    }    //------------------------------------------------------------------
    //  -------------------------------------------------------
    public void setOutput(String key, String value) throws IllegalAccessException{
        for(ParameterSingleValue p : outputParams){
            if(p.name.equals(key)) {
                p.setValueFromString(value);
                break;
            }
        }
        //      outputParams.add(val); //NOTE. If there was a value for val, it simply replace it.
    }    //------------------------------------------------------------------
    private double [] getOutcomesArray() throws IllegalAccessException{
        double output [] = new double[outputParams.size()];
        int i=0;
        for(Parameter p : outputParams){
            output[i++] = p.getDoubleValue();
        }
        return output;
    }
    //  ------------------------------------------------------------------
    private String [] getControlArray(int numDecimals) throws IllegalAccessException{
        String output [] = new String[controlParamsArray.size()];
        int i=0;
        for(Parameter p : controlParamsArray){
            output[i++] = p.getValueAsString();
        }
        return output;
    }
    /*  //------------------------------------------------------------------
        private void setOutcomesFromArray(double [] vals) throws IllegalAccessException{
        int i=0;
        for(ParameterSingleValue p : outputParams){
        //Remove trailing .000
        double aux = vals[i] - Math.floor(vals[i]);
        if(vals[i] - Math.floor(vals[i]) == 0.0) p.setValue((int) vals[i]);
        else p.setValue(vals[i]);
        }
        }*/
    //------------------------------------------------------------------
    //if resetFile = true, then the file is emptied and the header is thus saved.
    //Object IVV;       //independent variable's values
    //int IVI; //independent variable's index
    //  double [] NCHIVV; //non-changing independent variables values
    public void run() throws IllegalAccessException{
        double auxoutcome [] = new double[1];

        repetition = -1;
        //computeValuesForIteration(false);

        Run run = new Run(outputParams.size(), numRepetitions);;

        boolean print = output.doPrint(VerboseLevels.SIMPLE);

        //if(fileName != null) Utils.println(fileName, CVSheader(), true, true);
        boolean exception = false;
        for(int j=0; j<numRepetitions; j++){
            repetition = j;

            try{
                computeValuesForIteration();
                auxoutcome = getOutcomesArray();
            }
            catch(Exception e){
                e.printStackTrace();
                for(int i=0; i<auxoutcome.length; i++){
                    auxoutcome[i] = -1;
                }
                run.setMeanAndStdev(auxoutcome);
                exception = true;
                break;
            }

            run.addIteration(auxoutcome);

            //output
            if(print){
                System.out.println("Repetition = " + j);
                System.out.println(CVSheader());
            }
        }
        if(!exception){
            run.normalize();
            //setOutcomesFromArray(run.meanValues);
        }


        String aux = "#------------------------------------------------------------------------";
        if(output.doPrint(coVL.LEVEL1)) System.out.println(aux);
        if(output.doPrint(coVL.LEVEL1)) System.out.print(toStringDecorated(run));
        if(output.doPrint(coVL.LEVEL0)) System.out.println(run.CVSoutput(20, getControlArray(20)));
        if(output.doPrint(coVL.LEVEL1)) System.out.println(aux);
    }
    //------------------------------------------------------------------
    protected String toStringDecorated(Run run) throws IllegalAccessException {
        String ret = "# FLAGS = ";
        for(Flag f : flags.values()){
            if(f.getBooleanValue()) ret += f.toString() + " ";
        }
        ret += "\n";

        for(Parameter p : controlParamsArray){
            ret += "# "+ p.toString()+"\n";
        }

        ret += "#....................................";
        /*for(Parameter p : outputParams){
          ret += "# "+ p.toString()+"\n";
          }*/

        ret += "\n# " + outputParams.get(0).name + " = " + Utils.myDoubleToString(run.meanValue(0),3);
        /*if(numRepetitions > 1)*/ret += " +/- " + Utils.myDoubleToString(run.standardDev(0),2);
        for(int k=1 ;k <outputParams.size(); k++){
            ret += "\n# " + outputParams.get(k).name + " = " + Utils.myDoubleToString(run.meanValue(k),3);
            /*if(numRepetitions > 1)*/ ret += " +/- " + Utils.myDoubleToString(run.standardDev(k),2);
        }
        ret += "\n#....................................";
        ret += "\n";

        return ret;
    }
    //------------------------------------------------------------------
    protected String CVSheader() throws IllegalAccessException{
        String ret = "# CONTROL\n";//"#" + ctrlVarNames[0];

        int j=0;
        for(Parameter p : controlParamsArray) {
            ret += "#\t "+ (j+1) + ":" + p.name + "\n";
            j++;
        }
        int k=0;
        // computeValuesForIteration(false);
        ret += "#-------------------\n# OUTPUT\n";
        for(Parameter p : outputParams) {
            ret += "#\t "+ (j+1+2*k) + ":" + p.name + "\n";
            k++;
        }
        //       for(j=0; j<outputParams.size(); j++) ret += "#\t "+ (controlParamsArray.size()+j+1) + ":"+ (controlParamsArray.size()+1+(j)*(j>0?2:1)) + ":"+ outputParams.get(j) + "\n";

        return ret;
    }

}
