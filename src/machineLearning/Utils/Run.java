/*
 * Created on Nov 4, 2005
 *
 */
package machineLearning.Utils;

import java.util.Random;

/**
 * 
 * @author bromberg
 */
public class Run {

	//int capacity;	//maximum number of variables M variables
	public int iterations=0;		//actual number of inputs
	int numVars, capacity;
	
	double values[][];
	double sums[];
	double squareSums[];
	
	double meanValues[];
	double standardDev[];
	
	String varNames[];
	
	//---------------------------------------------------------------------------
	public Run(int numVariables, int capacity){
		RunInternal(numVariables, capacity);
	}
	public Run(int numVariables){
		RunInternal(numVariables, 10);
	}
	public void RunInternal(int numVariables, int capacity){
		this.numVars = numVariables;
		this.capacity = capacity;
		
		if(capacity > 0){
			values = new double[numVars][];
			for(int i=0; i<numVars; i++) values[i] = new double[capacity];
		}
		else values = null;
		
		sums = new double[numVars];
		squareSums = new double[numVars];

		meanValues = new double[numVars];
		standardDev = new double[numVars];

		varNames = new String[numVars];
	}
	//---------------------------------------------------------------------------
	public int numVars(){
		return numVars;
	}
	//---------------------------------------------------------------------------
	public void setVarName(int varIndex, String name){
		this.varNames[varIndex] = name;
	}
    //---------------------------------------------------------------------------
    public void setMeanAndStdev(double [] newValues){ //used for exception purposes
        for(int i=0; i<numVars; i++){
            meanValues[i] = -1;
            standardDev[i] = -1;
        }
    }
	//---------------------------------------------------------------------------
	public void addIteration(double [] newValues){
		Utils.ASSERT(numVars == newValues.length, "error in class Experiment.add");
		
		if(iterations == capacity){
			capacity *= 2;
			for(int i=0; i<numVars; i++){
				double [] aux = new double[capacity];
				//System.arraycopy(values[i], 0, aux, 0, capacity);
				System.arraycopy(values[i], 0, aux, 0, capacity/2);
				values[i] = null;
				values[i] = aux;
			}
		}
			
		for(int i=0; i<numVars; i++){
			if(capacity > 0){
				values[i][iterations] = newValues[i];
			}
			sums[i] += newValues[i];
			squareSums[i] += newValues[i]* newValues[i];
			
			meanValues[i] = sums[i]/(iterations+1);
		}
		iterations++;
	}
	//	----------------------------------------------------------------------------
	/**
	 * Shits all values down one index and adds the new values at the end;
	 */
	public void shiftedIteration(double [] newValues){
		Utils.ASSERT(numVars == newValues.length, "error in class Experiment.add");
		Utils.ASSERT(capacity > 0, "error in class Experiment.shiftedIteration. Capacity == 0.");

		//Substract first iteration from sums
		for(int i=0; i<numVars; i++){
			sums[i] -= values[i][0];
			squareSums[i] -= values[i][0] * values[i][0];
		}
		//Shift iterations
		for(int i=0; i<numVars; i++){
			System.arraycopy(values[i], 1, values[i], 0, capacity-1);
		}
		iterations--;
		//Add new values
		addIteration(newValues);
		
	}
	//---------------------------------------------------------------------------
	/**
	 * WORKS ONLY FOR EXPERIMENTS WITH ONE VARIABLE
	 */
	final public void addIteration(int varIndex, double newValue){
		double [] auxVal = new double[1];
		auxVal[0] = newValue;
		addIteration(auxVal);
	}
	//----------------------------------------------------------------------------
	final public void increment(){iterations++;};
	//	----------------------------------------------------------------------------
	/**
	 * Shits all values down one index and adds the new values at the end. 
	 * WORKS ONLY FOR EXPERIMENTS WITH ONE VARIABLE
	 */
	public void shiftedIteration(int varIndex, double newValue){
		double [] auxVal = new double[1];
		auxVal[0] = newValue;
		shiftedIteration(auxVal);
	}
	
	
	//	----------------------------------------------------------------------------
	public void normalize(){
		for(int i=0; i<numVars; i++){
			meanValues[i] = sums[i]/iterations;
	
			standardDev[i] = 0;
			for(int j=0; j<iterations; j++){
				standardDev[i] += Math.pow((values[i][j] - meanValues[i]),2); 
			}
			standardDev[i] = Math.sqrt(standardDev[i]/iterations); 
		}
	}
	final public double meanValue(int varIndex){return meanValues[varIndex];};
	final public double standardDev(int varIndex){return standardDev[varIndex];};
	

	public String CVSheader(){
		String ret = "";
		if(numVars > 0) ret = "E["+ varNames[0] +"], "+ "Var["+ varNames[0] +"]"; 
		for(int i=1; i<numVars; i++){
			ret += ", E["+ varNames[i] +"], "+ "Var["+ varNames[i] +"]";
		}
		ret += "\n";
		return ret;
	}
	
	
	/**
	 * @param d  Number of decimals
	 */
	public String CVSoutput(int d, String [] controlVals){
		return toString(d, ",", controlVals);
	}
	public String toString(){
		return toString(2, "\t", null);
	}

	public String toString(int d, String separator, String [] controlVals){
		String ret = "";
       // int dd;
       // boolean stdev = true;
        
        ret = controlVals[0];
        for(int i=1; i<controlVals.length; i++) ret += separator + controlVals[i];
        
		for(int i=0; i<numVars; i++){
            ret += separator;
            ret += String.format("%1."+d+"f", meanValues[i]);
            ret += separator+ String.format("%1."+d+"f", standardDev[i]); 
		}
		ret += "";
		return ret;
	}



	public static void main(String[] args) {
	}
}
