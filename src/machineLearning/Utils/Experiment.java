package machineLearning.Utils;

//import GSMN;

import java.util.Random;
import java.util.Scanner;

import machineLearning.Datasets.Statistics;
;

//First numIndepVars variables are independent and their values can be set.
abstract public class Experiment {
	protected String ctrlVarNames [], outputVarNames [];
	protected String [] controlVals; 
	protected int repetition=0;
    protected int numRepetitions=1;
	//public static boolean fastRun;
	public static boolean singleRun;
	
//	protected boolean screenOutput;

	static protected Random r;
	static public Output output;
	//protected boolean debugMode = true;
	
	//Parameters
	/*static final public double threshold(){return threshold;};
    static protected double threshold;
	static final public String testType(){return testType;};
    static protected String testType;*/
    
	//------------------------------------------------------------------
	public Experiment(Random r, String args [], String [] ctrlVarNames, String [] outputVarNames, int numRepetitions){
		
		this.r = r;
		
		this.ctrlVarNames = new String[ctrlVarNames.length];
        this.outputVarNames = new String[outputVarNames.length];
		System.arraycopy(ctrlVarNames, 0, this.ctrlVarNames, 0,  ctrlVarNames.length);
        System.arraycopy(outputVarNames, 0, this.outputVarNames, 0,  outputVarNames.length);
        
        controlVals = new String[ctrlVarNames.length];
        
        this.numRepetitions = numRepetitions;
        controlVals = processCommandLine(args);
        
        //output = new Output(logFileName);
		//output.addVerboseLevel("MAINLOOP");

    	//output = new Output(logFile);
    
    	Experiment.output.clearLogFile();
		Experiment.output.setVerboseLevel(
				//coVL.LOG 
				 coVL.MAINLOOP 
					//| coVL.MEMORY_USAGE
					| coVL.GSMN
					| coVL.EXPERIMENT
				//	| coVL.ACCURACY_EVAL
				//| coVL.ALL_TRIPLETS 
				//| coVL.INFERENCE
				//| coVL.DOTESTS 
				 //coVL.DEFEATERS
				//| coVL.COMPUTE_ACCEPTED
				//| coVL.DEFENDED_BY
				//| coVL.ATTACKERS
				//coVL.PREFERENCE
				//| coVL.PROOF
		)	;
	}
	//------------------------------------------------------------------
	public static void setGlobals(Random r){
		Experiment.r = r;
		
	}
	//------------------------------------------------------------------
	/*public void setDebugModeOff(){
		debugMode = false;
		output.silentMode = true;
		
	}
	public void setDebugModeOn(){
		debugMode = true;
		output.silentMode = false;
	}
	*///	------------------------------------------------------------------
	//double [] NCHIVV; //non-changing independent variables values
	abstract protected double [] getValuesForIteration(String [] controlVars);
//	------------------------------------------------------------------
	//double [] NCHIVV; //non-changing independent variables values
	//abstract protected String getVariablesValueString();
	//abstract protected String editOutcomeString(String string);
	
//	--------------------------------------------------------------------
	  //Separates a space separated input into an Strings[]
	  static public  java.util.ArrayList<String> processMultiOptions(String option){
		  
		  java.util.ArrayList<String> ret = new java.util.ArrayList<String>();
		  
		  //Strip quotes
		  if(option.charAt(0) == '\"'){
			  option = option.substring(1,option.length());
		  }
		  if(option.charAt(option.length()-1) == '\"'){
			  option = option.substring(0,option.length()-1);
		  }
		  Scanner sc = new Scanner(option);
		  while(sc.hasNext()){
			  ret.add(sc.next());
		  }
		  
/*		  int j=option.indexOf("\"")==-1?0:1;
		  
		  int k = option.indexOf(" ");
		  while(k != -1){
			  ret.add(option.substring(j,k));
			  j = k+1;
			  k = option.indexOf(" ",k+1);
		  }
		  k = option.indexOf("\"", j)==-1?option.length():option.length()-1;
			  
		  ret.add(option.substring(j,k));*/
		  return ret;
	  }
//  ------------------------------------------------------------------
    protected String [] processCommandLine(String [] args){
        String controlVals [ ] = new String[ctrlVarNames.length];
        if(args.length != 0){
            int i=0;
            whileLoop: while(i<ctrlVarNames.length*2){
                if(args[i].equalsIgnoreCase("-header")){
                    System.out.println(CVSheader());
                    System.exit(1);
                }
                for(int j=0; j<ctrlVarNames.length; j++){
                    if(args[i].equals("-"+ ctrlVarNames[j])){
                        controlVals[j] = args[i+1];
                        if(ctrlVarNames[j].equalsIgnoreCase("rep")) {
                            if(controlVals[j].charAt(0) == 'N' && controlVals[j].charAt(1) == 'r') {
                                singleRun = true;
                                numRepetitions = 1;
                                repetition = (new Integer(controlVals[j].substring(2))).intValue();
                            }
                            else {
                                singleRun = false;
                                numRepetitions = (new Integer(controlVals[j])).intValue();
                            }
                        }
                        if(ctrlVarNames[j].equalsIgnoreCase("seed")) {
                            r = new Random( (new Integer(args[i+1])).intValue() );
                        }
                        i += 2;
                        continue whileLoop;
                    }
                }
                System.err.println("\n\nInvalid "+ i + "th- input parameter: " + args[i] + ", previous: " + args[i-1] + " next: " + args[i+1]);
                System.exit(1);
            }
        }
        else{
            String out = "USAGE:  1)java Experiment (or subclass) ";
            for(int j=0; j<ctrlVarNames.length; j++){
                out += "-"+ ctrlVarNames[j] + " ";
            }
            out += "\n";
            out +=       "        2)java Experiment (or subclass) -header    To output a header line containing  variable names";
            System.out.println(out);
        }
        return controlVals;
    }    
	//------------------------------------------------------------------
	//if resetFile = true, then the file is emptied and the header is thus saved.
	//Object IVV;	//independent variable's values
	//int IVI; //independent variable's index
//	double [] NCHIVV; //non-changing independent variables values
	public void run(){
		double auxoutcome [] = new double[outputVarNames.length];
		Run run;
		
		boolean print = output.doPrint(VerboseLevels.SIMPLE);
		
		//double outcomesMeans [] = new double[outputVarNames.length];
		//double outcomesVars [] = new double[outputVarNames.length];

		//for(int i=0; i< numValues; i++){ //loop over values IVI-th variables
			run = null;
			run = new Run(outputVarNames.length, numRepetitions);
			//if(fileName != null) Utils.println(fileName, CVSheader(), true, true);
            boolean exception = false;
			for(int j=0; j<numRepetitions; j++){
				if(!singleRun) repetition = j;
                
                try{
                	auxoutcome = getValuesForIteration(controlVals);
                }
                catch(Exception e){
                    e.printStackTrace();
                    for(int i=0; i<outputVarNames.length; i++){
                        auxoutcome[i] = -1;   
                    }
                    run.setMeanAndStdev(auxoutcome);
                    exception = true;
                    break;
                }

				run.addIteration(auxoutcome);
				
				//output
				if(print){
					//System.out.println("Repetition = " + j + " = " + getVariablesValueString());
					System.out.println("Repetition = " + j);
					System.out.println("CONTROLS:");
	                for(int k=0 ;k <ctrlVarNames.length; k++){
	                    System.out.println("\t"+ctrlVarNames[k] + " = " + controlVals[k]);
	                }
	                System.out.println("OUTPUTS:");
	                for(int k=0 ;k <outputVarNames.length; k++){
	                	System.out.println("\t"+outputVarNames[k] + " = " + auxoutcome[k]);
	                }
	                System.out.println("");
				}
			}
            if(!exception){
            	run.normalize();
            }
			
            String aux = "#------------------------------------------------------------------------";
            output.printlnResult(VerboseLevels.SIMPLE | VerboseLevels.LOG,aux);
            output.printResult(  VerboseLevels.SIMPLE | VerboseLevels.LOG,toStringDecorated(run));
			output.printlnResult(VerboseLevels.SIMPLE | VerboseLevels.LOG,run.CVSoutput(3, controlVals));
            output.printlnResult(VerboseLevels.SIMPLE | VerboseLevels.LOG,aux);
		//}
	}
    //------------------------------------------------------------------
	protected String toStringDecorated(Run run){
        String ret= "# " + ctrlVarNames[0] + " = " + controlVals[0];
        for(int k=1 ;k <ctrlVarNames.length; k++){
            ret += "\n# "+ ctrlVarNames[k] + " = " + controlVals[k];
        }
        ret += "\n#...................................."; 
        ret += "\n# " + outputVarNames[0] + " = " + Utils.myDoubleToString(run.meanValue(0),3);
        if(numRepetitions > 1)ret += " +/- " + Utils.myDoubleToString(run.standardDev(0),2);
        for(int k=1 ;k <outputVarNames.length; k++){
            ret += "\n# " + outputVarNames[k] + " = " + Utils.myDoubleToString(run.meanValue(k),3);
            if(numRepetitions > 1) ret += " +/- " + Utils.myDoubleToString(run.standardDev(k),2);
       }
        ret += "\n#...................................."; 
        ret += "\n";
        
		return ret;
    }
	//------------------------------------------------------------------
	protected String CVSheader(){
	   String ret = "# CONTROL\n";//"#" + ctrlVarNames[0];
	   
       for(int j=0; j<ctrlVarNames.length; j++) ret += "#\t "+ (j+1) + ":" + (j+1) + ":" + ctrlVarNames[j] + "\n";
       ret += "#-------------------\n# OUTPUT\n";
       for(int j=0; j<outputVarNames.length; j++) ret += "#\t "+ (ctrlVarNames.length+j+1) + ":"+ (ctrlVarNames.length+1+(j)*(j>0?2:1)) + ":"+ outputVarNames[j] + "\n";

        return ret;
	}
	
}
