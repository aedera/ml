package machineLearning.Utils;

import java.util.Collection;
import java.util.Map;

import java.io.InputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.yaml.snakeyaml.Yaml;

import machineLearning.Datasets.FastDataset;
import machineLearning.Datasets.FastInstance;
import machineLearning.Datasets.Schema;
import machineLearning.Datasets.Attribute;

public class DataSets {
    public static FastDataset getDataSetFrom(String path)
        throws FileNotFoundException {
        Map<String, Object> hash =
            (Map<String, Object>)loadFromStream(path);

        FastDataset dataset = new FastDataset();
        Schema schema = getSchema(hash);
        dataset.schema(getSchema(hash));

        Collection<Collection<Integer>> instances =
            (Collection<Collection<Integer>>) hash.get("dataset");
        for (Collection<Integer> instance : instances) {
            FastInstance inst = new FastInstance(schema, "", instance.size());
            int i = 0;
            for (Integer valueOfVariable : instance)
                inst.setValue(valueOfVariable.intValue(), i++);

            dataset.addInstance(inst);
        }

        return dataset;
    }

    private static Schema getSchema(Map<String, Object> hash) {
        Collection<Integer> variables = (Collection<Integer>)hash.get("variables");
        Schema schema = new Schema();
        // Generate net
        for (int i = 0; i < variables.size(); i++) {
            Attribute attr = new Attribute("" + i, i, 2);
            schema.addAttribute(attr);
        }

        return schema;
    }

    private static Object loadFromStream(String path)
        throws FileNotFoundException {
        InputStream input = new FileInputStream(new File(path));
        Yaml yaml = new Yaml();

        return yaml.load(input);
    }

    /**
     * Generate a random dataset with <code>numVariables</code> variables and
     * size <code>numInstances</code>
     *
     * @todo is supposed variables binaries
     */
    public static FastDataset generateRandom(int numVariables, int numInstances) {
        Schema schema = new Schema();
        FastDataset dataset;

        for (int i = 0; i < numVariables; i++)
            schema.addAttribute(new Attribute("" + i, i, 2));

        dataset = new FastDataset("", schema);

        for (int i = 0; i < numInstances; i++)
            dataset.addInstance(new FastInstance(schema, "", numVariables));

        return dataset;
    }
}