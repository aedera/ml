package machineLearning.Utils;


public class coVL extends VerboseLevels {
	public static final int LEVEL0 = 32;
	public static final int LEVEL1 = 64;
	public static final int LEVEL2 = 128;
	public static final int LEVEL3 = 256;
	public static final int LEVEL4 = 512;

	public static final int ALL_TRIPLETS = 1024;
	//public static final int INFERENCE = 2048;
	public static final int DOTESTS = 4096;
	public static final int DEFEATERS = 8192;
	public static final int DEFENDED_BY = 8192*2;
	public static final int ATTACKERS = 8192*4;
	public static final int COMPUTE_ACCEPTED = 8192*8;
	public static final int PREFERENCE = 8192*16;
	public static final int PROOF = 8192*32;
	public static final int MEMORY_USAGE = 8192*64;
	//public static final int ACCURACY_DIFF = 8192*128;

	
	public static final int MAINLOOP = LEVEL2;
	public static final int INFERENCE = LEVEL3;
	public static final int GSMN = LEVEL2;
	public static final int GSIMN = LEVEL2;
	public static final int EXPERIMENT = LEVEL2;
	public static final int ACCURACY_DIFF = LEVEL2;
	public static final int ACCURACY_EVAL = LEVEL3;
	//public static final int  = 8192*16;

	
}
