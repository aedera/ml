package machineLearning.Utils;

import java.util.*;
import java.io.*;

import java.lang.reflect.*;


/**
 *
 * <p>Title: Utility class with some static useful methods.</p>
 * <p>Description: </p>
 * @author Facundo Bromberg
 * @version 1.0
 */
public class Utils
{
  public static int d;

  public static boolean _ASSERT;
  public static boolean _SCREEN_OUTPUT;
  public static boolean _FILE_OUTPUT;



  // In Microsoft's J++, the console window can close up before you get a chance to read it,
  // so this method can be used to wait until you're ready to proceed.
  public static void waitHere(String msg)
  {
    System.out.println("");
    System.out.print(msg);
    try { System.in.read(); }
    catch(Exception e) {} // Ignore any errors while reading.
  }

  public static synchronized boolean writeStringToFile(String contents)
  {
        return writeStringToFile(contents, null, true, true);
  }
  // Note: this does NOT add a final "linefeed" to the file.  Might not want to
  // get that upon a subsequent "read" of the file.
  public static synchronized boolean writeStringToFile(String contents, String fileName)
  {
    return writeStringToFile(contents, fileName, true, true);
  }
 // static final short SCREEN = 1;
  //static final short FILE = 2;
 // static final short BOTH = 4;

  public static synchronized BufferedWriter getFileWriter(String fileName, boolean append){
        BufferedWriter writer = null;
        try {
                writer = new BufferedWriter(new FileWriter(fileName));
        }
    catch(Exception ioe)
    {
      System.out.println("Exception writing to " + fileName + ".  Error msg: " + ioe);
      return null;
    }

    return writer;
  }


  /**
   * If fileName == null, it outputs to the screen.
   * @param contents
   * @param fileName
   * @param append
   * @return
   */
  public static synchronized boolean writeStringToFile(String contents, String fileName, boolean append, boolean output2screen)
  {
        if(fileName == null || output2screen) System.out.print(contents);
        //if(output2screen) System.out.print(contents);
    if( fileName == null) return false;

        BufferedWriter writer = null;
        try {
          writer = new BufferedWriter(new FileWriter(fileName, append));
          writer.write(contents, 0, contents.length());
      writer.close();
      return true;
    }
    catch(Exception ioe)
    {
      System.out.println("Exception writing to " + fileName + ".  Error msg: " + ioe);
      return false;
    }
  }

  static class IntegerDoublePair implements Comparable<IntegerDoublePair>{
          int int_;
          double double_;
          public int compareTo(IntegerDoublePair x){
                  int aux = (int)((double_ - x.double_)*10E12);
                  return aux;
          };
  }

  /**
   * weights is overwritten with weights in proper order
   * Sorts in ASCENDING ORDER
   */
  public static int [] sortIntArray(double [] weights){
          int length = weights.length;
          IntegerDoublePair x [] = new IntegerDoublePair[length];

          for(int i=0; i<length; i++){
                  x[i] = new IntegerDoublePair();
                  x[i].int_ = i;
                  x[i].double_ = weights[i];
          }
          Arrays.sort(x);

          int ints [] = new int[length];
          //double newWeights[] = new double[length];
          for(int i=0; i<length; i++) {
                  ints[i] = x[i].int_;
                  weights[i] = x[i].double_;
          }

          return ints;
  }

  public static int [] randomIntArray(int length, Random r){
          double x [] = new double[length];
          for(int i=0; i<length; i++){
                  x[i] = r.nextDouble();
          }
          return sortIntArray(x);
/*
          for(int i=0; i<length; i++){
                  x[i] = new IntegerDoublePair();
                  x[i].int_ = i;
                  x[i].double_ = r.nextDouble();
          }
          Arrays.sort(x);

          int ints [] = new int[length];
          for(int i=0; i<length; i++) ints[i] = x[i].int_;

          return ints;*/
  }

  public static double[] randomDoubleArray(int length, final Random r){
          double x [] = new double[length];

          for(int i=0; i<length; i++) x[i] = r.nextDouble();

          return x;
  }

  public static void emptyFile(String fileName){
        writeStringToFile("", fileName, false, false);
  }
/*  public static void println(String filename, String content){
        if(!_FILE_OUTPUT) return;
        writeStringToFile(content + "\n", filename, true, _SCREEN_OUTPUT);
  }
  public static void println(String filename, String content, boolean screenOutput, boolean fileOutput){

        if(!fileOutput) return;
        writeStringToFile(content + "\n", filename, true, screenOutput);
  }
  public static void print(String filename, String content){
        if(!_FILE_OUTPUT) return;
        writeStringToFile(content, filename, true, _SCREEN_OUTPUT);
  }
  public static void print(String filename, String content, boolean screenOutput, boolean fileOutput){

                if(!fileOutput) return;
                writeStringToFile(content, filename, true, screenOutput);
          }
*/
  /**
   * MyParseInt is similar to the Integer.parseInt but return Integer.MIN_VALUE in case of an error (like if the input is not a well formed integer string). It is used for non-numerical attribute values (e.g. "Tiny")
   * @author Facundo Bromberg
   */
  public static int myParseInt(String value)
  {
      int intValue = -1;

      try{ intValue = Integer.parseInt(value); }
      catch(Exception intE)
      {
          NumberFormatException nfe = new NumberFormatException();
          Class exceptionClass = intE.getClass();
          Class numberClass = nfe.getClass();
          String className = exceptionClass.getName();
          if(className.equalsIgnoreCase(numberClass.getName()))
          {
              intValue = Integer.MIN_VALUE;
          }
      }
      return intValue;
  }
  /**
     * MyParseDouble is similar to the Double.parseDouble but return Double.MIN_VALUE in case of an error (like if the input is not a well formed integer string). It is used for non-numerical attribute values (e.g. "Tiny")
     * @author Facundo Bromberg
     */
    public static double myParseDouble(String value)
    {
        double doubleValue = -1;

        try{ doubleValue = Double.parseDouble(value); }
        catch(Exception intE)
        {
            NumberFormatException nfe = new NumberFormatException();
            Class exceptionClass = intE.getClass();
            Class numberClass = nfe.getClass();
            String className = exceptionClass.getName();
            if(className.equalsIgnoreCase(numberClass.getName()))
            {
                doubleValue = Double.MIN_VALUE;
            }
        }
        return doubleValue;
    }
    /**
     * standarizedDoubleString receives a String. If the String parses a number, then it returns a standarized form of that number
     * @author Facundo Bromberg
     */
    public static String standarizedDoubleString(String str)
    {
        double auxD = Utils.myParseDouble(str);
        if(auxD != Double.MIN_VALUE) return Double.toString(auxD);
        else return str;
    }

    /**
     * returns a String representing the input double with "dec" decimal places.
     */
    public static String myDoubleToString(double input, int dec)
    {
        if (input == Double.MAX_VALUE) return "Infinity";
        String aux = "#####0.0";
        for(int i=1; i<dec; i++) aux += "0";
        Locale.setDefault(Locale.US);
        java.text.DecimalFormat df2 = new java.text.DecimalFormat(aux);
        return df2.format(input);
    }

    public static Object myNew(Object obj)
    {
      Object newobj = null;
      try {
        newobj = obj.getClass().newInstance();
      }
      catch (IllegalAccessException ex) {
      }
      catch (InstantiationException ex) {
      }

      return newobj;
/*      Class cls = obj.getClass();
      Constructor con[];

      con = cls.getDeclaredConstructors();
      Class param[] = con[0].getParameterTypes();
      Object paramValues[] = new Object[param.length];

      for(int x=0; i<con.length; i++){
        if(! Param[x].isPrimitive()){
          paramValues[x] = param[x].newInstance();
        }
      }*/
    }

    static public long factorial(int n){
        long ret = 1;
        for(long i=1; i<=n; i++) {
                ret *= i;
        }

        return ret;
    }
    static public double choose(int n, int m){
        //int aux = factorial(4);
        double ret = 1;
        if(Utils._ASSERT && (n<m || n+1<n)) {
                        System.out.println("Invalid Utils.choose arguments");
                        (new Exception()).printStackTrace();
                        System.exit(1);
        }
        for(int i=n-m+1; i<=n; i++) {
                ret *= i;
        }
        double aux = (double) factorial(m);
        ret /= aux;

        return ret;
    }

    /**
     * This was obtained from the Stirling's approximation log(n!) = n.log(n) - n
     * @param n
     * @param m
     * @return
     */
    static public double logChoose(double n, double m){
        if(m == n || m ==0) return 0;
        double ret = n*Math.log(n/(n-m)) + m*Math.log((n-m)/m);
        return ret;
    }

    /**
     * Returns the probability of m "heads" in a binomial of n coin tosses with head bias p.
     */
    static public double binomial(int n, double p, int m){
        return Math.exp(Utils.logChoose(n, m)) * Math.pow(p,m)* Math.pow(1-p, n-m);
    }
    static public boolean FORCED_ASSERT(boolean test, String message){
        //System.out.println("FORCED ASSERTED");
        boolean ret2 = _ASSERT;
        _ASSERT = true;
        boolean ret = ASSERT(test, message);
        _ASSERT = ret2;

        return ret;
    }
    static public void todo(){
        FORCED_ASSERT(false, "Don't know what, but something is wrong here or has to be changed." );
    }
    static public void todo(String message){
        FORCED_ASSERT(false, message);
    }
    static public boolean ASSERT(boolean test, String message){
        if(_ASSERT && !test){
                System.out.println(message);
                (new Exception()).printStackTrace();
                //System.exit(0);
                return false;
        }
        return true;
    }
    static public double log2(double x){
     return Math.log10(x)/Math.log10(2.0);
    }

    static public double max(double a, double b){return  a>b?a:b;};
    static public double min(double a, double b){return  a<b?a:b;};

    /** given an array of containing the log of the values, returns the array of values normalized. */
        //-----------------------------------------------------
        static final public double [] normalizeLogArray(double [] logArray)
        {

                double [] array = new double[logArray.length];
                double sum = 0;

                //Find minimum logP
                double max = -Double.MAX_VALUE;
                for(double log_d : logArray){
                        if(log_d > max) max = log_d;
                }
                //Now normalize temporarily using largest one and exponentiate
                sum=0;
                for(int i=0; i<logArray.length; i++){
            if(logArray[i] - max > Math.log(Double.MAX_VALUE)) array[i] = Double.MAX_VALUE;///N;
                        else array[i] = Math.exp(logArray[i] - max);

            sum += array[i];
                }

                for(int i=0; i<array.length; i++){
                if(sum == 0) array[i] = 0.0 ; //1.0/N;
                        else array[i] /=  sum;
                }


                //TO CHECK IF NORAMLIZEATION IS CORRECT
                sum = 0;
                for(double d : array) sum += d;

                return array;
        }

        //-----------------------------------------------------
        /** Returns the maximum in the array. */
        static public final int indexOfmax(double [] array){
                int index = 0;
                double max = -Double.MAX_VALUE;
                for(int i=0; i<array.length; i++){
                //to compute class with maximum probMass
                if(array[i] >= max){
                        max = array[i];
                        index = i;
                }
                }
                return index;
        }

    public static boolean isEven(int num) { return (num / 2 * 2 == num); }
    public static double dotProduct(double[] vector1, double[] vector2) {
	double summation = 0;
	for (int i = 0; i < vector1.length; i++)
	    summation = vector1[i] * vector2[i];
	return summation;
    }

    public static int getNumVariablesFromFilename(String filename) {
        java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("_n(\\d+)_");
        java.util.regex.Matcher matcher = pattern.matcher(filename);
        boolean matchFound = matcher.find();
	int nNodes = -1;

        if (matchFound)
            nNodes = new Integer(matcher.group(1)).intValue();

	return nNodes;
    }

    public static int getTauFromFilename(String filename) {
        java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("_t(\\d+)_");
        java.util.regex.Matcher matcher = pattern.matcher(filename);
        boolean matchFound = matcher.find();
	int tau = -1;

        if (matchFound)
            tau = new Integer(matcher.group(1)).intValue();

	return tau;
    }

    public static int getEpsilonFromFilename(String filename) {
        java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("_E(\\d+)_");
        java.util.regex.Matcher matcher = pattern.matcher(filename);
        boolean matchFound = matcher.find();
	int epsilon = -1;

        if (matchFound)
            epsilon = new Integer(matcher.group(1)).intValue();

	return epsilon;
    }

    public static String datasetName2ParamsName(String filename) {
	return toParamsFileName(getNumVariablesFromFilename(filename),
				getTauFromFilename(filename),
				getEpsilonFromFilename(filename));
    }

    public static String toParamsFileName(int n, int tau, int epsilon) {
	return "parameters_n"+n+"_t"+tau+"_E"+epsilon+".csv";
    }
}
