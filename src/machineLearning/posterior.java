package machineLearning;
import java.util.Vector;
import java.util.Random;
import java.util.Date;
import java.util.BitSet;

//import sun.security.krb5.internal.crypto.w;
import machineLearning.Utils.*;
import machineLearning.independenceBased.*;
import machineLearning.Graphs.*;
import machineLearning.Graphs.GraphsSimple.*;
import machineLearning.Datasets.*;
import machineLearning.DataStructures.*;

/**
 * @author bromberg
 * 
 */
public class posterior {

	Experiment2 exp;

	boolean saveInitialParticles = false;

	boolean readInitialParticles = false;

	GSOnDemand gs;

	boolean GS, exhaustive, PT; // PT=particleTests

	Vector allTests;

	static final int MINIMIZE = 0;

	static final int MAXIMIZE = 1;

	static final int I = IndependenceTestMargaritisUAI2001.I;

	static final int D = IndependenceTestMargaritisUAI2001.D;

	Random r;

	UGraphSimple trueNetSimple;

	structuresParticlesFilter s;//, s_minus1;

	// testsParticlesFilter fringe;
	testOptimization to;

	// Settings
	double e, alpha, beta, tau, eap, deltaStructures, deltaTests, b1, b2,
			epsilon, sigma;

	int alphaMax;

	int n, N, M, K; // M window for computing entropy's variance in move step
	
	double pi; //Degeneracy factor (minimum ratio of moves that must be accepted.

	int deltaD;

	int burnin, sample_iid_intervalS, sample_iid_intervalT;

	double gamma; //log_likelihood of exact case.
	
	// int move_iid_interval;
	// double C, T0, Tf; //simmulated annealing params

	double prior;

	String comment;

	int optimization;

	FastDataset data = null, allData = null;

	int currentTest = 0;

	double p_max;

	long runningTime = 0;

	// boolean speedUp;

	Statistics stats;

	boolean random_dl;

	// String entropyVsTFile = null;

	double avg = 0;

	// ----------------------------------------------------------------
	public posterior(int n) {
		this.n = n;

		doMemoryAllocation(n);
	}

	// ----------------------------------------------------------------
	public posterior(Experiment2 e) {
		this.exp = e;
	}

	// -----------------------------------------------------------------
	// MUST SET PARAMETERS PRIOR TO CALLING THIS PROCEDURE
	public void doMemoryAllocation(int n) {
		this.n = n;

		if (data == null)
			gs = new GSOnDemand(n, tau, r, this);
		else
			gs = new GSOnDemand(data, this);

		s = new structuresParticlesFilter(this);

		// Get all tests for size n
		Vector subsets;
		gsTriplet aux;

		to = new testOptimization(this);

		if (exhaustive) {
			exp.output.println(coVL.MAINLOOP | coVL.LOG, "GENERATING ALL TESTS");
			allTests = new Vector();
			FastSet V = new FastSet(new Set(gs.V), n);
			FastSet auxSet;
			for (int i = 0; i < n; i++) {
				for (int j = i + 1; j < n; j++) {
					// if(i==j) continue;

					auxSet = new FastSet(V);
					auxSet.remove(i);
					auxSet.remove(j);

					subsets = auxSet.getAllSubsets(V);
					for (int d = 0; d < subsets.size(); d++) {
						aux = new gsTriplet(i, j, (FastSet) subsets
								.elementAt(d), true);
						allTests.addElement(aux);
					}
				}
			}
		}
	}

	// --------------------------------------------------------------------
	public String toString() {
		String ret = "";

		ret += new Date(System.currentTimeMillis()).toString();
		ret += "\n";
		ret += "NOTE: " + comment + "\n";
		ret += "----" + "\n";
		ret += "\n";
		ret += "GS? = " + GS + "\n";
		ret += "Exhaustive search? = " + exhaustive + "\n";
		ret += "epsilon (permeability) = " + e + "\n";
		ret += "alpha (proposal's prob of flip) = " + alpha + "\n";
		ret += "AlphaMax (complexity of in-test-class proposal) = " + alphaMax
				+ "\n";
		ret += "beta (cost exponent) = " + beta + "\n";
		ret += "epsilon = " + epsilon + "\n"; // cutoff for termination
												// epsilon * log(N)
		ret += "deltaStructures (maximum s.v.) = " + deltaStructures + "\n";
		ret += "deltaTests = " + deltaTests + "\n";
		ret += "b1 (Pr of alpha moves) = " + b1 + "\n";
		ret += "b2 (Pr of in-tests-class moves) = " + b2 + "\n";
		ret += "sigma (stdev for in-tests-class moves) = " + sigma + "\n";
		ret += "tau = " + tau + "\n";
		ret += "prior = " + prior + "\n";
		ret += "\n";
		ret += "n (number of variables in the domain) = " + n + "\n";
		// ret += "NperC (number of particles per connectivity) = " + NperC +
		// "\n";
		// ret += "Nc (number of connectivities) = " + Nc + "\n";
		ret += "N (number of particles ) = " + N + "\n";
		ret += "K (beam width) = " + K + "\n";
		ret += "\nSimmulated annealing params: +\n";
		ret += "\n";
		ret += "enforced allowed percentage = " + eap + "\n";
		ret += "burnin = " + burnin + "\n";
		ret += "sample iid intervalS = " + sample_iid_intervalS + "\n";
		ret += "sample iid intervalT = " + sample_iid_intervalT + "\n";
		// ret += "move_iid_interval = " + move_iid_interval + "\n";
		if (optimization == posterior.MINIMIZE)
			ret += "optimization = MINIMIZE \n";
		if (optimization == posterior.MAXIMIZE)
			ret += "optimization = MAXIMIZE \n";

		return ret;
	}

	// -----------------------------------------------------------------
	static public double average(int[] vector) {
		double sum = 0;
		for (int i = 0; i < vector.length; i++) {
			sum += (double) vector[i];
		}
		return sum / vector.length;
	}

	// -----------------------------------------------------------------
	final public double[] normalize(double[] p) {
		double sum = 0;
		double[] w = new double[p.length];

		for (int i = 0; i < w.length; i++) {
			// sum += ((Double) weights.elementAt(i)).doubleValue();
			sum += p[i];
		}
		if (sum == 0) {
			for (int i = 0; i < w.length; i++) {
				// w[i] = ((Double) weights.elementAt(i)).doubleValue() / sum;
				w[i] = 1.0 / (double) w.length;
			}
		} else {
			for (int i = 0; i < w.length; i++) {
				w[i] = p[i] / sum;
			}
		}
		return w;
	}
	// -----------------------------------------------------------------
	/**
	 * Returns true if x and xp are in the same equivalence tests-class for set
	 * of tests T of cardinality t
	 */
	//
	public static boolean areEquivalent(Particle sp1,
			Particle sp2, gsTriplet[] T, int t) {
		for (int i = 0; i < t; i++) {
			gsTriplet y = T[i];
			boolean yval_x = sp1.separates(y.X(), y.Y(), i);
			boolean yval_xNew = sp2.separates(y.X(), y.Y(), i);
			if (yval_x != yval_xNew) {
				return false;
			}
		}
		return true;
	}

	// -----------------------------------------------------------------
	double[] log_dl = new double[2]; // probability of independence
	public UGraphSimple mainLoop() {
		double H = 0;
		gsTriplet y_min;
		int y_min_index;

		runningTime = System.currentTimeMillis();

		boolean print = exp.output.doPrint(coVL.MAINLOOP);
		//print = false;
		
		//if (data == null) trueNetSimple = new UGraphSimple(gs.trueNet);

		if(print) System.out.println(toString());

		s.init(burnin, sample_iid_intervalS);

		if(print) System.out.println(new Date(System.currentTimeMillis())
				.toString());

		if(print) {
			System.out.println("Before everything");
			s.printStatus();
		}

		int numIncorrect=0;
		mainLoop: while (true) {
			
			if(print) System.out.print("*");
			//s_minus1 = null;
			//s_minus1 = new structuresParticlesFilter(s);

			if(print) System.out.println(
							"\n-------------------------------------------------------------------------------\n");

			y_min = null;
			int K=0;
			while(y_min == null){
				/** SEARCH OPTIMAL TEST * */
				if(print) System.out.println("Searching optimal test ...");
				if (false || exhaustive)
					y_min = to.getOptimalTestsBruteForce();
				else
					//y_min = to.getOptimalTestGreedy();
					y_min = to.getOptimalTestDeterministic();
	
				if((true || K++ >= 10) && y_min == null) {
					if(print) System.out.println("\nOutput due to NULL test.");
					break mainLoop;
				}
			}			
			y_min_index = testIndex(y_min);

			Utils.FORCED_ASSERT(y_min != null, "Optimal test resulted null.");
			if (!Utils.FORCED_ASSERT(!y_min.getCond().isMember(y_min.X())	&& !y_min.getCond().isMember(y_min.Y()) && y_min.X() != y_min.Y(),
					"Cond set contains X or Y.")) {
				int asdas = 0;
			}

			//if(print) s.printStatus();

			/** DO TEST * */
			if (y_min_index == s.t) {// only do test if its new.
				//log_dl = gs.doTest(y_min, y_min_index, stats);
				gs.weightedTestsCost += 2+y_min.getCond().cardinality();
				gs.testsCost ++;

				if(data == null) {
					if(trueNetSimple.separates_nonRecursive(y_min.X(), y_min.Y(), y_min.getCond())) {
						log_dl[IndependenceTest.I] = -Math.log(e);
						log_dl[IndependenceTest.D] = Math.log(e);
					}
					else {
						log_dl[IndependenceTest.D] = -Math.log(e);
						log_dl[IndependenceTest.I] = Math.log(e);
					}
				}
				else {
					//double priorI = s.testPosterior(y_min, true);
					double priorI = 0.5;
					BayesianTest IT = new BayesianTest(data, exp.IT.trueModel(), priorI);
					log_dl = IT.log_likelihoods(new TripletSets(y_min.X(), y_min.Y(), y_min.getCond()));
				}
			}
			/*if(false && log_dl[posterior.I] > log_dl[posterior.D]) {
				double pI = .75;
				double alpha = prior*(1.0-pI) / (pI*(1.0-prior));
				log_dl[posterior.D] = Math.log(alpha)+log_dl[posterior.I];    
			}**/

			/** POST PROCESS TEST * */
			//if(s.t > 2) s.printStatus();
			s.recordTestData(y_min, y_min_index, log_dl);

			/** OUTPUT * */
			if( false || print){
				System.out.print("\nTESTING " + y_min);
				double prob_i = IndependenceTestMargaritisUAI2001.conditional_P(log_dl, 0.5);
				String probI = Utils.myDoubleToString(prob_i, 10);
				//String probD = Utils.myDoubleToString(1.0-IndependenceTestMargaritisUAI2001.conditional_P(log_dl, 0.5), 2);
				//String expecProbTrue2 = Utils.myDoubleToString(s.candidateTestProbability(y_min.X(), y_min.Y(), y_min.getCond(), true), 10);
				//String expecProbFalse = Utils.myDoubleToString(s.candidateTestProbability(y_min.X(), y_min.Y(), y_min.getCond(), false), 2);
				String ITProbTrue = Utils.myDoubleToString(Math.exp(exp.IT.independent(new Triplet(y_min.X(), y_min.Y(), y_min.getCond())).logpvalue()),2);
				//String ITProbFalse = Utils.myDoubleToString(1.0-Math.exp(exp.IT.independent(new Triplet(y_min.X(), y_min.Y(), y_min.getCond())).logpvalue()),2);

				//System.out.print("  = " + probI + "   |   " + ITProbTrue + " |  " + expecProbTrue);
				boolean trueNetSimpleIndependent = trueNetSimple!=null?trueNetSimple.separates_nonRecursive2(y_min.X(), y_min.Y(), y_min.getCond()):true;
				boolean correct = ((prob_i >= 0.5) == trueNetSimpleIndependent);
				if(!correct) numIncorrect++; 
				//System.out.print("  = " + probI + (correct?"  CORRECT":"  INCORRECT")+ "(" + numIncorrect + ")" +  "|  " + expecProbTrue);
				System.out.print("  = " + probI + (correct?"  CORRECT":"  INCORRECT")+ "(" + numIncorrect + ")" +  "|  " + s.reliability(y_min));
				
			}
			//if(print) System.out.print("        ESTIMATED ");
			if(print) System.out.print("    |   t = " + (s.t) + "  T = "
							+ gs.testsCost + ",  WT = " + gs.weightedTestsCost
							+ "\n\n");

			/** UPDATE POSTERIOR */
			//double h_prev = s.computeEmpiricalEntropy();
			
			//s.reCreateClasses();
			
			if(print && true) {
				System.out.println("Before everything");
				s.printStatus();
			}

			s.recomputeWeights();
			if(false && print) {
				System.out.println("after recomputing");
				s.printStatus();
			}
			/** UPDATE RESAMPLE * */
			s.resample();
			if(print && true) {
				System.out.println("after resampling, before moves. ");
				s.printStatus();
			}
			//s.classes.addParticle(trueS);
			/** PF MOVES * */
			s.move(M);
			/** UPDATE RESAMPLE * */
			if(print) {
				System.out.println("After moves");
				s.printStatus();
			}
			
			if(print) System.out.print("    |   t = " + (s.t) + "  T = "
					+ gs.testsCost + ",  WT = " + gs.weightedTestsCost
					+ "\n\n");

			/** TEST TERMINATION * */
			if(s.t == 26){
				int asdfsa=0;
			}
			if(s.t == 7){
				int asdfsad=0;
			}
			if(s.terminate()) {
				s.terminate();
				if(print) System.out.println("\nOutput due to termination condition.");
				break;
			}

			//s.normalizeLogPosteriors();
			//s.reCreateClasses();
			
			
		}

		// System.out.println("distinct = "+ distinct / ((double)s.t));

		/** FIND MOST PROBABLE * */
		UGraphSimple x = new UGraphSimple(s.classes.mostProbableParticle());

		if (data == null)
			if(print) System.out.println("Output's hamming: "
					+ x.hamming(trueNetSimple));
		if(print) System.out.println("OUTPUT NETWORK: \n" + x.toString());
		if (data == null)
			if(print) System.out.println("TRUE NETWORK: \n"
					+ trueNetSimple.toString());
		if(print) System.out.println(new Date(System.currentTimeMillis())
				.toString());

		s.printStatus();
		if(print) System.out.println(new Date(System.currentTimeMillis())
				.toString());

		runningTime = (System.currentTimeMillis() - runningTime) / 1000;

		return x;
	}

	// ------------------------------------------------------------------------------------------
	// -----------------------------------------------------------------
	public void initParameters() {

		N = 80;
		n = -1;
		tau = -1;
		prior = .5;

		alpha = 0.05;

		exhaustive = false;
		GS = false;
		PT = false;

		e = .0000001; // permeability;
		beta = 0.02;// 0.005; //cost penalty

		burnin = 2000;
		sample_iid_intervalS = 500;
		sample_iid_intervalT = 80;

		M = 20;
		b1 = 0.05;
		b2 = .95 * 1.0;

		deltaD = 100; // increment of data chunk

		epsilon = 0.5;

		comment = "";

		random_dl = false;

		optimization = posterior.MINIMIZE;

		alphaMax = 1;

		sigma = 13.1;

		deltaStructures = 0.2;// 0.03;
		deltaTests = 0.00005;
		;// .3;hp

		saveInitialParticles = false;
		readInitialParticles = false;
	}

	// -----------------------------------------------------------------

	/**
	 * Returns s.t if test has not been done yet.
	 */
	public int testIndex(Triplet test) {
		// FIXME Make more efficient:
		// IDEA: Come up with a way to compute a hash code for each test and
		// store
		// those tests done in a hashtable.

		int i = 0;
		for (i = 0; i < s.t; i++) {
			if (s.tests[i].isEqual(test)) {
				break;
			}
		}
		return i;
	}

	// -----------------------------------------------------------------
	public final double testScore(int X, int Y, FastSet S, double[] alpha, double[] infogain,
																					structuresParticlesFilter s) {
		return testScore(X, Y, S, alpha, infogain, s,null);
	}

	public final double testScore(int X, int Y,
			FastSet S, double[] alpha, double[] infogain,
			structuresParticlesFilter s, java.util.BitSet testString) {

		if (s == null)
			return 1.0;

		/** Check if test has been done already * */
		Triplet test = new Triplet(X, Y, S);

		int i = testIndex(test);

		double h;
		if(false){
			if (i == s.t)
				h = s.infoGain(X, Y, S, alpha, testString); // NEW TEST
			else {
				Utils.FORCED_ASSERT(false, "Oops, remove comment and fix infoGain");//h = s.infoGain(i, alpha, testString);
			}
		}
		h = s.infoGain(X, Y, S, alpha, testString); // NEW TEST
		
		double cost = 2 + S.cardinality();
		double prob;

		if (infogain != null)
			infogain[0] = h;
		// p.beta = (p.beta == -1)? cost: p.beta;
		if (true) {
			// h_y = s.maxEntropy_Gain_forCandidate();
			//Utils.FORCED_ASSERT(Math.abs(s.candidateTestProbability(X, Y, S, false) + s.candidateTestProbability(X, Y, S, true)-1)<0.0001, "");
			return h / Math.pow(cost, beta) ;//* Math.pow(Math.abs(s.candidateTestProbability(X, Y, S, false)-0.5),1);
			/*double aux = Math.log(0.5);
			if(h > 0){
				int asdfasdf=0;
			}
			return h  - 0.5* S.cardinality();*/
		}
		return prob;
	}

	// -----------------------------------------------------------------
	public static void main() {
		posterior post = new posterior(null);
		post.initParameters();
		post.doMemoryAllocation(6);
		post.mainLoop();
	}

}
