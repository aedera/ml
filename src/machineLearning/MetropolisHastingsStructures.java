package machineLearning;

//import machineLearning.Graphs.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.BayesianTest;
import machineLearning.independenceBased.IndependenceTest;
import machineLearning.independenceBased.TripletSets;

/*
 * Created on Oct 24, 2005
 *
 */

/**
 * @author bromberg
 * 
 */
public class MetropolisHastingsStructures {
	Random r; // uniform sampler
	double alpha; // temperature

	boolean wasLastAccepted;
	private UGraphSimple solution;
	private IndependenceTest it;
	private double gsmnHammingDistance;

	public MetropolisHastingsStructures(Random r, double alpha, UGraphSimple solution, IndependenceTest it, double gsmnHammingDistance) {
		this.r = r;
		this.alpha = alpha;
		this.solution = solution;
		this.it = it;
		this.gsmnHammingDistance = gsmnHammingDistance;
	}

	public MetropolisHastingsStructures(Random r, double alpha, structuresParticlesFilter s) {
		this.r = r;
		this.alpha = alpha;
		// this.s = s;
	}

	/**
	 * Iterates M number of "acceptances"
	 */

	// -----------------------------------------------------------------------------------------------
	public UGraphSimple iterate(UGraphSimple x, int M) {
//		int maxHammingDistance = (x.n * (x.n - 1)) / 2;
		double logProbSolution = calculateQualityMeasureFilledBlanket(solution);
		double u;
		List<Integer> hashcodes = new ArrayList<Integer>();
		UGraphSimple xstar = new UGraphSimple(x);
		UGraphSimple aux;

		System.out.println();
		
		/* just initializing the score */
		calculateQualityMeasureFilledBlanket(x);
		hashcodes.add(x.getCRC());
		for (int j = 0; j < M; j++) {
			boolean repeated = true;
			u = r.nextDouble();
			
			while(repeated){
				xstar = proposalUnbiased(x, xstar, r);
				int crc = xstar.getCRC();
				if(!hashcodes.contains(crc)){
					repeated = false;
				}
			}
			
			double acceptance = acceptance(x, xstar);
			if (u < acceptance) {
				// System.out.print(", u = " + u + ", ACCEPTED  ");
				aux = x;
				x = xstar;
				xstar = aux;
			} else {
				// System.out.print(", u = " + u + ", REJECTED  ");
			}
//			System.out.println(j + ", " + solution.hamming(x) / maxHammingDistance * 100 + ", " + gsmnHammingDistance / maxHammingDistance * 100 + ", " + LogProbX + ", " + logProbSolution);
			System.out.println(j + ", " + solution.hamming(x) + ", " + gsmnHammingDistance + ", " + LogProbX + ", " + logProbSolution);

		}
		return x;
	}

	// -----------------------------------------------------------------------------------------------
	double LogProbX, LogProbXStar;
//	double LogProbX2, LogProbXStar2;

	private final double acceptance(UGraphSimple x, UGraphSimple xstar) {
		LogProbX = x.logScore;
		LogProbXStar = calculateQualityMeasureFilledBlanket(xstar,x);
//		double LogProbXStar2 = calculateQualityMeasureFilledBlanket(xstar);
		
		double difference = LogProbXStar - LogProbX;
		double prob = Math.exp(difference);
		return prob;
	}

	// -----------------------------------------------------------------------------------------------

	private double calculateQualityMeasureFilledBlanket(UGraphSimple x) {
		BayesianTest bt = (BayesianTest) it;
		double logscore = 0;
		int n = x.n;
		x.blanketScores = new double[n];
		for (int i = 0; i < n; i++) {
			FastSet s = new FastSet(n);
			for (int j = 0; j < n; j++) {
				if (x.existEdge(i, j)) {
					s.add(j);
				}
			}

			double blanketScore = 0;
			for (int j = 0; j < n; j++) {
				if (i != j) {
					boolean exists = s.isMember(j);
					s.remove(j);
					TripletSets triplet = new TripletSets(i, j, s);
					double[] logLikelihoods = bt.log_likelihoods(triplet);
					boolean isDependent = exists;
					double lastTestScore = logLikelihoods[isDependent ? IndependenceTest.D : IndependenceTest.I];
					blanketScore += lastTestScore;

					if (exists) {
						s.add(j);
					}

				}
			}
			x.blanketScores[i] = blanketScore;
			logscore += blanketScore;
		}
		x.logScore = logscore;
		return logscore;
	}

	private double calculateQualityMeasureFilledBlanket(UGraphSimple x, UGraphSimple oldStructure) {
		BayesianTest bt = (BayesianTest) it;
		double logscore = 0;
		int n = x.n;
//		x.blanketScores = new double[n];
		List<Integer> flippedVariables = getFlippedVariables(x, oldStructure);
		for (int i = 0; i < n; i++) {
			if (flippedVariables.contains(i)) {
				FastSet s = new FastSet(n);
				for (int j = 0; j < n; j++) {
					if (x.existEdge(i, j)) {
						s.add(j);
					}
				}

				double blanketScore = 0;
				for (int j = 0; j < n; j++) {
					if (i != j) {
						boolean exists = s.isMember(j);
						s.remove(j);
						TripletSets triplet = new TripletSets(i, j, s);
						double[] logLikelihoods = bt.log_likelihoods(triplet);
						boolean isDependent = exists;
						double lastTestScore = logLikelihoods[isDependent ? IndependenceTest.D : IndependenceTest.I];
						blanketScore += lastTestScore;

						if (exists) {
							s.add(j);
						}

					}
				}
				x.blanketScores[i] = oldStructure.blanketScores[i];;
				x.blanketScores[i] = blanketScore;
				
				logscore += blanketScore;
			} else {
				logscore += oldStructure.blanketScores[i];
			}
		}
		x.logScore = logscore;
		return logscore;
	}

	private List<Integer> getFlippedVariables(UGraphSimple x, UGraphSimple oldStructure) {
		int n = x.n;
		List<Integer> flippedVariables = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if ((x.adjMatrix[i][j] && oldStructure.adjMatrix[i][j]) || (!x.adjMatrix[i][j] && !oldStructure.adjMatrix[i][j])) {
					;
				} else {
					if(!flippedVariables.contains(i)){
						flippedVariables.add(i);
					}
					if(!flippedVariables.contains(j)){
						flippedVariables.add(j);
					}
				}
			}
		}
		return flippedVariables;
	}

	// -----------------------------------------------------------------------------------------------
	// Changes the incomming structure
	private UGraphSimple proposalUnbiased(UGraphSimple xin, UGraphSimple xout, Random r) {
		double aux;
		int size = xin.size();

		xout.copyContentFrom(xin);

		for (int i = 0; i < size; i++) {
			for (int j = i + 1; j < size; j++) {
				aux = r.nextDouble();
				if (aux <= alpha) {
					xout.flipEdge(i, j);
					// System.out.print(" (" + i + "," + j + ") ");
				}
			}
		}
		return xout;
	}

}