package machineLearning;
//import java.util.*;

//import machineLearning.Utils.Run;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

import machineLearning.Utils.*;
import machineLearning.Utils.pfmnVL;
import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.*;
import machineLearning.Graphs.GraphsSimple.*;
import machineLearning.independenceBased.*;

/*
* Created on Feb 1, 2006
*
*/

/**
* @author bromberg
*
*/
public class TestDataFaithfulness extends Experiment2{
        public Triplet [] generateAllTriplets(int n) {
                int N = n*(n-1)/2 * (int)Math.pow(2, n-2);

                int x, y;
                FastSet S;
                Triplet [] triplets = new Triplet[N];

                int t=0;
                for (x = 0; x < n; x++) {
                        for (y = x + 1; y < n; y++) {
                                for (int card = 0; card <= n-2; card++) {
                                        FastSet V = new FastSet(n);
                                        V.setAlltoOne();
                                        V.remove(x);
                                        V.remove(y);
                                        FastPowerSet ps = new FastPowerSet(V, card, n);

                                        ps.initIterator();
                                        while (ps.next() != null) {
                                                S = ps.current();
                                                triplets[t] = new Triplet(x, y, S);
                                                t++;
                                        }
                                }
                        }
                }
                return triplets;
        }
  //---------------------------------------------------------------------------------
    public void computeValuesForIteration() throws IllegalAccessException{
      boolean print = getIntValue("verbose")==2;

      //MarkovNet xMn = new MarkovNet();
      MarkovNet trueNet = new MarkovNet();
      UGraphSimple trueNetSimple;
      FastDataset data = new FastDataset(getStringValue("dir") +getStringValue("dataset"));

          trueNet.read(data, getStringValue("dir") +getStringValue("dataset"), 5, getIntValue("D"));
      int n=data.schema().size();
      trueNetSimple =  new UGraphSimple(trueNet);

      IT = new BayesianTest(data, trueNetSimple);


      if(print) System.out.println("TRUE NET\n---------\n"+trueNet);

      /** Accuracy */
      VertexSeparation TrueNetTest = new VertexSeparation(new UGraphSimple(trueNetSimple));

      //
      pFact trueFact, testFact;
      Triplet triplets [] = generateAllTriplets(n);
      int numCorrect = 0, numIncorrect=0;
      int minCondSetWrong = Integer.MAX_VALUE;
      double avgCondSetWrong = 0;
      for(int i=0; i<triplets.length; i++){
          trueFact = TrueNetTest.independent(triplets[i]);
          testFact = IT.independent(triplets[i]);

          String str;
          if(trueFact.truth_value==testFact.truth_value ){
                  str = "CORRECT";
                  numCorrect++;
          }
          else{
                  str = "INCORRECT";;
                  numIncorrect++;

                  int card = testFact.getCond().cardinality();
                  if(card < minCondSetWrong) minCondSetWrong = card;
                  avgCondSetWrong += card;
          }

          if(print) System.out.println(testFact.toStringPValue() + " " + str + "(" + numIncorrect + ") " );
      }
      avgCondSetWrong = numIncorrect == 0? 0.0 : avgCondSetWrong/((double)numIncorrect);



          setOutput("|D|", ""+ data.size());
          setOutput("Accuracy", ""+ (((double)numCorrect/ (numCorrect+numIncorrect))));
          setOutput("minCondSetWrong", ""+minCondSetWrong);
          setOutput("avgCondSetWrong", ""+avgCondSetWrong);

    }
//  -----------------------------------------------------------------
    public static void main(String[] args) throws Exception{
        Utils._ASSERT = false;

                TestDataFaithfulness exp = new TestDataFaithfulness();

                exp.addControl(new ParameterSingleValue<String>("dir", "~bromberg/experiments/datasets/sampled/undirected/datasets/", "Folder containing dataset and truenet info."));
                //exp.addControl(new Parameter<String>("dataset", "Dataset/s separated by spaces. If the value is \"artificial\" it runs the exact case.", Parameter.processMultiOptions(T)));
                exp.addControl(new ParameterSingleValue<String>("dataset","sampledData_n6_t1_E100_M500_R1.csv" ,"Dataset/s separated by spaces. If the value is \"artificial\" it runs the exact case."));
                exp.addControl(new ParameterSingleValue<Integer>("D", 1000, "Number of rows to read from dataset. If <= 0 then do exact case."));

                exp.addOutput(new ParameterSingleValue<Double>("Accuracy", -1.0));
                exp.addOutput(new ParameterSingleValue<Double>("n", -1.0));
                exp.addOutput(new ParameterSingleValue<Double>("tau", -1.0));
                exp.addOutput(new ParameterSingleValue<Double>("|D|", -1.0));
                exp.addOutput(new ParameterSingleValue<Double>("E", -1.0));
                exp.addOutput(new ParameterSingleValue<Double>("minCondSetWrong", -1.0));
                exp.addOutput(new ParameterSingleValue<Double>("avgCondSetWrong", -1.0));


                int nA [] = {6};
                int tauA [] = {2};
                int [] DA = {10000, 13000, 15000, 18000, 20000};
                int [] EA = {200};
                for(int n : nA){
                        for(int E : EA){
                                for(int tau : tauA){
                                        for(int D : DA){
                                                String dataset = "sampledData_n"+ n +"_t"+ tau + "_E"+ E+"_M500_R1.csv";
                                                String argsS = "-verbose 2 -seed 1234 " + "-dataset "+ dataset +
                                                                           " -D "+ D +
                                                                           " -dir " + "Z:/experiments/datasets/sampled/undirected/datasets/";

                                                args = argsS.split(" ");

                                        exp.setOutput("n", ""+n);
                                        exp.setOutput("tau", ""+tau);
                                        exp.setOutput("E", ""+E);

                                            exp.readValues(args);

                                                exp.run();

                                        }
                                }
                        }
                }
    }
}

