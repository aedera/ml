package machineLearning.Datasets;

import java.util.*;
import java.io.*;

//import machineLearning.Utils.*;
import stringSerializer.*;

/**
 * Class Dataset
 *
 * Implements the functionality of a dataset with the attributes list
 * and the data itself both contained in abstract representations:
 * Attribute and Instance. The whole structure is inspired both by a
 * personal analysis and design of the functionality of datasets
 * and by knowledge of WEKA environment. But, no line of code was copied
 * from WEKA
 *
 * @author Facundo Bromberg
 * @version 1.1 Actualized for the use in Final Project (BayesNetworks inference) of CS572, AI, Fall 2002.
 * @version 1.2 Splitted in Dataset+Schema for Final Project (BayesNetworks incremental learning) of CS561
 * @version 1.3 Actualized for MAL Oct 2003.<!-- --> Splitted the concept of dataset and schema, now dataset contains a schema but it does not extend it.
 * DB, Spring 2003.
 */
public class Dataset
{
        /** For eficiency reasons, instances is implemented as a linked list.
         * Moreover, it is set public to allow the user an efficient iteration
         * by using the Iterator functionality.
         */
        public String name;

        private Schema schema;

        public Schema schema(){return schema;};
        public void schema(Schema _schema){schema = _schema;};

        public LinkedList instances;

        transient private ListIterator li;

    public Dataset(){;};

//        public Dataset(Schema schema){
  //        name = new String(schema.name);
    //    }
        public Dataset(String _name, Schema schema){
          //super(_name);
          this.schema = schema;
          instances = new LinkedList();
        }

        public Dataset(String _name){
        //super(_name);
        this.schema = null;
        instances = new LinkedList();
        }

        /**
         * Iteration funcionality.<!-- --> This method initiates an iteration over the instances.
         */
        public Instance getFirstInstance()
        {
            li = instances.listIterator(0);
            if(!li.hasNext()) return null;

            return (Instance) li.next();
        }

        /**
         * Returns next instance in iterator.
         */
        public Instance getNextInstance()
        {
            if(!li.hasNext()) return null;

            return (Instance) li.next();
        }

        /**
         * Procedure that returns a String representation of Attributes and Instances.
         */
        public String toString()
        {
            return toString(false, false);
        }

        /**
         * Procedure that returns a String representation of Attributes and Instances. If noHeader is true, then it returns only the instances
         */
        public String toString(boolean noHeader, boolean noInstanceValues)
        {
            String Names = "", Data = "";

            if(!noHeader)
            {
                Names = super.toString();
            }
            if(!noHeader) Data = "\nInstances data: \n";

            if(instances.size() == 0) return Names + "\n" + Data;

            ListIterator li = instances.listIterator(0);
            Instance inst = (Instance) li.next();

            while(true)
            {
                Data = Data + inst.toString(noInstanceValues);
                if(!noInstanceValues)   Data = Data + "\n";
                else Data = Data + ", ";

                if(li.hasNext()) inst = (Instance) li.next();
                else break;
            }

            return Names + Data;
        }

        /**
         * Procedure that returns a String in ARFF format of Attributes and Instances. If noHeader is true, then it returns only the instances
         */
        public String toARFF(boolean onlyData, String separator)
                                                                        {return toARFF(onlyData, separator, null);};
        public String toARFF(boolean onlyData, String separator, String FileName)
        {
                BufferedWriter writer = null;
                if(FileName != null){
                        try {
                                writer = new BufferedWriter(new FileWriter(FileName), 100000);
                        } catch (Exception ioe) {;}
                }


            String Data = "";
                if(!onlyData) Data = schema.toARFF() + "\n";

                System.out.println(schema.toARFF());

                if(instances.size() == 0) return Data;

                //ListIterator li = instances.listIterator(0);
                Instance inst = getFirstInstance();

                if(!onlyData) {
                        Data += "@DATA\n";
                        if(FileName != null) {
                                try{
                                        writer.write(Data, 0, Data.length());
                                        writer.flush();
                                }
                                catch(IOException e){;};
                        }
                }
                int f=0;
                while(inst != null)
                {
                        String str = inst.toARFF(separator) + "\n";
                        if(FileName == null) Data = Data + str;
                        if(f%100==0)System.out.print(f + "(" +str.length() + "):  " + str);
                        f++;
                        if(FileName != null) {
                                try{
                                        writer.write(str, 0, str.length());
                                }
                                catch(IOException e){;};
                        }

                        inst = getNextInstance();
                }

                if(FileName != null) {
                        try{writer.close();}
                        catch(IOException e){;};
                }
                return Data;
        }


        /**
         * Make Copy creates a new dataset but do not create new instances(attributes). Each cell in the instances (attributes) Vector will point to the same instance.
         */
        public Dataset makeCopy()
        {
          return subset(-1, "");
        }
        /**
         * Subset creates a copy of the dataset including only a subset of the instances.<!-- -->
         * More specifically, those that has the index-th attribute equal to "value"
         * If value = -1 , then, it copies all. That is, it returns a copy of whole dataset
         * If value = -2 , then, it doesn't copy any instance. That is, it returns a copy of the dataset
         * but without any instance
         */
        public Dataset subset(int attr_index, String value)
        {
          Dataset newD = (Dataset) new Dataset(name, schema);

          //Copy attributes Vector.
          for(int i=0; i<schema.size(); i++)
          {
            Attribute attr = schema.getAttribute(i);
            newD.schema.addAttribute(attr);
          }
          if(attr_index == -2 || instances.size() == 0) return newD;

          //Copy instances LinkedList
          ListIterator li = instances.listIterator(0);
          Instance aux_inst = (Instance) li.next();


          while(li.hasNext())
          {
              if(attr_index == -1 ) {
                  newD.addInstance(aux_inst);
              }
              else {
                  String  inst_value = aux_inst.getValue(attr_index);
                  if(inst_value.equalsIgnoreCase(value)) {
                      newD.addInstance(aux_inst);
                  }
              }
              aux_inst = (Instance) li.next();
          }
          return newD;
        }

        /**
         * Merge the two datasets.<!-- --> Of course, it only goes on
         * if the two datasets has the same schema (for the moment I only
         * check for equal size of schemas).
         */

       public Dataset merge(Dataset data){
                if(data.schema().size() != schema().size()) return null;

                Dataset newDataset = new Dataset("MERGE_OF " + name + "-" + data.name, schema());

                Instance instance = getFirstInstance();
                while(instance != null){
                        newDataset.addInstance(instance);
                        instance = getNextInstance();
                }

                instance = data.getFirstInstance();
                while(instance != null){
                        newDataset.addInstance(instance);
                        instance = data.getNextInstance();
                }

                return newDataset;
       }
       /**
        * Removes the column indexed by index.<!-- --> That is, removes the attribute
        * from the schema, and removes all the corresponding values from the instances.
        * @param index
        * @return
        */
       public void removeColumn(int index){
        Instance inst = this.getFirstInstance();
        while(inst != null){
                inst.removeValue(index);
                inst = this.getNextInstance();
        }
        schema.removeAttribute(index);
       }

        /**
         * getDirectProbability is a method that returns P( Xj | X1=v1 X2=v2
         * ... Xj-1=vj-1 Xj+1=vj-2 .... Xn=vn) by counting in the dataset all
         * the instances for which attribute X1 contains the value v1, X2
         * contains value v2, etc.
         *
         * @param inst The instance from which we obtain the values of the
         * conditioning variables
         * @param attr_index The index of the attribute for which we are
         * constructing the probability distribution.
         */
        public discreteProbabilityDistribution getDirectProbability(Instance inst, int attr_index)
        {
            Dataset sub = makeCopy();

            //We build a subset of this dataset iteratively by calling subset method for each of the
            //values in inst of the conditioning attributes.
            for(int i=0; i<inst.size(); i++)
            {
                Dataset aux;
                if(i == attr_index) continue;

//              String toFile = sub.toString();
//              Utils.writeStringToFile(toFile, "subset.dat");
                sub = sub.subset(i, inst.getValue(i));
            }

            //We now build the probability distribution of attr_index's attribute.
            Attribute attr = getAttribute(attr_index);
            discreteProbabilityDistribution newD = new discreteProbabilityDistribution(attr.size());
            for(int i=0; i<attr.size(); i++)
            {
                        Dataset aux = sub.subset(attr_index, attr.getValueName(i));
                        double prob = (double)aux.size()/(double)sub.size();
                        if(sub.size() == 0) prob = 0.0;
                        newD.setProbability(i, prob);
            }
            return newD;
        }


        /**
         * getDirectProbability is another version of the one presented above
         * that returns P( X | Y=v) by counting in the dataset all the
         * instances for which attribute X1 contains the value v1
         *
         * @param The index of the conditioning variable (Y)
         * @param The value of the conditioning variable (v)
         * @param The index of the conditioned variable (X)
         */
        public discreteProbabilityDistribution getDirectProbability(int cond_index, String value, int attr_index)
        {
            Dataset sub = subset(cond_index, value);

            //We now build the probability distribution of attr_index's attribute.
            Attribute attr = getAttribute(attr_index);
            discreteProbabilityDistribution newD = new discreteProbabilityDistribution(attr.size());
            for(int i=0; i<attr.size(); i++)
            {
                Dataset aux = sub.subset(attr_index, attr.getValueName(i));
                double prob = (double)aux.size()/(double)sub.size();
                if(sub.size() == 0) prob = 0.0;
                newD.setProbability(i, prob);
            }
            return newD;
        }
        /**
         * generateRandomInstance return an instance with random values (of course, among those declared in
         * the attribute).
         */
        Instance generateRandomInstance()
        {
            Instance newI = getFirstInstance().makeCopy();

            Random r = new Random();
            for(int i=0; i<numberOfAttributes(); i++)
            {
                Attribute attr = getAttribute(i);
                int newValueIndex = r.nextInt(attr.size());
                //Sets the i-th value of instance to the random value extracted from the i-thattribute.
                newI.setValue(attr.getValueName(newValueIndex), i);
            }
            return newI;
        }
         /**
          * Computes Information Gain. Gain(S, A) = Entropy(S) - Summation( ((|Sv| / |S|) * Entropy(Sv)) ). The summation goes over all values of attribute A.
          */
          private double infoGain(int attr_index)
          {
              Attribute A = getAttribute(attr_index);
              int count_size = A.size();

              int counts[] = valueBins(attr_index);
              double ent = entropy();

              for(int i=0; i<A.size(); i++)
              {
                  double ratio = ((double)counts[i])/((double)instances.size());
                  if(instances.size() == 0) ratio = 0;

                  Dataset examples_v = subset(attr_index, A.getValueName(i));

                  //Un-comment to see a nice file output of the subset.
//                String output = examples_v.toString();
//                Utils.writeStringToFile(output, "parsedDataset_v.dat");

                  ent = ent - ratio * examples_v.entropy();

              }
              return ent;
          }
          /**
           * Computes the entropy of the dataset. Entropy(S) = Summation(-p(I)
           * log2 p(I)), where p(I) is proportion of instances belonging to
           * class I. The summation goes over all class values
           */
           public double entropy()
           {
               Attribute class_attr = getAttribute(numberOfAttributes()-1);
               int count_size = class_attr.size();
               int counts[] = classValueBins();

               double ent = 0;
               for(int i=0; i<count_size; i++) {
                   double p_i = (double)counts[i] / (double)size();
                   if(size() == 0) p_i = 0;
                   double log_2;
                  if(p_i != 0) log_2 = Math.log(p_i) / Math.log(2);//computes log base 2.
                  else log_2 = 0.0;
                  ent = ent - p_i*log_2;
               }
               return ent;
           }
          /**
          * Return the attribute index of the attribute with highest information gain.
          */
          public int chooseSplitAttribute()
          {
              double max = Double.MIN_VALUE;
              int best_index = 0;

              for(int i=0; i<schema.size()-1; i++)
              {
                  if(getAttribute(i).ignore()) continue;
                  double  infogain =infoGain(i);
                  if( infogain > max) {
                      max = infogain;
                      best_index = i;
                  }
              }
              return best_index;
          }
          /**
           * Compare
           */
          public double compareDatasets(Dataset data, int index){
                double diff = 0;

                Instance inst1 = getFirstInstance();
                Instance inst2 = data.getFirstInstance();

                int fail = 0;
                while(inst1 != null && inst2 != null){
                        if(inst1.getValue(index) != inst2.getValue(index)) fail++;

                        inst1 = getNextInstance();
                        inst2 = data.getNextInstance();
                }

                //if()
                return (double)fail/(double)size();
          }

         /**
         * Majority Value returns the value of class attribute that appears more times in the dataset.
         */
        public String majorityValue()
        {
          Attribute class_attr = getAttribute(numberOfAttributes()-1);
          int count_size = class_attr.size();
          int counts[] = classValueBins();

          //Gets the majority Value.
          int max = Integer.MIN_VALUE;
          int majority_index = -1;
          for(int i=0; i<count_size; i++) {
            if(counts[i] > max) {
              max = counts[i];
              majority_index = i;
            }
          }
          return (String) class_attr.getValueName(majority_index);

        }


 /**
  * This method compute the number of instances in the dataset that has value at attribute A.
  * It stores the results in the array counts and return it;
  */
        private int [] valueBins(int attr_index)
        {
            Attribute attr = getAttribute(attr_index);
//          Attribute class_attr = getAttribute(numberOfAttributes()-1);

          //Will store the number of times that each class value appears.
          int  counts[] = new int[attr.size()];


          for(int i=0; i<attr.size(); i++) { counts[i] = 0;}

          if(instances.size() == 0) return counts;

          ListIterator li = instances.listIterator(0);
          Instance inst = (Instance) li.next();

          //Count ocurrences of each class value and store it in counts;
          while(true)
          {
            //This will return the index of the attribute value of the current instance.
              String aux =  inst.getValue(attr_index);
              int value_index = attr.getValueIndex(aux);

              counts[value_index]++;

              if(li.hasNext()) inst = (Instance) li.next();
              else break;
          }
          return counts;
        }
 /**
  * This method compute the number of times each class value appears in the dataset. Output: an int array int [] with the counts.
  */
        private int [] classValueBins()
        {
            return valueBins(numberOfAttributes()-1);
        }

        /**
         * returns the number of instances in the dataset.
         */
        public int size()
        {
                return instances.size();
        }

        /**
         * returns the number of attributes in the dataset.
         */
        public int numberOfAttributes()
        {
                return schema.size();
        }

        /**
         * Adds an attribute to the dataset.
         */
        /*public void addAttribute(Attribute n_attr){
                schema.addAttribute(n_attr);
        }*/

        /**
         * Retrieves the index-th attribute from the dataset.
         */
        public Attribute getAttribute(int index){
                return (Attribute) schema.getAttribute(index);
        }

        /**
         * Recall that instances is a LinkedList. Thus, this method is very un efficient.
         */
        public Instance getInstance(int index){
            return (Instance) instances.get(index);
        }

        /**
         * Set mark = false for all attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
         */
        public void unmarkAllAttributes()
        {
          for(int i=0; i<schema.size(); i++)
          {
            Attribute attr = getAttribute(i);
            attr.unmark();
          }
        }

        /**
         * Set mark = false for the i-th attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
         */
        public void markAttribute(int i)
        {
          Attribute attr = getAttribute(i);
          attr.mark();
        }

        /**
         * Return the mark flag of the i-th attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
         */
        public boolean marked(int i)
        {
          Attribute attr = getAttribute(i);
          return attr.marked();
        }

        /**
         * Return true if all attributes are marked (except the last one, which doesn't mater, recall it is the class attribute).
         */
        public boolean allMarked()
        {
          boolean ret = true;

          for(int i=0; i<schema.size(); i++)
          {
            Attribute attr = getAttribute(i);
            if(!attr.marked()) ret = false;
          }
          return ret;
        }

        /**
         * Returns a class attribute value. It is assumed that the class attribute is the last attribute.
         * Input: the value index.
         * Output: The index-th value of the class attribute (last one).
         */
        public String getClassValue(int index)
        {
          Attribute attr = getAttribute(schema.size()-1);
          return attr.getValueName(index);
        }

        public void clear(){
                instances.clear();
        }

        /** This version receives an instance in the internal representation.
         */
        public void addInstance(Instance inst)
        {
                instances.add(inst);
        }

        /** Removes instance from the Linked List. It does not frees the instance itself from memory.
         */
        public boolean removeInstance(Instance inst)
        {
          return instances.remove(inst);
        }

        /** Reads all the dataset from fileName, and rebuilds schema directly from data (not from ARFF).<!-- -->
         * It assumes the first line of the data file contains the names of the attributes separated by comas. */
        public int readAllWSchema(String fileName, char separator)
        {
          schema = new Schema(name);
          return read(fileName, 0, -1, separator, true);
        }

        /** Reads all the dataset from fileName. */
        public int readAll(String fileName, char separator)
        {
          return read(fileName, 0, -1, separator, false);
        }
        /** Reads the dataset from fileName.
         */
        public int read(String fileName, int startRow, int endRow, char separator, boolean rebuildSchema)
        {
          BufferedReader reader = null;
          try{
            reader = new BufferedReader(new FileReader(fileName));
          }
          catch(Exception ioe){}

          return read(reader, startRow, endRow, separator, rebuildSchema);
        }
        /** Reads the dataset from the reader given by the calling function.<1-- -->
         * It is simply a loop line by line until the end of the file or until endRow is reached.<1-- -->
         * If endRow = -1, then it reads until end of file. <!-- -->Each line is parsed as an instance.
     *
     * Lines starting with # are ignored.
     *
         * @return false number of lines read.
         *
         */
        private int read(BufferedReader reader, int startRow, int endRow, char separator, boolean rebuildSchema)
        {
          String line= "dummy";


          int row = 0;

          //Uses info in first row to create attributes and inizialize schema.
          if(rebuildSchema){
                try { line = reader.readLine(); }
            catch (IOException ex) { }
            parseFirstLine(line, separator);
            parseSecondLine(line, separator);
          }
          try { line = reader.readLine(); }
          catch (IOException ex) { }

          //Skip first startRow rows.
          while(line != null) {
            if(row == startRow) break;
            try { line = reader.readLine(); }
            catch (IOException ex) { }

            row++;
          }
          while(line != null){
            if(row == endRow && endRow != -1) break;

            if(line.length() > 0 && line.charAt(0)== '#'){
              continue;
            }
            //addInstance(parseInstanceEPinions(line, row));
            addInstance(parseInstance(line, row, separator, rebuildSchema));

            //if(row%100==0)System.out.println(row);

            try { line = reader.readLine(); }
            catch (IOException ex) { }

            row++;
          }

          postProcessLinearValues();

          return row;
        }
        private void parseFirstLine(String instanceStr, char separator){
            BufferedReader reader = null;
            try{
              reader = new BufferedReader(new StringReader(instanceStr));
            }
            catch(Exception ioe){}

            StreamTokenizer tokenizer = new StreamTokenizer(reader);

            tokenizer.resetSyntax();
            //tokenizer.whitespaceChars(0, ' ');
            tokenizer.wordChars(' ','\u00FF');
            //tokenizer.wordChars(' ','\u00FF');
            tokenizer.whitespaceChars(separator,separator);
            tokenizer.commentChar('|');
            tokenizer.eolIsSignificant(true);


            try{tokenizer.nextToken();}
            catch (Exception te){}

            String token = tokenizer.sval;

            int aux = numberOfAttributes();

            int attr_index = 0;
            while(tokenizer.ttype != StreamTokenizer.TT_EOL && tokenizer.ttype != StreamTokenizer.TT_EOF)
             {
                Attribute new_attr = new Attribute(token, attr_index);
                attr_index++;
                schema.addAttribute(new_attr);

                try{tokenizer.nextToken();}
                catch (Exception te){}
                token = tokenizer.sval;

              }
        }
        private void parseSecondLine(String instanceStr, char separator){
            BufferedReader reader = null;
            try{
              reader = new BufferedReader(new StringReader(instanceStr));
            }
            catch(Exception ioe){}

            StreamTokenizer tokenizer = new StreamTokenizer(reader);

            tokenizer.resetSyntax();
            //tokenizer.whitespaceChars(0, ' ');
            tokenizer.wordChars(' ','\u00FF');
            //tokenizer.wordChars(' ','\u00FF');
            tokenizer.whitespaceChars(separator,separator);
            tokenizer.commentChar('|');
            tokenizer.eolIsSignificant(true);


            try{tokenizer.nextToken();}
            catch (Exception te){}

            String token = tokenizer.sval;

            int aux = numberOfAttributes();

            int attr_index = 0;
            while(tokenizer.ttype != StreamTokenizer.TT_EOL && tokenizer.ttype != StreamTokenizer.TT_EOF)
             {
                //check first character equals '@'
                if(token.charAt(0) != '@'){
                        /*System.out.println("\n\nERROR: Ill formed attribute type.");
                                Exception e = new Exception();
                                e.printStackTrace();*/

                        //IGNORE SECOND LINE
                        System.out.println("\nIgnoring Second line.");
                        break;
                        }
                        int type = Attribute.DISCRETE;
                        if(token.equalsIgnoreCase("@all-discrete") || token.equalsIgnoreCase("@all-continuous")){
                                if(token.equalsIgnoreCase("@all-continuous")) type = Attribute.CONTINUOUS;
                                for(int i=0; i<schema.size(); i++){
                                        schema.getAttribute(i).type(type);
                                }
                                break;
                }

                        if(token.equalsIgnoreCase("@discrete")) type = Attribute.DISCRETE;
                        if(token.equalsIgnoreCase("@continuous")) type = Attribute.CONTINUOUS;

                        schema.getAttribute(attr_index).type(type);
                attr_index++;

                try{tokenizer.nextToken();}
                catch (Exception te){}
                token = tokenizer.sval;

              }
        }

        /** Parses one line (we assume one instance = one line). */
        private Instance parseInstance(String instanceStr, int row, char separator, boolean rebuildSchema)
        {
          BufferedReader reader = null;
          try{
            reader = new BufferedReader(new StringReader(instanceStr));
          }
          catch(Exception ioe){}

          StreamTokenizer tokenizer = new StreamTokenizer(reader);

          tokenizer.resetSyntax();
          //tokenizer.whitespaceChars(0, ' ');
          tokenizer.wordChars(' ','\u00FF');
          //tokenizer.wordChars(' ','\u00FF');
          tokenizer.whitespaceChars(separator,separator);
          tokenizer.commentChar('|');
          tokenizer.eolIsSignificant(true);


          try{tokenizer.nextToken();}
          catch (Exception te){}

          String token = tokenizer.sval;

//          while (tokenizer.ttype != StreamTokenizer.TT_EOF)
  //        {
            int aux = numberOfAttributes();
            //Ignore attributes with ignore=true
            /*      for(int i=0; i<numberOfAttributes(); i++)
                 {
                     if(getAttribute(i).ignore()) aux--;
                 }
             */
            Instance inst = new Instance(name + String.valueOf(row), aux);

            //Add values to new instance;
            int value_count = 0;
            while(tokenizer.ttype != StreamTokenizer.TT_EOL && tokenizer.ttype != StreamTokenizer.TT_EOF)
            {
                  Attribute currAttr = schema.getAttribute(value_count);

                      //Checks if value is missing. If so, we add that value to the attribute values list.
                      if(token.equalsIgnoreCase("?"))
                      {
                        Attribute aux_attr = (Attribute) schema.getAttribute(value_count);
                        if(!aux_attr.hasValue("?")) aux_attr.addValue("?");
                      }
                      //If attribute does not contain this value, we add the value to the attribute values list.
                      if(!currAttr.hasValue(token)) currAttr.addValue(token);
                      inst.addValue(token);

                      try{tokenizer.nextToken();}
                      catch (Exception te){}
                      token = tokenizer.sval;

                      value_count++;
            }
            return inst;
        }
        /** row not used. */
        private Instance parseInstanceEPinions(String instanceStr, int row)
                {
                String ltoken="", lltoken="";
                int integerIndex=-1;
                BufferedReader reader = null;
                try{
                        reader = new BufferedReader(new StringReader(instanceStr));
                }
                catch(Exception ioe){}

                StreamTokenizer tokenizer = new StreamTokenizer(reader);

                tokenizer.resetSyntax();
                //tokenizer.whitespaceChars(0, ' ');
                tokenizer.wordChars(' ','\u00FF');
                //tokenizer.wordChars(' ','\u00FF');
                tokenizer.whitespaceChars(',',',');
                tokenizer.commentChar('|');
                tokenizer.eolIsSignificant(true);


                try{tokenizer.nextToken();}
                catch (Exception te){}

                String token = tokenizer.sval;

                int aux = numberOfAttributes();
                Instance inst = new Instance(name,aux);

                //Add values to new instance;
                int value_count = 0;

                while(tokenizer.ttype != StreamTokenizer.TT_EOL && tokenizer.ttype != StreamTokenizer.TT_EOF)
                {
                        try{
                                Integer tI = new Integer(token);
                                tI.intValue();
                                integerIndex = value_count;
                                break;

                        }
                        catch(Exception e){
                                integerIndex = value_count;
//                              /break;
                        }

                        try{tokenizer.nextToken();}
                        catch (Exception te){}
                        token = tokenizer.sval;

                        value_count++;
                }
                String nameValue="";
                value_count = 0;
                reader = null;
                try{
                        reader = new BufferedReader(new StringReader(instanceStr));
                }
                catch(Exception ioe){}
                StreamTokenizer tokenizer2 = new StreamTokenizer(reader);

                tokenizer2.resetSyntax();
                tokenizer2.wordChars(' ','\u00FF');
                tokenizer2.whitespaceChars(',',',');
                tokenizer2.commentChar('|');
                tokenizer2.eolIsSignificant(true);

                try{tokenizer2.nextToken();}
                catch (Exception te){}

                token = tokenizer2.sval;


                while(tokenizer2.ttype != StreamTokenizer.TT_EOL && tokenizer2.ttype != StreamTokenizer.TT_EOF)
                {
//Checks if value is missing. If so, we add that value to the attribute values list.
                        if(token.equalsIgnoreCase("?"))
                        {
                                Attribute aux_attr = (Attribute) schema.getAttribute(value_count);
                                if(!aux_attr.hasValue("?")) aux_attr.addValue("?");
                        }
                        if(value_count >= 2 && value_count < integerIndex-1){
                                if(value_count == integerIndex-2) {
                                        nameValue += token;
                                        inst.addValue(nameValue);
                                }
                                else         nameValue += token + ",";

                                if(value_count < integerIndex-2){
                                        int kk =1;
                                }
                        }
                        else inst.addValue(token);

                        try{tokenizer2.nextToken();}
                        catch (Exception te){}
                        lltoken = ltoken;
                        ltoken= token;
                        token = tokenizer2.sval;

                        value_count++;
                }

                return inst;
        }
        //At this point, we have all the information needed to deal with the case of "continuous" or "linear" values

        /**
         * Returns a vertical merge of the two datasets.<!-- --> For the moment it appends
         * the attributes (and corresponding values at each instance) of the second dataset
         * at the end of the first dataset.<!-- --> If sizes don't match, it TRIMS the excedent
         * chunk from the largest dataset.
         */
        public static Dataset verticalMerge(Dataset d1, Dataset d2){
                /*if(d1.size() != d2.size()){
                        System.out.println("Error in Dataset.verticalMerge: datasets' sizes don't match.");
                        return null;
                }*/
                Schema s1 = d1.schema();
                Schema s2 = d2.schema();

                Schema schema = new Schema(s1.name +"_"+ s2.name);

                for(int i=0; i<s1.size(); i++){
                        schema.addAttribute(s1.getAttribute(i));
                }

                for(int i=0; i<s2.size(); i++){
                        schema.addAttribute(s2.getAttribute(i));
                }

                Dataset data = new Dataset("", schema);

                Instance i1 = d1.getFirstInstance();
                Instance i2 = d2.getFirstInstance();
                while(i1 != null && i2 != null){
                        Instance inst = new Instance();

                        int size1 = i1.size();
                        for(int j=0; j<i1.size(); j++){
                                inst.addValue(i1.getValue(j), j);
                        }
                        for(int j=0; j<i2.size(); j++){
                                inst.addValue(i2.getValue(j), j+size1);
                        }
                        data.addInstance(inst);

                        i1 = d1.getNextInstance();
                        i2 = d2.getNextInstance();
                }
                int s = data.size();
                return data;
        }
        /**
         * This procedure post process the dataset for the case of "linear" or "continuous" attribute values.
         */
        private void postProcessLinearValues()
        {
          String value_name;
          Attribute aux_attr;
          double min, max;

          for(int i=0; i<schema.size(); i++)
          {
            aux_attr = (Attribute) schema.getAttribute(i);
            if(aux_attr.type == Attribute.CONTINUOUS)
            {
              //If it is linear, then we retrieve the min and max values along all instances.
              min = Double.MAX_VALUE;
              max = Double.MIN_VALUE;

              ListIterator li = instances.listIterator(0);
              Instance aux_inst = (Instance) li.next();

              while(li.hasNext())
              {
                //If the value is "?" we should skip it.
                String aux_str = aux_inst.getValue(i);
                if(aux_str.equalsIgnoreCase("?")) {
                  aux_inst = (Instance) li.next();
                  continue;
                }

                double aux_int = Double.parseDouble(aux_str);

                if(aux_int <= min) min = aux_int;
                if(aux_int >= max) max = aux_int;

                aux_inst = (Instance) li.next();
//                it_index = li.nextIndex();
              }
              min = min;//For debugging purposes
              max = max; //For debugging purposes


              //Compute number of bins according to Sturge's rule (see David W. Scott, "Multivariate density estimation: Theory, Practice, and Visualization")
              int numBins = (int)(1.0 + Math.log(this.size())/Math.log(2.0));

              //Discretize the attribute
              aux_attr.discretizeValues(min, max, numBins);

              //Update the linear values in the dataset.
              li = instances.listIterator(0);
              aux_inst = (Instance) li.next();

              while(li.hasNext())
              {
                String aux_str = aux_inst.getValue(i);
                if(aux_str.equalsIgnoreCase("?"))
                {
                  aux_inst = (Instance) li.next();
                  continue;
                }
                double aux_int = Double.parseDouble(aux_str);
                String value = aux_attr.getBinName(aux_int);
                aux_inst.setValue(value, i);

                aux_inst = (Instance) li.next();

              }
            }
          }
        }



        /** Reads the dataset from the reader given by the calling function.
         */
 /*       public void parseNamesFile(String fileName)
        {
          BufferedReader reader = null;
          try{
            reader = new BufferedReader(new FileReader(fileName));
          }
          catch(Exception ioe){}

          parseNamesFile(reader);
        }

        /** This function receives a string version of the Names File and
         * convert it to the internal representation, that is, it creates
         * the corresponding attribute structure.
         */
/*      public void parseNamesFile(BufferedReader reader)
        {
                //int linePos = 0;
                Attribute n_attr;
                String attrName, token;
                StreamTokenizer tokenizer = new StreamTokenizer(reader);

                tokenizer.resetSyntax();
                tokenizer.whitespaceChars(0, ' ');
                tokenizer.wordChars(' '+1,'\u00FF');
                tokenizer.whitespaceChars(',',',');
                tokenizer.whitespaceChars('.','.');
                tokenizer.commentChar('|');
                tokenizer.eolIsSignificant(true);

                try {tokenizer.nextToken();}
                catch (Exception te){}
                token = tokenizer.sval;

                //Skips the first line that contains "ClassField."
                while(tokenizer.ttype != tokenizer.TT_EOL){
                  try{tokenizer.nextToken();}
                  catch (Exception te){}
                  token = tokenizer.sval;
                }

                //Reads until end of file
                //Each iteration of the loop is basically parsing one line
                //That is, at the beggining of the loop, the tokenizer should
                //point to the first pointer in the line
                while(tokenizer.ttype != tokenizer.TT_EOF)
                {
                  //Reads attribute name.
                  attrName = "PEPE";
                  attrName = token = "";
                  while(token.indexOf(':') == -1){
                    try{tokenizer.nextToken();}
                    catch (Exception te){}
                    token = tokenizer.sval;

                    attrName = attrName + " " + token;
                  }
                  attrName = attrName.substring(1, attrName.length()  -1 );
//                  attrName = attrName + token;
                  n_attr = new Attribute(attrName);

                  try{tokenizer.nextToken();}
                  catch (Exception te){}
                  token = tokenizer.sval;


                  while(tokenizer.ttype != tokenizer.TT_EOL && tokenizer.ttype != tokenizer.TT_EOF)
                  {

                    if(token.equalsIgnoreCase("date") ||
                       token.equalsIgnoreCase("time") ||
                       token.equalsIgnoreCase("ignore") ||
                       token.equalsIgnoreCase("label")  //Attribute dont used for classification.
                       )
                    {
                        n_attr.addValue("ignore");
                        n_attr.ignore(true);

                        try{tokenizer.nextToken();}
                        catch (Exception te){}
                        token = tokenizer.sval;

                        continue;
                    }
                    //Take care of linear/continuous case.
                    //It will be processed later, when the dataset is readed.
                    //This is because we need min and max.
                    if(token.equalsIgnoreCase("linear")) {
                        n_attr.addValue("linear");

                        try{tokenizer.nextToken();}
                        catch (Exception te){}
                        token = tokenizer.sval;
                        continue;
                    }
                    n_attr.addValue(token);

                    try{tokenizer.nextToken();}
                    catch (Exception te){}
                    token = tokenizer.sval;
                  }
                  addAttribute(n_attr);

                }
        }
*/


        /**
        * Unparse to my format, this procedure works paired with myUnserialize.<!-- -->
        * The output string stores the current state of the calling object. This
        * string can be used later to set the state of another object to the same state
        * of the original object.<!-- --> Note that this is not storing objects, but only their
        * state (current value of internal fields).
        * @return a String.
        */

        public String mySerialize(Parser parser)
        {
//          Parser parser = new Parser(pd, ed, vd, sd);

          Vector vblock = new Vector();

   //       vblock.addElement(new Block("DATASCHEMA", super.mySerialize(parser)));
          vblock.addElement(new Block("DATASCHEMA", schema, parser));

          //sum of counts block
          Block blk2 = new Block("DATAINSTANCES");
          Instance inst = getFirstInstance();
          while(inst != null){
            blk2.addValue(inst, parser);

            inst = getNextInstance();
          }
          vblock.addElement(blk2);


          return parser.filter(parser.unparseBlocks(vblock));
        }

        /**
           * Parse to my format, this procedure works paired with mySerialize.<!-- -->
           * The string output from this procedure is used to change the state of
           *  internal
           * field values of the current calling object.
           * @return a String.
           */

public void myUnserialize(String inputStr, Parser parser)
{
 //         Parser parser = new Parser (pd, ed, vd, sd);
          Vector vblock = parser.parse(parser.unfilter(inputStr));

          for(int i=0; i<vblock.size(); i++)
          {
            Block blk = (Block) vblock.get(i);

            if(blk.tag.equals("DATASCHEMA")) {
              //super.myUnserialize(blk.getString(0), parser);
              schema = null;
              schema = (Schema) blk.getObject(0, new Schema(), parser);
            }

            if(blk.tag.equals("DATAINSTANCES"))
            {
              instances = null;
              instances = new LinkedList();
              for(int j=0; j<blk.size(); j++)
              {
                Instance inst = (Instance) blk.getObject(j, new Instance(), parser);
                //Attribute attr = new Attribute("garbage");
                //blk.getObject(j, attr, parser);
                instances.add(inst);
              }
            }
          }

        }
      }