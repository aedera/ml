package machineLearning.Datasets;

//import machineLearning.Utils.BitS
import java.util.BitSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;
import java.util.Collection;
import java.util.ArrayList;
import java.io.*;

import machineLearning.Utils.FastSet;

import machineLearning.Graphs.Variable;

import machineLearning.Utils.Utils;
import stringSerializer.*;

/**
 * Class Dataset
 *
 * Implements the functionality of a dataset with the attributes list and the
 * data itself both contained in abstract representations: Attribute and
 * Instance. The whole structure is inspired both by a personal analysis and
 * design of the functionality of datasets and by knowledge of WEKA environment.
 * But, no line of code was copied from WEKA
 *
 * @author Facundo Bromberg
 * @version 1.1 Actualized for the use in Final Project (BayesNetworks
 *          inference) of CS572, AI, Fall 2002.
 * @version 1.2 Splitted in Dataset+Schema for Final Project (BayesNetworks
 *          incremental learning) of CS561
 * @version 1.3 Actualized for MAL Oct 2003.<!-- --> Splitted the concept of
 *          dataset and schema, now dataset contains a schema but it does not
 *          extend it. DB, Spring 2003.
 */
public class FastDataset implements Iterable<FastInstance> {
    /**
     * For eficiency reasons, instances is implemented as a linked list.
     * Moreover, it is set public to allow the user an efficient iteration by
     * using the Iterator functionality.
     */
    public String name;

    private Schema schema;

    public Schema schema() {
        return schema;
    };

    public void schema(Schema _schema) {
        schema = _schema;
    };

    public LinkedList<FastInstance> instances;

    transient private ListIterator li;

    public FastDataset() {
        instances = new LinkedList();
    };

    // public Dataset(Schema schema){
    // name = new String(schema.name);
    // }
    public FastDataset(String _name, Schema schema) {
        // super(_name);
        this.schema = schema;
        instances = new LinkedList();
    }

    public FastDataset(String _name) {
        // super(_name);
        this.schema = null;
        instances = new LinkedList();
    }

    public java.util.Iterator<FastInstance> iterator() {
        return instances.iterator();
    }

    /**
     * Iteration funcionality.<!-- --> This method initiates an iteration over
     * the instances.
     */
    public FastInstance getFirstInstance() {
        li = instances.listIterator(0);
        if (!li.hasNext())
            return null;

        return (FastInstance) li.next();
    }

    /**
     * Returns next instance in iterator.
     */
    public FastInstance getNextInstance() {
        if (!li.hasNext())
            return null;

        return (FastInstance) li.next();
    }

    /**
     * Procedure that returns a String representation of Attributes and
     * Instances.
     */
    public String toString() {
        return toString(false, false);
    }

    /**
     * Procedure that returns a String representation of Attributes and
     * Instances. If noHeader is true, then it returns only the instances
     */
    public String toString(boolean noHeader, boolean noInstanceValues) {
        String Names = "", Data = "";

        if (!noHeader) {
            Names = super.toString();
        }
        if (!noHeader)
            Data = "\nInstances data: \n";

        if (instances.size() == 0)
            return Names + "\n" + Data;

        ListIterator li = instances.listIterator(0);
        FastInstance inst = (FastInstance) li.next();

        while (true) {
            Data = Data + inst.toString(noInstanceValues);
            if (!noInstanceValues)
                Data = Data + "\n";
            else
                Data = Data + ", ";

            if (li.hasNext())
                inst = (FastInstance) li.next();
            else
                break;
        }

        return Names + Data;
    }

    public int[] scope() {
        int[] variables = new int[schema.size()];

        for (int i = 0; i < schema.size(); i++)
	    variables[i] = i;

        return variables;
    }

    /**
     * Procedure that returns a String in ARFF format of Attributes and
     * Instances. If noHeader is true, then it returns only the instances
     */
    public String toARFF(boolean onlyData, String separator) {
        return toARFF(onlyData, separator, null);
    };

    public String toARFF(boolean onlyData, String separator, String FileName) {
        BufferedWriter writer = null;
        if (FileName != null) {
            try {
                writer = new BufferedWriter(new FileWriter(FileName), 100000);
            } catch (Exception ioe) {
                ;
            }
        }

        String Data = "";
        if (!onlyData)
            Data = schema.toARFF() + "\n";

        // System.out.println(schema.toARFF());

        if (instances.size() == 0)
            return Data;

        // ListIterator li = instances.listIterator(0);
        FastInstance inst = getFirstInstance();

        if (!onlyData) {
            Data += "@DATA\n";
            if (FileName != null) {
                try {
                    writer.write(Data, 0, Data.length());
                    writer.flush();
                } catch (IOException e) {
                    ;
                }
                ;
            }
        }
        int f = 0;
        while (inst != null) {
            String str = inst.toARFF(separator) + "\n";
            if (FileName == null)
                Data = Data + str;
            // if(f%100==0)System.out.print(f + ": " + str);
            f++;
            if (FileName != null) {
                try {
                    writer.write(str, 0, str.length());
                } catch (IOException e) {
                    ;
                }
                ;
            }

            inst = getNextInstance();
        }

        if (FileName != null) {
            try {
                writer.close();
            } catch (IOException e) {
                ;
            }
            ;
        }
        return Data;
    }

    /**
     * Make Copy creates a new dataset but do not create new
     * instances(attributes). Each cell in the instances (attributes) Vector
     * will point to the same instance.
     */
    public FastDataset makeCopy() {
        return subset(-1, 0);
    }

    // ------------------------------------------------------------------------------------------------
    /**
     * Return a horizontal fragment.
     *
     * @param attr_index
     * @param value
     * @return
     */
    public FastDataset subsetHorizontal(int from, int to) {
        FastDataset newD = (FastDataset) new FastDataset(name, schema);

        // Copy attributes Vector.
        // Attribute attr = schema.getAttribute(k);
        // newD.schema.addAttribute(attr);

        // Copy instances LinkedList
        ListIterator li = instances.listIterator(0);
        FastInstance aux_inst = (FastInstance) li.next();

        int aux = 0;
        while (li.hasNext() && aux < from) {
            aux++;
            aux_inst = (FastInstance) li.next();
        }

        int numberCopied = 0;
        while (li.hasNext() && numberCopied < to - from) {
            newD.addInstance(aux_inst);

            aux_inst = (FastInstance) li.next();
            numberCopied++;
        }

        return newD;
    }

    // ------------------------------------------------------------------------------------------------
    /**
     * Return a horizontal fragment.
     *
     * @param attr_index
     * @param value
     * @return
     */
    public FastDataset subsetVertical(FastSet colIndexes) {
        FastDataset newData = new FastDataset();

        //Create new schema
        Schema newSchema = new Schema();
        int colIndex = colIndexes.first();
        while(colIndex >= 0){
            newSchema.addAttribute(schema.getAttribute(colIndex));

            colIndex = colIndexes.next();
        }

        FastDataset newD = (FastDataset) new FastDataset(name, newSchema);

        ListIterator li = instances.listIterator(0);
        FastInstance aux_inst = (FastInstance) li.next();
        while (li.hasNext()) {
            newD.addInstance(new FastInstance(aux_inst.project(colIndexes), newSchema));

            aux_inst = (FastInstance) li.next();
        }


        return newD;
    }
    // ------------------------------------------------------------------------------------------------
    /**
     * Return a random horizontal fragment of expected size size()*alpha, where
     * alpha is the probability of selecting an instance row.
     */
    public FastDataset randomSubsetHorizontal(double alpha, Random r) {
        FastDataset newD = (FastDataset) new FastDataset(name, schema);

        ListIterator li = instances.listIterator(0);
        FastInstance aux_inst = (FastInstance) li.next();

        double randomAux;
        while (li.hasNext()) {
            randomAux = r.nextDouble();
            if (randomAux <= alpha)
                newD.addInstance(aux_inst);

            aux_inst = (FastInstance) li.next();
        }

        return newD;
    }

    // ------------------------------------------------------------------------------------------------

    /**
     * Subset creates a copy of the dataset including only a subset of the
     * instances.<!-- --> More specifically, those that has the index-th
     * attribute equal to "value" If value = -1 , then, it copies all. That is,
     * it returns a copy of whole dataset If value = -2 , then, it doesn't copy
     * any instance. That is, it returns a copy of the dataset but without any
     * instance
     */
    public FastDataset subset(int attr_index, int value) {
        FastDataset newD = (FastDataset) new FastDataset(name, schema);

        // Copy attributes Vector.
        for (int i = 0; i < schema.size(); i++) {
            Attribute attr = schema.getAttribute(i);
            newD.schema.addAttribute(attr);
        }
        if (attr_index == -2 || instances.size() == 0)
            return newD;

        // Copy instances LinkedList
        ListIterator li = instances.listIterator(0);
        FastInstance aux_inst = (FastInstance) li.next();

        while (li.hasNext()) {
            if (attr_index == -1) {
                newD.addInstance(aux_inst);
            } else {
                int inst_value = aux_inst.getValue(attr_index);
                if (inst_value == value) {
                    newD.addInstance(aux_inst);
                }
            }
            aux_inst = (FastInstance) li.next();
        }
        return newD;
    }

    /**
     * @return a <code>FastDataset</code> instance containing the instances of
     * this from <code>base</code> instance index to <code>offset</code>
     * instance index.
     */
    public FastDataset getSubSet(int base, int offset)
        throws IndexOutOfBoundsException {
        if ((base < 0) && (offset >= size()))
            throw new IndexOutOfBoundsException("Range out of bound the dataset.");

        if (isEmpty()) return null;

        FastDataset subset = new FastDataset();
        subset.schema(schema());

        subset.instances = new LinkedList(instances.subList(base, offset));

        return subset;
    }

    /**
     * Merge the two datasets.<!-- --> Of course, it only goes on if the two
     * datasets has the same schema (for the moment I only check for equal size
     * of schemas).
     */

    public FastDataset merge(FastDataset data) {
        if (data.schema().size() != schema().size())
            return null;

        FastDataset newDataset = new FastDataset("MERGE_OF " + name + "-"
                                                 + data.name, schema());

        FastInstance instance = getFirstInstance();
        while (instance != null) {
            newDataset.addInstance(instance);
            instance = getNextInstance();
        }

        instance = data.getFirstInstance();
        while (instance != null) {
            newDataset.addInstance(instance);
            instance = data.getNextInstance();
        }

        return newDataset;
    }

    /**
     * getDirectProbability is a method that returns P( Xj | X1=v1 X2=v2 ...
     * Xj-1=vj-1 Xj+1=vj-2 .... Xn=vn) by counting in the dataset all the
     * instances for which attribute X1 contains the value v1, X2 contains value
     * v2, etc.
     *
     * @param The
     *            instance from which we obtain the values of the conditioning
     *            variables
     * @param The
     *            index of the attribute for which we are constructing the
     *            probability distribution.
     */
    public discreteProbabilityDistribution getDirectProbability(
                                                                FastInstance inst, int attr_index) {
        FastDataset sub = makeCopy();

        // We build a subset of this dataset iteratively by calling subset
        // method for each of the
        // values in inst of the conditioning attributes.
        for (int i = 0; i < inst.size(); i++) {
            Dataset aux;
            if (i == attr_index)
                continue;

            // String toFile = sub.toString();
            // Utils.writeStringToFile(toFile, "subset.dat");
            sub = sub.subset(i, inst.getValue(i));
        }

        // We now build the probability distribution of attr_index's attribute.
        Attribute attr = getAttribute(attr_index);
        discreteProbabilityDistribution newD = new discreteProbabilityDistribution(
                                                                                   attr.size());
        for (int i = 0; i < attr.size(); i++) {
            FastDataset aux = sub.subset(attr_index, i);
            double prob = (double) aux.size() / (double) sub.size();
            if (sub.size() == 0)
                prob = 0.0;
            newD.setProbability(i, prob);
        }
        return newD;
    }

    /**
     * getDirectProbability is another version of the one presented above that
     * returns P( X | Y=v) by counting in the dataset all the instances for
     * which attribute X1 contains the value v1
     *
     * @param The
     *            index of the conditioning variable (Y)
     * @param The
     *            value of the conditioning variable (v)
     * @param The
     *            index of the conditioned variable (X)
     */
    public discreteProbabilityDistribution getDirectProbability(int cond_index,
                                                                int value, int attr_index) {
        FastDataset sub = subset(cond_index, value);

        // We now build the probability distribution of attr_index's attribute.
        Attribute attr = getAttribute(attr_index);
        discreteProbabilityDistribution newD = new discreteProbabilityDistribution(
                                                                                   attr.size());
        for (int i = 0; i < attr.size(); i++) {
            FastDataset aux = sub.subset(attr_index, i);
            double prob = (double) aux.size() / (double) sub.size();
            if (sub.size() == 0)
                prob = 0.0;
            newD.setProbability(i, prob);
        }
        return newD;
    }

    /**
     * generateRandomInstance return an instance with random values (of course,
     * among those declared in the attribute).
     */
    FastInstance generateRandomInstance() {
        FastInstance newI = getFirstInstance().makeCopy();

        Random r = new Random();
        for (int i = 0; i < numberOfAttributes(); i++) {
            Attribute attr = getAttribute(i);
            int newValueIndex = r.nextInt(attr.size());
            // Sets the i-th value of instance to the random value extracted
            // from the i-thattribute.
            newI.setValue(newValueIndex, i);
        }
        return newI;
    }

    /**
     * returns the number of instances in the dataset.
     */
    public int size() {
        return instances.size();
    }

    public boolean isEmpty() { return instances.isEmpty(); }

    /**
     * returns the number of attributes in the dataset.
     */
    public int numberOfAttributes() {
        return schema.size();
    }

    /**
     * Retrieves the index-th attribute from the dataset.
     */
    public Attribute getAttribute(int index) {
        return (Attribute) schema.getAttribute(index);
    }

    /**
     * Recall that instances is a LinkedList. Thus, this method is very un
     * efficient.
     */
    public FastInstance getInstance(int index) {
        return (FastInstance) instances.get(index);
    }

    /**
     * Set mark = false for all attributes. This mark is used for
     * classification. If an attribute is marked, then it has already been
     * classified.
     */
    public void unmarkAllAttributes() {
        for (int i = 0; i < schema.size(); i++) {
            Attribute attr = getAttribute(i);
            attr.unmark();
        }
    }

    /**
     * Set mark = false for the i-th attributes. This mark is used for
     * classification. If an attribute is marked, then it has already been
     * classified.
     */
    public void markAttribute(int i) {
        Attribute attr = getAttribute(i);
        attr.mark();
    }

    /**
     * Return the mark flag of the i-th attributes. This mark is used for
     * classification. If an attribute is marked, then it has already been
     * classified.
     */
    public boolean marked(int i) {
        Attribute attr = getAttribute(i);
        return attr.marked();
    }

    /**
     * Return true if all attributes are marked (except the last one, which
     * doesn't mater, recall it is the class attribute).
     */
    public boolean allMarked() {
        boolean ret = true;

        for (int i = 0; i < schema.size(); i++) {
            Attribute attr = getAttribute(i);
            if (!attr.marked())
                ret = false;
        }
        return ret;
    }

    /**
     * Returns a class attribute value. It is assumed that the class attribute
     * is the last attribute. Input: the value index. Output: The index-th value
     * of the class attribute (last one).
     */
    public String getClassValue(int index) {
        Attribute attr = getAttribute(schema.size() - 1);
        return attr.getValueName(index);
    }

    public void clear() {
        instances.clear();
    }

    /**
     * This version receives an instance in the internal representation.
     */
    public void addInstance(FastInstance inst) {
        instances.add(inst);
    }

    /**
     * Removes instance from the Linked List. It does not frees the instance
     * itself from memory.
     */
    public boolean removeInstance(FastInstance inst) {
        return instances.remove(inst);
    }

    /**
     * Reads all the dataset from fileName, and rebuilds schema directly from
     * data (not from ARFF).<!-- --> It assumes the first line of the data file
     * contains the names of the attributes separated by comas.
     */
    public int readAllWSchema(String fileName, char separator) {
        return readAllWSchema(fileName, 0, separator);
    }

    public int readAllWSchema(String fileName, int startRow, char separator) {
        return readAllWSchema(fileName, startRow, -1, separator);
    }

    public int readAllWSchema(String fileName, int startRow, int endRow,
                              char separator) {
        schema = new Schema(name);
        return read(fileName, startRow, endRow, separator, true);
    }

    public int readAllWSchema(String fileName, int startRow, int endRow,
                              char separator, BitSet attrsToRead) {
        schema = new Schema(name);
        return read(fileName, startRow, endRow, separator, true, attrsToRead);
    }

    /** Reads all the dataset from fileName. */
    public int readAll(String fileName, char separator) {
        return read(fileName, 0, -1, separator, false);
    }

    /**
     * Reads the dataset from fileName.
     */
    public int read(String fileName, int startRow, int endRow, char separator,
                    boolean rebuildSchema) {
        return read(fileName, startRow, endRow, separator, rebuildSchema, null);
    }

    public int read(String fileName, int startRow, int endRow, char separator,
                    boolean rebuildSchema, BitSet attrsToRead) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileName));
        } catch (Exception ioe) {
            System.err.println(ioe.getMessage());
        }

        return read(reader, startRow, endRow, separator, rebuildSchema,
                    attrsToRead);
    }

    public static int getOffset(String fileName, int nVariables) {
	int offset = 0;
	BufferedReader reader = null;
	String line = null;

        try {
            reader = new BufferedReader(new FileReader(fileName));
	    // verify line equals 1, 2, 3, ..., nVariables
	    do {
		offset++;
		line = reader.readLine();
		if (line == null || line.length() == 0) continue;
		if (line.matches("(\\d+,?)+")) break;
	    } while (line != null);
        } catch (Exception ioe) {
            System.err.println(ioe.getMessage());
        }

	return offset;
    }

    public static FastDataset read(String fileName, int nVariables) {
	return read(fileName, nVariables, getOffset(fileName, nVariables));
    }

    public static FastDataset read(String fileName) {
	return read(fileName, Utils.getNumVariablesFromFilename(fileName));
    }

    public static FastDataset read(int upto, String fileName) {
	int nVariables = Utils.getNumVariablesFromFilename(fileName);
	int offset = getOffset(fileName, nVariables);
	return read(fileName, nVariables, offset, upto);
    }

    public static FastDataset read(String fileName, int nVariables, int offset) {
	return read(fileName, nVariables, offset, -1);
    }

    public static FastDataset read(String fileName, int nVariables, int offset, int upto) {
        Schema sc = new Schema();
        FastInstance inst;
        BufferedReader reader = null;
	// instance's index relative to dataset
	int count = 0;

        try {
            reader = new BufferedReader(new FileReader(fileName));
        } catch (Exception ioe) {
            System.err.println(ioe.getMessage());
        }

        for (int i = 0; i < nVariables; i++)
            sc.addAttribute(new Attribute("" + i, i, 2));

	FastDataset dataset = new FastDataset("", sc);
	String line = null;

	try {
	    for (int i = 0; i < offset; i++) reader.readLine();
	    do {
		line = reader.readLine();
		if (line == null || line.length() == 0) continue;
		String[] array = line.split(",");
		inst = new FastInstance(sc, "", nVariables);

		for (int j = 0; j < array.length; j++)
		    inst.setValue(new Integer(array[j]).intValue(), j);

		inst.index = count++;
		dataset.addInstance(inst);

		--upto;
	    } while (line != null && upto > 0);
	} catch (IOException ex) {
	    System.err.println(ex.getMessage());
	}

	return dataset;
    }

    /**
     * Reads the dataset from the reader given by the calling function.<1-- -->
     * It is simply a loop line by line until the end of the file or until
     * endRow is reached.<1-- --> If endRow = -1, then it reads until end of
     * file. <!-- -->Each line is parsed as an instance.
     *
     * @return false number of lines read.
     */
    private int read(BufferedReader reader, int startRow, int endRow,
                     char separator, boolean rebuildSchema, BitSet attrsToRead) {
        String line = "dummy";

        int row = 0;

        // Skip first startRow rows.
        while (line != null) {
            if (row >= startRow - 1)
                break;
            try {
                line = reader.readLine();
            } catch (IOException ex) {
            }

            row++;
        }
        // try { line = reader.readLine(); }
        // catch (IOException ex) { }

        // Uses info in first row to create attributes and inizialize schema.
        boolean parsed2ndLine = true;
        if (rebuildSchema) {
            try {
                line = reader.readLine();
            } catch (IOException ex) {
            }

            while (true) {
                // skip comments #
                if (line.length() > 0 && line.charAt(0) == '#') {
                    try {
                        line = reader.readLine();
                    } catch (IOException ex) {
                    }
                    continue;
                } else {
                    break;
                }
            }
            if (line.equalsIgnoreCase("")) {
                System.out
                    .println("ERROR in read, trying to parse schema from empty line");
                (new Exception()).printStackTrace();
                System.exit(1);
            }
            parseFirstLine(line, separator, attrsToRead);

            try {
                line = reader.readLine();
            } catch (IOException ex) {
            }
            parsed2ndLine = parseSecondLine(line, separator, attrsToRead);
        }
        if (parsed2ndLine) {
            try {
                line = reader.readLine();
            } catch (IOException ex) {
            }
        }
        while (line != null && line.length() != 0) {
            if (row == endRow && endRow != -1)
                break;

            // skip comments #
            if (line.length() > 0 && line.charAt(0) == '#') {
                continue;
            }
            // addInstance(parseInstanceEPinions(line, row));
            FastInstance inst = FastInstance.parseInstance(line, row,
                                                           separator, schema(), attrsToRead);
            if (inst != null)
                addInstance(inst);

            // if(row%100==0)System.out.println(row);

            try {
                line = reader.readLine();
            } catch (IOException ex) {
            }

            row++;
        }

        postProcessLinearValues();

        return row;
    }

    private void parseFirstLine(String instanceStr, char separator,
                                BitSet attrsToRead) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new StringReader(instanceStr));
        } catch (Exception ioe) {
        }

        StreamTokenizer tokenizer = new StreamTokenizer(reader);

        tokenizer.resetSyntax();
        // tokenizer.whitespaceChars(0, ' ');
        tokenizer.wordChars(' ', '\u00FF');
        // tokenizer.wordChars(' ','\u00FF');
        tokenizer.whitespaceChars(separator, separator);
        tokenizer.commentChar('|');
        tokenizer.eolIsSignificant(true);

        try {
            tokenizer.nextToken();
        } catch (Exception te) {
        }

        String token = tokenizer.sval;

        int aux = numberOfAttributes();

        int attr_index = 0, k = 0;
        while (tokenizer.ttype != StreamTokenizer.TT_EOL
               && tokenizer.ttype != StreamTokenizer.TT_EOF) {
            if (attrsToRead != null && !attrsToRead.get(k)) {
                k++;

                try {
                    tokenizer.nextToken();
                } catch (Exception te) {
                }
                token = tokenizer.sval;

                continue;
            }
            Attribute new_attr = new Attribute(token, attr_index);
            attr_index++;
            schema.addAttribute(new_attr);

            try {
                tokenizer.nextToken();
            } catch (Exception te) {
            }
            token = tokenizer.sval;

        }
    }

    private boolean parseSecondLine(String instanceStr, char separator,
                                    BitSet attrsToRead) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new StringReader(instanceStr));
        } catch (Exception ioe) {
        }

        StreamTokenizer tokenizer = new StreamTokenizer(reader);

        tokenizer.resetSyntax();
        // tokenizer.whitespaceChars(0, ' ');
        tokenizer.wordChars(' ', '\u00FF');
        // tokenizer.wordChars(' ','\u00FF');
        tokenizer.whitespaceChars(separator, separator);
        tokenizer.commentChar('|');
        tokenizer.eolIsSignificant(true);

        try {
            tokenizer.nextToken();
        } catch (Exception te) {
        }

        String token = tokenizer.sval;

        int aux = numberOfAttributes();

        int attr_index = 0, k = 0;
        while (tokenizer.ttype != StreamTokenizer.TT_EOL
               && tokenizer.ttype != StreamTokenizer.TT_EOF) {
            if (attrsToRead != null && !attrsToRead.get(k)) {
                k++;

                try {
                    tokenizer.nextToken();
                } catch (Exception te) {
                }
                token = tokenizer.sval;

                continue;
            }

            // check first character equals '@'
            if (token.charAt(0) != '@') {
                // IGNORE SECOND LINE
                if (attr_index == 0) {
                    // System.out.println("\nIgnoring Second line.");
                    return false;
                } else {
                    System.out.println("\n\nERROR: Ill formed attribute type.");
                    Exception e = new Exception();
                    e.printStackTrace();

                }
                break;
            }
            int type = Attribute.DISCRETE;
            if (token.equalsIgnoreCase("@all-discrete")
                || token.equalsIgnoreCase("@all-continuous")) {
                if (token.equalsIgnoreCase("@all-continuous"))
                    type = Attribute.CONTINUOUS;
                for (int i = 0; i < schema.size(); i++) {
                    schema.getAttribute(i).type(type);
                }
                return true;
            } else if (token.equalsIgnoreCase("@discrete")) {
                type = Attribute.DISCRETE;
            } else if (token.equalsIgnoreCase("@continuous")) {
                type = Attribute.CONTINUOUS;
            } else {
                System.out.println("\n\nERROR: Ill formed attribute type.");
                Exception e = new Exception();
                e.printStackTrace();
            }

            schema.getAttribute(attr_index).type(type);
            attr_index++;

            try {
                tokenizer.nextToken();
            } catch (Exception te) {
            }
            token = tokenizer.sval;

        }
        if (attr_index != schema.size()) {
            System.out.println("\n\nERROR: Missing attribute types.");
            Exception e = new Exception();
            e.printStackTrace();
        }
        return true;
    }

    /**
     * This procedure post process the dataset for the case of "linear" or
     * "continuous" attribute values.
     */
    private void postProcessLinearValues() {
        String value_name;
        Attribute aux_attr;
        double min, max;

        int attr_index;
        for (int i = 0; i < schema.size(); i++) {/*
                                                  * if(attrsToRead != null &&
                                                  * !attrsToRead.get(i)) {
                                                  * continue; }
                                                  */

            aux_attr = (Attribute) schema.getAttribute(i);
            if (aux_attr.type == Attribute.CONTINUOUS) {
                // If it is linear, then we retrieve the min and max values
                // along all instances.
                min = Double.MAX_VALUE;
                max = Double.MIN_VALUE;

                FastInstance aux_inst = getFirstInstance();
                while (aux_inst != null) {
                    // If the value is "?" we should skip it.
                    int val = aux_inst.getValue(i);
                    /*
                     * if(aux_str.equalsIgnoreCase("?")) { aux_inst = (Instance)
                     * li.next(); continue; }
                     */
                    double aux_dbl = Double.parseDouble(aux_attr
                                                        .getValueName(val));

                    if (aux_dbl <= min)
                        min = aux_dbl;
                    if (aux_dbl >= max)
                        max = aux_dbl;

                    aux_inst = getNextInstance();
                    // it_index = li.nextIndex();
                }
                min = min;// For debugging purposes
                max = max; // For debugging purposes

                // Compute number of bins according to Sturge's rule (see David
                // W. Scott, "Multivariate density estimation: Theory, Practice,
                // and Visualization")
                int numBins = (int) (1.0 + Math.log(this.size())
                                     / Math.log(2.0));

                // Discretize the attribute

                Attribute temp_attr = new Attribute(aux_attr);
                aux_attr.discretizeValues(min, max, numBins);

                // Update the linear values in the dataset.
                aux_inst = getFirstInstance();
                while (aux_inst != null) {
                    int val = aux_inst.getValue(i);
                    /*
                     * if(aux_str.equalsIgnoreCase("?")) { aux_inst = (Instance)
                     * li.next(); continue; }
                     */
                    double aux_dbl = Double.parseDouble(temp_attr
                                                        .getValueName(val));
                    String value = aux_attr.getBinName(aux_dbl);
                    int valInd = aux_attr.getValueIndex(value);
                    if (aux_attr.getValueIndex(value) == 48) {
                        int kkkk = 0;
                    }
                    aux_inst.setValue(aux_attr.getValueIndex(value), i);

                    aux_inst = getNextInstance();

                }
            }
        }
    }

    // DRY java.util.Collections.shuffle
    public static FastDataset shuffle(final FastDataset dataset) {
	return shuffle(dataset, new java.util.Random());
    }
    // DRY java.util.Collections.shuffle
    public static FastDataset shuffle(final FastDataset dataset, java.util.Random rand) {
	FastDataset shuffled = new FastDataset("", dataset.schema());
        int[] indices =
            machineLearning.Utils.Utils.randomIntArray(dataset.size(), rand);

	for (int i : indices) shuffled.addInstance(dataset.getInstance(i));

	return shuffled;
    }

    public static FastDataset difference(FastDataset a, FastDataset b) {
	FastDataset diff = new FastDataset("", a.schema());
	diff.instances = new LinkedList<FastInstance>(a.instances);
	diff.instances.removeAll(b.instances);

	return diff;
    }

    public double
        getEmpiricalExpectation(int[] positions, int... values)
        throws Exception {
        FastInstance instance = getFirstInstance();
        // count the number of instance in the dataset that have values at
        // positions of instances
        int count = 0;

        while (instance != null) {
            if (instance.containsAll(positions, values)) count++;
            instance = getNextInstance();
        }

        return (double) count / size();
    }
}