package machineLearning.Datasets;

import java.util.*;
import java.io.*;
/**
 * Class dataSource
 *
 * Abstraction for a data source.<!-- --> Has means to connect to the datasource, reads its schema and
 * data. <!-- -->
 *
 * @author Facundo Bromberg
*/

abstract public class dataSource {
  /** Data source name. */
 protected String name;

  /** This could become a filename, a database dns, etc.*/
 protected String url;

 /** Stores the last read dataset */
 protected Dataset dataBuffer;

 /** Schema of the datasource */
 protected Schema schema;

 /** For incremental reading of data. */
 transient protected int rowPosition;

 /** Access methods */
 public String name(){return this.name;};
 public String url(){return this.url;};
 public Schema schema(){return this.schema;};
 public Dataset dataBuffer(){
 	return this.dataBuffer;
 	};

 /** Set methods */
 public void url(String url){this.url = url;};
 public void name(String name){this.name = name;};
 public void schema(Schema schema){this.schema = schema;};
 public void dataBuffer(Dataset dataBuffer){this.dataBuffer = dataBuffer;};


 public dataSource() { ; };
 public dataSource(String name, String url)
 {
   this.url = url;
   this.name = name;

   schema = null;
   dataBuffer = null;
 }

/*---------------------------------------------------*/
/**
 * @param buffersize = number of instances to retrieve (named because it simulates a bufferring of continuously-streaming data)
 * @return A Dataset containing the next buffersize records from the dataSource;
 */
 abstract public Dataset read(int buffersize, char separator);

/*---------------------------------------------------*/
/**
 * @return A Dataset containing all the records in the database
 */
 abstract public Dataset readAll(char separator);

/** sets the row pointer to zero */
 abstract public void reset();

 abstract public String toString();
}