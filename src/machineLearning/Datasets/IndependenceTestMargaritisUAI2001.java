package machineLearning.Datasets;

//import java.util.*;
import java.util.Random;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Map;
import java.util.Enumeration;

import machineLearning.Utils.*;
import machineLearning.Graphs.*;

//import weka.core.*;

/**
 * Created on Feb 01, 2006
 * 
 * @author Facundo Bromberg
 * 
 * Computes the probability of independence given data, i.e. Pr(M_I | D),
 * 
 * SEE NOTEBOOK DIMITRIS 6, pp33-35
 */
public class IndependenceTestMargaritisUAI2001 {
	static int MINIMUM_COUNTS = 0;		//if total counts in a contingency table (in unconditional) is lower than this number, it returns the prior
	public static int I = 0;
	public static int D = 1;
	
	static double totalGamma2 = 1.00;
	static double b = 0.01, a = 1.0;
    static double err_threshold = 1E-10;
	
	//--------------------------------------------------------------------------------
	//returns true if condtiionaly independent
	static public boolean conditional(double [] log_dl) {
		return log_dl[I] > log_dl[D];
	}
	static public double conditional_P(double [] log_dl, double prior) {
        double log_P = -Math.log1p(Math.exp(log_dl[1] - log_dl[0] + Math.log(1-prior) - Math.log(prior)));
        
        double P = Math.exp(log_P);
		return P;
	}
    //static public boolean conditional(FastDataset data, Attribute A,Attribute B, attributesSet condSet, double prior) {
    static public boolean conditional(FastDataset data, int A_i, int B_i, Set cond, double prior) {
       return conditional(data, A_i, B_i, cond, prior, null);   
    }
    static public boolean conditional(FastDataset data, int A_i, int B_i, Set cond, double prior, double [] logpvalue) {
		double [] log_dl = log_likelihoods(data, A_i, B_i, cond, prior);
		
        double log_P = -Math.log1p(Math.exp(log_dl[1] - log_dl[0] + Math.log(1-prior) + Math.log(prior)));
        double P = Math.exp(log_P);

        if(logpvalue != null) {
         //   pvalue[0] =  Math.exp(log_dl[IndependenceTestMargaritisUAI2001.I] - log_dl[IndependenceTestMargaritisUAI2001.D]);
            logpvalue[0] = log_P;
        }

        return P>0.5;//log_dl[I] > log_dl[D];
	}
    //--------------------------------------------------------------------------------
    //Return the log-data-likelihoods of the conditionals
    static public Hashtable readTables(FastDataset data, Attribute A,
            Attribute B, attributesSet condSet) {
        int N, val_A, val_B;
        int nvals_A, nvals_B;
        double[][] contTable;
        double P = 0.0; //probability to return
        double M1 = 0; //number of slices (configurations) that contains any
                       // data.
        nvals_A = A.size();
        nvals_B = B.size();

        String key;
        Hashtable contTables = new Hashtable(data.size());

        FastInstance ex = data.getFirstInstance();
        int k = 0;
        while (ex != null) {
            key = getHashCode(condSet.projectInstance(ex));

            if (!contTables.containsKey(key)) {
                contTable = new double[nvals_A][nvals_B];
                contTables.put(key, contTable);
                M1++;
            } else {
                contTable = (double[][]) contTables.get(key);
            }
            k++;

            val_A = ex.getValue(A.index());
            val_B = ex.getValue(B.index());

            contTable[val_A][val_B]++;

            ex = data.getNextInstance();
        }
        return contTables;  
    }
    //--------------------------------------------------------------------------------
    //Return the log-data-likelihoods of the conditionals
    static public double [] log_likelihoods(FastDataset data, int A_i,
        int B_i, Set cond, double prior) {

        attributesSet condSet = new attributesSet(cond);
        
        double M = numberOfSlices(condSet);
        double log_p, log_q, w = Math.pow(prior, 1.0/M);
        double log_w = 1.0/M*Math.log(prior);
        double log1p_w = Math.log1p(-w);
        //double aux3 = Math.log(1-w);
        double log_ai, log_bi;   //ai = pi * wi,  bi = qi*(1-wi)
        
        double [] log_dl = new double[2];
        
        Attribute A = data.schema().getAttribute(A_i);
        Attribute B = data.schema().getAttribute(B_i);
        if (A_i == B_i) {
            log_dl[I] = log_dl[D] = 1.0;
            return log_dl; /* Shortcut. */
        }

        /* Sanity checks. */
        if (A.size() <= 1 || B.size()<=1) {
            log_dl[I] = 1.0;
            log_dl[D] = 0.0;
            return log_dl;
        }

        Hashtable contTables = readTables(data, A, B, condSet); 


        double logP = 0;

        Enumeration en = contTables.elements();
        double[][] currTable;
      
        double [] log_udl;  //unconditional data likelihoods;
        
        log_dl[I] = log_dl[D] = 0.0;
        
        double log_A=0, log_BB=0;
        double aux, aux2;
        while (en.hasMoreElements()) {
            currTable = (double[][]) en.nextElement();
            
            log_udl = unconditionalLogDataLikelihoods(currTable, prior);
            log_p = log_udl[I];
            log_q = log_udl[D];

            log_ai = log_p + log_w;
            log_bi = log_q + log1p_w;
            
            //Log likelihood of independence model
            //log(Pr(D | M_CI)) = sum_i log(p_i)
            log_dl[I] += log_p;
            
            //Log likelihood of dependence model
            if(log_p>=log_q){
                //aux2 = log_bi - log_ai;
                //double aux3 = Math.exp(log_bi-log_ai);
                aux = Math.log1p(Math.exp(log_bi-log_ai));
                log_A += log_ai + aux;
                
                
                log_BB += -aux;
            }
            else{
                //aux2 = log_ai - log_bi;
                //double aux3 = Math.exp(log_ai - log_bi);
                aux = Math.log1p(Math.exp(log_ai - log_bi));

                log_A += log_bi + aux;
                
                //double aux5 = log_ai - log_bi - aux;
                log_BB += log_ai - log_bi - aux;

            }
        }
        if(log_BB == 0){
        	int kk=0;
        }
        double aux3 = Math.exp(log_BB);
        aux2 = Math.log1p(-Math.exp(log_BB));
        double aux4 = Math.log(-Math.expm1(log_BB));
        log_dl[D] = log_A - Math.log(0.5) + aux4;
        
        double PP = Math.exp(log_BB);
        double log_P = -Math.log1p(Math.exp(log_dl[1] - log_dl[0] + Math.log(1-prior) + Math.log(prior)));
        double P = Math.exp(log_P);
        double log_PP = log_BB;
       // System.out.println("log(P) (" + A.index() + "," + B.index() + ") = " + log_BB);
        
        return log_dl;  //if ratio of likelihoods (dep/ind) > 1, we got dependence
    }

    //--------------------------------------------------------------------------------
    //Return the log-data-likelihoods of the conditionals
    static public double [] log_likelihoods_FirstAttemptAfterAAAI06(FastDataset data, Attribute A,
            Attribute B, attributesSet condSet, double prior) {

        if(condSet.size() > 0){
         int k=0;   
        }
        double log_BB_threshold = -err_threshold - Math.log(1.0/err_threshold -1.0);
        log_BB_threshold = Math.log(1+err_threshold) -Math.log(err_threshold);
        double M = numberOfSlices(condSet);
        double log_p, log_q, w = Math.pow(prior, 1.0/M);
        double log_w = 1.0/M*Math.log(prior);
        double log_one_minus_w = Math.log(1-w);
        
        double [] log_dl = new double[2];
        
        if (A.index() == B.index()) {
            log_dl[I] = log_dl[D] = 1.0;
            return log_dl; /* Shortcut. */
        }

        /* Sanity checks. */
        if (A.size() <= 1 || B.size()<=1) {
            log_dl[I] = 1.0;
            log_dl[D] = 0.0;
            return log_dl;
        }

        Hashtable contTables = readTables(data, A, B, condSet); 


        double logP = 0;

        Enumeration en = contTables.elements();
        double[][] currTable;
      
        double [] log_udl;  //unconditional data likelihoods;
        
        log_dl[I] = log_dl[D] = 0.0;
        
        double log_AA=0, log_BB=0;
        while (en.hasMoreElements()) {
            currTable = (double[][]) en.nextElement();
            
            log_udl = unconditionalLogDataLikelihoods(currTable, prior);
            log_p = log_udl[I];
            log_q = log_udl[D];

            //Log likelihood of independence model
            //log(Pr(D | M_CI)) = sum_i log(p_i)
            log_dl[I] += log_p;
            
            //Log likelihood of dependence model
            log_AA += log_p + log_w;
            
            double log_bb =  log_p + log_w - log_q - log_one_minus_w ;
            
            if(log_p>=log_q){
            	log_BB += Math.log(1.0 + Math.exp(-log_bb));
            }
            else{
                log_BB += Math.log(Math.exp(log_bb) + 1) - log_bb;
            }
        }
        if(log_BB > log_BB_threshold)  log_dl[D] = log_AA - Math.log(1-prior) + log_BB; //approximation is safe  
        else if(log_BB == 0) {
        	log_dl[D] = log_AA - Math.log(1-prior);
        }
        else{
            log_dl[D] = log_AA - Math.log(1-prior) + Math.log(Math.exp(log_BB)-1.0);   
        }
//        double aux = log_dl[D] = log_AA - Math.log(1-prior) + log_BB;
//        double aux1 = log_AA - Math.log(1-prior) + Math.log(Math.exp(log_BB)-1.0);
//        double aux2 =aux1 - aux;
        
        //P = Math.exp(logP);
        double P = 1.0 / (1.0 + (1 - prior)
                / prior
                * Math.exp(log_dl[D] - log_dl[I]));

        return log_dl;  //if ratio of likelihoods (dep/ind) > 1, we got dependence
    }

	//--------------------------------------------------------------------------------
	//Return the log-data-likelihoods of the conditionals
	static public double [] log_likelihoods_AAAI06(FastDataset data, Attribute A,
			Attribute B, attributesSet condSet, double prior) {

        double [] log_dl = new double[2];
		
		if (A.index() == B.index()) {
			log_dl[I] = log_dl[D] = 1.0;
			return log_dl; /* Shortcut. */
		}

		/* Sanity checks. */
		if (A.size() <= 1 || B.size()<=1) {
			log_dl[I] = 1.0;
			log_dl[D] = 0.0;
			return log_dl;
		}

		Hashtable contTables = readTables(data, A, B, condSet); 


		double logP = 0;

		Enumeration en = contTables.elements();
		double[][] currTable;
		double M1_check = 0;
		//double priorSlice = Math.pow(prior, 1.0/M);
		
		double [] log_udl;	//unconditional data likelihoods;
		
		log_dl[I] = log_dl[D] = 0.0;
		
		double aD, aI, bD, bI;
		int threshold = -10;
		while (en.hasMoreElements()) {
			currTable = (double[][]) en.nextElement();
			
			log_udl = unconditionalLogDataLikelihoods(currTable, prior);
			
			aD = log_udl[D] + Math.log(1-a);
			aI = log_udl[I] + Math.log(a);
			bD = log_udl[D] + Math.log(1-b);
			bI = log_udl[I] + Math.log(b);

			if(aI > aD) {
				if(aD - aI > threshold) log_dl[I] += aI + Math.log( 1 + Math.exp(aD-aI) );
				else log_dl[I] += aI;
				
				log_dl[I] += aI + Math.log( 1 + Math.exp(aD-aI) );
			}
			else if(aI > aD) {
				if(aI - aD > threshold)log_dl[I] += aD + Math.log( 1 + Math.exp(aI-aD) );
				else log_dl[I] += aD;
				
				log_dl[I] += aD + Math.log( 1 + Math.exp(aI-aD) );
			}
			if(bI > bD) {
				if(bD-bI > threshold) log_dl[D] += bI + Math.log( 1 + Math.exp(bD-bI));
				else log_dl[D] += bI;
				
				log_dl[D] += bI + Math.log( 1 + Math.exp(bD-bI));
			}
			else {
				if(bI-bD > threshold) log_dl[D] += bD + Math.log( 1 + Math.exp(bI-bD));
				else log_dl[D] += bD;
				
				log_dl[D] += bD + Math.log( 1 + Math.exp(bI-bD));
			}

			M1_check++;
		}
		
		//P = Math.exp(logP);
		double P = 1.0 / (1.0 + (1 - prior)
				/ prior
				* Math.exp(log_dl[D] - log_dl[I]));

		return log_dl;	//if ratio of likelihoods (dep/ind) > 1, we got dependence
	}

    //--------------------------------------------------------------------------------
    //number of slices
    static final private double numberOfSlices( attributesSet condSet){
        double M = 1;
        for(int i=0; i<condSet.cardinality(); i++){
        	M *= condSet.getAttribute(i).size();
        }
        return M;
    }

	//--------------------------------------------------------------------------------
	//Returns an array with the log of data likelihoods of the unconditional test. Index 0 for independence, 1 for dependence.
	static final private double [] unconditionalLogDataLikelihoods(double [][] matrix, double prior){
		double [] dataLikelihoods = new double [2]; 
		
		int nrows, ncols, row, col;
		//double N = 0;

		nrows = matrix.length;
		ncols = matrix[0].length;

		//	  	these are the cI+, and c+J respectively
		double[] ciPlus = new double[nrows]; //was rtotal before
		double[] cPlusj = new double[ncols]; //was ctotal before

		double[] c = new double[nrows * ncols]; //flatten matrix

		double gammai = 1.0;//totalGamma / ((double) nrows * ncols); //these are the
															   // gammas_i,
															   // which are all
															   // the same and
															   // set the
															   // 1/#cells.
		double alphai = 1.0;//totalGamma / ((double) ncols); //these are the
													   // alphas_i, which are
													   // all equal to the
													   // number to 1/#cols.
		double betai = 1.0;//totalGamma / ((double) nrows); //these are the beta_i,
													  // which are all equal to
													  // the number to 1/#rows.

		double N = 0;
		for (row = 0; row < nrows; row++) {
			for (col = 0; col < ncols; col++) {
				ciPlus[row] += matrix[row][col];
				cPlusj[col] += matrix[row][col];
				c[row * ncols + col] = matrix[row][col];
				N += matrix[row][col];
			}
		}

		if(N < MINIMUM_COUNTS) {
			dataLikelihoods[0] = dataLikelihoods[1] = 1.0;
			return dataLikelihoods;
		}
		
        totalGamma2 = 0;
        for(int i=0; i<c.length;i++) totalGamma2 += gammai;
        
		dataLikelihoods[I] = logYfunction(ciPlus, alphai) + logYfunction(cPlusj, betai);
		dataLikelihoods[D] = logYfunction(c, gammai);
		
		return dataLikelihoods;
	}
	//	-----------------------------------------------------------------------
	//Returns an array with the log of data likelihoods of the conditional test. Index 0 for independence, 1 for dependence.
	/*static final private double [] conditionalLogDataLikelihoods(double [][] matrix, double prior){
		double [] dl = new double [2];
		
		dl[0] = 1.0; 
		dl[1] = 0.0;
		
		for(int i=0; i)
	}*/
	//-----------------------------------------------------------------------
	/**
	 */
	static private double unconditional(double[][] matrix, double prior) {
		double [] dataLikelihoods = unconditionalLogDataLikelihoods(matrix, prior);
		
		double pI = 1.0 / (1.0 + (1 - prior)
				/ prior
				* Math.exp(dataLikelihoods[D] - dataLikelihoods[I]));

		return pI;
	}

	/*
	 * ----------------------------------------------------------------------
	 * 
	 * Auxiliary functions, needed by chisquare().
	 * 
	 * ----------------------------------------------------------------------
	 */

	//--------------------------------------------------------------------------------
	static private double logYfunction(double c[], double gammai) {
		//totalGamma is supposed to be summer over all gammai, which are the
		// same and thus we collapsed into a single variable gammai

		double N = 0;
        totalGamma2 = 0;
		for (int i = 0; i < c.length; i++){
			N += c[i];
            totalGamma2 += gammai;
        }

		double ret = gammln(totalGamma2) - gammln(totalGamma2 + N);

		for (int i = 0; i < c.length; i++) {
			ret += gammln(gammai + c[i]);
			ret -= gammln(gammai);
		}

		return ret;
	}

	//--------------------------------------------------------------------------------
	static private String getHashCode(int[] a) {
		String aux = "empty";
		if (a.length > 0)
			aux = "" + a[0];
		for (int i = 1; i < a.length; i++)
			aux += "," + a[i];
		return aux;

	}

	//--------------------------------------------------------------------------------
	static double gammln(double xx) {
		double x, y, tmp, ser;
		double cof[] = { 76.18009172947146, -86.50532032941677,
				24.01409824083091, -1.231739572450155, 0.1208650973866179e-2,
				-0.5395239384953e-5 };
		int j;

		y = x = xx;
		tmp = x + 5.5;
		tmp -= (x + 0.5) * Math.log(tmp);
		ser = 1.000000000190015;
		for (j = 0; j <= 5; j++)
			ser += cof[j] / ++y;
		return -tmp + Math.log(2.5066282746310005 * ser / x);
	}
}

