package machineLearning.Datasets;

import java.util.*;
import java.io.*;

//import machineLearning.Utils.*;
import stringSerializer.*;
/**
 * Class Schema
 *
 * Implements the functionality of a Schmea with the attributes list;
 *
 * @author Facundo Bromberg
 * @version 1.2 Actualized for MAL.<!-- --> Splitted the concept of dataset and schema, now dataset contains a schema but it does not extend it.
 * @version 1.1 Actualized for the use in Final Project (BayesNetworks inference) of CS572, AI, Fall 2002.
 * @version 1.0 Splitted from Dataset for final project CS561 Spring 2003.
 */
public class Schema
{
	public String name;
	protected Vector attributes;
	protected Vector indexVariable;

	public Schema(){
	    this("noName");
	}
	public Schema(String _name){
		name = _name;
		attributes = new Vector(0, 5);
		indexVariable = new Vector(0, 5);
	}
	public Schema(int n){
		name = "noMame";
		attributes = new Vector(0, n);
	}
        /** returns the number of attributes */
        public int size(){return attributes.size();};
	/**
	 * Method that return the index of a value of the attr_index-th attribute.
	 */
	int getValueIndex(String value, int attr_index)
	{
	    Attribute attr = getAttribute(attr_index);
	    return attr.getValueIndex(value);
	}

	/**
	 * Procedure that returns a String representation of the Schema.
	 */
	public String toString()
	{
	    String Names ="";

	    Names = "Names Definitions: \n";
	    for(int i=0; i<attributes.size(); i++)
	    {
		Attribute attr = (Attribute) attributes.elementAt(i);
		Names = Names + attr.toString() + "\n";
	    }
	    return Names;
	}

	/**
	 * Procedure that returns a String in ARFF format of the Schema.
	 */
	public String toARFF()
	{
		String Names ="@RELATION " + "\"" + name + "\"" + "\n\n";

		for(int i=0; i<attributes.size(); i++)
		{
			Attribute attr = (Attribute) attributes.elementAt(i);
			Names += attr.toARFF() + "\n";
		}
		return Names;
	}
	/**
		 */
	public String toNamesDataFormat()
	{
		//String Names ="@RELATION " + name + "\n\n";
		Attribute attr = (Attribute) attributes.elementAt(attributes.size()-1);
		String Names = attr.name() + ".\n";

		//Names = "Names Definitions: \n";
		for(int i=0; i<attributes.size(); i++)
		{
			attr = (Attribute) attributes.elementAt(i);
			Names += attr.toNamesDataFormat() + "\n";
		}
		return Names;
	}

	/**
         * Make Copy creates a new dataset but do not add a single instance. The instances linked list is empty for the new created dataset.
         */
        public Schema makeACopy()
        {
	    Schema newD = (Schema) new Schema(name);

	    //Copy attributes Vector.
	    for(int i=0; i<attributes.size(); i++)
	    {
	      Attribute attr = getAttribute(i);
	      newD.addAttribute(attr);
	    }
	    return newD;
        }


	/**
	 * returns the number of attributes in the dataset.
	 */
	public int numberOfAttributes()
	{
		return attributes.size();
	}

	/**
	 * Adds an attribute to the dataset.
	 */
	public void addAttribute(Attribute n_attr){
	    indexVariable.addElement(n_attr.index());
	    attributes.addElement(n_attr);
	}
	/**
	 * Removes the attribute from the schema.
	 * @param index
	 */
	public void removeAttribute(int index){
		attributes.remove(index);
	}
	/**
	 * Retrieves the index-th attribute from the dataset.
	 */
	public Attribute getAttribute(int index){
		return (Attribute) attributes.elementAt(index);
	}

        /**
         * Retrieves the attribute named 'name'.
         */
        public Attribute getAttribute(String name)
        {
          int i=-1;
          for(i=0; i<numberOfAttributes(); i++)
          {
            if(name.equals(getAttribute(i).name()))
              break;
          }
          return (Attribute) attributes.elementAt(i);
        }

        /**
         * Retrieves the index of the attribute named 'name'.
         */
        public int getAttributeIndex(String name)
        {
          int i=0;
          for(i=0; i<numberOfAttributes(); i++)
          {
            if(name.equals(getAttribute(i).name()))
              break;
          }
                return i;
        }

	/**
	 * Set mark = false for all attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
	 */
        public void unmarkAllAttributes()
        {
          for(int i=0; i<attributes.size(); i++)
          {
            Attribute attr = getAttribute(i);
            attr.unmark();
          }
        }

        /**
         * Set mark = false for the i-th attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
         */
        public void markAttribute(int i)
        {
          Attribute attr = getAttribute(i);
          attr.mark();
        }

        /**
         * Return the mark flag of the i-th attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
         */
        public boolean marked(int i)
        {
          Attribute attr = getAttribute(i);
          return attr.marked();
        }

        /**
         * Return true if all attributes are marked (except the last one, which doesn't mater, recall it is the class attribute).
         */
        public boolean allMarked()
        {
          boolean ret = true;

          for(int i=0; i<attributes.size(); i++)
          {
            Attribute attr = getAttribute(i);
            if(!attr.marked()) ret = false;
          }
          return ret;
        }

        /**
         * Returns a class attribute value. It is assumed that the class attribute is the last attribute.
         * Input: the value index.
         * Output: The index-th value of the class attribute (last one).
         */
        public String getClassValue(int index)
        {
          Attribute attr = getAttribute(attributes.size()-1);
          return attr.getValueName(index);
        }
/**
         * Returns a class attribute value. It is assumed that the class attribute is the last attribute.
         * Input: the value index.
         * Output: The index-th value of the class attribute (last one).
         */
        public int getClassIndex()
        {
          return attributes.size()-1;
        }


	/** This function receives a string version of the Names File and
	 * convert it to the internal representation, that is, it creates
	 * the corresponding attribute structure.<!-- --> It assumes a .arff file format.
	 */
	public void parseNamesFile(String FileName)
	{
	    BufferedReader reader = null;
	    try{
		reader = new BufferedReader(new FileReader(FileName));
	    }
	    catch(Exception ioe){}

	    //int linePos = 0;
	    Attribute n_attr;
	    String attrName, token;
	    StreamTokenizer tokenizer = new StreamTokenizer(reader);

	    tokenizer.resetSyntax();
	    //tokenizer.whitespaceChars(0, ' ');
	    //tokenizer.wordChars(' '+1,'\u00FF');
	    tokenizer.wordChars(' ','\u00FF');
	    tokenizer.whitespaceChars(',',',');
	    tokenizer.whitespaceChars(':',':');
	    tokenizer.commentChar('|');
	    tokenizer.eolIsSignificant(true);

	    try {tokenizer.nextToken();}
	    catch (Exception te){}
	    token = tokenizer.sval;

	    //Skips the first line that contains "ClassField."
	    while(tokenizer.ttype != tokenizer.TT_EOL){
			try{tokenizer.nextToken();}
			catch (Exception te){}
			token = tokenizer.sval;
		}

	    //Reads until end of file
	    //Each iteration of the loop is basically parsing one line
	    //That is, at the beggining of the loop, the tokenizer should
	    //point to the first pointer in the line
	    int attr_index=0;
	    while(tokenizer.ttype != tokenizer.TT_EOF)
	    {
		//Reads attribute name.
		attrName = "PEPE";
		token = "";
		attrName = "";
		//READS ATTRIBUTE NAME
//		while(token.indexOf(':') == -1){

			try{tokenizer.nextToken();}
		    catch (Exception te){}
		    token = tokenizer.sval;

		    attrName = attrName + token;
//		}
		//attrName = attrName.substring(1, attrName.length()  -1 );
		//attrName = attrName.substring(0, attrName.length()  - );
		//                  attrName = attrName + token;
		n_attr = new Attribute(attrName, attr_index++);

		try{tokenizer.nextToken();}
		catch (Exception te){}
		token = tokenizer.sval;


		while(tokenizer.ttype != tokenizer.TT_EOL && tokenizer.ttype != tokenizer.TT_EOF)
		{

		    if(token.equalsIgnoreCase("date") ||
		       token.equalsIgnoreCase("time") ||
		       token.equalsIgnoreCase("ignore") ||
		       token.equalsIgnoreCase("label") 	//Attribute dont used for classification.
		       )
		    {
			n_attr.addValue("ignore");
			n_attr.ignore(true);

			try{tokenizer.nextToken();}
			catch (Exception te){}
			token = tokenizer.sval;

			continue;
		    }
		    //Take care of linear/continuous case.
		    //It will be processed later, when the dataset is readed.
		    //This is because we need min and max.
		    if(token.equalsIgnoreCase("linear")) {
		    	n_attr.type(Attribute.CONTINUOUS);
			n_attr.addValue("linear");

			try{tokenizer.nextToken();}
			catch (Exception te){}
			token = tokenizer.sval;
			continue;
		    }
		    if(token != null) token = token.trim();
		    n_attr.addValue(token);

		    try{tokenizer.nextToken();}
		    catch (Exception te){}
		    token = tokenizer.sval;
		}
		addAttribute(n_attr);

	    }
	}

        /**
     * Unparse to my format, this procedure works paired with myUnserialize.<!-- -->
     * The output string stores the current state of the calling object. This
     * string can be used later to set the state of another object to the same state
     * of the original object.<!-- --> Note that this is not storing objects, but only their
     * state (current value of internal fields).
     * @return a String.
     */

   public String mySerialize(Parser parser)
   {
 //    Parser parser = new Parser (pd, ed, vd, sd);

     Vector vblock = new Vector();

     //m_Counts block
     vblock.addElement(new Block("SCHEMANAMES", name));

     //sum of counts block
     Block blk2 = new Block("SCHEMAATTRIBUTES");
     for(int i=0; i<attributes.size(); i++) blk2.addValue(attributes.get(i), parser);
     vblock.addElement(blk2);


     return parser.filter(parser.unparseBlocks(vblock));
   }

   /**
   * Parse to my format, this procedure works paired with mySerialize.<!-- -->
   * The string output from this procedure can be use to change the state of any
   * Attribute object (values of internal
   * components and parameters) to the state of the current calling object.
   * @return a String.
   */
  public void myUnserialize(String inputStr, Parser parser)
  {
    Vector vblock = parser.parse(parser.unfilter(inputStr));

    for(int i=0; i<vblock.size(); i++)
    {
      Block blk = (Block) vblock.get(i);

       if(blk.tag.equals( "SCHEMANAMES"))
       {
         name = blk.getString(0);
       }

      else if(blk.tag.equals("SCHEMAATTRIBUTES"))
      {
        attributes = null;
        attributes = new Vector();
        for(int j=0; j<blk.size(); j++)
        {
          Attribute attr = (Attribute) blk.getObject(j, new Attribute(), parser);
          //Attribute attr = new Attribute("garbage");
          //blk.getObject(j, attr, parser);
          attributes.addElement(attr);
        }
      }

    }


  }
    public Vector getIndex() { return attributes; }
}
