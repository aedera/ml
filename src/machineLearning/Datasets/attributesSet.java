/*
 * Created on Mar 16, 2005
 */
package machineLearning.Datasets;

import java.util.*;

import machineLearning.Graphs.GraphRVar;
import machineLearning.Utils.Set;

/**
 * It has an iterator that goes through all possible configurations!.
 */
public class attributesSet extends Set implements Iterator{
        boolean hasNext = true;
        int indexes [];

        public attributesSet(){;};
        /**
         * @param U is a Set of GraphRVar
         */
        public attributesSet(Set U){
                if(U.size() == 0) return;

                for(int i=0; i<U.size(); i++){
                        Object curr = (Object) U.elementAt(i);
                        String aux = curr.getClass().getName();
                        if( aux.equals("machineLearning.Graphs.GraphRVar")){
                                add(((GraphRVar)curr).attr);
                        }
                        else add(curr);
                }
        }
        public void initIterator(){
                hasNext = true;
                indexes = new int[size()];

                for(int i=0; i<size();i++){
                        indexes[i]=0;
                }
        }
        public int [] currentAsArray(){
                return indexes;
        }

        /**
         * returns the current configuration as a set of values
         */
        public Object current(){
                Attribute attr;
                Vector a = new Vector(size());

                for(int i=0; i<size(); i++){
                        attr = ((Attribute)elementAt(i));
                        a.add(attr.getValueName(indexes[i]));
                }
                return a;
        }
        public int [] currentAsIntArray(){
                return indexes;
        }
        public boolean hasNext(){
                return hasNext;
                /*if(indexes[0]>=getAttribute(0).size()) return false;
                return true;*/
/*              for(int i=0; i<size(); i++){
                        if(indexes[i]<getAttribute(i).size()-1) return true;
                }
                return false;
*/      }

        public Object next(){
                int i=size()-1;
                while(i>=0 && indexes[i]>=getAttribute(i).size()-1 ) {
                        indexes[i] = 0;
                        i--;
                }
                if(i>=0) indexes[i]++;
                if(i<0) hasNext = false;
                return (Object)indexes;
        }

        public void remove(){;};

        public Attribute getAttribute(int i){return (Attribute)elementAt(i);};

        /**
         * returns the index in this of the attribute with schema-index attr_index.<!-- --> If attribute is not
         * in attributeSet, it returns -1;
         * @param attr_index
         * @return
         */
        public int hasAttribute(int attr_index){
                for(int j=0; j<size(); j++) {
                        int local_attr_index = getAttribute(j).index();
                        if(local_attr_index == attr_index) return j;
                }
                return -1;
        }
        /**
         * tests whether the instances values match with the current configuration values
         */
        public boolean matchWithInstance(FastInstance inst){
                int local_index=-1;
                Attribute attr = null;
                String currValue ="";

                for(int i=0; i<inst.size(); i++){
                        if((local_index=hasAttribute(i))>=0){
                                attr = getAttribute(local_index);
                                //currValue = attr.getValueName(indexes[local_index]);
                                if(inst.getValue(i)!=indexes[local_index]    ) return false;
                        }
                }
                return true;
        }

        public int [] projectInstance(FastInstance inst){
                int [] a = new int[size()];
                for(int i=0; i<size(); i++){
                        a[i] = inst.getValue(getAttribute(i).index());
                }
                return a;
        }

        public double numOfConfigurations(){
                double n = 1;
                for(int i=0; i<size();i++) {
                        n *= getAttribute(i).size();

                }
                return n;
        }
        public static void main(String[] args) {
        }
}
