/*
 * Created on Mar 16, 2005
 *
 *@author Facundo Bromberg
 */
package machineLearning.Datasets;

//import java.util.*;
import java.util.Random;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Map;
import java.util.Enumeration;

import machineLearning.Utils.*;
import machineLearning.Graphs.*;
//import weka.core.*;

/**
 * @author Facundo Bromberg
 *
 */
public class Statistics {
		/** If the number of counts in a cell is smaller than this number, don't add this cell when computing chi-square
		 /* A value of -1 set this method off. See Yaramakala04thesis, p.34 */
	static int p_brinMethod1 = 5;
	static double  df=0;
	//static double nCellsFailed;
	//double nTestsFailed=0;

	static double ess = 0;
	
	public static final int CHISQUARE = -1;
	public static final int MUTUALINFORMATION = -2;
	public static final int MARGARITIS_UAI2001 = -3;

	public static double meanLogDataLikelihoodRatio = 8.0;//.00000001; //
	public static double eta = 0.0;
	
	//Used in getVarWMaxMI
	static final public int MIN = -3;
    static final public int MAX = -4;
	static final public int AVERAGE = -5;
	
	public double error;	//for sampleLogDataLikelihoods
	public Statistics(){
		error=0;
	}

	
	/**
	 * if X~N(u, s^2) then a*X+b~N(au+b, (as)^2)), 
	 * therefore if X~N(0,1), as it is the case for nextGaussian(), then aX+b~N(b, a^2)
	 */
	static public double sampleFromGaussian(Random r, double mean, double sigma){
		return sigma*r.nextGaussian()+mean;	
	}
	
	public double [] sampledLogDataLikelihoods(Random r, MarkovNet trueNet, int X, int Y, Vector cond){
		double ret [] = new double[2];
		boolean independent = false;
		
//		independent=trueNet.separates(X, Y, cond);
		
		if(true){
			double rand = r.nextDouble()*(.5+eta);
			double probTest = independent?(1-rand): rand;
	
			double aux  = Math.sqrt(1.0/probTest-1.0);
			ret[IndependenceTestMargaritisUAI2001.I] = Math.log(1.0/aux);
			ret[IndependenceTestMargaritisUAI2001.D] = Math.log(aux);
	
			if((ret[IndependenceTestMargaritisUAI2001.I] > ret[IndependenceTestMargaritisUAI2001.D]) != independent) {
				error++;
			}
		}
		if(false){
			double rand = r.nextDouble();
			double probTest;
			if(rand < eta) {	//has error
				probTest = independent?(0.4): 0.6;
			}
			else {
				probTest = independent?(0.9999999): 1-0.9999999;
			}

			double aux  = Math.sqrt(1.0/probTest-1.0);
			ret[IndependenceTestMargaritisUAI2001.I] = Math.log(1.0/aux);
			ret[IndependenceTestMargaritisUAI2001.D] = Math.log(aux);
	
			if((ret[IndependenceTestMargaritisUAI2001.I] > ret[IndependenceTestMargaritisUAI2001.D]) != independent) {
				error++;
			}
			else{
				
			}
		}
		return ret;
	}

	/*	public void nTestsFailed(double nTestsF){
		nTestsFailed = nTestsF; 
	}
	public double nTestsFailed(){return nTestsFailed;};
*////////////////////////////////////////////////////
/////  CHI-SQUARE TESTS    ///////////////////////
///////////////////////////////////////////////////
    static public boolean independent(FastDataset data, int A_i, int B_i, Set cond, double confidence, boolean assumeIndependence, int testType, boolean [] testFailed){
    	return independent(data, A_i, B_i, cond, confidence, assumeIndependence, testType, testFailed, null );
    }
	static public boolean independent(FastDataset data, int A_i, int B_i, Set cond, double confidence, boolean assumeIndependence, int testType, boolean [] testFailed, double [] logpvalue){
        assumeIndependence = false;
		double val=0;
		if(testType == MARGARITIS_UAI2001){
			double prior = 0.5;
			return IndependenceTestMargaritisUAI2001.conditional(data, A_i,B_i, cond,  prior, logpvalue);
			
		}
		if(testType == CHISQUARE){
			val = chiSquareCond(data, A_i, B_i, cond,  assumeIndependence, testFailed, logpvalue); //return log of p_value!!!!
            
			//if(pvalue != null) pvalue[0] = val;
			return val>confidence;
		}
		return assumeIndependence;
	}

	  /**
	   * Computes chi-value for one cell in a contingency table.
	   *
	   * @param freq the observed frequency in the cell
	   * @param expected the expected frequency in the cell
	   * @return the chi-value for that cell; 0 if the expected value is
	   * too close to zero 
	   */
	  private static double chiCell(double freq, double expected, 
	                                boolean yates){

	    // Cell in empty row and column?
	    if (expected <= 0) {
	      return 0;
	    }

	    // Compute difference between observed and expected value
	    double diff = Math.abs(freq - expected);

	    // Return chi-value for the cell
	    return (diff * diff / expected);
	  }

	  /**
	   * Computes chi-squared statistic for a contingency table.
	   *
	   * @param matrix the contigency table
	   * @param yates is Yates' correction to be used?
	   * @return the value of the chi-squared statistic
	   */
	  static public double chiVal(double [][] matrix, int [] nCellsFailed) {
	    int nrows, ncols, row, col;
	    double[] rtotal, ctotal;
	    double expect = 0, chival = 0, n = 0;
	    boolean yates = false;
	    
	    nrows = matrix.length;
	    ncols = matrix[0].length;
	    rtotal = new double [nrows];
	    ctotal = new double [ncols];
	    for (row = 0; row < nrows; row++) {
	      for (col = 0; col < ncols; col++) {
	      	rtotal[row] += matrix[row][col];
	      	ctotal[col] += matrix[row][col];
	      	n += matrix[row][col];
	      }
	    }
	   
	    chival = 0.0;
	    for (row = 0; row < nrows; row++) {
          	for (col = 0; col < ncols; col++) {
                if(ctotal[col] == 0 || rtotal[row] == 0) {
                    df--;
                    continue;
                }
      			expect = (ctotal[col] * rtotal[row]) / n;
      			if(expect != 0) chival += Math.abs(matrix[row][col] - expect) * Math.abs(matrix[row][col] - expect)/ expect;
      			//chival += chiCell (matrix[row][col], expect, yates);
      			if(expect < 5.0) {
      				nCellsFailed[0]++;
      			}
          	}
	    }
	    return chival;
	  }

	
	static private String getHashCode(int [] a){
		String aux= "empty";
		if(a.length > 0) aux = "" + a[0];
		for(int i=1; i<a.length; i++) aux += ","+a[i];
		return aux;

	}
	
	//If assumeIndepende = true, it will return ture in case of unreliable tests.
    static public double chiSquareCond(FastDataset data, int A_i, int B_i, Set cond, boolean assumeIndependence, boolean [] nTestsFailed){
        return chiSquareCond(data, A_i, B_i, cond, assumeIndependence, nTestsFailed, null);
    }
	static public double chiSquareCond(FastDataset data, int A_i, int B_i, Set cond, boolean assumeIndependence, boolean [] nTestsFailed, double [] pvalue){

		  int numCells, val_A, val_B;
		  //unsigned long conf, nconfs;
		  double chsq=0, chsq1=0;
		  int nvals_A, nvals_B;
		  double [][] contTable;
		  double pr=0.0, pr1=0;

          Attribute A = data.schema().getAttribute(A_i);
          Attribute B = data.schema().getAttribute(B_i);
          attributesSet condSet = new attributesSet(cond);
          
		  if (A.index() == B.index())
		    return 1.0;   /* Shortcut. */

		  nvals_A = A.size();
		  nvals_B = B.size();
		  
		  /* Sanity checks. */
		  if (nvals_A <= 1) {
		    return 0.0;
		  }
		  if (nvals_B <= 1) {
		    return 0.0;
		  }

		  //for (conf = 0; conf < nconfs; conf++) {
		  int numConf=0;
		  String key;
		  Hashtable contTables = new Hashtable(data.size());
		  
	  	FastInstance ex = data.getFirstInstance();
	  	int k=0;
	  	while(ex != null){
	  		key = getHashCode(condSet.projectInstance(ex));
	  		
	  		if(!contTables.containsKey(key)){
	  			contTable = new double[nvals_A][nvals_B];
	  			contTables.put(key, contTable);
	  			numConf++;
			}
	  		else{
	  			contTable = (double [] []) contTables.get(key);
	  		}
	  		k++;
	    	
	  		val_A = ex.getValue(A.index());
		  	val_B = ex.getValue(B.index());
		  	
		  	contTable[val_A][val_B]++;
		  	
		  	ex = data.getNextInstance();
	    }
	  	
	  	
		double numConfigs = condSet.numOfConfigurations();
		int [] nCellsFailed= new int[1];
		
		//df = (nvals_A-1)*(nvals_B-1)*numConf;//
        df = (nvals_A*nvals_B-1)*numConf;//
        //df = 0;
		numCells = (nvals_A-1)*(nvals_B-1);
		chsq = 0.0;
			  
		Enumeration configs = contTables.keys();
		//System.out.println(" ");
 		while(configs.hasMoreElements()){
			contTable = (double [][])contTables.get(configs.nextElement());
		
		  	chsq += chiVal(contTable, nCellsFailed);
            //df += (nvals_A*nvals_B-1);//

		  }
		double ret;
		double ratio = (double)nCellsFailed[0]/((double)nvals_A*nvals_B*numConf);
        
       //if(1.0-ratio < 0.8 || df<=0){
		if( ratio >= 0.4 || df<=0){
			//System.out.println("1-nCellsFailed/(numCells*numConfigs<0.2) in dataset: " + data.name + ", nCellsFailed: " + nCellsFailed +", numConfigs: " + numConfigs+ ", numCells" + numCells+", df: "+df+   ">>> assumedIndependence="+assumeIndependence);
			if(nTestsFailed != null) {
				nTestsFailed[0] = true;
			}
			if(assumeIndependence) return 1.0;
		}
        if(chsq == 0) return 0;
//        double aux = gammq(1, 5);
//        aux = gammq(1, 0.0);
        
        double aux = ln_gammq(0.5 * df, 0.5 *chsq);
        
        if(pvalue != null) pvalue[0] = aux;
        
        /*double aux2 = gammq(0.5*df, 0.5*chsq);
        double aux3 = Math.exp(aux);
        Utils.FORCED_ASSERT(Math.abs(aux2 - aux3)/aux2<0.1, "");
    */	
        return Math.exp(aux);
    	
        /*double aux = gammq(0.5 * df, 0.5 *chsq);
        
        if(pvalue != null) pvalue[0] = aux;
    	return aux;*/
	}

	/* ----------------------------------------------------------------------

	   Auxiliary functions, needed by chisquare().

	   ---------------------------------------------------------------------- */
	/* Forward declarations. */

	static double gamser, gammcf, gln;
	static double FPMIN = 1.0e-30;
	static int ITMAX = 2000;
	static double EPS = 3.0e-7;
	static void nerror(String msg)
	{
	  System.out.println("%s" + msg);
	  System.exit(1);
	}

	public static double gammq(double df, double x)
	{
	  
	  if (x < 0.0 || df <= 0.0){
	    nerror("Invalid arguments in routine gammq()");
	    (new Exception()).printStackTrace();
	    System.exit(0);
	  }
	  if (x < (df + 1.0)) {
   	    if(x == 0) return 1;
   	    
   	    gser(df, x);
	    return 1.0 - gamser;
	  } else {
	    gcf(df, x);
	    return gammcf;
	  }
	}

	static void gser(double df, double x)
	{
	  int n;
		double sum, del, ap;

		gln = gammln(df);
		if (x < 0.0) {
			nerror("x less than 0 in routine gser()");
			return;
		} else {
			ap = df;
			del = sum = 1.0 / df;
			for (n = 1; n <= ITMAX; n++) {
				++ap;
				del *= x / ap;
				sum += del;
				if (Math.abs(del) < Math.abs(sum) * EPS) {
					gamser = sum * Math.exp(-x + df * Math.log(x) - (gln));
					return;
				}
			}
			nerror("a too large, ITMAX too small in routine gser()");
			return;
		}
	}

	
	static void gcf(double a, double x)
	{
	  int i;
	  double an, b, c, d, del, h;

	  gln = gammln(a);
	  b = x + 1.0 - a;
	  c = 1.0 / FPMIN;
	  d = 1.0 / b;
	  h = d;
	  for (i = 1; i <= ITMAX; i++) {
	    an = -i * (i - a);
	    b += 2.0;
	    d = an * d + b;
	    if (Math.abs(d) < FPMIN) d = FPMIN;
	    c = b + an / c;
	    if (Math.abs(c) < FPMIN) c = FPMIN;
	    d = 1.0 / d;
	    del = d * c;
	    h *= del;
	    if (Math.abs(del - 1.0) < EPS) break;
	  }
	  if (i > ITMAX)
	    nerror("a too large, ITMAX too small in gcf()");
	  gammcf = Math.exp(-x + a * Math.log(x) - (gln)) * h;
	}

	static double gammln(double xx)
	{
	  double x, y, tmp, ser;
	   double cof[] = {
	    76.18009172947146, -86.50532032941677,
	    24.01409824083091, -1.231739572450155,
	    0.1208650973866179e-2, -0.5395239384953e-5
	  };
	  int j;

	  y = x = xx;
	  tmp = x + 5.5;
	  tmp -= (x + 0.5) * Math.log(tmp);
	  ser = 1.000000000190015;
	  for (j = 0; j <= 5; j++)
	    ser += cof[j] / ++y;
	  return -tmp + Math.log(2.5066282746310005 * ser / x);
	}

	////////////////////////////////////////////////////////////////
	//// FUNCTION FOR COMPUTING log_gammq
	//////////////////////////////////////////////////////////////////
	public static double ln_gammq(double df, double x)
	{
	  
	  if (x < 0.0 || df <= 0.0){
	    Utils.FORCED_ASSERT(true, "Invalid arguments in routine gammq()");
	  }
	  if (x < (df + 1.0)) {
   	    if(x == 0) return 0;
   	    
	    return Math.log(-Math.expm1(ln_gser(df, x)));
	  } else {
	    return ln_gcf(df, x);
	  }
	}
	
	static double ln_gser(double df, double x)
	{
	  int n;
		double sum, del, ap;

		gln = gammln(df);
		if (x < 0.0) {
			Utils.FORCED_ASSERT(true, "x less than 0 in routine gser()");
			return -1;
		} else {
			ap = df;
			del = sum = 1.0 / df;
			for (n = 1; n <= ITMAX; n++) {
				++ap;
				del *= x / ap;
				sum += del;
				if (Math.abs(del) < Math.abs(sum) * EPS) {
					//gamser = sum * Math.exp(-x + df * Math.log(x) - (gln));
					return Math.log(sum) -x + df * Math.log(x) - (gln);
				}
			}
			Utils.FORCED_ASSERT(true, "a too large, ITMAX too small in routine gser()");
			return -1;
		}
	}

	static double ln_gcf(double a, double x)
	{
	  int i;
	  double an, b, c, d, del, h;

	  gln = gammln(a);
	  b = x + 1.0 - a;
	  c = 1.0 / FPMIN;
	  d = 1.0 / b;
	  h = d;
	  for (i = 1; i <= ITMAX; i++) {
	    an = -i * (i - a);
	    b += 2.0;
	    d = an * d + b;
	    if (Math.abs(d) < FPMIN) d = FPMIN;
	    c = b + an / c;
	    if (Math.abs(c) < FPMIN) c = FPMIN;
	    d = 1.0 / d;
	    del = d * c;
	    h *= del;
	    if (Math.abs(del - 1.0) < EPS) break;
	  }
	  if (i > ITMAX)
	    nerror("a too large, ITMAX too small in gcf()");
	  return -x + a * Math.log(x) - (gln) + Math.log(h);
	}

static public double mutualInformation(Attribute X, Attribute Y, FastDataset data){
	double mi = 0;
	double pxy [] = new double [1];
	double px [] = new double [1];
	double py [] = new double [1];
	double aux;
	
	
	attributesSet XYset = new attributesSet();
	XYset.add(0,X);
	XYset.add(1,Y);
	
	attributesSet Xset = new attributesSet();
	Xset.add(X);

	attributesSet Yset = new attributesSet();
	Yset.add(Y);
	
	Hashtable XYhash = superFastProbability(data, XYset, ess);
	Hashtable Xhash = superFastProbability(data, Xset, ess);
	Hashtable Yhash = superFastProbability(data, Yset, ess);
	
	Enumeration configsXY = XYhash.keys();
	while(configsXY.hasMoreElements()){
		String keyXY = (String) configsXY.nextElement();
		pxy = (double [])XYhash.get(keyXY);
		
		//Actually, the order is irrelevant here, as long as one is X and the other is Y.
		String keyX = keyXY.substring(0, keyXY.indexOf(","));
		String keyY = keyXY.substring(keyXY.indexOf(",")+1, keyXY.length());
		
		px = (double [])Xhash.get(keyX);
		py = (double [])Yhash.get(keyY);
		
		aux = pxy[0]/(px[0]*py[0]);
		aux = Math.log(aux);
		aux = pxy[0]*aux;
		mi += aux;
	}
	
	return mi;
}

public static  double conditionalMutualInformation(Attribute X, Attribute Y, Set cond, FastDataset data){
	double mi = 0;
	double pxyc [] = new double [1];
	double pxc [] = new double [1];
	double pyc [] = new double [1];
	double pc [] = new double [1];
	double aux;
	
	
	attributesSet XYC = new attributesSet(cond);
	XYC.add(0,Y);
	XYC.add(0,X);
	
	attributesSet XC = new attributesSet(cond);
	XC.add(0, X);

	attributesSet YC = new attributesSet(cond);
	YC.add(0, Y);
	
	Hashtable XYChash = superFastProbability(data, XYC, ess);
	Hashtable XChash = superFastProbability(data, XC, ess);
	Hashtable YChash = superFastProbability(data, YC, ess);
	Hashtable Chash = superFastProbability(data, new attributesSet(cond), ess);
	
	Enumeration configsXYC = XYChash.keys();
	while(configsXYC.hasMoreElements()){
		String keyXYC = (String) configsXYC.nextElement();
		pxyc = (double [])XYChash.get(keyXYC);
		
		//Actually, the order is irrelevant here, as long as one is X and the other is Y.
		int comma = keyXYC.indexOf(",");
		String keyXC = keyXYC.substring(0, comma);
		int comma2 = keyXYC.indexOf(",", comma+1);
		String keyYC="";
		if(comma2>=0) keyYC = keyXYC.substring(comma+1, comma2);
		else keyYC = keyXYC.substring(comma+1, keyXYC.length());
		
		String keyC = "";
		if(comma2>=0) keyC = keyXYC.substring(comma2+1, keyXYC.length());
		
		if(comma2 >=0){
			keyXC = keyXC + "," + keyC;
			keyYC = keyYC + "," + keyC;
		}
		
		pxc = (double [])XChash.get(keyXC);
		pyc = (double [])YChash.get(keyYC);
		pc = (double [])Chash.get(keyC);
		
		aux = pxyc[0]*pc[0]/(pxc[0]*pyc[0]);
		aux = Math.log(aux);
		aux = pxyc[0]*aux;
		mi += aux;
	}
	return mi;
}

/*static public double computeUnconditionalTest(Set V, FastDataset data, int x, int y, int testType, boolean assumeIndependence, Set cond){
	double ret=0;
	
	Attribute X = ((GraphRVar)V.elementAt(x)).attr;
	Attribute Y = ((GraphRVar)V.elementAt(y)).attr;
	
	if(testType == Statistics.MUTUALINFORMATION) ret = mutualInformation(X, Y, data);
	else if(testType == Statistics.CHISQUARE) {
		ret= chiSquareCond(data, X,Y, new attributesSet(cond),  assumeIndependence, null);
	}
	else if (testType == Statistics.MARGARITIS_UAI2001) {
		double [] log_dl = IndependenceTestMargaritisUAI2001.log_likelihoods(data, X, Y, new attributesSet(cond), 0.5);
		ret = Math.exp(log_dl[IndependenceTestMargaritisUAI2001.I] - log_dl[IndependenceTestMargaritisUAI2001.D]);
	}
	return ret;
}*/

/**
 * From variable in G not in F, it checks its mi with all other variables and stores its maximum if type==MAX, or average if type==AVERAGE. Then, it compares these maximums, and 
 * return the variable with the maximum maximum. This method is used to choose then ext variable to Grow-Shrink (i.e. visit).
 * @param G
 * @return
 */
static public GraphicalModelNode getVarWExtremeUT(double [][] mi, Set V, Set F, int type, int testType){
	int n = V.size();
	GraphicalModelNode maxVar=null;
	double average = 0, maxint, max, compAux;
	
	max = Double.MAX_VALUE;
    //max = 0;
	nextI: for(int i=0; i<n; i++){
        GraphicalModelNode X = (GraphicalModelNode) V.elementAt(i);
        if(X.attr.index() == 15){
        	int kks=0;
        }
        for(int j=0;j<F.size();j++) {
            int kk=((GraphicalModelNode)(F.elementAt(j))).attr.index();
            int kk2 = X.attr.index();
            if(((GraphicalModelNode)(F.elementAt(j))).attr.index() == X.attr.index()) {
                continue nextI;
            }
        }
		//if(F.contains(X)) continue;
		
		//maxVar = X;
		
		average = 0;
		//maxint = 0;
        maxint = Double.MAX_VALUE;
		for(int j=0; j<n; j++){
			if(i == j) continue;

			if(mi[i][j] < 0.0){
				System.out.println("ERROR in Statistics.getVarWExtremeUT: mi[][] < 0, i.e., not initialized.");
				(new Exception()).printStackTrace();
				System.exit(1);
			}

			if(mi[i][j] <= maxint) maxint = mi[i][j];
			average += mi[i][j]; 
		}
		average /= (double)n;
		
		if(type == MAX) compAux = maxint;
		else if(type == AVERAGE) compAux = average;
		else{
			compAux = 0;
			System.out.println("ERROR: invalid type in Statistics.getVarWMaxMI");
			(new Exception()).printStackTrace();
		}
		
		if(testType == Statistics.CHISQUARE || testType == Statistics.MARGARITIS_UAI2001){
			//if(compAux >= max) {
            if(compAux <= max) {
				max = compAux;
				maxVar = X;
			}
		}
	}

	return maxVar;
}
/** Given variable X, it return the variable Y (not in F) for which the mi (X,Y) is maximum. This method is used to choose the next variable
 * during the growing phase of X.
 */
static public GraphicalModelNode getVarMostIndependent(double [][] mi, Set V, GraphicalModelNode X, Set F, int testType){
	GraphicalModelNode Var=null;
	double extreme;
	
	if(testType == Statistics.CHISQUARE || testType == Statistics.MARGARITIS_UAI2001){
		extreme = Double.MAX_VALUE;
        //extreme = 0;
		
		for(int i=0; i<mi[X.index()].length; i++){
			GraphicalModelNode Y = (GraphicalModelNode) V.elementAt(i);
			if(F.contains(Y)) continue;  //this should skip X itself, since X \in F.
			
			//if(mi[X.index()][i] >= extreme) {
            if(mi[X.index()][i] <= extreme) {
				extreme = mi[X.index()][i];
				Var = Y;
			}
		}
	}
	return Var;
}
////////////////////////////////////
////PROBABILITIES ///////////////
////////////////////////////////
	/**
	 * Returns the empirical distribution P(T ,C=c) for each value of t of T. 
	 */
	static double [] probabilityDist(FastDataset data, GraphRVar T, attributesSet C, int [] c, double ess){
		int TnumValues = T.attr.size();
		double [] p = new double[TnumValues];
		attributesSet A = new attributesSet(C);	// A = {C,T}
		
		int [] a = new int[c.length+1];
		for(int i=0;i<c.length;i++) a[i]=c[i];
		
		A.addElement(T);
		for(int i=0; i<TnumValues; i++){
			a[a.length-1] = i; //addElement adds the element at the last position
			
//			P(T=t,C=c)/P(C=c) = P(T=t | C=c)
			p[i] = probability(data, A, a, ess);
		}
		
		return p;
	}
	/**
	 * Returns the empirical distribution P(T | C) computed from the input data, where T is a single variable, C 
	 * the conditioning set, is a set of variables, and c, a set of Strings, is the assignment of values for C.
	 */
	static double [] condProbabilityDist(FastDataset data, GraphRVar T, attributesSet C, int [] c, double ess){
		int TnumValues = T.attr.size();
		double [] p = new double[TnumValues];
		attributesSet A = new attributesSet(C);
		
		int [] a = new int[c.length+1];
		for(int i=0;i<c.length;i++) a[i]=c[i];
		
		A.addElement(T);
		double p2 = probability(data, C, c, ess);
		for(int i=0; i<TnumValues; i++){
			a[a.length-1] = i;//T.attr.getValueName(i);  //addElement adds the element at the last position
			
//			P(T=t,C=c)/P(C=c) = P(T=t | C=c)
			double p1 = probability(data, A, a, ess);
			//p[i] = p2!=0? p1 / p2 : 1.0;
			if(p2 ==0) p[i] = 1.0;
			else p[i] = p1/p2;
		}
		
		return p;
	}
	
	public static long count(FastDataset data, attributesSet A, int [] a, double ess){
		return (long) probability(data, A, a, ess, false);
	}
	public static double probability(FastDataset data, attributesSet A, int [] a, double ess){
		return probability(data, A, a, ess, true);
	}
	/**
	 * computes the empirical probability of the variables (GraphRVar) in set of variables "A: taking values 
	 * in set of values (Strings) "a" 
	 */
	public static double probability(FastDataset data, attributesSet A, int [] a, double ess, boolean normalize){
		double N=data.size();
		double n=0.0;
		Attribute curr;
		int val, exVal;
		boolean eq;
		
		FastInstance ex = data.getFirstInstance();
		while(ex!= null){
			eq = true;
			for(int i=0; i<A.size(); i++){
				curr = (Attribute) A.elementAt(i);
				val = a[i];
				//val = curr.attr.getValueIndex((String)a.elementAt(i));
				exVal = ex.getValue(curr.index());
				eq = (val==exVal);
				if(!eq) break;
			}
			if(eq) n++;
			ex = data.getNextInstance();
		}
		if(!normalize) return n;
		
		
		//Normalization
		double nVals = 1;
		for(int i=0; i<A.size(); i++) {
			Attribute attr = (Attribute)A.elementAt(i);
			nVals *= attr.size();
		}
		return (n+ess/nVals)/(N+ess);
	}

	static Hashtable superFastProbabilityDist(FastDataset data, GraphRVar T, attributesSet C, double ess){
		double N=data.size();
		double [] p =null;
		Attribute curr;
		int val, exVal;
		boolean eq;
		Hashtable P = new Hashtable(data.size());
		String key;
		
		FastInstance ex = data.getFirstInstance();
		while(ex!= null){
	  		key = getHashCode(C.projectInstance(ex));
	  		
	  		if(!P.containsKey(key)){
	  			p = new double[T.attr.size()];
	  			P.put(key, p);
			}
	  		else{
	  			p = (double []) P.get(key);
	  		}
	  		int Tvalue = ex.getValue(T.index());
	  		p[Tvalue]++;

	  		ex = data.getNextInstance();
		}
		Enumeration configs = P.keys();
		while(configs.hasMoreElements()){
			p = (double [])P.get(configs.nextElement());
			double sum = 0.0;
			for(int i=0; i<T.attr.size();i++) {
				p[i] = (p[i]+ess/C.numOfConfigurations())/(N+ess);
				sum += p[i];
			}
			if(sum > 1.1){
				int k=0;
			}
		  }
		return P;
	}

	public static Hashtable superFastProbability(FastDataset data, attributesSet A, double ess){
		Hashtable P = superFastCounts(data, A);
		double N=data.size();
		double [] p ={0};
		
		Enumeration configs = P.keys();
		while(configs.hasMoreElements()){
			p = (double [])P.get(configs.nextElement());
			p[0] = (p[0]+ess/A.numOfConfigurations())/(N+ess);
		  }
		return P;
	}
	
	public static Hashtable superFastCounts(FastDataset data, attributesSet A){
		double [] p ={0};
		Attribute curr;
		int val, exVal;
		boolean eq;
		Hashtable P = new Hashtable(data.size());
		String key;
		
		FastInstance ex = data.getFirstInstance();
		if(A.isEmpty()){
			int k=0;
		}
		while(ex!= null){
	  		key = getHashCode(A.projectInstance(ex));
	  		
	  		if(!P.containsKey(key)){
	  			p = new double[1];
	  			P.put(key, p);
			}
	  		else{
	  			p = (double []) P.get(key);
	  		}
	  		p[0]++;

	  		ex = data.getNextInstance();
		}
		
		return P;
	}

	
	///////////////////////////////////////////////////
/////  KL-DIVERGENCE        ///////////////////////
///////////////////////////////////////////////////
	/**
	 * Networks fitness
	 */
	public static double KLFitnessOfNetwork(FastDataset data, MarkovNet MN, double ess){
		return KLFitnessOfNetwork(data, MN, ess, null);
	}
	public static double KLFitnessOfNetwork(FastDataset data, MarkovNet MN, double ess, Vector output)
	{
		double fit = 0;
		Set neighbors = new Set(0);
	
		//System.out.println("KLFitnessNetwork started for a network of size: " + MN.size());
		if(output != null) output.clear();
		for(int i=0; i<MN.size(); i++){
			if(i == 183){
				int k=0;
			}
			GraphRVar T = (GraphRVar) MN.V().elementAt(i);
//			double fitAux = KLFitnessOfVariable(data, T, new Set(T.adjacencyList()), new Set(MN.V()), ess);
//			Double FITAUX = new Double(fitAux);
//			//if(FITAUX.isNaN()) fitAux = 0.0;
//			fit += fitAux;
//			if(output != null)output.add(i, new Double(fitAux));
		}
		//System.out.println("fit: " + fit + ", MN.size(): " + MN.size() + " ratio: " + fit/(double)MN.size());
		return fit/(double)MN.size();
	}
	/**
	 * Explained in detail at Sandeeps Yaramakala's MS Thesis. <!-- --> U is the universe of variables
	 */
	public static double KLFitnessOfVariable(FastDataset data, GraphRVar T, Set B, Set U, double ess){
		//System.out.println("KLFitnessVariable started: " + T + ",  " + B + ",  " + U);
		Set rest = Set.minus(U, B);
		rest.remove(T);
		if(rest.size() == 0) return 0;
		
		double deltaT = 0;
		//System.out.println("rest.size() " + rest.size());
		for(int i=0; i<rest.size(); i++){
			GraphRVar X = (GraphRVar) rest.elementAt(i);
			double sfe = expectedKL(data, T, B, X, ess);
			
			//double e = expectedKLsuperSlow(data, T, B, X, ess);
			//System.out.println(sfe + ",  " + e);
			deltaT += sfe;
		}
		//System.out.println("deltaT: " + deltaT);
		//deltaT = deltaT/(double)rest.size();
		//System.out.println("Fitness of " + T.index() + " = " + deltaT);
		return deltaT;
	}
	/**
	 * Sum_{b,x} P(B=b,X=x).KL[ P(T|B=b,X=x) ||  P(T|B=b) ]
	 */
	public static double expectedKLsuperSlow(FastDataset data, GraphRVar T, Set _B, GraphRVar X, double ess){
		Set _BX = new Set(_B);
		int conf [];
		double EKL = 0, p1=0, paux, p2[];
		
		_BX.add(X);		// _BX = {B,X}
		attributesSet BX = new attributesSet(_BX);
		
		BX.initIterator();
		int[] b, bx;
		while(BX.hasNext()){
			bx = BX.currentAsIntArray();
			b = new int[bx.length-1];
			for(int i=0;i<bx.length-1;i++) b[i]=bx[i];
			
			//double p1 = probability(data, _BX, bx, ess);
			paux = probability(data, new attributesSet(_B), b, ess);
			//double [] p2 = condProbabilityDist(data, T, _BX, bx);
			p2 = probabilityDist(data, T, BX, bx,ess);
			for(int i=0; i<p2.length;i++) p1 += p2[i];
			for(int i=0; i<p2.length; i++) {
				if(p1 ==0) p2[i] = 0.0;
				else p2[i] /= p1;
			}
			double [] p3 = probabilityDist(data, T, new attributesSet(_B), b, ess);
			for(int i=0; i<p3.length; i++) {
				if(paux ==0) p3[i] = 1.0;
				p3[i] /= paux;
			}
			//double [] p3 = condProbabilityDist(data, T, _B, b);
			EKL += p1*KL(p2 , p3);
			
			b = null;
			BX.next();
		}
		return EKL;
	}
	public static double expectedKL(FastDataset data, GraphRVar T, Set _B, GraphRVar X, double ess){
		Set _BX = new Set(_B);
		int conf [];
		double EKL = 0, p1=0, paux, p2[];
		
		_BX.add(X);		// _BX = {B,X}
		attributesSet BX = new attributesSet(_BX);
		
		Hashtable PBX = superFastProbability(data, BX, ess);
		Hashtable PTBX = superFastProbabilityDist(data, T, BX, ess);
		Hashtable PTB = superFastProbabilityDist(data, T, new attributesSet(_B), ess);
		Hashtable PB = superFastProbability(data, new attributesSet(_B), ess);
		
		double [] pbx, pb, ptbx, ptb;
		Enumeration configsBX = PTBX.keys();
		while(configsBX.hasMoreElements()){
			String keyBX = (String) configsBX.nextElement();
			pbx = (double [])PBX.get(keyBX);
			ptbx = (double [])PTBX.get(keyBX);
			String keyB; 
			if(keyBX.lastIndexOf(",")>=0)keyB = keyBX.substring(0,keyBX.lastIndexOf(","));
			else {
				keyB = "empty";
			}
			if(!PTB.containsKey(keyB) || !PB.containsKey(keyB)){
				int k=0;
			}
			pb = (double [])PB.get(keyB);
			ptb = (double [])PTB.get(keyB);
			
			for(int i=0; i<ptbx.length; i++) {
				if(pbx == null) {
					ptbx[i] = 0.0;
				}
				else {
					if(pbx[0] == 0) {
						if(ptbx.length == 0) System.out.println("ERRROR in KLExpected, ptbx.length==0");
						ptbx[i] = 1.0/ptbx.length; 
					}
					else ptbx[i] = ptbx[i]/pbx[0];
				}
			}
			
			double [] aux = new double[ptb.length];
			for(int i=0; i<ptb.length; i++) {
				if(pb == null) {
					ptb[i] = 0.0;
				}
				else {
					if(pb[0] == 0) {
						if(ptb.length == 0) System.out.println("ERROR in KLExpected, ptb.length=0");
						ptb[i] = 1.0/ptb.length;
					}
					else aux[i] = ptb[i]/pb[0];
				}
			}
			double aux2 = KL(ptbx, aux);
			//if(T.index() == 183 || T.index() == 184 || T.index()==185) System.out.println("pbx[0]: " + pbx[0] + ", KL(ptbx, aux):" + aux);
			EKL += pbx[0]*KL(ptbx, aux);
		  }
		return EKL;
	}
	
	public static double KL(double [] P1, double [] P2){
		if(P1.length != P2.length) {
			System.out.println("ERROR at KL");
			return -1;
		}
		double kl = 0;
		for(int i=0; i<P1.length; i++){
//			Special cases
			if(P1[i]==0 || P2[i] == 0) {
				kl+=0;
			}
			else
				kl += P1[i]*Math.log(P1[i]/P2[i]);
		}
		if(kl<0){
			int k=0;
		}
		return kl;
	}

	
///////////////////////////////////////////////////
/////  HAMMERSLEY-CLIFFORD  ///////////////////////
///////////////////////////////////////////////////
	public static double computeCountsProductOfInstance(Vector maxCliques, Vector Vcounts, FastInstance inst){
		double c = 1;
		double [] cv = new double[1];
		for(int i=0; i<maxCliques.size(); i++){
			Set maxClique = (Set) maxCliques.elementAt(i);
			Hashtable counts = (Hashtable) Vcounts.elementAt(i); 
			attributesSet cliqueA = new attributesSet(maxClique);
			
			String config = getHashCode(cliqueA.projectInstance(inst));
			cv = (double []) counts.get(config);
			c *= cv[0];
		}
		
		return c;
	}
	
	/**
	 * configurations is a hashtable containing all the configurations (that actually appears in the dataset) of the universe of variables V.
	 * @param Vcounts
	 * @param configurations
	 * @return
	 */
	public static double computePartitionFunction2(Vector maxCliques, Vector Vcounts, Hashtable configurations, Schema schema){
		double Z = 0;
				
		Enumeration configs = configurations.keys();
		while(configs.hasMoreElements()){
			String config = (String) configs.nextElement();
			//System.out.println(config);
			FastInstance inst = FastInstance.parseInstance(config, -1, ',', schema, true, null);
			Z += computeCountsProductOfInstance(maxCliques, Vcounts, inst);
		}
		return Z;
	}

    //p is overwritten, and must have an extra memory slot at the end
    final static public int sampleFromUnivariateDestructive(double [] p, Random rand){
        double cdf [] = p;//new double[p.length+1];
        double p_1 = p[1], sum = p[0], p_0;
        double paux;// = p[1], paux;
        
/*        p = new double[6];
        p[0] = 0.025;
        p[1] = 0.1;
        p[2] = .125;
        p[3] = .2;
        p[4] = .25;
        p[5] = .3;*/
        
        //for(int i=0;i<p.length;i++)System.out.println(p[i]+",");
        p_1 = p[1];
        p[1] = p[0];
        p[0] = 0;
        for(int i=2;i<p.length;i++) {
            paux = p[i];
            p[i] = p[i-1] + p_1;
            p_1 = paux;
        }
        //cdf[0] = 0;
        
        double y = rand.nextDouble();
        for(int i=0;i<cdf.length-1;i++) {
            if(cdf[i] <= y && y <= cdf[i+1]) return i;
        }
        
        //Shouldn't go beyond this point.
        if(Utils._ASSERT){
            System.out.println("ERROR in sampleFromUnivariate");
            (new Exception()).printStackTrace();
            System.out.println(1);
            //System.exit(1);
        }            
        return -1;
        
    }

	final static public int sampleFromUnivariate(double [] p, Random rand){
		double pp [] = new double[p.length+1];
		for(int i=0;i<p.length;i++) pp[i] = p[i];
		
        return sampleFromUnivariateDestructive(pp, rand);
	}

	public static double logLikelihood_(FastDataset data, MarkovNet G){
		double ll=0, aux;
		
		System.out.println("Computing Max Cliques...");
		//System.out.println(G);
		Vector maxCliques = G.MaxCliques3();
		Vector Vcounts = new Vector(maxCliques.size());
		//	System.out.println(maxCliques);
		
		//Compute counts for each of the cliques
		System.out.println("Computing Counts ...");
		for(int i=0; i<maxCliques.size(); i++){
			Vcounts.add(i, superFastCounts(data, new attributesSet( (Set)maxCliques.elementAt(i) )) );
		}
		
		System.out.println("Computing partition function ...");
		Hashtable configurations = superFastCounts(data, new attributesSet(new Set(G.V())));
		double Z = computePartitionFunction2(maxCliques, Vcounts, configurations, data.schema());
		//double FastZ = computePartitionFunction(Vcounts);
		FastInstance ex = data.getFirstInstance();
		while(ex!= null){
			aux = computeCountsProductOfInstance(maxCliques, Vcounts, ex);
			aux /= Z;
			aux = -Math.log(aux);
			ll += aux;
			
	  		ex = data.getNextInstance();
		}
		
		
		return ll;
	}
	public static void main(String[] args) {
		
	}

}
