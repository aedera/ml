package machineLearning.Datasets;

import java.util.*;
import java.io.*;
/**
 * Class dataSource
 *
 * Abstraction for a data source.<!-- --> Has means to connect to the datasource, reads its schema and
 * data. <!-- -->
 *
 * @author Facundo Bromberg
*/
public class dataSourceARTF extends dataSource
{
  /** This could become a filename, a database dns, etc.*/
  protected String namesFile;

  /** Access methods */
  public String namesFile(){return this.namesFile;};

  /** Set methods */
  public void namesFile(String namesFile){this.namesFile = namesFile;};

  public dataSourceARTF(){ ; };

  public dataSourceARTF(String name, String fileName, String namesFile)
 {
   super(name, fileName);
   this.namesFile = namesFile;

   schema = new Schema(name + "' schema");
   schema().parseNamesFile(namesFile);
 }

 /** sets the row pointer to zero */
  public void reset(){rowPosition = 0;};

/*---------------------------------------------------*/
/**
 * @param buffersize = number of instances to retrieve (named because it simulates a bufferring of continuously-streaming data).<!-- -->
 * If buffersize = -1, it reads the rest of the dataset.
 * @return A Dataset containing the next buffersize records from the dataSource;
 */
 public Dataset read(int buffersize, char separator)
 {
   dataBuffer = null;
   dataBuffer = new Dataset(name + "' dataset", schema());

   if(buffersize == -1) {
     rowPosition += dataBuffer.read(url(), rowPosition, -1, separator, false);

   }
   else rowPosition += dataBuffer.read(url(), rowPosition, rowPosition + buffersize, separator, false);


   return dataBuffer;
 }

/*---------------------------------------------------*/
/**
 * @return A Dataset containing all the records in the database
 */
  public Dataset readAll(char separator)
  {
    reset();
   return read(-1, separator);
  }

  public String toString()
  {
    String ret = "Datasource: " + name() + "\n";
    ret += "Data File: "+ url() + "\n";
    ret += "Names File: "+ namesFile() + "\n";
    ret += "Instances of last read Dataset: "+ dataBuffer().toString() + "\n";

    return ret;
  }
}