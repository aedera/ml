package machineLearning.Datasets;

import java.util.regex.Pattern;
import java.util.Collection;
import java.util.ArrayList;

import java.io.BufferedReader;
import java.io.StreamTokenizer;
import java.io.StringReader;


import machineLearning.Utils.FastSet;
import machineLearning.Graphs.Assigment;
import machineLearning.Graphs.Variable;

/**
 * Abstraction for an instance (a row in the dataset).<!-- --> It contains a
 * name and a set of attribute values.
 */
public class FastInstance
{
    Schema schema;

    /**  name of the instance.  */
    public String name;

    /**
     * <p>array of values indexes.</p>
     *
     * <p>This allows an alternative and faster representation of instances.
     *  Instead of a vector of strings, we have an int array that contains the
     *  indexes of the values at Attribute. For instance, say we have two
     *  attributes A1={"Apple", "Orange", "Banana"}, and A2={"Red", "Green",
     *  "Yellow}.  Then, instance ["Apple", "Yellow"] is re-encoded as
     *  [0,2].</p>
     */
    public int [] values;

    // index relative to belongs to instance FastDataset
    public int index;

    public FastInstance(int numbOfValues){
        name = "Unnamed";
        schema = null;
        values = new int[numbOfValues];
    };

    /**
     * Instanciate a <code>FastInstance</code> from a string. For example:
     * <code>1,0,1,0,1,1</code>. The separator is <code>,</code> and have six
     * elements whose values are: 1, 0, 1, 0, 1, 1.
     *
     */
    public FastInstance(String instance, String separator) {
        name = "Unnamed";
        schema = null;

        String[] valuesOfInstance = Pattern.compile(separator).split(instance);
        values = new int[valuesOfInstance.length];
        int i = 0;

        for (String value : valuesOfInstance)
            values[i++] = Integer.parseInt(value);
    }

    public FastInstance(Schema schema, String _name, int numb_of_values){
        this.schema = schema;
        name = _name;
        values = new int[numb_of_values];
    }

    /**
     * Creates a FastInstance from an array of doubles containing the values
     * @return
     */
    public FastInstance(double [] v){
        values = new int[v.length];
        for(int i=0; i<v.length; i++){
            if(Math.IEEEremainder(v[i],1.0) != 0){
                System.out.println("ERROR: invalid instance values");
                Exception e = new Exception();
                e.printStackTrace();
                System.exit(1);
            }
            values[i]=(int)v[i];
        }
    }

    public FastInstance(int[] v, Schema schema){
        this.schema = schema;
        values = new int[v.length];
        for (int i=0; i<v.length; i++) values[i]=v[i];
    }

    public FastInstance(int[] v) { this(v, null); }

    /**
     * makeCopy copies the instance
     */
    public FastInstance makeCopy()
    {
        FastInstance newI = new FastInstance(schema, name, size());
        for(int i=0; i<values.length; i++) newI.setValue(values[i], i);

	newI.index = this.index;

        return newI;
    }

    /**
     * @todo hardcoded number of dimension for variables
     **/
    public Assigment toAssigment() {
        Collection<Variable> variables = new ArrayList<Variable>();

        for (int i = 0; i < values.length; i++)
            variables.add(new Variable(2, i));

        return new Assigment(variables, values);
    }

    /**
     * Returns the number of values in the instance.
     */
    public int size(){return values.length;}

    /**
     * Changes the index-th value.
     */
    final public void setValue(int value, int index)
    {
        values[index] = value;
    }

    public java.util.Collection<Integer> getValues() {
        java.util.Collection<Integer> v = new java.util.ArrayList<Integer>();
        for (int value : values) v.add(new Integer(value));
        return v;
    }

    /**
     * Returns the index-th value.
     */
    final public int getValue(int index)
    {
        return values[index];
    }
    /**
     * The value returned is an agregation value for all elements in X computed as follows:
     * v_0 = values of instance for attribute X_0;
     * v_i = value of instance for attribute X_i *  Prod_j=0^(i-1) number of values of attribute X_j
     *
     * value = Sum_i v_i
     */
    final public int getValue(FastSet X)
    {
        int val=0; //dummy
        int prod=1;

        int index = X.first();
        if(index >= 0) {
            val = values[index];
            prod = schema.getAttribute(index).size();
        }
        index = X.next();
        while(index >=0){
            val += values[index] * prod;
            prod *= schema.getAttribute(index).size();
            index = X.next();
        }

        return val;
    }

    public String getStringValue(int index){
        if(schema != null) return schema.getAttribute(index).getValueName(values[index]);
        else{
            System.out.println("Tried to access Schema in FastInstance, but schema = null");
            (new Exception()).printStackTrace();
            System.exit(1);
            return null;
        }
    }

    /**
     * Used for classifiers where it is assumed that the class attribute is the last one.<!-- -->
     * Returns then the last value.
     */
    public String getClassValueString()
    {
        return getStringValue(values.length-1);
    }

    public int [] project(FastSet Z){
        int [] a = new int[Z.cardinality()];

        int k = Z.first();
        int i=0;
        while(k >= 0){
            a[i] = getValue(k);
            k = Z.next();
            i++;
        }
        return a;
    }

    /**
     * Returns a string representation of the instance.
     */
    public String toString()
    {
        return internalToString(false);
    }
    public String toString(boolean noInstanceValues)
    {
        return internalToString(noInstanceValues);
    }

    /**
     * Used internally by toString method.
     */
    private String internalToString(boolean noInstanceValues)   {
        String aux = "";
        if(!noInstanceValues)
            {
                aux =": ";
                for(int i=0; i<size(); i++)
                    {
                        aux = aux + getStringValue(i) + " ";
                    }
            }
        //          return name + aux;
        return aux;
    }

    /**
     * Converts it to ARFF format
     * @author bromberg
     *
     */
    public String toARFF(String separator)
    {
        String aux = "";
        for(int i=0; i<size(); i++)
            {
                aux = aux + getStringValue(i);
                if(i < size()-1) aux += separator;

            }
        return aux;
    }
    /**
     * @author bromberg
     *
     */
    public String toNamesDataFormat()
    {
        String aux = "";
        for(int i=0; i<size(); i++)
            {
                aux = aux + getStringValue(i);
                if(i < size()-1) aux += ",";

            }
        return aux;
    }

    public static FastInstance parseInstance(String instanceStr, int row, char separator, Schema schema){
        return parseInstance(instanceStr, row, separator, schema, false, null);
    }
    public static FastInstance parseInstance(String instanceStr, int row, char separator, Schema schema, java.util.BitSet attrsToRead){
        return parseInstance(instanceStr, row, separator, schema, false, attrsToRead);
    }
    public static FastInstance parseInstance(String instanceStr, int row, char separator, Schema schema, boolean treatValuesAsIndexes, java.util.BitSet attrsToRead)

    {
        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new StringReader(instanceStr));
        }
        catch(Exception ioe){}

        StreamTokenizer tokenizer = new StreamTokenizer(reader);

        tokenizer.resetSyntax();
        //tokenizer.whitespaceChars(0, ' ');
        tokenizer.wordChars(' ','\u00FF');
        //tokenizer.wordChars(' ','\u00FF');
        tokenizer.whitespaceChars(separator,separator);
        tokenizer.commentChar('|');
        tokenizer.eolIsSignificant(true);


        try{tokenizer.nextToken();}
        catch (Exception te){}

        String token = tokenizer.sval;

        FastInstance inst;
        if(schema != null)  inst = new FastInstance(schema, schema.name + String.valueOf(row), schema.numberOfAttributes());
        else inst = new FastInstance(schema.numberOfAttributes());

        //Add values to new instance;
        int value_count = 0, attr_index = 0;
        while(tokenizer.ttype != StreamTokenizer.TT_EOL && tokenizer.ttype != StreamTokenizer.TT_EOF)
            {
                if(attrsToRead != null && !attrsToRead.get(value_count)) {
                    value_count++;

                    try{tokenizer.nextToken();}
                    catch (Exception te){}
                    token = tokenizer.sval;

                    continue;
                }
                Attribute currAttr = null;
                if(schema != null) currAttr = schema.getAttribute(attr_index);

                //Checks if value is missing. If so, we ignore the instance.
                if(token.equalsIgnoreCase("?"))
                    {
                        //Attribute aux_attr = (Attribute) schema.getAttribute(value_count);
                        //if(!aux_attr.hasValue("?")) aux_attr.addValue("?");
                        return null;
                    }
                //If attribute does not contain this value, we add the value to the attribute values list.
                if(!treatValuesAsIndexes){
                    if(!currAttr.hasValue(token)) currAttr.addValue(token);
                    inst.setValue(currAttr.getValueIndex(token), attr_index);
                }
                //Used for Statistics class, to convert from a configuration in the Hashtables (made up of the integer values, not the string values
                //as when this procedure is used from Dataset.
                else{
                    Integer integ = new Integer(token);
                    inst.setValue(integ.intValue(), value_count);
                }
                try{tokenizer.nextToken();}
                catch (Exception te){}
                token = tokenizer.sval;

                attr_index ++;

                value_count++;
            }
        return inst;
    }

    public Boolean[] toBoolean()   {
    	Boolean[] aux = new Boolean[size()];
    		for(int i=0; i<size(); i++)
    		{
    			aux[i] = schema.getAttribute(i).getValueName(values[i]).equals("0") ? false:true;
    		}
    	//          return name + aux;
    	return aux;
    }

    public boolean containsAll(int[] positions, int... values) throws Exception {
        if (positions.length > size())
            throw new Exception("Unmatch dimensions.");

        int j = 0;
        for (int i : positions) {
            if (this.values[i] != values[j]) return false;
            j++;
        }
        return true;
    }

    public Schema schema() { return schema; }

    public static void main(String [] args){
        Schema schema = new Schema();
        Attribute attr1 = new Attribute("", 0, 5);
        Attribute attr2 = new Attribute("", 1, 4);
        Attribute attr3 = new Attribute("", 2, 3);
        Attribute attr4 = new Attribute("", 3, 7);

        schema.addAttribute(attr1);
        schema.addAttribute(attr2);
        schema.addAttribute(attr3);
        schema.addAttribute(attr4);

        FastInstance fi = new FastInstance(schema, "", 4);
        fi.setValue(4, 0);
        fi.setValue(0, 1);
        fi.setValue(0, 2);
        fi.setValue(1, 3);

        FastSet X = new FastSet(4);
        X.add(1);
        X.add(2);
        X.add(3);

        fi.getValue(X);
    }
}