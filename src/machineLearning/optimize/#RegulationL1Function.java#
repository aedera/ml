package machineLearning.optimize;

import lbfgsb.*;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.FactorGraph;

public class RegulationL1Function extends ScoreBasedFunction {
    protected double beta_;

    public RegulationL1Function(FastDataset dataset, FactorGraph factors, double beta) {
        super(dataset, factors);
        beta_ = beta;
    }

    public FunctionValues getValues(double[] point) {
        double functionValue = -(likelihood(factors_) + regularizate(point));
        double[] gradientValue = likelihoodGradient(factors_);

        for (int i = 0; i < gradientValue.length; i++)
            gradientValue[i] += -point[i] / (beta_ * Math.abs(point[i]));

        return new FunctionValues(functionValue, gradientValue);
    }

    private double regularizate() {
        double outcome = -1 / beta_;

        for (double parameter : factors_.parametersToArray())
            outcome += Math.abs(parameter);

        return outcome;
    }
}