package machineLearning.optimize;

import java.util.List;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.Factor;
import machineLearning.Graphs.Variable;


public abstract class AbstractDifferentiableFunction implements Function {
    protected double[] gradient_;
    protected double value_;
    protected Factor[] factors_;
    protected FastDataset dataset_;
    protected int sizeOfDataset_;
    protected int[] scope_;

    public
	AbstractDifferentiableFunction(FastDataset dataset, Factor[] factors) {
	dataset_ = dataset;
	sizeOfDataset_ = dataset_.size();
	scope_ = dataset.scope();

	setStructure(factors);
    }

    public double[][] getValues(double[] point) {
	compute(point);

	double[] lossGradient = new double[gradient_.length];
	for (int i = 0; i < lossGradient.length; i++)
	    lossGradient[i] = -gradient_[i];

	return new double[][] { new double[] {-value_} , lossGradient };
    }

    public void compute(double[] point) {
	value_ = computeFunction(point);
	gradient_ = computeGradient(point);
    }

    public double[] getGradient() { return gradient_; }
    public double getValue() { return value_; }

    public void setStructure(final Factor[] factors) {
	factors_ = factors;
    }

    public void setDataset(final FastDataset dataset) {
	dataset_ = dataset;
    }

    public Regularized getRegularization() {
	return Regularized.NORMAL;
    }

    public void setHiperParam() {
        throw new UnsupportedOperationException ("Not regularizable.");
    }

    public double getHiperParam() { return 0; }
}