package machineLearning.optimize;

public abstract class OptimizerGeneral<T> implements Optimizer<T> {
    protected Expander<T> expander_;
    protected int numAscents;

    protected OptimizerGeneral(Expander<T> expander) {
        expander_ = expander;
        numAscents = 0;
    }

    public int getAscents() { return numAscents; }
}