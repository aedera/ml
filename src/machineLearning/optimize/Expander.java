package machineLearning.optimize;

import java.util.List;

public interface Expander<T> {

    /**
     * @return a <code>List</code> of children nodes of
     * <code>nodeToExpand</code>.
     */
    public List<Node<? extends T>> expand(final Node<? extends T> nodeToExpand);
}