package machineLearning.optimize;

/**
 * Represent nodes in an optimization algorith. Encapsule it the candidate
 * solutions
 */
public class Node<T> {
    private T object_;
    private double value_;

    public Node(double value) {
        setValue(value);
    }

    public Node(T object, double value) {
        this(value);
        setObject(object);
    }

    public void setObject(T object) { object_ = object; }
    public void setValue(double value) { value_ = value; }

    public T getObject() { return object_; }
    public double getValue() { return value_; }

    @Override
    public String toString() {
        return object_.toString() + " -> " + value_;
    }
}