package machineLearning.optimize;

import static java.lang.Math.*;

import java.util.Collections;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;

import machineLearning.Graphs.Factor;
import machineLearning.Graphs.VarSet;
import machineLearning.Graphs.Potencial;
import machineLearning.Graphs.IndicatorFeature;
import machineLearning.Graphs.FactorGraph;

public class GreedyStructureSearch {
    public Function function_;
    public double thresold_;
    private double threshold_;
    private MAPScore score_;
    private boolean modify_;
    private int negative_;
    private int sizeFree_;
    private int sizeNoFree_;

    public GreedyStructureSearch(MAPScore score, double thresold) {
	thresold_ = thresold;
	modify_ = false;
	score_ = score;
	function_ = score_.getFunction();
	threshold_ = function_.getRegularization() == Function.Regularized.L1 ?
	    function_.getHiperParam() : Double.NEGATIVE_INFINITY;
    }

    public GreedyStructureSearch(MAPScore score) { this(score, 10e-2); }

    public GreedyStructureSearch(MAPScore score, double thresold, int negative) {
	this(score, thresold);
	negative_ = negative;
    }

    public double[]
	search(double[] parameters, Factor[] free, List<Factor> current)
	throws Exception {
	sizeFree_ = free.length;
	sizeNoFree_ = current.size();
	score_.setFunction(function_);
	int k = 0;

	do {
	    function_.setStructure(current.toArray(new Factor[0]));
	    parameters = score_.compute(parameters);
	    //System.out.println("PAR*: " + java.util.Arrays.toString(parameters));
	    //System.out.println("GRAD: " + java.util.Arrays.toString(function_.getGradient()));

	    parameters = removeInactives(parameters, current);

	    // grafting modify current and return new parameters
	    parameters = grafting(parameters, free, current);
	} while (!terminationCondition());

	return parameters;
    }

    // TODO: best performance
    private double[] removeInactives(final double[] parameters, List<Factor> current) {
	int count = 0;

	for (int i = 0; i < parameters.length; i++)
	    if (abs(parameters[i]) <= thresold_) {
		// to remove shift all elements because rest by count
		current.remove(i - count);
		++count;
	    }

	if (count == 0) return parameters;

	double[] pars = new double[parameters.length - count];
	int j = 0;
	for (int i = 0; i < parameters.length; i++)
	    if (parameters[i] > thresold_)
		pars[j++] = parameters[i];

	sizeNoFree_ -= count;

	return pars;
    }

    private boolean terminationCondition(){
	if (!modify_) return true;

	if (sizeFree_ < 0) return true;

	return false;
    }

    private double[]
	grafting(final double[] parameters, Factor[] all, List<Factor> current) {
	if (sizeFree_ <= 0) {
	    sizeFree_ = -1;
	    return parameters;
	}

	// add new parameter
	double[] newParams = new double[parameters.length + 1];
	System.arraycopy(parameters, 0, newParams, 0, parameters.length);

	modify_ = false;
	double maxValue = threshold_;
	int maxIndex = Integer.MIN_VALUE;
	double currentValue;
	Factor select = null;
	Factor[] noFree = new Factor[sizeNoFree_ + 1];
	System.arraycopy(current.toArray(new Factor[0]), 0, noFree, 0, noFree.length - 1);
	function_.setStructure(noFree);

	for (int i = 0; i < all.length; i++) {
	    if (all[i] == null) continue;
	    noFree[noFree.length - 1] = all[i];
	    currentValue = function_.computeGradient(newParams.length -1, newParams);

	    if (maxValue < currentValue) {
		maxValue = currentValue;
		maxIndex = i;
		select = all[i];
	    }
	}

	if (select != null) {
	    sizeFree_ -= 1;
	    sizeNoFree_ += 1;
	    current.add(all[maxIndex]);
	    all[maxIndex] = null;

	    // add parameter
	    modify_ = true;
	} else
	    newParams = parameters;


	return newParams;
    }
}