package machineLearning.optimize;

import static java.lang.Math.*;
import java.util.Iterator;
import java.util.List;
import java.util.Collection;

import machineLearning.Datasets.FastInstance;
import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.Factor;
import machineLearning.Graphs.AbstractFactor;
import machineLearning.Graphs.IndicatorFeature;
import machineLearning.Graphs.Assigment;
import machineLearning.Graphs.Variable;
import machineLearning.Graphs.FactorGraph;
import machineLearning.Graphs.Assigment;

public class PseudoLogLikelihoodFunction extends AbstractDifferentiableFunctionRegularized {
    public PseudoLogLikelihoodFunction(FastDataset dataset, Factor[] factors) {
	super(dataset, factors);
    }

    public PseudoLogLikelihoodFunction(FastDataset dataset, Factor[] factors,
				       Regularized type) {
	super(dataset, factors, type);
    }

    public PseudoLogLikelihoodFunction(FastDataset dataset, Factor[] factors,
				       Regularized type, double constant) {
	super(dataset, factors, type, constant);
    }

    public double computeFunction(final double[] parameters) {
	return computeFunction(dataset_, parameters, factors_, type_);
    }

    public double
	computeFunction(final FastDataset dataset, final double[] parameters,
			final Factor[] factors, final Regularized type) {
        double summation = 0.0;
	for (FastInstance instance : dataset) {
	    summation += scope_.length * calMargin(parameters, factors, instance);
	    for (int var : scope_)
		summation -=
		    log(summationOverAllValuesOfVariable(parameters, factors,
							 var, instance));
	}

        return (summation / sizeOfDataset_) + computeRegularization(parameters);
    }

    public double[] computeGradient(final double[] parameters) {
	return computeGradient(dataset_, parameters, factors_, type_);
    }

    public double[]
	computeGradient(final FastDataset dataset, final double[] parameters,
			final Factor[] factors, final Regularized type) {
	double[] gradient = new double[parameters.length];

	for (int i = 0; i < parameters.length; i++)
	    gradient[i] = computeGradient(i, parameters, dataset, factors, type);

	return gradient;
    }

    public double computeGradient(int i, final double[] parameters) {
	return computeGradient(i, parameters, dataset_, factors_, type_);
    }

    public double
	computeGradient(int index, final double[] parameters,
			final FastDataset dataset,
			final Factor[] factors,
			final Regularized type) {
	double summation = 0;
	double summationDataset = 0;
	Factor factor = factors[index];
	Assigment assigment = null;
	Assigment innerAssig = null;
	FastInstance innerAssig2 = null;
	double innerValue = 0;
	double outcome = 0;
	double valueF = ((IndicatorFeature)factor).count;

	for (int variable : ((AbstractFactor)factor).scope_) {
	    summationDataset = 0;
	    for (FastInstance instance : dataset) {
		summation = summationOverAllValuesOfVariable(parameters, factors,
							     variable, instance,
							     factor);
		summation /=
		    summationOverAllValuesOfVariable(parameters,factors,
						     variable, instance);

		summationDataset += summation;
	    }

	    outcome += (valueF - summationDataset) / sizeOfDataset_;
	}

	return outcome + computeRegularizationGradient(parameters[index]);
    }

    private double calMargin(final double[] parameters,
			     final Factor[] factors,
			     final FastInstance instance) {
	double value;
	double summation = 0;
	for (int i = 0; i < parameters.length; i++) {
	    value = ((IndicatorFeature)factors[i]).
		    assigmentsToValues[instance.index][scope_.length][0];

	    if (value == 0) continue;

	    summation += parameters[i];
	}

	return summation;
    }

    private double calMargin(final double[] parameters,
			     final Factor[] factors,
			     final FastInstance instance,
			     final int variable,
			     final int v) {
	double value;
	double summation  = 0;

	for (int i = 0; i < parameters.length; i++) {
	    value = ((IndicatorFeature)factors[i]).
		    assigmentsToValues[instance.index][variable][v];

	    if (value == 0) continue;

	    summation += parameters[i];
	}

	return summation;
    }


    private double
	summationOverAllValuesOfVariable(final double[] parameters,
					 final Factor[] factors,
					 final int variable,
					 final FastInstance instance) {
	return summationOverAllValuesOfVariable(parameters, factors,
						variable, instance, null);
    }

    private double
	summationOverAllValuesOfVariable(final double[] parameters,
					 final Factor[] factors,
					 final int variable,
					 final FastInstance instance,
					 final Factor factor) {
	double summation = 0;
	int index = variable;
	int orig = instance.values[index];

	if (factor != null) {
	    double innerValue;

	    for (int value = 0; value < 2; value++) {
		instance.values[index] = value;

		innerValue = ((IndicatorFeature)factor).
		    assigmentsToValues[instance.index][index][value];

		if (innerValue == 0) continue;

		summation += exp(calMargin(parameters, factors, instance, index, value));
	    }
	} else {
	    for (int value = 0; value < 2; value++) {
		instance.values[index] = value;

		summation += exp(calMargin(parameters, factors, instance, index, value));
	    }
	}

	// restore original value of instance
	instance.values[index] = orig;
	return summation;
   }
}