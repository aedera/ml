package machineLearning.optimize;

import lbfgs.Minimizer;
import lbfgs.DifferentiableFunction;

public class LBFGS {
    public double[] point;
    // The function value minimize
    public double functionValue;
    // The gradient for the function value
    public double[] gradient;

    public double[][]
        optimize(Function function, double[] initialPoint)
        throws Exception {
        double[][] result = null;

	Minimizer.Regularization reg;
	double constReg = 0;

	switch (function.getRegularization()) {
	case L2:
	    reg = Minimizer.Regularization.L2;
	    break;
	case L1:
	    reg = Minimizer.Regularization.L1;
	    constReg = function.getHiperParam();
	    break;
	default:
	    reg = Minimizer.Regularization.L2;
	    break;
	}

	Minimizer m = new Minimizer(reg);
	m.constReg = constReg;

        try { result = m.run(function, initialPoint); }
	catch (Exception e) { throw e; }

        point = initialPoint;
        functionValue = result[0][0];
        gradient = result[1];

        return result;
    }
}