package machineLearning.optimize;

import lbfgs.DifferentiableFunction;

import machineLearning.Graphs.Factor;

public interface Function extends DifferentiableFunction {
    // glue
    public double[][] getValues(double[] point);

    public enum Regularized { NORMAL, BIC, L1, L2, BLOCK_L1 };
    public double[] getGradient();
    public double getValue();

    // compute function and gradient
    public void compute(final double[] point);
    public double computeFunction(final double[] point);
    public double[] computeGradient(final double[] point);

    public double computeGradient(int ith, final double[] point);
    public void setStructure(final Factor[] factors);
    public Regularized getRegularization();
    public double getHiperParam();
    public void setHiperParam(double hiper);
}