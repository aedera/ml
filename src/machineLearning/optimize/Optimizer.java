package machineLearning.optimize;

public interface Optimizer<T> {
    /**
     * @return <p>a <code>Node</code> if it has converged</p>
     */
    public Node<? extends T> optimize();

    public int getAscents();
}