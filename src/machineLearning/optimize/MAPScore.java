package machineLearning.optimize;

public class MAPScore {
    private Function function_;
    private LBFGS optimizer_;

    public MAPScore(Function function) {
	function_ = function;
	optimizer_ = new LBFGS();
    }

    public double[] compute(final double[] parameters) throws Exception {
	optimizer_ = new LBFGS();

	optimizer_.optimize(function_, parameters);

	return argMax();
    }
    public void setFunction(Function function) { function_ = function; }
    public Function getFunction() { return function_; }
    public double[] argMax() { return optimizer_.point; }
    public double score() { return optimizer_.functionValue; }
}