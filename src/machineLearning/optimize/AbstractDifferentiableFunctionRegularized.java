package machineLearning.optimize;

import static java.lang.Math.*;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.Factor;

public abstract class AbstractDifferentiableFunctionRegularized
    extends AbstractDifferentiableFunction {
    protected Regularized type_;
    protected double constant_;

    public
	AbstractDifferentiableFunctionRegularized(FastDataset dataset,
						  Factor[] factors,
						  Regularized type) {
	this(dataset, factors, type, 0);
    }

    public
	AbstractDifferentiableFunctionRegularized(FastDataset dataset,
						  Factor[] factors) {
	this(dataset, factors, Regularized.NORMAL, 0);
    }

    public
	AbstractDifferentiableFunctionRegularized(FastDataset dataset,
						  Factor[] factors,
						  Regularized type,
						  double constant) {
	super(dataset, factors);
	constant_ = constant;
	type_ = type;
    }


    public double
	computeRegularizationGradient(double parameter) {
	double value = 0;

	switch (type_) {
	case NORMAL: break;
	case L1:
	    if (parameter == 0) {
		value = 0;
		break;
	    }

	    value = constant_ * (parameter / abs(parameter));
	    break;
	case L2:
	    value = pow(constant_, 2) * parameter;
	    break;
	}

	return -value;
    }

    public double[] computeRegularizationGradient(final double[] parameters) {
	double[] gradient = new double[parameters.length];

	for (int i = 0; i < parameters.length; i++)
	    gradient[i] = computeRegularizationGradient(parameters[i]);

	return gradient;
    }

    public double computeRegularization(final double[] parameters) {
	double value = 0;

	switch (type_) {
	case NORMAL: break;
	case L1:
	    for (int i = 0; i < parameters.length; i++)
		value += abs(parameters[i]);

	    value *= constant_;
	    break;
	case L2:
	    for (int i = 0; i < parameters.length; i++)
		value += pow(parameters[i], 2);

	    value *= .5 * pow(constant_, 2);
	    break;
	}

	return -value;
    }

    public void setHiperParam(double constant) { constant_ = constant; }
    public double getHiperParam() { return constant_; }
    public Regularized getRegularization() { return type_; }
}