package machineLearning.optimize;

import java.util.List;

import machineLearning.DataStructures.Model;
import machineLearning.Graphs.UGraph;

public class HillClimbing<T> extends OptimizerGeneral<T> {
    private Node<T> currentNode_;

    public HillClimbing(Expander<T> expander, Node<T> initialNode) {
        super(expander);
        currentNode_ = initialNode;
    }

    public Node<? extends T> optimize() {
        numAscents = 0;
        System.out.println("\nStarting hill climbing ...");
        for(;;) {
            double bestValue = currentNode_.getValue();
            Node<T> bestNode = currentNode_;

            Model m = (Model)currentNode_.getObject();
            UGraph g = ((UGraph)m.getStructure());
            System.out.println(g.getBOG() +  " " + bestValue);
            
            List<Node<? extends T>> neighbors = expander_.expand(currentNode_);
            for (Node node : neighbors){
                if (node.getValue() > bestValue) {
                    bestValue = node.getValue();
                    bestNode = node;
                }
            }

            if (bestValue <= currentNode_.getValue()) return currentNode_;

            numAscents++;
            currentNode_ = bestNode;
        }
    }
}