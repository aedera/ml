package machineLearning.Exceptions;

public class IllegalDimensionException extends Exception {
    private String error;

    public IllegalDimensionException() {
        super(); error = "Illegal Dimension";
    }

    public IllegalDimensionException(String error) {
        super(error); this.error = error;
    }

    public String getError() { return error; }
}