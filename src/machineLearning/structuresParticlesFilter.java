package machineLearning;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Random;
import java.util.Vector;

import machineLearning.Datasets.FastDataset;
import machineLearning.Datasets.IndependenceTestMargaritisUAI2001;
import machineLearning.Datasets.Statistics;
import machineLearning.Datasets.attributesSet;
import machineLearning.Graphs.GraphRVar;
import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FactSimpleGSIMN;
import machineLearning.Utils.FastSet;
import machineLearning.Utils.Output;
import machineLearning.Utils.Run;
import machineLearning.Utils.Set;
import machineLearning.Utils.Utils;
import machineLearning.Utils.coVL;
import machineLearning.independenceBased.Triplet;
import machineLearning.independenceBased.gsTriplet;

public class structuresParticlesFilter {
	static boolean dynamic;
	
	Particle [] particles;//, initialParticles;
	//Hashtable classes, testsStrings;
	
	//int [] particlesRep;
	Random r;

	//bMetropolisHastingsStructures metro_dynamic;
	MetropolisHastingsStructures metro;

	//double normalization = 0;
	particleClasses classes;
	
	int numStructures = 0;
	
	double pds[][];		// P(d_j | x)  for every j and every value of y_j^x.
	double p_D_0[];		//P(d_1:t | x) = Prod P(d_j | x)
	//double partPosteriors[], partPosteriors_plus1[], partPosteriors_minus1[];
	public gsTriplet tests[];
	public int [] testChunksInfo; //How many chunks of data has been used for i-th test.
	//double sumPartPosteriors=1.0, sumPartPosteriors_plus1=1.0, sumPartPosteriors_minus1=1.0;
	double h, hp;
	//double h_minus1;
    Hashtable uniqueParticles;
	
	public int t;
	int /*NperC, Nc,*/ N;  //number of particles per connectivity and number of connectivities.
	int T;
	posterior p;
	
	double H;
	//double prodOfFalse = 1.0;  		//This stores \prod_{i=0}^t  Pr(y_i = F |  d_i)  
	
	//Status 
	double similarity;
	double accuracy;
	double numDifferent;
	
	Run entropyRun;
	
	int infoGainCount=0;
	//
	BitSet connectivities;
    
    
    //BitSet testString = new BitSet();

	//-----------------------------------------------------------------
	//-----------------------------------------------------------------
	//-----------------------------------------------------------------
	//-----------------------------------------------------------------
	
	public structuresParticlesFilter(posterior p){//, Particle [] initialParticles){
		this.t=0;
		this.p = p;
		//this.NperC = p.NperC;
		//this.Nc = p.Nc;
		this.N = p.N;
		this.r = p.r;
        this.T = 20;
		
		//this.N = NperC*Nc;
		
		metro = new MetropolisHastingsStructures(r, p.alpha, this);
		//metro_dynamic = new MetropolisHastingsStructures(r, p.alpha, this);

		entropyRun = new Run(1, 10);
		
		particles = new Particle[N];
		//this.initialParticles = initialParticles;
		
	    uniqueParticles = new Hashtable(N);
        //classes = new classParticles;//new Hashtable(N);

		//testsStrings = new Hashtable(p.n*p.n*p.n);
		
		tests = new gsTriplet[T];
		testChunksInfo = new int [T]; //How many chunks of data has been used for i-th test.
		pds = new double[2][];
		for(int i=0; i< 2; i++) {
			pds[i] = new double[T];
		}
	}
	//-----------------------------------------------------------------
/*	public structuresParticlesFilter(structuresParticlesFilter s){
		this.t= s.t;
		this.p = s.p;
		this.N = s.N;
		this.T = s.T;
		this.r = s.r;
		
		this.h = s.h;
        this.hp = s.hp;
		this.H = s.H;
		
		metro = new MetropolisHastingsStructures(r, p.alpha, this);
		//metro_dynamic = new MetropolisHastingsStructures(r, p.alpha, this);

		classes = new Hashtable(N);
		this.testsStrings = (Hashtable) s.testsStrings.clone(); 
		
		//Copy particles and reconstruct tree
		particles = new Particle[N];
		
        uniqueParticles = new Hashtable(N);
        
		//classTreeP = new classTree(p);
		//tree.setAsRoot();
        for(int i=0; i<N; i++) {
			particles[i] = new Particle(s.particles[i]);
			
            //uniqueParticles.put(particles[i], particles[i]);
			
			//classTreeP.addParticle(particles[i]);
		}
		tests = new gsTriplet[T];
		System.arraycopy(s.tests, 0, this.tests, 0, T);

		testChunksInfo = new int[T];
		System.arraycopy(s.testChunksInfo, 0, this.testChunksInfo, 0, T);

		pds = new double[2][];
		for(int i=0; i< 2; i++) {
			pds[i] = new double[T];
			System.arraycopy(s.pds[i], 0, pds[i], 0, T);
		}
		
		infoGainCount = s.infoGainCount;
	}*/
	//-----------------------------------------------------------------
	public void init(int burnin, int sample_iid_interval){
		initialSample_();
		//initialSample(burnin, sample_iid_interval);
		
		t=0;
		//Initialize all weights to 1
		double [] logPartPosterior = new double[N];
		for(int i=0; i<N; i++){
			particles[i].w = 1.0/((double)N);

			//logPartPosterior[i] = particles[i].logPrior = particles[i].logPartPosterior = logPrior(particles[i]);
		}
/*		double [] normalized = normalizeLogPosteriors(logPartPosterior);
		for(int i=0; i<N; i++) particles[i].normalizedPosterior(normalized[i]);
		
		reCreateClasses();*/
		
		H = Math.log(Math.pow(2, p.n*(p.n-1)/2));//computeEmpiricalEntropy();
		
	}
	//-----------------------------------------------------------------
	public Particle [] initialSample(int burnin, int iidInterval){
		this.r = p.r;
			
		boolean print = p.exp.output.doPrint(coVL.MAINLOOP);
		
		//Generate random network as initial one.
		MarkovNet xMN = new MarkovNet();
		double tau = r.nextInt((int)((double)p.n/2.0));
		xMN.generateRandomStructure(tau, r, p.n, 0);
		UGraphSimple x = new UGraphSimple(xMN);
		UGraphSimple xp = new UGraphSimple(p.n);

		Particle sp, auxsp;
		//Particle [] particles = new Particle[N];
		
		if(print) System.out.println("\n\nGENERATING STRUCTURE PARTICLES");

		GraphRVar node = null;
		
		//burn-in
        for(int i=0; i<N; i++) {
            xMN = new MarkovNet();
            tau = r.nextInt((int)((double)p.n/2.0));
            xMN.generateRandomStructure(tau, r, p.n, 0);
            x = new UGraphSimple(xMN);
            particles[i] = new Particle(this, x, 0);
        }
		
        classes = new particleClasses(this);
        
        if(print) System.out.println("GENERATING PARTICLE #" + 0);
//		particles[0]  = metro.iterate(new Particle(particles[0]), burnin);
		
		numStructures++;
		
		//sample N particles
		if(print) System.out.println("");
		for(int i=1; i<N; i++){
			if(print) System.out.print(" | " + i);
			if((i+1)%(80/4) == 0) if(print) System.out.print("\n");

//            particles[i]  = metro.iterate(new Particle(particles[i-1]), iidInterval);
		}
		return particles;
	}
	//-----------------------------------------------------------------
	public Particle [] initialSample_(){
		this.r = p.r;
			
		boolean print = p.exp.output.doPrint(coVL.MAINLOOP);
		
		if(print) System.out.println("\n\nGENERATING STRUCTURE PARTICLES");

		GraphRVar node = null;
		
		int M = p.n*(p.n-1)/2;
        for(int i=0; i<N; i++) {
        	int m = r.nextInt(M);  //first we choose a random cardinality;
            particles[i] = new Particle(this, new UGraphSimple(p.n, m, r), i);  //then we generate a random particle with THAT cardinality
        }
		
        classes = new particleClasses(this);
        
		return particles;
	}
	//	-----------------------------------------------------------------
	/**
	 * log(p) = log(1/choose) = log 1 - log choose = -log choose.
	 */
	static final double logPrior(UGraphSimple x){
		int n = x.size();
		int m = n*(n-1)/2;
		double ret =  -(Utils.logChoose(m, x.numberOfEdges()));
		ret -= m* Math.log(2);
		return ret;
	}
	//-----------------------------------------------------------------
	/** Uniform */
	static final double logPrior_(UGraphSimple x){
		int n = x.size();
		int nn = n*(n-1)/2;
		return (double)(-n*(n-1)/2)*Math.log(2);
        //return (double)(n*(n-1)/2)*Math.log(p.tau);
	}
	/** Uniform for each cardinality*/
	static final double logPrior__(UGraphSimple x){
		return logPrior_edgeUniform(x);
	}
	static final double logPrior_edgeUniform(UGraphSimple x){
		int n = x.size();
		int m = n*(n-1)/2;
		double ret =  -(Utils.logChoose(m, x.numberOfEdges()));
		//ret -= m* Math.log(2);
		ret -= Math.log(m);
		
		return ret;
	}
	//-----------------------------------------------------------------
	public void printStatus(){
		boolean print = p.exp.output.doPrint(coVL.MAINLOOP);
		if(!print) return;

	    classes = new particleClasses(this);

		//normalizeLogPosteriors();
	    
		
		numDifferent = howManyDifferent();
		similarity = particlesSimilarity();


		double accuracy  = accuracy(p.trueNetSimple, null)[1]; 
		//accuracy = aux[1];
        double [] alpha = new double[1];
        //double ig = t==0?0.0:infoGain(tests[t-1].X, tests[t-1].Y, tests[t-1].FastZ, alpha, null);
		//h = computeEmpiricalEntropy();

		
		//double P_ind = IndependenceTestMargaritisUAI2001.conditional_P(p.log_dl, p.prior);
		gsTriplet test = t == 0?null:tests[t-1];
		//boolean trueNetTestValue = p.trueNetSimple.separates_nonRecursive(test.X(), test.Y(), test.getCond());


		if(print) {
			System.out.println("True Net Probability = " + (p.trueNetSimple!=null? Utils.myDoubleToString(computeClassLogProb(new Particle(this,p.trueNetSimple)),2): -1) +
					           " Prob of most probable particle = " + Utils.myDoubleToString(classes.mostProbableClass().normalizedClasssProbability(),4));
			//System.out.println("Normalized probability of most probable class = "  
			  //         + Utils.myDoubleToString(mostProbableClass().classProbMass,2));
			System.out.println(
					//"Classes Empirical Entropy = " +Utils.myDoubleToString(computeClassEmpiricalEntropy(), 5)+", h = "+ Utils.myDoubleToString(h,4) + ", alpha = " + Utils.myDoubleToString(alpha[0],2) + "\n|different| = "+ numDifferent 
				//+ ", |TrueNet's Tests&Card Class| = "+ howManyInTrueNetClass() 
				//+ ", |TrueNet's Tests Class| = "+ howManyInTrueNetTestsClass() + ", " + 
				"particles similarity = " + Utils.myDoubleToString(similarity,2) + ",  Avg Hamming to TrueNet = " + Utils.myDoubleToString(accuracy,2));
		}
		

//		Printing cardinalities
		int cards [] = new int[p.n*(p.n-1)/2+1];
		for(int l=0; l<N; l++){
			int kkk = particles[l].numberOfEdges();
			if(print && false) System.out.print(kkk + ", ");
			cards[kkk]++;
		}
		int true_m=-1;
		String star;
		
		if(true){
			System.out.println(classes.toString());
			
        	if(print) System.out.println("");
        	
   			true_m = p.trueNetSimple!=null? p.trueNetSimple.numberOfEdges() : -1;
    		Output out = p.exp.output;
            for(int l=0; l<=p.n*(p.n-1)/2; l++){
            	long start = System.currentTimeMillis();
            	if(true){
	    			if(l == true_m) star = "*";
	    			else star = "";
	    			if(cards[l] != 0) {
		    			out.print(coVL.MAINLOOP | coVL.LOG, "(" + cards[l] + star+ ")");
	    			}
	    			else out.print(coVL.MAINLOOP | coVL.LOG, "."+star);
	    			if(l==(p.n*(p.n-1)/2) ) out.println(coVL.MAINLOOP | coVL.LOG, "");
            	}
    		}
        }
	    if(print) System.out.println("");
		hp = h;
	}
	//	-----------------------------------------------------------------
	//receives log of data likelihoods
	public boolean recordTestData(gsTriplet test, int testIndex, double [] log_dl){

		//NOTE: recall that testIndex is 't' in case the test has not been done yet. 
		tests[testIndex] = test;
		testChunksInfo[testIndex]++;
		if(testIndex == t) {
			//testsStrings.put(test.toString(), test);
			t++;
		}
		//Update value of log_dl
		pds[IndependenceTestMargaritisUAI2001.I][testIndex] = log_dl[IndependenceTestMargaritisUAI2001.I];
		pds[IndependenceTestMargaritisUAI2001.D][testIndex] = log_dl[IndependenceTestMargaritisUAI2001.D];

		if(t==T){ //increase size of some arrays
			gsTriplet [] aux2 = new gsTriplet[2*T];
			System.arraycopy(tests, 0, aux2, 0, T);
			tests = null;
			tests = aux2;

			int []  testChunksInfo_aux = new int[2*T];
			System.arraycopy(testChunksInfo, 0, testChunksInfo_aux, 0, T);
			testChunksInfo = null;
			testChunksInfo = testChunksInfo_aux;

			double [] aux3 = new double[2*T];
			System.arraycopy(pds[0], 0, aux3, 0, T);
			pds[0] = null;
			pds[0] = aux3;
			
			double [] aux4 = new double[2*T];
			System.arraycopy(pds[1], 0, aux4, 0, T);
			pds[1] = null;
			pds[1] = aux4;
			
			T = 2*T;
		}
		return true;
	}
//	-----------------------------------------------------------------
	public void recomputeWeights(){
		boolean print = p.exp.output.doPrint(coVL.MAINLOOP);
		double w [] = new double[p.N];
		
		//RECOMPUTE WEIGHTS
	    classes = new particleClasses(this);
	    double sum = 0;
		for(int i=0; i<N; i++){
			//double prev = 1.0; //After resampling, the posterior of all particles is the same
			//double curr = particles[i].classInfo.particleProbability(particles[i]);
			Utils.FORCED_ASSERT(particles[i].classInfo == classes.getClass(particles[i].getClassStringSignature()), "Particle.classInfo is wrong.");
			w[i] = particles[i].w = particles[i].classInfo.particleWeight(particles[i]);
			sum += particles[i].w;
		}		
		for(int i=0; i<N; i++) particles[i].w /= sum;
	}
	//-----------------------------------------------------------------
	public void resample(){
		double w [] = new double[p.N];
		for(int i=0;i<particles.length; i++) w[i] = particles[i].w;
		
		if(false) { //set to true if you want multinomial resampling, false for residual resampling
			reassignParticles(multinomial_resampling(w, p.N, null));
		}
		else{
			reassignParticles(residual_resampling(w, p.N));
		}
	}
	//-----------------------------------------------------------------
	/**
	 * Returns the indexes for the new particles, sampled using residual resampling according to weights w.
	 * See Moulines' paper for details and notation of comments (multinomial_sampling1.pdf in CI08's folder).
	 */
	public int [] residual_resampling(double [] w, int N){
		int repetitions [] = new int[N]; //number of times i-th particle should be repeated
		double [] hat_w = new double[N];
		
		int R = 0;
		for(int i=0; i<N;i++){
			repetitions[i] = (int) (((double)N)*w[i]);//(int) Math.floor(((double)N)*w[i]);  //computing first term of N^i = floor(N*w^i) + Nhat^i
			R += repetitions[i];
		}
		//compute renormalized weights (hat(w)_i) for the multinomial
		for(int i=0; i<N;i++){
			hat_w[i] = ((double)(((double)N)*w[i] - repetitions[i])) / ((double) N - R); 
		}
		int new_particles_indexes [] = new int [N];
		new_particles_indexes = multinomial_resampling(hat_w, N-R, new_particles_indexes);
		
		//We now must combine the reptitions as indexes into the indexes obtained from the multinomial
		int k=0; //number of particles added so far.
		for(int i=0; i<N;i++){
			//For each unit in repetition add a particle
			for(int j=0; j<repetitions[i]; j++){
				new_particles_indexes[N-R+k] = i;
				k++;
			}
		}
		
		return new_particles_indexes;
	}
	//-----------------------------------------------------------------
	/**
	 * Returns the indexes for the new particles, sampled using multinoial resampling according to weights w.
	 */
	public int [] multinomial_resampling(double [] w, int N, int [] new_particle_index_out){
		//INITIALIZATION
		int new_particle_index [] = new_particle_index_out;
		if(new_particle_index == null) new_particle_index = new int[N];

		//COMPUTE indexes of new particles
    	for(int i=0; i<N; i++){
    		new_particle_index[i] = Statistics.sampleFromUnivariate(w, r);
    	}
    	
    	return new_particle_index;
	}
	/**
	 * re-assigns the particles according to the input indexes. Used in resampling.
	 */
	private void reassignParticles(int new_particle_index []){
		Particle [] old_particles = new Particle[N];
		
		for(int i=0;i<particles.length; i++) {
			old_particles[i] = new Particle(particles[i]); 
		}

		classes.removeAllParticles();
    	for(int i=0; i<N; i++){
    		particles[i] = null;	//for garbage collection.
			Particle newP = particles[i] = new Particle(old_particles[new_particle_index[i]]);//new Particle(particles[next[i]]);
			classes.addParticle(newP);
			
			particles[i].index = i;
			
			//particles[i].w = w_aux[i];
		}
    	classes.cleanUpEmptyClasses();
    	
    	/*for(int i=0; i<N; i++) {
    		particles[i].normalizedPosterior(1.0/((double)N));
    	}*/
        return;
	}
	//-----------------------------------------------------------------
	public void move(int M){
		Particle auxsp;
        boolean print = p.exp.output.doPrint(coVL.MAINLOOP);
				
        //if(print) System.out.print("MOVING STRUCTURES: \n");

         int [] accepted = new int[N];
         for(int i=0; i<N; i++) accepted[i] = 0;
         
    	 int numAccepted = 0;
         for(int j=0;j<M;j++){
             for(int i=0; i<N; i++){
//                particles[i] = metro.iterate(new Particle(particles[i]), 1);

                if(metro.wasLastAccepted && accepted[i] < p.pi) numAccepted += (metro.wasLastAccepted?1:0);
                if(metro.wasLastAccepted)  accepted[i]++;
                
            }
            if(numAccepted >= p.pi * N) {
            	break; 
            }
            int asd=0;
         }
	}
	//-----------------------------------------------------------------
	//-----------------------------------------------------------------
	public boolean terminate(){
		if(true){
			classes = new particleClasses(this);
		    
			for(particleClasses.ClassInfo ci: classes.classes.values()){
				if(ci.structures.keySet().size() != 1) return false;
			}
			return true;
		}
		else if(false){
			particleClasses.ClassInfo ci = classes.mostProbableClass();
			if(ci.structures.keySet().size() == 1) {
				return true;
			}
			return false;
		}
		return false;
	};
	//i-th Bit set means independence with that test(i)
	//-----------------------------------------------------------------
//	-----------------------------------------------------------------
	public void recomputeClasses(){
	    classes = new particleClasses(this);
	}
	//-----------------------------------------------------------------
    final public java.util.BitSet getTestsString(int X, int Y, FastSet S){
        java.util.BitSet outputTestString = new java.util.BitSet();
        outputTestString.clear();
    	for(int i=0; i<N; i++){
    		if(particles[i].separates_nonRecursive(X, Y, S)) outputTestString.set(i); 
        }
        return outputTestString;
    }
    //-----------------------------------------------------------------
    final public java.util.BitSet getTestsString2(int X, int Y, FastSet S, java.util.BitSet from, java.util.BitSet to, boolean UP){
        //java.util.BitSet to = new java.util.BitSet();
        to.clear();
        boolean testVal = false;
        int i;
       
        if(UP){
        	i = from.nextClearBit(0);
            while(i >= 0 && i < N){
                testVal = particles[i].separates_nonRecursive(X, Y, S);
                if(testVal) to.set(i);

                i = from.nextClearBit(i+1);
            }
        }
        else{
        	i = from.nextSetBit(0);
            while(i >= 0){
                testVal = particles[i].separates_nonRecursive(X, Y, S);
                if(testVal) to.set(i);

                i = from.nextSetBit(i+1);
            }
        }
        //outputTestString.clear();
        
        /*for(int i=0; i<N; i++){
            auxVal = from.get(i);
            if(UP){
                if(auxVal) {
                    testVal  = auxVal;       //if independent, by SU the value is not changed.
                }
                else {
                    testVal = particles[i].separates_nonRecursive(X, Y, S);
                    if(testVal != auxVal){
                    	int aslkgjas=0;
                    }
                }
            }
            if(!UP){
                if(!auxVal) {
                    testVal = auxVal;
                }
                else {
                    testVal = particles[i].separates_nonRecursive(X, Y, S);
                    if(testVal != auxVal){
                        int aslkgjas=0;
                    }
                }
            }
            if(testVal) {
                to.set(i);
            }
        }*/
        if(false){
            java.util.BitSet aux = getTestsString(X, Y,S);
            Utils.FORCED_ASSERT(aux.equals(to), "OOPS in getTestsString");
        }
        return to;
    }
    //-----------------------------------------------------------------
	//Computes info gain for candidate tests X, Y|S
    //alpha[] contains a single element and is used for returning alpha
    /**
     *     // boolean UP is used for speeding it up. If UP=true means that we S changed from last iteration to SU{z}, otherwise S-{z}.
    // If speedyup = false this speedup is not done. 

     */
    double igain, aux2, Cardinality;
    int card;
    int numInTrue, numInFalse;
    //Particle spTrue=null, spFalse=null, sp2;
    Particle sp2;
    double probTrue, probFalse;
    Vector classParticles;
    Enumeration configs;
	/*--------------------------------------------------------------------------*/
 /*   public double candidateTestProbability2(int X, int Y, FastSet S, boolean truth_value){
    	double sum=0;
    	for(Particle sp: particles){
    		if(sp.separates_nonRecursive(X, Y, S) == truth_value) {
    			sum += sp.normalizedPosterior;
    		}
    	}
    	return sum;
    }*/
	/*--------------------------------------------------------------------------*/
	/*--------------------------------------------------------------------------*/
    /**
     * Used for candidate test not yet tested. That is, (X,Y|S) \notin tests vector
     */
	public double infoGain(int X, int Y, FastSet S, double [] alpha, java.util.BitSet testString){
		
        Hashtable<String, ArrayList<Particle>> true_subclasses = new Hashtable<String, ArrayList<Particle>>();
        Hashtable<String, ArrayList<Particle>> false_subclasses = new Hashtable<String, ArrayList<Particle>>();
        
        //Splits particles into true and false sub classes, and stores the resulting arrays of particles in a hash table with classes as keys. 
        for(particleClasses.ClassInfo classInfo : classes.classes.values()){
			//ArrayList<Particle> [] subclasses = classInfo.candidateSubClass(X, Y, S);
			ArrayList<Particle> true_subclass = new ArrayList<Particle>();
			ArrayList<Particle> false_subclass = new ArrayList<Particle>();
			
			for(Particle p : classInfo.particles.values()) {
				if(p.separates_nonRecursive(X, Y, S)) true_subclass.add(p);
				else false_subclass.add(p);
			}
			true_subclasses.put(classInfo.key, true_subclass);
			false_subclasses.put(classInfo.key, false_subclass);
		}
        
        //Compute the info gain
        double igain = 0;
		ArrayList<Particle> true_subclass, false_subclass;
        for(particleClasses.ClassInfo classInfo : classes.classes.values()){
        	true_subclass = true_subclasses.get(classInfo.key);
        	double totalSize = classInfo.particles.size();

        	double trueSize = true_subclass.size();
        	igain += (trueSize == 0)? 0.0 : trueSize * Math.log(trueSize/totalSize);
        	
        	false_subclass = false_subclasses.get(classInfo.key);
        	double falseSize = false_subclass.size();
        	igain += (falseSize==0)? 0.0 : falseSize * Math.log(falseSize/totalSize);
        	
        	/*for(Particle p : true_subclass){
        		igain += Math.log(trueSize/totalSize);
            }
        	
        	false_subclass = false_subclasses.get(classInfo.key);
        	double falseSize = false_subclass.size();
        	for(Particle p : false_subclass){
        		igain += Math.log(falseSize/totalSize);
            }*/
        	
        }
		return -igain;
	}
	/*--------------------------------------------------------------------------*/
	//int num, num_not;
/*	double prob, prob_not;
	ClassInfo classInfo, classInfoNot;
	*//**
     * Used for candidate test already tested. That is, used for i-th test in tests array
     *//*
	public double infoGain(int i, double [] alpha, java.util.BitSet testString){
		infoGainCount++;
		gsTriplet test = tests[i];
		
		igain = 0;
        if(alpha != null) alpha[0] = 0;

        
        boolean testVal = false, auxVal;
		
		configs = classes.keys();
		while(configs.hasMoreElements()){
			classInfo = (structuresParticlesFilter.ClassInfo) classes.get(configs.nextElement());

			//num = classParticles.cardinality() ;
			//prob = classInfo.classProbMass;//((Particle) classParticles.elementAt(0)).normalizedPosterior;
	
		  *//** COMPUTE CARDINALITY AND PROBABILITY OF CLASS WHOSE VALUE FOR i-TH TEST IS NEGATED**//*
			char [] keyChars = particles[i].getClassStringSignature(tests, t).toCharArray();
		  	keyChars[i] = keyChars[i]==0? '1' : '0';
		  	String key = new String(keyChars);

		  	classInfoNot = (structuresParticlesFilter.ClassInfo) classes.get(key);
		  	if(classInfoNot == null)	continue; //since with num_not = 0, it becomes num*prob*log(1.0) = 0;
		  		  	
            if(alpha != null) {
                alpha[0] += classInfo.classParticles.size();
                Cardinality += (classInfo.classParticles.size() + classInfoNot.classParticles.size());
            }
            Utils.FORCED_ASSERT(classInfo.classProbMass != 0, "Pr(x|d) = 0    in infoGain. Will have to check what's infoGain's value in this case" +
            		"through a careful limit calculation for Pr(x|d) --> 0.");
            igain -= classInfo.classProbMass * Math.log1p(classInfoNot.classProbMass/classInfo.classProbMass);
		}
        if(alpha != null) alpha[0] /= Cardinality;
        if(igain != 0) igain = -igain;
		return igain;
	}
	-------------------------------------------------------------
    public double maxInfoGain(){
        igain = 0;
        
        configs = classes.keys();
        
        while(configs.hasMoreElements()){
            classParticles = (Vector )((structuresParticlesFilter.ClassInfo) classes.get(configs.nextElement())).classParticles;
        
            numInTrue = numInFalse = 0;
            
            card = classParticles.size() ;
            
            for(int j=0; j<card; j++){
                sp2 = (Particle) classParticles.elementAt(j);
                
                igain+= 0.5 * sp2.normalizedPosterior() * Math.log(0.5 / card);
            }
        }
        igain = -igain;
        return igain;
        
    }

//	-----------------------------------------------------------------
	public double maxEntropy_Gain_forCandidate(){
		return -N*Math.log(0.5);
	}
//	-----------------------------------------------------------------
	public double minEntropy_Gain_forCandidate(){
		return 0.0;//this.computeEntropy() + Math.log(0.5);
	}
*/	//-----------------------------------------------------------------
   /* public double computeEmpiricalEntropy2(double NN){
        double h=0;
        double delta = NN/((double)N);
        
        normalizeLogPosteriors(); //re-populates uniqueParticles

        uniqueParticles.clear();
        for(int i=0; i<N; i++){
            if(p.r.nextDouble() < delta)
            	   uniqueParticles.put(particles[i], particles[i]);
        }

        Enumeration enume = uniqueParticles.elements();
        Particle curr;
        while(enume.hasMoreElements()){
            curr = (Particle) enume.nextElement();
            h += curr.normalizedPosterior * Math.log(curr.normalizedPosterior); 
        }
        return h*(-1);
    }*/
//	-----------------------------------------------------------------
/*    class UniqueParticlesInfo{
    	Particle sp;
    	int count = 1;
    }
	public double computeEmpiricalEntropy(){
        uniqueParticles.clear();
        int k;
        for(int i=0; i<N; i++){
        	UniqueParticlesInfo upi = (UniqueParticlesInfo) uniqueParticles.get(particles[i]);
        	if(upi == null){
        		upi = new UniqueParticlesInfo();
        		upi.sp = particles[i];
        		uniqueParticles.put(particles[i], upi);
        	}
        	else{
        		upi.count++;
        	}
            k = particles[i].hashCode();
        }

        double h=0, sum = 0;
		//normalizeLogPosteriors(); 
		
        Enumeration enume = uniqueParticles.elements();
        Particle curr;
        while(enume.hasMoreElements()){
        	UniqueParticlesInfo upi = (UniqueParticlesInfo) enume.nextElement();
            h += upi.sp.normalizedPosterior()* upi.count * Math.log(upi.sp.normalizedPosterior() * upi.count); 
            sum += upi.sp.normalizedPosterior()* upi.count; 
        }
		return h*(-1);
	}*/
	//-----------------------------------------------------------------
//	-----------------------------------------------------------------
//	-----------------------------------------------------------------
//	-----------------------------------------------------------------
	/*public double computeClassEmpiricalEntropy(){
		double h=0;
		configs = classes.keys();
		ClassInfo max = null;
		while(configs.hasMoreElements()){
			String key = (String) configs.nextElement();
			double p = ((structuresParticlesFilter.ClassInfo) classes.get(key)).classProbMass;
			h += (p==0)?0:p*Math.log(p);
			
		}
		return -h;
	}*/
//	-----------------------------------------------------------------
/*	private final void normalizeWeights(int time){
		double sum = 0, aux=0;
		for(int i=0; i<particles.length; i++){
			if(time == 0) aux = particles[i].w;
			//if(time == 1) aux = particles[i].w_plus1;
			sum += aux;
		}
		for(int i=0; i<particles.length; i++){
			if(time == 0) particles[i].w /= sum;
			//if(time == 1) particles[i].w_plus1 /= sum;
		}
	}*/
//	-----------------------------------------------------------------
/*	public class ClassInfo {
		Vector classParticles = new Vector();
		double classProbMass = 0;
		boolean trueStrClass = false;
	}
	public final int reCreateClasses(){
		String key;
		int numClasses = 0;
		Hashtable  classes = new Hashtable(N);
		
		Particle trueParticle = new Particle(this, p.trueNetSimple);
		String keyTrueStr = trueParticle.getClassStringSignature(tests, t);
		//normalizeLogPosteriors();
		
	  	for(int i=0; i<N; i++){
	  		if(particles[i] == null) return -1;
	  		
	  		key = particles[i].getClassStringSignature(tests, t);

	  		ClassInfo classInfo;
	  		if(!classes.containsKey(key)){
	  			classInfo = new ClassInfo();
	  			classInfo.classParticles.add(particles[i]);
	  			classInfo.classProbMass += particles[i].normalizedPosterior();
		  		if(keyTrueStr.equals(key)) classInfo.trueStrClass = true; 
	  			classes.put(key, classInfo);
	  			
	  			numClasses++;
			}
	  		else{
	  			classInfo = (structuresParticlesFilter.ClassInfo) classes.get(key);
	  			classInfo.classParticles.add(particles[i]);
	  			classInfo.classProbMass += particles[i].normalizedPosterior();
	  		}
	  		particles[i].classInfo = classInfo;
	  	}
	  	return numClasses;
	}*/
//	-----------------------------------------------------------------
	public final double howManyDifferent(){
		//return tree.unique.cardinality();
        return uniqueParticles.entrySet().size();
	}
//	-----------------------------------------------------------------
//	-----------------------------------------------------------------
	//-----------------------------------------------------
	public final double particlesSimilarity(){
		//MarkovNet xi, xj;//x0 =(MarkovNet) particles.elementAt(0);
		double sim = 0;
		double q=0;
		
		for(int i=0; i<N; i++){
			for(int j=i+1; j<N; j++){
				q++;
				sim += particles[i].hamming(particles[j]);
			}
		}
		sim /= q;
		return q==0?0.0 : sim;
	}
	//-----------------------------------------------------------------
	//-----------------------------------------------------
	private int howManyInTrueNetClass(){
//		if(p.gs.trueNet == null) return -1;
		
		java.util.BitSet clas = new java.util.BitSet(t);
		UGraphSimple trueNetSimple = new UGraphSimple(1);
		int ne = trueNetSimple.numberOfEdges();
		
		for(int i=0; i<t; i++){
			gsTriplet y = tests[i];
		    if(trueNetSimple.separates_nonRecursive(y.X(), y.Y(), y.getCond())) clas.set(i);
		}
		
		int ret=0;
		for(int j=0; j<particles.length; j++){
			java.util.BitSet part = new java.util.BitSet(t);

			for(int i=0; i<t; i++){
				gsTriplet y = tests[i];
				if(particles[j].separates_nonRecursive(y.X(), y.Y(), y.getCond())) part.set(i);
			}
			if(clas.equals(part) && ne == particles[j].numberOfEdges()){
				ret++;
			}
		}
		return  ret;
	}
//	-----------------------------------------------------------------
	private int howManyInTrueNetTestsClass(){
//		if(p.gs.trueNet == null) return -1;
		java.util.BitSet clas = new java.util.BitSet(t);
		UGraphSimple trueNetSimple = new UGraphSimple(1);
		int ne = trueNetSimple.numberOfEdges();
		
		for(int i=0; i<t; i++){
			gsTriplet y = tests[i];
            if(trueNetSimple.separates_nonRecursive(y.X(), y.Y(), y.getCond())) clas.set(i);
		}
		
		int ret=0;
		for(int j=0; j<particles.length; j++){
			java.util.BitSet part = new java.util.BitSet(t);

			for(int i=0; i<t; i++){
				gsTriplet y = tests[i];
                if(particles[j].separates_nonRecursive(y.X(), y.Y(), y.getCond())) part.set(i);
			}
			if(clas.equals(part)){
				ret++;
			}
		}
		return  ret;
	}

//	-----------------------------------------------------------------
	public final double [] exact_accuracy(UGraphSimple trueNet){
		double min_index = -1;
		double min = Double.MAX_VALUE;
		double aux;
		
		for(int i=0; i<N; i++){
			aux = trueNet.hamming(particles[i]);
			if(aux < min){
				min = aux;
				min_index = i;
			}
		}
		double [] ret = new double[2];
		ret[0] = min_index;
		ret[1] = min;
		return ret;
	}
	//Computes average (over particles) hamming distance to true network
	//	-----------------------------------------------------------------
	//Expected accuracy
	public final double [] accuracy(UGraphSimple trueNet, Particle [] particles){
		double [] ret = new double[2];
		ret[0] = -1;
		ret[1] = -1;
		
		if(trueNet == null) return ret;
		
		double min_index = -1;
		double min = Double.MAX_VALUE;
		double accuracy = 0;
		
		if(particles == null) particles = this.particles;
		for(int i=0; i<particles.length; i++){
			accuracy += trueNet.hamming(particles[i]) ;//* particles[i].w;
		}
		
		ret[1] = accuracy / ((double)particles.length);
		return ret;
	}
		
	//	-----------------------------------------------------------------
	public double accuracy(UGraphSimple outputNet, FastDataset data, UGraphSimple trueNet, int maxCardCondSet, int numTests){
		double accuracy = 0;
		double numOfTests = 0, numTestsFailedLocal = 0;
		Set V = new Set();
		Set cond=null, Vxy;
		attributesSet as = null;
		Vector subsets;
		boolean a=true,b=true;
		GraphRVar X=null, Y=null;
		KBsimpleGSIMN kb = new KBsimpleGSIMN(V.size());
		int ix=0, iy=0;
		//MarkovNet G = this.G;
		//MarkovNet G = origNet;
		
		if(maxCardCondSet > outputNet.size()-2) maxCardCondSet = outputNet.size()-2;
		// Random sampling of tests //
		Random rand = new Random(2134);
		
		//Compute max number of tests
		double N = V.size();
		double maxnumtests = 0;
		for(int j=0; j<maxCardCondSet+1; j++) {
			double aux = Utils.choose((int)N, j);;
			maxnumtests += (double) aux;
		}
		maxnumtests *= N*(N-1)/2.0;
		//System.out.println("\n\n\nmaxnumtests: " + maxnumtests);

		for(int i=0; i<numTests; i++){
			int counter = 0;
			while(true && counter < maxnumtests){
				counter++;
				Vxy = null;
				Vxy = new Set(V);

				//Select X
				ix = rand.nextInt(V.size());
				X = null;
				X = (GraphRVar) V.elementAt(ix);
				Vxy.remove(X);
	
				//Select Y
				iy = ix;
				Y = null;
				while(iy == ix){
					iy = rand.nextInt(V.size());
				}
				Y = (GraphRVar) V.elementAt(iy);
				Vxy.remove(Y);
				
				//Select condSet
				int condSize = rand.nextInt(maxCardCondSet+1<=Vxy.size()?maxCardCondSet+1:Vxy.size());
				Set VxyCopy = new Set(Vxy);
				as = null;
				cond = null;
				cond = new Set();
				for(int j=0; j<condSize; j++){
					int condVar = rand.nextInt(VxyCopy.size());
					//as.add(  ((GraphRVar)(VxyCopy.elementAt(condVar))).attr);
					cond.add(VxyCopy.elementAt(condVar));
					VxyCopy.remove(condVar);
				}
				FactSimpleGSIMN fact = new FactSimpleGSIMN(V.size(), true, X.index(), Y.index(), cond);
				//System.out.println(fact);
				
				if(kb.contains(fact)) {
					//System.out.print(counter+",");
					continue;
					
				}
				else {
					kb.addFact(fact);
					//System.out.print(fact);
					break;
				}
			}
			if(counter == maxnumtests) break;
			
			Statistics stat = new Statistics();
			double log_dl[] ;
			try{
				if(data != null){
					boolean [] testFailed = new boolean[1];
					a = IndependenceTestMargaritisUAI2001.conditional(data, X.index(), Y.index(), cond, p.prior);
					//a = log_dl[IndependenceTestMargaritisUAI2001.D] < log_dl[IndependenceTestMargaritisUAI2001.I] ;
					//a = stat.independent(data, p.gs.Varray[X.index()].attr, p.gs.Varray[Y.index()].attr, as, 0.05, false, Statistics.CHISQUARE, testFailed);

					if(!testFailed[0]) {//that is, last test did not fail, then
						numOfTests++;
						b=outputNet.separates_nonRecursive(X.index(),Y.index(),new FastSet(cond, p.n));
						if(a==b) accuracy++;
					}
					else {
						numTestsFailedLocal++;
					}
				}
				if(trueNet != null){
					a=trueNet.separates_nonRecursive(X.index(),Y.index(),new FastSet(cond, p.n));
					b=outputNet.separates_nonRecursive(X.index(),Y.index(),new FastSet(cond, p.n));
					if(a==b) accuracy++;
					numOfTests ++;
				}
				
			}
			catch(Exception e){
				System.out.print(e);
				e.printStackTrace();
			}

			//System.out.println(i + ":("+ ix + ","+ iy + ") |" + cond + " accuracy=" + accuracy/numOfTests);
			//if(i%100 == 0) System.out.print(".");
			

		}
		if(data != null) {
			//System.out.println("  numberOfTests: " + numOfTests + "  Correct: " + accuracy);
			//System.out.println(G);
			//System.out.println(origNet);
		}
		
		return accuracy/numOfTests;
	}
	
	
	// -----------------------------------------------------------------
	public final double reliability(Triplet t){
		double average = 0;
		for(int i=0; i<N; i++){
			double splc = particles[i].shortestPathLengthConditional(t.X(), t.Y(), t.getCond());
			average += 1.0/splc;
		}
		return average /= N;
	}

	
	//======================================================================
	//G is the output net
	
	
	
	//-----------------------------------------------------------------
	
	//-----------------------------------------------------------------
	//-----------------------------------------------------------------
	//-----------------------------INTERNALS---------------------------
	//-----------------------------------------------------------------
	//-----------------------------------------------------------------
//	-----------------------------------------------------------------
	//-----------------------------------------------------------------
	/*protected void normalizeLogPosteriors2(){
		normalizeLogPosteriors(null);
	}*/
	static private double [] normalizeLogPosteriors(double [] logPosteriorsPlus1)
	{
		int N = logPosteriorsPlus1.length;
		
		double logP [] = new double[N];
		double P [] = new double[N];

		int indexes [] = new int[N]; //indexes[i] is the index of the particle who's probability is logP[i];
		double sum = 0, log_sum=0;
		for(int i=0; i<N; i++){
            if(logPosteriorsPlus1 != null) {
				logP[i] = logPosteriorsPlus1[i];
				P[i] = logPosteriorsPlus1[i];
			}
			/*else {
				//logP[i] = computeClassLogProb(particles[i]);//particles[i].logPartPosterior;
				P[i] = particles[i].normalizedPosterior;
				logP[i] = Math.log(P[i]);//particles[i].logPartPosterior;
			}*/
            sum += P[i];
            log_sum += logP[i];
        }
		//if(sum == 1.0) return P;
		
		
		//Find minimum logP
		double max = logP[0];
		for(int i=1; i<N; i++){
			if(logP[i] > max) max = logP[i];
		}
		//Now normalize temporarily using the smallest one and exponentiate
		sum=0;
		for(int i=0; i<N; i++){
            if(logP[i] - max > Math.log(Double.MAX_VALUE)) P[i] = Double.MAX_VALUE/P.length;
			else P[i] = Math.exp(logP[i] - max);
            
            sum += P[i];
		}
		
		for(int i=0; i<N; i++){
			if(sum == 0) P[i] = 1.0 / (double)P.length;
			else P[i] = P[i] / sum;
			//particles[i].normalizedPosterior = P[i];
			if(logPosteriorsPlus1 != null) {
				logPosteriorsPlus1[i] = Math.log(P[i]);
			}
		}
		
		sum = 0;
		for(int i=0; i<N; i++){
            sum += P[i];
        }

		return P;
	}		
	//-------------------------------------------------------------------------

	//-----------------------------------------------------------------
/*	public final int mostProbableParticle(){
		double strP [] = new double [N];
		
		//normalizeLogPosteriors();
		
//        Hashtable unique = new Hashtable();
//        unique.clear();
//        for(int i=0; i<N; i++)  unique.put(particles[i], particles[i]);
		
    	for(int i=0; i<N; i++){
    		strP[i] = particles[i].normalizedPosterior();
    		for(int j=0; j<N; j++){
    			if(i==j) continue;
    			if(particles[i].hashCode() == particles[j].hashCode()) {
    				strP[i] += particles[j].normalizedPosterior();
    			}
    		}
		}
    			
		
		double maxW=Double.MIN_VALUE;
		int iMax=0;
		for(int i=0; i<N; i++){ 
			if(particles[i].w > maxW) {
				maxW = particles[i].w;
				iMax = i;
			}

			if(strP[i] > maxW) {
				maxW = strP[i];
				iMax = i;
			}
		}
		return iMax;
	}*/
//	-----------------------------------------------------------------
	//-----------------------------------------------------------------
	/*public final void assignClass(Particle x){
		configs = classes.keys();
		ClassInfo max = null;
		while(configs.hasMoreElements()){
			String key = (String) configs.nextElement();
			classInfo = (structuresParticlesFilter.ClassInfo) classes.get(key);

			String xkey= x.getClassStringSignature(tests, t);
			if(key.equals(xkey)) {
				x.classInfo = classInfo;
			}
		}
	}*/
	//-----------------------------------------------------------------
	//Computes P(d_1:t | x) = P(x) * Prod_j^t P(d_j |x)
/*	//-----------------------------------------------------------------
	public final double computeClassLogProb(Particle x){
		double val2=0;
//		val2 = logPrior(x);
		
//		if(x.classInfo == null) assignClass(x);
//		val2 = (x.classInfo==null || x.classInfo.classParticles==null || x.classInfo.classParticles.size()==0)?0:Math.log(1.0/(x.classInfo.classParticles.size())); 
		
		int index;
		
		for(int j=0 ;j<t; j++){
			index = (x.separates_nonRecursive(tests[j].X(), tests[j].Y(), tests[j].getCond()))?  	IndependenceTestMargaritisUAI2001.I : 
																IndependenceTestMargaritisUAI2001.D;
			//val[j+1] = val[j]+pds[index][j];
			val2 += pds[index][j];
		}
		double aux = Math.exp(val2);
		return val2;
	}
*/	//-----------------------------------------------------------------
	//-----------------------------------------------------------------
	//Computes P(d_1:t | x) = P(x) * Prod_j^t P(d_j |x)
	//public final double computeParticleLogProb(Particle x){return computeParticleLogProb(x, true);};
/*	public final double computeParticleLogProb(Particle x){
		//if(class_) return computeClassLogProb(x);
		
		double val2 = logPrior(x);
		//if(x.classInfo != null)
		//val2 = Math.log(1.0/(x.classInfo.classParticles.size())); 
		
		
		int index;
		
		for(int j=0 ;j<t; j++){
			index = (x.separates_nonRecursive(tests[j].X(), tests[j].Y(), tests[j].getCond()))?  	IndependenceTestMargaritisUAI2001.I : 
																IndependenceTestMargaritisUAI2001.D;
			//val[j+1] = val[j]+pds[index][j];
			val2 += pds[index][j];
		}
		return val2;
	}*/
//	----------------------------------------------------------
	public final double computeClassLogProb(Particle x){
		double val2=0;
		
		int index;
		
		for(int j=0 ;j<t; j++){
			index = (x.separates_nonRecursive(tests[j].X(), tests[j].Y(), tests[j].getCond()))?  	
								IndependenceTestMargaritisUAI2001.I : IndependenceTestMargaritisUAI2001.D;
			val2 += pds[index][j];
		}
		double aux = Math.exp(val2);
		return val2;
	}
	//-----------------------------------------------------------------
	//Computes P(Y = y | d_1:t)
	/*public final double testPosterior(gsTriplet Y, boolean y){
		double p = 0;
		for(int j=0; j<N; j++){
			int index = (particles[j].separates_nonRecursive(Y.X(), Y.Y(), Y.getCond()))?  	IndependenceTestMargaritisUAI2001.I : 
				IndependenceTestMargaritisUAI2001.D;
			if(y && index == IndependenceTestMargaritisUAI2001.I) p += particles[j].classInfo.particleProbability(particles[j]); 
			if(!y && index == IndependenceTestMargaritisUAI2001.D) p += particles[j].classInfo.particleProbability(particles[j]);
		}
		return p;
	}*/
	public static void main(String[] args) {
	}
}
