package machineLearning;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Enumeration;
import machineLearning.Graphs.*;
import machineLearning.Utils.*;
/**
 * Class KB
 * 
 * Encapsulates the storage and retrieval of facts
 * 
 * @author Facundo Bromberg
 */
public class KBsimpleGSIMN implements Comparator{
	private Hashtable [][] S;	//each cell contains a Hashtable of Facts, with key = Fact.getCond().toString().
					//indeces are the indexes of the variables. For instance, {z} stored at cell i,j represents the fact
					// (V[i], {z}, V[j]). More precisely, if the KB is used for independence facts, then the fact is 
					//I(V[i], {z}, V[j]). 
	
	/** Determines whether this KB stores independencies or dependencies. <!-- --> It is used in contains (and probable elsewhere)*/
	private boolean I;
	boolean IDcombined;
	
	KBsimpleGSIMN(int numVars, boolean I){
		S = new Hashtable[numVars][];
		for(int i=0;i<numVars;i++) {
			S[i] = new Hashtable[numVars];
			for(int j=i; j<numVars;j++){
				S[i][j] = new Hashtable(10);
			}
			for(int j=0; j<i;j++){
				S[i][j] = S[j][i];
			}
		}
		this.I = I;
		IDcombined = false;
	}
	KBsimpleGSIMN(int numVars){
		S = new Hashtable[numVars][];
		for(int i=0;i<numVars;i++) {
			S[i] = new Hashtable[numVars];
			for(int j=i; j<numVars;j++){
				S[i][j] = new Hashtable(10);
			}
			for(int j=0; j<i;j++){
				S[i][j] = S[j][i];
			}
		}
		IDcombined = true;
	}
	
	public Enumeration getFacts(int x, int y){return S[x][y].elements();};
	
	public boolean smaller(Object o1, Object o2){
		return ((GraphRVar)o1).index() < ((GraphRVar)o2).index();
	}
	public boolean equal(Object o1, Object o2){
		return ((GraphRVar)o1).index() == ((GraphRVar)o2).index();
	}

	public boolean I(){
		if(IDcombined){
			System.out.println("\n\nAttempt to access KBsimpleGSIMN.I() in a IDcombined knowledge base.");
			Exception e = new Exception();
			e.printStackTrace();
			System.exit(0);
		}
		return I;
	}
	//f.cond will be sorted in place.
	/**
	 * f is the fact being add to the KB, DO NOT CHECK for existence.
	 */
	void addFact(FactSimpleGSIMN f){
		//if(contains(f)) return;
		
        if((f.X()==2 && f.Y() == 18) || (f.X()==18 && f.Y() == 2)){
            int laksdjf=0;   
           }

		int x = f.X();
		int y = f.Y();
		
		if(x==y) {
			System.out.println("\n\nERROR: adding fact I(X,FastZ,Y) with X=Y");
			System.exit(0);
		}
		if(!IDcombined && f.I() != I) {
			System.out.println("\n\nERROR: adding Independence (dependence)Fact in Dependence (independence) KB ");
			Exception e = new Exception();
			e.printStackTrace();
			System.exit(0);
		}
		
		Hashtable facts = S[x][y];
		FastSet Sp;
		
		boolean add=true;
		
        if(f.getCondNotFast() == null){
         int asdlkfjas=0;   
        }
		if(f.getCond() != null) facts.put(f.getCond().toString(), f);
		else facts.put(f.getCondNotFast().toString(), f);

	}

	FactSimpleGSIMN get(FactSimpleGSIMN f){
		int x = f.X();
		int y = f.Y();
		//FastSet FastZ = f.getCond();
		
		if(x==y) {
			System.out.println("\n\nERROR: querying for fact I(X,FastZ,Y) with X=Y");
			Exception e = new Exception();
			e.printStackTrace();
			//System.exit(0);
		}
		if(!IDcombined && f.I() != I) {
			System.out.println("\n\nERROR: querying for Independence (dependence) in Dependence (independence) KB ");
			Exception e = new Exception();
			e.printStackTrace();
			System.exit(0);
		}
		Hashtable facts = S[x][y];
		//String xx = toString();
		FactSimpleGSIMN fs;
		if(f.getCond() != null) fs = (FactSimpleGSIMN)facts.get(f.getCond().toString());
		else fs = (FactSimpleGSIMN)facts.get(f.getCondNotFast().toString());
		
		if(fs != null && fs.I() == f.I()){
			return fs;
		}
		return null;
	}

	/**
	 * returns true if a subsetEq of cond is contained in cell [index(L), index(R)].<!-- --> That is,
	 * if a fact equal (or provable equivalent by Strong Union) is in the KB.
	 */
	boolean contains(FactSimpleGSIMN f){
		return get(f) != null;
	}

	/**
	 * If K_{x,y} contains a subset of S, it returns it, otherwise it returns null 
	 */
	Set getSubset(int x, int y, Set S){
		Enumeration sets = getFacts(x, y);
		while(sets.hasMoreElements()){
			Set A = ((FactSimpleGSIMN) sets.nextElement()).getCondNotFast();
			if(Set.subSet(A, S)) return A;
		}
		return null;
	}
    FactSimpleGSIMN getSubsetFact(int x, int y, Set S){
        Enumeration sets = getFacts(x, y);
        while(sets.hasMoreElements()){
            FactSimpleGSIMN F = (FactSimpleGSIMN) sets.nextElement();
            if(Set.subSet(F.getCondNotFast(), S)) return F;
        }
        return null;
    }

	Vector getSubsets(int x, int y, Set S){
		Vector subsets = new Vector();
		Enumeration sets = getFacts(x, y);
		while(sets.hasMoreElements()){
			Set A = ((FactSimpleGSIMN) sets.nextElement()).getCondNotFast();
			if(Set.subSet(A, S)) subsets.add(A);
		}
		return subsets;
	}

	/**
	 * If K_{x,y} contains a superset of S, it returns it, otherwise it returns null 
	 */
	Set getSuperset(int x, int y, Set S){
		Enumeration sets = getFacts(x, y);
		while(sets.hasMoreElements()){
			Set A = ((FactSimpleGSIMN) sets.nextElement()).getCondNotFast();
			if(Set.subSet(S, A)) {
				return A;
			}
		}
		return null;
	}
    FactSimpleGSIMN getSupersetFact(int x, int y, Set S){
        Enumeration sets = getFacts(x, y);
        while(sets.hasMoreElements()){
            FactSimpleGSIMN F = (FactSimpleGSIMN) sets.nextElement();
            if(Set.subSet(S, F.getCondNotFast())) {
                return F;
            }
        }
        return null;
    }

	Vector getSupersets(int x, int y, Set S){
		Vector supsets = new Vector();
		Enumeration sets = getFacts(x, y);
		while(sets.hasMoreElements()){
			Set A = ((FactSimpleGSIMN) sets.nextElement()).getCondNotFast();
			if(Set.subSet(S, A)) {
				supsets.add(A);
			}
		}
		return supsets;
	}

    Vector getSubsetFacts(int x, int y, Set S){
        Vector supsetFacts = new Vector();
        Enumeration sets = getFacts(x, y);
        while(sets.hasMoreElements()){
            FactSimpleGSIMN F = (FactSimpleGSIMN) sets.nextElement();
            if(Set.subSet(F.getCondNotFast(), S)) {
                supsetFacts.add(F);
            }
        }
        return supsetFacts;
    }
    Vector getSupersetFacts(int x, int y, Set S){
        Vector supsetFacts = new Vector();
        Enumeration sets = getFacts(x, y);
        while(sets.hasMoreElements()){
            FactSimpleGSIMN F = (FactSimpleGSIMN) sets.nextElement();
            if(Set.subSet(S, F.getCondNotFast())) {
                supsetFacts.add(F);
            }
        }
        return supsetFacts;
    }

	/*public int memoryComplexity(){
		int mem = 0;
		
		for(int i=0;i<S.length;i++){
			for(int j=0;j<S.length;j++){
				//int aux2 = S.length;
				int aux = (int)((double)(S.length)/8.0 + 1.0);
				int aux2 = S[i][j].size();
				mem += 4;
				Enumeration enum = S[i][j].elements();
				while(enum.hasMoreElements()){
					mem += aux;
					enum.nextElement();
				}
			}
		}
		
		return mem; 
	}*/
	/*public void addFactWithSubSupsets(FactSimpleGSIMN f, Set V, KBsimpleGSIMN Ikb, KBsimpleGSIMN Dkb){
		//if(f.toString().equals("!I(0, {3}, 1).")){
		if(f.X()==0 && f.Y()==1){
			int k=0;
		}
		if(!IDcombined ) {
			System.out.println("\n\nERROR: Trying to use addFactWithSubSupsets on a non \"IDcombined\" KB.");
			Exception e = new Exception();
			e.printStackTrace();
			System.exit(0);
		}

		Vector SS=null;
		FastSet VmA = new FastSet(FactSimpleGSIMN.convertToFastSet(V, V.size()));
		VmA.remove(f.X());
		VmA.remove(f.Y());
		
		if(!f.I()) SS = f.getCond().getAllSubsets(FactSimpleGSIMN.convertToFastSet(V, V.size()));
		if(f.I()) SS = f.getCond().getAllSupersets(VmA);
		
		int i;
		for(i=0; i<SS.size(); i++){
			FactSimpleGSIMN f2 = new FactSimpleGSIMN(f.I(), f.X(), f.Y(), (FastSet)SS.elementAt(i));
			if(FastSet.isEqual(f.getCond(), (FastSet)SS.elementAt(i))){
				if(f.antecedentOne()!= null && f.antecedentTwo()!= null){
					if(f.antecedentOne().axiom().equals("DATA") || f.antecedentTwo().axiom().equals("DATA")){
						int k=0;
					}
				}
				f2.antecedents(f.antecedentOne(), f.antecedentTwo());
				f2.axiom(f.axiom());
			}
			else {
				f2.antecedents(f, null);
				f2.axiom("SU");
			}
			if(!contains(f2) && 
					(   (Ikb!=null && f2.I() && !Ikb.contains(f2) ) || 
					    (Dkb!=null &&!f2.I() && !Dkb.contains(f2) ) )
				)
			{
				addFact(f2);
			}
			
		}
	}
	*//**
	 * (see Maragaritis-1 notebook, page 36) Consistency implies  f \notin GSMNTestedFacts \implies f \in this(ForwardChain KB) 
	 * @param GSMNTestedFacts
	 * @return a Vector of those facts that are in "this" (i.e. infered by ForwardChain) and not infered by GSMN (i.e. not in the input: testedFacts)
	 */
	public Vector consistencyCheck(KBsimpleGSIMN testedFacts, Set V){
		FastSet emptySet = FastSet.emptySet(S.length);
		Vector SS = emptySet.getAllSupersets(FactSimpleGSIMN.convertToFastSet(V, V.size()));
		Vector newlyInfered = new Vector(10000);
		
		System.out.println("\n\nCOMPARING GSMN-GSIMN and FORWARD CHAINING.");
		System.out.println("-----------------------------------------");
		FactSimpleGSIMN fI = new FactSimpleGSIMN(true, 0,0,null, null);
		FactSimpleGSIMN fD = new FactSimpleGSIMN(true, 0,0,null, null);
		FactSimpleGSIMN f=null;
//		System.out.println(this.toString());
		for(int x=0; x<S.length; x++){
			for(int y=x+1; y<S.length; y++){
				for(int i=0; i<SS.size(); i++){
					FastSet S = (FastSet) SS.elementAt(i);
					if(S.isMember(x) || S.isMember(y)) continue;
					
					fI.set(true, x, y, S);
					fD.set(false, x, y, S);
					if(!testedFacts.contains(fI) && !testedFacts.contains(fD)){
						if(!this.contains(fI) && !this.contains(fD)){
							System.out.println("\n\nERROR: in KBsimpleGSIMN.consistencyCheck.");
							Exception e = new Exception();
							e.printStackTrace();
							System.exit(0);
						}
					}
					else{
						if(this.contains(fI)) {
							//newlyInfered.add(fI);
							f=get(fI);
							System.out.println(f.proof());
						}
						else if(this.contains(fD)) {
							f=get(fD);
							System.out.println(f.proof());
						}
						
					}
				}
			}
		}
		return newlyInfered;
	}
	/**
	 * Same as above but now "this" is a knowledge base with tested facts (not infered, i.e. its complement).
	 */
		public String toString(){
		return toString(false);
	}
	public String toString(boolean proof){
		String ret = "";
		for(int i=0;i<S.length;i++){
			for(int j=i+1; j<S.length; j++){
				Hashtable SOCS = S[i][j];
				Enumeration en = SOCS.elements();
				while(en.hasMoreElements()){
					if(proof)ret += ((FactSimpleGSIMN) en.nextElement()).proof() + "\n";
					else ret += ((FactSimpleGSIMN) en.nextElement()).toString() + "\n";
/*					int i1 = i, j1 = j;
					String ret2 = I?"":"!"; 
					ret += ret2+"I(" + i1 +", " + ((FastSet)((FactSimpleGSIMN)en.nextElement()).getCond()).toString() + ", " + j1 + ").\n"; 
*/				}
			}
		}
		return ret;
	}
}
