package machineLearning;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Map.Entry;

//import machineLearning.Datasets.IndependenceTestMargaritisUAI2001;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.Utils.Utils;


/**
 * Maintains the classes of all particles
 */
public class particleClasses {
	/**
	 * Represents a single class
	 */
	class ClassInfo {
		Hashtable<String, Particle> particles = new Hashtable<String, Particle>();
		//FastSet particles = new FastSet(s.N);
		Hashtable<String, Particle> structures = new Hashtable<String, Particle>();
		Hashtable<String, Integer> particlesPerStructures = new Hashtable<String, Integer>();
		double logConditional_dt_yt = 0;
		double conditional_dt_yt = 0;
		double logProbMass = 0;
		double probMass = 0;
		String key;
		boolean trueStrClass = false;
		boolean mostProbable = false;
		double size;
		
		ClassInfo(String key){
			this.logConditional_dt_yt = Particle.computeLogConditional_dt_yt(s, key);
			this.logProbMass = Particle.computeClassLogProb(s, key);
			this.key = key;
			
			Particle trueS = s.p.trueNetSimple!=null? new Particle(s, s.p.trueNetSimple) : null;
			if(trueS!= null && trueS.getClassStringSignature().equals(key)) trueStrClass = true;
		}
		//------------
		/*private int getIndex(Particle x){
			int i=0;
			for(Particle xx : particles.values()) {
				if(x==xx) return i;
				i++;
			}
			return -1;
		}*/
//		------------
		public final void addParticle(Particle x){
			x.classInfo = this;

			if(!Utils.FORCED_ASSERT(particles.get(x)==null, "Trying to add a particle that is stored in ClassInfo.particles.")){
				int safasd=0;
			}
			particles.put(x.hashStringParticle(),x);
			//x.indexInClass = particles.size()-1;
			String keyStruct = x.hashString();
			structures.put(keyStruct,x);
			
			Integer count = particlesPerStructures.remove(keyStruct);
			if(count == null) count = new Integer(0);
			else{
				int asdfas=0;
			}
			count++;
			particlesPerStructures.put(keyStruct, count);
			
			
			//Assert particlesPerStructures adds up to N, the number of particles;
			if(false){
				int sum = 0;
				for(Integer I : particlesPerStructures.values()) sum += I;
				Utils.FORCED_ASSERT(sum == particles.size(), "Mistmatch between number of particles and sum of counts");
			}
		}
		//------------
		public final void removeParticle(Particle x){
			//x.classInfo = null;
			//numParticles--;
			Particle xx = particles.get(x.hashStringParticle());
			if(!Utils.FORCED_ASSERT(xx!=null, "Trying to remove a particle that is not stored in ClassInfo.particles.")){
				int safasd=0;
			}
			particles.remove(x.hashStringParticle());
			String keyStruct = x.hashString();
			
			Integer count = particlesPerStructures.remove(keyStruct);
			Utils.FORCED_ASSERT(count != null, "Trying to decrement an inexisting counter");
			count--;
			if(count > 0) particlesPerStructures.put(keyStruct, count);
			else structures.remove(keyStruct);

			if(false){
				int sum = 0;
				for(Integer I : particlesPerStructures.values()) sum += I;
				Utils.FORCED_ASSERT(sum == particles.size(), "Mistmatch between number of particles and sum of counts");
			}
		}
		//------------
		private final void removeAll(){
			particles.clear();
			structures.clear();
			particlesPerStructures.clear();
		}
		//------------
		public final double particleWeight(Particle p){
			//Utils.FORCED_ASSERT(structures.size() > 0, "No structures?");
			Integer Nx = particlesPerStructures.get(p.hashString());
			if(Nx == null) Nx = 1;
			//return (probMass/((double)particles.size()))/((double)Nx);// * Math.exp(s.logPrior(p));
			//return probMass;// * Math.exp(s.logPrior(p));
			//return (probMass/((double)Nx));// Math.exp(s.logPrior(p));
			
			return conditional_dt_yt;
		}
//		------------
		public final double normalizedClasssProbability(){
			return probMass;
		}
		//------------
		//Return an array of ArrayList, one for true subclass (index 1), one for false subclass (index 0).
/*		public ArrayList<Particle> candidateSubClass(int X, int Y, FastSet Z){
			ArrayList<Particle>[] subclasses = new ArrayList<Particle>[2];
			
			for(Particle x : particles.values()) {
				if(x.separates_nonRecursive(X, Y, Z) == truth_value) subclass.add(x);
			}
			return subclass;
		}*/
		//------------
		public String toString(){
			Particle [] parts = new Particle[particles.size()];
			int i=0;
			for(Particle p : particles.values()) parts[i++] = p;
			
		 	return "|class|="+particles.size()+" (Pr(str)="+ Utils.myDoubleToString(particleWeight(parts[0]),6)+ ", "+  
		 			"Pr(class_mass)="+Utils.myDoubleToString(normalizedClasssProbability(),8) + ", " + 
		 			"Similarity=" + Utils.myDoubleToString(particlesSimilarity(),2) +
		 			", AvgHamm2True=" + Utils.myDoubleToString(s.accuracy(s.p.trueNetSimple, parts)[1],2) 
		 			+ ")" + (trueStrClass?" *":"")
		 			+ (mostProbable?" +":"")
		 			+ "   "+key+"\n";
		 	
		}
		//------------
		public final double particlesSimilarity(){
			double sim = 0;
			double q=0;
			
			for(Particle sp : particles.values()){
				for(Particle spp : particles.values()){
					q++;
					sim += sp.hamming(spp);
				}
			}
			sim /= q;
			return q==0?0.0 : sim;
		}
	}
	//************************************************************************
	//Hashtable<Particle, Particle> structures;
	Hashtable<String, ClassInfo> classes;
	structuresParticlesFilter s;
	//ClassInfo trueClass;
	String trueKey;
	
	public particleClasses(structuresParticlesFilter s){
		classes = new Hashtable<String,ClassInfo>();
		//structures = new Hashtable<Particle, Particle>();
		this.s = s;
	
		Particle trueS = s.p.trueNetSimple!=null? new Particle(s, s.p.trueNetSimple) : null;
		trueKey = trueS!=null? trueS.getClassStringSignature() : "";

		int i=0;
		for(Particle x : s.particles)  {
			x.recomputeStringSignature();
			addParticle(x);
			i++;
		}
		for(ClassInfo ci : classes.values()) ci.size = ci.particles.size();
		
/*		Particle trueS = new Particle(s, s.p.trueNetSimple);
		s.classes.addParticle(trueS);*/

		//Stores true class.
//		Particle trueS = new Particle(s, s.p.trueNetSimple);
//		addParticle(trueS);
/*		String key = trueS.getClassStringSignature();
		trueClass = new ClassInfo(key);
		classes.put(key, trueClass);*/
		
		
		
		normalizeClassLogPosteriors();
		normalizeConditional_dt_yt();
	}
	//-----------------------------------------------------
	public ClassInfo getClass(String key){
		return classes.get(key);
	}
	//-----------------------------------------------------
	public ClassInfo getClass(Particle x){
		return classes.get(x.getClassStringSignature());
	}
	//-----------------------------------------------------
	public void removeAllParticles(){
		for(ClassInfo ci : classes.values()){
			/*Particle [] parts = new Particle[ci.particles.values().size()];
			int i=0;
			for(Particle p : ci.particles.values()) parts[i++] = p;
			for(Particle p : parts){
				removeParticle(p);
			}*/
			ci.removeAll();
		}
	}
//	-----------------------------------------------------
	public void cleanUpEmptyClasses(){
		ArrayList<ClassInfo> toDelete = new ArrayList<ClassInfo>();
		
		for(ClassInfo ci : classes.values()){
			if(ci.particles.size() == 0) {
				toDelete.add(ci);
			}
		}
		for(ClassInfo ci : toDelete){
			classes.remove(ci.key);
		}
	}
	//-----------------------------------------------------
	public ClassInfo addClass(String key){
		Utils.FORCED_ASSERT(classes.get(key) == null, "Trying to add an existing class");
		
		ClassInfo ci = new ClassInfo(key);
		classes.put(key, ci);
		
		normalizeClassLogPosteriors();

		return ci;
	}
	//-----------------------------------------------------
	public void removeClass(String key){
		Particle trueS = new Particle(s, s.p.trueNetSimple);
		String keyTrue = trueS.getClassStringSignature();
		if(key.equals(keyTrue)) return;
		//addParticle(trueS);

		ClassInfo ci = classes.get(key);

		Utils.FORCED_ASSERT(ci != null, "Trying to remove a non existing class.");
		Utils.FORCED_ASSERT(ci.particles.size() == 0, "Trying to remove a non empty class.");
		
		classes.remove(ci);

	}
	//-----------------------------------------------------
	public void addParticle(Particle x){
		String key = x.getClassStringSignature();
		ClassInfo ci = classes.get(key);
		//ClassInfo ci = x.classInfo;
		if(ci != null) {
			ci.addParticle(x);
				
		}
		else {
			ci = new ClassInfo(key);
			ci.addParticle(x);
			classes.put(key, ci);
		}
		if(ci.key.equals(trueKey)) ci.trueStrClass = true;  
		
		if(false){
			for(ClassInfo c : classes.values()){
				while(true){
					ArrayList<Particle> parts = new ArrayList<Particle>();
					for(Particle y : c.particles.values()) parts.add(y);
					int numStructures = 0;
					while(!parts.isEmpty()){
						Particle y = parts.remove(0);
						//parts.remove(y);
						
						numStructures++;
						ArrayList<Particle> partsSub = new ArrayList<Particle>();
						for(Particle z : parts) partsSub.add(z);
						while(!partsSub.isEmpty()){
							Particle z = partsSub.remove(0);
							if(y.hashString().equals(z.hashString())) {
								parts.remove(y);
							}
						}
					}
					if(!Utils.FORCED_ASSERT(numStructures == c.structures.values().size(), "Error with structures")){
						int asdfas=0;
						continue;
					}
					if(!Utils.FORCED_ASSERT(numStructures == c.structures.size(), "Error with structures")){
						int asdfas=0;
						continue;
					}
					break;
				}
			}
		}
	}
	//-----------------------------------------------------
	public void removeParticle(Particle x){
		String key = x.getClassStringSignature();
		ClassInfo ci = classes.get(key);
		//ClassInfo ci = x.classInfo;

		Utils.FORCED_ASSERT(ci != null, "Trying to remove a structure from an empty class");
		
		ci.removeParticle(x);
		
		if(ci.particles.size() == 0) classes.remove(ci);
		
		/*Particle structure = structures.get(x);
		Utils.FORCED_ASSERT(structure != null, "Trying to remove a structure that is not in structures.");
		structure.removeParticle();*/
	}
	//-----------------------------------------------------
	/**
	 * Removes the particle x from its class and adds particle xstar to its class
	 */
	public void transferParticle(Particle x, Particle xstar){
		if(x == xstar) return; //shortcut
		
		addParticle(xstar);
		removeParticle(x);
	}
	//-----------------------------------------------------
	/**
	 * 
	 */
	/*public void moveParticle(Particle x, Structure xstar){
		if(x == xstar) return; //shortcut
		
		addParticle(xstar);
		removeParticle(x);
	}*/
	//-----------------------------------------------------
	public final ClassInfo mostProbableClass(){
		double maxW=-Double.MAX_VALUE;

		ClassInfo max = null;
		for(Entry<String,ClassInfo> e: classes.entrySet()){
			if(e.getValue().normalizedClasssProbability() > maxW) {
				maxW = e.getValue().normalizedClasssProbability();
				max = e.getValue();
			}
		}
		return max;
	}
	//-----------------------------------------------------
	protected void normalizeParticlesLogPosteriors()
	{
		int size =0;
		for(ClassInfo ci : classes.values()) size += ci.particles.values().size();

		double logArray [] = new double[size];
		
		int i=0;
		for(ClassInfo ci : classes.values()){
			for(Particle p : ci.particles.values()){
				logArray[i] = ci.logProbMass + structuresParticlesFilter.logPrior(p);
				i++;
			}	
		}
		
		double array [] = Utils.normalizeLogArray(logArray);
		
		i=0;
		for(ClassInfo ci : classes.values()){
			for(Particle p : ci.particles.values()){
				p.normalizedProbMass = array[i++];
			}
		}
	}	
	//-----------------------------------------------------
	protected void normalizeClassLogPosteriors()
	{
		double logArray [] = new double[classes.values().size()];
		
		int i=0;
		for(ClassInfo ci : classes.values()){
			logArray[i] = ci.logProbMass;
			i++;
		}
		
		double array [] = Utils.normalizeLogArray(logArray);
		
		int imax = Utils.indexOfmax(array);
		i=0;
		for(ClassInfo ci : classes.values()){
	        ci.probMass = array[i];
	        if(i == imax) ci.mostProbable = true;
	        i++;
		}
	}	
	//-----------------------------------------------------
	/**It may seem inappropiate to normalize a conditional over classes, but the fact is that this is exactly the weight, and so the goal of
	this routine is to normalize the weights.*/
	private void normalizeConditional_dt_yt()
	{
		
		double logArray [] = new double[classes.values().size()];
		
		int i=0;
		for(ClassInfo ci : classes.values()){
			logArray[i] = ci.logConditional_dt_yt;
			i++;
		}
		
		double array [] = Utils.normalizeLogArray(logArray);
		
		
		//normalizeConditional_dt_yt_old();
		
		i=0;
		for(ClassInfo ci : classes.values()){
	        ci.conditional_dt_yt = array[i++];
		}
		
		
	}
		
	//------------------------------------
	private void normalizeConditional_dt_yt_old(){
				
		
		//double N = classes.size();
		double sum = 0;

		//Find minimum logP
		double max = -Double.MAX_VALUE;
		for(ClassInfo ci : classes.values()){
			if(ci.logConditional_dt_yt > max) max = ci.logConditional_dt_yt;
		}
		//Now normalize temporarily using largest one and exponentiate.
		//First, compute sum
		sum=0;
		for(ClassInfo ci : classes.values()){
            if(ci.logConditional_dt_yt - max > Math.log(Double.MAX_VALUE)) ci.conditional_dt_yt = Double.MAX_VALUE;///N;
			else ci.conditional_dt_yt = Math.exp(ci.logConditional_dt_yt - max);
            
            sum += ci.conditional_dt_yt;
		}
		//Now normalize
		for(ClassInfo ci : classes.values()){
	        if(sum == 0) ci.conditional_dt_yt = 0.0 ; //1.0/N;
			else ci.conditional_dt_yt /=  sum;
		}
		
		//TO CHECK IF NORAMLIZEATION IS CORRECT
			sum = 0;
			for(ClassInfo ci : classes.values()){
	            sum += ci.conditional_dt_yt;
	        }
	}		

	//-----------------------------------------------------
	public Particle mostProbableParticle(){
		return (Particle) mostProbableClass().particles.values().toArray()[0];
	}
	//-----------------------------------------------------
	public String toString(){
		String ret = "";//"True class probability = " + Math.exp(trueClass.logProbMass) +"\n";
		
		for(ClassInfo ci : classes.values()) ret += ci.toString();
		
		return ret;
	}

}

