package machineLearning.independenceBased;

import java.util.Collection;
import java.util.HashMap;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.BN;
import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.Utils.Set;
import machineLearning.Utils.Utils;
import machineLearning.Datasets.*;

public class chiSquare extends independenceTestOnData {
	public static double default_alpha = 0.05;
	
	int nCellsFailed = 0;

	//protected double entropyOfExpected;  
	
	// --------------------------------------------------------------
	public chiSquare(FastDataset data) {
		super(data, null, null, Math.log(default_alpha));
	}
	// --------------------------------------------------------------
	public chiSquare(FastDataset data, double threshold) {
		super(data, null, null, Math.log(threshold));
	}
	// --------------------------------------------------------------
	public chiSquare(FastDataset data, UGraphSimple trueModel) {
		super(data, trueModel, null, Math.log(default_alpha));
	}
//	 --------------------------------------------------------------
	public chiSquare (FastDataset data, BN trueModel) {
		super(data, null, trueModel, Math.log(default_alpha));
	}
	// --------------------------------------------------------------
	public chiSquare(FastDataset data, UGraphSimple trueModel, double threshold) {
		super(data, trueModel, null, Math.log(threshold));
	}
//	 --------------------------------------------------------------
	public chiSquare(FastDataset data, BN trueModel, double threshold) {
		super(data, null, trueModel, Math.log(threshold));
	}
	//--------------------------------------------------------------
	final protected double computeLogPValue(TripletSets triplet){
		return chiSquareCond(triplet);
	}
	// --------------------------------------------------------------
	final public boolean hasTestCorrection(){return false;};

//	 --------------------------------------------------------------
	public boolean independentCorrected(Triplet triplet) 
	{
		if(!hasTestCorrection()) Utils.FORCED_ASSERT(false, "Can't query the error correction engine, hasCorrection = false.");
		return false;
	}
	
	// --------------------------------------------------------------
	private double chiSquareCond(TripletSets triplet) {
		
		FastSet X = triplet.X;
		FastSet Y = triplet.Y;
		FastSet Z = triplet.FastZ;
		
		double ret = sanityChecks(X, Y, Z);
		if(ret == Double.MIN_VALUE || ret == 0.0) return ret;

		HashMap<SliceForHashTable, double[][]> contTables  = readTables(triplet);

		int numConf = contTables.size();

		
		df = (nvals_A * nvals_B - 1) * numConf;//

		/** Computes conditional chsq value */
		double chsq = 0.0;
		//entropyOfExpected = 0;
		nCellsFailed = (int) (nvals_A * nvals_B * (nvals_C - numConf)); //number of empty cells
		int aux = nCellsFailed;
		for(double [][] ct : contTables.values()){
			chsq += chiVal(ct);
			
			ct = null;
		}
		contTables = null;
		
		reliability = -((double) nCellsFailed)/(double)(nvals_A * nvals_B * nvals_C);
		//reliability = entropyOfExpected;
		
		/** Checks if failed */
		double ratio = (double) nCellsFailed	/ ((double) nvals_A * nvals_B * numConf);
		if (ratio >= 0.4 || df <= 0)  failed = true;

		double logP_value = chsq==0? 0.0 : ln_gammq(.5*df, 0.5*chsq);
		//double power = 1-Math.exp(logP_value);

		return logP_value;
	}
//	 ------------------------------------------------------------
	private double sanityChecks(FastSet X, FastSet Y, FastSet Z){
		if (!FastSet.Intersection(X,Y).isEmpty())
			return 0.0; /* Log(1.0) Shortcut. */
		
		
		//		count number of values in of X's attributes
		nvals_A = 1l;
		int k = X.first();
		while(k >= 0){
			nvals_A *= data.schema().getAttribute(k).size();
			k = X.next();
		}
		
		//count number of values in of Y's attributes
		nvals_B = 1l;
		k = Y.first();
		while(k >= 0){
			nvals_B *= data.schema().getAttribute(k).size();
			k = Y.next();
		}
		
//		count number of values in of FastZ's attributes
		nvals_C = 1l;
		k = Z.first();
		while(k >= 0){
			nvals_C *= data.schema().getAttribute(k).size();
			k = Z.next();
		}
		
		/* Sanity checks. */
		if (nvals_A <= 1) {
			return Double.MIN_VALUE; //Log(0.0)
		}
		if (nvals_B <= 1) {
			return Double.MIN_VALUE; //Log(0.0)
		}
		
		return 0.5; //something different than 0 or 1
	}
	// --------------------------------------------------------------
	//final public double confidence (){return confidence;};
	// --------------------------------------------------------------
	// --------------------------------------------------------------
	// --------------------------------------------------------------
	// --------------------------------------------------------------
/*	*//**
	 * Computes chi-value for one cell in a contingency table.
	 * 
	 * @param freq
	 *            the observed frequency in the cell
	 * @param expected
	 *            the expected frequency in the cell
	 * @return the chi-value for that cell; 0 if the expected value is too close
	 *         to zero
	 *//*
	private static double chiCell(double freq, double expected, boolean yates) {

		// Cell in empty row and column?
		if (expected <= 0) {
			return 0;
		}

		// Compute difference between observed and expected value
		double diff = Math.abs(freq - expected);

		// Return chi-value for the cell
		return (diff * diff / expected);
	}
*/
	/**
	 * Computes chi-squared statistic for a contingency table.
	 * 
	 * @param matrix
	 *            the contigency table
	 * @param yates
	 *            is Yates' correction to be used?
	 * @return the value of the chi-squared statistic
	 */


	/*
	 * ----------------------------------------------------------------------
	 * 
	 * Auxiliary functions, needed by chisquare().
	 * 
	 * ----------------------------------------------------------------------
	 */
	/* Forward declarations. */
/*//	 ------------------------------------------------------------
	protected double percentageCellsFailed(HashMap<String, double[][]> contTables){
		double totalCellsFailed = nvals_A * nvals_B * (nvals_C - numConf);
		
		nCellsFailed = 0;
		for(double [][] ct : contTables.values()){
			chiVal(ct);
			//totalCellsFailed += nCellsFailed[0];
		}
		
		
		return totalCellsFailed;
	}

*/	// ------------------------------------------------------------
	protected double chiVal(double[][] matrix) {
		int nrows, ncols, row, col;
		double[] rtotal, ctotal;
		double expect = 0, chival = 0, n = 0;
		//boolean yates = false;

		//nCellsFailed[0] = 0;

		nrows = matrix.length;
		ncols = matrix[0].length;
		rtotal = new double[nrows];
		ctotal = new double[ncols];
		for (row = 0; row < nrows; row++) {
			for (col = 0; col < ncols; col++) {
				rtotal[row] += matrix[row][col];
				ctotal[col] += matrix[row][col];
				n += matrix[row][col];
			}
		}

		chival = 0.0;
		for (row = 0; row < nrows; row++) {
			for (col = 0; col < ncols; col++) {
				if (ctotal[col] == 0 || rtotal[row] == 0) {
					df--;
					continue;
				}
				expect = (ctotal[col] * rtotal[row]) / n;
				//entropyOfExpected -= (expect == 0.0)? 0.0 : expect/n * Math.log(expect/n);
				
				if (expect != 0)
					//chival += Math.abs(matrix[row][col] - expect) * Math.abs(matrix[row][col] - expect) / expect;
					chival += computeCell (matrix[row][col], expect);
				if (expect < 5.0) {
					nCellsFailed++;
				}
			}
		}
		return chival;
	}
 //--------------------------------------------------
	//Oij is the frequency count and Eij is the null-hypthesis (independence) model expected value
	protected double computeCell(double Oij, double Eij){
		return Math.abs(Oij - Eij)	* Math.abs(Oij - Eij) / Eij;
	}
	 //--------------------------------------------------

	static double gamser, gammcf, gln;

	static double FPMIN = 1.0e-30;

	static int ITMAX = 2000;

	static double EPS = 3.0e-7;

	static void nerror(String msg) {
		System.out.println("%s" + msg);
		System.exit(1);
	}

	public static double gammq(double df, double x) {

		if (x < 0.0 || df <= 0.0) {
			nerror("Invalid arguments in routine gammq()");
			(new Exception()).printStackTrace();
			System.exit(0);
		}
		if (x < (df + 1.0)) {
			if (x == 0)
				return 1;

			gser(df, x);
			return 1.0 - gamser;
		} else {
			gcf(df, x);
			return gammcf;
		}
	}

	static void gser(double df, double x) {
		int n;
		double sum, del, ap;

		gln = gammln(df);
		if (x < 0.0) {
			nerror("x less than 0 in routine gser()");
			return;
		} else {
			ap = df;
			del = sum = 1.0 / df;
			for (n = 1; n <= ITMAX; n++) {
				++ap;
				del *= x / ap;
				sum += del;
				if (Math.abs(del) < Math.abs(sum) * EPS) {
					gamser = sum * Math.exp(-x + df * Math.log(x) - (gln));
					return;
				}
			}
			nerror("a too large, ITMAX too small in routine gser()");
			return;
		}
	}

	static void gcf(double a, double x) {
		int i;
		double an, b, c, d, del, h;

		gln = gammln(a);
		b = x + 1.0 - a;
		c = 1.0 / FPMIN;
		d = 1.0 / b;
		h = d;
		for (i = 1; i <= ITMAX; i++) {
			an = -i * (i - a);
			b += 2.0;
			d = an * d + b;
			if (Math.abs(d) < FPMIN)
				d = FPMIN;
			c = b + an / c;
			if (Math.abs(c) < FPMIN)
				c = FPMIN;
			d = 1.0 / d;
			del = d * c;
			h *= del;
			if (Math.abs(del - 1.0) < EPS)
				break;
		}
		if (i > ITMAX)
			nerror("a too large, ITMAX too small in gcf()");
		gammcf = Math.exp(-x + a * Math.log(x) - (gln)) * h;
	}

	static double gammln(double xx) {
		double x, y, tmp, ser;
		double cof[] = { 76.18009172947146, -86.50532032941677,
				24.01409824083091, -1.231739572450155, 0.1208650973866179e-2,
				-0.5395239384953e-5 };
		int j;

		y = x = xx;
		tmp = x + 5.5;
		tmp -= (x + 0.5) * Math.log(tmp);
		ser = 1.000000000190015;
		for (j = 0; j <= 5; j++)
			ser += cof[j] / ++y;
		return -tmp + Math.log(2.5066282746310005 * ser / x);
	}

	// //////////////////////////////////////////////////////////////
	// // FUNCTION FOR COMPUTING log_gammq
	// ////////////////////////////////////////////////////////////////
	public static double ln_gammq(double df, double x) {

		if (x < 0.0 || df <= 0.0) {
			Utils.FORCED_ASSERT(true, "Invalid arguments in routine gammq()");
		}
		if (x < (df + 1.0)) {
			if (x == 0)
				return 0;

			return Math.log(-Math.expm1(ln_gser(df, x)));
		} else {
			return ln_gcf(df, x);
		}
	}

	static double ln_gser(double df, double x) {
		int n;
		double sum, del, ap;

		gln = gammln(df);
		if (x < 0.0) {
			Utils.FORCED_ASSERT(true, "x less than 0 in routine gser()");
			return -1;
		} else {
			ap = df;
			del = sum = 1.0 / df;
			for (n = 1; n <= ITMAX; n++) {
				++ap;
				del *= x / ap;
				sum += del;
				if (Math.abs(del) < Math.abs(sum) * EPS) {
					// gamser = sum * Math.exp(-x + df * Math.log(x) - (gln));
					return Math.log(sum) - x + df * Math.log(x) - (gln);
				}
			}
			Utils.FORCED_ASSERT(true,
					"a too large, ITMAX too small in routine gser()");
			return -1;
		}
	}

	static double ln_gcf(double a, double x) {
		int i;
		double an, b, c, d, del, h;

		gln = gammln(a);
		b = x + 1.0 - a;
		c = 1.0 / FPMIN;
		d = 1.0 / b;
		h = d;
		for (i = 1; i <= ITMAX; i++) {
			an = -i * (i - a);
			b += 2.0;
			d = an * d + b;
			if (Math.abs(d) < FPMIN)
				d = FPMIN;
			c = b + an / c;
			if (Math.abs(c) < FPMIN)
				c = FPMIN;
			d = 1.0 / d;
			del = d * c;
			h *= del;
			if (Math.abs(del - 1.0) < EPS)
				break;
		}
		if (i > ITMAX)
			nerror("a too large, ITMAX too small in gcf()");
		return -x + a * Math.log(x) - (gln) + Math.log(h);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int D [] = {10, 20, 40, 80, 160, 320, 640, 900, 1200, 1600, 2400};
		for(int d : D){
			String dataFileName = "sampledData_n6_t1_E100_M500_R8.csv";
			FastDataset data = new FastDataset(dataFileName);
			MarkovNet xMn = new MarkovNet();
		    xMn.read(data, dataFileName, 5, d);
		    UGraphSimple trueNet =  new UGraphSimple(new UGraphSimple(xMn));
		   // System.out.println(trueNet);
		    
		    int X = 1;
		    int Y = 4;
		    FastSet FastZ = new FastSet(6);
		    chiSquare cs = new chiSquare(data, 0.05);
			pFact pf = cs.independent(new Triplet(X, Y, FastZ));
			System.out.println(d + ", " + pf.toStringPValue());
			
/*			Set Z = new Set();
			double logpvalue[] = new double[1];
			
			int k = FastZ.first();
			while(k >= 0) {
				Z.add(data.schema().getAttribute(k));
				k = FastZ.next();
			}
			boolean out = Statistics.independent(data, X, Y, Z, 0.05, false, Statistics.CHISQUARE, null, logpvalue);
			System.out.println((out?"I":"!I") + (Triplet.toString(X, Y, FastZ)) + " = " + logpvalue[0]);*/
		}
	}

}
