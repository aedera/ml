package machineLearning.independenceBased;

import machineLearning.Utils.FastSet;

/**
 * @author bromberg
 *
 */
public class pFact {
	protected TripletSets triplet;
	final public boolean truth_value;
	protected double logpvalue;  
	final boolean failed;
	//final protected String string;
	
	protected pFact(pFact f, boolean truth_value){
		this.triplet = f.triplet;
		this.truth_value = truth_value;
		this.logpvalue = f.logpvalue;
		this.failed = f.failed;
		
		//string = (truth_value?" I":"!I") + triplet.toString();
	}
	public pFact(pFact f){
		this.triplet = f.triplet;
		this.truth_value = f.truth_value;
		this.logpvalue = f.logpvalue;
		this.failed = f.failed;

		//string = (truth_value?" I":"!I") + triplet.toString();
	}
	public pFact(TripletSets trip, boolean truth_value) {
		triplet = trip;
		this.truth_value = truth_value;
		this.logpvalue = -1;
		this.failed = false;

		//string = (truth_value?" I":"!I") + triplet.toString();
	}
	public pFact(Triplet trip, boolean truth_value) {
		int n = trip.getCond().cardinality();
		triplet = new TripletSets(new FastSet(n,trip.X()), new FastSet(n, trip.Y()), trip.getCond());
		this.truth_value = truth_value;
		this.logpvalue = -1;
		this.failed = false;
		
		//string = (truth_value?" I":"!I") + triplet.toString();
	}
	public pFact(TripletSets trip, boolean truth_value, double logpvalue) {
		triplet = trip;
		this.truth_value = truth_value;
		this.logpvalue = logpvalue;
		this.failed = false;

		//string = (truth_value?" I":"!I") + triplet.toString();
	}
	public pFact(TripletSets trip, boolean truth_value, double logpvalue, boolean failed) {
		triplet = trip;
		this.truth_value = truth_value;
		this.logpvalue = logpvalue;
		this.failed = failed;
		
		//string = (truth_value?" I":"!I") + triplet.toString();
	}
	
	final public void symmetrice(){triplet = triplet.symmetric();};

	//public TripletAbstract triplet(){return triplet;};
	public boolean truth_value(){return truth_value;};
	public Triplet triplet(){return new Triplet(triplet.X().first(), triplet.Y().first(), triplet.getCond());};

	public pFact negation(){return new pFact(this, !truth_value);};

	final public double logpvalue(){return this.logpvalue;}; 
	public void logpvalue(double logpvalue){
		this.logpvalue = logpvalue;
	}; 
	
	//public TripletAbstract triplet(){return triplet;};
	
	public int numVars(){return triplet.numVars();};
	
	final public FastSet X(){return ((TripletSets)triplet).X();};
	final public FastSet Y(){return ((TripletSets)triplet).Y();};
	final public FastSet getCond(){return triplet.getCond();};

	

	final public String toString(){
		//return toStringInternal(triplet, truth_value);
		//return string;
		return (truth_value?" I":"!I") + triplet.toString();
	}
	
	final private String getKey(){
		return toString();
	}
	
	public int hashCode() {
		return toString().hashCode();
	}
	public boolean equals(Object o) {
		return toString().equals(o.toString());
	}
	final public boolean isEqual(int x, int y, int ... Z){
		return triplet.isEqual(x, y, Z);
	}
	final public boolean isEqual(boolean truth_value, int x, int y, int ... Z){
		return this.truth_value == truth_value && triplet.isEqual(x, y, Z);
	}

/*	static public String getKey(TripletSets triplet, boolean truth_value){
//		return (truth_value?" I":"!I") + TripletSets.getKey(X, Y, Z);
		return toStringInternal(triplet, truth_value);
	}*/
/*	static final public String toStringInternal(TripletSets triplet, boolean truth_value){
		//return toString();
		return (truth_value?" I":"!I") + triplet.toString();//TripletSets.getKey(X, Y, Z);
	}*/

	final public String toStringPValue(){
		return toStringTriplet()+ " = " + Math.exp(logpvalue);
	}
	final public String toStringTriplet(){
		return triplet.toString();
	}
}
