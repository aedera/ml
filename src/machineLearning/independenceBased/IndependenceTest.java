package machineLearning.independenceBased;

import java.util.Random;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.BN;
import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.Utils.Utils;

abstract public class IndependenceTest {
	public final static short NULL = -1;
	public final static short I = 0;
	public final static short D = 1;

	protected boolean failed = false;
	protected FastDataset data;
	protected UGraphSimple trueModel;
	protected BN directedTrueModel;
	
	protected int n;

	final protected double log_threshold; //if pvalue smaller than threshold it is dependent
	protected double threshold; //if pvalue smaller than threshold it is dependent
	//static private boolean threshold_initialized = false;
	protected double log_pvalue; //if pvalue smaller than threshold it is dependent
	
	//--------------------------------------------------------------
	public IndependenceTest(FastDataset data, UGraphSimple trueModel, BN directedTrueModel, double log_threshold){
		this.data = data;
		this.trueModel = trueModel;
		this.directedTrueModel = directedTrueModel;
		this.log_threshold = log_threshold;
		this.threshold = Math.exp(log_threshold);
		
		if(data != null) this.n = data.schema().size();
		else if(trueModel != null) this.n = trueModel.size(); 
		else if(directedTrueModel != null) this.n = directedTrueModel.size(); 
	}
	//--------------------------------------------------------------
	public IndependenceTest(FastDataset data, double log_threshold, int n, double tau, Random r){
		MarkovNet trueNet = new MarkovNet();
		trueNet.generateRandomStructure_cardinality(((int)tau)*n/2, r, n, 0);

		this.data = data;
		this.trueModel = new UGraphSimple(trueNet);
		this.log_threshold = log_threshold;
		this.threshold = Math.exp(log_threshold);
		
		if(data != null) this.n = data.schema().size();
		else if(trueModel != null) this.n = trueModel.n;
	}
	
	// --------------------------------------------------------------
	public pFact independent(Triplet triplet) {
		int n = triplet.FastZ.capacity;
		return independent(new TripletSets(new FastSet(n, triplet.X), new FastSet(n, triplet.Y), triplet.FastZ )); // return log of p_value!!!!
	}
	//	--------------------------------------------------------------
	public abstract pFact independent(TripletSets triplet);
		//--------------------------------------------------------------
	double computeLogPValue(TripletSets triplet) throws Exception{
		throw new Exception("Shouldn't be used in this class.");
	};

	//--------------------------------------------------------------
	//public abstract pFact independent(Triplet triplet);
	
	
	//--------------------------------------------------------------
	final public boolean independentInTrueModel(Triplet triplet) 
	{
		Utils.FORCED_ASSERT(trueModel != null,"Can't query the true model, trueModel=null.");
		return trueModel.separates_nonRecursive(triplet.X(), triplet.Y(), triplet.getCond());
	}
	//--------------------------------------------------------------
	final public boolean independentInTrueModel(TripletSets triplet) 
	{
		Utils.FORCED_ASSERT(trueModel != null,"Can't query the true model, trueModel=null.");
		Utils.FORCED_ASSERT(triplet.X().cardinality() == 1 && triplet.Y().cardinality() == 1,"Invalid triplet");
		return trueModel.separates_nonRecursive(triplet.X().first(), triplet.Y().first(), triplet.getCond());
	}
	//--------------------------------------------------------------
	abstract public boolean independentInData(Triplet triplet);
	// --------------------------------------------------------------
	abstract public boolean hasTestCorrection();
	//--------------------------------------------------------------
	public boolean independentCorrected(Triplet triplet){
		Utils.FORCED_ASSERT(false, "Shouldn't call this method in this class.");
		return false;
	}
	//--------------------------------------------------------------
	final public UGraphSimple trueModel(){return trueModel;};
	//--------------------------------------------------------------
	final public BN directedTrueModel(){return directedTrueModel;};
	//--------------------------------------------------------------
	final public int numVars(){return n;};
	//--------------------------------------------------------------
	final public double log_threshold(){return log_threshold;};
//	--------------------------------------------------------------
	final public double threshold(){return threshold;};
	
	final public void setThreshold(double threshold){this.threshold = threshold;};
	
	// --------------------------------------------------------------
	final public boolean hasData(){return data != null;};
	// --------------------------------------------------------------
	final public boolean hasTrueModel(){return trueModel != null;};

	public void printProof(short printType, pFact f){
		;
	}
}
