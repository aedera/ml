package machineLearning.independenceBased;

import machineLearning.Utils.*;

/**
 * Class Fact
 * 
 * @author Facundo Bromberg
 */
public class TripletSets extends TripletAbstract{
	final public FastSet X,Y;  //Left, Right, Third (for weak transitivity and chordality)
	//static FastSet X_dummy, Y_dummy;
	//final private String string;
	/**
	 * Used for recovering the proof path.
	 */
	//Note, cond is refering to an externally created object. Hopefully nobody frees or changes it.
	
	public TripletSets(int n){
		super(new FastSet(n));
		X = new FastSet(n);
		Y = new FastSet(n);

		//string = "("+ this.X + "," + this.Y + "|" + this.FastZ + ")";
	};
	public TripletSets(Triplet f){
		super(f.FastZ);
		X = new FastSet(f.FastZ.capacity, f.X());
		Y = new FastSet(f.FastZ.capacity, f.Y());
		//this.FastZ = new FastSet(Z);
		
		//X_dummy = new FastSet(X.capacity);
		//Y_dummy = new FastSet(X.capacity);
		
		//string = "("+ this.X + "," + this.Y + "|" + this.FastZ + ")";
	}
	public TripletSets(TripletSets f){
		super(f.FastZ);
		this.X = new FastSet(f.X);
		this.Y = new FastSet(f.Y);
		//this.FastZ = new FastSet(Z);
		
		//X_dummy = new FastSet(X.capacity);
		//Y_dummy = new FastSet(X.capacity);
		
		//string = "("+ this.X + "," + this.Y + "|" + this.FastZ + ")";
	}
	public TripletSets(FastSet  X, FastSet Y, FastSet Z){
		super(Z);
		this.X = new FastSet(X);
		this.Y = new FastSet(Y);
		//this.FastZ = new FastSet(Z);
		
		//X_dummy = new FastSet(X.capacity);
		//Y_dummy = new FastSet(X.capacity);
		
		//string = "("+ this.X + "," + this.Y + "|" + this.FastZ + ")";
	}
	public TripletSets(int  X, int Y, FastSet Z){
		super(Z);
		this.X = new FastSet(Z.capacity, X);
		this.Y = new FastSet(Z.capacity, Y);
		//this.FastZ = new FastSet(Z);
		
		//X_dummy = new FastSet(Z.capacity);
		//Y_dummy = new FastSet(Z.capacity);
		
		//string = "("+ this.X + "," + this.Y + "|" + this.FastZ + ")";
	}
	
	final public TripletSets symmetric(){
		return new TripletSets(Y, X, FastZ);
	}

	public int numVars(){
		return X.cardinality() + Y.cardinality() + FastZ.cardinality();
	}

	public final boolean isEqual(TripletAbstract trip){
		TripletSets tripS = (TripletSets) trip;
		return FastSet.isEqual(X, tripS.X)  &&  FastSet.isEqual(Y, tripS.Y)  &&  FastSet.isEqual(FastZ, tripS.FastZ);
	}
	
	final public boolean isEqual(int x, int y, int ... Z){
		boolean ret = X.isMember(x) && Y.isMember(y) && X.cardinality() == 1 && Y.cardinality() == 1 && FastZ.cardinality() == Z.length;
		if(!ret) return false;
		
		for(int z : Z){
			if(!FastZ.isMember(z)) return false;
		}
		return true;
	}

	public final FastSet X(){return X;};
	public final FastSet Y(){return Y;};
	
	final public String toString(){
		//return string;
		return "("+ this.X + "," + this.Y + "|" + this.FastZ + ")";
	}
	
//	--------------------------------------------
	@Override
	final public int hashCode() {
		//no borrar este hashCode(), es útil para el cacheo de tests
		return getKey().hashCode();
	}
}
