package machineLearning.independenceBased;

import java.util.Random;

import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.Utils;

final public class VertexSeparation extends IndependenceTest {
	// --------------------------------------------------------------
	public VertexSeparation(UGraphSimple trueModel) {
		super(null, trueModel, null, 0.5);
	}

	// --------------------------------------------------------------
	public VertexSeparation(int n, double tau, Random r){
		super(null, 0.5, n, tau, r);
	}
	// --------------------------------------------------------------
	public pFact independent(Triplet triplet) {
		boolean ret = trueModel.separates_nonRecursive(triplet.X, triplet.Y, triplet.FastZ);
		return new pFact(new TripletSets(triplet), ret, ret?0.0:Double.MIN_VALUE);
	}
	// --------------------------------------------------------------
	public pFact independent(TripletSets triplet) {
		Utils.FORCED_ASSERT(triplet.X.cardinality() == 1  &&  triplet.Y.cardinality() == 1, "invalid triplet in VertexSeparation.independent");

		log_pvalue = computeLogPValue(triplet); // return log of p_value!!!!

		return new pFact(triplet, log_pvalue > log_threshold, log_pvalue, false);
	}
	//--------------------------------------------------------------
	protected double computeLogPValue(TripletSets triplet){
		boolean ret = trueModel.separates_nonRecursive(triplet.X.first(), triplet.Y.first(), triplet.FastZ);
		return ret? Math.log(1.0) : Double.MIN_VALUE;
	}
	//--------------------------------------------------------------
	public boolean independentInData(Triplet triplet) 
	{
		Utils.FORCED_ASSERT(false, "Shouldn't run this method in this class.");
		return false;
	}
// --------------------------------------------------------------
	final public boolean hasTestCorrection(){return false;};
	// --------------------------------------------------------------
	public static void main(String[] args) {
	}

}
