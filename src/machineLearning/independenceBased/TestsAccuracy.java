package machineLearning.independenceBased;

import java.util.Random;

import machineLearning.Utils.Experiment;
import machineLearning.Utils.Experiment2;
import machineLearning.Utils.FastSet;
import machineLearning.Utils.Utils;
import machineLearning.Utils.coVL;

/*
 * Created on Sep 20, 2006
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author bromberg
 *
 */
public class TestsAccuracy {
    //static final short ROC_TPR = 0;   //true positive rate = #(true positives)/#(positives)
    //static final short ROC_FPR = 1;     //false positive rate = #(false positives)/#(negatives)
	static final short ROC_TP = 0;
	static final short ROC_TN = 1;
    static final short ROC_POS = 2;       //class ratio = #(negatives)/#(positives)
    static final short ROC_NEG = 3;       //total number of counts
    //static String ROC_OUTPUT;

   // private Random r;
    public IndependenceTest test, oracle;
    
    double ROC [][] = new double [4][];
    public Triplet [][] kbs;
    public pFact [][] testResults, oracleResults;
    int size[];
    int n;
	double TI =0, TD =0, N =0, NUM_IND = 0, NUM_DEP = 0;
	int minCondSetWrong = -1;
	double avgCondSetWrong = 0.0;
    
    //------------------------------------------------------------------------------
	 //Tests over numTests random tests
    public TestsAccuracy(IndependenceTest test, IndependenceTest oracle, int maxCondSize, int numTests, Random r){
    	if(Utils._ASSERT){
    		if(!Utils.FORCED_ASSERT(test.n == oracle.n, "")){
    			int asfd=0;
    		}
    	}
        this.test = test;
        this.oracle = oracle;
        this.n = test.n;
        size = new int[n-2];

        kbs = sampleRandomTests(n, maxCondSize, numTests, r);
        
        testResults = new pFact[kbs.length][];
        oracleResults = new pFact[kbs.length][];
        for(int i=0; i<kbs.length; i++){
        	testResults[i] = new pFact[kbs[i].length];
        	oracleResults[i] = new pFact[kbs[i].length];
        }
        
        computeROC();
    }
    //------------------------------------------------------------------------------
    //Tests over kbs tests
    public TestsAccuracy(IndependenceTest test, IndependenceTest oracle, Triplet [][] kbs){
    	if(Utils._ASSERT){
    		if(!Utils.FORCED_ASSERT(test.n == oracle.n, "")){
    			int asfd=0;
    		}
    	}
        this.test = test;
        this.oracle = oracle;
        this.n = test.n;
        size = new int[n-2];

        this.kbs = kbs;
        
        testResults = new pFact[kbs.length][];
        oracleResults = new pFact[kbs.length][];
        for(int i=0; i<kbs.length; i++){
        	testResults[i] = new pFact[kbs[i].length];
        	oracleResults[i] = new pFact[kbs[i].length];
        }
        
        computeROC();
    }
    //------------------------------------------------------------------------------
    public TestsAccuracy(IndependenceTest test, IndependenceTest oracle, TestsAccuracy acc){
    	if(Utils._ASSERT){
    		if(!Utils.FORCED_ASSERT(test.n == oracle.n, "")){
    			int asfd=0;
    		}
    	}

    	this.test = test;
        this.oracle = oracle;
        this.n = test.n;
        size = new int[n-2];
        
        kbs = acc.kbs;

        testResults = new pFact[kbs.length][];
        for(int i=0; i<kbs.length; i++) testResults[i] = new pFact[kbs[i].length];

        oracleResults = new pFact[kbs.length][];
        for(int i=0; i<kbs.length; i++)  oracleResults[i] = new pFact[kbs[i].length];

        
        //If the tests are the same, use results from acc,
        if(true){
	        if(test == acc.test) {
	        	//testResults = acc.testResults;
	        	for(int i=0; i<kbs.length; i++)  {
	        		for(int j=0; j<kbs[i].length;j++){
	        			if(acc.testResults[i][j] != null) testResults[i][j] = new pFact(acc.testResults[i][j]);
	        		}
	        	}
	        }
	        
	        //If the oracles are the same, use results from acc,
	        if(oracle == acc.oracle) {
	        	for(int i=0; i<kbs.length; i++)  {
	        		for(int j=0; j<kbs[i].length;j++){
	        			if(acc.oracleResults[i][j] != null) oracleResults[i][j] = new pFact(acc.oracleResults[i][j]);
	        		}
	        	}
	        }
        }        
        computeROC();
    }
    //------------------------------------------------------------------------------
    /**
     * Used to grow dynamically with addFact
     */
    public TestsAccuracy(IndependenceTest test, IndependenceTest oracle){
        this.test = test;
        this.oracle = oracle;
        this.n = test.n;
        size = new int[n-1];

        kbs = new Triplet[n-1][10];
        
        testResults = new pFact[kbs.length][];
        oracleResults = new pFact[kbs.length][];
        for(int i=0; i<kbs.length; i++){
        	testResults[i] = new pFact[kbs[i].length];
        	oracleResults[i] = new pFact[kbs[i].length];
        }
    }

    //------------------------------------------------------------------------------
    private boolean alreadyAdded(TripletSets triplet){
        int maxK=0;
        for(int k = 0; k<kbs.length; k++) {
            if(kbs[k] == null) break;
            maxK++; 
        }
            
        for(int k = 0; k<maxK; k++){
            int i=0;
            while(i < kbs[k].length && kbs[k][i] != null ){
            	TripletSets kbsT = new TripletSets(kbs[k][i]);
            	if(kbsT.equals(triplet)) return true;
            	i++;
            }
        }
    	return false;
    }
      //------------------------------------------------------------------------------
    public void addTest(Triplet f){
    	//if(alreadyAdded(new TripletSets(f))) return;
    	if(f == null) return;
    	int card = f.getCond().cardinality();
    	if(card > n-2) return;
    	
    	kbs[card][size[card]] = f;
    	          
    	//next will overflow
    	if(size[card]+1 == kbs[card].length){
    		Triplet aux []= new Triplet[kbs[card].length * 2];
    		for(int i=0; i<kbs[card].length; i++){
    			aux[i] = kbs[card][i]; 
    		}
    		kbs[card] = null;
    		kbs[card] = aux;
    	}
    	size[card]++;
    }
    //------------------------------------------------------------------------------
    public boolean addFact(pFact testResult){
    	return addFact(testResult, null);
    }
    public boolean addFact(pFact testResult, pFact oracleResult){
    	if(alreadyAdded(testResult.triplet)) return false;
    	
    	int card = testResult.getCond().cardinality();
    	
    	if(card >= testResults.length || size[card] >= testResults[card].length){
    		int sadfdsa=0;
    	}
    	testResults[card][size[card]] = testResult;
    	if(oracleResult == null) oracleResults[card][size[card]] = oracle.independent(testResult.triplet);
    	else oracleResults[card][size[card]] = oracleResult;

    	//next will overflow
    	if(size[card]+1 == kbs[card].length){
    		pFact aux []= new pFact[kbs[card].length * 2];
    		for(int i=0; i<kbs[card].length; i++){
    			aux[i] = testResults[card][i]; 
    		}
    		testResults[card] = null;
    		testResults[card] = aux;
    		
    		aux = new pFact[kbs[card].length * 2];
    		for(int i=0; i<kbs[card].length; i++){
    			aux[i] = oracleResults[card][i]; 
    		}
    		oracleResults[card] = null;
    		oracleResults[card] = aux;
    	}

    	
    	addTest(new Triplet(testResult.triplet.X().first(), testResult.triplet.Y().first(), testResult.triplet.getCond()));
    	return true;
    }
    /**
     * It performs ALL statistical tests from data that can be done for conditioning sets of cardinality lower (or equal) than maxCardCondSet. 
     * <!-- --> Each of them is compared with the test done on the output network (this.G). <!-- --> If origNet is 
     * provided, it compares the two networks (G and orig) on the same tests. It returns the ratio of
     * comparisons that match.
     * @return
     */

    
    //------------------------------------------------------------------------------
    //Computes accuracy of cardinalities UP TO (inclusive) input parameter card
    final public double accuracyExactCard(int card){
    	N = ROC[ROC_POS][card]+ROC[ROC_NEG][card];
        //NUM_IND += ROC[ROC_POS][card];
        //NUM_DEP += ROC[ROC_NEG][card];
        TI =  ROC[ROC_TP][card];
        TD =  ROC[ROC_TN][card];
        
        return (TI+TD)/N;
    }
//  ------------------------------------------------------------------------------
    final public double accuracySmallerEqCard(int card){
    	recompute(card);
    	return (TI+TD)/N;
    }
 //  ------------------------------------------------------------------------------
    public double accuracy(){
    	if(ROC[0] == null) computeROC();
    	recompute(ROC[0].length-1);
    	return (TI+TD)/N;
    }
   
    final public int minCondSetWrong(){
        return minCondSetWrong;
    }
    final public double avgCondSetWrong(){
        return avgCondSetWrong;
    }
//  ------------------------------------------------------------------------------
   
    //------------------------------------------------------------------------------
    final public double TIR(){return TIR(ROC[0].length-1);};
    final public double TIR(int card){
    	recompute(card);
    	return NUM_IND==0? 0.0 : TI/NUM_IND;
    }
    //------------------------------------------------------------------------------
    final public double TDR(){return TDR(ROC[0].length-1);};
    final public double TDR(int card){
    	recompute(card);
    	return NUM_DEP==0? 0.0 : TD/NUM_DEP;
    }
    //------------------------------------------------------------------------------
    final public double FIR(){return FIR(ROC[0].length-1);};
    final public double FIR(int card){
    	recompute(card);
    	return NUM_IND==0? 0.0 : (NUM_IND-TI)/NUM_IND;
    }
    //------------------------------------------------------------------------------
    final public double FDR(){return FDR(ROC[0].length-1);};
    final public double FDR(int card){
    	recompute(card);
    	return NUM_DEP==0? 0.0 : (NUM_DEP-TD)/NUM_DEP;
    }
//  ------------------------------------------------------------------------------
//  ------------------------------------------------------------------------------
//  ------------------------------------------------------------------------------
//  ------------------------------------------------------------------------------
    final public double N(){return N(ROC[0].length-1);};
    final public double N(int card){
    	recompute(card);
    	return N;
    }
//  ------------------------------------------------------------------------------
    final public double TI(){return TI(ROC[0].length-1);};
    final public double TI(int card){
    	recompute(card);
    	return TI;
    }
    //------------------------------------------------------------------------------
    final public double TD(){return TD(ROC[0].length-1);};
    final public double TD(int card){
    	recompute(card);
    	return TD;
    }
    //------------------------------------------------------------------------------
    final public double FI(){return FI(ROC[0].length-1);};
    final public double FI(int card){
    	recompute(card);
    	return (NUM_IND-TI);
    }
    //------------------------------------------------------------------------------
    final public double FD(){return FD(ROC[0].length-1);};
    final public double FD(int card){
    	recompute(card);
    	return (NUM_DEP-TD);
    }
    
    
    //  ------------------------------------------------------------------------------
    final private void recompute(int card){
    	N=0;
    	NUM_IND=0;
    	NUM_DEP=0;
    	TI=0;
    	TD=0;
    	 for(int k =0; k<=card; k++){
             N += ROC[ROC_POS][k]+ROC[ROC_NEG][k];
             NUM_IND += ROC[ROC_POS][k];
             NUM_DEP += ROC[ROC_NEG][k];
             TI +=  ROC[ROC_TP][k];
             TD +=  ROC[ROC_TN][k];
         }
    	 if(Utils._ASSERT){
	    	 Utils.FORCED_ASSERT(N == NUM_IND + NUM_DEP, "");
	    	 Utils.FORCED_ASSERT(N == TI + (NUM_IND-TI) + TD + (NUM_DEP-TD), "");
    	 }
    }
    //------------------------------------------------------------------------------
    private boolean  computeROC(){
        boolean a=true,b=true;
        
        int TOT_POS=0, TOT_NEG=0;
        int TOT_TP=0, TOT_FN=0;
        
        int maxK=0;
        for(int k = 0; k<kbs.length; k++) {
            if(kbs[k] == null) break;
            maxK++; 
        }
            
        for(int i=0; i<4; i++) ROC[i] = new double[maxK];
    
        //numOfTests = 0;
        
        boolean print = Experiment2.output==null?false:Experiment2.output.doPrint(coVL.ACCURACY_EVAL);
        boolean print_reportDifferent = Experiment2.output==null?false:Experiment2.output.doPrint(coVL.ACCURACY_DIFF);
        
        //print = true;
        //print_reportDifferent = true;
        
        minCondSetWrong = -1; //do not change initialization!
    	avgCondSetWrong = 0.0;
        
        if(print) System.out.println( "\n\nACCURACY ");
        if(print) System.out.println("-------");
        //grows a KB with tests to be performed
        for(int k = 0; k<maxK; k++){
        	if(print) System.out.println("\n Cardinality = " + k);
            
            double FN = 0, POS = 0, NEG = 0, TP = 0;
            int i=0;
            while(i < kbs[k].length && kbs[k][i] != null ){
            	Triplet triplet = kbs[k][i];
            	if(triplet.isEqual(4, 5, 3)){
            		int asda=0;
            	}
                //X = kbs[k][i].X().first();
                //Y = kbs[k][i].Y().first();
                //cond = kbs[k][i].getCond();
                
            	if(testResults[k][i] == null) {//do not erase this.
            		testResults[k][i] = test.independent(kbs[k][i]);
            	}
            	if(oracleResults[k][i] == null) {
            		oracleResults[k][i] = oracle.independent(kbs[k][i]);
            	}
                
            	a = testResults[k][i].truth_value();
                b = oracleResults[k][i].truth_value();
                
                if(print) System.out.print("\n" + kbs[k][i] + "  test " + a + "  oracle " + b + "  ");
                if(true && print_reportDifferent && a!= b) {
                	if(print) System.out.print("\n" + kbs[k][i] + "  test " + a + "  oracle " + b + "  ");
                	if(false && oracleResults[k][i].isEqual(0, 3)){
						oracle.printProof((short)2, oracleResults[k][i]);
						oracle.printProof((short)2, oracleResults[k][i].negation());
                	}
                }
                if(a){
                    POS++;
                    TOT_POS++;
                    if(b)  {
                    	TP++;  //TI
                    	TOT_TP++;
                    	if(print) System.out.print("TI " + TOT_TP);
                    }
                    else {
                    	//System.out.println("Test " + triplet + " in true = " + oracleResults[k][i] + " in net = " + testResults[k][i]);
                    	//System.exit(1);
                    	if(print) System.out.print("FI " + (TOT_POS-TOT_TP));
                    	if(minCondSetWrong == -1) minCondSetWrong = k;
                    	avgCondSetWrong += k;
                    }
                }
                else{
                    NEG++;
                    TOT_NEG++;
                    if(b) {
                    	FN++; //FN
                    	TOT_FN++;
                    	if(print) System.out.print("FD " + TOT_FN);
                    	//System.out.println("Test " + triplet + " in true = " + oracleResults[k][i] + " in net = " + testResults[k][i]);
                    	//System.exit(1);

                    	if(minCondSetWrong == -1) minCondSetWrong = k;
                    	avgCondSetWrong += k;
                    }
                    else {
                    	if(print) System.out.print("TD " + (TOT_NEG-TOT_FN));
                    }
                }
                if(print)  System.out.print("  Accuracy = " + "(" + TOT_TP + "+" + (TOT_NEG-TOT_FN) + ")/" + (TOT_POS+TOT_NEG) + "= " + ((double)(TOT_TP+TOT_NEG-TOT_FN)/(double)(TOT_POS+TOT_NEG)));
                i++;

            }
            ROC[ROC_TP][k] = TP ;
            ROC[ROC_TN][k] = (NEG-FN);
            ROC[ROC_POS][k] = POS;
            ROC[ROC_NEG][k] = NEG;

            avgCondSetWrong /= (((double)FN) + ((double)(TOT_POS-TP)));
            if(print) System.out.println("\nCardinality   " + k + " Accuracy ="  + ((double)(TP+NEG-FN)/(double)(POS+NEG)));

            //TOT_POS += NUM_IND;
            //TOT_TP += TI;
        }
        return true;//accuracy/numOfTests;
    }
    //------------------------------------------------------------------------------
    public double dependencyRatioForEdgeInFirstTest(int X, int Y){
    	double numDep = 0, tot = 0;
        boolean a=true;
        
        int maxK=0;
        for(int k = 0; k<kbs.length; k++) {
            if(kbs[k] == null) break;
            maxK++; 
        }
            
        
        boolean print = Experiment.output==null?false:Experiment.output.doPrint(coVL.ACCURACY_EVAL);
        
        //grows a KB with tests to be performed
        for(int k = 0; k<maxK; k++){
            int i=0;
            while(i < kbs[k].length && kbs[k][i] != null ){
            	i++;
            	Triplet triplet = kbs[k][i-1];
            	if(triplet.X != X && triplet.X != Y) continue;
            	if(triplet.Y != X && triplet.Y != Y) continue;
                
            	tot++;
            	if(testResults[k][i-1] == null) {
            		testResults[k][i-1] = test.independent(kbs[k][i-1]);
            	}
                
            	a = testResults[k][i-1].truth_value();
                
            	if(!a) numDep++; 
            }
        }
        return tot==0?0.0:numDep/tot;
    }
    //------------------------------------------------------------------------------
    static public Triplet [][] sampleRandomTests(int n, int maxCondSize, int numTests, Random r){
        FastSet cond=null;
        FastSet Vxy;
        int x,y;
        Triplet triplets [][] = new Triplet[maxCondSize+1][];
        
        for(int k=maxCondSize; k>=0; k--){
        	if(k > n-2){
        		continue;
        	}
            
            triplets[k] = new Triplet[numTests/(maxCondSize+1)];
            int count=0;
            while(count < numTests/(maxCondSize+1) ){
                Vxy = null;
                Vxy = new FastSet(n);
                Vxy.setAlltoOne();
    
                //Select X
                x = r.nextInt(n);
                Vxy.remove(x);
    
                //Select Y
                y = x;
                while(y == x){
                    y = r.nextInt(n);
                }
                Vxy.remove(y);
                
                //Select condSet
                FastSet VxyCopy = new FastSet(Vxy);
                cond = null;
                cond = new FastSet(n);
                for(int j=0; j<k; j++){
                    int condVar = r.nextInt(VxyCopy.cardinality());
                    cond.add(VxyCopy.getOn(condVar));
                    VxyCopy.remove(VxyCopy.getOn(condVar));
                }
                triplets[k][count] = new Triplet(x, y, cond);
                
                count++;
            }
        }
        return triplets;
    }

    static public Triplet [][] sampleRandomTests(int n, int maxCondSize, int numTests, Random r, int x, int y){
    	FastSet cond=null;
    	FastSet Vxy;
    	Triplet triplets [][] = new Triplet[maxCondSize+1][];
    	
    	for(int k=maxCondSize; k>=0; k--){
    		if(k > n-2){
    			continue;
    		}
    		
    		triplets[k] = new Triplet[numTests/(maxCondSize+1)];
    		int count=0;
    		while(count < numTests/(maxCondSize+1) ){
    			Vxy = null;
    			Vxy = new FastSet(n);
    			Vxy.setAlltoOne();
    			
    			Vxy.remove(x);
    			
    			Vxy.remove(y);
    			
    			//Select condSet
    			FastSet VxyCopy = new FastSet(Vxy);
    			cond = null;
    			cond = new FastSet(n);
    			for(int j=0; j<k; j++){
    				int condVar = r.nextInt(VxyCopy.cardinality());
    				cond.add(VxyCopy.getOn(condVar));
    				VxyCopy.remove(VxyCopy.getOn(condVar));
    			}
    			if(Utils._ASSERT) Utils.ASSERT(cond.cardinality() == k, "GSIMN.Accuracy: invalid cond size.");
    			
    			triplets[k][count] = new Triplet(x, y, cond);
    			
    			count++;
    		}
    	}
    	return triplets;
    }
    
    /**
     * Given the true network as input, for each node X add a test for each neighbor Y given all other neighbors N_X except of Y, i.e.
     * X,Y | {N_X - Y}
     */
    /*public FactSimpleGSIMN [][] sampleNeighborsTests(MarkovNet trueNet){
        FastSet cond=null;
        Set V = new Set(trueNet.V());
        Set N_X; //Neighbors of X
        GraphNode X=null, Y=null;
        //KBsimpleGSIMN  kb = new KBsimpleGSIMN(V.size());
        FactSimpleGSIMN facts [][] = new FactSimpleGSIMN[1][];
        
        int N = V.size();

		//Experiment.output.println(pfmnVL.SAMPLE_NEIGHBORS_TESTS| pfmnVL.HIGHER, trueNet.toString());
        Experiment.output.println(pfmnVL.SAMPLE_NEIGHBORS_TESTS | pfmnVL.HIGHER, trueNet.toString());

        int numTests = 0;
        for(int x=0; x<N; x++){
        	numTests +=  ((GraphRVar)V.elementAt(x)).degree();
        }
    	facts[0] = new FactSimpleGSIMN[numTests];

    	int count=0;
        for(int x=0; x<N; x++){
        	X = null;
        	X = (GraphRVar) V.elementAt(x);

        	N_X = null;
        	N_X = new Set(X.adjacencyList());

        	for(int y=0; y<N_X.size(); y++){
        		Y = null;
        		Y = (GraphRVar) N_X.elementAt(y);

        		//Select condSet
        		Set N_XMinusY = new Set(N_X);
        		cond = null;
        		cond = new FastSet(V.size());
        		for(int j=0; j<N_XMinusY.size(); j++){
        			if(j == y) continue;
        			cond.add(((GraphRVar)N_XMinusY.elementAt(j)).index());
        		}
        		facts[0][count] = new FactSimpleGSIMN(true, X.index(), Y.index(), cond, V);
        		//System.out.println(fact);
        		Experiment.output.println(pfmnVL.SAMPLE_NEIGHBORS_TESTS | pfmnVL.HIGHER, facts[0][count].toString());
        		count++;
        	}
        }
        return facts;
    }
*/



    public static void main(String[] args) {
	}
}
