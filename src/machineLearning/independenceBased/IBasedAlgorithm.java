package machineLearning.independenceBased;

import machineLearning.Graphs.GraphsSimple.DirectedGraph;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;

public interface IBasedAlgorithm {
	void run();
	public UGraphSimple getOutputNetwork();
}
