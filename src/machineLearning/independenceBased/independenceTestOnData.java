package machineLearning.independenceBased;

import java.util.HashMap;

import machineLearning.Datasets.FastDataset;
import machineLearning.Datasets.FastInstance;
import machineLearning.Graphs.BN;
import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.Utils.Set;
import machineLearning.Utils.Utils;
import machineLearning.Datasets.*;

abstract public class independenceTestOnData extends IndependenceTest {
	static public  int numTestsDone = 0;
	
//	//--- BORRAR ESTA VARIABLE!!
//	static public long readTableBTMillis = 0;
	
	protected Long nvals_A, nvals_B, nvals_C;
	protected double df;

	protected double reliability;  
	
	// --------------------------------------------------------------
	public independenceTestOnData(FastDataset data, UGraphSimple trueModel, BN directedTrueModel, double log_threshold) {
		super(data, trueModel, directedTrueModel, log_threshold);
	}
	
	//--------------------------------------------------------------
	public pFact independent(TripletSets triplet) {
		numTestsDone++;

		log_pvalue = computeLogPValue(triplet); // return log of p_value!!!!

		// if(pvalue != null) pvalue[0] = val;
		//return new pFact(triplet, log_pvalue > log_threshold, log_pvalue, -totalCellsFailed, failed);
		return new pFact(triplet, log_pvalue > log_threshold, log_pvalue, failed);
	}
	//--------------------------------------------------------------
	public void independentInPlace(pFact pf) {
		numTestsDone++;

		log_pvalue = computeLogPValue(pf.triplet); // return log of p_value!!!!

		pf.logpvalue(log_pvalue);
	}
	//--------------------------------------------------------------
	abstract double computeLogPValue(TripletSets triplet);

	//--------------------------------------------------------------
	final public FastDataset data(){return data;};
	//--------------------------------------------------------------
	public boolean independentInData(Triplet triplet)
	{
		return independent(triplet).truth_value;
	}
//	--------------------------------------------------------------
	public boolean independent(pFact pf)
	{
		Utils.FORCED_ASSERT(pf.logpvalue != -1, "p-value has not been computed yet!");
		return log_pvalue > log_threshold;
	}
	// --------------------------------------------------------------
	public HashMap<SliceForHashTable, double[][]>  readTables(TripletSets triplet) {
		
//		long inicio = System.nanoTime();
		
		double [][] contTable;
		
		FastSet X = triplet.X;
		FastSet Y = triplet.Y;
		FastSet Z = triplet.FastZ;
		
		int val_A, val_B;
		//double chsq = 0;

		
		// for (conf = 0; conf < nconfs; conf++) {
		int numConf = 0;
		//String key;
		
		HashMap<SliceForHashTable, double[][]> contTables = new HashMap<SliceForHashTable, double[][]> (data.size());

		FastInstance ex = data.getFirstInstance();
		int k = 0;
		while (ex != null) {
			//String key = getHashCode(ex.project(Z));
			SliceForHashTable key = new SliceForHashTable(ex.project(Z));
			

			if (!contTables.containsKey(key)) {
				contTable = new double[nvals_A.intValue()][nvals_B.intValue()];
				contTables.put(key, contTable);
				numConf++;
				
			} else {
				contTable = (double[][]) contTables.get(key);
			}
			k++;

			val_A = ex.getValue(X);
			val_B = ex.getValue(Y);

			contTable[val_A][val_B]++;

			ex = data.getNextInstance();
		}
		//totalCellsFailed = numCellsFailed(contTables);

//		readTableBTMillis += System.nanoTime() - inicio;
		
		return contTables;
	}
//	 ------------------------------------------------------------
	//abstract double numCellsFailed(HashMap<String, double[][]> contTables);
	// ------------------------------------------------------------
	static private String getHashCode(int[] a) {
		String aux = "empty";
		if (a.length > 0)
			aux = "" + a[0];
		for (int i = 1; i < a.length; i++)
			aux += "," + a[i];
		return aux;

	}
	// ------------------------------------------------------------
	public static void main(String[] args) {
		String dataFileName = "sampledData_n6_t2_E100_M500.csv";
		FastDataset data = new FastDataset(dataFileName);
		MarkovNet xMn = new MarkovNet();
	    xMn.read(data, dataFileName, 5, 200);
	    UGraphSimple trueNet =  new UGraphSimple(new UGraphSimple(xMn));
	    System.out.println(trueNet);
	    
	    int X = 0;
	    int Y = 1;
	    FastSet FastZ = new FastSet(6, 3, 2);
	    chiSquare cs = new chiSquare(data, 0.05);
		pFact pf = cs.independent(new Triplet(X, Y, FastZ));
		System.out.println(pf.toStringPValue());
		
		Set Z = new Set();
		double logpvalue[] = new double[1];
		
		int k = FastZ.first();
		while(k >= 0) {
			Z.add(data.schema().getAttribute(k));
			k = FastZ.next();
		}
		boolean out = Statistics.independent(data, X, Y, Z, 0.05, false, Statistics.CHISQUARE, null, logpvalue);
		System.out.println((out?"I":"!I") + (Triplet.toString(X, Y, FastZ)) + " = " + logpvalue[0]);

	}

}
