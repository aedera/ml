package machineLearning.independenceBased;

//import java.util.*;
import java.util.Collection;
import java.util.HashMap;

import machineLearning.Utils.*;
import machineLearning.Datasets.FastDataset;
import machineLearning.Datasets.IndependenceTestMargaritisUAI2001;
import machineLearning.Graphs.*;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;

//import weka.core.*;

/**
 * Created on Feb 01, 2006
 * 
 * @author Facundo Bromberg
 * 
 *         Computes the probability of independence given data, i.e. Pr(M_I |
 *         D),
 * 
 *         SEE NOTEBOOK DIMITRIS 6, pp33-35
 */
public class BayesianTest extends independenceTestOnData {

	public static final short PER_CELL = 1;
	public static final short PER_SLICE = 2;
	public static final short TOTAL_UNIFORM = 3;
	short gammaType = PER_CELL;
	static final double defaultPrior = 0.5;
	final double prior;
	public boolean useTestsCache = false;
	public HashMap<TripletSets, double[]> tripletsCache;

	static int MINIMUM_COUNTS = 0; // if total counts in a contingency table (in
	// unconditional) is lower than this number,
	// it returns the prior

	static double totalGamma2 = 1.00;
	static double b = 0.01, a = 1.0;
	static double err_threshold = 1E-10;

	// FIXME: Borrar constantes!!
	public static long contTablesBT = 0;
	public static long restMillis = 0;

	// --------------------------------------------------------------
	public BayesianTest(FastDataset data) {
		super(data, null, null, Math.log(defaultPrior));
		this.prior = defaultPrior;
		// this.logprior = Math.log(prior);
	}

	// --------------------------------------------------------------
	public BayesianTest(FastDataset data, UGraphSimple trueModel) {
		super(data, trueModel, null, Math.log(defaultPrior));
		this.prior = defaultPrior;
		// this.logprior = Math.log(prior);
	}

	// --------------------------------------------------------------
	public BayesianTest(FastDataset data, BN trueModel) {
		super(data, null, trueModel, Math.log(defaultPrior));
		this.prior = defaultPrior;
		// this.logprior = Math.log(prior);
	}

	// --------------------------------------------------------------
	public BayesianTest(FastDataset data, double prior) {
		super(data, null, null, Math.log(prior));
		this.prior = prior;
		// this.logprior = Math.log(prior);
	}

	// --------------------------------------------------------------
	public BayesianTest(FastDataset data, UGraphSimple trueModel, double prior) {
		super(data, trueModel, null, Math.log(0.5));
		this.prior = prior;
		// this.logprior = Math.log(prior);
	}

	// --------------------------------------------------------------
	public BayesianTest(FastDataset data, BN trueModel, double prior) {
		super(data, null, trueModel, Math.log(prior));
		this.prior = prior;
		// this.logprior = Math.log(prior);
	}

	public BayesianTest(FastDataset data, boolean useTestsCache) {
		super(data, null, null, Math.log(defaultPrior));
		this.prior = defaultPrior;
		this.useTestsCache = true;
		if (useTestsCache)
			tripletsCache = new HashMap<TripletSets, double[]>();
	}

	public BayesianTest(FastDataset data, boolean useTestsCache, int n) {
		super(data, null, null, Math.log(defaultPrior));
		this.n = n;
		this.prior = defaultPrior;
		this.useTestsCache = useTestsCache;
		if (useTestsCache)
			tripletsCache = new HashMap<TripletSets, double[]>();
	}

	// --------------------------------------------------------------
	public void setTestType(short type) {
		gammaType = type;
	};

	// --------------------------------------------------------------
	final protected double computeLogPValue(TripletSets triplet) {
		double[] log_dl = log_likelihoods(triplet);

		double log_P = -Math.log1p(Math.exp(log_dl[D] - log_dl[I]
				+ Math.log(1 - prior) - Math.log(prior)));
		// double P = Math.exp(log_P);

		return log_P;
	}

	// --------------------------------------------------------------------------------
	public boolean independent(double[] log_dl) {
		return independent(log_dl, 0.5);
	}

	// --------------------------------------------------------------
	public boolean independent(double[] log_dl, double threshold) {
		return log_dl[I] > Math.log(threshold / (1 - threshold)) + log_dl[D];
	}

	// --------------------------------------------------------------
	public double logProbIndependent(double[] log_dl) {
		double aux = -Math.log1p(Math.exp(log_dl[D] - log_dl[I]));

		boolean IND1 = independent(log_dl, 0.3);
		boolean IND2 = (aux > Math.log(0.3));
		if (IND1 != IND2)
			throw new RuntimeException("Error en logProbIndependent()");
		return aux;
	}

	public double logProbDependent(double[] log_dl) {
		double aux = -Math.log1p(Math.exp(log_dl[I] - log_dl[D]));

		boolean IND1 = independent(log_dl, 0.5);
		boolean IND2 = (Math.exp(aux) <= 0.5);
		if (IND1 != IND2)
			throw new RuntimeException("Error en logProbDependent()");
		return aux;
	}

	// --------------------------------------------------------------
	final public boolean hasTestCorrection() {
		return false;
	};

	// --------------------------------------------------------------
	public boolean independentCorrected(Triplet triplet) {
		if (!hasTestCorrection())
			Utils
					.FORCED_ASSERT(false,
							"Can't query the error correction engine, hasCorrection = false.");
		return false;
	}

	// --------------------------------------------------------------------------------
	// Return the log-data-likelihoods of the conditionals
	public double[] log_likelihoods(TripletSets triplet) {
		return log_likelihoods(triplet, 0.5);
	}

	public static int cached=0,  nocached=0;
	public double[] log_likelihoods(TripletSets triplet, double threshold) {
		double[] cachedLikelihoods = tripletsCache != null ? tripletsCache
				.get(triplet) : null;
		if (useTestsCache && cachedLikelihoods != null) {
			cached++;
			return cachedLikelihoods;
		} else {
			nocached++;

			// long inicio = System.nanoTime();

			FastSet X = triplet.X;
			FastSet Y = triplet.Y;
			FastSet Z = triplet.FastZ;

			// attributesSet condSet = new attributesSet(cond);

			nvals_C = 1l;
			int k = Z.first();
			int i = 0;
			while (k >= 0 && k < n) {
				// System.out.println(i + ": multiplico " +nvals_C+
				// "*"+data.schema().getAttribute(k).size()+"="+nvals_C*data.schema().getAttribute(k).size());
				nvals_C *= data.schema().getAttribute(k).size();
				k = Z.next();
				i++;
			}
			double log_p, log_q, w = Math.pow(prior, 1.0 / nvals_C);
			double log_w = 1.0 / nvals_C * Math.log(prior);
			double log1p_w = Math.log1p(-w);
			// double aux3 = Math.log(1-w);
			double log_ai, log_bi; // ai = pi * wi, bi = qi*(1-wi)

			double[] log_dl = new double[2];

			// Attribute A = data.schema().getAttribute(A_i);
			// Attribute B = data.schema().getAttribute(B_i);
			if (!FastSet.Intersection(X, Y).isEmpty()) {
				log_dl[I] = log_dl[D] = 1.0;
				return log_dl; /* Shortcut. */
			}
			// count number of values in of X's attributes
			nvals_A = 1l;
			k = X.first();
			while (k >= 0) {
				nvals_A *= data.schema().getAttribute(k).size();
				k = X.next();
			}

			// count number of values in of Y's attributes
			nvals_B = 1l;
			k = Y.first();
			while (k >= 0) {
				nvals_B *= data.schema().getAttribute(k).size();
				k = Y.next();
			}

			// count number of values in of FastZ's attributes

			/* Sanity checks. */
			if (nvals_A <= 1 || nvals_B <= 1) {
				log_dl[I] = 1.0;
				log_dl[D] = 0.0;
				return log_dl;
			}

			// restMillis += System.nanoTime() - inicio;

			// long A = System.nanoTime() - inicio;
			// inicio = System.nanoTime();
			HashMap<SliceForHashTable, double[][]> contTables = readTables(triplet);
			// long B = System.nanoTime() - inicio;

			// inicio = System.nanoTime();
			// contTablesBT = contTablesBT + contTables.size();

			double[] log_udl; // unconditional data likelihoods;

			log_dl[I] = log_dl[D] = 0.0;

			double log_A = 0, log_BB = 0;
			double aux;
			Collection<double[][]> contTablesA = contTables.values();
			for (double[][] currTable : contTablesA) {
				log_udl = unconditionalLogDataLikelihoods(currTable, prior);
				log_p = log_udl[I];
				log_q = log_udl[D];

				log_ai = log_p + log_w;
				log_bi = log_q + log1p_w;

				// Log likelihood of independence model
				// log(Pr(D | M_CI)) = sum_i log(p_i)
				log_dl[I] += log_p;

				// Log likelihood of dependence model
				if (log_p >= log_q) {
					// aux2 = log_bi - log_ai;
					// double aux3 = Math.exp(log_bi-log_ai);
					aux = Math.log1p(Math.exp(log_bi - log_ai));
					log_A += log_ai + aux;

					log_BB += -aux;
				} else {
					// aux2 = log_ai - log_bi;
					// double aux3 = Math.exp(log_ai - log_bi);
					aux = Math.log1p(Math.exp(log_ai - log_bi));

					log_A += log_bi + aux;

					// double aux5 = log_ai - log_bi - aux;
					log_BB += log_ai - log_bi - aux;

				}
			}

			// double aux3 = Math.exp(log_BB);
			// dsouble aux2 = Math.log1p(-Math.exp(log_BB));
			double aux4 = Math.log(-Math.expm1(log_BB));
			log_dl[D] = log_A - Math.log(threshold) + aux4; // TODO: VER PORQUÉ
			// USANDO, EN VEZ DE
			// 0.5, UN VALOR
			// ALTO
			// (Double.MAX_VALUE)
			// mejora mucho el
			// resultado de
			// GSMNS

			// double PP = Math.exp(log_BB);
			// double log_P = -Math.log1p(Math.exp(log_dl[1] - log_dl[0] +
			// Math.log(1-prior) + Math.log(prior)));
			// double P = Math.exp(log_P);
			// double log_PP = log_BB;
			// System.out.println("log(P) (" + A.index() + "," + B.index() +
			// ") = " + log_BB);

			// restMillis += System.nanoTime() - inicio;

			// long C = System.nanoTime() - inicio;
			// System.out.println("A: " + A/1000 + "  C: " + C/1000 + "  B: " +
			// B/1000);

			cache(triplet, log_dl);
			return log_dl; // if ratio of likelihoods (dep/ind) > 1, we got
			// dependence
		}
	}

	// --------------------------------------------------------------------------------
	// Returns an array with the log of data likelihoods of the unconditional
	// test. Index 0 for independence, 1 for dependence.
	final private double[] unconditionalLogDataLikelihoods(double[][] matrix,
			double prior) {
		double[] dataLikelihoods = new double[2];

		int nrows, ncols, row, col;
		// double N = 0;

		nrows = matrix.length;
		ncols = matrix[0].length;

		// these are the cI+, and c+J respectively
		double[] ciPlus = new double[nrows]; // was rtotal before
		double[] cPlusj = new double[ncols]; // was ctotal before

		double[] c = new double[nrows * ncols]; // flatten matrix

		double gammai = 1.0;
		if (gammaType == PER_CELL)
			gammai = 1.0;
		else if (gammaType == PER_SLICE)
			gammai = 1.0 / ((double) (nrows * ncols));
		else if (gammaType == TOTAL_UNIFORM)
			gammai = (1.0 / (double) ((nrows * ncols))) / ((double) nvals_C);

		/*
		 * double alphai = 1.0; double betai = 1.0;
		 */

		double alphaSum = ncols * gammai, betaSum = nrows * gammai; // the
		// marginals
		// of the
		// gamma

		double N = 0;
		for (row = 0; row < nrows; row++) {
			for (col = 0; col < ncols; col++) {
				ciPlus[row] += matrix[row][col];
				cPlusj[col] += matrix[row][col];
				c[row * ncols + col] = matrix[row][col];
				N += matrix[row][col];
			}
		}

		if (N < MINIMUM_COUNTS) {
			dataLikelihoods[0] = dataLikelihoods[1] = 1.0;
			return dataLikelihoods;
		}

		totalGamma2 = 0;
		for (int i = 0; i < c.length; i++)
			totalGamma2 += gammai;

		dataLikelihoods[I] = logYfunction(ciPlus, alphaSum)
				+ logYfunction(cPlusj, betaSum);
		// dataLikelihoods[I] = logYfunction(ciPlus, alphai) +
		// logYfunction(cPlusj, betai);
		dataLikelihoods[D] = logYfunction(c, gammai);

		return dataLikelihoods;
	}

	// --------------------------------------------------------------------------------
	final private double logYfunction(double c[], double gammai) {
		// totalGamma is supposed to be summer over all gammai, which are the
		// same and thus we collapsed into a single variable gammai

		double N = 0;
		totalGamma2 = 0;
		for (int i = 0; i < c.length; i++) {
			N += c[i];
			totalGamma2 += gammai;
		}

		double ret = gammln(totalGamma2) - gammln(totalGamma2 + N);

		for (int i = 0; i < c.length; i++) {
			ret += gammln(gammai + c[i]);
			ret -= gammln(gammai);
		}

		return ret;
	}

	// --------------------------------------------------------------------------------
	final private double gammln(double xx) {
		double x, y, tmp, ser;
		double cof[] = { 76.18009172947146, -86.50532032941677,
				24.01409824083091, -1.231739572450155, 0.1208650973866179e-2,
				-0.5395239384953e-5 };
		int j;

		y = x = xx;
		tmp = x + 5.5;
		tmp -= (x + 0.5) * Math.log(tmp);
		ser = 1.000000000190015;
		for (j = 0; j <= 5; j++)
			ser += cof[j] / ++y;
		return -tmp + Math.log(2.5066282746310005 * ser / x);
	}

	// ------------------------------------------------------------
	protected double numCellsFailed(HashMap<String, double[][]> contTables) {
		return 0;
	}

	/**
	 * @param log_dl
	 * @return
	 */
	public double[] logProbs(double[] log_dl) {
		double[] probs = new double[2];
		double aux = -Math.log1p(Math.exp(log_dl[I] - log_dl[D]));

		probs[D] = aux;

		aux = -Math.log1p(Math.exp(log_dl[D] - log_dl[I]));

		probs[I] = aux;

		return probs;
	}

	public double[] probs(double[] log_dl) {
		double[] probs = new double[2];
		double aux = -Math.log1p(Math.exp(log_dl[I] - log_dl[D]));

		probs[D] = Math.exp(aux);

		aux = -Math.log1p(Math.exp(log_dl[D] - log_dl[I]));

		probs[I] = Math.exp(aux);

		return probs;
	}

	public static void main(String[] args) {
		String dataFileName = "sampledData_n6_t2_E100_M500.csv";
		FastDataset data = new FastDataset(dataFileName);
		MarkovNet xMn = new MarkovNet();
		xMn.read(data, dataFileName, 5, 2000);
		UGraphSimple trueNet = new UGraphSimple(new UGraphSimple(xMn));
		System.out.println(trueNet);

		int X = 3;
		int Y = 1;
		FastSet FastZ = new FastSet(6, 4, 2);
		BayesianTest bt = new BayesianTest(data);
		pFact pf = bt.independent(new Triplet(X, Y, FastZ));
		System.out.println(pf.toStringPValue());

		Set Z = new Set();
		double logpvalue[] = new double[1];

		int k = FastZ.first();
		while (k >= 0) {
			Z.add(data.schema().getAttribute(k));
			k = FastZ.next();
		}

		boolean out = IndependenceTestMargaritisUAI2001.conditional(data, X, Y,
				Z, 0.5, logpvalue);
		System.out.println((out ? "I" : "!I") + (Triplet.toString(X, Y, FastZ))
				+ " = " + logpvalue[0] + ", " + Math.exp(logpvalue[0]));

	}

	public double[] getFromCache(TripletSets triplet) {
		if (useTestsCache) {
			return tripletsCache.get(triplet);
		} else {
			return null;
		}
	}

	public void cache(TripletSets triplet, double[] scores) {
		if (useTestsCache) {
			tripletsCache.put(triplet, scores);
		}
	}
}
