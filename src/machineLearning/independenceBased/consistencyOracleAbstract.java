package machineLearning.independenceBased;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import machineLearning.Datasets.Statistics;
import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.BN;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.Utils.Output;
import machineLearning.Utils.Utils;
import machineLearning.independenceBased.Triplet;
import machineLearning.independenceBased.TripletSets;
import machineLearning.independenceBased.IndependenceTest;
import machineLearning.independenceBased.independenceTestOnData;
import machineLearning.independenceBased.pFact;

abstract public class consistencyOracleAbstract extends IndependenceTest{
	public long profilingTime=0;


	static public Output output;

	protected independenceTestOnData test;

	// -----------------------------------------------------------------
	public consistencyOracleAbstract(independenceTestOnData test, int initialCapacity) {
		super(test.data(),  null, null, test.log_threshold);
		
		this.test = test;
	}
	// -----------------------------------------------------------------
	public consistencyOracleAbstract(independenceTestOnData test, int initialCapacity, UGraphSimple trueModel) {
		super(test.data(),  trueModel, null, test.log_threshold);
		
		this.test = test;
	}
	// -----------------------------------------------------------------
	public consistencyOracleAbstract(independenceTestOnData test, int initialCapacity, BN trueModel) {
		super(test.data(),  null, trueModel, test.log_threshold);
		
		this.test = test;
	}
	//--------------------------------------------------------------
	public boolean hasTestCorrection(){return true;};
	//--------------------------------------------------------------
	public boolean independentInData(Triplet triplet){return test.independent(triplet).truth_value;};
	//--------------------------------------------------------------
	public boolean independentCorrected(Triplet triplet) 
	{
		int n = triplet.getCond().capacity;
		return independent(new TripletSets(new FastSet(n, triplet.X()), new FastSet(n, triplet.Y()), triplet.getCond())).truth_value;
	}
	//--------------------------------------------------------------
	abstract public void mainLoop();

	

}
