package machineLearning.independenceBased;

import java.util.Random;

import machineLearning.Graphs.BN;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.Utils;

final public class dSeparation extends IndependenceTest {
	// --------------------------------------------------------------
	public dSeparation(BN trueModel) {
		super(null, null, trueModel, 0.5);
	}

	// --------------------------------------------------------------
	public pFact independent(Triplet triplet) {
		boolean [] Z = new boolean[triplet.FastZ.capacity];
		for(int i=0; i<Z.length; i++) Z[i] = triplet.FastZ.isMember(i);
		
		boolean ret = directedTrueModel.areIndep(triplet.X, triplet.Y, Z);
		return new pFact(new TripletSets(triplet), ret, ret?0.0:Double.MIN_VALUE);
	}
	// --------------------------------------------------------------
	public pFact independent(TripletSets triplet) {
		Utils.FORCED_ASSERT(triplet.X.cardinality() == 1  &&  triplet.Y.cardinality() == 1, "invalid triplet in dSeparation.independent");
		return independent(new Triplet(triplet.X.first(), triplet.Y.first(), triplet.FastZ));
		
		/*log_pvalue = computeLogPValue(triplet); // return log of p_value!!!!

		return new pFact(triplet, log_pvalue > log_threshold, log_pvalue, false);*/
	}
	//--------------------------------------------------------------
	protected double computeLogPValue(TripletSets triplet){
		Utils.FORCED_ASSERT(false, "WARNING.");
		
		//boolean ret = this.directedTrueModel.areIndep(triplet.X.first(), triplet.Y.first(), triplet.FastZ.toBooleanArray());
		boolean ret = trueModel.separates_nonRecursive(triplet.X.first(), triplet.Y.first(), triplet.FastZ);
		return ret? Math.log(1.0) : Double.MIN_VALUE;
	}
	//--------------------------------------------------------------
	public boolean independentInData(Triplet triplet) 
	{
		Utils.FORCED_ASSERT(false, "Shouldn't run this method in this class.");
		return false;
	}
// --------------------------------------------------------------
	final public boolean hasTestCorrection(){return false;};
	// --------------------------------------------------------------
	public static void main(String[] args) {
	}

}
