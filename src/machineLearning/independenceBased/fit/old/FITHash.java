package machineLearning.independenceBased.fit.old;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.BN;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.BayesianTest;
import machineLearning.independenceBased.SliceForHashTable;
import machineLearning.independenceBased.TripletSets;

/**
 * 
 * @author seba
 * 
 */
public class FITHash extends BayesianTest {
//	public boolean useHash = true;
//	public int K = 10;
//
//	public long inferredTables = 0;
//	public long notInferredTables = 0;
//
//	// Estructura que almacena las Cache para cada par de variables X,Y.
//	public Map<BitSet, CTCache_XY> xyPairsCache = new HashMap<BitSet, CTCache_XY>();
//	public CTCache_XY[][] xyPairsCache2;
//	
//	public boolean saveIntermediateTables = false;

	public FITHash(FastDataset data, BN trueModel, double prior) {
		super(data, trueModel, prior);
	}

	public FITHash(FastDataset data, BN trueModel) {
		super(data, trueModel);
	}

	public FITHash(FastDataset data, double prior) {
		super(data, prior);
	}

	public FITHash(FastDataset data, UGraphSimple trueModel, double prior) {
		super(data, trueModel, prior);
	}

	public FITHash(FastDataset data, UGraphSimple trueModel) {
		super(data, trueModel);
	}

//	public FITHash(FastDataset data) {
//		super(data);
//		/* una cache para cada para x y, y para cada cardinalidad posible para cada par*/
//		xyPairsCache = new HashMap<BitSet, CTCache_XY>();
//		xyPairsCache2 = new CTCache_XY[n*(n-1)/2][n];
//	}

//	/**
//	 * Calcula las contTables para el triplete (X,Y|Z) a partir de un
//	 * supercondicionante de Z. Si no existen las contTables en la Cache para el
//	 * supercondicionente, o la Cache para el par de variables X,Y tampoco
//	 * existe, calcula las tablas a partir del DataSet.
//	 * 
//	 * @param triplet
//	 *            - Triplete para el que se van a calcular las contTables.
//	 * @return Las tablas de Contingencia para cada configuracion (slice) del
//	 *         triplete (X,Y|Z).
//	 */
//	public HashMap<SliceForHashTable, double[][]> readTables(TripletSets triplet) {
//
//		HashMap<SliceForHashTable, double[][]> contTables = null;
//		
//		BitSet keyBitSetXY = new BitSet(triplet.X.bitset.length());
//		keyBitSetXY.or(triplet.X.bitset);
//		keyBitSetXY.or(triplet.Y.bitset);
//		
//		CTCache_XY cache_XY = null;
//		if(useHash){
//			cache_XY = xyPairsCache.get(keyBitSetXY);
//		}else{
//			cache_XY = xyPairsCache2[triplet.X.first()][triplet.Y.first()];
//		}
//		
//		ConditionalContTables superConditionalCT = null;
//
//		if (cache_XY != null) {
//
//			superConditionalCT = getCTSuperConditional(cache_XY, triplet.FastZ);
//
//			if (superConditionalCT != null) {
//				if (superConditionalCT.getConditionalZ().equals(triplet.FastZ)) {
//
//					inferredTables = inferredTables + superConditionalCT.getContTables().size();
//					return superConditionalCT.getContTables();
//
//				} else {
//
//					while ((superConditionalCT.getConditionalZ().cardinality() - triplet.FastZ.cardinality()) > 1 && saveIntermediateTables) {
//						superConditionalCT = calcMiddleConditionals(cache_XY, superConditionalCT, triplet);
//					}
//
//					contTables = calcContTables(superConditionalCT, triplet.FastZ);
//					inferredTables = inferredTables + contTables.size();
//				}
//			} else {
//				superConditionalCT = createContTablesSC(cache_XY, triplet);
//
//				while ((superConditionalCT.getConditionalZ().cardinality() - triplet.FastZ.cardinality()) > 1  && saveIntermediateTables) {
//					superConditionalCT = calcMiddleConditionals(cache_XY, superConditionalCT, triplet);
//				}
//
//				contTables = calcContTables(superConditionalCT, triplet.FastZ);
//
//				notInferredTables = notInferredTables + contTables.size();
//			}
//
//		} else {
//			cache_XY = new CTCache_XY();
//			if(useHash){
//				xyPairsCache.put(keyBitSetXY, cache_XY);
//			}else{
//				xyPairsCache2[triplet.X.first()][triplet.Y.first()] = cache_XY;
//			}
//			 
//			superConditionalCT = createContTablesSC(cache_XY, triplet);
//
//			while ((superConditionalCT.getConditionalZ().cardinality() - triplet.FastZ.cardinality()) > 1  && saveIntermediateTables) {
//				superConditionalCT = calcMiddleConditionals(cache_XY, superConditionalCT, triplet);
//			}
//
//			contTables = calcContTables(superConditionalCT, triplet.FastZ);
//
//			notInferredTables = notInferredTables + contTables.size();
//		}
//
//		saveCache(cache_XY, contTables, triplet.FastZ);
//
//		return contTables;
//	}
//
//	/**
//	 * 
//	 * @param cache_XY
//	 * @param superConditionalCT
//	 * @param tripletSC
//	 * @return
//	 */
//	private ConditionalContTables calcMiddleConditionals(CTCache_XY cache_XY, ConditionalContTables superConditionalCT, TripletSets tripletSC) {
//		HashMap<SliceForHashTable, double[][]> contTables = null;
//
//		FastSet fastZ = tripletSC.FastZ;
//
//		FastSet fastMC = new FastSet(superConditionalCT.getConditionalZ());
//
//		int[] a = new int[fastMC.cardinality()];
//
//		int i = 0;
//		int index = fastMC.first();
//		while (index != -1) {
//			a[i] = index;
//			i++;
//			index = fastMC.next();
//		}
//
//		Random random = new Random();
//		index = random.nextInt(fastMC.cardinality());
//		while (fastZ.isMember(a[index])) {
//			index = random.nextInt(fastMC.cardinality());
//		}
//		fastMC.flip(a[index]);
//
//		contTables = calcContTables(superConditionalCT, fastMC);
//
//		saveCache(cache_XY, contTables, fastMC);
//
//		return new ConditionalContTables(fastMC, contTables);
//	}
//
//	/**
//	 * Genera las contTables para un supercondicionante de cardinalidad =
//	 * MAX_CARDINALITY, a partir de Z, y las cachea.
//	 * 
//	 * @param cache_XY
//	 * @param tripletZ
//	 * @return
//	 */
//	private ConditionalContTables createContTablesSC(CTCache_XY cache_XY, TripletSets tripletZ) {
//
//		HashMap<SliceForHashTable, double[][]> contTables = null;
//
//		FastSet fastZ = tripletZ.FastZ;
//		int x = tripletZ.X.first();
//		int y = tripletZ.Y.first();
//
//		int flips = K - fastZ.cardinality();
//
//		FastSet fastSC = new FastSet(tripletZ.FastZ);
//
//		Random random = new Random();
//		for (int i = 0; i < flips; i++) {
//			int index = random.nextInt(fastZ.capacity);
//			while (index == x || index == y || fastSC.isMember(index)) {
//				index = random.nextInt(fastZ.capacity);
//			}
//			fastSC.flip(index);
//		}
//
//		TripletSets tripletSC = new TripletSets(x, y, fastSC);
//		contTables = super.readTables(tripletSC);
//		saveCache(cache_XY, contTables, tripletSC.FastZ);
//
//		return new ConditionalContTables(tripletSC.FastZ, contTables);
//	}
//
//	/**
//	 * Devuelve el supercondicionante de Z que se encuentre a la menor distancia
//	 * posible (distancia = |supercondicionante| - |condicionante|) junto a sus
//	 * contTables.
//	 * 
//	 * @param cache_XY
//	 *            - Cache para el par de variables (X,Y).
//	 * @param fastZ
//	 *            - Condicionante Z para el triplete.
//	 * @return El supercondicioante de Z con sus contTables o null si no
//	 *         encuentra un supercondicionante para Z.
//	 */
//	private ConditionalContTables getCTSuperConditional(CTCache_XY cache_XY, FastSet fastZ) {
//
//		for (int i = 0; i <= K; i++) {
//
//			int index = fastZ.first();
//			String key = (fastZ.cardinality() + i) + "" + index;
//
//			ListPointersContTables listPointersCT = cache_XY.get(key);
//
//			if (listPointersCT != null && listPointersCT.containsKey(fastZ)) {
//
//				ConditionalContTables conditionalContTables = new ConditionalContTables(fastZ, listPointersCT.get(fastZ));
//				return conditionalContTables;
//
//			} else if (listPointersCT != null) {
//
//				// Busca la lista que menos apuntadores contenga.
//				while (index != -1) {
//					index = fastZ.next();
//					key = (fastZ.cardinality() + i) + "" + index;
//
//					ListPointersContTables auxListPointers = cache_XY.get(key);
//
//					if (auxListPointers != null && auxListPointers.size() < listPointersCT.size()) {
//						listPointersCT = auxListPointers;
//					}
//
//				}
//
//				Set<FastSet> keyConditionalPointers = listPointersCT.keySet();
//
//				for (FastSet fastSC : keyConditionalPointers) {
//					BitSet auxBitSet = new BitSet(fastZ.bitset.length());
//					auxBitSet = (BitSet) fastZ.bitset.clone();
//					auxBitSet.and(fastSC.bitset);
//
//					if (auxBitSet.equals(fastZ.bitset)) {
//						ConditionalContTables conditionalContTables = new ConditionalContTables(fastSC, listPointersCT.get(fastSC));
//						return conditionalContTables;
//					}
//				}
//
//			}
//		}
//
//		return null;
//	}
//
//	/**
//	 * Calcula las tablas de contigencia del condicionante Z a partir de las
//	 * contTables del supercondionante de Z.
//	 * 
//	 * @param superConditionalCT
//	 *            - Tablas de continegencia del supercondicionante de Z.
//	 * @param fastZ
//	 *            - El condicionante Z del triplete para el que estamos
//	 *            calculando las tablas.
//	 * @return Las tablas de Contingencia para cada configuracion (slice) del
//	 *         triplete (X,Y|Z).
//	 */
//	private HashMap<SliceForHashTable, double[][]> calcContTables(ConditionalContTables superConditionalCT, FastSet fastZ) {
//
//		HashMap<SliceForHashTable, double[][]> mapContTablesZ = new HashMap<SliceForHashTable, double[][]>();
//
//		FastSet fastSC = superConditionalCT.getConditionalZ();
//		HashMap<SliceForHashTable, double[][]> mapContTablesSC = superConditionalCT.getContTables();
//
//		Set<SliceForHashTable> slicesSC = mapContTablesSC.keySet();
//
//		for (SliceForHashTable sliceSC : slicesSC) {
//
//			double[][] contTableSC = mapContTablesSC.get(sliceSC);
//			SliceForHashTable sliceZ = getSliceZ(sliceSC, fastSC, fastZ);
//
//			double[][] contTableZ = mapContTablesZ.get(sliceZ);
//
//			if (contTableZ != null) {
//				addTables(contTableSC, contTableZ);
//			} else {
//				contTableZ = new double[contTableSC.length][contTableSC[0].length];
//				addTables(contTableSC, contTableZ);
//				mapContTablesZ.put(sliceZ, contTableZ);
//			}
//		}
//
//		return mapContTablesZ;
//	}
//
//	/**
//	 * Suma las celdas de la tabla contTableSC a las celdas de la tabla
//	 * contTableZ.
//	 * 
//	 * @param contTableSC
//	 *            - Tabla de contingencia del Supercondionante.
//	 * @param contTableZ
//	 *            - Tabla de contingencia del condicionante Z.
//	 */
//	private void addTables(double[][] contTableSC, double[][] contTableZ) {
//		for (int i = 0; i < contTableZ.length; i++) {
//			for (int j = 0; j < contTableZ[0].length; j++) {
//				contTableZ[i][j] = contTableZ[i][j] + contTableSC[i][j];
//			}
//		}
//	}
//
//	/**
//	 * Guarda en la Cache las tablas de contingencia calculadas y la informacion
//	 * necesaria para recuperarlas cuando sea necesario.
//	 * 
//	 * @param cache_XY
//	 *            - Cache para el par de variables (X,Y) de triplete.
//	 * @param mapContTables
//	 *            - Tablas de contingecia para el triplete (X,Y|Z).
//	 * @param fastZ
//	 *            - Condicinante Z del triplete.
//	 */
//	private void saveCache(CTCache_XY cache_XY, HashMap<SliceForHashTable, double[][]> mapContTables, FastSet fastZ) {
//
//		int cardinality = fastZ.cardinality();
//		int index = fastZ.first();
//
//		while (index != -1) {
//			String key = cardinality + "" + index;
//			ListPointersContTables listPointerCT = null;
//
//			if (cache_XY.containsKey(key)) {
//				listPointerCT = cache_XY.get(key);
//			} else {
//				listPointerCT = new ListPointersContTables();
//				cache_XY.put(key, listPointerCT);
//			}
//
//			listPointerCT.put(fastZ, mapContTables);
//
//			index = fastZ.next();
//		}
//
//	}
//
//	/**
//	 * Genera una configuracion para el condicionante Z a partir de una
//	 * configuracion del supercondicionante y el propio condicionante Z.
//	 * 
//	 * @param sliceSC
//	 *            - Una configuracion del supercondicionante de Z.
//	 * @param fastSC
//	 *            - El supercondicionante de Z.
//	 * @param fastZ
//	 *            - El condicionante Z.
//	 * @return Una configuracion del condicionante Z.
//	 */
//	private SliceForHashTable getSliceZ(SliceForHashTable sliceSC, FastSet fastSC, FastSet fastZ) {
//
//		int[] a = new int[fastZ.cardinality()];
//
//		int indexZ = fastZ.first();
//		for (int i = 0; i < fastZ.cardinality(); i++) {
//
//			int indexSC = fastSC.first();
//			for (int j = 0; j < fastSC.cardinality(); j++) {
//
//				if (indexZ == indexSC) {
//					a[i] = sliceSC.a[j];
//					break;
//				}
//
//				indexSC = fastSC.next();
//			}
//
//			indexZ = fastZ.next();
//		}
//
//		SliceForHashTable sliceZ = new SliceForHashTable(a);
//
//		return sliceZ;
//	}

}
