package machineLearning.independenceBased.fit;

import java.util.HashMap;
import java.util.Map;

/**
 * <b>Parametros para el HashMap</b>
 * </p>
 * String (key): Compuesta por la vardinalidad del condicionante Z (para el que estamos calculando  
 * las contTables) y por una de las variables de condicionante. Esto representa los dos niveles  
 * de direccionamiento de la estructura de datos propuesta.
 * </p>
 * PointersContTables (values): representa la lista de apuntadores a las contTables para una
 * cardinalidad y una variable especifica.
 * </p>
 * 
 * @author seba
 *  
 */
@SuppressWarnings("serial")
public class CTCache_XY extends HashMap<String, ListPointersContTables> {

	public CTCache_XY() {
		super();
	}

	public CTCache_XY(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
	}

	public CTCache_XY(int initialCapacity) {
		super(initialCapacity);
	}

	public CTCache_XY(Map<? extends String, ? extends ListPointersContTables> m) {
		super(m);
	}

	
}
