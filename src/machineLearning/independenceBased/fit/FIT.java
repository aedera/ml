package machineLearning.independenceBased.fit;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.BN;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.BayesianTest;
import machineLearning.independenceBased.SliceForHashTable;
import machineLearning.independenceBased.TripletSets;

/**
 * 
 * @author seba
 * 
 */
public class FIT extends BayesianTest {

	public int K = 10;

	public long inferredTables = 0;
	public long notInferredTables = 0;
	public long numTables = 0;

	// Estructura que almacena las Cache para cada par de variables X,Y.
	public Map<BitSet, CTCacheXY> xyPairsCache = new HashMap<BitSet, CTCacheXY>();

	public boolean saveIntermediateTables = false;

	public FIT(FastDataset data, BN trueModel, double prior) {
		super(data, trueModel, prior);
	}

	public FIT(FastDataset data, BN trueModel) {
		super(data, trueModel);
	}

	public FIT(FastDataset data, double prior) {
		super(data, prior);
	}

	public FIT(FastDataset data, UGraphSimple trueModel, double prior) {
		super(data, trueModel, prior);
	}

	public FIT(FastDataset data, UGraphSimple trueModel) {
		super(data, trueModel);
	}

	public FIT(FastDataset data) {
		super(data);
	}

	/**
	 * Calcula las contTables para el triplete (X,Y|Z) a partir de un
	 * supercondicionante de Z. Si no existen las contTables en la Cache para el
	 * supercondicionente, o la Cache para el par de variables X,Y tampoco
	 * existe, calcula las tablas a partir del DataSet.
	 * 
	 * @param triplet
	 *            - Triplete para el que se van a calcular las contTables.
	 * @return Las tablas de Contingencia para cada configuracion (slice) del
	 *         triplete (X,Y|Z).
	 */
	public HashMap<SliceForHashTable, double[][]> readTables(TripletSets triplet) {

		HashMap<SliceForHashTable, double[][]> contTables = null;

		BitSet keyBitSetXY = new BitSet(triplet.X.bitset.length());
		keyBitSetXY.or(triplet.X.bitset);
		keyBitSetXY.or(triplet.Y.bitset);

		CTCacheXY cache_XY = xyPairsCache.get(keyBitSetXY);


		if (cache_XY == null) {
			cache_XY = new CTCacheXY(n);
			xyPairsCache.put(keyBitSetXY, cache_XY);
		}

		ConditionalContTables superConditionalCT = getCTSuperConditional(cache_XY, triplet.FastZ);
		
		boolean inferred = true;
		if (superConditionalCT == null) {
			superConditionalCT = createContTablesSC(cache_XY, triplet);
			inferred = false;
		}

		if(saveIntermediateTables) {
			while ((superConditionalCT.getConditionalZ().cardinality() - triplet.FastZ.cardinality()) > 1) {
				superConditionalCT = calcMiddleConditionals(cache_XY, superConditionalCT, triplet);
			}
		}
		contTables = calcContTables(cache_XY, superConditionalCT, triplet.FastZ);
		
		if (inferred) {
			inferredTables  ++;
		} else{
			notInferredTables++;
		}

		return contTables;
	}

	/**
	 * 
	 * @param cache_XY
	 * @param superConditionalCT
	 * @param tripletSC
	 * @return
	 */
	private ConditionalContTables calcMiddleConditionals(CTCacheXY cache_XY, ConditionalContTables superConditionalCT, TripletSets tripletSC) {
		HashMap<SliceForHashTable, double[][]> contTables = null;

		FastSet fastZ = tripletSC.FastZ;

		FastSet fastMC = new FastSet(superConditionalCT.getConditionalZ());

		int[] a = new int[fastMC.cardinality()];

		int i = 0;
		int index = fastMC.first();
		while (index != -1) {
			a[i] = index;
			i++;
			index = fastMC.next();
		}

		Random random = new Random();
		index = random.nextInt(fastMC.cardinality());
		while (fastZ.isMember(a[index])) {
			index = random.nextInt(fastMC.cardinality());
		}
		fastMC.flip(a[index]);

		contTables = calcContTables(cache_XY, superConditionalCT, fastMC);

		return new ConditionalContTables(fastMC, contTables);
	}

	/**
	 * Genera las contTables para un supercondicionante de cardinalidad =
	 * MAX_CARDINALITY, a partir de Z, y las cachea.
	 * 
	 * @param cache_XY
	 * @param tripletZ
	 * @return
	 */
	private ConditionalContTables createContTablesSC(CTCacheXY cache_XY, TripletSets tripletZ) {

		HashMap<SliceForHashTable, double[][]> contTables = null;

		int x = tripletZ.X.first();
		int y = tripletZ.Y.first();

		int flips = K;

		FastSet fastSC = new FastSet(tripletZ.FastZ);

		Random random = new Random();
		FastSet compfastSC = (new FastSet(fastSC)).complement();
		for (int i = 0; i < flips && compfastSC.cardinality() > 0; i++) {
			int index = compfastSC.getOn(random.nextInt(compfastSC.cardinality()));
			fastSC.add(index);
			compfastSC.remove(index);
		}
		
//		for (int i = 0; i < flips; i++) {
//			int index = random.nextInt(fastZ.capacity);
//			while (index == x || index == y || fastSC.isMember(index)) {
//				index = random.nextInt(fastZ.capacity);
//			}
//			fastSC.flip(index);
//		}

		TripletSets tripletSC = new TripletSets(x, y, fastSC);
		contTables = super.readTables(tripletSC);
		
		saveCache(cache_XY, contTables, tripletSC.FastZ);
		
		numTables += contTables.size();
		return new ConditionalContTables(tripletSC.FastZ, contTables);
	}

	/**
	 * Devuelve el supercondicionante de Z que se encuentre a la menor distancia
	 * posible (distancia = |supercondicionante| - |condicionante|) junto a sus
	 * contTables.
	 * 
	 * @param cache_XY
	 *            - Cache para el par de variables (X,Y).
	 * @param fastZ
	 *            - Condicionante Z para el triplete.
	 * @return El supercondicioante de Z con sus contTables o null si no
	 *         encuentra un supercondicionante para Z.
	 */
	private ConditionalContTables getCTSuperConditional(CTCacheXY cache_XY, FastSet fastZ) {

//		int KK = ((fastZ.cardinality()+K) <= fastZ.capacity)?  fastZ.cardinality()+K : fastZ.capacity;
		
		int cardinality = fastZ.cardinality();
		for (int i = cardinality; i <= fastZ.capacity; i++) {
			if(cache_XY.map[i] == null) continue;
			
			// Busca la lista que menos apuntadores contenga.
			int index = fastZ.first();
			index = (index == -1)? n : index;
			int min_size=Integer.MAX_VALUE, jmin=index;
			
			while(index != -1) {
				if(cache_XY.map[i][index] != null) {
					int size = cache_XY.map[i][index].size() ; 
					if(size < min_size) {
						min_size = size;
						jmin = index;
					}
				}
				index = fastZ.next();
			}
			
			if(cache_XY.map[i][jmin] != null){
				for (ConditionalContTables CCT : cache_XY.map[i][jmin]) {
					if( FastSet.isSubSet(fastZ, CCT.getConditionalZ())) {
						return CCT; 
					}
				}
			}
		}

		return null;
	}

	/**
	 * Calcula las tablas de contigencia del condicionante Z a partir de las
	 * contTables del supercondionante de Z.
	 * 
	 * @param superConditionalCT
	 *            - Tablas de continegencia del supercondicionante de Z.
	 * @param fastZ
	 *            - El condicionante Z del triplete para el que estamos
	 *            calculando las tablas.
	 * @return Las tablas de Contingencia para cada configuracion (slice) del
	 *         triplete (X,Y|Z).
	 */
	private HashMap<SliceForHashTable, double[][]> calcContTables(CTCacheXY cache_XY, ConditionalContTables superConditionalCT, FastSet fastZ) {

		FastSet fastSC = superConditionalCT.getConditionalZ();

		if(fastSC.isEqual(fastZ)) return superConditionalCT.getContTables();
		
			
		HashMap<SliceForHashTable, double[][]> mapContTablesZ = new HashMap<SliceForHashTable, double[][]>();
		HashMap<SliceForHashTable, double[][]> mapContTablesSC = superConditionalCT.getContTables();


		Set<Entry<SliceForHashTable, double[][]>> slicesSC = mapContTablesSC.entrySet();

//		long inicio = System.nanoTime();
		for (Entry<SliceForHashTable, double[][]> sliceSC : slicesSC) {
			double[][] contTableSC = sliceSC.getValue();//mapContTablesSC.get(sliceSC);
			SliceForHashTable sliceZ = getSliceZ(sliceSC.getKey(), fastSC, fastZ);

			double[][] contTableZ = mapContTablesZ.get(sliceZ);

			if (contTableZ != null) {
				addTables(contTableSC, contTableZ);
			} else {
				contTableZ = new double[contTableSC.length][contTableSC[0].length];
				addTables(contTableSC, contTableZ);
				mapContTablesZ.put(sliceZ, contTableZ);
			}
		}

//		System.out.println("\n"  + (System.nanoTime() - inicio)/1000);
		saveCache(cache_XY, mapContTablesZ, fastZ);

		return mapContTablesZ;
	}

	/**
	 * Suma las celdas de la tabla contTableSC a las celdas de la tabla
	 * contTableZ.
	 * 
	 * @param contTableSC
	 *            - Tabla de contingencia del Supercondionante.
	 * @param contTableZ
	 *            - Tabla de contingencia del condicionante Z.
	 */
	private void addTables(double[][] contTableSC, double[][] contTableZ) {
		for (int i = 0; i < contTableZ.length; i++) {
			for (int j = 0; j < contTableZ[0].length; j++) {
				contTableZ[i][j] = contTableZ[i][j] + contTableSC[i][j];
			}
		}
	}

	/**
	 * Guarda en la Cache las tablas de contingencia calculadas y la informacion
	 * necesaria para recuperarlas cuando sea necesario.
	 * 
	 * @param cache_XY
	 *            - Cache para el par de variables (X,Y) de triplete.
	 * @param mapContTables
	 *            - Tablas de contingecia para el triplete (X,Y|Z).
	 * @param fastZ
	 *            - Condicinante Z del triplete.
	 */
	private void saveCache(CTCacheXY cache_XY, HashMap<SliceForHashTable, double[][]> mapContTables, FastSet fastZ) {

		int cardinality = fastZ.cardinality();
		int index = fastZ.first();

		
		while(true){
			if(index==-1){
				int asdfa=2435;
			}
			index = (index == -1)? n : index;

			if(cache_XY.map[cardinality] == null) cache_XY.map[cardinality] = new Vector[n+1];
			if (cache_XY.map[cardinality][index] == null) {
				cache_XY.map[cardinality][index] = new Vector<ConditionalContTables>();
			}
			cache_XY.map[cardinality][index].add(new ConditionalContTables(fastZ, mapContTables));

			if(index == n) break;
			index = fastZ.next();
		}
		
	}

	/**
	 * Genera una configuracion para el condicionante Z a partir de una
	 * configuracion del supercondicionante y el propio condicionante Z.
	 * 
	 * @param sliceSC
	 *            - Una configuracion del supercondicionante de Z.
	 * @param fastSC
	 *            - El supercondicionante de Z.
	 * @param fastZ
	 *            - El condicionante Z.
	 * @return Una configuracion del condicionante Z.
	 */
	private SliceForHashTable getSliceZ(SliceForHashTable sliceSC, FastSet fastSC, FastSet fastZ) {
		int[] a = new int[fastZ.cardinality()];

		int indexSC = fastSC.first();
		int jSC = 0, j=0;
		while(indexSC != -1) {
			if(fastZ.isMember(indexSC)) {
				a[j] = sliceSC.a[jSC];
				j++;
			}
			
			indexSC = fastSC.next();
			jSC++;
		}
		SliceForHashTable sliceZ = new SliceForHashTable(a);
		return sliceZ;
	}
	
	public void resetCache() {
		xyPairsCache.clear();
	}

}
