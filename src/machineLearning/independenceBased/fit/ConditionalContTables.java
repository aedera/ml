package machineLearning.independenceBased.fit;

import java.util.HashMap;

import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.SliceForHashTable;

/**
 * 
 * @author sperez
 *
 */
class ConditionalContTables {
	
	private FastSet conditionalZ;
	private HashMap<SliceForHashTable, double[][]> contTables;
	
	public ConditionalContTables(FastSet conditionalZ, HashMap<SliceForHashTable, double[][]> contTables) {
		super();
		this.conditionalZ = conditionalZ;
		this.contTables = contTables;
	}

	public FastSet getConditionalZ() {
		return conditionalZ;
	}

	public void setConditionalZ(FastSet conditionalZ) {
		this.conditionalZ = conditionalZ;
	}

	public HashMap<SliceForHashTable, double[][]> getContTables() {
		return contTables;
	}

	public void setContTables(HashMap<SliceForHashTable, double[][]> contTables) {
		this.contTables = contTables;
	}
	
}
