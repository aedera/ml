package machineLearning.independenceBased.fit;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;

import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.SliceForHashTable;



/**
 * <b>Parametros para el HashMap</b> </p> String (key): Compuesta por la
 * vardinalidad del condicionante Z (para el que estamos calculando las
 * contTables) y por una de las variables de condicionante. Esto representa los
 * dos niveles de direccionamiento de la estructura de datos propuesta. </p>
 * PointersContTables (values): representa la lista de apuntadores a las
 * contTables para una cardinalidad y una variable especifica. </p>
 * 
 * @author seba
 * 
 */
public class CTCacheXY {
	public Vector<ConditionalContTables> map[][];
//	public double[][] map;
//	static public double numMBytes = 0;
	
	public CTCacheXY(int n) {
		super();
//		numMBytes += 4*(n+1)*(n+1);
		/*
		 * habria un vector para cada par xy, 
		 * de k+1 cardinalidades distintas,
		 * y con indices para cada variable
		 */
		map = new Vector[n+1][];
	}

}
