package machineLearning.independenceBased.fit;

import java.util.HashMap;
import java.util.Map;

import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.SliceForHashTable;


/**
 * Representa una lista de apuntadores a las contTables (HashMap<SliceForHashTable, double[][]>) 
 * de los distintos condicionantes (FastSet).
 * </p>
 * <b>Parametros para el HashMap</b>
 * </p>
 * FastSet (key): Es el condicionante Z.
 * </p>
 * HashMap (values): Representa todas las configuraciones (Slice) no nulas posibles para
 * las variables del condicionante Z. Cada configuracion tiene su tabla de contigencia.
 * </p>
 * 
 * @author seba
 *
 */
public class ListPointersContTables extends HashMap<FastSet, HashMap<SliceForHashTable, double[][]>> {

	public ListPointersContTables() {
		super();
	}

	public ListPointersContTables(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
	}

	public ListPointersContTables(int initialCapacity) {
		super(initialCapacity);
	}

	public ListPointersContTables(Map<? extends FastSet, ? extends HashMap<SliceForHashTable, double[][]>> m) {
		super(m);
	}

}
