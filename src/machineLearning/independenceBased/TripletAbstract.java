package machineLearning.independenceBased;

import machineLearning.Utils.FastSet;

abstract public class TripletAbstract{
	public final FastSet FastZ;
	protected double log_dl[];

	abstract  public boolean isEqual(TripletAbstract trip);
	
	final public FastSet getCond(){return FastZ;};
	/*TripletAbstract(FastSet FastZ, double [] log_dl){
		this.FastZ = FastZ;
		this.log_dl = log_dl;
	}*/
	TripletAbstract(FastSet FastZ){
		this.FastZ = FastZ;
		this.log_dl = null;
	}
	
	abstract public TripletAbstract symmetric();

	abstract public int numVars();
	

	final protected String getKey(){
		return toString();
	}
	public boolean equals(Object o) {
		return toString().equals(o.toString());
	}

}