package machineLearning.independenceBased;

public class SliceForHashTable {
	public int [] a;
	int hashCode;
	
	public SliceForHashTable(int [] a){
		this.a = a;
		hashCode = 0;
		for(int A : a) hashCode += A;
		
	}
	public boolean equals(Object o){
		SliceForHashTable other = (SliceForHashTable) o;
		if(a.length != other.a.length) return false;
		
		for(int i=0; i<a.length; i++) if(a[i] != other.a[i]) return false;
		return true;
	}
	
	public int hashCode(){
		return hashCode;
	}
	
	public String toString() {
		String string = "";
		
		for (int i = 0; i < a.length; i++) {
			string = string + a[i];
		}
		
		return string;
	}
}
