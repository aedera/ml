package machineLearning.independenceBased;

import machineLearning.Graphs.GraphRVar;
import machineLearning.Utils.*;

/**
 * Class Fact
 * 
 * @author Facundo Bromberg
 */
public class FactSimple{
	/**
	 * sets type of fact: independence (I=true) or dependence (I=false)fact.
	 */
	private boolean I;	
	//private GraphRVar L, R;
	private int L,R, TH;  //Left, Right, Third (for weak transitivity and chordality)
	private  FastSet cond;
	private Set condNotFast;
	private int order;
	//private int N;
	/**
	 * Used for recovering the proof path.
	 */
	private FactSimple antecedentOne=null, antecedentTwo=null, antecedentThree=null;
	private String axiom = "DATA";
	static final String separator = "@"; 
	
	//Note, cond is refering to an externally created object. Hopefully nobody frees or changes it.
	public FactSimple(){};
	public FactSimple(int N, boolean Independent, int L, int R, final Set cond){
		FastSet aux = convertToFastSet(cond, N);
		FactSimpleInternal(Independent, L, R, aux, cond);
	}
	public FactSimple(boolean I, Triplet f){
		//FastSet aux = convertToFastSet(f.getCond(), f.getCond().size());
		FactSimpleInternal(I, f.X, f.Y, f.getCond(), f.getCondNotSimple());
	}
	public FactSimple(FactSimple f){
		FactSimpleInternal(f.I(), f.X(), f.Y(), f.getCond(), f.getCondNotFast());
	}
	public FactSimple(boolean Independent, int  L, int R, final FastSet cond){
		FactSimpleInternal(Independent, L, R, cond, null);
	}
	public void FactSimpleInternal(boolean Independent, int  L, int R, final FastSet cond, final Set condNotFast){
		
		//this.N = N;
		I = Independent;
		this.L = L;
		this.R = R;
		
		if(condNotFast != null) this.condNotFast = new Set(condNotFast);
		if(cond != null) this.cond  = cond;
	}
	
	
	public final int  X(){return L;};
	public final int  Y(){return R;};
	public final FastSet getCond(){return cond;};
	public final Set getCondNotFast(){return condNotFast;};

	
	void set(int N, boolean Independent, int  L, int  R, final Set cond){
		FastSet aux = convertToFastSet(cond, N);
		set(Independent, L,R, aux);
	}
	void set(boolean Independent, int  L, int  R, final FastSet cond){
		
		//this.N = N;
		I = Independent;
		this.L = L;
		this.R = R;
		this.cond = cond;
	}
	public boolean I(){return I;};
	FactSimple I(boolean val){
		I = val;
		return this;
	}
	
	public int order(){return order;};
	public void order(int order){this.order = order;};
	
	FactSimple antecedentOne(){return antecedentOne;};
	FactSimple antecedentTwo(){return antecedentTwo;};
	FactSimple antecedentThree(){return antecedentThree;};
	FactSimple antecedents(FactSimple antecedent1, FactSimple antecedent2, FactSimple antecedent3){
		this.antecedentOne = antecedent1;
		this.antecedentTwo = antecedent2;
		this.antecedentThree = antecedent3;
		
		return this;
	}
	String axiom(){return axiom;};
	FactSimple axiom(String axiom){
		this.axiom = axiom;
		return this;
	}
	//Used to sort it. We put the variable with smaller name on the left. And then we sort the cond set similarly.
	//Note that it implements Insertion sort, which runs in linear time if input (i.e. cond) is already sorted.
	static Set sort(Set cond){
		GraphRVar key;
		int key2;
		int i;
		
		//Insertion sort
		for(int j=1; j<cond.size(); j++){
			key = (GraphRVar)cond.elementAt(j);
			i = j-1;
			while(i>=0 && (key2= ((GraphRVar)cond.elementAt(i)).index()) > (key.index())  ){
				cond.setElementAt(cond.elementAt(i), i+1);
				i = i-1;
			}
			//cond.setElementAt(key2, i+1);
			cond.setElementAt(key, i+1);
		}
		//Test if it works
		if(cond.size() > 3){
			int k=0;
		}
		for(int j=1; j<cond.size(); j++){
			GraphRVar key1 = (GraphRVar)cond.elementAt(j-1);
			GraphRVar key3 = (GraphRVar)cond.elementAt(j);
			if(key1.index() > key3.index()){
				System.out.println("ERROR: Sorting is not working.");
			}
		}
		return cond;
	};
	
	public FactSimple symmetric(){
		return new FactSimple(I, R,L, cond);
	}
	static FastSet convertToFastSet(Set S, int numVarsInV){
		FastSet cond = new FastSet(numVarsInV);
		for(int i=0; i<S.size(); i++){
			cond.add(((GraphRVar)S.elementAt(i)).index()); 
		}
		return cond;
	}
	
	static public Set convertToSet(FastSet S, Set V){
		Set newS = new Set();
		
		for(int i=0; i<V.size(); i++){
			if(S.isMember(i)) newS.add(V.elementAt(i));
		}
		return newS;
	}
	public String proof(){
		return "\n"+proofInt("                                                   ")+"";
	}
	public String proofInt(String tab){
		String proof="";
		if(this.antecedentOne != null){
			String tabint = tab;
			if(tabint.length() > 10) tabint = tabint.substring(0, tabint.length()-10);
			proof += antecedentOne.proofInt(tabint) + "\n";
			if(antecedentTwo != null) proof += antecedentTwo.proofInt(tabint) + "\n";
			if(antecedentThree != null) proof += antecedentThree.proofInt(tabint) + "\n";
		}
		proof += tab+"("+ axiom +") => " + this.toString();
		return proof;
	}
	
	public String toString(){
		int i1 = L, j1 = R;
		String ret = I?"":"!"; 
		String ret2;
		if(cond != null) ret2 =  order() +":"+ret+ "I(" + L +", " + cond + ", " +R + ").";
		else ret2 =  order() +":"+ret+ "I(" + L +", " + condNotFast + ", " +R + ").";
		
		return ret2;
	}
}
