package machineLearning.independenceBased;

import machineLearning.Graphs.GraphRVar;
import machineLearning.Utils.*;

/**
 * Class Fact
 * 
 * @author Facundo Bromberg
 */
public class gsTriplet extends Triplet{
	public boolean grown = true; //if false, it must be shrinked
	
	/**
	 * Used for recovering the proof path.
	 */
	//Note, cond is refering to an externally created object. Hopefully nobody frees or changes it.
	
	public gsTriplet(gsTriplet f){
		super(f);
		this.grown = f.grown;
		
	}
	public gsTriplet(int  X, int Y, Set Z, int n, boolean grown){
		super(X,Y,Z,n);
		this.grown = grown;
		
	}
	public gsTriplet(int  X, int Y, Set Z, boolean grown){
		super(X,Y,Z,-1);
		this.grown = grown;
		
	}
	public gsTriplet(int  X, int Y, FastSet Z, boolean grown){
		super(X,Y,Z);
		this.grown = grown;
		
	}
	
	/*public void copyContentFromFast(gsTriplet f){

		this.grown = f.grown;
		this.X(f.X());
		this.Y(f.Y());
		
		this.FastZ.clear();
		this.FastZ.Union(f.FastZ);
	}*/
}
