package machineLearning.independenceBased;

/***************************************************************************
 *
 * Bayesian test implementation.  Based on D. Margaritis and S. Thrun, "A
 * Bayesian Multiresolution Test", Proceedings of the Uncertainty in AI
 * conference, 2001.
 *
 * (C) 2006 Dimitris Margaritis, All Rights Reserved.
 *
 ***************************************************************************/

enum HyperparamType { PER_CELL, TOTAL_UNIFORM }

public final class BayesianTestDimitris extends StatisticalTest {

  // Prior probability of idependence.
  public double priorIndep;
  
  // Natural logarithm of the prob. of independence of the last test
  // conducted.
  private double logProbIndepLast;
  
  // PER_CELL or TOTAL_UNIFORM.
  public HyperparamType htype;
  
  // Value of the hyperparameter.
  public double hval;

  /**
   * 
   * Constructor.
   * 
   */

  public BayesianTestDimitris(String datafile, double priorIndep,
                      HyperparamType htype, double hval) {
    super(datafile);
    this.priorIndep = priorIndep;
    logProbIndepLast = -1.0;
    this.htype = htype;
    this.hval = hval;
    logProbIndepLast = -1.0;
  }

  /**
   * 
   * Conditional independence test.  Return true/false.
   *
   */
  public boolean areIndep(boolean[] X, boolean[] Y, boolean[] Z) {
    logProbIndepLast = lnProbIndep(X, Y, Z);
    boolean areIndep = (logProbIndepLast >= Math.log(priorIndep));

    num_tests++;
    num_weighted_tests +=
      setNumMembers(X) + setNumMembers(Y) + setNumMembers(Z);

    if (verbose)
      System.out.println("INDEP( " +
                         setToString(X) + " , " +
                         setToString(Y) + " | " +
                         setToString(Z) +
                         " ), pr_indep = " + Math.exp(logProbIndepLast) +
                         " (" +
                         (areIndep ? "independent" : "dependent") +
                         ")");

    return areIndep;
  }

  // The strength of the Bayesian test is the posterior probability of
  // independence.

  public double lastTestIndepStrength() {
    return logProbIndepLast;
  }

  /**
   * Compute the Bayesian conditional independence test with equal
   * hyperparameters summing to 1 i.e.,
   *
   * sum_{ij} gamma[i][j] = 1
   * sum_{i} alpha[i] = 1
   * sum_{j} beta[j] = 1
   *
   * Returns the the posterior probability of independence.
   */
  private double lnProbIndep(boolean[] X, boolean[] Y, boolean[] Z) {
    assert X.length == Y.length : "X and Y have different sizes.";
    assert X.length == Z.length : "X and Z have different sizes.";

    // Make union of X and Y.
    boolean[] XY = new boolean[nvars];
    for (int j = 0; j < nvars; j++) XY[j] = (X[j] || Y[j]);

    int nslices = numConfigs(Z);

    double pr_indep_slice = Math.pow(priorIndep, 1.0 / nslices);
    double ratio_indep_slice = (1 - pr_indep_slice) / pr_indep_slice;

    int[][] counts_X = makeHistogramFromDataset(X, Z);
    int[][] counts_Y = makeHistogramFromDataset(Y, Z);
    int[][] counts_XY = makeHistogramFromDataset(XY, Z);
    assert counts_X.length == nslices : "counts_X.length != nslices";
    assert counts_Y.length == nslices : "counts_Y.length != nslices";
    assert counts_XY.length == nslices : "counts_XY.length != nslices";

    double lnnum = 0.0;
    double lndenom = 0.0;
    for (int s = 0; s < nslices; s++) {

      double ln_p =
        ln_upsilon(counts_X[s], nslices) +
        ln_upsilon(counts_Y[s], nslices);
      double ln_q = ln_upsilon(counts_XY[s], nslices);

      if (ln_p >= ln_q) {
        lndenom += Math.log1p(ratio_indep_slice * Math.exp(ln_q - ln_p));
      } else {  // ln_p < ln_q
        lnnum += Math.log(pr_indep_slice) + ln_p;
        lndenom +=
          Math.log(1 - pr_indep_slice) + ln_q +
          Math.log1p(1/ratio_indep_slice * Math.exp(ln_p - ln_q));
      }

    } // end for (s)

    double ln_pr_indep = lnnum - lndenom;

    return ln_pr_indep;
  }

  /**
   * Natural logarithm of the data likelihood of a model.  The counts in
   * each cell are provided as arguments.
   */
  private final double ln_upsilon(int[] counts, int nslices) {
    int ncounts = counts.length;
    double epsilon = -1.0;
    switch (htype) {
    case PER_CELL:
      epsilon = hval;
      break;
    case TOTAL_UNIFORM:
      epsilon = hval / nslices / ncounts;
      break;
    default:
      System.err.println("Unknown HyperparamType: " + htype);
      System.exit(1);
    }
    int ndata = 0;
    double ln_u = 0.0;
    for (int c = 0; c < ncounts; c++) {
      ln_u += gammln(counts[c] + epsilon) - gammln(epsilon);
      ndata += counts[c];
    }
    ln_u += gammln(ncounts * epsilon) - gammln(ndata + ncounts * epsilon);

    return ln_u;
  }

  /**
   * Auxiliary function: Natural logarithm of the Gamma function.
   */
  private static final double gammln(double xx) {
    double x, y, tmp, ser;
    double[] cof = {
      76.18009172947146, -86.50532032941677,
      24.01409824083091, -1.231739572450155,
      0.1208650973866179e-2, -0.5395239384953e-5
    };
    int j;

    if ((int) xx == xx) {
      double res = 0.0;
      for (int a = (int) xx - 1; a >= 2; a--) {
        res += Math.log(a);
      }
      return res;
    }

    y = x = xx;
    tmp = x + 5.5;
    tmp -= (x + 0.5) * Math.log(tmp);
    ser = 1.000000000190015;
    for (j = 0; j <= 5; j++)
      ser += cof[j] / ++y;
    return -tmp + Math.log(2.5066282746310005 * ser / x);
  }

  /**
   * 
   * Program starts here.
   *
   */
  public static void main(String[] args) {
/*	String dir = "Z:/experiments/datasets/sampled/undirected/datasets/";
	String dataset = dir+"small.csv";
	String [] args2 = {"-v", dataset, "PER_CELL", "1", "2", "10", "3", "7", "8"};
    args = args2;*/
    
    if ((args.length == 5 && args[0].equals("-v")) || args.length < 5) {
      System.out.println("");
      System.out.println("USAGE: BayesianTest [-v] <datafile> " +
                         "{ PER_CELL | TOTAL_UNIFORM } <hyperparam_value> " +
                         "X Y Z1 Z2 ...");
      System.out.println("");
      System.out.println("X, Y, Z1, ... are numbers indicating variables " +
                         "(numbers starting at 1)");
      System.out.println("");
      System.exit(1);
    }

    
    
    int argi = 0;
    boolean verbose = false;
    if (args[0].equals("-v")) {
      verbose = true;
      argi++;
    }
    String datafile = args[argi++];
    String hyperparam_style = args[argi++];
    HyperparamType htype = null;
    if (hyperparam_style.equals("PER_CELL"))
      htype = HyperparamType.PER_CELL;
    else if (hyperparam_style.equals("TOTAL_UNIFORM"))
      htype = HyperparamType.TOTAL_UNIFORM;
    else {
      System.err.println("Only PER_CELL or TOTAL_UNIFORM are " +
                         "allowed in the 3rd argument.");
      System.exit(1);
    }
    double hval = Double.parseDouble(args[argi++]);
    BayesianTestDimitris bt = new BayesianTestDimitris(datafile, 0.5, htype, hval);
    int X = Integer.parseInt(args[argi++]);
    if (X < 1 || X > bt.nvars) {
      System.err.println("ERROR: Variable " + X +
                         " is out of range [1, " + bt.nvars + "].");
      System.exit(1);
    }
    X--;
    int Y = Integer.parseInt(args[argi++]);
    if (Y < 1 || Y > bt.nvars) {
      System.err.println("ERROR: Variable " + Y +
                         " is out of range [1, " + bt.nvars + "].");
      System.exit(1);
    }
    Y--;
    boolean[] Z = new boolean[bt.nvars];
    while (argi < args.length) {
      int Z_i = Integer.parseInt(args[argi++]);
      if (Z_i < 1 || Z_i > bt.nvars) {
        System.err.println("ERROR: Variable " + Z_i +
                           " is out of range [1, " + bt.nvars + "].");
        System.exit(1);
      }
      Z[Z_i-1] = true;
    }
    bt.verbose = verbose;

    //
    // INDEP(X, Y | Z) => 0 exit status => command "succeeded".
    //
    System.exit(bt.areIndep(X, Y, Z) ? 0 : 1);
  }
}

/***************************************************************************/
