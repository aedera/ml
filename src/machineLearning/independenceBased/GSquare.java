package machineLearning.independenceBased;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.BN;
import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;

public class GSquare extends chiSquare {
	// --------------------------------------------------------------
	public GSquare (FastDataset data) {
		super(data, default_alpha);
	}
	// --------------------------------------------------------------
	public GSquare (FastDataset data, UGraphSimple trueModel) {
		super(data, trueModel, default_alpha);
	}
	// --------------------------------------------------------------
	public GSquare (FastDataset data, BN trueModel) {
		super(data, trueModel, default_alpha);
	}
	// --------------------------------------------------------------
	public GSquare (FastDataset data, double threshold) {
		super(data, threshold);
	}
	// --------------------------------------------------------------
	public GSquare (FastDataset data, UGraphSimple trueModel, double threshold) {
		super(data, trueModel, threshold);
	}
	// --------------------------------------------------------------
	public GSquare (FastDataset data, BN trueModel, double threshold) {
		super(data, trueModel, threshold);
	}
	 //--------------------------------------------------
	//Oij is the frequency count and Eij is the null-hypthesis (independence) model expected value
	protected double computeCell(double Oij, double Eij){
		return Oij == 0? 0.0 : 2.0*Oij * Math.log(Oij / Eij);
	}

	public static void main(String[] args) {
		int D [] = {10, 20, 40, 80, 160, 320, 640, 900, 1200, 1600, 2400};
		for(int d : D){
			String dataFileName = "sampledData_n6_t1_E100_M500_R8.csv";
			FastDataset data = new FastDataset(dataFileName);
			MarkovNet xMn = new MarkovNet();
		    xMn.read(data, dataFileName, 5, d);
		    UGraphSimple trueNet =  new UGraphSimple(new UGraphSimple(xMn));
		   // System.out.println(trueNet);
		    
		    int X = 1;
		    int Y = 4;
		    FastSet FastZ = new FastSet(6);
		    chiSquare cs = new chiSquare(data, 0.05);
		    GSquare gs = new GSquare(data, 0.05);
			pFact pf_gs = gs.independent(new Triplet(X, Y, FastZ));
			pFact pf_cs = cs.independent(new Triplet(X, Y, FastZ));
			System.out.print(d + ", " + pf_cs.toStringPValue());
			System.out.print(", " + pf_gs.toStringPValue() + "\n");
			
/*			Set Z = new Set();
			double logpvalue[] = new double[1];
			
			int k = FastZ.first();
			while(k >= 0) {
				Z.add(data.schema().getAttribute(k));
				k = FastZ.next();
			}
			boolean out = Statistics.independent(data, X, Y, Z, 0.05, false, Statistics.CHISQUARE, null, logpvalue);
			System.out.println((out?"I":"!I") + (Triplet.toString(X, Y, FastZ)) + " = " + logpvalue[0]);*/
		}
	}

}
