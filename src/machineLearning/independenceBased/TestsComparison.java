package machineLearning.independenceBased;

import machineLearning.Utils.Experiment;
import machineLearning.Utils.Utils;
import machineLearning.Utils.coVL;

public class TestsComparison {
	/**
	 * Dimension correspond to ta, oracle, ta_cond respectively,
	 * and I (F) correspond to independence (dependence).
	 * So for instance, 
	 * 				matrix[I][D][D] 
	 * corresponds to ta_cond and oracle are dependent and ta is indepdnent 
	 */
	static final short D = 0; //stands for Dependent
	static final short I = 1; //stands for Independent
	int [][][] counts = new int[2][2][2];
	
	TestsAccuracy ta, ta_cond;
	
	public TestsComparison(TestsAccuracy ta, TestsAccuracy ta_cond){
		this.ta = ta;
		this.ta_cond = ta_cond;
		
		//Checks both have the same oracle
		//Utils.FORCED_ASSERT(ta.oracle == ta_cond.oracle, "Test oracles mismatch. ");
		
		//Checks both have the same triplets
		if(ta == null || ta_cond == null){
			int asdfasd=0;
		}
		/*Utils.FORCED_ASSERT(ta.kbs.length == ta_cond.kbs.length, "KBS length mismatch. ");
		for(int i=0; i<ta.kbs.length; i++)  {
			Utils.FORCED_ASSERT(ta.kbs[i].length == ta_cond.kbs[i].length, "KBS[i] length mismatch. ");
			for(int j=0; j<ta.kbs[i].length;j++){
				Utils.FORCED_ASSERT((ta.kbs[i][j] == null && ta_cond.kbs[i][j] == null) || ta.kbs[i][j].isEqual(ta_cond.kbs[i][j]), "Triplet mistmatch");
    		}
    	}*/
		
		computeCounts();
		
	}
	//--------------------------------------------------------------------------------
	public int getCount(boolean ta, boolean oracle, boolean ta_cond){
		return counts[getIndex(ta)][getIndex(oracle)][getIndex(ta_cond)];
	}
	//--------------------------------------------------------------------------------
	public double getProbability(boolean ta, boolean oracle, boolean ta_cond){
		double num = getCount(ta, oracle, ta_cond);
		double den = getCount(ta, oracle, ta_cond) + getCount(!ta, oracle, ta_cond);
		return den==0?0:num/den;
	}
	//--------------------------------------------------------------------------------
	private final short getIndex(boolean val){
		return val?I:D;
	}
	//--------------------------------------------------------------------------------
	private void computeCounts(){
		short taIx, ta_condIx, oracleIx, oracle_condIx;
		
		
         int k=0;
         
	    //grows a KB with tests to be performed
		for(int i=0; i<ta.kbs.length; i++)  {
			for(int j=0; j<ta.kbs[i].length;j++){
				// ta == null  IFF  ta_cond == null
				if(!Utils.FORCED_ASSERT((ta.testResults[i][j] != null || ta_cond.testResults[i][j]==null) &&
						(ta.testResults[i][j] == null || ta_cond.testResults[i][j]!=null), "not true that ta == null  IFF  ta_cond == null")){
					int asdfas=0;
				}
				
				if(ta.testResults[i][j] == null) continue;
				
				taIx = getIndex(ta.testResults[i][j].truth_value);  
				ta_condIx = getIndex(ta_cond.testResults[i][j].truth_value);
				oracleIx = getIndex(ta.oracleResults[i][j].truth_value);
				oracle_condIx = getIndex(ta_cond.oracleResults[i][j].truth_value);
				if(!Utils.FORCED_ASSERT(oracleIx == oracle_condIx, "oracle values mismatch")){
					int asdfas=0;
				}
				counts[taIx][oracleIx][ta_condIx]++;
				k++;
    		}
    	}
		k = k;
	}
}
