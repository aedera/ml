package machineLearning.independenceBased;

import machineLearning.Utils.*;

/**
 * Class Fact
 * 
 * @author Facundo Bromberg
 */
public class Triplet extends TripletAbstract{
	/**
	 * sets type of fact: independence (I=true) or dependence (I=false)fact.
	 */
	final public int X,Y;  //Left, Right, Third (for weak transitivity and chordality)
	private Set Z;

	/**
	 * Used for recovering the proof path.
	 */
	public Triplet(Triplet f){
		super(f.FastZ);
		this.X = f.X>f.Y? f.Y : f.X;
		this.Y = f.X>f.Y? f.X : f.Y;
		this.Z = null;
	}
	
	/**
	 * @param X
	 * @param Y
	 * @param Z
	 */
	public Triplet(int  X, int Y, Set Z){
		super(null);
		
		this.X = X>Y? Y : X;
		this.Y = X>Y? X : Y;
		
		this.Z = new Set(Z);
		
	}
	
	/**
	 * @param X
	 * @param Y
	 * @param Z
	 * @param n
	 */
	public Triplet(int  X, int Y, Set Z, int n){
		super(new FastSet(n));
		
		this.X = X>Y? Y : X;
		this.Y = X>Y? X : Y;
		
		this.Z = new Set(Z);

	}
	
	/**
	 * @param X
	 * @param Y
	 * @param Z
	 */
	public Triplet(int  X, int Y, FastSet Z){
		super(Z);
		
		this.X = X>Y? Y : X;
		this.Y = X>Y? X : Y;
		
		this.Z = null;

	}
	
	/**
	 * @return
	 */
	public final Set getCondNotSimple(){
		return Z;
	}
	
	/**
	 * @param S
	 */
	public void setCondNotSimple(Set S){
		Z = S;
	}
	
	/* (non-Javadoc)
	 * @see machineLearning.independenceBased.TripletAbstract#symmetric()
	 */
	public Triplet symmetric(){
		return new Triplet(Y, X, FastZ);
	}

	/* (non-Javadoc)
	 * @see machineLearning.independenceBased.TripletAbstract#numVars()
	 */
	public int numVars(){
		return 2 + ((Z==null)? FastZ.cardinality() : Z.cardinality());
	}
	
	/* (non-Javadoc)
	 * @see machineLearning.independenceBased.TripletAbstract#isEqual(machineLearning.independenceBased.TripletAbstract)
	 */
	public final boolean isEqual(TripletAbstract trip){
		Triplet tripS = (Triplet) trip;
		if(tripS.X != X) {
			return false;
		}
		
		if(tripS.Y != Y) {
			return false;
		}
		
		if(tripS.FastZ != null && FastZ != null){
			if(!FastSet.isEqual(tripS.FastZ, FastZ))  return false;
			else return true;
		}
		
		if(tripS.getCondNotSimple() != null && getCondNotSimple() != null && !Set.isEqual(tripS.getCondNotSimple(), getCondNotSimple())) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * @param x
	 * @param y
	 * @param Z
	 * @return
	 */
	final public boolean isEqual(int x, int y, int ... Z){
		boolean ret = X==x && Y==y && FastZ.cardinality() == Z.length;
		if(!ret) {
			return false;
		}
		
		for(int z : Z){
			if(!FastZ.isMember(z)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @return
	 */
	public int cost(){
		if(Z != null) {
			return 2+Z.size();
		} else {
			return 2+FastZ.cardinality();
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	final public int hashCode() {
		return getKey().hashCode();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	final public String toString(){
		return toStringInternal(this.X, this.Y, this.FastZ, this.Z);
	}
	
	/**
	 * @return
	 */
	public String toStringSets(){
		return StringSets();
	}
	
	/**
	 * @param X
	 * @param Y
	 * @param Z
	 * @return
	 */
	static public String toString(int X, int Y, FastSet Z){
		String ret2;
		ret2 =  "(" + X +"," + Y + "|";
		if(Z != null && !Z.isEmpty()) ret2 += Z + ")";
		else ret2 += Z + ")";
		
		return ret2;
	}
	
	/**
	 * @param X
	 * @param Y
	 * @param FastZ
	 * @param Z
	 * @return
	 */
	final private String toStringInternal(int X, int Y, FastSet FastZ, Set Z){
		String ret2;
		ret2 =  "(" + X +"," + Y + "|";
		if(Z != null && !Z.isEmpty()) ret2 += Z + ")";
		else ret2 += FastZ + ")";
		
		return ret2;
	}
	
	/**
	 * @return
	 */
	final private String StringSets(){
		String ret2;
		ret2 =  "({" + X +"},{" + Y + "}|";
		if(Z != null && !Z.isEmpty()) ret2 += Z + ")";
		else ret2 += FastZ + ")";
		
		return ret2;
	}

	public int X(){return X;} 
	public int Y(){return Y;} 
}
