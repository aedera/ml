package machineLearning.independenceBased.fitPivot;

import java.util.HashMap;

import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.SliceForHashTable;
import machineLearning.independenceBased.TripletSets;

/**
 * 
 * @author sperez
 *
 */
class TripletTables {
	
	private TripletSets triplet;
	private HashMap<SliceForHashTable, double[][]> contTables;
	
	public TripletTables(TripletSets triplet, HashMap<SliceForHashTable, double[][]> contTables) {
		super();
		this.triplet = triplet;
		this.contTables = contTables;
	}

	public TripletSets getTriplet() {
		return triplet;
	}

	public void setTriplet(TripletSets triplet) {
		this.triplet = triplet;
	}

	public HashMap<SliceForHashTable, double[][]> getContTables() {
		return contTables;
	}

	public void setContTables(HashMap<SliceForHashTable, double[][]> contTables) {
		this.contTables = contTables;
	}
	
}
