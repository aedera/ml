package machineLearning.independenceBased.fitPivot;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;

import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.SliceForHashTable;



/**
 * 
 * @author seba
 * 
 */
public class CTCacheXYPivot {
	public Vector<TripletTables> map[][];
//	public double[][] map;
//	static public double numMBytes = 0;
	
	public CTCacheXYPivot(int n) {
		super();
//		numMBytes += 4*(n+1)*(n+1);
		/*
		 * habria un vector para cada par xy, 
		 * de k+1 cardinalidades distintas,
		 * y con indices para cada variable
		 */
		map = new Vector[n+1][];
	}

}
