package machineLearning.independenceBased.fitPivot;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;

import machineLearning.Datasets.FastDataset;
import machineLearning.Graphs.BN;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.FastSet;
import machineLearning.independenceBased.BayesianTest;
import machineLearning.independenceBased.SliceForHashTable;
import machineLearning.independenceBased.TripletSets;

/**
 * 
 * @author seba
 * 
 */
public class FITPivot extends BayesianTest {

	public int K = 10;

	public long inferredTables = 0;
	public long notInferredTables = 0;
	public long numTables = 0;

	// Estructura que almacena laa caches de tablas.
	public Map<BitSet, CTCacheXYPivot> xyPairsCache = new HashMap<BitSet, CTCacheXYPivot>();

	public boolean saveIntermediateTables = false;

	public long weightedNumberOfTests;

	public int weightedNumberOfNotInferredTests=0;

	public int weightedNumberOfInferredTests=0;

	public int inferredTimes;

	public int notInferredTimes;

	public FITPivot(FastDataset data, BN trueModel, double prior) {
		super(data, trueModel, prior);
	}

	public FITPivot(FastDataset data, BN trueModel) {
		super(data, trueModel);
	}

	public FITPivot(FastDataset data, double prior) {
		super(data, prior);
	}

	public FITPivot(FastDataset data, UGraphSimple trueModel, double prior) {
		super(data, trueModel, prior);
	}

	public FITPivot(FastDataset data, UGraphSimple trueModel) {
		super(data, trueModel);
	}

	public FITPivot(FastDataset data) {
		super(data);
	}

	/**
	 * Calcula las contTables para el triplete (X,Y|Z) a partir de un
	 * supercondicionante de Z. Si no existen las contTables en la Cache para el
	 * supercondicionente, o la Cache para el par de variables X,Y tampoco
	 * existe, calcula las tablas a partir del DataSet.
	 * 
	 * @param triplet - Triplete para el que se van a calcular las contTables.
	 * @return Las tablas de Contingencia para cada configuracion (slice) del
	 *         triplete (X,Y|Z).
	 */
	public HashMap<SliceForHashTable, double[][]> readTables(TripletSets triplet) {

		HashMap<SliceForHashTable, double[][]> tables = null;

		BitSet keyBitSetXY = new BitSet(2);
		keyBitSetXY.flip(0);
		keyBitSetXY.flip(1);

		CTCacheXYPivot cache_XY = xyPairsCache.get(keyBitSetXY);

		if (cache_XY == null) {
			cache_XY = new CTCacheXYPivot(n);
			xyPairsCache.put(keyBitSetXY, cache_XY);
		}

		TripletTables superTripletTables = getSuperTripletTables(cache_XY, triplet);
		
		boolean inferred = true;
		if (superTripletTables == null) {
			superTripletTables = createTablesSuperTriplet(cache_XY, triplet);
			inferred = false;
		}

		superTripletTables = projectTable(cache_XY, superTripletTables, triplet);
		
		if(saveIntermediateTables) {
			while ((superTripletTables.getTriplet().FastZ.cardinality() - triplet.FastZ.cardinality()) > 1) {
				superTripletTables = calculateMiddleTriplets(cache_XY, superTripletTables, triplet);
			}
		}
		
		tables = calculateTables(cache_XY, superTripletTables, triplet);
		
		if (inferred) {
			weightedNumberOfInferredTests += 2 + triplet.FastZ.cardinality();
			inferredTables++;
//			System.out.println("triplete inferido: " + triplet);
		} else{
			weightedNumberOfNotInferredTests += 2 + triplet.FastZ.cardinality();
			notInferredTables++;
//			System.out.println("triplete no inferido: " + triplet);
		}

		return tables;
	}

	/**
	 * 
	 * @param cache_XY
	 * @param superConditionalCT
	 * @param tripletSC
	 * @return
	 */
	private TripletTables calculateMiddleTriplets(CTCacheXYPivot cache_XY, TripletTables superConditionalCT, TripletSets tripletSC) {
		HashMap<SliceForHashTable, double[][]> contTables = null;

		FastSet fastZ = tripletSC.FastZ;

		FastSet fastMC = new FastSet(superConditionalCT.getTriplet().FastZ);

		int[] a = new int[fastMC.cardinality()];

		int i = 0;
		int index = fastMC.first();
		while (index != -1) {
			a[i] = index;
			i++;
			index = fastMC.next();
		}

		Random random = new Random();
		index = random.nextInt(fastMC.cardinality());
		while (fastZ.isMember(a[index])) {
			index = random.nextInt(fastMC.cardinality());
		}
		fastMC.flip(a[index]);

		TripletSets tripletMC = new TripletSets(tripletSC.X, tripletSC.Y, fastMC);
		
		contTables = calculateTables(cache_XY, superConditionalCT, tripletMC);
		return new TripletTables(tripletMC, contTables);
	}

	/**
	 * 
	 * @param cache_XY
	 * @param triplet
	 * @return
	 */
	private TripletTables createTablesSuperTriplet(CTCacheXYPivot cache_XY, TripletSets triplet) {

		HashMap<SliceForHashTable, double[][]> tables = null;

		int flips = K;

		FastSet fastSC = new FastSet(triplet.FastZ);

		Random random = new Random();
		FastSet compfastSC = (new FastSet(fastSC)).complement();
		for (int i = 0; i < flips && compfastSC.cardinality() > 0; i++) {
			int index = compfastSC.getOn(random.nextInt(compfastSC.cardinality()));
			fastSC.add(index);
			compfastSC.remove(index);
		}
		
		TripletSets superTriplet = new TripletSets(triplet.X.first(), triplet.Y.first(), fastSC);
		tables = super.readTables(superTriplet);
		
		saveCache(cache_XY, tables, superTriplet);
		
		numTables += tables.size();
		return new TripletTables(superTriplet, tables);
	}

	/**
	 * 
	 * @param cache_XY
	 * @param triplet
	 * @return
	 */
	private TripletTables getSuperTripletTables(CTCacheXYPivot cache_XY, TripletSets triplet) {

		FastSet fastZ = new FastSet(triplet.FastZ);
		fastZ.flip(triplet.X.first());
		fastZ.flip(triplet.Y.first());
		
		int cardinality = fastZ.cardinality();
		for (int i = cardinality; i <= fastZ.capacity; i++) {
			if(cache_XY.map[i] == null) continue;
			
			// Busca la lista que menos apuntadores contenga.
			int index = fastZ.first();
			index = (index == -1)? n : index;
			int min_size=Integer.MAX_VALUE, jmin=index;
			
			while(index != -1) {
				if(cache_XY.map[i][index] != null) {
					int size = cache_XY.map[i][index].size() ; 
					if(size < min_size) {
						min_size = size;
						jmin = index;
					}
				}
				index = fastZ.next();
			}
			
			if(cache_XY.map[i][jmin] != null){
				for (TripletTables tt : cache_XY.map[i][jmin]) {				
					FastSet fastZtt = new FastSet(tt.getTriplet().FastZ);
					fastZtt.flip(tt.getTriplet().X.first());
					fastZtt.flip(tt.getTriplet().Y.first());
					if( FastSet.isSubSet(fastZ, fastZtt)) {
						return tt; 
					}
				}
			}
		}

		return null;
	}


	/**
	 * 
	 * @param cache_XY
	 * @param superTriplet
	 * @param triplet
	 * @return
	 */
	private HashMap<SliceForHashTable, double[][]> calculateTables(CTCacheXYPivot cache_XY, TripletTables superTriplet, TripletSets triplet) {

		if(superTriplet.getTriplet().isEqual(triplet)) return superTriplet.getContTables();
		
		FastSet fastSC = superTriplet.getTriplet().FastZ;
		FastSet fastZ = triplet.FastZ;	
			
		HashMap<SliceForHashTable, double[][]> mapContTablesZ = new HashMap<SliceForHashTable, double[][]>();
		HashMap<SliceForHashTable, double[][]> mapContTablesSC = superTriplet.getContTables();

		Set<Entry<SliceForHashTable, double[][]>> slicesSC = mapContTablesSC.entrySet();

		for (Entry<SliceForHashTable, double[][]> sliceSC : slicesSC) {
			double[][] contTableSC = sliceSC.getValue();//mapContTablesSC.get(sliceSC);
			SliceForHashTable sliceZ = getSliceZ(sliceSC.getKey(), fastSC, fastZ);

			double[][] contTableZ = mapContTablesZ.get(sliceZ);

			if (contTableZ != null) {
				addTables(contTableSC, contTableZ);
			} else {
				contTableZ = new double[contTableSC.length][contTableSC[0].length];
				addTables(contTableSC, contTableZ);
				mapContTablesZ.put(sliceZ, contTableZ);
			}
		}

		saveCache(cache_XY, mapContTablesZ, triplet);

		return mapContTablesZ;
	}

	/**
	 * Suma las celdas de la tabla contTableSC a las celdas de la tabla
	 * contTableZ.
	 * 
	 * @param contTableSC
	 *            - Tabla de contingencia del Supercondionante.
	 * @param contTableZ
	 *            - Tabla de contingencia del condicionante Z.
	 */
	private void addTables(double[][] contTableSC, double[][] contTableZ) {
		for (int i = 0; i < contTableZ.length; i++) {
			for (int j = 0; j < contTableZ[0].length; j++) {
				contTableZ[i][j] = contTableZ[i][j] + contTableSC[i][j];
			}
		}
	}

	/**
	 * 
	 * @param cache_XY
	 * @param mapContTables
	 * @param triplet
	 */
	private void saveCache(CTCacheXYPivot cache_XY, HashMap<SliceForHashTable, double[][]> mapContTables, TripletSets triplet) {

		FastSet fastZ = new FastSet(triplet.FastZ);
		fastZ.flip(triplet.X.first());
		fastZ.flip(triplet.Y.first());
		
		int cardinality = fastZ.cardinality();
		int index = fastZ.first();
		
		while(true){

			index = (index == -1)? n : index;

			if(cache_XY.map[cardinality] == null) cache_XY.map[cardinality] = new Vector[n+1];
			if (cache_XY.map[cardinality][index] == null) {
				cache_XY.map[cardinality][index] = new Vector<TripletTables>();
			}
			
			cache_XY.map[cardinality][index].add(new TripletTables(triplet, mapContTables));

			if(index == n) break;
			index = fastZ.next();
		}
		
	}


	/**
	 * 
	 * @param sliceSC
	 * @param fastSC
	 * @param fastZ
	 * @return
	 */
	private SliceForHashTable getSliceZ(SliceForHashTable sliceSC, FastSet fastSC, FastSet fastZ) {
		int[] a = new int[fastZ.cardinality()];

		int indexSC = fastSC.first();
		int jSC = 0, j=0;
		while(indexSC != -1) {
			if(fastZ.isMember(indexSC)) {
				a[j] = sliceSC.a[jSC];
				j++;
			}
			
			indexSC = fastSC.next();
			jSC++;
		}
		SliceForHashTable sliceZ = new SliceForHashTable(a);
		return sliceZ;
	}
	
	public void resetCache() {
		xyPairsCache.clear();
	}

	/**
	 * 
	 * @param tripletTables
	 * @param tripletPrime
	 * @return
	 */
	private TripletTables projectTable(CTCacheXYPivot cache_XY, TripletTables tripletTables, TripletSets tPrime){

		TripletSets triplet = tripletTables.getTriplet();

		FastSet ftp = new FastSet(triplet.FastZ);
		ftp.flip(triplet.X.first());
		ftp.flip(triplet.Y.first());
		ftp.flip(tPrime.X.first());
		ftp.flip(tPrime.Y.first());
		
		TripletSets tripletPrime = new TripletSets(tPrime.X,tPrime.Y,ftp);

		if (triplet.isEqual(tripletPrime)) return tripletTables;
				
		HashMap<SliceForHashTable, double[][]> mapTablesPrime = new HashMap<SliceForHashTable, double[][]>();
		
		HashMap<SliceForHashTable, double[][]> mapTables = tripletTables.getContTables();
		Set<SliceForHashTable> slices = mapTables.keySet();

		FastSet fastTriplet = new FastSet(triplet.FastZ);

		int xTriplet = triplet.X.first();
		int yTriplet = triplet.Y.first();		

		FastSet fastTripletPrime = new FastSet(tripletPrime.FastZ);
		
		int xTripletPrime = tripletPrime.X.first();
		int yTripletPrime = tripletPrime.Y.first();
		
		for (SliceForHashTable slice : slices) {
			double[][] table =  mapTables.get(slice);
			int[] a = slice.a;

			for (int i = 0; i < table.length; i++) {
				for (int j = 0; j < table[0].length; j++) {
					
					if (table[i][j] == 0) continue;
					
					int[] v = new int[n];
					v[xTriplet] = i;
					v[yTriplet] = j;
									
					int k = fastTriplet.first();
					int l = 0;
					while(k != -1) {
						v[k] = a[l];
						l++;
						k = fastTriplet.next();
					}

					int[] aPrime = new int[slice.a.length];					
					k = fastTripletPrime.first();
					l = 0;
					while(k != -1) {
						aPrime[l] = v[k];
						l++;
						k = fastTripletPrime.next();
					}
					
					SliceForHashTable slicePrime = new SliceForHashTable(aPrime);
					if (!mapTablesPrime.containsKey(slicePrime))
						mapTablesPrime.put(slicePrime, new double[table.length][table[0].length]);
					mapTablesPrime.get(slicePrime)[ v[xTripletPrime] ][ v[yTripletPrime] ] = table[ v[xTriplet] ][ v[yTriplet] ];	
					
				}
			}
			
		}
		
		saveCache(cache_XY, mapTablesPrime, tripletPrime);
		
		TripletTables ttPrime = new TripletTables(tripletPrime, mapTablesPrime);
		return ttPrime;
	}	
	
//	/**
//	 * 
//	 * @param mapTables
//	 */
//	private void imprimirTablas(HashMap<SliceForHashTable, double[][]> mapTables) {
//		Set<SliceForHashTable> slices = mapTables.keySet();
//		
//		for (SliceForHashTable slice : slices) {
//			double[][] table = mapTables.get(slice);
//			
//			System.out.println("\nSlice: " + slice);
//			for (int i = 0; i < table.length; i++) {
//				for (int j = 0; j < table.length; j++) {
//					System.out.print(table[i][j] + "\t");
//				}
//				System.out.println();
//			}
//		}
//	}
//	
//	
//	public static void main(String[] args){
////		FITPivot f = new FITPivot(null);
////		f.n = 4;
////		
////		// (0,1|2,3)
////		FastSet ft = new FastSet(f.n);
////		ft.setAlltoOne();
////		ft.flip(0);
////		ft.flip(1);
////		TripletSets t = new TripletSets(0,1,ft);
////
////		HashMap<SliceForHashTable, double[][]> mapTables = new HashMap<SliceForHashTable, double[][]>();
////		
////		Random r = new Random();
////		
////		int k = 0;
////		for (int i = 0; i < 2; i++) {
////			for (int j = 0; j < 2; j++) {
////				int a[] = {i,j};
////				SliceForHashTable slice = new SliceForHashTable(a);
////				double[][] table = {{k++,k++},
////					 	 			{k++,k++}};
////				mapTables.put(slice, table);
////			}
////		}		
//
//		
////==============================================================//
//		
//		FITPivot fit = new FITPivot(null);		
//		fit.n = 5;
//		
//		// (0,1|2,3)
//		FastSet ft = new FastSet(fit.n);
//		ft.setAlltoOne();
//		ft.flip(0);
//		ft.flip(1);
//		TripletSets t = new TripletSets(0,1,ft);
//
//		HashMap<SliceForHashTable, double[][]> mapTables = new HashMap<SliceForHashTable, double[][]>();
//		
//		Random r = new Random();
//		
//		int q = 0;
//		for (int i = 0; i < 2; i++) {
//			for (int j = 0; j < 2; j++) {
//				for (int k = 0; k < 2; k++) {
//	
//					int a[] = {i,j,k};
//					SliceForHashTable slice = new SliceForHashTable(a);
//				
//	//				double[][] table = new double[2][2];
//	//				if (i==1) {
//	//					table[0][0] = 0;
//	//					table[1][0] = 0;
//	//					table[0][1] = 0;
//	//					table[1][1] = 0;
//	//				} else {
//	//					table[0][0] = k++;
//	//					table[1][0] = 0;
//	//					table[0][1] = k++;
//	//					table[1][1] = k++;
//	//				}
//					
//					double[][] table = {{q++,q++},
//		  		 						{q++,q++}};	
//	//				if (i!=1)
//					mapTables.put(slice, table);
//
//				}
//			}
//		}
//		
//		System.out.println(t);
//		System.out.println("------------------");
//		fit.imprimirTablas(mapTables);
//				
//		TripletTables tTables = new TripletTables(t,mapTables);
//		
//		// (0,2|1,3)
//		FastSet ftPrime = new FastSet(fit.n);
////		ftPrime.setAlltoOne();
////		ftPrime.flip(0);
////		ftPrime.flip(2);
//		TripletSets tPrime = new TripletSets(2,3,ftPrime);
//		
//		
//		CTCacheXYPivot cache_XY = new CTCacheXYPivot(fit.n);
//		
//		TripletTables tTablesPrime = fit.projectTable(cache_XY, tTables, tPrime);
//		
//		HashMap<SliceForHashTable, double[][]> mapTablesPrime = tTablesPrime.getContTables();
//
//		System.out.println("\n" + tTablesPrime.getTriplet());
//		System.out.println("------------------");		
//		fit.imprimirTablas(mapTablesPrime);
//	}

		
}
