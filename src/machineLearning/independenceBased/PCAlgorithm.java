package machineLearning.independenceBased;

import java.util.ArrayList;
import java.util.Random;

import machineLearning.DataStructures.DoublyLinkedList;
import machineLearning.Graphs.BN;
import machineLearning.Graphs.GraphsSimple.*;
import machineLearning.Utils.*;

public class PCAlgorithm implements IBasedAlgorithm{
	static boolean verbose = false;
	
	public IndependenceTest IT;
	int n;
	BN bn, bnPartial;
	UGraphSimple C;
	FastSet Sepset[][];
	Random r;
	public TestsAccuracy ta;
	
	public PCAlgorithm(IndependenceTest IT, int seed){
		this.IT = IT;
		this.ta = null;
		this.n = IT.n;
		this.Sepset = new FastSet[n][n];
		this.r = new Random(seed);
		
	}
	public PCAlgorithm(IndependenceTest IT, int seed, TestsAccuracy ta){
		this.IT = IT;
		this.ta = ta;
		this.n = IT.n;
		this.Sepset = new FastSet[n][n];
		this.r = new Random(seed);
		
	}
	/** Auxiliary function of run() that orient all possible edges
	 * until no new edge can be oriented.
	 *
	 */
	private void orientEdges(UGraphSimple C){
		//System.out.println(C);
		boolean somethingChanged = true;
		while(somethingChanged){
			somethingChanged = false;

			/** C) Orient some edges */
			for(int x=0; x<n; x++){
				for(int y=0; y<n; y++){
					if(x==y) continue;
					if(!C.existEdge(x,y)) continue;
					for(int z=0; z<n; z++){
						if(bn.isParent(y, x) || bn.isParent(y, z)) continue;
						if(bn.isParent(x, y) && bn.isParent(z, y)) continue;
						if(x == z || y == z) continue;
						if(C.existEdge(y, z) && !C.existEdge(x,z) && !Sepset[x][z].isMember(y)){
							bn.makeParent(x, y);
							bn.makeParent(z, y);
							somethingChanged = true;
							
						}
					}
				}
			}
			//System.out.println("After C)"+ bn);

			/** D.1) Orient rest of edges */
			for(int b=0; b<n; b++){
				for(int c=0; c<n; c++){
					if(b==c) continue;
					
					//if not oriented yet, try to orient
					if(!C.existEdge(b, c) || bn.isParent(c, b) || bn.isParent(b, c)) continue; //no undirected edge, or already oriented

					if((b == 3 && c == 4))
					{
						int u=23534;
					}

					a: for(int a=0; a<n; a++){
						if(a == b || a == c) continue;


						if(bn.isParent(a, b) && !C.existEdge(a, c)){
							if((b == 3 && c == 4))
							{
								int u=23534;
							}
							//Is there any arrowhead pointing at b?
							for(int w=0; w<n;w++){
								if(a==w || b==w || c==w) continue;
								if(bn.isParent(w, b)){
									continue a;
								}
							}
							bn.makeParent(b,c);
							somethingChanged = true;
						}
					}
				}
			}
			//System.out.println("After D.1)"+ bn);

			/** D.2) Orient rest of edges */
			//if there is a directed path from a to b, and an edge between a and b, oriented a-b as a->b
			//System.out.println("D.2)"+ bn);
			for(int a=0; a<n; a++){
				for(int b=0; b<n; b++){
					if(a==b) continue;
					
					//exists path?
					if(!bn.isParent(a, b) && !bn.isParent(b, a) && C.existEdge(a,b) && bn.pathExists(a, b)) {
						if((a == 0 && b == 2))
						{
							int u=23534;
						}
						bn.makeParent(a, b);
						somethingChanged = true;
					}
				}
			}
			//System.out.println("After D.2)"+ bn);

			boolean extraOrd = false;
			/** D.3 */
			if(extraOrd){
				for(int x=0; x<n; x++){
					for(int y=0; y<n; y++){
						if(x==y) continue;
						for(int z=0; z<n; z++){
							if(x==z || y==z) continue;
							
							if(C.existEdge(x, z) && C.existEdge(y,z)){
								for(int w=0; w<n; w++){
									if(x == w || y==w || z==w) continue;
									
									//if(!bn.isParent(z, w) && C.existEdge(z, w) && bn.isParent(x,w) && bn.isParent(y, w)){
									if(!bn.isParent(z, w) && C.existEdge(z, w) && bn.isParent(x,w) && bn.isParent(y, w)){
										if((z== 0 && w == 2))
										{
											int u=23534;
										}
										bn.makeParent(z, w);
										somethingChanged = true;
									}
								}
							}
						}
					}
				}
				//System.out.println("After D.3)"+ bn);
			}
			/** D.4 */
			if(extraOrd){
				for(int x=0; x<n; x++){
					for(int y=0; y<n; y++){
						if(x==y) continue;
						if(bn.isParent(x, y)){
							for(int z=0; z<n; z++){
								if(x==z || y==z) continue;
								
								if(bn.isParent(y, z)){
									for(int w=0; w<n; w++){
										if(x == w || y==w || z==w) continue;
										
										//if(C.existEdge(x, w) && !bn.isParent(x, w) && C.existEdge(y, w) && C.existEdge(z, w) && !bn.isParent(z, w)){
										if(C.existEdge(x, w) && C.existEdge(y, w) && C.existEdge(z, w) && !bn.isParent(z, w)){
											if((z== 0 && w == 2))
											{
												int u=23534;
											}
											bn.makeParent(z, w);
											somethingChanged = true;
										}
									}
								}
							}
						}
					}
				}
				//System.out.println("After D.4)"+ bn);
			}
		}
	}
	//----------------------------------------------------
	/** Returns an integer with two fields containing an undirected edge not yet oriented,
	 * if such edge exists, and null otherwise.
	 * 
	 */
	private int[] unorientedEdge(UGraphSimple C){
		int [] edge = new int[2];
		for(int x=0; x<n; x++){
			for(int y=0; y<n; y++){
				if(x==y) continue;
				
				//if not oriented yet, try to orient
				if(C.existEdge(x, y) && !bn.isParent(y, x) && !bn.isParent(x, y)) {
					edge[0] = x;
					edge[1] = y;
					return edge;
				}
			}
		}
		return null;
	}			
	//----------------------------------------------------
	/** Returns an ArrayList<int []> containing all edges not yet oriented	 */
	private ArrayList<int[]> unorientedEdges(UGraphSimple C, BN bn){
		ArrayList<int[]> edges = new ArrayList<int[]>();
		for(int x=0; x<n; x++){
			for(int y=x+1; y<n; y++){
				if(x==y) continue;
				
				//if not oriented yet, try to orient
				if(C.existEdge(x, y) && !bn.isParent(y, x) && !bn.isParent(x, y)) {
					int [] edge = {x,y};
					edges.add(edge);
					//int [] edge_symm = {y,x};
					//edges.add(edge_symm);
				}
			}
		}
		return edges;
	}			
	//----------------------------------------------------
	/**
	 * Returns the number of unshielded v structures it creats
	 */
	//private int createsUnshieldedVStructure(int x, int y, UGraphSimple C){
	/*final private boolean createsUnshieldedVStructure(int x, int y, UGraphSimple C){
		return createsUnshieldedVStructure(x, y, C, null);
	}*/
	final private boolean createsUnshieldedVStructure(int x, int y, UGraphSimple C, BN bn){
		if(bn == null) {
			Utils.todo("If this is never triggered, erase the whole if statement");
			bn  = this.bn;
		}
		
		//if(!C.existEdge(x, y)) return 0;
		if(!C.existEdge(x, y)) return false;
		
		//First looks for V structure
		int num=0;
		for(int z=0; z<n; z++){
			if(x==z || y==z) continue;
			
			if(bn.isParent(z, y)) {  //x->y<-z
				//if(!C.existEdge(x, z)) num++; //its unshielded
				if(!C.existEdge(x, z)) return true; //its unshielded
			}
		}
		return false;
	}
	//----------------------------------------------------
	private boolean createsViolation(int x, int y, BN bn, UGraphSimple C){
		if(createsUnshieldedVStructure(x, y, C, bn)){
			return true;
		}
		else{
			bn.makeParent(x,y);
			boolean cycleExists = bn.cycleExists(x);

			bn.removeEdge(x, y);
			return cycleExists;
		}
	}
	//----------------------------------------------------
	public BN orientRemainingEdges(){
		return bn=orientRemainingEdges(C, new BN(bnPartial), new ArrayList<int []>(unorientedEdges(C, bnPartial)));
	}
//	----------------------------------------------------
	private BN orientRemainingEdges(UGraphSimple C, BN bn, ArrayList<int []> remainingEdges){
		int x,y;
		//Termination condition?
		if(remainingEdges.size() == 0) return bn;
		
		while(remainingEdges.size() > 0){
			int randomNum = r.nextInt(remainingEdges.size());
			int randEdge [] = remainingEdges.remove(randomNum);
			
			for(int i=0; i<2; i++){
				x = i==0?randEdge[0]:randEdge[1];
				y = i==0?randEdge[1]:randEdge[0];
				
				if( createsViolation(x, y, bn, C)) {
					if(i==0)continue;
					else return null;
				}
				else {
					bn.makeParent(x, y);
					BN bnOut = orientRemainingEdges(C, new BN(bn), new ArrayList<int []>(remainingEdges));
					if(bnOut == null) {
						bn.removeEdge(x, y);
						if(i==0) continue;
						else return null;
					}
					else return bnOut;
				}
			}
		}
		return null;
	}
	//----------------------------------------------------
	public void run(){
		pFact pf;
		
		/** A) Form the complete undirected graph C on the vertex set */
		UGraphSimple C = new UGraphSimple(n);
		bn = new BN(n+"");
		
		for(int i=0; i<n; i++){
			for(int j=i+1; j<n; j++) C.addEdge(i,j);
		}
		
		/** B) iterate over all cardinalities of conditioning sets */
		for(int m=0; m<n; m++){
			if(verbose) System.out.println("m = " + m);
			
			/** Iterate over all edges */
			for(int x=0; x<n; x++){
				for(int y=0; y<n; y++) {
					if(x == y) continue;
					if(!C.existEdge(x, y)) continue;
					
					//System.out.println(C);
					if((x == 0 && y == 2) || (x == 2 && y == 0)){
						int asdf=0;
					}
					
					//Count number of edges of i
					FastSet neighbors = new FastSet(n);
					for(int k=0; k<n; k++) {
						if(C.adjMatrix[x][k] && k!=y) neighbors.add(k);
					}
					if(neighbors.cardinality() < m) continue;

					//if(verbose) System.out.println("\t(" + x +  "," +y + ")");

					ArrayList<FastSet> subsets = neighbors.getAllSubsets(m);
					if(neighbors.cardinality() == m) subsets.add(neighbors);
					subsets: for(FastSet S : subsets){
						Triplet triplet = new Triplet(x,y,S);
						pf = IT.independent(triplet);
						if(pf == null){
							//IT.independent(triplet);
						}
						if(ta != null) {
							if(pf.X().first() == 0 && pf.Y().first()==2 && pf.getCond().cardinality()==0){
								int asdfsa=0;
							}
							//if(ta.addFact(pf))
									//System.out.println(pf + ((pf.truth_value==ta.oracle.independent(pf.triplet).truth_value)?"CORRECT":"INCORRECT"));
						}
						if(pf.truth_value()){
							
							if((x == 0 && y == 2) || (x == 2 && y == 0)){
								int asdf=0;
							}
							if(false || verbose) System.out.println(pf);
							C.removeEdge(x,y);
							Sepset[x][y] = S;
							Sepset[y][x] = S;
							if(verbose) System.out.println("\t\tRemove edge \n" + C);
							break subsets;
						}
						else if(verbose) System.out.println(pf);

						
						if(false && x ==2 && y == 5 && S.isMember(4) && S.cardinality() == 1){//
							//if(Experiment.output.doPrint(coVL.PROOF) && !IT.getClass().getSimpleName().equalsIgnoreCase("AIT_Efficient2")) {
							if(true && !IT.getClass().getSimpleName().equalsIgnoreCase("AIT_Efficient2")) {							
				        		//IT.printProof((short)2, pf.negation() );
								IT.printProof((short)2, pf.negation());
				        	}
						}
					}
				}
			}
		}
		//System.out.println("Unoriented Network " + C);

		/*for(int x=0; x<n; x++){
			for(int y=x+1; y<n; y++) {
				System.out.println("Sepset["+x+"]["+y+"] = " + Sepset[x][y]);
			}
		}*/
	
		
		if(IT.directedTrueModel() != null){
			UGraphSimple trueModel = new UGraphSimple(IT.directedTrueModel());
			UGraphSimple outputModel = C;
			if(false && !Utils.FORCED_ASSERT(outputModel.hamming(trueModel) == 0, "ERROR IN PC-ALGORITHM, PART (B)")) {
				System.out.println("True Undirected Model \n" + trueModel.toString() + "\nOutput Undirected Model \n" + outputModel.toString());
				System.exit(-1);
			}
		}
		
		int[] edge;
		
		orientEdges(C);
		
		bnPartial = new BN(bn);
		
		bn = orientRemainingEdges(C, new BN(bn), new ArrayList<int []>(unorientedEdges(C,bn)));
		
		this.C = C;

		//return bn;
	}
	//	-------------------------------------------
	public Object output(){return bn;};
	public BN outputBN(){return bn;};
	public BN outputBNPartial(){return bnPartial;};
	public UGraphSimple outputSimple(){return C;};
	//	-------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String dir = "z:/ait/experiments/experiment_sampled_BN/datasets/";

		BN bn;
		dSeparation ds;
		PCAlgorithm pc;
		
		//System.exit(1);

		Random rand = new Random(3434);
		
		int n_array[] = { 6, 8, 10, 12 };
		int twidth_array[] = { 3, 4,5 };
		//int n_array[] = { 6};
		//int twidth_array[] = { 4};
		int numRepetitions = 10;
		for (int i = 0; i < n_array.length; i++) {
			for (int j = 0; j < twidth_array.length; j++) {
				int n = n_array[i];
				int twidth = twidth_array[j];
				for (int r = 1; r <= numRepetitions; r++) {
					//if(r != 9) continue;
					String name_base = "_N" + n + "_D" + twidth + "_R" + r;
					String networkFile = "bn" + name_base + ".txt";
					
					bn = new BN(dir + networkFile);
					ds =  new dSeparation(bn);
					pc = new PCAlgorithm(ds, 123456, null);

					if(n==6 && twidth == 5 && r==7){
						int asdf=23;
					}
					pc.run();
					
					//Undirected Accuracy
					UGraphSimple pcout = new UGraphSimple(pc.outputSimple());
					UGraphSimple trueSimple = new UGraphSimple(bn);
					//System.out.println("Output network = " + pcout + "\nTrue Network = " + trueSimple);
			        VertexSeparation pcTestV = new VertexSeparation(pcout);
			        VertexSeparation trueModelTestV = new VertexSeparation(trueSimple);
			        TestsAccuracy accPCAITV = new TestsAccuracy(pcTestV, trueModelTestV, bn.size()-2, 2000, rand);
					System.out.print("Accuracy for " + networkFile + " (Undirected) =  " + accPCAITV.accuracy() );

					//Directed accuracy
					//System.out.println("Output network = " + pc.output() + "\nTrue Network = " + bn);
			        dSeparation pcTest = new dSeparation(pc.outputBN());
			        dSeparation trueModelTest = new dSeparation(bn);
			        TestsAccuracy accPCAIT = new TestsAccuracy(pcTest, trueModelTest, bn.size()-2, 2000, rand);

			        
					System.out.print("  (directed) = " + accPCAIT.accuracy() + "\n");
					//Utils.FORCED_ASSERT((new UGraphSimple(pc.bn)).hamming(new UGraphSimple(bn)) == 0, "ERROR IN PC-ALGORITHM");
				}
			}
		}

	}
	public DirectedGraph getOutputNetwork() {
		// TODO Auto-generated method stub
		return null;
	}

}
