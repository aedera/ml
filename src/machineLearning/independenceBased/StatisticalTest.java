package machineLearning.independenceBased;
/*****************************************************************************
 * 
 * Statistical test abstract class.
 * 
 * (C) 2006 Dimitris Margaritis, All Rights Reserved.
 * 
 ****************************************************************************/

import java.io.*;
import java.util.*;

abstract class StatisticalTest {

  // Number of tests executed. Can be reset to zero externally.
  public int num_tests = 0;

  // Number of WEIGHTED tests executed. Can be reset to zero externally. The
  // weight of a test is the number of variables it involves.
  public int num_weighted_tests = 0;

  // Number of variables in the domain.
  public int nvars;

  // Size of "dataset" i.e., number of tuples in it.
  public int ndata;

  // nvals[var] is the number of values of variable "var".
  public int[] nvals;
  
  // Data set associated with this test.
  public int[][] dataset;
  
  //Data points to use. The size of this is "ndata".
  private boolean[] use_datapoints;

  //Print info messages or not.
  public boolean verbose;

  /**
   *
   * Constructor.
   *
   */
  public StatisticalTest(String datafile) {
    try {
      readDataSet(datafile);
    } catch (IOException e) {
      System.err.println("Error reading data file \"" + datafile + "\": " + e);
      System.exit(1);
    }
    assert ndata > 0 : "Dataset has no data";
    use_datapoints = new boolean[ndata];
    Arrays.fill(use_datapoints, true); // Use all datapoints by default.
    verbose = false;
  }

  /**
   * Set the "use_datapoints" array to use only a subset of the dataset.
   * 
   */
  public void setDatapointsToUse(boolean[] dataset_subset) {
    assert use_datapoints.length == ndata;
    use_datapoints = dataset_subset.clone();
  }

  /**
   *
   * Variable to String.
   *
   */
  protected static String varname(int X) {
    return "" + (X + 1);
  }

  /**
   * 
   * Print a set represented by a boolean[] array.
   * 
   */
  protected static String setToString(boolean[] S) {
    boolean first_var = true;
    String str = "{";
    for (int X = 0; X < S.length; X++) {
      if (S[X]) {
        if (first_var)
          first_var = false;
        else
          str += ",";
        str += " " + varname(X);
      }
    }
    str += " }";
    return str;
  }

  /**
   * 
   * Count the number of members of a set represented by a boolean[].
   * 
   */
  protected static int setNumMembers(boolean[] S) {
    int nmembers = 0;
    for (int X = 0; X < S.length; X++)
      if (S[X]) nmembers++;

    return nmembers;
  }

  /**
   * 
   * Independence test. Returns true/false.
   * 
   * Also, a function for returning the strength of the last independence test,
   * which depends on the test implementation.
   * 
   */

  //
  // Conditional test. Must be defined by the concrete subclass.
  //
  abstract public boolean areIndep(boolean[] X, boolean[] Y, boolean[] Z);

  //
  // Unconditional test.
  //

  public boolean areIndep(boolean[] X, boolean[] Y) {
    return areIndep(X, Y, new boolean[nvars]);
  }

  //
  // Conditional test for X, Y single variables.
  //

  public boolean areIndep(int X, int Y, boolean[] Z) {
    boolean[] Xset = new boolean[nvars];
    Xset[X] = true;
    boolean[] Yset = new boolean[nvars];
    Yset[Y] = true;
    return areIndep(Xset, Yset, Z);
  }

  //
  // Unconditional test for X, Y single variables.
  //

  public boolean areIndep(int X, int Y) {
    boolean[] Xset = new boolean[nvars];
    Xset[X] = true;
    boolean[] Yset = new boolean[nvars];
    Yset[Y] = true;
    return areIndep(Xset, Yset, new boolean[nvars]);
  }

  //
  // Function for returning the strength of the last independence test.
  //

  abstract public double lastTestIndepStrength();

  /**
   * 
   * Return the number of configurations of a set S of variables.
   * 
   */

  protected int numConfigs(boolean[] S) {
    int n = 1;
    for (int i = 0; i < S.length; i++)
      if (S[i]) n *= nvals[i];

    return n;
  }

  /**
   * 
   * Utility function: Create the joint 2D histogram of sets X and Y for every
   * value of Z from the dataset.
   * 
   */
  protected int[][] makeHistogramFromDataset(boolean[] X, boolean Y[],
      boolean[] Z) {
    int nvals_X = numConfigs(X);
    int nvals_Y = numConfigs(Y);
    int nvals_XY = nvals_X * nvals_Y;
    int nvals_Z = numConfigs(Z);
    int[][] counts = new int[nvals_Z][nvals_XY];

    for (int i = 0; i < ndata; i++) {
      if (!use_datapoints[i]) continue;
      int index_Z = dataToIndex(i, Z);
      int index_X = dataToIndex(i, X);
      int index_Y = dataToIndex(i, Y);
      int index_XY = index_Y * nvals_X + index_X;
      counts[index_Z][index_XY]++;
    } // end for (i)

    return counts;
  }

  /**
   * Utility function: Create the 1D histogram of set X for every value of Z
   * from the dataset.
   */
  protected int[][] makeHistogramFromDataset(boolean[] X, boolean[] Z) {
    int nvals_X = numConfigs(X);
    int nvals_Z = numConfigs(Z);
    int[][] counts = new int[nvals_Z][nvals_X];

    for (int i = 0; i < ndata; i++) {
      if (!use_datapoints[i]) continue;
      int index_Z = dataToIndex(i, Z);
      int index_X = dataToIndex(i, X);
      counts[index_Z][index_X]++;
    } // end for (i)

    return counts;
  }

  /**
   * 
   * Convert a configuration of a set of variables (taken from a dataset row) to
   * an index. The row is given as the index "row". Only variables from the set
   * "X", represented as a boolean[] array, are used. Each variable X[i] in the
   * domain takes nvals[i] values.
   * 
   */
  protected int dataToIndex(int row, boolean[] X) {
    assert nvars == X.length : "\"X\" does not contain " + ndata
        + " variables.";

    int index = 0;
    for (int i = 0; i < X.length; i++) {
      if (X[i]) index = index * nvals[i] + dataset[row][i];
    }

    return index;
  }

  /**
   * 
   * Read a data file and store it in the dataset tabular 2-D array of tuples.
   * 
   */
  private void readDataSet(String datafile) throws IOException {
    List<ArrayList<String>> lines = new ArrayList<ArrayList<String>>();
    try {
      BufferedReader data_reader;
      if (datafile.equals("-")) { // Read from System.in
        data_reader = new BufferedReader(new InputStreamReader(System.in));
      } else {
        data_reader = new BufferedReader(new FileReader(datafile));
      }
      Map<String,String> words = new HashMap<String,String>();
      String line_string;
      while ((line_string = data_reader.readLine()) != null) {
        StringTokenizer line_tokens = new StringTokenizer(line_string);
        ArrayList<String> line = new ArrayList<String>();
        while (line_tokens.hasMoreTokens()) {
          String word = line_tokens.nextToken();
          if (words.get(word) == null) {
            words.put(word, word); // Word string never seen before.
          } else {
            word = words.get(word); // Use old string for this word.
          }
          line.add(word);
        }
        lines.add(line);
      }
      data_reader.close();
    } catch (IOException e) {
      System.err.println(e);
      System.exit(1);
    }
    ndata = lines.size();

    // Check if all lines have the same length (equal to nvars).
    nvars = -1;
    for (int i = 0; i < ndata; i++) {
      List<String> line = lines.get(i);
      if (nvars == -1) {
        nvars = line.size();
      } else {
        if (line.size() != nvars) {
          System.err.println("Uneven line \"" + line + "\". Exiting.");
          System.exit(1);
        }
      }
    }

    // Convert Strings to unique integer codes.
    dataset = new int[ndata][nvars];
    List<Map<String,Integer>> varValue2id = new ArrayList<Map<String,Integer>>(
        nvars);
    for (int j = 0; j < nvars; j++)
      varValue2id.add(new HashMap<String,Integer>(50));
    nvals = new int[nvars];
    for (int i = 0; i < ndata; i++) {
      List<String> line = lines.get(i);
      for (int j = 0; j < nvars; j++) {
        String valueString = line.get(j);
        if (varValue2id.get(j).get(valueString) == null) {
          varValue2id.get(j).put(valueString, nvals[j]);
          nvals[j]++;
        }
      } // end for (j)
    } // end for (i)
    for (int i = 0; i < ndata; i++) {
      List<String> line = lines.get(i);
      for (int j = 0; j < nvars; j++) {
        String valueString = line.get(j);
        Integer v = varValue2id.get(j).get(valueString);
        assert v != null : "String \"" + valueString + "\" not in hash "
            + "table, error.";
        dataset[i][j] = v;
      } // end for (j)
    } // end for (i)
  }

  /**
   *
   * Utility function: Print a 1D histogram.
   *
   */
  protected static void print1DHistogram(int[] hist) {

    System.out.print("[ ");
    for (int i = 0; i < hist.length; i++) {
      System.out.print(hist[i] + " ");
    } // end for (i)
    System.out.println("]");
  }
}

/** ************************************************************************ */
