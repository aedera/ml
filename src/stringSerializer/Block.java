package stringSerializer;

import java.util.*;
import java.io.*;
import java.lang.reflect.*;

import stringSerializer.*;

/**
*
* <p>Title: Block</p>
* <p>Description: A block corresponds to an object's parameter.<!-- --> It's grammar is as follows:<p>
 * <p> <b>[block] </b>        ::= [pd][tag][pd] { [tagged_value] }*  [ed][tag][ed] <p>
* <p><b>[tagged_value]</b>  ::= [vd] [pd][value_tag][pd]   [value]   [ed][value_tag][ed] [vd] <p>
* <p><b>[pd]</b> ::= <b>[ed]</b> ::= <b>[vd]</b> ::= [Java String] //Are delimiter Strings (e.g. ZZZ, XXX). <p>
* <p><b>[tag]</b> ::=  [Java String]  //which serves to delimite the block. <p>
* <p><b>[value_tag]</b> ::= [Java String] //used to delimite the value. <p>
 * <p><b>[value]</b> ::= [Java String] OR [Java double] OR [object] <p>
 *
 * See Parser.java for a complete description of the grammar.
 </p>
* @author Facundo Bromberg
* @version 1.0
*/
public class Block
{
 public String tag;
 Vector typedValues;

 public Block()
 {
   typedValues = new Vector();
 }
 public Block(String _tag)
 {
   tag = _tag;
   typedValues = new Vector();
 }

 public Block(String _tag, Object _value, Parser parser)
  {
    tag = _tag;
    typedValues = new Vector();
    ImySerializable _value2 = (ImySerializable) _value;
    String _value3 = _value2.mySerialize(parser);
    typedValues.addElement(new typedValue(_value3));
  }

 public Block(String _tag, String _value)
  {
    tag = _tag;
    typedValues = new Vector();
    typedValues.addElement(new typedValue(_value));
  }

  public Block(String _tag, double _value)
   {
     tag = _tag;
     typedValues = new Vector();
     typedValues.addElement(new typedValue(_value));
   }

 public void addValue(String _value)
 {
   typedValues.addElement(new typedValue(_value));
 }

 public void addValue(double _value)
 {
   typedValues.addElement(new typedValue(_value));
 }

   public void addValue(Object _value, Parser parser)
   {
       ImySerializable _value2 = (ImySerializable) _value;
       String _value3 = _value2.mySerialize(parser);
       typedValues.addElement(new typedValue(_value3));
  }



 public String getString(int i)
 {
   typedValue tv = (typedValue) typedValues.get(i);
   return tv.string_value;
 }

 public double getDouble(int i)
{
  typedValue tv = (typedValue) typedValues.get(i);
  return tv.double_value;
}
/* public void getObject(int i, ImySerializable object, Parser parser)
 {
   typedValue tv = (typedValue) typedValues.get(i);

   object.myUnserialize(tv.string_value, parser.getpd(), parser.geted(), parser.getvd(), parser.getsd());

 }
*/
public ImySerializable getObject(int i, Object dummyObj, Parser parser)
{
  typedValue tv = (typedValue) typedValues.get(i);

  ImySerializable object = null;

  try {
//    Class cls = Class.forName(className, false, null);
  //  object = (ImySerializable) cls.newInstance();
      object = (ImySerializable) dummyObj.getClass().newInstance();

  }
//  catch (ClassNotFoundException ex) {
  //}
  catch (IllegalAccessException ex) {
  }
  catch (InstantiationException ex) {
  }


  object.myUnserialize(tv.string_value, parser);

  return object;
}

/**
 * Used for object that are cotainer (for example ArrayD that may contain Integer, Doubles, discretePro abilityDistribution, etc).
 */
public ImySerializableContainer getContainerObject(int i, Object dummyObj, Object containedObj, Parser parser)
{
  typedValue tv = (typedValue) typedValues.get(i);

  ImySerializableContainer object = null;

  try {
//    Class cls = Class.forName(className, false, null);
  //  object = (ImySerializable) cls.newInstance();
      object = (ImySerializableContainer) dummyObj.getClass().newInstance();

  }
//  catch (ClassNotFoundException ex) {
  //}
  catch (IllegalAccessException ex) {
  }
  catch (InstantiationException ex) {
  }


  object.myUnserialize(tv.string_value, containedObj, parser);

  return object;
}

 public int size() {return typedValues.size();};

 /**
   * Unparse block returns a string block from a object Block.
   */
  public String unparseBlock(String pd, String ed, String vd, String sd)
  {
    String ret = pd + sd;
    ret += tag + sd;
    ret += pd + sd;

    for(int i=0; i<typedValues.size() ; i++)
    {
      typedValue tv = (typedValue)typedValues.get(i);
      ret += tv.unParseTypedValue(pd, ed, vd, sd, tag, i);
    }
    ret += ed + sd;
    ret += tag + sd;
    ret += ed + sd;

    return ret;
  }

}
