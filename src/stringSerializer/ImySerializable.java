package stringSerializer;



/**
 *
 * <p>Title: Interface for my serializer.<!-- --> A class should be implement this interface
 * in order to be able to serialize the state of an object of that class.</p>
 * @author Facundo Bromberg
 * @version 1.0
 */
public interface ImySerializable
{
  /**
      * Unparse to my format, this procedure works paired with myUnserialize.<!-- -->
      * The output string stores the current state of the calling object. This
      * string can be used later to set the state of another object to the same state
      * of the original object.<!-- --> Note that this is not storing objects, but only their
      * state (current value of internal fields).
      * @return a String.
      */
  public String mySerialize(Parser parser);


  /**
           * Parse to my format, this procedure works paired with mySerialize.<!-- -->
           * The string output from this procedure is used to change the state of
           *  internal
           * field values of the current calling object.
           * @return a String.
           */
public void myUnserialize(String inputStr, Parser parser);
}
