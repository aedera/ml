package stringSerializer;


import java.util.*;
import java.io.*;

import stringSerializer.*;
/**
 *
 * <p>Title: Parser for object' state serialization.</p>
 * <p>Description: Parse and Unparse considering the following grammar:
 * <p> <b>[object] </b>        ::= { [block] }*    //(concatenation of blocks) <p>
 * <p> <b>[block] </b>        ::= [pd][tag][pd] { [tagged_value] }*  [ed][tag][ed] <p>
 * <p><b>[tagged_value]</b>  ::= [vd] [pd][value_tag][pd]   [value]   [ed][value_tag][ed] [vd] <p>
 * <p><b>[pd]</b> ::= <b>[ed]</b> ::= <b>[vd]</b> ::= [Java String] //Are delimiter Strings (e.g. ZZZ, XXX). <p>
 * <p><b>[tag]</b> ::=  [Java String]  //which serves to delimite the block. <p>
 * <p><b>[value_tag]</b> ::= [Java String] //used to delimite the value. <p>
 * <p><b>[value]</b> ::= [Java String] OR [Java double] OR [object] <p>

 *
 * YES! Nesting is allowed, i.e. you can set as a value another object.<!-- --> Also
 * notice you can add more than one value (Kleene star above), which means you can
 * parse container object fields as Vector, arrays, LinkedLists, etc.
 *
 *   </p>
 * @author Facundo Bromberg
 * @version 1.0
 */
public class Parser
{
  /** parameter delimiter, used to delimite the name of the parameter in the beggining of a block*/
  protected String pd;

  /** parameter delimiter, used to delimite the name of the parameter at the end of the block*/
  protected String ed;

  /** value delimiter, used to delimite the name of the value for the parameter.*/
  protected String vd;

  /** space delimiter, used to delimite the spaces between strings and delimiters (besides spaces).*/
  protected String sd;

  /** Internally used tokenizer */
  private StreamTokenizer tokenizer;

  /**
   * No argument constructor, assigns default values to the delimiters.
   */
  public Parser ()
  {
    pd = ":";
    ed = "|";
    vd = "&";
    sd = ".";

}

  /**
   * Constructor, note that all the delimiters are assumed of size one, i.e. only one char.
   * @param _pd parameter delimiter, used to delimite the name of the parameter in the beggining of a block
   * @param _ed parameter delimiter, used to delimite the name of the parameter at the end of the block.
   * @param _vd value delimiter, used to delimite the name of the value for the parameter.
   */
  public Parser (String _pd, String _ed, String _vd, String _sd)
  {
    pd = _pd;
    ed = _ed;
    vd = _vd;
    sd = _sd;

  }

  public String getpd(){return pd;};
  public String geted(){return ed;};
  public String getvd(){return vd;};
  public String getsd(){return sd;};

  /** Initialize the string tokenizer.*/
  StreamTokenizer getTokenizer(String str)
  {
    BufferedReader reader = null;
    try{
      reader = new BufferedReader(new StringReader(str));
    }
    catch(Exception ioe){}

    StreamTokenizer tokenizer = new StreamTokenizer(reader);

    tokenizer.resetSyntax();
//    tokenizer.whitespaceChars(' ', ' ');
    tokenizer.wordChars('A','Z');
    tokenizer.wordChars('a','z');
    tokenizer.wordChars('0','9');
    tokenizer.wordChars('.','.');
    tokenizer.wordChars('-','-');
    tokenizer.wordChars(' ',' ');
    tokenizer.wordChars(pd.charAt(0),pd.charAt(0));
    tokenizer.wordChars(ed.charAt(0),ed.charAt(0));
    tokenizer.wordChars(vd.charAt(0),vd.charAt(0));
    tokenizer.wordChars(' '+1,'\u00FF');

//    tokenizer.whitespaceChars(',', ',') ;
  tokenizer.whitespaceChars(sd.charAt(0), sd.charAt(0)) ;

    tokenizer.eolIsSignificant(false);

    return tokenizer;
  }

  /** parses the input string, returns a vector of Blocks */
  public Vector parse(String string2parse)
  {
    tokenizer = getTokenizer(string2parse);
    nextToken();


    Vector bvector = new Vector();
    Block blk = parseNextBlock();

    while(blk != null)
    {
      bvector.addElement(blk);
      blk = parseNextBlock();
    }

    return bvector;
 }

 /** parses the next token (trimmed lists, or simply words).
  * <!-- --> If called after startParser, it parses the first token in the input string */
 protected void nextToken()
 {
   try {tokenizer.nextToken();}
   catch (Exception te){}
 }

 /**
  * Unparse a vector of blocks.
  */
 public String unparseBlocks(Vector vblock)
 {
   String ret = "";
   for(int i=0; i<vblock.size() ; i++)
   {
     Block blk = (Block)vblock.get(i);
     ret += blk.unparseBlock(pd, ed, vd, sd);
   }

   return ret;
 }


 /**
  * parses the next block in the tokenizer.
  * @return the block info in a Block object.
  */
 private Block parseNextBlock()
 {
   if(tokenizer.ttype == tokenizer.TT_EOF) return null;

   delimitedTag dt = getNextTag();

   Block blk = new Block(dt.tag);

 //  String value = getNextValueString();
 //  blk.typedValues.addElement(parseTypedValue(value));

   //Reads value;
   while(tokenizer.sval.equals(vd))
   {
     String value = getNextValueString();
     blk.typedValues.addElement(parseTypedValue(value));
   }
   getNextTag();
   return blk;
 }

 /**
  * Parses value strings consisting of <vd> <pd> <valueTag> <pd> <value> <ed> <valueTag> <ed> <vd>
  * @return
  */
 String getNextValueString()
 {
   nextToken(); //read vd;
   delimitedTag dt_start = getNextTag();

   String ret = tokenizer.sval;
   nextToken();
   while(true)
   {
     if(tokenizer.sval.equals(ed))
     {
       delimitedTag dt2 = getNextTag();
       if(!dt2.tag.equals(dt_start.tag)) {
         String aux = sd+dt2.toString(sd);
         ret += aux;
       }
       else {
         break;
       }
     }
     else {
       ret += sd+tokenizer.sval;
       nextToken();
     }

   }
   nextToken();  //reads final vd.

   return ret;
 }

 delimitedTag getNextTag()
 {
   delimitedTag dt = new delimitedTag();
   dt.type = tokenizer.sval;
   nextToken();
   dt.tag = tokenizer.sval;
   nextToken();
   nextToken();

   return dt;
 }

 typedValue parseTypedValue(String value)
 {
   value = value.trim();
   typedValue tv = new typedValue();

   StreamTokenizer tokenizer2 = getTokenizer(value);
   tokenizer2.parseNumbers();

   try {tokenizer2.nextToken();}
   catch (Exception te){}

   String aux="";

   if(tokenizer2.ttype == tokenizer2.TT_NUMBER)
   {
     tv.type = "NUMBER";
     tv.double_value = tokenizer2.nval;

   }

   //else it is of type "STRING";
   else
   {
     tv.type = "STRING";
     tv.string_value = value;
   }
   return tv;
 }

 /**
  * Filter any appearence of orig with dest in inputStr.
  */
/* public String filter(String inputStr, String orig, String dest)
 {
   String ret = new String(inputStr);

//   orig = "--";
  //  ret = "FACU---DI---TO";
//   String aux5 = ret.substring(0,4);
//   String aux2 =  aux5+ ret.substring(4, ret.length());
  String aux6="";
   for(int i=0; i<ret.length(); i++)
   {
     //Find match for orig
     int k=0;
     boolean bingo = false;
     char kkk = ret.charAt(i);
     while(ret.charAt(i) == orig.charAt(k))
     {
       k++;
       i++;
       if(k >= orig.length()) {
         //aux6 = ret.substring(i-15);
         kkk=kkk;
         bingo = true;
         break;
       }
       if(i >= ret.length()) {
         bingo = false;
         break;
       }
     }
     if(bingo) {
       String aux = ret.substring(0, i-k) + dest +
           ret.substring(i, ret.length());
       i=i-(orig.length() - dest.length());
       //String aux7 = aux.substring(i-15);
       //aux6 = aux6;
       //kkk=kkk;
       ret = aux;
     }
   }
   return ret;

// return inputStr.replaceAll(orig, dest);
 // return inputStr.replace(orig, dest);
 }

 /**
  * Filter any appearence of orig with dest in inputStr.
  */
 public String filter(String inputStr, String orig, String dest)
 {
   String ret = new String(inputStr);

   int orig_size = orig.length();
   for(int i=0; i<ret.length()-(orig_size-1); i++)
   {
     String retSub = ret.substring(i, i+orig_size);
     if(orig.equals(retSub)){
       //Found match, we then replace it.
       ret = ret.substring(0, i) + dest + ret.substring(i+orig_size, ret.length());
       i+= dest.length()-1;
     }
   }
  return ret;

// return inputStr.replaceAll(orig, dest);
 // return inputStr.replace(orig, dest);
 }

 /** Used to post filter the String output by parser, usually overrided by sub clases. */
 public String filter(String inputStr){return inputStr;};

 /** Used to pre filter the String input to unparser, usually overrided by sub clases. */
 public String unfilter(String inputStr){return inputStr;};
}


/**
* <p>Title: Delimited Tag</p>
* <p>Description: It's a tag but delimited by pd-sd-tag-sd-pd or ed-sd-tag-sd-ed</p>
* @author Facundo Bromberg
* @version 1.0
*/
class delimitedTag
{
 String tag;
 String type;

 String toString(String sd)
 {
   String str = "";
   str += type + sd;
   str += tag + sd;
   str += type + sd;

   return str;
 }
}

/**
*
* <p>Title: typedValue</p>
* <p>Description: Internal class</p>
* <p>Copyright: Copyright (c) 2003</p>
* @author Facundo Bromberg
* @version 1.0
*/
class typedValue
{
  public String type;
  public String string_value;
  public double double_value;

  public typedValue()
  {
    super();
  }
  typedValue(String value)
  {
    type = "STRING";
    string_value = value;
  }

  typedValue(double value)
  {
    type = "NUMBER";
    double_value = value;
  }

  public String toString()
  {
    if(type.equals("NUMBER"))
    {
      Double dbl = new Double(double_value);
      return dbl.toString();
    }
    return string_value;
  }

  public String unParseTypedValue(String pd, String ed, String vd, String sd, String block_tag, int value_index)
  {
    String str = "";

    str += vd + sd ;
    str += pd + sd;
    str += block_tag + value_index + sd;
    str += pd + sd;

    str += toString() + sd;

    str += ed + sd;
    str += block_tag + value_index + sd;
    str += ed + sd;
    str += vd + sd;


    return str;
  }


}
