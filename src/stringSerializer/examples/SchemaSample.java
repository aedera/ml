package stringSerializer.examples;


import java.util.*;
import java.io.*;

import stringSerializer.*;
/**
 * Class Schema
 *
 * Implements the functionality of a Schmea with the attributes list;
 * The whole structure is inspired both by a personal analysis and design of the functionality of datasets
 * and by knowledge of WEKA environment.<!-- --> But, no line of code was copied
 * from WEKA
 *
 * @author Facundo Bromberg
 * @version 1.1 Actualized for the use in Final Project (BayesNetworks inference) of CS572, AI, Fall 2002.
 * @version 1.0 Splitted from Dataset for final project CS561 Spring 2003.
 */
public class SchemaSample implements ImySerializable
{
	public String name;
	protected Vector attributes;

	public SchemaSample(){
		name = "noMame";
		attributes = new Vector(0, 5);
	}
	public SchemaSample(String _name){
		name = _name;
		attributes = new Vector(0, 5);
	}

	/**
	 * Method that return the index of a value of the attr_index-th attribute.
	 */
	int getValueIndex(String value, int attr_index)
	{
	    AttributeSample attr = getAttribute(attr_index);
	    return attr.getValueIndex(value);
	}

	/**
	 * Procedure that returns a String representation of the Schema.
	 */
	public String toString()
	{
	    String Names ="";

	    Names = "Names Definitions: \n";
	    for(int i=0; i<attributes.size(); i++)
	    {
		AttributeSample attr = (AttributeSample) attributes.elementAt(i);
		Names = Names + attr.toString() + "\n";
	    }
	    return Names;
	}
        /**
         * Make Copy creates a new dataset but do not add a single instance. The instances linked list is empty for the new created dataset.
         */
        public SchemaSample makeACopy()
        {
	    SchemaSample newD = (SchemaSample) new SchemaSample(name);

	    //Copy attributes Vector.
	    for(int i=0; i<attributes.size(); i++)
	    {
	      AttributeSample attr = getAttribute(i);
	      newD.addAttribute(attr);
	    }
	    return newD;
        }


	/**
	 * returns the number of attributes in the dataset.
	 */
	public int numberOfAttributes()
	{
		return attributes.size();
	}

	/**
	 * Adds an attribute to the dataset.
	 */
	public void addAttribute(AttributeSample n_attr){
		attributes.addElement(n_attr);
	}

	/**
	 * Retrieves the index-th attribute from the dataset.
	 */
	public AttributeSample getAttribute(int index){
		return (AttributeSample) attributes.elementAt(index);
	}

        /**
         * Retrieves the attribute named 'name'.
         */
        public AttributeSample getAttribute(String name)
        {
          int i=0;
          for(i=0; i<numberOfAttributes(); i++)
          {
            if(name == getAttribute(i).name())
              break;
          }
                return (AttributeSample) attributes.elementAt(i);
        }

        /**
         * Retrieves the index of the attribute named 'name'.
         */
        public int getAttributeIndex(String name)
        {
          int i=0;
          for(i=0; i<numberOfAttributes(); i++)
          {
            if(name.equals(getAttribute(i).name()))
              break;
          }
                return i;
        }

	/**
	 * Set mark = false for all attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
	 */
        public void unmarkAllAttributes()
        {
          for(int i=0; i<attributes.size(); i++)
          {
            AttributeSample attr = getAttribute(i);
            attr.unmark();
          }
        }

        /**
         * Set mark = false for the i-th attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
         */
        public void markAttribute(int i)
        {
          AttributeSample attr = getAttribute(i);
          attr.mark();
        }

        /**
         * Return the mark flag of the i-th attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
         */
        public boolean marked(int i)
        {
          AttributeSample attr = getAttribute(i);
          return attr.marked();
        }

        /**
         * Return true if all attributes are marked (except the last one, which doesn't mater, recall it is the class attribute).
         */
        public boolean allMarked()
        {
          boolean ret = true;

          for(int i=0; i<attributes.size(); i++)
          {
            AttributeSample attr = getAttribute(i);
            if(!attr.marked()) ret = false;
          }
          return ret;
        }

        /**
         * Returns a class attribute value. It is assumed that the class attribute is the last attribute.
         * Input: the value index.
         * Output: The index-th value of the class attribute (last one).
         */
        public String getClassValue(int index)
        {
          AttributeSample attr = getAttribute(attributes.size()-1);
          return attr.getValueName(index);
        }


	/** This function receives a string version of the Names File and
	 * convert it to the internal representation, that is, it creates
	 * the corresponding attribute structure.<!-- --> It assumes a .arff file format.
	 */
	public void parseNamesFile(String FileName)
	{
	    BufferedReader reader = null;
	    try{
		reader = new BufferedReader(new FileReader(FileName));
	    }
	    catch(Exception ioe){}

	    //int linePos = 0;
	    AttributeSample n_attr;
	    String attrName, token;
	    StreamTokenizer tokenizer = new StreamTokenizer(reader);

	    tokenizer.resetSyntax();
	    tokenizer.whitespaceChars(0, ' ');
	    tokenizer.wordChars(' '+1,'\u00FF');
	    tokenizer.whitespaceChars(',',',');
	    tokenizer.whitespaceChars('.','.');
	    tokenizer.commentChar('|');
	    tokenizer.eolIsSignificant(true);

	    try {tokenizer.nextToken();}
	    catch (Exception te){}
	    token = tokenizer.sval;

	    //Skips the first line that contains "ClassField."
	    while(tokenizer.ttype != tokenizer.TT_EOL){
		try{tokenizer.nextToken();}
		catch (Exception te){}
		token = tokenizer.sval;
	    }

	    //Reads until end of file
	    //Each iteration of the loop is basically parsing one line
	    //That is, at the beggining of the loop, the tokenizer should
	    //point to the first pointer in the line
	    while(tokenizer.ttype != tokenizer.TT_EOF)
	    {
		//Reads attribute name.
		attrName = "PEPE";
		attrName = token = "";
		while(token.indexOf(':') == -1){
		    try{tokenizer.nextToken();}
		    catch (Exception te){}
		    token = tokenizer.sval;

		    attrName = attrName + " " + token;
		}
		attrName = attrName.substring(1, attrName.length()  -1 );
		//                  attrName = attrName + token;
		n_attr = new AttributeSample(attrName);

		try{tokenizer.nextToken();}
		catch (Exception te){}
		token = tokenizer.sval;


		while(tokenizer.ttype != tokenizer.TT_EOL && tokenizer.ttype != tokenizer.TT_EOF)
		{

		    if(token.equalsIgnoreCase("date") ||
		       token.equalsIgnoreCase("time") ||
		       token.equalsIgnoreCase("ignore") ||
		       token.equalsIgnoreCase("label") 	//AttributeSample dont used for classification.
		       )
		    {
			n_attr.addValue("ignore");
			n_attr.ignore(true);

			try{tokenizer.nextToken();}
			catch (Exception te){}
			token = tokenizer.sval;

			continue;
		    }
		    //Take care of linear/continuous case.
		    //It will be processed later, when the dataset is readed.
		    //This is because we need min and max.
		    if(token.equalsIgnoreCase("linear")) {
			n_attr.addValue("linear");

			try{tokenizer.nextToken();}
			catch (Exception te){}
			token = tokenizer.sval;
			continue;
		    }
		    n_attr.addValue(token);

		    try{tokenizer.nextToken();}
		    catch (Exception te){}
		    token = tokenizer.sval;
		}
		addAttribute(n_attr);

	    }
	}

        /**
     * Unparse to my format, this procedure works paired with myUnserialize.<!-- -->
     * The output string stores the current state of the calling object. This
     * string can be used later to set the state of another object to the same state
     * of the original object.<!-- --> Note that this is not storing objects, but only their
     * state (current value of internal fields).
     * @return a String.
     */

   public String mySerialize(Parser parser)
   {
 //    Parser parser = new Parser (pd, ed, vd, sd);

     Vector vblock = new Vector();

     //m_Counts block
     vblock.addElement(new Block("SCHEMANAMES", name));

     //sum of counts block
     Block blk2 = new Block("SCHEMAATTRIBUTES");
     for(int i=0; i<attributes.size(); i++) blk2.addValue(attributes.get(i), parser);
     vblock.addElement(blk2);


     return parser.filter(parser.unparseBlocks(vblock));
   }

   /**
   * Parse to my format, this procedure works paired with mySerialize.<!-- -->
   * The string output from this procedure can be use to change the state of any
   * AttributeSample object (values of internal
   * components and parameters) to the state of the current calling object.
   * @return a String.
   */
  public void myUnserialize(String inputStr, Parser parser)
  {
    Vector vblock = parser.parse(parser.unfilter(inputStr));

    for(int i=0; i<vblock.size(); i++)
    {
      Block blk = (Block) vblock.get(i);

       if(blk.tag.equals( "SCHEMANAMES"))
       {
         name = blk.getString(0);
       }

      else if(blk.tag.equals("SCHEMAATTRIBUTES"))
      {
        attributes = null;
        attributes = new Vector();
        for(int j=0; j<blk.size(); j++)
        {
          AttributeSample attr = (AttributeSample) blk.getObject(j, new AttributeSample(), parser);
          //AttributeSample attr = new AttributeSample("garbage");
          //blk.getObject(j, attr, parser);
          attributes.addElement(attr);
        }
      }

    }


  }

}
