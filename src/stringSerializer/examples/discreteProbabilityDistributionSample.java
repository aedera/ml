package stringSerializer.examples;


/*
 /*
 *    discreteProbabilityDistribution.java
 *    Author: Facundo Bromberg
 *
*/

import java.util.*;

import stringSerializer.*;
/**
 * Simple symbolic (values in Dataset language) probability distribution with estimator based on symbol
 * frequency counts that ssumes a uniform prior probability for each symbol.<!-- --> Can also store a
 * probability directly.
 *
 * @author Facundo Bromberg
 */

public class discreteProbabilityDistributionSample  implements ImySerializable{

  /** Hold the counts */
  private double [] m_Counts;

  /** Hold the sum of counts */
  private double m_SumOfCounts;

  /** Hold the number of symbols */
  private int m_numSymbols;

  /** Hold the equivalent sample size */
  private int m_equivalent_sample_size;

  /** Hold the equivalent sample size */
  private boolean isEstimator;

  public discreteProbabilityDistributionSample()
  {}
  /**
   * Constructor for a probability distribution with estimator capabilities.
   *
   * @param numSymbols the number of possible symbols (remember to include 0)
   * @param m_ess the number of equivalent sample size
   */
  public discreteProbabilityDistributionSample(int numSymbols, int m_ess) {

//	m_equivalent_sample_size = (int)(m_ess / numSymbols)*numSymbols;
      m_equivalent_sample_size = m_ess;
      if(m_ess == -1) isEstimator = false;//because it receives m_ess
      else isEstimator = true;
      m_numSymbols = numSymbols;

      m_Counts = new double [m_numSymbols];

      for(int i = 0; i < m_numSymbols; i++) {
	  m_Counts[i] = 0;
      }
      m_SumOfCounts = 0;
  }
  /**
   * Constructor for a probability distribution without estimator capabilities.<!-- --> In this case,
   * m_Counts stores the probabilities directly;
   *
   * @param numSymbols the number of possible symbols (remember to include 0)
   * @param m_ess the number of equivalent sample size
   */
  public discreteProbabilityDistributionSample(int numSymbols) {

//	m_equivalent_sample_size = (int)(m_ess / numSymbols)*numSymbols;
      m_equivalent_sample_size = -1;
      isEstimator = false;
      m_numSymbols = numSymbols;

      m_Counts = new double [m_numSymbols];

      for(int i = 0; i < m_numSymbols; i++) {
	  m_Counts[i] = 0;
      }
  }

  /**
   * Add one count to the value in valueIndex.
   *
   * @param valueIndex the value index
   * @param weight the weight assigned to the data value.
   * @return false if isEstimator is false.
   */
  public boolean incrementValue(int valueIndex, double weight)
  {
      if(!isEstimator) return false;

      m_Counts[valueIndex] += weight;
      m_SumOfCounts += weight;

      return true;
  }

  /** returns the value of isEstimator. */
  public boolean isEstimator() {return isEstimator;}

  /** sets the value of isEstimator. */
  public void isEstimator(boolean n) {isEstimator = n;}

  /**
   * Get a probability estimate for a value.<!-- -->
   * Assume a uniform prior probability = 1/k, where k = num_Symbols.
   *
   * @param valueIndex the value to estimate the probability of
   * @return the estimated probability of the supplied value
   */
  public double getProbability(int valueIndex)
  {
      return getProbability(valueIndex, -1);
  }

  /**
    * Get a probability estimate for a value BUT weighted over input parameter N, not m_SumOfCounts.<!-- -->
    * It is used by the method getJointProbability of condProbabilityTable for computing joint probabilities
    * of the variables stored in the table, i.e. instead of P(A|B,C,D)=N(A,B,C,D)/m_SumOfCounts (modulo de priors)
    * it computes P(A,B,C,D) = N(A,B,C,D)/N where N alone is the input parameter N.<!-- -->
    * Assume a uniform prior probability = 1/k, where k = num_Symbols. <!-- --> If n == -2, it just return the
    * counts (the sufficient statistics).
    *
    * @param valueIndex the value to estimate the probability of
    * @return the estimated probability of the supplied value
    */
   public double getProbability(int valueIndex, int n)
   {
       double N = n;
       if(N == -1) N= m_SumOfCounts;

       if(!isEstimator || n == -2) {
	   return m_Counts[valueIndex];
       }

       if (N == 0  && m_equivalent_sample_size == 0) {
	   return 0;
       }
       double prior = (double) 1 / (double)m_numSymbols;
       return ((double)m_Counts[valueIndex] + m_equivalent_sample_size*prior) / (N + m_equivalent_sample_size);
  }
  /**
   * Returns the index with the highest probability
   */
  public int getMostProbableIndex()
  {
      double max = Double.MIN_VALUE;
      int index = 0;
      for(int i=0; i<size(); i++)
      {
	  double aux = getProbability(i);
	  if(aux > max) {
	      max = aux;
	      index = i;
	  }
      }
      return index;

  }
  /**
   * sets the probability at valueIndex position.<!-- --> If the distribution is of estimator type
   * (isEstimator = true), then it return false.
   */
  public boolean setProbability(int valueIndex, double prob)
  {
      if(isEstimator) return false;

      m_Counts[valueIndex] = prob;
      return true;
  }
  /**
   * Gets the number of symbols this estimator operates with
   *
   * @return the number of estimator symbols
   */
  public int size()
  {
      return m_numSymbols;
  }

  /**
   * Normalize the probability distribution
   */
  public void normalize()
  {
      double sum = 0;
      for(int i=0; i<size() ;i++) sum += getProbability(i);
      for(int i=0; i<size() ;i++)
      {
	  setProbability(i, getProbability(i) / sum);
      }
  }
  /**
   * Internal product of the probability distribution (acting as a vector) with the
   * input distribution.
   */
  public int inProduct(discreteProbabilityDistributionSample v)
  {
      if(size() != v.size()) return -1;

      int res = 0;
      for(int i=0; i<size() ;i++)
      {
	  res += getProbability(i) * v.getProbability(i);
      }
      return res;
  }

  /**
   * Term by term multiplication of the two distributions.<!-- -->
   * eg (1, 2, 3) (3, 2, 1)  = (3, 4, 3)
   * @return null if sizes mismatch
   */
  public discreteProbabilityDistributionSample product(discreteProbabilityDistributionSample v)
  {
      if(size() != v.size()) return null;

      discreteProbabilityDistributionSample newD = new  discreteProbabilityDistributionSample(size(), m_equivalent_sample_size);
      for(int i=0; i<size() ;i++)
      {
	  newD.setProbability(i, getProbability(i) * v.getProbability(i));
      }
      return newD;
  }

  /**
   * isEqual compares the two distributions term by term.
   */
  public boolean isEqual(discreteProbabilityDistributionSample v)
  {
      if(size() != v.size()) return false;

      for(int i=0; i<size() ;i++)
      {
	  if(getProbability(i) != v.getProbability(i)) return false;
      }
      return true;
  }

  /**
   * distance Returns a double value with the distance between the two distributions.<!-- --> The distance
   * is measured according to the mean square error between the two.
   */
  public double distance(discreteProbabilityDistributionSample other)
  {
      if(size() != other.size()) return -1;

      double error = 0;
      for(int i=0; i<size() ;i++)
      {
	  double err;
	  err = getProbability(i) - other.getProbability(i);
	  error += err*err ;
      }
      return error/2;

  }
  /**
   * return an exact copy of the input distribution.
   */
  public discreteProbabilityDistributionSample makeCopy()
  {
      discreteProbabilityDistributionSample newD = new  discreteProbabilityDistributionSample(size(), m_equivalent_sample_size);

      newD.isEstimator(isEstimator);

      for(int i=0; i<size() ;i++)
      {
	  newD.setProbability(i, getProbability(i));
      }
      return newD;
  }
  /**
   * Set all the probabilities to input parameter "value".
   */
  public void setAllProbabilities(double value)
  {
      for(int i=0; i<size() ;i++)
      {
	  setProbability(i, value);
      }
  }

  /**
   * Makes the current distribution an exact copy of the input distribution v.
   */
  public boolean copyFrom(discreteProbabilityDistributionSample v)
  {
      if(size() != v.size()) return false;

      isEstimator = v.isEstimator();

      for(int i=0; i<size() ;i++)
      {
	  setProbability(i, v.getProbability(i));
      }
      return true;
  }
  /**
   * Display a representation of this estimator
   */
  public String toString()
  {
      String out = "";
      if(true)//!isEstimator)
      {
	  out += "(";
	  for(int i=0; i<size(); i++)
	  {
	      out += SSUtils.myDoubleToString(getProbability(i), 3) + ", ";
	  }
	  out = out.substring(0, out.length()-2);
	  out += ")";
      }
      return out;
  }


  /**
   * Unparse to my format, this procedure works paired with myUnserialize.<!-- -->
   * The output string stores the current state of the calling object. This
   * string can be used later to set the state of another object to the same state
   * of the original object.<!-- --> Note that this is not storing objects, but only their
   * state (current value of internal fields).
   * @return a String.
   */

   public String mySerialize(Parser parser)
   {
//     Parser parser = new Parser (pd, ed, vd, sd);

     Vector vblock = new Vector();

     //m_Counts block
     Block blk = new Block("COUNTS");
     for(int i=0; i<m_Counts.length; i++) blk.addValue(m_Counts[i]);
     vblock.addElement(blk);

     //sum of counts block
     Block blk2 = new Block("SUMOFCOUNTS", m_SumOfCounts);
     vblock.addElement(blk2);

     //Generating num of symbosl
     Block blk3 = new Block("MNUMSYMBOLS", m_numSymbols);
     vblock.addElement(blk3);

     //Generating ess
     Block blk4 = new Block("ESS", m_equivalent_sample_size);
     vblock.addElement(blk4);


     //Generating isestimator
     Block blk5 = new Block("ISESTIMATOR");
     if(isEstimator) blk5.addValue("TRUE");
       else blk5.addValue("FALSE");
     vblock.addElement(blk5);

     return parser.filter(parser.unparseBlocks(vblock));
   }

   /**
   * Parse to my format, this procedure works paired with mySerialize.<!-- -->
   * The string output from this procedure is used to change the state of
   *  internal
   * field values of the current calling object.
   * @return a String.
   */

   public void myUnserialize(String inputStr, Parser parser)
   {
//     Parser parser = new Parser (pd, ed, vd, sd);
     Vector vblock = parser.parse(parser.unfilter(inputStr));

     for(int i=0; i<vblock.size(); i++)
     {
       Block blk = (Block) vblock.get(i);

        if(blk.tag.equals( "COUNTS"))
        {
          m_Counts = null;
          m_Counts = new double[blk.size()];
          for(int j=0; j<m_Counts.length ; j++)
          {
            m_Counts[j] = blk.getDouble(j);
          }
        }

       else if(blk.tag.equals("SUMOFCOUNTS"))
       {
         m_SumOfCounts = blk.getDouble(0);
       }

       else if(blk.tag.equals( "MNUMSYMBOLS"))
       {
        m_numSymbols = (int) blk.getDouble(0);
       }
       else if(blk.tag.equals( "ESS"))
       {
         m_equivalent_sample_size = (int) blk.getDouble(0);
       }
       else if(blk.tag.equals( "ISESTIMATOR"))
        {
          if(blk.getString(0).equals("TRUE")) isEstimator = true;
          else isEstimator = false;
        }



     }


   }

}
