package stringSerializer.examples;


/*
/*
 *    condProbabilityTableSample.java
 *    Author Facundo Bromberg. 2002.
 *
 *
 */



import java.io.*;
import java.util.*;
import stringSerializer.*;

/**
 * Implements a conditional probability table P(A | B1 B2 B3 .<!-- -->.<!-- --> Bk) .<!-- --> For every assignment of values
 * to each of the conditioning variables B1 .. Bk we store the probability that variable A takes a certain value. <!-- -->
 * The table consist then in a k-dimensional matrix (ArrayDSample) which will store, at each cell, a vector containing as many slots
 * as values in A.<!-- --> Each cell then represent an complete assignment of values to the variables, and stores the probability
 * associated to that assignment, i..<!-- -->e..<!-- --> P(A=a3 | B1 = b14, B2 = b23, .<!-- -->.<!-- --> , Bk = bk7) will be stored in the
 * 3rd cell of the vector (discreteProbabilityDistributionSample) stored at the Matrix cell [4, 3, .<!-- -->.<!-- -->, 7].
 */

public class condProbabilityTableSample implements ImySerializable{

    /** Index of the attribute (or variable) in the dataset header */
    protected int m_attribute_index;

    /** AttributeSample (or variable) in the dataset header */
    protected AttributeSample attribute;

    /** Indexes of the attributes corresponding to the parents variables */
    protected int [] m_condAttributeSet;

    /** Multidimentional Vector-like data structure  used for storing the conditional Probability Table*/
    protected ArrayDSample estimators;

    /** for the case no conditional variables we have only one estimator*/
   protected discreteProbabilityDistributionSample estimator;

    /** Local Reference copy of the dataset*/
    transient protected SchemaSample data;

    /** Total number of instances used to estimate probabilities; it's reset to zero at creation of table
     * and incremented at every call of updateInstance. */
    protected int N;

    /** Number of networks referencing this CPT*/
    public int counter;

    /** Used to synchronize with BNNode. */
    transient public boolean notUsedSinceLastUpdate;

    public condProbabilityTableSample(){
      estimator = null;
      estimators = null;
    };  //For it to be serializable
/*---------------------------------------------------*/
  /**
   * Construct the Table without probability estimation from dataset.
   *
   * @param inst the DatasetSample. We use it to extract the attributes structure
   * @param attrIndex the index of the node/AttributeSample/random_variable for which we build the Table.
   * @param condAttrSet (defined above)
   */

  public condProbabilityTableSample(SchemaSample inst, int attrIndex, int [] condAttrSet)
  {
      initTable(inst, attrIndex, condAttrSet, -1);
  }

/*---------------------------------------------------*/
  /**
   * Construct the Table with probability estimation from dataset.
   *
   * @param inst the DatasetSample. We use it to extract the attributes structure
   * @param attrIndex the index of the node/AttributeSample/random_variable for which we build the Table.
   * @param condAttrSet (defined above)
  */

  public condProbabilityTableSample(SchemaSample _data, int attrIndex, int [] condAttrSet, int ess)
  {
      initTable(_data, attrIndex, condAttrSet, ess);
  }
/*---------------------------------------------------*/
  /**
   * Construct the Table with probability estimation from dataset.
   *
   * @param inst the DatasetSample. We use it to extract the attributes structure
   * @param attrIndex the index of the node/AttributeSample/random_variable for which we build the Table.
   * @param condAttrSet (defined above)
   * @param ess the equivalent sample size used by the probability m-estimator.
   */

  public void initTable(SchemaSample _data, int attrIndex, int [] condAttrSet, int ess)
  {
      int j, num;
      attribute = _data.getAttribute(attrIndex);
      int m_numOfValues = attribute.size();
      int m_numCondAttributes = condAttrSet.length;

      data = _data;

      //Assign values to class variables.
      m_attribute_index = attrIndex;

      m_condAttributeSet = new int[m_numCondAttributes];
      for(int i=0; i<m_numCondAttributes; i++) {
	  m_condAttributeSet[i] = condAttrSet[i];
      }
      // Reserve space for the table
      int [] dimentions;
      dimentions = new int[m_numCondAttributes];

      //dimentions[0] = inst.attribute(attrIndex).numValues();
      for(int i=0; i< m_numCondAttributes; i++) {
	  dimentions[i] = data.getAttribute(m_condAttributeSet[i]).size();
      }

      //special case for no conditioning variables
      if(m_numCondAttributes == 0) {
	  if(ess == -1) estimator = new discreteProbabilityDistributionSample(m_numOfValues);
	  else estimator = new discreteProbabilityDistributionSample(m_numOfValues, ess);
      }
      //Creates and stores estimators
      //For every value of the conditional r.v.,  we have an array of estimators
      else {
	  estimators = new ArrayDSample(dimentions);
	  int [] coord = new int[estimators.getDepth()];

	  for(int i= 0; i<coord.length; i++) coord[i] = 0;

	  while(coord != null) {
	      discreteProbabilityDistributionSample est;
	      if(ess == -1) est = new discreteProbabilityDistributionSample(m_numOfValues);
	      else est = new discreteProbabilityDistributionSample(m_numOfValues, ess);

	      estimators.set(coord, (Object) est);
	      coord = estimators.getNextCoord(coord);
	  }


	  dimentions = null;
	  N=0;
      }

      notUsedSinceLastUpdate = true;
  }

  /**
     * Recomputes the values of the table for the input data.
     */
    public void computeTable(DatasetSample data)
    {
      // Compute counts
      InstanceSample inst = data.getFirstInstance();

      while (inst != null) {

          updateInstance(inst);
          inst = data.getNextInstance();
      }
    }

 /*---------------------------------------------------*/
 /**
  * Retrieves the marginal probability P(X), where X is the table's variable P(X|A,B, ...)
  */
 public  discreteProbabilityDistributionSample getMarginalProbability()
 {
     //gets the list of attributes not included in attrSet
/*     int [] attrSetComplement = new int[m_condAttributeSet.length - attrSet.length];
     int j=0, k=0;
     for(int i=0; i<m_condAttributeSet.length ; i++)
     {
	 if(m_condAttributeSet[i] == attrSet[j]) j++;
	 else {
	     attrSetComplement[k] = m_condAttributeSet[i];
	     k++;
	 }
     }
*/
     discreteProbabilityDistributionSample marginal = new discreteProbabilityDistributionSample(attribute.size());

     for(int j=0; j<marginal.size(); j++)
     {
	 double val = 0;
	 int [] coord = getInitialCoord();

	 while(coord != null)
	 {
	     val += getJointProbability(coord, j);
	     coord = getNextCoord(coord);
	 }
	 marginal.setProbability(j, val);
     }
     return marginal;
 }

 /*---------------------------------------------------*/
 /**
  * Retrieves the marginal probability P(X|Y), where X is the table's variable P(X|A,B, ...)
  * and Y is one of the parents.
  */
 public  condProbabilityTableSample getParentMarginalProbability(int parentIndex)
 {
     int [] condAttrSet = new int[1];
     condAttrSet[0] = parentIndex;

     condProbabilityTableSample marginal = new condProbabilityTableSample(data, m_attribute_index, condAttrSet);

     AttributeSample condAttr = data.getAttribute(parentIndex);
     for(int j=0; j<condAttr.size(); j++)
       {
	   for(int x=0; x<attribute.size(); x++)
	   {
	       double val = 0;
	       int [] coord = getInitialCoord();
	       coord[parentIndex] = j;
	       while(coord != null)
	       {
		   val += getJointProbability(coord, x);
		   //Gets next coordinate but keeping fix the parentIndex
		   coord = getNextCoord(coord, parentIndex);
	       }
	       marginal.setProbability(coord, x, val);
	   }
     }
     return marginal;
 }

 /*---------------------------------------------------*/
 /**
  * Shrinks the table by one dimension, i.e. if table is P(A|B,C,D) and parentIndex is
  * 1 (corresponding to C), then the output is P(A|B,D) = For each instantiation of
  * A,B,D return the Summation over all C values of P(A|B,C,D).
  */
/* public  condProbabilityTableSample getMarginalProbability(int parentIndex)
 {
     int [] condAttrSet = new int[m_condAttributeSet.length-1];
     int flag = 0;
     for(int i=0; i<condAttrSet.length; i++)
     {
       if(i==parentIndex) flag = 1;
       condAttrSet[i] = m_condAttributeSet[i+flag];
     }

     condProbabilityTableSample marginal = new condProbabilityTableSample(data, m_attribute_index, condAttrSet);

     int [] coord = getInitialCoord();
     coord[parentIndex] = j;
     do
     {
       coord = getNextCoord(coord, parentIndex);

     }while(coord != null);


     AttributeSample condAttr = data.getAttribute(parentIndex);
     for(int j=0; j<condAttr.size(); j++)
       {
           for(int x=0; x<attribute.size(); x++)
           {
               double val = 0;
               int [] coord = getInitialCoord();
               coord[parentIndex] = j;
               while(coord != null)
               {
                   val += getJointProbability(coord, x);
                   //Gets next coordinate but keeping fix the parentIndex
                   coord = getNextCoord(coord, parentIndex);
               }
               marginal.setProbability(coord, x, val);
           }
     }
     return marginal;
 }

  /**
   * Given a new data instance, it re-estimate/update the probabilities of the tables.
   *
   * @param inst new instance
   */
  public void updateInstance(InstanceSample inst)
  {
      updateInstance(inst, 1);
  }
  /*---------------------------------------------------*/
  /**
   * Given a new data instance, it re-estimate/update the probabilities of the tables.
   *
   * @param inst new instance
   */
  public void updateInstance(InstanceSample inst, double instanceWeight)
  {
      notUsedSinceLastUpdate = true;

      String value = inst.getValue(m_attribute_index);
      int valueIndex = attribute.getValueIndex(value);

      if(m_condAttributeSet.length > 0){
	  discreteProbabilityDistributionSample est = (discreteProbabilityDistributionSample) estimators.get(getCoords(inst));
	  est.incrementValue(valueIndex , instanceWeight);
      }
      else {
	  estimator.incrementValue(valueIndex , instanceWeight);
     }
     N++;
  }

  /*---------------------------------------------------*/
   /**
    * Retrieves the sufficient statistics of the attributes in the table.<!-- --> I.e. instead of
    * P(A|B,C,D) it returns N(A,B,C,D)
    * @param coords is a vector of values for each of the conditioning variables
    * @param valueIndex is the value of the conditioned variable.
    */
   public  double getN(int [] coords, int valueIndex)
   {
       return getProbability(coords, valueIndex, -2);
   }


 /*---------------------------------------------------*/
 /**
  * Retrieves the joint probability of the attributes in the table.<!-- --> I.e. instead of
  * P(A|B,C,D) it returns P(A,B,C,D)
  * @param coords is a vector of values for each of the conditioning variables
  * @param valueIndex is the value of the conditioned variable.
  */
 public  double getJointProbability(int [] coords, int valueIndex)
 {
     return getProbability(coords, valueIndex, N);
 }
  /*-------------------------------------------*/
  /**
   * The instance is an assignment of values to each of the variables in the graph.<!-- --> This function
   * returns the conditional probability associated to the values in instance that is stored in the
   * conditional probability table.
   */
  public double getProbability(InstanceSample inst)
 {
     int numCondAttr = m_condAttributeSet.length;
     int [] coords = getCoords(inst);

     String value = inst.getValue(m_attribute_index);
     int valueIndex = attribute.getValueIndex(value);
     return getProbability(coords, valueIndex, -1);
 }
 /*---------------------------------------------------*/
 /**
  * Retrieves the probability.
  * @param coords is a vector of values for each of the conditioning variables
  * @param valueIndex is the value of the conditioned variable.
  */
 public  double getProbability(int [] coords, int valueIndex)
 {
     return getProbability(coords, valueIndex, -1);
 }

 /*---------------------------------------------------*/
 /**
  * Retrieves the probability.
  * @param coords is a vector of values for each of the conditioning variables
  * @param valueIndex is the value of the conditioned variable.
  */

 private  double getProbability(int [] coords, int valueIndex, int _N) {
     if(coords != null && coords.length > 0) {
	 discreteProbabilityDistributionSample est = (discreteProbabilityDistributionSample) estimators.get(coords);
	 return est.getProbability(valueIndex, _N);
     }
     else return estimator.getProbability(valueIndex, _N);
 }
 /*---------------------------------------------------*/
  /**
   * Sets the probability.
   * @param coords is a vector of values for each of the conditioning variables
   * @param valueIndex is the value of the conditioned variable.
   */
  public  boolean setProbability(int [] coords, int valueIndex, double P)
  {
      if(coords != null && coords.length > 0) {
	  discreteProbabilityDistributionSample est = (discreteProbabilityDistributionSample) estimators.get(coords);
	  return est.setProbability(valueIndex, P);
      }
      else return estimator.setProbability(valueIndex, P);
  }
/*---------------------------------------------------*/
 /**
  * returns the total number of cells in the probability table.
  */
  public int totalEntries() {
/*
//     THIS IS FOR TESTING THE PARSER FOR ArrayDSample and discreteProbabilityDistributionSample Classes

     if(m_condAttributeSet.length != 0){
      System.out.println("OLD arraD:" + estimators.toString());
      String estParsed = estimators.mySerialize("$", "&", "*", " ");
      ArrayDSample newArrayD = new ArrayDSample();
      newArrayD.myUnserialize(estParsed, new discreteProbabilityDistributionSample(), "$", "&", "*", " ");

      System.out.println("NEW arraD:" + newArrayD.toString());
    }
    else{
      String estParsed = estimator.mySerialize("$", "&", "*", " ");
      discreteProbabilityDistributionSample newArrayD = new discreteProbabilityDistributionSample();
      newArrayD.myUnserialize(estParsed, "$", "&", "*", " ");

      System.out.println("OLD dpd:" + estimator.toString());
      System.out.println("NEW dpd:" + newArrayD.toString());
    }

   */       if(m_condAttributeSet.length == 0) return 1;
          else return estimators.totalEntries();
  }

/*---------------------------------------------------*/
  /**
   * Parses the instance inst into coord.
   */
  public int [] getCoords(InstanceSample inst) {
      int m_numCondAttributes = m_condAttributeSet.length;

      int [] coordinates = new int[m_numCondAttributes];

      for(int i=0; i< m_numCondAttributes; i++)
      {
	  String value = inst.getValue(m_condAttributeSet[i]);
	  coordinates[i] = data.getAttribute(m_condAttributeSet[i]).getValueIndex(value);
     }

      return coordinates;
  }

  /**
   * Just return getNextCoord implemented in ArrayDSample.
   */
   public int [] getNextCoord(int [] coord)
   {

       //This is for the case when we have only 1 estimator (no conditioning variables)

       if(estimators == null) return null;



       return estimators.getNextCoord(coord);

   }



   /**

    * Just return getNextCoord implemented in ArrayDSample.

    */

    public int [] getNextCoord(int [] coord, int dim)

    {

	//This is for the case when we have only 1 estimator (no conditioning variables)

	if(estimators == null) return null;



	return estimators.getNextCoord(coord, dim);

    }



   /**

    * Util method that increments the coordinate of dimension dim by one.

    *

    * @param coord the coordinates for which we wants the next one.

    * @param dim dimension to which increase the coordinate by one.

    * @return true if the increment was performed, false otherwise.

    */

   public boolean incrementCoord(int [] coord, int dim)

   {

       return estimators.incrementCoord(coord, dim);

   }



   /**
    * Just returns a new allocated coords vector initialized to 0.
    */
     public int [] getInitialCoord()
     {
         if(estimators == null) return null;

         int [] coord = new int[estimators.getDepth()];
	 for(int i= 0; i<coord.length; i++) coord[i] = 0;
	 return coord;

     }

     /*---------------------------------------------------*/
         /**
        * Display a representation of this probabilityTable
        */
       public String toString() {

         String result = "\n\nProbability Table:";
         result += "\n AttributeSample Index: " +  m_attribute_index;
         result += "\n AttributeSample Description: " +  attribute.toString();
         result += "\n Conditioning Attributes' Indexes: ";
         for(int i=0; i<m_condAttributeSet.length; i++) result += m_condAttributeSet[i] + " ";

         result+=  "\nTABLE DATA: ";
         if(m_condAttributeSet.length == 0) result+=  estimator.toString();
         else result += estimators.toString();

         return result;
       }



       /**
     * Unparse to my format, this procedure works paired with myUnserialize.<!-- -->
     * The output string stores the current state of the calling object. This
     * string can be used later to set the state of another object to the same state
     * of the original object.<!-- --> Note that this is not storing objects, but only their
     * state (current value of internal fields).
     * @return a String.
     */

 public String mySerialize(Parser parser)
 {
//   Parser parser = new Parser (pd, ed, vd, sd);

   Vector vblock = new Vector();

   vblock.addElement(new Block("CPTMYATTRIBUTEINDEX", m_attribute_index));

   vblock.addElement(new Block("CPTATTRIBUTE", attribute, parser));

   Block blk3 = new Block("CPTCONDATTRIBUTESET");
   for(int i=0; i<m_condAttributeSet.length; i++) blk3.addValue(m_condAttributeSet[i]);
   vblock.addElement(blk3);

   if(estimators != null ) vblock.addElement(new Block("CPTESTIMATORS", estimators, parser));

   if(estimator != null ) vblock.addElement(new Block("CPTESTIMATOR", estimator, parser));

// IT IS TRANSIENT   vblock.addElement(new Block("CPTSCHEMA", data, parser));

   vblock.addElement(new Block("CPTN", N));

   vblock.addElement(new Block("CPTCOUNTER", counter));

   return parser.filter(parser.unparseBlocks(vblock));
 }

 /**
   * Parse to my format, this procedure works paired with mySerialize.<!-- -->
   * The string output from this procedure is used to change the state of
   *  internal
   * field values of the current calling object.
   * @return a String.
   */

public void myUnserialize(String inputStr, Parser parser)
{
//  Parser parser = new Parser(pd, ed, vd, sd);
  Vector vblock = parser.parse(parser.unfilter(inputStr));

  for (int i = 0; i < vblock.size(); i++) {
    Block blk = (Block) vblock.get(i);

    if (blk.tag.equals("CPTMYATTRIBUTEINDEX")) m_attribute_index = (int) blk.getDouble(0);

    if (blk.tag.equals("CPTATTRIBUTE")) attribute = (AttributeSample) blk.getObject(0, new AttributeSample(), parser);

    if (blk.tag.equals("CPTCONDATTRIBUTESET")) {
      m_condAttributeSet = null;
      m_condAttributeSet = new int[blk.size()];
      for(int j=0; j<blk.size(); j++) m_condAttributeSet[j] = (int) blk.getDouble(j);
    }

    if (blk.tag.equals("CPTESTIMATORS")) {
      estimators = (ArrayDSample) blk.getContainerObject(0, new ArrayDSample(),
          new discreteProbabilityDistributionSample(), parser);
    }


    if (blk.tag.equals("CPTESTIMATOR")) {
      estimator = (discreteProbabilityDistributionSample) blk.getObject(0,
          new discreteProbabilityDistributionSample(), parser);
    }

    if (blk.tag.equals("CPTN")) N = (int) blk.getDouble(0);

    if (blk.tag.equals("CPTCOUNTER")) counter = (int) blk.getDouble(0);
  }
}


}



