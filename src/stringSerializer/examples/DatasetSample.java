package stringSerializer.examples;


import java.util.*;
import java.io.*;

import stringSerializer.*;
/**
 * Class DatasetSample
 *
 * Implements the functionality of a dataset with the attributes list
 * and the data itself both contained in abstract representations:
 * AttributeSample and InstanceSample. The whole structure is inspired both by a
 * personal analysis and design of the functionality of datasets
 * and by knowledge of WEKA environment. But, no line of code was copied
 * from WEKA
 *
 * @author Facundo Bromberg
 * @version 1.1 Actualized for the use in Final Project (BayesNetworks inference) of CS572, AI, Fall 2002.
 * @version 1.2 Splitted in DatasetSample+Schema for Final Project (BayesNetworks incremental learning) of CS561
 * DB, Spring 2003.
 */
public class DatasetSample extends SchemaSample implements ImySerializable
{
	/** For eficiency reasons, instances is implemented as a linked list.
	 * Moreover, it is set public to allow the user an efficient iteration
	 * by using the Iterator functionality.
	 */
	public LinkedList instances;

	transient private ListIterator li;

        public DatasetSample(){;};

//        public DatasetSample(Schema schema){
  //        name = new String(schema.name);
    //    }
	public DatasetSample(String _name){
          super(_name);
          instances = new LinkedList();
	}

	/**
	 * Iteration funcionality.<!-- --> This method initiates an iteration over the instances.
	 */
	public InstanceSample getFirstInstance()
	{
	    li = instances.listIterator(0);
	    if(!li.hasNext()) return null;

	    return (InstanceSample) li.next();
	}

	/**
	 * Returns next instance in iterator.
	 */
	public InstanceSample getNextInstance()
	{
	    if(!li.hasNext()) return null;

	    return (InstanceSample) li.next();
	}

	/**
	 * Procedure that returns a String representation of Attributes and Instances.
	 */
	public String toString()
	{
	    return toString(false, false);
	}

	/**
	 * Procedure that returns a String representation of Attributes and Instances. If noHeader is true, then it returns only the instances
	 */
	public String toString(boolean noHeader, boolean noInstanceValues)
	{
	    String Names = "", Data = "";

	    if(!noHeader)
	    {
		Names = super.toString();
	    }
	    if(!noHeader) Data = "\nInstances data: \n";

	    if(instances.size() == 0) return Names + "\n" + Data;

	    ListIterator li = instances.listIterator(0);
	    InstanceSample inst = (InstanceSample) li.next();

	    while(true)
	    {
		Data = Data + inst.toString(noInstanceValues);
		if(!noInstanceValues)	Data = Data + "\n";
		else Data = Data + ", ";

		if(li.hasNext()) inst = (InstanceSample) li.next();
		else break;
	    }

	    return Names + Data;
	}
        /**
         * Make Copy creates a new dataset but do not add a single instance. The instances linked list is empty for the new created dataset.
         */
        public DatasetSample makeHeaderCopy()
        {
          return subset(-1, "-2");
        }
        /**
         * Make Copy creates a new dataset but do not create new instances(attributes). Each cell in the instances (attributes) Vector will point to the same instance.
         */
        public DatasetSample makeCopy()
        {
          return subset(-1, "");
        }
        /**
         * Subset creates a copy of the dataset including only a subset of the instances. More specifically, those that has the index-th attribute equal to "value"
         * If value = -1 , then, it copies all. That is, it returns a copy of whole dataset
         * If value = -2 , then, it doesn't copy any instance. That is, it returns a copy of the dataset
         * but without any instance
         */
        public DatasetSample subset(int attr_index, String value)
        {
          DatasetSample newD = (DatasetSample) new DatasetSample(name);

          //Copy attributes Vector.
          for(int i=0; i<attributes.size(); i++)
          {
            AttributeSample attr = getAttribute(i);
            newD.addAttribute(attr);
          }
	  if(attr_index == -2 || instances.size() == 0) return newD;

          //Copy instances LinkedList
          ListIterator li = instances.listIterator(0);
	  InstanceSample aux_inst = (InstanceSample) li.next();


          while(li.hasNext())
          {
	      if(attr_index == -1 ) {
		  newD.addInstance(aux_inst);
	      }
	      else {
		  String  inst_value = aux_inst.getValue(attr_index);
		  if(inst_value.equalsIgnoreCase(value)) {
		      newD.addInstance(aux_inst);
		  }
	      }
	      aux_inst = (InstanceSample) li.next();
	  }
	  return newD;
	}

	/**
	 * getDirectProbability is a method that returns P( Xj | X1=v1 X2=v2 ... Xj-1=vj-1 Xj+1=vj-2 .... Xn=vn)
	 * by counting in the dataset all the instances for which attribute X1 contains the value v1, X2 contains
	 * value v2, etc.
	 *
	 * @param The instance from which we obtain the values of the conditioning variables
	 * @param The index of the attribute for which we are constructing the probability distribution.
	 */
	public discreteProbabilityDistributionSample getDirectProbability(InstanceSample inst, int attr_index)
	{
	    DatasetSample sub = makeCopy();

	    //We build a subset of this dataset iteratively by calling subset method for each of the
	    //values in inst of the conditioning attributes.
	    for(int i=0; i<inst.size(); i++)
	    {
		DatasetSample aux;
		if(i == attr_index) continue;

//		String toFile = sub.toString();
//		Utils.writeStringToFile(toFile, "subset.dat");
		sub = sub.subset(i, inst.getValue(i));
	    }

	    //We now build the probability distribution of attr_index's attribute.
	    AttributeSample attr = getAttribute(attr_index);
	    discreteProbabilityDistributionSample newD = new discreteProbabilityDistributionSample(attr.size());
	    for(int i=0; i<attr.size(); i++)
	    {
		DatasetSample aux = sub.subset(attr_index, attr.getValueName(i));
		double prob = (double)aux.size()/(double)sub.size();
		if(sub.size() == 0) prob = 0.0;
		newD.setProbability(i, prob);
	    }
	    return newD;
	}

	/**
	 * getDirectProbability is another version of the one presented above that returns P( X | Y=v)
	 * by counting in the dataset all the instances for which attribute X1 contains the value v1
	 *
	 * @param The index of the conditioning variable (Y)
	 * @param The value of the conditioning variable (v)
	 * @param The index of the conditioned variable (X)
	 */
	public discreteProbabilityDistributionSample getDirectProbability(int cond_index, String value, int attr_index)
	{
	    DatasetSample sub = subset(cond_index, value);

	    //We now build the probability distribution of attr_index's attribute.
	    AttributeSample attr = getAttribute(attr_index);
	    discreteProbabilityDistributionSample newD = new discreteProbabilityDistributionSample(attr.size());
	    for(int i=0; i<attr.size(); i++)
	    {
		DatasetSample aux = sub.subset(attr_index, attr.getValueName(i));
		double prob = (double)aux.size()/(double)sub.size();
		if(sub.size() == 0) prob = 0.0;
		newD.setProbability(i, prob);
	    }
	    return newD;
	}
	/**
	 * generateRandomInstance return an instance with random values (of course, among those declared in
	 * the attribute).
	 */
	InstanceSample generateRandomInstance()
	{
	    InstanceSample newI = getFirstInstance().makeCopy();

	    Random r = new Random();
	    for(int i=0; i<numberOfAttributes(); i++)
	    {
		AttributeSample attr = getAttribute(i);
		int newValueIndex = r.nextInt(attr.size());
		//Sets the i-th value of instance to the random value extracted from the i-thattribute.
		newI.setValue(attr.getValueName(newValueIndex), i);
	    }
	    return newI;
	}
	 /**
	  * Computes Information Gain. Gain(S, A) = Entropy(S) - Summation( ((|Sv| / |S|) * Entropy(Sv)) ). The summation goes over all values of attribute A.
	  */
	  private double infoGain(int attr_index)
	  {
	      AttributeSample A = getAttribute(attr_index);
	      int count_size = A.size();

	      int counts[] = valueBins(attr_index);
	      double ent = entropy();

	      for(int i=0; i<A.size(); i++)
	      {
		  double ratio = ((double)counts[i])/((double)instances.size());
		  if(instances.size() == 0) ratio = 0;

		  DatasetSample examples_v = subset(attr_index, A.getValueName(i));

		  //Un-comment to see a nice file output of the subset.
//		  String output = examples_v.toString();
//		  Utils.writeStringToFile(output, "parsedDataset_v.dat");

		  ent = ent - ratio * examples_v.entropy();

	      }
	      return ent;
	  }
	  /**
	   * Computes the entropy of the dataset. Entropy(S) = Summation(-p(I) log2 p(I)), where p(I) is proportion of instances belonging to class I. The summation goes over all class values
	   */
	   public double entropy()
	   {
	       AttributeSample class_attr = getAttribute(numberOfAttributes()-1);
	       int count_size = class_attr.size();
	       int counts[] = classValueBins();

	       double ent = 0;
	       for(int i=0; i<count_size; i++) {
		   double p_i = (double)counts[i] / (double)size();
		   if(size() == 0) p_i = 0;
		   double log_2;
		  if(p_i != 0) log_2 = Math.log(p_i) / Math.log(2);//computes log base 2.
		  else log_2 = 0.0;
		  ent = ent - p_i*log_2;
	       }
	       return ent;
	   }
	  /**
	  * Return the attribute index of the attribute with highest information gain.
	  */
	  public int chooseSplitAttribute()
	  {
	      double max = Double.MIN_VALUE;
	      int best_index = 0;

	      for(int i=0; i<attributes.size()-1; i++)
	      {
		  if(getAttribute(i).ignore()) continue;
		  double  infogain =infoGain(i);
		  if( infogain > max) {
		      max = infogain;
		      best_index = i;
		  }
	      }
	      return best_index;
	  }
         /**
         * Majority Value returns the value of class attribute that appears more times in the dataset.
         */
        public String majorityValue()
        {
          AttributeSample class_attr = getAttribute(numberOfAttributes()-1);
	  int count_size = class_attr.size();
          int counts[] = classValueBins();

          //Gets the majority Value.
          int max = Integer.MIN_VALUE;
          int majority_index = -1;
          for(int i=0; i<count_size; i++) {
            if(counts[i] > max) {
              max = counts[i];
              majority_index = i;
            }
          }
          return (String) class_attr.getValueName(majority_index);

        }


 /**
  * This method compute the number of instances in the dataset that has value at attribute A.
  * It stores the results in the array counts and return it;
  */
	private int [] valueBins(int attr_index)
	{
	    AttributeSample attr = getAttribute(attr_index);
//	    AttributeSample class_attr = getAttribute(numberOfAttributes()-1);

	  //Will store the number of times that each class value appears.
	  int  counts[] = new int[attr.size()];


	  for(int i=0; i<attr.size(); i++) { counts[i] = 0;}

	  if(instances.size() == 0) return counts;

	  ListIterator li = instances.listIterator(0);
	  InstanceSample inst = (InstanceSample) li.next();

	  //Count ocurrences of each class value and store it in counts;
	  while(true)
	  {
	    //This will return the index of the attribute value of the current instance.
	      String aux =  inst.getValue(attr_index);
	      int value_index = attr.getValueIndex(aux);

	      counts[value_index]++;

	      if(li.hasNext()) inst = (InstanceSample) li.next();
	      else break;
	  }
	  return counts;
	}
 /**
  * This method compute the number of times each class value appears in the dataset. Output: an int array int [] with the counts.
  */
	private int [] classValueBins()
        {
	    return valueBins(numberOfAttributes()-1);
	}

	/**
	 * returns the number of instances in the dataset.
	 */
	public int size()
        {
                return instances.size();
        }

	/**
	 * returns the number of attributes in the dataset.
	 */
	public int numberOfAttributes()
	{
		return attributes.size();
	}

	/**
	 * Adds an attribute to the dataset.
	 */
	public void addAttribute(AttributeSample n_attr){
		attributes.addElement(n_attr);
	}

	/**
	 * Retrieves the index-th attribute from the dataset.
	 */
	public AttributeSample getAttribute(int index){
		return (AttributeSample) attributes.elementAt(index);
	}

	/**
	 * Recall that instances is a LinkedList. Thus, this method is very un efficient.
	 */
	public InstanceSample getInstance(int index){
	    return (InstanceSample) instances.get(index);
	}

	/**
	 * Set mark = false for all attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
	 */
        public void unmarkAllAttributes()
        {
          for(int i=0; i<attributes.size(); i++)
          {
            AttributeSample attr = getAttribute(i);
            attr.unmark();
          }
        }

        /**
         * Set mark = false for the i-th attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
         */
        public void markAttribute(int i)
        {
          AttributeSample attr = getAttribute(i);
          attr.mark();
        }

        /**
         * Return the mark flag of the i-th attributes. This mark is used for classification. If an attribute is marked, then it has already been classified.
         */
        public boolean marked(int i)
        {
          AttributeSample attr = getAttribute(i);
          return attr.marked();
        }

        /**
         * Return true if all attributes are marked (except the last one, which doesn't mater, recall it is the class attribute).
         */
        public boolean allMarked()
        {
          boolean ret = true;

          for(int i=0; i<attributes.size(); i++)
          {
            AttributeSample attr = getAttribute(i);
            if(!attr.marked()) ret = false;
          }
          return ret;
        }

        /**
         * Returns a class attribute value. It is assumed that the class attribute is the last attribute.
         * Input: the value index.
         * Output: The index-th value of the class attribute (last one).
         */
        public String getClassValue(int index)
        {
          AttributeSample attr = getAttribute(attributes.size()-1);
          return attr.getValueName(index);
        }

	/** This version receives an instance in the internal representation.
	 */
        public void addInstance(InstanceSample inst)
	{
		instances.add(inst);
	}

        /** Removes instance from the Linked List. It does not frees the instance itself from memory.
         */
        public boolean removeInstance(InstanceSample inst)
        {
          return instances.remove(inst);
        }

        /** Reads the dataset from fileName.
         */
        public void readDataset(String fileName)
        {
          BufferedReader reader = null;
          try{
            reader = new BufferedReader(new FileReader(fileName));
          }
          catch(Exception ioe){}

          readDataset(reader);
        }
	/** Reads the dataset from the reader given by the calling function.
	 * It is simply a loop line by line until the end of the file.
	 * Each line is parsed as an instance.
	 */
        public void readDataset(BufferedReader reader)
        {
          StreamTokenizer tokenizer = new StreamTokenizer(reader);

               tokenizer.resetSyntax();
               tokenizer.whitespaceChars(0, ' ');
               tokenizer.wordChars(' '+1,'\u00FF');
               tokenizer.whitespaceChars(',',',');
               tokenizer.commentChar('|');
               tokenizer.eolIsSignificant(true);

               try{tokenizer.nextToken();}
               catch (Exception te){}
               String token = tokenizer.sval;

               int count = 0;
               while (tokenizer.ttype != StreamTokenizer.TT_EOF)
               {
                 int aux = numberOfAttributes();
                 //Ignore attributes with ignore=true
     /*	    for(int i=0; i<numberOfAttributes(); i++)
                 {
                     if(getAttribute(i).ignore()) aux--;
                 }
       */          InstanceSample inst = new InstanceSample(name + String.valueOf(count), aux);

                 //Add values to new instance;
                 int value_count = 0;
                 while(tokenizer.ttype != StreamTokenizer.TT_EOL && tokenizer.ttype != StreamTokenizer.TT_EOF){
                   //Checks if value is missing. If so, we add that value to the attribute values list.
                   if(token.equalsIgnoreCase("?"))
                   {
                     AttributeSample aux_attr = (AttributeSample) attributes.elementAt(value_count);
                     if(!aux_attr.hasValue("?")) aux_attr.addValue("?");
                   }
                   //What if token is not exactly equal to the values stored in Names file. i.e. "08" != "8".
                   //To solve that, both here and in parsing the names file, I converted it double and back to Strings.

                   inst.addValue(token);

                   try{tokenizer.nextToken();}
                   catch (Exception te){}
                   token = tokenizer.sval;

                   value_count++;
                 }

                 try{tokenizer.nextToken();}
                 catch (Exception te){}
                 token = tokenizer.sval;
                 count++;

                 //Stores new instance in dataset;
                 addInstance(inst);
               }
               postProcessLinearValues();


        }
        //At this point, we have all the information needed to deal with the case of "continuous" or "linear" values

        /**
         * This procedure post process the dataset for the case of "linear" or "continuous" attribute values.
         */
        private void postProcessLinearValues()
        {
          String value_name;
          AttributeSample aux_attr;
          double min, max;

          for(int i=0; i<attributes.size(); i++)
          {
            aux_attr = (AttributeSample) attributes.elementAt(i);
            //If the attribute has a linear value, then "linear" is the first value name in
            //the list of value names of attribute.
            value_name = (String) aux_attr.getValueName(0);
            if(value_name.equalsIgnoreCase("linear"))
            {
              //If it is linear, then we retrieve the min and max values along all instances.
              min = Double.MAX_VALUE;
              max = Double.MIN_VALUE;

	      ListIterator li = instances.listIterator(0);
              InstanceSample aux_inst = (InstanceSample) li.next();

              //Sets the minimum and maximum to the value of first instance
              while(li.hasNext())
              {
                //If the value is "?" we should skip it.
                String aux_str = aux_inst.getValue(i);
                if(aux_str.equalsIgnoreCase("?")) {
                  aux_inst = (InstanceSample) li.next();
                  continue;
                }

                double aux_int = Double.parseDouble(aux_str);

                if(aux_int <= min) min = aux_int;
                if(aux_int >= max) max = aux_int;

                aux_inst = (InstanceSample) li.next();
//                it_index = li.nextIndex();
              }
              min = min;//For debugging purposes
              max = max; //For debugging purposes


              //Discretize the attribute
              aux_attr.discretizeValues(min, max);

              //Update the linear values in the dataset.
              li = instances.listIterator(0);
              aux_inst = (InstanceSample) li.next();

              while(li.hasNext())
              {
                String aux_str = aux_inst.getValue(i);
                if(aux_str.equalsIgnoreCase("?"))
                {
//                  aux_inst.setValue("?", i);
                  aux_inst = (InstanceSample) li.next();
                  continue;
                }

                double aux_int = Double.parseDouble(aux_str);
                String value = aux_attr.getBinName(aux_int);
                aux_inst.setValue(value, i);

                aux_inst = (InstanceSample) li.next();

              }
            }
          }
        }


        /** Reads the dataset from the reader given by the calling function.
         */
        public void parseNamesFile(String fileName)
        {
          BufferedReader reader = null;
          try{
            reader = new BufferedReader(new FileReader(fileName));
          }
          catch(Exception ioe){}

          parseNamesFile(reader);
        }

	/** This function receives a string version of the Names File and
	 * convert it to the internal representation, that is, it creates
	 * the corresponding attribute structure.
	 */
	public void parseNamesFile(BufferedReader reader)
	{
		//int linePos = 0;
		AttributeSample n_attr;
		String attrName, token;
		StreamTokenizer tokenizer = new StreamTokenizer(reader);

		tokenizer.resetSyntax();
		tokenizer.whitespaceChars(0, ' ');
		tokenizer.wordChars(' '+1,'\u00FF');
		tokenizer.whitespaceChars(',',',');
		tokenizer.whitespaceChars('.','.');
		tokenizer.commentChar('|');
		tokenizer.eolIsSignificant(true);

                try {tokenizer.nextToken();}
                catch (Exception te){}
                token = tokenizer.sval;

                //Skips the first line that contains "ClassField."
                while(tokenizer.ttype != tokenizer.TT_EOL){
                  try{tokenizer.nextToken();}
                  catch (Exception te){}
                  token = tokenizer.sval;
                }

                //Reads until end of file
                //Each iteration of the loop is basically parsing one line
                //That is, at the beggining of the loop, the tokenizer should
                //point to the first pointer in the line
                while(tokenizer.ttype != tokenizer.TT_EOF)
                {
                  //Reads attribute name.
                  attrName = "PEPE";
                  attrName = token = "";
                  while(token.indexOf(':') == -1){
                    try{tokenizer.nextToken();}
                    catch (Exception te){}
                    token = tokenizer.sval;

                    attrName = attrName + " " + token;
                  }
                  attrName = attrName.substring(1, attrName.length()  -1 );
//                  attrName = attrName + token;
                  n_attr = new AttributeSample(attrName);

                  try{tokenizer.nextToken();}
                  catch (Exception te){}
                  token = tokenizer.sval;


                  while(tokenizer.ttype != tokenizer.TT_EOL && tokenizer.ttype != tokenizer.TT_EOF)
                  {

                    if(token.equalsIgnoreCase("date") ||
                       token.equalsIgnoreCase("time") ||
                       token.equalsIgnoreCase("ignore") ||
                       token.equalsIgnoreCase("label") 	//AttributeSample dont used for classification.
                       )
                    {
			n_attr.addValue("ignore");
			n_attr.ignore(true);

			try{tokenizer.nextToken();}
			catch (Exception te){}
			token = tokenizer.sval;

			continue;
                    }
                    //Take care of linear/continuous case.
                    //It will be processed later, when the dataset is readed.
                    //This is because we need min and max.
                    if(token.equalsIgnoreCase("linear")) {
			n_attr.addValue("linear");

			try{tokenizer.nextToken();}
			catch (Exception te){}
			token = tokenizer.sval;
			continue;
                    }
                    n_attr.addValue(token);

                    try{tokenizer.nextToken();}
                    catch (Exception te){}
                    token = tokenizer.sval;
                  }
                  addAttribute(n_attr);

                }
	}

        /**
        * Unparse to my format, this procedure works paired with myUnserialize.<!-- -->
        * The output string stores the current state of the calling object. This
        * string can be used later to set the state of another object to the same state
        * of the original object.<!-- --> Note that this is not storing objects, but only their
        * state (current value of internal fields).
        * @return a String.
        */

        public String mySerialize(Parser parser)
        {
//          Parser parser = new Parser(pd, ed, vd, sd);

          Vector vblock = new Vector();

          vblock.addElement(new Block("DATASCHEMA", super.mySerialize(parser)));

          //sum of counts block
          Block blk2 = new Block("DATAINSTANCES");
          InstanceSample inst = getFirstInstance();
          while(inst != null){
            blk2.addValue(inst, parser);

            inst = getNextInstance();
          }
          vblock.addElement(blk2);


          return parser.filter(parser.unparseBlocks(vblock));
        }

        /**
           * Parse to my format, this procedure works paired with mySerialize.<!-- -->
           * The string output from this procedure is used to change the state of
           *  internal
           * field values of the current calling object.
           * @return a String.
           */

public void myUnserialize(String inputStr, Parser parser)
{
 //         Parser parser = new Parser (pd, ed, vd, sd);
          Vector vblock = parser.parse(parser.unfilter(inputStr));

          for(int i=0; i<vblock.size(); i++)
          {
            Block blk = (Block) vblock.get(i);

            if(blk.tag.equals("DATASCHEMA")) {
              super.myUnserialize(blk.getString(0), parser);
            }

            if(blk.tag.equals("DATAINSTANCES"))
            {
              instances = null;
              instances = new LinkedList();
              for(int j=0; j<blk.size(); j++)
              {
                InstanceSample inst = (InstanceSample) blk.getObject(j, new InstanceSample(), parser);
                //AttributeSample attr = new AttributeSample("garbage");
                //blk.getObject(j, attr, parser);
                instances.add(inst);
              }
            }
          }

        }
      }