package stringSerializer.examples;


import java.util.*;
import java.io.*;

import stringSerializer.*;
/**
 * Abstraction for an instance (a row in the dataset).<!-- --> It contains a name and a set of attribute values.
 */
public class InstanceSample implements ImySerializable
{

    /**  name of the instance.  */
    	public String name;

    /**  Vector of values.  */
    	private Vector values;

      public InstanceSample(){;};
     /**
      * Constructor
      */
        public InstanceSample(String _name, int numb_of_values){
	    name = _name;
	    values = new Vector(numb_of_values, 5);
	}
	/**
	 * Constructor
	 */
	public InstanceSample(String _name){
		name = _name;
	}

	/**
	 * makeCopy copies the instance
	 */
	public InstanceSample makeCopy()
	{
	    InstanceSample newI = new InstanceSample(name, size());
	    for(int i=0; i<values.size(); i++) newI.addValue(getValue(i));

	    return newI;
	}
	/**This function receives the attribute object for the index-th
	 * column in the dataset. From this attribute object it extracts
	 * the value names for each bin, which will become the values
	 * themself, since each value is discretized.
	 */
	private void setDiscretizedValue(AttributeSample attr, int value, int index)
	{
		int numValues = attr.size();
		for(int i=0; i<numValues; i++) {
			//Here I am assuming that the value cannot be outside the range
			//originally passed to attribute (i.e. min <= value <= max)
			if(value < attr.getValueBinEnd(i)) {
				setValue(attr.getValueName(i), index);
				break;
			}
		}
	}

	/**
	 * Returns the number of values in the instance.
	 */
	public int size(){return values.size();}

	/**
	 * Adds a value.
	 */
        public void addValue(String value)
        {
	    values.addElement(value);
        }

	/**
	 * Changes the index-th value.
	 */
	public void setValue(String value, int index)
        {
	    values.setElementAt(value, index);
        }

	/**
	 * Returns the index-th value.
	 */
	public String getValue(int index)
	{
	    return (String) values.elementAt(index);
	}

	/**
	 * Used for classifiers where it is assumed that the class attribute is the last one.<!-- -->
	 * Returns then the last value.
	 */
        public String getClassValueString()
        {
	    return (String) values.elementAt(values.size()-1);
        }

	/**
	 * Returns true if the input value is the value of the last attribute (the class attribute).
	 */
	public boolean isClassValue(String val)
	{
	    String val2 = SSUtils.standarizedDoubleString(val);
	    return val2.equalsIgnoreCase(SSUtils.standarizedDoubleString(getClassValueString()));
	}

	/**
	 * Returns a string representation of the instance.
	 */
	public String toString()
	{
	    return internalToString(false);
	}
	public String toString(boolean noInstanceValues)
	{
	    return internalToString(noInstanceValues);
	}

	/**
	 * Used internally by toString method.
	 */
	private String internalToString(boolean noInstanceValues)	{
	    String aux = "";
	    if(!noInstanceValues)
	    {
		aux =": ";
		for(int i=0; i<size(); i++)
		{
		    aux = aux + values.elementAt(i) + " ";
		}
	    }
	    return name + aux;
	}

        /**
        * Unparse to my format, this procedure works paired with myUnserialize.<!-- -->
        * The output string stores the current state of the calling object. This
        * string can be used later to set the state of another object to the same state
        * of the original object.<!-- --> Note that this is not storing objects, but only their
        * state (current value of internal fields).
        * @return a String.
        */

        public String mySerialize(Parser parser)
        {
//          Parser parser = new Parser(pd, ed, vd, sd);

          Vector vblock = new Vector();

          //names block
             Block blk = new Block("INSTANCENAME", name);
             vblock.addElement(blk);


          //
          //values = null;
          Block blk2 = new Block("INSTANCEVALUES");
          for(int i=0; i< values.size(); i++){
            blk2.addValue((String)values.get(i));
          }
          vblock.addElement(blk2);


          return parser.filter(parser.unparseBlocks(vblock));
        }

        /**
           * Parse to my format, this procedure works paired with mySerialize.<!-- -->
           * The string output from this procedure is used to change the state of
           *  internal
           * field values of the current calling object.
           * @return a String.
           */

public void myUnserialize(String inputStr, Parser parser)
{
  Vector vblock = parser.parse(parser.unfilter(inputStr));

  for(int i=0; i<vblock.size(); i++)
  {
    Block blk = (Block) vblock.get(i);

  if(blk.tag.equals( "INSTANCENAME")) name = blk.getString(0);

    if(blk.tag.equals("INSTANCEVALUES"))
    {
      values = null;
      values = new Vector();
      for(int j=0; j<blk.size(); j++)
      {
        values.addElement(blk.getString(j));
      }
    }

  }

}


}