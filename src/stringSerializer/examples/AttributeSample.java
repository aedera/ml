package stringSerializer.examples;


import java.util.*;
import java.io.*;

import stringSerializer.*;

/**
 * Class that implements the behavior of a classifier's attribute.<!-- --> It encapsulates values Vector and other flags.
 *
 * @author Facundo Bromberg
 */
public class AttributeSample implements ImySerializable
{
    /**
     * Name of the attribute.
     */
    public String name;
    /**
     * Values vector.
     */
    private Vector valuesNames;
    /**
     * If true, this attribute should not be used for classification (use only by classifiers).
     */
    private boolean _ignore;
    /**
     * Mark used for future classification. If the attribute is marked, it has already been used
     * for classification (use only by classifiers).
     */
    private boolean marked;

    /**
     * Only for linear/continuous attributes.<!-- --> If stores the discretized bins.
     */
    private Vector valuesBinStart;
    /**
     * Only for linear/continuous attributes.<!-- --> Number of bins.
     */
    private double binSize;

    public AttributeSample(){;};
    /**
     * Constructor.
     * @param _name  The name of the attribute
     */
    public AttributeSample(String _name){
	name = _name;
	valuesNames = new Vector(0, 5);
	_ignore = false;
	marked = false;
	valuesBinStart = null;
	binSize = -1.0;
    }

    /**
     *
     * @return the name of the attribute
     */
    public String name()
    {
      return name;
    }

    /**
     * Creates a string representation of the attribute.
     * @return a string representation of the attribute
     */
    public String toString()
    {
	String aux = "";
	for(int i=0; i<size(); i++)
	{
	    aux = aux + getValueName(i) + " ";
	}
	return name + "(" + binSize +"): " + aux;
    }
    /**
     * Set method.
     * @param ign A boolean for setting the ignore flag.
     */
    public void ignore(boolean ign) {_ignore = ign;}
    /**
     * Get method.
     * @return the state of the ignore flag.
     */
    public boolean ignore() {return _ignore;}

    /**
     * Set marked to true.
     */
    public void mark() {marked = true;}
    /**
     * Set marked to false.
     */
    public void unmark() {marked = false;}
    /**
     * Get method.
     * @return status of marked flag.
     */
    public boolean marked() {return marked;}

    /**
     * Transforms a linear attribute in a discretized 5 values attribute: "Tiny, small, medium, large and Huge".
     * @param min Minimum value among all instances in the dataset.<!-- --> The idea is to call this method after the dataset was completely read.
     * @param max Maximum value among all instances in the dataset.<!-- --> The idea is to call this method after the dataset was completely read.
     */
    public void discretizeValues(double min, double max)
    {
          valuesNames.removeElementAt(0);  //We eliminate the value "linear"
	  addValue("huge",0);
	  addValue("large",0);
	  addValue("medium",0);
	  addValue("small",0);
	  addValue("tiny",0);

	  valuesBinStart = new Vector(0, 5);
	  binSize = (max-min)/5.0;
	  Double arg = new Double(min);
	  valuesBinStart.addElement(arg);
          arg = new Double(min + binSize);
          valuesBinStart.addElement(arg);
          arg = new Double(min + 2*binSize);
          valuesBinStart.addElement(arg);
          arg = new Double(min + 3*binSize);
          valuesBinStart.addElement(arg);
          arg = new Double(min + 4*binSize);
          valuesBinStart.addElement(arg);

        }

	/**
	 * Transforms linear values into discretized values according to the bin the fall into.
	 * @param value Numerical value to be transformed into a discretized value.
	 * @return "Error" in case of un-bounded value (value that do not fall into any bin).
	 */
        public String getBinName(double value)
        {
          for(int i=valuesBinStart.size() - 1; i>=0; i--)
          {
            if(value >= getValueBinStart(i)) return getValueName(i);
          }
          return "Error";
        }

	/**
	 * @returns Number of discrete values.
	 */
	public int size()
	{
		return valuesNames.size();
	}

	/**
	 * @return the left boundary of the index-th bin
	 */
	public double getValueBinStart(int index)
	{
          Double aux = (Double)valuesBinStart.elementAt(index);
          return aux.doubleValue();
	}

	/**
	 * @return the right boundary of the index-th bin
	 */
	public double getValueBinEnd(int index)
	{
		return getValueBinStart(index) + binSize;
	}

	/**
	 * Add a value to the attribute.
	 * @param name Name for the new value
	 * @param index Position at where to insert the new value
	 */
	public void addValue(String name, int index)
	{
	  valuesNames.insertElementAt(name, index);
	}

	/**
	 * Add the new value at beggining of value list.
	 */
	public void addValue(String name)
	{
          valuesNames.addElement(name);
	}

	/**
	 * Checks if the attribute contains the given value.
	 * @param name Value name to check upon.
	 * @return true if it contains a value equal to name.
	 */
        public boolean hasValue(String name)
        {
          for(int i=0; i<valuesNames.size(); i++)
          {
            String auxStr = (String) valuesNames.elementAt(i);
            if(auxStr.equalsIgnoreCase(name)) return true;
          }
          return false;
        }

	/**
	 * Changes the name of the index-th value.
	 */
	public void setValueName(String name, int index)
	{
		valuesNames.setElementAt(name, index);
	}

	/**
	 * @return value of the index-th value.
	 */
	public String getValueName(int index)
	{
	    return (String) valuesNames.elementAt(index);
	}

	/**
	 * Returns the index in the value list which contains a value-tag equal to input parameter <value>.
	 */
        public int getValueIndex(String value)
	{
	    if(ignore()) return 0;

	    boolean valueIsNumber = true;
	    double doubleValue = -1;

	    doubleValue = SSUtils.myParseDouble(value);
	    for(int i=0; i<size(); i++) {
		//The following conditionals take care of the case of non-numerical attribute values.
		if(doubleValue != Double.MIN_VALUE) {
		    double current_db = SSUtils.myParseDouble((String)valuesNames.elementAt(i));
		    if(current_db != Double.MIN_VALUE){
			if(doubleValue == current_db) return i;
		    }
		}
		else {

		    String aux = (String)valuesNames.elementAt(i);
		    if(value.equalsIgnoreCase(aux)) return i;
		}
	    }
	    return -1;
        }

        /**
* Unparse to my format, this procedure works paired with myUnserialize.<!-- -->
* The output string stores the current state of the calling object. This
* string can be used later to set the state of another object to the same state
* of the original object.<!-- --> Note that this is not storing objects, but only their
* state (current value of internal fields).
* @return a String.
*/

            public String mySerialize(Parser parser)
            {
//              Parser parser = new Parser (pd, ed, vd, sd);

              Vector vblock = new Vector();

              //names block
              Block blk = new Block("ATTRNAME", name);
              vblock.addElement(blk);


              //Generating valuesNames
              Block blk2 = new Block("ATTRVALUESNAMES");
              for(int i=0; i<valuesNames.size(); i++)
              {
                blk2.addValue((String)valuesNames.get(i));
              }
              vblock.addElement(blk2);

              //Generating ignore
              Block blk3 = new Block("ATTRIGNORE");
              if(_ignore) blk3.addValue("TRUE");
                else blk3.addValue("FALSE");

              vblock.addElement(blk3);

              //Generating marked
              Block blk4 = new Block("ATTRMARKED");
              if(marked) blk4.addValue("TRUE");
                else blk4.addValue("FALSE");

              vblock.addElement(blk4);


              //Generating valueBinSizes
              if(valuesBinStart != null)
              {
                Block blk5 = new Block("ATTRVALUESBINSTART");
                for(int i=0; i<valuesBinStart.size(); i++)
                {
                  Double dbl = (Double)(valuesBinStart.get(i));
                  blk5.addValue(dbl.doubleValue());
                }
                vblock.addElement(blk5);
              }

              //Generating binSized
              Block blk6 = new Block("ATTRBINSIZE", binSize);
              vblock.addElement(blk6);

              return parser.filter(parser.unparseBlocks(vblock));
            }

            /**
           * Parse to my format, this procedure works paired with mySerialize.<!-- -->
           * The string output from this procedure is used to change the state of
           *  internal
           * field values of the current calling object.
           * @return a String.
           */

    public void myUnserialize(String inputStr, Parser parser)
    {
//      Parser parser = new Parser (pd, ed, vd, sd);
      Vector vblock = parser.parse(parser.unfilter(inputStr));

      valuesBinStart = null;

      for(int i=0; i<vblock.size(); i++)
      {
        Block blk = (Block) vblock.get(i);

        if(blk.tag.equals( "ATTRNAME")) name = blk.getString(0);

        else if(blk.tag.equals("ATTRVALUESNAMES"))
        {
          if(valuesNames != null) valuesNames.clear();
          else valuesNames = new Vector();
          for(int j=0; j<blk.size(); j++)
          {
            valuesNames.addElement(blk.getString(j));
          }
        }

        else if(blk.tag.equals( "ATTRIGNORE"))
        {
          if(blk.getString(0).equals( "TRUE")) _ignore = true;
          else _ignore = false;
        }
        else if(blk.tag.equals( "ATTRMARKED"))
        {
          if(blk.getString(0) == "TRUE") marked = true;
          else marked = false;
        }
        else if(blk.tag.equals( "ATTRVALUESBINSTART"))
        {
          valuesBinStart.clear();
          for(int j=0; j<blk.size(); j++)
          {
            Double dbl = new Double(blk.getDouble(j));
            valuesNames.addElement(dbl);
          }

        }

        //Generating binSized
        else if(blk.tag.equals( "ATTRBINSIZE")) binSize = blk.getDouble(0);


      }


    }

  }