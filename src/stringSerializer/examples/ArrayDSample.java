package stringSerializer.examples;
/*
/*
 *    ArrayD.java
 *    @author Facundo Bromberg. 2002.
 */

import java.io.*;
import java.util.*;

import stringSerializer.*;
/**
 * Class implementing a multidimensional array.
 *
 * The running time of core methods (set and get) is linear in the number of dimensions.
 *
 *
 * @author Facundo Bromberg
 */
public class ArrayDSample implements ImySerializableContainer{

  /** Higher vector in the hierarchy . */
  protected Vector m_array;

  /** Number of dimensions. */
  protected int [] dimensions;

  public ArrayDSample()
  {
    dimensions = null;
    m_array = null;
  }
  /**
   * Initializes the arrayD.
   *
   * @param dims the number of dimensions for the new arrayD.
   */
  public ArrayDSample(int [] dims) {
	  dimensions = new int[dims.length];

	  for(int i=0; i<dims.length; i++) {
		dimensions[i] = dims[i];
	  }
	  m_array = generateArray(dimensions);
  }
  /*-----------------------------------------------------------*/
  /**
   * Util method that retrieves the next coordinate in a standard enumeration fashion.
   *
   * @param coord the coordinates for which we wants the next one.
   * @return the next coordinates in the standard enumeration (not binary, but given by the dimensions)
   */
  public int [] getNextCoord(int [] coord){
	  int [] newcoord = new int[coord.length];
	  int k=-1;

	  for(int i=0; i<dimensions.length; i++) newcoord[i] = coord[i];
	  k=dimensions.length - 1;
	  while(k >= 0 && newcoord[k]+1 == dimensions[k]){
		  newcoord[k] = 0;
		  k--;
	  }
	  if(k < 0) return null;
	  else newcoord[k] += 1;

	  return newcoord;
  }
  /*-----------------------------------------------------------*/
  /**
   * Util method that retrieves the next coordinate in a standard enumeration fashion keeping fixed the
   * coordinate at dimension dim.
   *
   * @param coord the coordinates for which we wants the next one.
   * @param dim fow which we do not increase the coordinate;
   * @return the next coordinates in the standard enumeration (not binary, but given by the dimensions)
   */
  public int [] getNextCoord(int [] coord, int dim)
  {
	  int [] newcoord = new int[coord.length];
	  int k=-1;

	  for(int i=0; i<dimensions.length; i++) newcoord[i] = coord[i];
	  k=dimensions.length - 1;
	  if(k == dim) k--;
	  while(k >= 0 && newcoord[k]+1 == dimensions[k])
	  {
	      //skip the dimension dim.
	      if(k==dim)
	      {
		  k--;
		  if(k < 0) break;
		  else continue;
	      }
	      newcoord[k] = 0;
	      k--;
	  }
	  if(k < 0) return null;
	  else newcoord[k] += 1;

	  return newcoord;
  }
  /*-----------------------------------------------------------*/
  /**
   * Util method that increments the coordinate of dimension dim by one.
   *
   * @param coord the coordinates for which we wants the next one.
   * @param dim dimension to which increase the coordinate by one.
   * @return true if the increment was performed, false otherwise.
   */
  public boolean incrementCoord(int [] coord, int dim)
  {
      if(dim >= dimensions.length) return false;
      if(coord[dim]+1 == dimensions[dim]) return false;

      coord[dim] = coord[dim]+1;
      return true;
  }

 /*---------------------------------------------------*/
  /**
   * Retrieves the Object stored in the coordinates given as input.
   **/
  public Object get(int [] coordinates) {
	return  getElementInternal(m_array, coordinates);
  }
 /*---------------------------------------------------*/
  /**
   * Retrieves the Object stored in the coordinates given as input pre-casted us int.
   **/
  public int getInt(int [] coordinates) {
	return  ((Integer) getElementInternal(m_array, coordinates)).intValue();
  }
/*---------------------------------------------------*/
  /**
   * Retrieves the Object stored in the coordinates given as input pre-casted us double.
   **/
  public double getDouble(int [] coordinates) {
	return  ((Double) getElementInternal(m_array, coordinates)).doubleValue();
  }
/*---------------------------------------------------*/
  /**
   * Stores in coordinates, the Object newvalue.
   **/
  public void set(int [] coordinates, Object newvalue) {
	  setElementInternal(m_array, coordinates, newvalue);
  }
/*---------------------------------------------------*/
  /**
   * Stores in coordinates, the int newvalue.
   **/
  public void setInt(int [] coordinates, int newvalue) {
	  Integer aux = new Integer(newvalue);
	  setElementInternal(m_array, coordinates, (Object) aux);
  }
/*---------------------------------------------------*/
  /**
   * Stores in coordinates, the double newvalue.
   **/
  public void setDouble(int [] coordinates, double newvalue) {
	  Double aux = new Double(newvalue);
	  setElementInternal(m_array, coordinates, (Object) aux);
  }
/*---------------------------------------------------*/
  /**
   * Increments the value stored in coordinate by one.
   * This method has an unpredictable behaviour for non integer inputs.
   */
  public void increaseElementByOne(int [] coordinates) {
	increaseElementByOneInternal(m_array, coordinates);
  }
/*---------------------------------------------------*/
  /**
   * Returns the size of the array along the dimension d.
   */
  public int getDimensionLength(int d) {
	return dimensions[d];
  }
/*---------------------------------------------------*/
  /**
   * Returns the number of dimensions.
   */
  public int getDepth() {
	return dimensions.length;
  }
/*---------------------------------------------------*/
  /**
   * Returns the total size of the array, given by d1*d2*......*dk, where di are the
   * size of each dimension and k is the number of dimensions.
   */
  public int totalEntries() {
	  int tot = dimensions[0];
	  for(int i=1;i<dimensions.length; i++) tot *= dimensions[i];

	  return tot;
  }
/*---------------------------------------------------*/
  /**
   * Returns an array A with the size of each dimension, such that A[i] = getDimensionLength(i)
   */
  public int [] getDimensionsArray() {
	  int [] newdim = new int[dimensions.length];
	  for(int i=0;i<dimensions.length; i++) newdim[i] = dimensions[i];

	  return newdim;
  }
/*-------------------------------------------------------*/
  /**
   *  Helpful method that removes an element from an array and returns the resulting array.<!-- -->
   * So for example, if array = (1, 2, 3, 4) and dim=0, the output is = (2, 3, 4).
   */
  public int [] minus(int [] array, int dim) {
    int [] newarray = new int[array.length - 1];
    for(int i=0; i<dim; i++) newarray[i] = array[i];
    for(int i=dim; i<(array.length-1); i++) newarray[i] = array[i+1];

    return newarray;
  }
  /*---------------------------------------------------*/
  /**
   *  Private method that generates the array, given the dimensions array as input. It is used
   * by the constructor.
   */
  private Vector generateArray(int [] dims){
    Vector vec = new Vector();
    if(dims.length == 1) {
      for(int i=0; i<dims[0]; i++) {
        //initialize the array with dummy Objects.
        Integer zero = new Integer(0);
        vec.addElement(zero);
      }
      return vec;
    }
    else {
      int [] y = minus(dims, 0);

      for(int i=0; i<dims[0]; i++) {
        vec.addElement(generateArray(y));
      }
    }
    return vec;
  }
/*---------------------------------------------------*/
  private Object getElementInternal(Vector vec, int [] coordinates) {
		if(coordinates.length == 1) {
			return vec.elementAt(coordinates[0]);
		}
		else {
			int [] y = minus(coordinates, 0);

			return getElementInternal((Vector)vec.elementAt(coordinates[0]), y);
		}
  }
/*---------------------------------------------------*/
/**
 *  Linear in the number of dimentions.
 */
  private void setElementInternal(Vector vec, int [] coordinates, Object newvalue) {
		if(coordinates.length == 1) {
			vec.setElementAt(newvalue, coordinates[0]);
		}
		else {
			int [] y = minus(coordinates, 0);

			setElementInternal((Vector) vec.elementAt(coordinates[0]), y, newvalue);
		}
  }
/*---------------------------------------------------*/
/**
 *  Linear in the number of dimentions.
 */
  private void increaseElementByOneInternal(Vector vec, int [] coordinates) {
	    if(coordinates.length == 1) {
			int newvalue = ((Integer)vec.elementAt(coordinates[0])).intValue();
			Integer newValue = new Integer(newvalue + 1);
			vec.setElementAt((Object)newValue, coordinates[0]);
		}
		else {
			int [] y = minus(coordinates, 0);

			increaseElementByOneInternal((Vector) vec.elementAt(coordinates[0]), y);
		}
  }

  /**
   * Returns a formated string output of the arrayD.
   */
  public String toString()
  {
    String ret = "\nMULTI-DIMENSIONAL ARRAY:\n";
          ret += "-----------------------\n\n";

    //Dimensions
    ret += "DIMENSIONS' VECTOR: (";
    for(int i=0; i<dimensions.length-1; i++) {
      ret += dimensions[i] + ",";
    }
    ret += dimensions[dimensions.length-1] + ")\n";

    int [] coord = this.getDimensionsArray();
    for(int i=0 ;i< coord.length; i++) coord[i] = 0;

    ret += "DATA (" + get(coord).getClass().toString() +") : \n";

    Vector vec = (Vector) toStringInternal(m_array, dimensions);
    for(int i=0; i<vec.size(); i++) {
      ret += (String)vec.get(i) + "\n";
    }

    return ret;
  }

  /**
   * Returns a vector of Strings, one per element in the input vector.
   */
  private Vector toStringInternal(Vector vector, int [] dims)
  {
    Vector ret  = new Vector();

    if(dims.length == 1) {
      for(int i=0; i<dims[0]; i++) {
        //Integer zero = (Integer) ;
        ret.addElement(i + ": " + m_array.get(i).toString());
      }
    }
    else {
      int[] y = minus(dims, 0);

      for (int i = 0; i < dims[0]; i++) {
        Vector out = toStringInternal((Vector)vector.get(i), y);
        //String str = "";
        for(int j=0; j < out.size(); j++) {
          //str += (String) out.get(j) + " ";
          ret.addElement(j + out.get(j).toString());
        }
      }
    }
    return ret;
  }
  /**
* Unparse to my format, this procedure works paired with myUnserialize.<!-- -->
* The output string stores the current state of the calling object. This
* string can be used later to set the state of another object to the same state
* of the original object.<!-- --> Note that this is not storing objects, but only their
* state (current value of internal fields).
* @return a String.
*/

     public String mySerialize(Parser parser)
     {
  //     Parser parser = new Parser (pd, ed, vd, sd);

       Vector vblock = new Vector();

       //m_Counts block
       Block blk = new Block("ARRAYDDIMENSIONS");
       for(int i=0; i<dimensions.length; i++) blk.addValue(dimensions[i]);
       vblock.addElement(blk);

       //sum of counts block
       Block blk2 = new Block("ARRAYDMARRAY");
       blk2.addValue(mySerializeVector(m_array, dimensions, parser));
       vblock.addElement(blk2);


       return parser.filter(parser.unparseBlocks(vblock));
     }

     /**
      * Internal method that recursively generates the string for the (multi-dimensional) Vector m_array.
      */
     private String mySerializeVector(Vector m_array, int [] dims, Parser parser)
     {
       Block blk = new Block("ARRAYDMARRAYNESTED" + dims.length);

       if(dims.length == 1) {
         for(int i=0; i<dims[0]; i++) {
//           Integer zero = (Integer) m_array.get(i);
//           attributes.get(i), parser
           blk.addValue(m_array.get(i), parser);
         }
       }
       else {
         int[] y = minus(dims, 0);

         String ret = "";
         Vector vblock = new Vector();
         for (int i = 0; i < dims[0]; i++) {
           blk.addValue(mySerializeVector( (Vector) m_array.get(i), y, parser));
         }
       }
       return blk.unparseBlock(parser.getpd(), parser.geted(), parser.getvd(), parser.getsd());
     }

     /**
   * Parse to my format, this procedure works paired with mySerialize.<!-- -->
   * The string output from this procedure is used to change the state of
   *  internal
   * field values of the current calling object.
   * @return a String.
   */

    public void myUnserialize(String inputStr, Object dummyClass, Parser parser)
    {
//      Parser parser = new Parser(pd, ed, vd, sd);
      Vector vblock = parser.parse(parser.unfilter(inputStr));

      for (int i = 0; i < vblock.size(); i++) {
        Block blk = (Block) vblock.get(i);

        if (blk.tag.equals("ARRAYDDIMENSIONS")) {
          dimensions = null;
          dimensions = new int[blk.size()];
          for(int j=0; j<blk.size(); j++) dimensions[j] = (int) blk.getDouble(j);
        }

        else if (blk.tag.equals("ARRAYDMARRAY")) {
          m_array = null;
          m_array = myUnserializeVector(blk.getString(0), dimensions, parser, dummyClass);
        }

      }
    }
    public void myUnserialize(String inputStr, Parser parser){;};

    private Vector myUnserializeVector(String inputStr, int [] dims, Parser parser, Object dummyClass)
    {
      Vector vblock = parser.parse(inputStr);
//      Block blk = new Block("ARRAYDMARRAYNESTED" + dims.length);
      Block blk = (Block) vblock.get(0);

      Vector vector = new Vector();

      //There should be only one block in vblock, with as many values as elements in that Vector.
      if(dims.length == 1) {
        for(int i=0; i<dims[0]; i++) {
          Object newObj = blk.getObject(i, dummyClass, parser);
//          int aux = (int) blk.getDouble(i);
 //         Integer zero = new Integer(aux);
          vector.addElement(newObj);
        }
      }
      else {
        int[] y = minus(dims, 0);

        for (int i = 0; i < dims[0]; i++) {
          String aux = blk.getString(i);
          vector.addElement(myUnserializeVector(aux, y, parser, dummyClass));
        }
      }
      return vector;

    }



}

  /*---------------------------------------------------*/
   /**
    *  Private method that generates the array, given the dimensions array as input. It is used
    * by the constructor.
    */ /*           Vector vec = new Vector();
                 if(dims.length == 1) {
                         for(int i=0; i<dims[0]; i++) {
                                 Integer zero = new Integer(0);
                                 vec.addElement(zero);
                         }
                         return vec;
                 }
                 else {
                         int [] y = minus(dims, 0);

                         for(int i=0; i<dims[0]; i++) {
                                 vec.addElement(generateArray(y));
                         }
                 }
                 return vec;
 /*  private Vector generateArray(int [] dims){

   }*/

