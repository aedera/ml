package stringSerializer;


import java.util.*;
import java.io.*;

import stringSerializer.*;
/**
 *
 * <p>Title: Parser for machineLearning classes serialization.</p>
 * <p>Description: Used to parse/unparse the object states in machineLeanring project. <!-- -->
 *   Offers functionality as iterating over <blocks>.<!-- --> The grammar is as follows:
 *   </p>
 * @author Facundo Bromberg
 * @version 1.0
 */
public class ZeusParser extends Parser
{
  int pos = 0;
  /**
   * No argument constructor, assigns default values to the delimiters.
   */
  public ZeusParser ()
  {
    super("ZZZ", "ZZ", "FastZ", "$");

 }

 /** Used to post filter the String output by parser, usually overrided by sub clases. */
 public String filter(String inputStr)
 {
   reset();
//   inputStr = "--&&---*^--^--";
   int illint = getIllegalChar(inputStr);
   String filtered = inputStr;

   Vector header = new Vector();
   while(illint  != filtered.length() && illint != -1)
   {
         String replacement = getFreeAlphanumericSymbol(filtered, 'Y');
         char aux [] = new char[1];
         aux[0] = filtered.charAt(illint);

         //We generate a header that will contain the illegalchar-replacements pairs.
         //And guess what? we parse them :)
         header.addElement(new String(aux));
         header.addElement(replacement);

         filtered = filter(filtered, new String(aux), replacement);
         illint = getIllegalChar(filtered);
   }

/*   Vector vblock = new Vector();
   //Generate the header block
   Block blk2 = new Block("HEAD");
   for(int i=0; i<header.size(); i++) blk2.addValue(header.get(i), this);
   vblock.addElement(blk2);
*/

   //Let's generate the header
   String headerStr = "";
//   String headerTag = pd;//getFreeAlphanumericSymbol(filtered, 'Y');
//   String headerTagInternal = getFreeAlphanumericSymbol(filtered, 'Y');
   headerStr += pd + sd;
   for(int i=0; i<header.size(); i+=2){
     String aux = (String)header.get(i);
     int ch = aux.charAt(0);
     headerStr += ch + sd;

     headerStr += (String) header.get(i+1) + sd;

   }
   headerStr += pd + sd;

   filtered = headerStr.concat(filtered);


     return filtered;
  }

  private void reset(){pos = 0;};

  /** Get the first alphanumeric symbol for use in the parser, e.g. */
  private String getFreeAlphanumericSymbol(String inputStr, char ch)
  {
    char start [] = new char[1];

    start[0] = ch;

    while(true) {
      if(start[0] == 'A') return null;

      String fans = "";
      fans = fans.concat(new String(start));
      fans = fans.concat(new String(start));
      fans = fans.concat(new String(start));
      int i;
      for ( i = 0; i < inputStr.length() - 2; i++) {
        String substr = inputStr.substring(i, i + 3);
        if (substr.equals(fans)){
          start[0]--;
          break;
        }
      }
      if(i == inputStr.length() - 2)return fans;
    }
  }
  /** Get the first illegal character position in the input String */
  private int getIllegalChar(String inputStr)
  {
    while(true){
      if(pos >= inputStr.length()) return -1;
      char ch = inputStr.charAt(pos);
//      int type = Character.getType(ch);
      if(!Character.isLetter(ch))
        if(!Character.isDigit(ch))
          if(Character.getType(ch) != Character.CURRENCY_SYMBOL)
        if(ch != '_'){
          return pos;
        }

      if(pos == inputStr.length()) return pos;
      pos++;
    }
   }
 /** Used to pre filter the String input to unparser, usually overrided by sub clases. */
 public String unfilter(String inputStr)
 {
   //First, let's separate the header from the rest
   int i;
   for( i=2; i<inputStr.length(); i++){
     if(inputStr.substring(i, i+pd.length()).equals(pd)) break;
   }
   String unfiltered = inputStr.substring(i+4, inputStr.length());
   String header = inputStr.substring(0, i+4);

   //Let's parse the header
   StreamTokenizer tokenizer = getTokenizer(header);

   //read initial pd
   try {tokenizer.nextToken();}
   catch (Exception te){}

   while(true){
     //Read illegalChar (in int format)
     try {tokenizer.nextToken();}
     catch (Exception te){}

     if(tokenizer.sval != null && tokenizer.sval.equals(pd)) break;

     char [] ch = new char[1];
     ch[0] = (char) SSUtils.myParseInt(tokenizer.sval);
     String chStr = new String(ch);

     //Read the replacement
     try {tokenizer.nextToken();}
     catch (Exception te){}

     String replacement = tokenizer.sval;

     unfiltered = filter(unfiltered, replacement, chStr);
   }
   return unfiltered;

  }



}
