package stringSerializer;



/**
 *
 * <p>Title: In some cases, objects contain objects of an unkown type.<!-- --> The most
 * common example is containers such as Vectors, which can store objects of any type.<!-- -->
 * In order to serialize a Vector (or a class which contain that vector), we need to
 * serialize the objects contained in it. <!-- --> The parser has no way (well, not exactly true)
 * to know the class of that object.<!-- --> For that reason I simply add another unserializer
 * method that receives a dummy object of the contained class (see examples of this method in ArrayD.myUnserialize())</p>
 * @author Facundo Bromberg
 * @version 1.0
 */
public interface ImySerializableContainer extends ImySerializable
{
//  public void myUnserialize(String inputStr, Object dummyClass , String pd, String ed, String vd, String sd);
  public void myUnserialize(String inputStr, Object dummyClass , Parser parser);
}
