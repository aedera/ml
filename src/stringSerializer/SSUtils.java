package stringSerializer;

import java.util.*;
import java.io.*;

import java.lang.reflect.*;

/**
 *
 * <p>Title: Utility class with some static useful methods.</p>
 * <p>Description: </p>
 * @author Facundo Bromberg
 * @version 1.0
 */
public class SSUtils
{
  public static int d;

  // In Microsoft's J++, the console window can close up before you get a chance to read it,
  // so this method can be used to wait until you're ready to proceed.
  public static void waitHere(String msg)
  {
    System.out.println("");
    System.out.print(msg);
    try { System.in.read(); }
    catch(Exception e) {} // Ignore any errors while reading.
  }

  // Note: this does NOT add a final "linefeed" to the file.  Might not want to
  // get that upon a subsequent "read" of the file.
  public static synchronized boolean writeStringToFile(String contents, String fileName)
  {
    return writeStringToFile(contents, fileName, true);
  }
  public static synchronized boolean writeStringToFile(String contents, String fileName, boolean append)
  {
    try
    { java.io.File        file   = new java.io.File(fileName);       // Want this to be an auto-flush file.
      java.io.PrintWriter stream = new java.io.PrintWriter(new java.io.FileOutputStream(file, append), true);

      stream.print(contents);
      stream.close();
      return true;
    }
    catch(Exception ioe)
    {
//      Error("Exception writing to " + fileName + ".  Error msg: " + ioe);
      return false;
    }
  }
  /**
   * MyParseInt is similar to the Integer.parseInt but return Integer.MIN_VALUE in case of an error (like if the input is not a well formed integer string). It is used for non-numerical attribute values (e.g. "Tiny")
   * @author Facundo Bromberg
   */
  public static int myParseInt(String value)
  {
      int intValue = -1;

      try{ intValue = Integer.parseInt(value); }
      catch(Exception intE)
      {
	  NumberFormatException nfe = new NumberFormatException();
	  Class exceptionClass = intE.getClass();
	  Class numberClass = nfe.getClass();
	  String className = exceptionClass.getName();
	  if(className.equalsIgnoreCase(numberClass.getName()))
	  {
	      intValue = Integer.MIN_VALUE;
	  }
      }
      return intValue;
  }
  /**
     * MyParseDouble is similar to the Double.parseDouble but return Double.MIN_VALUE in case of an error (like if the input is not a well formed integer string). It is used for non-numerical attribute values (e.g. "Tiny")
     * @author Facundo Bromberg
     */
    public static double myParseDouble(String value)
    {
	double doubleValue = -1;

	try{ doubleValue = Double.parseDouble(value); }
	catch(Exception intE)
	{
	    NumberFormatException nfe = new NumberFormatException();
	    Class exceptionClass = intE.getClass();
	    Class numberClass = nfe.getClass();
	    String className = exceptionClass.getName();
	    if(className.equalsIgnoreCase(numberClass.getName()))
	    {
		doubleValue = Double.MIN_VALUE;
	    }
	}
	return doubleValue;
    }
    /**
     * standarizedDoubleString receives a String. If the String parses a number, then it returns a standarized form of that number
     * @author Facundo Bromberg
     */
    public static String standarizedDoubleString(String str)
    {
	double auxD = SSUtils.myParseDouble(str);
	if(auxD != Double.MIN_VALUE) return Double.toString(auxD);
	else return str;
    }

    /**
     * returns a String representing the input double with "dec" decimal places.
     */
    public static String myDoubleToString(double input, int dec)
    {
        if (input == Double.MAX_VALUE) return "Infinity";
	String aux = "###,##0.0";
	for(int i=1; i<dec; i++) aux += "0";
	java.text.DecimalFormat df2 = new java.text.DecimalFormat(aux);
	return df2.format(input);
    }

    public static Object myNew(Object obj)
    {
      Object newobj = null;
      try {
        newobj = obj.getClass().newInstance();
      }
      catch (IllegalAccessException ex) {
      }
      catch (InstantiationException ex) {
      }

      return newobj;
/*      Class cls = obj.getClass();
      Constructor con[];

      con = cls.getDeclaredConstructors();
      Class param[] = con[0].getParameterTypes();
      Object paramValues[] = new Object[param.length];

      for(int x=0; i<con.length; i++){
        if(! Param[x].isPrimitive()){
          paramValues[x] = param[x].newInstance();
        }
      }*/
    }
}
