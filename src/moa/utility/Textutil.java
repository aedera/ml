package moa.utility;

import java.text.DecimalFormat;

public class Textutil {

	public static String matrixToString(double [][] m, String format){
		DecimalFormat dec = new DecimalFormat(format);
		String s ="";				
		for (int i=0; i<m.length; i++){			
			for (int j=0; j<m[i].length; j++)
				s += dec.format(m[i][j])+"\t";
			s +="\n";
		}		
		return s;
	}
	
	public static String matrixToString(int [][] m, String format){
		DecimalFormat dec = new DecimalFormat(format);
		String s ="";				
		for (int i=0; i<m.length; i++){			
			for (int j=0; j<m[i].length; j++)
				s += dec.format(m[i][j])+"\t";
			s +="\n";
		}		
		return s;
	}

	public static String vectorToString(double [] v, String format){
		DecimalFormat dec = new DecimalFormat(format);
		String s ="";				
		for (int i=0; i<v.length; i++)			
				s += dec.format(v[i])+"\t";
		return s;
	}

	public static String vectorToString(int [] v, String format){
		DecimalFormat dec = new DecimalFormat(format);
		String s ="";				
		for (int i=0; i<v.length; i++)			
				s += dec.format(v[i])+"\t";
		return s;
	}

	public static String intToString(int v, String format){
		return new DecimalFormat(format).format(v);
	}

	public static String doubleToString(double v, String format){
		return new DecimalFormat(format).format(v);
	}

}
