package moa.optimtools.problems.onemax;
import moa.optimtools.eautility.Solution;
import moa.optimtools.problems.FitnessFunction;

public class OneMax extends FitnessFunction <Boolean>{
  private int probSize;

  public OneMax(int probSize) {
    this.probSize = probSize;
  }

  public double returnFit(Solution<Boolean> sol, int length, int curIter){
    double fit = 0.0;
    for (int i=0; i<probSize; i++)
      if (sol.getAlleles()[i]) fit++;
    return fit;
  }

}

