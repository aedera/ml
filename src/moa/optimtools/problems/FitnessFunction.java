package moa.optimtools.problems;
import moa.optimtools.eautility.Solution;

public abstract class FitnessFunction <T>{

  //abstract method that evaluates the fitness given a bitstring solution
  public abstract double returnFit(Solution<T> sol, int length, int curIter);

  // method that should be overwritten to return a string with phenotype (actual decoded results)
  // from a chromosome, 
  // Do not override if no decoding necessary 
  public String getDecodedResults(Solution<T> sol, int length, int curIter){
	   String decodedString = "No further decoding necessary \n";
	   //nothing to decode
	   return decodedString;	  
  }

}
