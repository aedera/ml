package moa.optimtools.problems.ising;

import java.io.BufferedReader;
import java.io.FileReader;

import moa.optimtools.eautility.Solution;
import moa.optimtools.problems.FitnessFunction;


public class IsingModel extends FitnessFunction<Boolean> {

	private int n = 16;
	int[] MaxIsing = { 18, 20, 24, 18, 50, 52, 48, 50, 86, 84, 88, 94, 136,
			142, 142, 138, 348, 368, 356, 352, 572, 556, 554, 576 };
	int[] allvars24 = { 16, 16, 16, 16, 36, 36, 36, 36, 64, 64, 64, 64, 100,
			100, 100, 100, 256, 256, 256, 256, 400, 400, 400, 400 };
	int inst = 1;
	private String pathOfIsingInstance;
	private Integer numVars;
	private Integer dim;
	private Integer neigh;
	private Integer width;
	private int[][] lattice;
	private int[][] inter;
	private Solution<Boolean> ind;
	private int length;
	private int curIter;

	public IsingModel(int n, String pathOfIsingInstances) {
		this.n = n;
		this.pathOfIsingInstance = pathOfIsingInstances;
	}

	@Override
	public double returnFit(Solution<Boolean> ind, int length, int curIter) {
		this.ind = ind;
		this.length = length;
		this.curIter = curIter;
		loadIsing();
		return evalIsing();
	}

	/**
	 * This code is a copy of the matlab code of the File LoadIsing.m from the
	 * Mateda framework
	 */
	private void loadIsing() {
		String instance = pathOfIsingInstance + "SG_" + n + "_" + inst + ".txt";
		try {
			BufferedReader reader = null;
			reader = new BufferedReader(new FileReader(instance));
			String line = reader.readLine().trim();
			numVars = new Integer(line);

			line = reader.readLine().trim();
			dim = new Integer(line);

			line = reader.readLine().trim();
			neigh = new Integer(line);

			line = reader.readLine().trim();
			width = new Integer(line);

			int neighbor = (int) (Math.pow(2, neigh) * dim);
			lattice = new int[numVars][neighbor + 1];
			inter = new int[numVars][neighbor];

			for (int i = 0; i < numVars; ++i) {
				String[] values = reader.readLine().split(" ");
				lattice[i][0] = new Integer(values[0]);
				if (lattice[i][0] > 0) {
					for (int j = 0; j < lattice[i][0] + 1; ++j) {
						lattice[i][j+1] = new Integer(values[j + 1]);
					}
					for (int j = 0; j < lattice[i][0]; ++j) {
						inter[i][j] = new Integer(values[j+1+lattice[i][0]]);
					}
				}
			}
			reader.close();
		} catch (Exception ioe) {
			System.err.println(ioe.getMessage());
		}

	}

	private double evalIsing() {
		double r = 0;
		for(int i=0; i<lattice[0].length; ++i){
			if(lattice[i][0]>0){
				for (int j = 1; j < lattice[i][0] + 1; ++j) {
					if(i<lattice[i][j]){
						double auxr = 2 * ((ind.getAllele(i)==ind.getAllele(lattice[i][j]))?1:0) - 1;
						r += (auxr * inter[i][j-1]);
					}
				}
					
			}
		}
		return -1*r;
	}
}
