package moa.optimtools.problems.desc3;

import moa.optimtools.eautility.Solution;
import moa.optimtools.problems.FitnessFunction;

public class Deceptive3 extends FitnessFunction<Boolean> {
	private int n; 

	public Deceptive3(int probSize) {
		this.n = probSize;
	}

	public double returnFit(Solution<Boolean> sol, int length, int curIter) {
		double fitness = 0;
		for (int i = 1; i <= n / 3; i++) {
			boolean[] block = new boolean[3];
			block[0] = sol.getAllele((3 * i) - 3);
			block[1] = sol.getAllele((3 * i) - 2);
			block[2] = sol.getAllele((3 * i) - 1);
			fitness += fGdec(block);
		}
		sol.setFitness(fitness);
//		System.out.println("solution: " + sol );
		return fitness*3;
	}

	private double fGdec(boolean[] block) {
		int u = 0;
		assert block.length == 3;
		for (int i = 0; i < block.length; i++) {
			if (block[i]) {
				u++;
			}
		}
		double[] fGdec = { 0.9, 0.8, 0.0, 1.0 };
		return fGdec[u];
	}

}
