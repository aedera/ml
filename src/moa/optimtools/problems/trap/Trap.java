package moa.optimtools.problems.trap;
import java.util.Random;

import moa.optimtools.eautility.Solution;
import moa.optimtools.problems.FitnessFunction;


public class Trap extends FitnessFunction <Boolean>{
  private int probSize;  //n
  public int orderSize;  // k
  private boolean permute;		//flag to set if premutation based or not
  private int [] p; 			//random permuatation
  
  public Trap(int probSize, int orderSize, boolean permute) {
    this.probSize = probSize;
    this.orderSize = orderSize;
	
	this.permute = permute;
	if (permute) {
		p = new int[probSize];
		for (int i=0; i<probSize; i++) p[i] = i;
		p = moa.utility.Mathutil.randPermutation(p, new Random());
	}
	//System.out.println(utility.Textutil.vectorToString(p, "0.0"));  
  }

  public double returnFit(Solution<Boolean> sol, int length, int curIter){
    int f=0;
    int numBlock = probSize / orderSize;
    float fitness = 0;
     for (int i=0; i<numBlock; i++)
      {
       for (int j=0; j<orderSize; j++){ 
    	   if (!permute) if (sol.getAlleles()[(orderSize*i)+j]) f++;    		 
    	   if (permute) if(sol.getAlleles()[p[(orderSize*i)+j]]) f++;
       }		
       if (f==orderSize) fitness=fitness + orderSize;
         else fitness = fitness + (orderSize-1-f);
       f=0;
      }
    return(fitness);
  }

}
