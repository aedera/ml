package moa.optimtools.problems.royalroad;
import moa.optimtools.eautility.Solution;
import moa.optimtools.problems.FitnessFunction;

public class RoyalRoad extends FitnessFunction <Boolean>{

  private int probSize;
  public int blockSize;

  public RoyalRoad(int probSize, int blockSize) {
    this.probSize = probSize;
    this.blockSize = blockSize;
  }

  public double returnFit(Solution<Boolean> sol, int length, int curIter){
    int numBlock = probSize/blockSize;
    int cnt;
    double fit=0.0;
    for (int i=0; i<numBlock; i++){
      cnt=0;
      for (int j=0; j<blockSize; j++)
        if (sol.getAlleles()[(i*blockSize)+j]) cnt++;
      if (cnt==blockSize) fit+=blockSize;
    }
    return fit;
  }


}
