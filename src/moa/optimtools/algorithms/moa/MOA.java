package moa.optimtools.algorithms.moa;

import moa.optimtools.algorithms.PopBasedAlgorithm;
import moa.optimtools.eautility.BPopulation;
import moa.optimtools.eautility.BSolution;
import moa.optimtools.eautility.Results;
import moa.optimtools.eautility.Solution;
import moa.optimtools.problems.FitnessFunction;
import moa.utility.Mathutil;

import java.util.Random;
import java.util.ArrayList;


public class MOA extends PopBasedAlgorithm <Boolean> {

  private int ss;		  	  //Selection size - should be alwaysless than popSize
  private double cr;		  //coolinr rate 0 to 1
  private double it;			  //iteration times, for sampling
  private int maxNeighbours;  //maximum neighbours to be considered while finding network
  private double thresholdCoeff;	//threshold coefficient -  coefficent to regulate the effect of mean of MI matrix
  
  //private int optFit;
  private int structure[][];
  private String structureType;  
  protected String statType;
  
  private double jointProbTableOfVariableAndItsNeighbours[][];
  private double univMargProbVector[];		  //Probability vector

  public MOA(FitnessFunction <Boolean> fitFunc, int solLength, int popSize, int ss, double cr, double it, int maxGen, int eliteSol, Random rand, boolean optFitnessKnown, double optFitness, String structureType, String statType, int maxNeighbours, double thresholdCoeff){
    super.fitFunc = fitFunc;
    super.rand = rand;
    this.solLength = solLength;
    this.popSize = popSize;
    this.ss = ss;
    this.cr = cr;
    this.it = it;
    this.maxGen = maxGen;
    this.eliteSol = eliteSol;
    this.structureType = structureType;
    this.statType = statType;
    this.maxNeighbours = maxNeighbours;
    this.thresholdCoeff = thresholdCoeff; 

    super.pop = new BPopulation(popSize, solLength, rand);
    super.spop = new BPopulation(popSize, solLength, rand);
    bestSol = new BSolution(solLength, rand);
    super.results = new Results(optFitnessKnown, optFitness, bestSol, spop.solutions[0], 0, 0);

    //create probability vector for for storing univariate marginal probabilities of variable being 1
    univMargProbVector = new double[solLength];

    //create joint probability table for a variable together with its neighbors
    //number of neighbors is still unknown and will be updated in each generation
    jointProbTableOfVariableAndItsNeighbours = new double [solLength][];
  }

  
  public int decode(boolean alleles[], int startPos, int endPos){
    int accum, powerof2;
    accum =0; powerof2=1;
    for (int i = endPos-1; i>=startPos; i--){
      if (alleles[i]) accum = accum + powerof2;
      powerof2 = powerof2 * 2;
    }
    return(accum);
  }//{decode}

  public void code(int number, boolean alleles[], int startPos, int endPos){
    int wholenumber = number;
    int reminder = 0;
    for (int i = startPos; i<endPos; i++){
		reminder = wholenumber % 2;
		wholenumber = wholenumber/2;
    	if (reminder==0) alleles[endPos-i] = false; else alleles[endPos-i]=true;
    }
  }//{code}

  public double getProdUniMargProb(int var1, int var2, int conf){
    if 	    (conf==0) return (1-univMargProbVector[var1]) * (1-univMargProbVector[var2]);
    else if (conf==1) return (1-univMargProbVector[var1]) * (univMargProbVector[var2]);
    else if (conf==2) return (univMargProbVector[var1])   * (1-univMargProbVector[var2]);
    else if (conf==3) return (univMargProbVector[var1])   * (univMargProbVector[var2]);
    return -1.0;
  }

  public int [][] getTrap5Structure(int solLength){	  	  
	  int [][] structure = new int [solLength][];	  
	  int blockLength = 5;	// since it is trap 5
	  for (int i=0; i<solLength; i++){
		  structure[i] = new int[blockLength-1];
		  int blockNo = i/blockLength;
		  int allelerNo = i % blockLength;
		  int count=0;
		  for (int j=0; j<blockLength; j++)
			  if (allelerNo != j){
				  structure[i][count] = blockNo*blockLength + j;
				  count++;
			  }
	  }	  
	  return structure;	  
  }

  public int [][] getTrap4Structure(int solLength){	  	  
	  int [][] structure = new int [solLength][];	  
	  int blockLength = 4;	// since it is trap 5
	  for (int i=0; i<solLength; i++){
		  structure[i] = new int[blockLength-1];
		  int blockNo = i/blockLength;
		  int allelerNo = i % blockLength;
		  int count=0;
		  for (int j=0; j<blockLength; j++)
			  if (allelerNo != j){
				  structure[i][count] = blockNo*blockLength + j;
				  count++;
			  }
	  }	  
	  return structure;	  
  }

  
  public int [][] get3DeceptiveStructure(int solLength){	  	  
	  int [][] structure = new int [solLength][];	  
	  int blockLength = 3;	// since it is 3 deceptive
	  for (int i=0; i<solLength; i++){
		  structure[i] = new int[blockLength-1];
		  int blockNo = i/blockLength;
		  int allelerNo = i % blockLength;
		  int count=0;
		  for (int j=0; j<blockLength; j++)
			  if (allelerNo != j){
				  structure[i][count] = blockNo*blockLength + j;
				  count++;
			  }
	  }
	  return structure;	  
  }

  public int convert(int a, int max, int min){
	if (a>=max) return a-max;
	else if (a<min) return max + a;
	else return a;
  }
  
  public int [][] getDeceptive3Overlap1Structure(int solLength){	  	  
	  boolean cycle;
	  if ((solLength/3)%2 == 0) cycle = true;
	  else cycle = false;
	  
	  int [][] structure = new int [solLength][];
	  if (cycle) {
		  for (int i=0; i<solLength; i++){
			  if (i%2 == 0) {
				  structure[i] = new int[4];
				  structure[i][0] = convert(i-2, solLength, 0);
				  structure[i][1] = convert(i-1, solLength, 0);
				  structure[i][2] = convert(i+1, solLength, 0);
				  structure[i][3] = convert(i+2, solLength, 0);
			  }else {
				  structure[i] = new int[2];
				  structure[i][0] = convert(i-1, solLength, 0);
				  structure[i][1] = convert(i+1, solLength, 0);
			  }
		  }
	  } else {
		  for (int i=0; i<solLength; i++){
			  if (i%2 == 0) {
				  if (i==0){
					  structure[i] = new int[2];
					  structure[i][0] = i+1;
					  structure[i][1] = i+2;
				  }else if (i==solLength-1){
					  structure[i] = new int[2];
					  structure[i][0] = i-2;
					  structure[i][1] = i-1;					  
				  }else {
					  structure[i] = new int[4];
					  structure[i][0] = i-2;
					  structure[i][1] = i-1;
					  structure[i][2] = i+1;
					  structure[i][3] = i+2;					  
				  }
			  }else {
				  structure[i] = new int[2];
				  structure[i][0] = convert(i-1, solLength, 0);
				  structure[i][1] = convert(i+1, solLength, 0);
			  }			  
		  }
	  }
	  	               
	  return structure;	  
  }


  //d = orderSize-overlapSize
  public int [][] getTrapOverlap1Structure(int solLength, int orderSize, int overlapSize){
	  //int d = 4;	//for trap5Overlap1 since orderSize-overlapSize = 4
	  int d = orderSize-overlapSize;
	  int [][] structure = new int [solLength][];
	  for (int i=0; i<solLength; i++){
		  if ((i+d)%d == 0) {
			  structure[i] = new int[d*2];
			  for (int j=0; j<d; j++)
				  structure[i][j] = convert(i-d+j, solLength, 0);
			  for (int j=d; j<d*2; j++)
				  structure[i][j] = convert(i+j-d+1, solLength, 0);
		  }else {
			  structure[i] = new int[d];
			  int start = (i/d)*d;
			  int nbr = start;
			  for (int j=0; j<d; j++){
				  if (nbr==i) nbr++;
				  structure[i][j] = convert(nbr, solLength, 0);
				  nbr++;
			  }
		  }
	  }
	  return structure;	  
  }  

  //Directly get for problem structure for some specific functions
  public int[][] getKnownStructure(String structureType, int solLength){
	  int [][] structure = null;	  
	  if (structureType.equals("Trap5")) structure = getTrap5Structure(solLength); 
	  if (structureType.equals("Decep3")) structure = get3DeceptiveStructure(solLength);
	  if (structureType.equals("Trap5-1")) structure = getTrapOverlap1Structure(solLength, 5, 1);
	  if (structureType.equals("Trap4-1")) structure = getTrapOverlap1Structure(solLength, 4, 1);
	  if (structureType.equals("Trap3-1")) structure = getTrapOverlap1Structure(solLength, 3, 1);
	  if (structureType.equals("Decep3-1")) structure = getDeceptive3Overlap1Structure(solLength);
	  //if (structureType.equals("Decep3-1")) structure = getTrapOverlap1Structure(solLength, 3, 1); //same as above
	  if (structureType.equals("rADPs4-0")) structure = getTrap4Structure(solLength);
	  return structure;
  }


  //given a solution and a setOf variable positions, returns the configuration of those variables (bits) as a decoded number
  public int getiConf(BSolution sol, int [] varPos){
	  boolean [] varSet = new boolean [varPos.length];
	  for (int i=0; i<varPos.length; i++)
		  varSet[i] = sol.getAllele(varPos[i]);
	  return decode(varSet, 0, varSet.length);
  }

  //given set of variables and a configuration for them, calculates joint probability of that set for that configuration
  public double getJointProb(int [] varPos, int conf, BPopulation spop, int ss){
	int count = 0;
	for (int i=0; i<ss; i++){
		if (conf == getiConf( (BSolution) spop.solutions[i], varPos)) count++;
	}
	double prob = (double)count/(double)ss; 
	//double prob = ((double)count+0.000001)/(double)ss; 
	//double prob = ((double)count+1)/((double) ss+(varPos.length*varPos.length)); 
	return prob;
  }

  //given set of variables, calculates joint probability of that set for all of its configuration and puts to the table
  public double[] estimateJointProbTable(int [] varPos, BPopulation spop, int ss, boolean laplace){
	int verVal = 2; 													//binary variable so two posibility, 0 or 1
	int maxConf = (int) Math.pow(verVal, varPos.length);
	double [] jointProbTable = new double [maxConf];
	//count the number of repetition of configurations in the selected set
	for (int i=0; i<ss; i++) jointProbTable[getiConf( (BSolution) spop.solutions[i], varPos)]++;	
	//convert it to probabilities
	if (laplace) for (int i=0; i<maxConf; i++)	jointProbTable[i] = (jointProbTable[i]+1.0)/(double)(ss+maxConf);//laplace correction
	else for (int i=0; i<maxConf; i++)	jointProbTable[i] = jointProbTable[i]/(double)ss;
	
	return jointProbTable;
  }
  
  
  public double indpTestLinkDetect(int xPos, int yPos, int solLength){
	  BSolution sol = new BSolution(solLength, rand);
	  BSolution tmpSol = new BSolution(solLength, rand);
	  sol.copyTo(tmpSol);
	  
	  sol.evaluate(fitFunc, 1);
	  double orgFit = sol.getFitness();
	  
	  sol.setAllele(xPos, !sol.getAllele(xPos));
	  sol.evaluate(fitFunc, 1);
	  double iFit = sol.getFitness();
	  sol.setAllele(xPos, !sol.getAllele(xPos));
	  
	  sol.setAllele(yPos, !sol.getAllele(yPos));
	  sol.evaluate(fitFunc, 1);
	  double jFit = sol.getFitness();
	  sol.setAllele(yPos, !sol.getAllele(yPos));

	  sol.setAllele(xPos, !sol.getAllele(xPos));
	  sol.setAllele(yPos, !sol.getAllele(yPos));
	  sol.evaluate(fitFunc, 1);
	  double ijFit = sol.getFitness();
	  sol.setAllele(xPos, !sol.getAllele(xPos));
	  sol.setAllele(yPos, !sol.getAllele(yPos));
	  
	  return Math.abs((orgFit - iFit + orgFit - jFit) - (orgFit - ijFit));
  };
  


  //Independent test - efficient version
  public double indpTest(int xPos, int yPos, BPopulation spop, int solLength, int ss, String statType){

	  // if it is of type "LinkageDetection" do it and return the stat
	  if (statType.matches("LinkageDetection"))
		  return indpTestLinkDetect(xPos, yPos, solLength);

	  //Otherwise
	  int verVal = 2; 													//binary variable so two posibility, 0 or 1
	  double stat = 0.0;

	  //find p(x,y) - joint probability table for set xy
	  int [] xyPos = new int [2];
	  xyPos[0] = xPos;  xyPos[1] = yPos;
	  double [] jointProbTablexy = estimateJointProbTable(xyPos, spop, ss, false);

	  //find p(x) - joint (marginal) probability table for x
	  int [] xPosA = new int [1];
	  xPosA[0] = xPos;
	  double [] jointProbTablex = estimateJointProbTable(xPosA, spop, ss, false);

	  //find p(y) - joint (marginal) probability table for y
	  int [] yPosA = new int [1];
	  yPosA[0] = yPos;
	  double [] jointProbTabley = estimateJointProbTable(yPosA, spop, ss, false);

	  int maxConfxy = (int) Math.pow(verVal, xyPos.length);
	  if (statType.matches("JointEqMargProd"))		  
		  for (int j=0; j<maxConfxy; j++)
			  stat += Math.abs(jointProbTablexy[j] - (jointProbTablex[j/2]*jointProbTabley[j%2]));
	  if (statType.matches("ChiSquare"))
		  for (int j=0; j<maxConfxy; j++)
			  stat += (((jointProbTablexy[j]*ss) - (jointProbTablex[j/2]*jointProbTabley[j%2]*ss)) * ((jointProbTablexy[j]*ss) - (jointProbTablex[j/2]*jointProbTabley[j%2]*ss)))/(jointProbTablex[j/2]*jointProbTabley[j%2]*ss);
	  if (statType.matches("CrossEntropy"))
		  for (int j=0; j<maxConfxy; j++)
			  if (jointProbTablexy[j]>0)
				  stat += jointProbTablexy[j]*Math.log(jointProbTablexy[j]/(jointProbTablex[j/2]*jointProbTabley[j%2]));

	  return stat;
  }
  

  //get structure using "statType"
  public int[][] getStructure(BPopulation spop, int solLength, int ss, String statType, int maxNeighbours){

	  //start with a complete undirected graph
	  ArrayList [] compStruct = new ArrayList [solLength] ;		//store the neighbours
	  ArrayList [] compStructVal = new ArrayList[solLength];	//store actual mutual unformation values
	  for (int i=0; i<solLength; i++){
		  compStruct[i] = new ArrayList ();
		  compStructVal[i] = new ArrayList();
	  }

	  for (int i=0; i<solLength; i++)
		  for (int j=0; j<solLength; j++){
			compStruct[i].add(j); 	//neighbours of i
			if (j==i) compStructVal[i].add(0.00);	//if same variable then no dependency 
			else compStructVal[i].add(100.00);		//initilaise with very high dependency 
		}

	  //first perform zero order independence test and create the matrix of mutual informations
	  double indpValue;
	  double sumMutualInfo=0.0;	  
	  for (int i=0; i<solLength-1; i++)							//now for each node i in the solution
		for (int j=i+1; j<solLength; j++){						//for each adjecent nodes of that node
			indpValue = indpTest(i, j, spop, solLength, ss, statType);
			compStructVal[i].set(j, indpValue);
			compStructVal[j].set(i, indpValue);
			sumMutualInfo+=indpValue+indpValue;
		}

	  //now calculate the threshold 
	  double ceThreshold  = thresholdCoeff * (sumMutualInfo/(solLength*(solLength-1))); //1.5 times the average of mutual info martix
	  
	  //now reduce the number of edges according to the threshold
	  boolean independent = false;
	  for (int i=0; i<solLength; i++)									//now for each node i in the solution
			for (int j=0; j<compStruct[i].size(); j++)					//for each adjecent nodes of that node
			{
				if ((Double)compStructVal[i].get(j)<ceThreshold)	independent = true;

				if (independent){
					//System.out.println("Removed  " + i + " --> " + (Integer)compStruct[i].get(j));
					compStruct[i].remove(j);
					compStructVal[i].remove(j);
					j=j-1;
					independent = false;
					//compStruct[j].remove(i); 		//similarly - this should work
				}
			}
	  	  	  
	  //sort the edges according their independence value
	  for (int i=0; i<solLength; i++)
		  Mathutil.sortRefGivenValueMaxToMin(compStruct[i], compStructVal[i]);
	 			  
	  //Copy remaining structure to a two dymensional matrix and return it
	  int [][] structure = new int [solLength][];					//create space for storing the neighbours of each variable
	  for (int i=0; i<solLength; i++){
		if (compStruct[i].size() < maxNeighbours )
			structure[i] = new int[compStruct[i].size()];
		else 
			structure[i] = new int[maxNeighbours];
	  	for (int j=0; j<structure[i].length; j++)
	  		structure[i][j] = (Integer) compStruct[i].get(j);
	  }
	  
	  //System.out.println(utility.Textutil.matrixToStringWithRowIndex(structure, "0"));
	  
	  double [][] value = new double [solLength][];					//create space for storing the neighbours of each variable
	  for (int i=0; i<solLength; i++){
		if (compStruct[i].size() < maxNeighbours )
			value[i] = new double[compStructVal[i].size()];
		else 
			value[i] = new double[maxNeighbours];
		  	//value[i] = new double[compStructVal[i].size()];
	  	for (int j=0; j<value[i].length; j++)
	  		value[i][j] = (Double) compStructVal[i].get(j);
	  }
	  //System.out.println(utility.Textutil.matrixToString(value, "0.000"));

	  
	  return structure;
  }


  public void estimateUndirectedStructure(BPopulation spop, int solLength, int ss, int gen){
	//if (gen > maxGen/5) return;
	//if (gen > 1) return;
    if (structureType.equals("ToFind")) 
       	structure = getStructure(spop, solLength, ss, statType, maxNeighbours);
    else structure =  getKnownStructure(structureType, solLength);    
  }

  private void sampleSolution(double[][] jointProbOfVariableAndItsNeighbours, int structure[][], Solution<Boolean> sol, int iter, Random rand, int g){
    int solLength = sol.getLength();
    for (int i=0; i<solLength; i++) if (rand.nextDouble()>0.5) sol.setAllele(i, true); else sol.setAllele(i, false);
    int pos, n, cfgVal, numProb;
    double condProb1, diff;

    for (int i=0; i<iter; i++){
      pos = (int)(rand.nextDouble()* solLength);
      n = structure[pos].length+1;//number of neighbors for current variable + 1 for itself
      numProb = (int)Math.pow(2,n);
      boolean config[] = new boolean[n];
      config[0]=true;
      for (int k=1; k<n; k++) config[k] = sol.getAllele(structure[pos][k-1]);
      cfgVal = decode(config, 0, n);

      if (cr!=0.0){
    	  //this is gibbs sampling
    	  diff = jointProbOfVariableAndItsNeighbours[pos][cfgVal-(numProb/2)] - jointProbOfVariableAndItsNeighbours[pos][cfgVal]; //if diff =0 i.e no difference thenprobability of sampling 1 is 0.5
    	  condProb1 = 1/ (1 + Math.exp(diff * g * cr));
      }
      else {
    	  //this is same as saying, p(xi=1|Ni)=p(xi=1,Ni)/(p(xi=1,Ni)+p(xi=-1,Ni))
    	  condProb1 = jointProbOfVariableAndItsNeighbours[pos][cfgVal] / (jointProbOfVariableAndItsNeighbours[pos][cfgVal] + jointProbOfVariableAndItsNeighbours[pos][cfgVal-(numProb/2)]);
      }
      
      //System.out.print(condProb1+" ");
      if (condProb1 > rand.nextDouble()) sol.setAllele(pos, true); else sol.setAllele(pos, false);
    }
  }

  private void estimateAndSampleMRF(Random rand, int g){

    //now estimate structure of the MRF from the population
    estimateUndirectedStructure((BPopulation) spop, solLength, ss, g);

    //estimate parameters - for all variables, joint probability of itself and its neighbours, which is sufficient to estimate conditional probabilities
    for (int i=0; i<solLength; i++){
      int [] varSet = new int [structure[i].length+1];
  	  varSet[0] = i;
  	  for (int j=1; j<structure[i].length+1; j++) varSet[j] = structure[i][j-1];
  	  jointProbTableOfVariableAndItsNeighbours[i] = estimateJointProbTable(varSet, (BPopulation) spop, ss, true);  	  
    }

    //int iter = solLength*it;	//it*n
    int iter = (int) (solLength * Math.log(solLength)* it);	//it* n * ln(n)
    //System.out.println(solLength + "\t"+ Math.log(solLength) + "\t"+ it + "\t"+ iter);

    //sample
    for (int i=0; i<eliteSol; i++) spop.solutions[i].copyTo(pop.solutions[i]);     //copy elite solutions to new pop, if any
    for (int i=eliteSol; i<popSize; i++) sampleSolution(jointProbTableOfVariableAndItsNeighbours, structure, pop.solutions[i], iter, rand, g);
  }

  /**
   * The customised selection and variation operator.
   * Extended from the super class <code> PopBasedAlgorithm 
   */
  public void performSelectionAndVariation(Random rand, int gen){
	  estimateAndSampleMRF(rand, gen+1);
  }

}
