package moa.optimtools.algorithms;
import moa.optimtools.eautility.Results;
import moa.optimtools.problems.FitnessFunction;

import java.util.Random;


public abstract class Algorithm <T>{

  /**
   * A random number generator
   */
  protected Random rand;
  
  /**
   * Fitness function -  the objective function
   */
  protected FitnessFunction <T> fitFunc;
  
  /**
   * Container to store set of parameters and results, 
   * such as best solutions, evaluations, best fitness, 
   * optimum fitness etc
   */
  protected Results results;       
  
  
  
  public void setResults(Results results){
	  this.results = results;
  }


  public Results getResults(){
	  return results;
  }

  /**
   * Set fitness function
   * @param fitFunc
   */
  public void setFitFunc(FitnessFunction <T> fitFunc){
    this.fitFunc = fitFunc;
  }

  /**
   * Return fitness function
   * @param fitFunc
   * @return
   */
  public FitnessFunction <T> getFitFunc(){
    return fitFunc;
  }
  
  /**
   * Framework to print heading for the algortihm statistic
   * Override to show heading for the output, such as in system terminal or to in graphics
   * This method is overwritten in population based algorithm to print heading for the statistic 
   * in system terminal
   * This method is further overwridden in GUI and left blank so that nothing will be printed   
   */
  public void showStatHeader(){
	  // override to print something during begining of algorithm iteration  
  }	


  /**
   * Framework to print current algortihm statistic
   * Override to show output, such as to print in system terminal or to print in graphics
   * This method is overwritten in population based algorithm to print statistic in system terminal
   * This method is further overwritten in GUI to print statistic in graphical window   
   * @param g current iteration
   */
  public void showCurrentStat(int g){
	  // override to print something during algorithm iteration  
  }	

  /**
   * Framework to print the final result of the algorithm
   * It can be used to, for example, decode final results and print if required, 
   * This method is overwritten in GUI to print results in textBox
   * @param fitFunc : Fitness function that can be used to decode the solution
   */
  public void showFinalResults(FitnessFunction <T> fitFunc){
	  // override to print something after algorithm iteration is finished  
  }

  /**
   * Framework to print do any final calculation before algorithm exits 
   * It can be used to, for example, change any fitness evaluation values 
   * This method is overwritten in GUI to print results in textBox
   * @param fitFunc : Fitness function that can be used to decode the solution
   */
  public void doFinalCalculation(FitnessFunction <T> fitFunc){
	  // override to print something after algorithm iteration is finished  
  }
  
  /**
   * Abstract method that runs the algorithm
   * Note: a default implementation could be inserted  
 * @throws Exception 
   */
  public abstract void runAlgorithm() throws Exception;

}
