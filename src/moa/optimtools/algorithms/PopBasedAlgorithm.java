package moa.optimtools.algorithms;

import moa.optimtools.eautility.Population;
import moa.optimtools.eautility.Solution;
import moa.optimtools.problems.FitnessFunction;

import java.util.GregorianCalendar;
import java.util.Random;

public abstract class PopBasedAlgorithm  <T> extends Algorithm <T>{
	public Population <T> pop;     //Population to workwith
	public Population <T> spop;    //sorted population
	public Solution <T> bestSol;    //best solution from the entire run of the algorithm
	
	public int solLength;
	public int popSize;
	public int maxGen;		  	  //maximum generation allowed
	public int eliteSol;		  //number of elite solution to be transfered to next generation
	public long runtime;
	  
	/**
	 * Evaluate and sort population
	 * 
	 * @param fitFunc
	 * @param curIter
	 * @param from
	 */	
	public void evaluateAndSortPop(FitnessFunction <T> fitFunc, int curIter, int from){
		pop.evaluate(fitFunc, curIter, from);
		pop.copyTo(spop);
		spop.sort();		  
	}
	
	/**
	 * Preforms selection and variation operations to the current generation to
	 * produce new population.
	 * 
	 * This method should be different for each population based algorithm
	 * and therefore be forced to implemented by each of them
	 * @throws Exception 
	 *
	 */
	public abstract void performSelectionAndVariation(Random rand, int gen) throws Exception;
		
	/**
	 * Print headings for statistics
	 *
	 */
	public void showStatHeader(){
//		System.out.println("\nGen\tAvgFit\tMaxFit");		
	}
	
	/**
	 * Print current population statistic, by default to system terminal
	 * Override to show more sophesticated output, such as to print in graphics
	 * Or override and leave the body blank to show nothing 
	 * @param g current generation
	 */
	public void showCurrentStat(int g){
		// By default simply print to the terminal
		// override for more advance features such as print to graphics 
		System.out.println(g+"\t"+spop.getAvgFit()+"\t"+spop.solutions[0].getFitness());
	}	
	
	
	/**
	 * Populate values in the output object
	 * This can be overwritten if any custom output values needs to be populated
	 */
	public void prepareOutputResult(){
	    if (results.getNumGen()==0) results.setNumGen(maxGen);					//set the number of generations done
	    results.setFitEval(popSize + (results.getNumGen()*(popSize-eliteSol)));	//set the number of fitness evaluation done
	    results.setBestSol(bestSol);
	    results.setFinalSol(spop.solutions[0]);	   		
	}
	
		
	/**
	 * Implementation of the key method from the super class. 
	 * This method is more or less generic and therefore may not require 
	 * overwriting in individual extension to a specific algorithm,
	 * but can be overwritten if algorithm has a radically different workflow
	 * @throws Exception 
	 */
	public void runAlgorithm() throws Exception{
		  long structureLearningBeginRuntime = new GregorianCalendar().getTimeInMillis();

		showStatHeader();
		evaluateAndSortPop(fitFunc, 1, 0);
	    
		//start iteration
	    for (int g=0; g<maxGen; g++){
	      showCurrentStat(g);	      
	      if (bestSol.getFitness() < spop.solutions[0].getFitness()) spop.solutions[0].copyTo(bestSol);
	      if (results.optFitnessKnown )
	    	  if (spop.solutions[0].getFitness()>=results.getOptFitness()){ results.setNumGen(g+1); break;};    
		  	    	
	      performSelectionAndVariation(rand, g+1);	      
	      evaluateAndSortPop(fitFunc, g+2, eliteSol);		//evaluate the new solution, those after elite solutions
	    }
	    
	    evaluateAndSortPop(fitFunc, maxGen, 0);
	    if (bestSol.getFitness() < spop.solutions[0].getFitness()) 
	    	spop.solutions[0].copyTo(bestSol);
	    
	    // Populate values in the output (Result) object 
	    prepareOutputResult();
	    
	    showFinalResults(fitFunc);
		  
	    runtime = new GregorianCalendar().getTimeInMillis() - structureLearningBeginRuntime;

	  }	  
	  
	/**
	 * set population
	 * @param pop
	 */
	public void setPopulation(Population <T> pop){
		this.pop = pop;
	}
	
	/**
	 * get population
	 * @return
	 */
	public Population <T> getPopulation(){
		return this.pop;
	}
	
	/**
	 * set sorted population
	 * @param spop
	 */
	public void setSPopulation(Population <T> spop){
		this.spop = spop;
	}

	/**
	 * get sorted population
	 * @return
	 */
	public Population <T> getSPopulation(){
		return this.spop;
	}
	

	
}
