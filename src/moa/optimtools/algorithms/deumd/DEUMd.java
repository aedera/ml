package moa.optimtools.algorithms.deumd;

import moa.optimtools.algorithms.PopBasedAlgorithm;
import moa.optimtools.eautility.BPopulation;
import moa.optimtools.eautility.BSolution;
import moa.optimtools.eautility.Misc;
import moa.optimtools.eautility.Results;
import moa.optimtools.problems.FitnessFunction;
import moa.utility.Mathutil;

import java.util.Random;


public class DEUMd extends PopBasedAlgorithm <Boolean> {

  private int ss;		  //Selection size - should be alwaysless than popSize
  private double tc;	  //Learning rate 0 to 1

  private double pv[];		  //Probability vector
  private double alpha[];	  //vector of mrf parameters

  public DEUMd(
		  FitnessFunction <Boolean> fitFunc, 
		  int solLength, 
		  int popSize, 
		  int ss, 
		  double tc, 
		  int maxGen, 
		  int eliteSol, 
		  Random rand, 
		  boolean optFitnessKnown, 
		  double optFitness) {
	  
	    super.fitFunc = fitFunc;
	    super.rand = rand;
	    this.solLength = solLength;
	    this.popSize = popSize;
	    this.ss = ss;
	    this.tc = tc;
	    this.maxGen = maxGen;
	    this.eliteSol = eliteSol;

	    pop = new BPopulation(popSize, solLength, rand);
	    spop = new BPopulation(popSize, solLength, rand);
	    bestSol = new BSolution(solLength, rand);
	    super.results = new Results(optFitnessKnown, optFitness, bestSol, spop.solutions[0], 0, 0);

	    pv = new double[solLength];
	    alpha = new double[solLength+1]; //solLength+1 is for constant c in system of equations

	    for (int i=0; i<solLength; i++) pv[i] = 0.5;//initilaise pv[] with 0.5 in the begning, not necessary for DEUMd
  }


  private int toInt(boolean b){
    if (b) return 1;
      else return -1;
  }


  /*estimate and sample pv*/
  private void estimateAndSampleMRF(Random rand, int g){

    int rows = ss;
    int cols = solLength+1;  //solLength+1 is for constant c
    
    //keep copy of original fitness before normalising, may be required later for elitism
    BPopulation spopOrg = new BPopulation(popSize, solLength, rand);
    spop.copyTo(spopOrg);
     
    //normalise fitness to all positive if there are any negative fitness
    Misc.normalaiseFitnessToAllPositive(spop);

    //estimate
    //build matrix a(ss*solLength)
    double [][] a = new double[rows][cols];
    for (int i = 0; i < rows; i++){
      for (int j = 0; j < cols-1; j++)
        a[i][j] = toInt(spop.solutions[i].getAllele(j));
      a[i][cols-1] = 1;  //for constant C
    }

    //build vector f(ss)
    double [] f = new double[rows];
    for (int i = 0; i < rows; i++)
      f[i] = -Math.log(spop.solutions[i].getFitness());
      //f[i] = -spop.solutions[i].getFitness();

    //solve Ax+c=F and estimate alpha (mrf parameters)
    Mathutil.solveWithSVD(a,rows,cols,f,alpha);

   //estimate marginal probabilities
   for (int i = 0; i < solLength; i++)
     pv[i] = 1.0 / (1.0 + Math.exp(alpha[i]*tc*g));

   /*for (int i = 0; i < (solLength+1); i++) System.out.print(" "+alpha[i]);
   System.out.println(" ");
   for (int i = 0; i < solLength; i++) System.out.print(" "+pv[i]);
   System.out.println(" ");*/

    //sample
    for (int i=0; i<eliteSol; i++) spopOrg.solutions[i].copyTo(pop.solutions[i]);     //copy elite solutions to new pop, if any
    for (int i=eliteSol; i<popSize; i++) pop.solutions[i].sample(pv, rand);
  }

  /**
   * The customised selection and variation operator.
   * Extended from the super class <code> PopBasedAlgorithm 
   */
  public void performSelectionAndVariation(Random rand, int gen){
	  estimateAndSampleMRF(rand, gen+1);
  }


}
