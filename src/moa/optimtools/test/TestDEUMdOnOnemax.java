package moa.optimtools.test;

import java.util.Random;

import moa.optimtools.algorithms.deumd.DEUMd;
import moa.optimtools.problems.FitnessFunction;
import moa.optimtools.problems.onemax.OneMax;




public class TestDEUMdOnOnemax
{

  public static void main(String args[]) {

	//parameters
    int solLength = 100;					//Solution length
    int popSize = 50;					//Population size
    int maxGen = 1;						//Maximum generation allowed, for onemax DEUMd solves it in initial generation
    boolean optFitnessKnown = true;		//Is optimum fitness to the problem known?
    double optFitness = solLength; 		//If yes, give optimum fitness value, otherwise init it to 0 (or any value)
    int eliteSol = 0;					//Number of best solutions from current generation to be transfered to next geenration
    int ss = 50;						//number of best solution to be selected for estimation of distribution 
    double tc = 1000;					//temperature coefficient for DEUMd

    
    System.out.println("\nStarting Onemax with DEUMd");
    
    //Create new instance of onemax fitness function
    FitnessFunction <Boolean> omFitFunc = new OneMax(solLength);
    
    //Create new instance of DEUMd algorithm for Onemax
    DEUMd omDEUMd = new DEUMd(omFitFunc, solLength, popSize, ss, 
    		tc, maxGen, eliteSol, new Random(), optFitnessKnown, optFitness);
    
    //Run DEUMd
    try {
		omDEUMd.runAlgorithm();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    System.out.println(omDEUMd.getResults().toString());
    
    
  }


}
