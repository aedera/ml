package moa.optimtools.test;

import java.util.Random;

import moa.optimtools.algorithms.deumd.DEUMd;
import moa.optimtools.problems.FitnessFunction;
import moa.optimtools.problems.royalroad.RoyalRoad;


public class TestDEUMdOnRoyalroad {

  public static void main(String args[]) {

	// parameters
    int solLength = 40;
    int popSize = 40;
    boolean optFitnessKnown = true;
    double optFitness = solLength; 
    int maxGen = 1000;
    int eliteSol = 0;
    int ss = 30;
    double tc = 1;

    System.out.println("\nStarting Royalroad with DEUMd");
    
    //Create new instance of onemax fitness function
    int problemSize = solLength;
    int blockSize = 4;
    FitnessFunction <Boolean> rrFitFunc = new RoyalRoad(problemSize, blockSize);
    
    //Create new instance of DEUMpv algorithm for Onemax        
    DEUMd rrDEUMd = new DEUMd(rrFitFunc, solLength, popSize, ss, tc, 
    		maxGen, eliteSol, new Random(), optFitnessKnown, optFitness);
    
    //Run DEUMpv    
    try {
		rrDEUMd.runAlgorithm();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    System.out.println(rrDEUMd.getResults().toString());
  }

}
