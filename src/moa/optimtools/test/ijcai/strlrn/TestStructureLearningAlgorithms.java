package moa.optimtools.test.ijcai.strlrn;

/**
 * Testing structure learning algorithms for synthetic data
 */


import java.util.ListIterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import machineLearning.Datasets.FastDataset;
import machineLearning.Datasets.FastInstance;
import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.GraphsSimple.UGraphSimple;
import machineLearning.Utils.Utils;
import machineLearning.Utils.Experiment2;
import machineLearning.Utils.Parameter;
import machineLearning.Utils.ParameterSingleValue;
import machineLearning.Utils.Factors;
import machineLearning.Utils.Graphs;
import machineLearning.Utils.DataSets;
import machineLearning.Graphs.LogLinearModel;
import machineLearning.Graphs.GraphBase;
import machineLearning.Graphs.UGraph;
import machineLearning.Graphs.MarkovNet;
import machineLearning.Graphs.FactorGraph;
import machineLearning.Graphs.Feature;
import machineLearning.Graphs.IndicatorFeature;
import machineLearning.Graphs.DiscreteFactor;
import machineLearning.Graphs.AbstractFactor;
import machineLearning.Graphs.Factor;
import machineLearning.Graphs.VarSet;
import machineLearning.Graphs.Variable;
import machineLearning.Graphs.Assigment;
import machineLearning.optimize.Function;
import machineLearning.optimize.AbstractDifferentiableFunction;
import machineLearning.optimize.AbstractDifferentiableFunctionRegularized;
import machineLearning.optimize.PseudoLogLikelihoodFunction;
import machineLearning.optimize.MAPScore;
import machineLearning.optimize.GreedyStructureSearch;
import machineLearning.independenceBased.BayesianTest;
import moa.optimtools.algorithms.moa.MOAforIBMAP;
import moa.optimtools.eautility.BPopulation;
import moa.optimtools.problems.FitnessFunction;
import moa.optimtools.problems.trap.Trap;

import org.utn.ia.ml.learners.deprec.eda.MutualInformationStructureLearner;
import org.utn.ia.ml.learners.gsmn.GSMN;
import org.utn.ia.ml.learners.ibmaphc.IBMAPHC;


public class TestStructureLearningAlgorithms extends Experiment2 {
	private static TestStructureLearningAlgorithms experiment;
	public BayesianTest IT = null;
	private FastDataset dataset;
	private Random random;
	private UGraphSimple structureFoundBYMI;
	private static int seed = 123456;
	private static int n = 50;
	private static int tau = 4;
	private static int rep = 1;
	private static int E = 100;
	private static int D = 300;

	private static String dir = "";
	private static String datasets[] = { "sampledData_n12_t5_D500K_M500.csv" };
    private static double hiper_ = 0;
    private static double threshold_ = 1e-2;
    private int[] variables;
    private Factor[] initial;
    private Factor[] all;
    private double timeCache_;
    private static Factor real_;
    private static MarkovNet trueNet;
	public static void main(String[] args) {
		try {
			experiment = new TestStructureLearningAlgorithms();
			addControls();
			addOutputs();
			run(args);
		} catch (IllegalAccessException e) {
			e.printStackTrace(System.err);
			System.exit(1);
		}
	}

	/**
	 * @throws IllegalAccessException
	 */
	private static void addOutputs() throws IllegalAccessException {
		experiment.addOutput(new ParameterSingleValue<Double>(
				"GSMNHammingDistance", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"GSMNRuntime", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"GSMN_KL", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"IBMAPHC_HammingDistance", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>("IBMAPHC_runtime",
				-1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"IBMAPHC_KL", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"MOAMutualInformation_HammingDistance", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"MOAMutualInformation_runtime", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"MOAMutualInformation_KL", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"Graf+L1_HammingDistance", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"Graf+L1_runtime", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"Graf+L1_KL", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"Graf+L2_HammingDistance", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"Graf+L2_runtime", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"Graf+L2_KL", -1d));
	}

	private static void addControls() {
		experiment.addControl(new ParameterSingleValue<String>("dir", dir,
				"Folder containing dataset and truenet info."));
		experiment
				.addControl(new Parameter<String>(
						"dataset",
						"Dataset/s separated by spaces. If the value is \"artificial\" it runs the exact case.",
						datasets));
		experiment
				.addControl(new ParameterSingleValue<Integer>("D", 50,
						"Number of rows to read from dataset. If <= 0 then do exact case."));
		experiment
				.addControl(new ParameterSingleValue<Integer>(
						"n",
						10,
						"Number of variables in the domain. Used only by exact case, o.w. it's read from data file."));
		experiment
				.addControl(new ParameterSingleValue<Integer>(
						"tau",
						1,
						"Connectivity parameter. Used only by exact case, o.w. it's read from data file. "));
		experiment.addControl(new ParameterSingleValue<Integer>("repetition",
				1, "Used when the repetitions are done externally."));
		experiment.addControl(new ParameterSingleValue<Boolean>("hasTrueModel",
				true, "Used e.g., for real experiments."));
		experiment.addControl(new ParameterSingleValue<Double>("hiper", hiper_,
								       "Hiperparameter used at regularization."));
		experiment.addControl(new ParameterSingleValue<Double>("threshold", threshold_,
								       "Threshold to remove features."));
	}

	private static void run(String[] args) throws IllegalAccessException {
		String T = "sampledData_n" + n + "_t" + tau + "_E" + E + "_M500_R"
				+ rep + ".csv ";
		if (dir.isEmpty()) {
			dir = "/home/fschluter/experiments/datasets/sampled/undirected/datasets/";
		}
		String argsS = "-verbose 2 -seed 123456 -dir " + dir + " -dataset " + T
				+ "-repetition " + rep + " -n " + n + " -tau " + tau + " -D "
				+ D + " -hasTrueModel true " + "-hiper " + hiper_ + " "
		                + "-threshold " + threshold_;

		if (args.length == 0) {
			args = argsS.split(" ");
		}

		experiment.readValues(args, "dataset");

		experiment.run();
	}

	@Override
	protected void computeValuesForIteration() throws IllegalAccessException {
		n = Utils.getNumVariablesFromFilename(getStringValue("dataset"));
		String fileNameToLoad = getStringValue("dir")
				+ getStringValue("dataset");
		dataset = new FastDataset();
		trueNet = new MarkovNet();
		trueNet.read(dataset, fileNameToLoad, 5, getIntValue("D"));
		buildRealModel();

		/* Just one bayesian test instance */
		IT = new BayesianTest(dataset);

		setOutputs(trueNet, dataset);
	}

	/**
	 * @param trueNet
	 * @throws IllegalAccessException
	 */
    private void setOutputs(MarkovNet trueNet, FastDataset dataset) throws IllegalAccessException {
		System.out.println("");
		random = new Random(seed);

		variables = dataset.scope();
		initial = Factors.generateInitial(variables, r);
		all = Factors.generateFactors(variables, initial);
		hiper_ = getDoubleValue("hiper");
		threshold_ = getDoubleValue("threshold");
		timeCache_ = makeCaches(dataset, initial);
		timeCache_ += makeCaches(dataset, all);

		try {
			gsmn(trueNet);
			MOA_mutualInformationLearner(trueNet);
			ibmapg(trueNet);
			scoreBased(trueNet, dataset, 1);
			scoreBased(trueNet, dataset, 2);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(1);
		}
	}

    private double makeCaches(FastDataset dataset, Factor[] factors) {
	double initTime = new java.util.GregorianCalendar().getTimeInMillis();

	Factors.makeCache(dataset, factors);

	return new java.util.GregorianCalendar().getTimeInMillis() - initTime;
    }

    private void scoreBased(MarkovNet trueNet, FastDataset dataset, int typeReg) throws Exception {
	Function.Regularized reg = (typeReg >= 2) ? Function.Regularized.L2 : Function.Regularized.L1;

	AbstractDifferentiableFunctionRegularized function =
	    new PseudoLogLikelihoodFunction(dataset, initial, reg, hiper_);
	MAPScore score = new MAPScore(function);

	GreedyStructureSearch greedy =
	    new GreedyStructureSearch(score, threshold_, 1);

	FactorGraph fg = null;
	double initTime = new java.util.GregorianCalendar().getTimeInMillis();

	List<Factor> structure = new ArrayList<Factor>(java.util.Arrays.asList(initial));

	double[] params = greedy.search(new double[4], all, structure);

	double endTime = new java.util.GregorianCalendar().getTimeInMillis() - initTime + timeCache_;
	UGraph nt = new UGraph(structure, variables.length);
	int h = (int)new UGraphSimple(trueNet).hamming(new UGraphSimple(nt));

	String type = "Graf+L"+typeReg;

	experiment.setOutput(type+"_runtime", "" + endTime);
	experiment.setOutput(type+"_HammingDistance", "" + h);

	FactorGraph g = new LogLinearModel(structure.toArray(new Factor[0]), params);
	g.nVar = n;
	g.normalize();

	computeKL(g, type);
    }

    private void graphToFeatures(UGraphSimple graph, String type) throws Exception {
	Factor[] fs = Factors.graphToFeatures(graph);
	AbstractDifferentiableFunction function =
	    new PseudoLogLikelihoodFunction(dataset, fs);
	Factors.makeCache(dataset, fs);
	MAPScore score = new MAPScore(function);
	double[] params = score.compute(new double[fs.length]);

	Factor lf = new LogLinearModel(fs, params);
	((FactorGraph)lf).nVar = n;
	lf.normalize();

	computeKL(lf, type);
    }

    private GSMN gsmn(MarkovNet trueNet) throws IllegalAccessException, Exception {
		GSMN gsmn = new GSMN(IT);
		gsmn.run();

		graphToFeatures(gsmn.getOutputNetwork(), "GSMN");

		experiment.setOutput("GSMNHammingDistance", ""
				+ new UGraphSimple(gsmn.getOutputNetwork())
						.hamming(new UGraphSimple(trueNet)));
		experiment.setOutput("GSMNRuntime", ""
				+ gsmn.getRuntime());
		return gsmn;
	}

    private void ibmapg(MarkovNet trueNet) throws IllegalAccessException, Exception {
		UGraphSimple initialStructure = new UGraphSimple(getIntValue("n"));  //empty network
		IBMAPHC ibmapg = new IBMAPHC(Level.OFF, trueNet);
		ibmapg.neighborsStrategy = IBMAPHC.NEIGHBORS_STRATEGY_HEURISTICS; ;
		ibmapg.incrementalScoreComputing=true;
//		ibmapg.MN = getIntValue("tau");
		BayesianTest btDagger3 = new BayesianTest(dataset, true);
		ibmapg.run(getIntValue("n"), btDagger3, initialStructure.adjMatrix);

		graphToFeatures(ibmapg.outputNetwork, "IBMAPHC");

		experiment.setOutput("IBMAPHC_HammingDistance", ""
				+ ibmapg.outputNetwork.hamming(
						new UGraphSimple(trueNet)));
		experiment.setOutput("IBMAPHC_runtime", ""
				+ ibmapg.getRuntime());

	}


	private void MOA_mutualInformationLearner(MarkovNet trueNet) throws Exception {
		MutualInformationStructureLearner learner = new MutualInformationStructureLearner();
		learner.run(dataset, Integer.MAX_VALUE, 1.5);
		int probSize = 45;							// Size of the trap problem
		int orderSize = 5;							// Order of the blocks in trap problem
	    boolean permute = true;
	    FitnessFunction <Boolean> trap5FitFunc = new Trap(probSize, orderSize, permute);
		int popSize = dataset.size();
		int solLength = dataset.numberOfAttributes();
	    int maxGen = 50;
	    int ss = (int)(popSize*0.5);
	    int eliteSol = (int)(popSize*0.5);
	    double cr = 0.5;
	    int it = 4;
	    String structureType = "ToFind";
		String statType = "CrossEntropy";
	    int maxNeighbours = getIntValue("tau");
	    double thresholdCoeff = 1.5;
	    double optFit = probSize;
	    boolean optFitKnown = true;

	    MOAforIBMAP trap5MOA = new MOAforIBMAP(trap5FitFunc, solLength, popSize, ss, cr, it, maxGen, eliteSol, random, optFitKnown, optFit, structureType, statType, maxNeighbours, thresholdCoeff);
		BPopulation spop = getPopulationFromDataset();
		int[][] structure = trap5MOA.getStructure(spop, solLength, ss, statType, maxNeighbours);
		structureFoundBYMI = new UGraphSimple(structure);
		UGraphSimple trueStructure = new UGraphSimple(trueNet);

		graphToFeatures(structureFoundBYMI, "MOAMutualInformation");

		experiment.setOutput("MOAMutualInformation_HammingDistance", ""
				+ structureFoundBYMI.hamming(trueStructure));
		experiment.setOutput("MOAMutualInformation_runtime", ""
				+ trap5MOA.structureLearningRuntime);
	}

	@SuppressWarnings("unchecked")
	private BPopulation getPopulationFromDataset() {
		int popSize = dataset.size();
		int solLength = dataset.numberOfAttributes();
		BPopulation populationFromDataset = new BPopulation(popSize, solLength, random);

        ListIterator li = dataset.instances.listIterator(0);
        FastInstance inst = (FastInstance) li.next();
        int i=0;
        while (true) {
        	Boolean [] instanceBooleanValues =  inst.toBoolean();
			populationFromDataset.solutions[i].setAlleles(instanceBooleanValues) ;

            if (li.hasNext())
                inst = (FastInstance) li.next();
            else
                break;

            i++;
        }
		return populationFromDataset;
	}

    private void buildRealModel() {
	try {
	    trueNet.readParameters(getStringValue("dir") +Utils.datasetName2ParamsName( getStringValue("dataset")));
	} catch (Exception e) { e.printStackTrace(System.err); }

	real_ = new FactorGraph(Factors.factorizeToPairwise(trueNet));
	((FactorGraph)real_).nVar = n;
	real_.normalize();
    }

    private void
	computeKL(Factor q, String type) throws Exception {
	double kl =
	    machineLearning.Utils.Metric.kLDivergence(n, real_, q);

	experiment.setOutput(type + "_KL", ""+kl);
    }
}
