package moa.optimtools.test.ijcai.strlrn;


public class ExperimentStructureLearningAlgorithms {
	private static String dir = "/home/fschluter/experiments/datasets/sampled/undirected/datasets/";
	private static int[] ns = { 50 };
	// private static int[] ns = { 8, 12, 20, 36 };
	private static int[] taus = { 4};
	private static int[] Ds = {50, 100, 200, 300}; // , 800, 1600};
	private static int reps = 10;

	public static void main(String[] args) {

		for (int rep = 1; rep <= reps; rep++) {

			for (int i = 0; i < ns.length; i++) {

				int n = ns[i];

				for (int j = 0; j < taus.length; j++) {

					int tau = taus[j];

					for (int k = 0; k < Ds.length; k++) {
						int D = Ds[k];

						String T = "sampledData_n" + n + "_t" + tau
								+ "_E100_M500_R" + rep + ".csv ";

						String[] argss = { "-verbose", "2", "-seed", "123456",
								"-dir", dir, "-dataset", T, "-repetition",
								"" + rep, " -n ", "" + n, " -tau ", "" + tau,
								" -D ", "" + D, " -hasTrueModel", "true" };

						TestStructureLearningAlgorithms.main(argss);

					}
				}
			}
		}
	}

}
