package moa.optimtools.test.ijcai.moa.onemax;

import moa.TestMOAStructureLearning;

public class ExperimentMOA_OneMax {

	private static String problem = "Trap3";
	private static int maxGen = 1000;
	// static int[] problemSizes = { 16};
	static int[] problemSizes = { 30, 30, 30, 30, 30, 30 };
//	static int[] problemSizes = { 30, 90, 180, 240, 360 };
	// static int[] populationSizes = { 100};
//	static int[] populationSizes = { 600, 1200, 1800, 2000, 2400 };
	static int[] populationSizes = { 60, 70, 150, 200, 250, 300 };
	static int[] MNValues = { 1}; //, 4, 8, Integer.MAX_VALUE};
//	static int[] MNValues = { 1, 4, 8, Integer.MAX_VALUE};
	private static Integer isingInstance = 1;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String repetition = "1";
//		String technique = "ToFind";
		String technique = "ToFindIBMAPHC-EMPTY";
		if (args != null && args.length >= 4 && args[0].equals("-technique")
				&& args[2].equals("-repetition")) {
			technique = args[1];
			repetition = args[3];
		}

		for (int MN : MNValues) {
			int k = 0;
			for (int problemSize : problemSizes) {
//				MNValues[3] = problemSize -1 ;
				int populationSize = populationSizes[k];
				String[] args2 = { "-verbose", "2", "-seed", "123456",
						"-problemSize", "" + problemSize, "-MN", "" + MN,
						"-populationSize", "" + populationSize, "-times",
						"" + 1, "-technique", "" + technique, "-problem",
						"" + problem, "-maxGen", "" + maxGen, "-isingInstance",
						"" + isingInstance, "-repetition", repetition };
				TestMOAStructureLearning.main(args2);
				++k;
			}
		}
	}
}
