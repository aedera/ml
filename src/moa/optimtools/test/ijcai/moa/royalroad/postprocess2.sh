#!/bin/bash
rm temp*
cat resultsRoyalRoadIBMAPHC.dat | sed '/^#/d' | gawk -F, '{print($4,$5,$6,$13,$11,$15)}' | sort -n -k 1,1 -k 2,2 -k 3,3 -k 5,5 >| tempibmaproyalRoad
cat resultsRoyalRoadToFind.dat | sed '/^#/d' | gawk -F, '{print($4,$5,$6,$13,$11,$15)}' | sort -n -k 1,1 -k 2,2 -k 3,3 -k 5,5 >| tempmiroyalRoad
# cat moa_royalRoad_royalRoad.dat | sed '/^#/d' | gawk -F, '{print($4,$5,$13)}' >| temproyalRoadroyalRoad

# paste tempmiroyalRoad tempibmaproyalRoad >| temppastedfiles


for n in 32 92 180 240 360
do
for MN in 4 8 29
do

cat tempmiroyalRoad | gawk -v n=$n -v MN=$MN '($1==n && $3==MN){
			D = $2;
			counts[D]++;
			
			sum2MI[D] += $4*$4;
			sumMI[D] += $4;

			
			sum2RT[D] += $6*$6;
			sumRT[D] += $6;
		}END {
		  
		  for (D in counts) {
		      avgMI[D] = sumMI[D] / counts[D];
		      avg2MI[D] = sum2MI[D] / counts[D];

		      avgRT[D] = sumRT[D] / counts[D];
		      avg2RT[D] = sum2RT[D] / counts[D];
		  }

		  for (D in counts) {
		    cnt = counts[D];
		    if (cnt <= 1)
		      coeff= -1;
		    else if (cnt == 2)
		      coeff = 4.30;
		    else if (cnt > 2 && cnt < 5)
		      coeff = (4.32 + 2.57) / 2.0;
		    else if (cnt == 5)
		      coeff = 2.57;
		    else if (cnt > 5 && cnt < 10)
		      coeff = (2.57 + 2.23) / 2.0;
		    else if (cnt == 10)
		      coeff = 2.23;
		    else if (cnt > 10 && cnt < 20)
		      coeff = (2.23 + 2.09) / 2.0;
		    else if (cnt == 20)
		      coeff = 2.09;
		    else if (cnt > 20 && cnt < 30)
		      coeff = (2.09 + 2.04) / 2.0;
		    else if (cnt == 30)
		      coeff = 2.04;
		    else if (cnt > 32 && cnt < 120)
		      coeff = (2.04 + 1.98) / 2.0;
		    else if (cnt == 120)
		      coeff = 1.98;
		    else
		      coeff = 1.96;
		
		    if (coeff != -1){
 		      stddevMI[D] = coeff * \
 			     sqrt((avg2MI[D] - avgMI[D]*avgMI[D]) / (cnt-1));

 		      stddevRT[D] = coeff * \
 			     sqrt((avg2RT[D] - avgRT[D]*avgRT[D]) / (cnt-1));

		    }

		  }

		  for (D in counts){
		      printf "%s %s %s %s %s %s \n",
			  n, 
			  D,
			  avgMI[D],
			  stddevMI[D],
			  avgRT[D],
			  stddevRT[D];
		  }
	}' | sort -n -k 1,1 -k 2,2 >| tempMI_MN${MN}_n$n



cat tempibmaproyalRoad | gawk -v n=$n -v MN=$MN '($1==n && $3==MN){
			D = $2;
			counts[D]++;
			
			sum2MI[D] += $4*$4;
			sumMI[D] += $4;
			sum2RT[D] += $6*$6;
			sumRT[D] += $6;
		}END {
		  
		  for (D in counts) {
		      avgMI[D] = sumMI[D] / counts[D];
		      avg2MI[D] = sum2MI[D] / counts[D];

		      avgRT[D] = sumRT[D] / counts[D];
		      avg2RT[D] = sum2RT[D] / counts[D];

		  }

		  for (D in counts) {
		    cnt = counts[D];
		    if (cnt <= 1)
		      coeff= -1;
		    else if (cnt == 2)
		      coeff = 4.30;
		    else if (cnt > 2 && cnt < 5)
		      coeff = (4.32 + 2.57) / 2.0;
		    else if (cnt == 5)
		      coeff = 2.57;
		    else if (cnt > 5 && cnt < 10)
		      coeff = (2.57 + 2.23) / 2.0;
		    else if (cnt == 10)
		      coeff = 2.23;
		    else if (cnt > 10 && cnt < 20)
		      coeff = (2.23 + 2.09) / 2.0;
		    else if (cnt == 20)
		      coeff = 2.09;
		    else if (cnt > 20 && cnt < 30)
		      coeff = (2.09 + 2.04) / 2.0;
		    else if (cnt == 30)
		      coeff = 2.04;
		    else if (cnt > 32 && cnt < 120)
		      coeff = (2.04 + 1.98) / 2.0;
		    else if (cnt == 120)
		      coeff = 1.98;
		    else
		      coeff = 1.96;
		
		    if (coeff != -1){
 		      stddevMI[D] = coeff * \
 			     sqrt((avg2MI[D] - avgMI[D]*avgMI[D]) / (cnt-1));
 		      stddevRT[D] = coeff * \
 			     sqrt((avg2RT[D] - avgRT[D]*avgRT[D]) / (cnt-1));

		    }

		  }

		  for (D in counts){
		      printf "%s %s %s %s %s %s \n",
			  n, 
			  D,
			  avgMI[D],
			  stddevMI[D],
			  avgRT[D],
			  stddevRT[D];
		  }
	}' | sort -n -k 1,1 -k 2,2 >| tempIBMAP_MN${MN}_n$n

done

done

for MN in 4 8 29
do
for n in 32 92 180 240 360
do

paste tempMI_MN${MN}_n$n tempIBMAP_MN${MN}_n$n >> tempMN${MN}_n$n
cat tempMN${MN}_n$n | sort -n -k 1,1 -k 2,2 >> temptograpgMN${MN}
done 
done

for MN in 4 8 29
do
gnuplot<<EOF
		  set terminal postscript eps enhanced monochrome font "Helvetica,24"
		  set key box left 
		  set output 'royalRoad_MN$MN.eps'  
		  set datafile missing '-'
		  set title "MOA evals for RoyalRoad, MN=$MN"
# 		  set auto x
		  set xlabel "{/=32 {/Times-Oblique n}} (D)"
		  set ylabel "evals"
		  set grid ytics
		  show grid
		  set xtics   ("32\n(600)" 0.00000, "92\n(1.2k)" 1.00000, "180\n(2.4k)" 2.00000, "240\n(3.6k)" 3.00000 )

		  plot 'temptograpgMN$MN' u 3 t "MI" w linespoints pt 5 , \
		  '' u 9 t " IBMAP" w linespoints pt 8

		  set title "MOA runtime for RoyalRoad, MN=$MN"
		  set ylabel "runtime"
		  set output 'royalRoadRuntime_MN$MN.eps'  
		  plot 'temptograpgMN$MN' u 5 t "MI" w linespoints pt 5 , \
		  '' u 11 t " IBMAP" w linespoints pt 8

EOF

cat temptograpgMN${MN} | gawk '{printf "%s %s | %s(%s) | %s(%s) | %s(%s) | %s(%s) \n",$1,$2,$3,$4,$9,$10,$5,$6,$11,$12; }' >| tableMN${MN}
done

# rm temp*