#!/bin/bash

cat moa_ibmaphc_royalroad.dat | sed '/^#/d' | gawk -F, '{print($4,$5,$13)}' >| tempibmaproyalroad
cat moa_mi_royalroad.dat | sed '/^#/d' | gawk -F, '{print($4,$5,$13)}' >| tempmiroyalroad

paste tempmiroyalroad tempibmaproyalroad >> temppastedfiles
# paste tempmionemax tempibmaponemax >> temppastedfiles

echo "" >| temptograpg
for n in 32 92 180 240 360
do

cat temppastedfiles | gawk -v n=$n '($1==n){
			D = $2;
			counts[D]++;
			
			sum2MI[D] += $3*$3;
			sumMI[D] += $3;

			sum2IBMAP[D] += $6*$6;
			sumIBMAP[D] += $6;

		}END {
		  
		  for (D in counts) {
		      avgMI[D] = sumMI[D] / counts[D];
		      avg2MI[D] = sum2MI[D] / counts[D];

		      avgIBMAP[D] = sumIBMAP[D] / counts[D];
		      avg2IBMAP[D] = sum2IBMAP[D] / counts[D];

		  }

		  for (D in counts) {
		    cnt = counts[D];
		    if (cnt <= 1)
		      coeff= -1;
		    else if (cnt == 2)
		      coeff = 4.30;
		    else if (cnt > 2 && cnt < 5)
		      coeff = (4.30 + 2.57) / 2.0;
		    else if (cnt == 5)
		      coeff = 2.57;
		    else if (cnt > 5 && cnt < 10)
		      coeff = (2.57 + 2.23) / 2.0;
		    else if (cnt == 10)
		      coeff = 2.23;
		    else if (cnt > 10 && cnt < 20)
		      coeff = (2.23 + 2.09) / 2.0;
		    else if (cnt == 20)
		      coeff = 2.09;
		    else if (cnt > 20 && cnt < 30)
		      coeff = (2.09 + 2.04) / 2.0;
		    else if (cnt == 30)
		      coeff = 2.04;
		    else if (cnt > 30 && cnt < 120)
		      coeff = (2.04 + 1.98) / 2.0;
		    else if (cnt == 120)
		      coeff = 1.98;
		    else
		      coeff = 1.96;
		
		    if (coeff != -1){
 		      stddevMI[D] = coeff * \
 			     sqrt((avg2MI[D] - avgMI[D]*avgMI[D]) / (cnt-1));

 		      stddevIBMAP[D] = coeff * \
 			     sqrt((avg2IBMAP[D] - avgIBMAP[D]*avgIBMAP[D]) / (cnt-1));

		    }

		  }

		  for (D in counts){
		      printf "%s %s %s %s %s %s\n",
			  n, 
			  D,
			  avgMI[D],
			  stddevMI[D],
			  avgIBMAP[D],
			  stddevIBMAP[D];
		  }
	}' | sort -n -k 1,1 -k 2,2 >> temptograpg
done

gnuplot<<EOF
		  set terminal postscript eps enhanced monochrome font "Helvetica,24"
		  set key box left 
		  set output 'royalroad.eps'  
		  set datafile missing '-'
# 		  set auto x
		  set xlabel "{/=30 {/Times-Oblique n}}\n D"
		  set grid ytics
		  show grid
		  set xtics   ("32\n(600)" 0.00000, "92\n(1.4k)" 1.00000, "180\n(4.5k)" 2.00000, "240\n(10k)" 3.00000, "360\n(24k)" 4.00000)

		  plot 'temptograpg' u 3 t "MI" w linespoints pt 5 , \
		  '' u 5 t " IBMAP" w linespoints pt 8



EOF

# rm temp*