package moa.optimtools.test.ijcai.moa.royalroad;

public class ExperimentMOA_ToFindIBMAPHC_RoyalRoad {

	private static String problem = "RoyalRoad4"; 
	private static int maxGen = 1000;
//	static int[] problemSizes = { 16};
	static int[] problemSizes = { 32, 92, 180, 240, 360};
//	static int[] populationSizes = { 100};
	static int[] populationSizes = { 500, 750, 1000, 1500, 2000};
	static int[] MNValues = {4};
	static int times = 1;
	private static Integer isingInstance = 1;

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		if (args != null  && args.length >= 4 && args[0].equals("-problem")
				&& args[2].equals("-problemSize")) {
			if (args[0] != null) {
				if (args[0].equals("-problem")) {
					problem = args[1];
				}
			}

			if (args[2].equals("-problemSize")) {
				problemSizes = new int[1];
				problemSizes[0] = new Integer(args[3]);
			}

			if (args.length > 4 && args[4].equals("-isingInstance")) {
				isingInstance = new Integer(args[5]);
			}
		}

		String[] techniques;
		if (problem.equals("OneMax") || problem.equals("Trap5")
				|| problem.equals("Dec3") || problem.equals("RoyalRoad4") 
				|| problem.equals("RoyalRoad5") || problem.equals("Ising")) {
			techniques = new String[2];
			techniques[0] = "ToFind";
			techniques[1] = "ToFindIBMAPHC-EMPTY";
//			techniques[1] = "ToFindGSMN";
//			techniques[1] = "ToFindIBMAPHC-GSMN";
//			techniques[1] = "ToFindIBMAPHC-EMPTY";
//			techniques[1] = "ToFindIBMAPHC-MI";
//			techniques[2] = problem;
		} else {
			techniques = new String[1];
			techniques[0] = "ToFind";
		}

		int k=0;
		for (int problemSize : problemSizes) {
			int populationSize = populationSizes[k];
			for (int MN : MNValues) {
				for (int i=1;i<=times;++i) {
				for (String technique : techniques) {
						String[] args2 = { "-verbose", "2", "-seed", "123456",
								"-problemSize", "" + problemSize, "-MN",
								"" + MN, "-populationSize",
								"" + populationSize, "-times", "" + 1,
								"-technique", "" + technique, "-problem",
								"" + problem, "-maxGen", "" + maxGen, 
								"-isingInstance", ""+isingInstance, 
								"-repetition", ""+i 
								};
	
						TestMOAStructureLearning.main(args2);
					}
				}
			}
			++k;
		}
	}
}
