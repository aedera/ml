package moa.optimtools.test.ijcai.moa.ising2d;

/**
 * 
 */


import java.io.BufferedReader;
import java.io.FileReader;
import java.util.GregorianCalendar;
import java.util.Random;

import machineLearning.Utils.Experiment2;
import machineLearning.Utils.Parameter;
import machineLearning.Utils.ParameterSingleValue;
import moa.optimtools.algorithms.moa.MOAforIBMAP;
import moa.optimtools.problems.FitnessFunction;
import moa.optimtools.problems.desc3.Deceptive3;
import moa.optimtools.problems.ising2d.Ising2d;
import moa.optimtools.problems.onemax.OneMax;
import moa.optimtools.problems.royalroad.RoyalRoad;
import moa.optimtools.problems.trap.Trap;
import moa.utility.Mathutil;

/**
 * @author fschluter
 */
public class TestMOAStructureLearning extends Experiment2 {
	private static int MN = 4;
	private static TestMOAStructureLearning experiment;
//	private static int seed = 123456;
	private static int problemSize = 100;
	private static int populationSize = 2000;
	private static String[] technique = { "ToFind" }; 
//	private static String[] technique = { "ToFindIBMAPHC-EMPTY" }; 
//	private static String[] technique = { "ToFindIBMAPHC-GSMN" }; 
	private static String[] problem = { "Ising" }; // Trap5
	
	private String isingInstancesPath = "/home/fschluter/IsingInstances/";
//	private String isingInstancesPath = "/home/fschluter/software/Mateda2.0/functions/ising-model/IsingInstances/";
	private boolean optFitKnown = true;
	private int optFit;
	private static int isingInstance = 2;
	private static int times = 1;
	private static Integer maxGen = 1000;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			experiment = new TestMOAStructureLearning();
			addControls();
			addOutputs();
			run(args);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @throws IllegalAccessException
	 */
	private static void addOutputs() throws IllegalAccessException {
		experiment.addOutput(new ParameterSingleValue<Double>(
				"evalsAvg", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"evalsStdDev", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"runtime", -1d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"optimumFound", 0d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"outOfMemory", 0d));
		experiment.addOutput(new ParameterSingleValue<Double>(
				"maxFit", -1d));
//		experiment.addOutput(new ParameterSingleValue<String>(
//				"evolution", ""));
	}

	/**
	 * 
	 */
	private static void addControls() {
		experiment.addControl(new ParameterSingleValue<Integer>("problemSize", problemSize,""));
		experiment.addControl(new ParameterSingleValue<Integer>("populationSize", populationSize,""));
		experiment.addControl(new ParameterSingleValue<Integer>("MN", MN,""));
		experiment.addControl(new Parameter<String>("technique","",	technique));
		experiment.addControl(new Parameter<String>("problem","",	problem));
		experiment.addControl(new ParameterSingleValue<Integer>("isingInstance", isingInstance,""));
		experiment.addControl(new ParameterSingleValue<Integer>("times", times,""));
		experiment.addControl(new ParameterSingleValue<Integer>("repetition", 0,""));
		experiment.addControl(new ParameterSingleValue<Integer>("maxGen", maxGen ,""));
	}

	/**
	 * @param args
	 * @throws IllegalAccessException
	 */
	private static void run(String[] args) throws IllegalAccessException {
		String argsS = "-verbose 2 -seed 1 -problemSize " + problemSize + " -populationSize " + populationSize
				+ " -times " + times + " -technique " + technique[0] + " -MN " + MN;
		if (args.length == 0) {
			args = argsS.split(" ");
		}
		experiment.readValues(args);
		experiment.run();
	}

	

	@Override
	protected void computeValuesForIteration() throws IllegalAccessException {
		//initilaise new Trap function
		int probSize = getIntValue("problemSize");							// Size of the trap problem 

		FitnessFunction <Boolean> fitnessFunction = null;
		optFit = probSize;
	    if(getStringValue("problem").equals("OneMax")){
	    	fitnessFunction = new OneMax(probSize);
	    }else if(getStringValue("problem").equals("Trap5")){
	    	int orderSize = 5;							// Order of the blocks in trap problem
	    	boolean permute = false;
	    	fitnessFunction = new Trap(probSize, orderSize, permute);
	    }else if(getStringValue("problem").equals("Trap4")){
	    	int orderSize = 4;							// Order of the blocks in trap problem
	    	boolean permute = false;
	    	fitnessFunction = new Trap(probSize, orderSize, permute);
	    }else if(getStringValue("problem").equals("RoyalRoad")){
	    	fitnessFunction = new RoyalRoad(probSize, 4);
	    }else if(getStringValue("problem").equals("Dec3")){
	    	fitnessFunction = new Deceptive3(probSize);
	    }else if(getStringValue("problem").equals("Ising")){
//	    	fitnessFunction = new IsingModel(probSize, isingInstancesPath);
	    	String ss = createFileContentString(isingInstancesPath, probSize);
	    	fitnessFunction = new Ising2d(ss);
	    	optFit = ((Ising2d)fitnessFunction).optFit;
	    }else{
	    	throw new RuntimeException("bad problem parameter");
	    }
	    
	    //initilaise MOA with Trap function
	    int solLength = probSize;					//length of the binary solution  - equals to the problem size for the 3deceptive function
//	    int maxGen = Integer.MAX_VALUE;				//Maximum Generation - number of generations allowed
	    int maxGen = getIntValue("maxGen");			//Maximum Generation - number of generations allowed
	    int popSize = getIntValue("populationSize");//population size
	    int ss = (int)(popSize*0.5);				//selection size - number of best solution to be selected from the population
	    int eliteSol = (int)(popSize*0.5);			//Elitism - number of best solution to be transfered to next generation
//	    int eliteSol = (int)(popSize*0.5);			//Elitism - number of best solution to be transfered to next generation
	    double cr = 0.5;							//Cooling Rate - used for linear temperature (T=1/cr*g) in Gibbs sampling (g is current generation), if CR is set to 0.0 then simply conditional probabilities are used without estimating boltzman probability.
	    int it = 4;									//Iteration coeficient - coefficint controling number of random walk allowed during sampling a solution which is equal to it*sl
	    Random rand = new Random(getIntValue("seed"));				//random seed - usefull when repeating the same experiment - set to a number for repeating same result 
//	    Random rand = new Random(seed);				//random seed - usefull when repeating the same experiment - set to a number for repeating same result 
	    String structureType = getStringValue("technique");				//Type of the structure - this can also be "trap5" or "3Deceptive", in which case the known structure will be used
		String statType = "CrossEntropy"; 			//Set which statistic test to use to find the structure of the Markov Network, possible values = "CrossEntropy","ChiSquare", "JointEqMargProd", "LinkageDetection" 
	    int maxNeighbours = getIntValue("MN");						//Maximum Neighbour - maximum munber of neighbours allowed during structure learning process, only used when st = "ToFind"
	    double thresholdCoeff = 1.5;				//Threshold Coeffecient - used to weight the mean of MI matrix to be used as the threshold for independency, only usefull when st = "ToFind"			
	    		           		    
	    int times = getIntValue("times");	;								//number times the experiment should be run
	    double bestFitness[] = new double[times];	//space to store the best fitness in each run of the algorithm 
	    double fitEval[] = new double[times];		//space to store the number of fitness evaluation done by each run of the algorithm 
	    
			runMOA(fitnessFunction, solLength, maxGen, popSize, ss, eliteSol, cr, it,
					rand, structureType, statType, maxNeighbours,
					thresholdCoeff, optFit, optFitKnown , times, bestFitness,
					fitEval);
		

	}

	private String createFileContentString(String isingInstancesPath2, int probSize) throws IllegalAccessException {
		String instance = isingInstancesPath2 + "SG_" + probSize + "_" + getIntValue("isingInstance") + ".txt";
		String fileContentString = "";
		try {
			BufferedReader reader = null;
			reader = new BufferedReader(new FileReader(instance));
			String line = reader.readLine();
			while(line!=null){
				fileContentString += line + "\n";
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception ioe) {
			System.err.println(ioe.getMessage());
		}
		
		return fileContentString;
	}

	private void runMOA(FitnessFunction<Boolean> trap5FitFunc,
			int solLength, int maxGen, int popSize, int ss, int eliteSol,
			double cr, int it, Random rand, String structureType,
			String statType, int maxNeighbours, double thresholdCoeff,
			double optFit, boolean optFitKnown, int times,
			double[] bestFitness, double[] fitEval) throws IllegalAccessException  {

		MOAforIBMAP moa = null; 
		long initRuntime = new GregorianCalendar().getTimeInMillis();
		int optimumFound = 0; 
		double maxFit = Integer.MIN_VALUE;
		
		try{
			
			for(int i=0; i<times; i++) {
				moa = new MOAforIBMAP(trap5FitFunc, solLength, popSize, ss, cr, it, maxGen, eliteSol, rand, optFitKnown, optFit, structureType, statType, maxNeighbours, thresholdCoeff);
			    moa.runAlgorithm();		    	
				bestFitness[i] = moa.getResults().getBestSolFitness();			//store best fitness of this run 
				fitEval[i] = moa.getResults().getFitEval();					//store number of fitness evaluation of this run
				optimumFound += moa.getResults().getOptFitness()==moa.getResults().getBestSolFitness()?1:0;
				double bestFit = moa.getResults().getBestSolFitness();
				maxFit = (bestFit>maxFit)? bestFit:maxFit;
			 }
			
			long endRuntime = new GregorianCalendar().getTimeInMillis();
			experiment.setOutput("evalsAvg", ""+Mathutil.findMean(fitEval));
			experiment.setOutput("evalsStdDev", ""+Mathutil.findStDev(fitEval));
			experiment.setOutput("runtime", ""+ (endRuntime - initRuntime) / getIntValue("times"));
			experiment.setOutput("maxFit", ""+ maxFit);
			experiment.setOutput("optimumFound", ""+ optimumFound / times);
		
		} catch (Exception e) {
			experiment.setOutput("outOfMemory", ""+ 1d);
		}
	}
}
