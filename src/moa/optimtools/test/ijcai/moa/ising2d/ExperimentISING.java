package moa.optimtools.test.ijcai.moa.ising2d;

public class ExperimentISING {

	private static String problem = "Ising"; 
	private static int maxGen = 1000;
	static int[] problemSizes = { 36};
//	static int[] problemSizes = { 30, 90, 180, 240, 360};
	static int[] populationSizes = {600};
//	static int[] populationSizes = { 600, 1400, 4500, 10000, 24000};
	static int[] MNValues = {2};
	static int times = 1;
	private static Integer isingInstance = 1;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String repetition="1";
		if (args != null  && args.length == 2 && args[0].equals("-repetition")) {
					repetition = args[1];
		}
		
		String[] techniques;
		if (problem.equals("OneMax") || problem.equals("Trap5")
				|| problem.equals("Dec3")) {
			techniques = new String[1];
//			techniques[0] = "ToFind";
//			techniques[1] = "ToFindGSMN";
//			techniques[1] = "ToFindIBMAPHC-GSMN";
			techniques[0] = "ToFindIBMAPHC-EMPTY";
//			techniques[1] = "ToFindIBMAPHC-MI";
//			techniques[1] = problem;
		} else {
			techniques = new String[2];
			techniques[0] = "ToFind";
			techniques[1] = "ToFindIBMAPHC-EMPTY";
		}

		int k=0;
		for (int problemSize : problemSizes) {
			int populationSize = populationSizes[k];
			for (int MN : MNValues) {
				for (String technique : techniques) {
						String[] args2 = { "-verbose", "2", "-seed", "123456",
								"-problemSize", "" + problemSize, "-MN",
								"" + MN, "-populationSize",
								"" + populationSize, "-times", "" + 1,
								"-technique", "" + technique, "-problem",
								"" + problem, "-maxGen", "" + maxGen, 
								"-isingInstance", ""+isingInstance, 
								"-repetition", repetition 
								};
	
						TestMOAStructureLearning.main(args2);
				}
			}
			++k;
		}
	}
}
