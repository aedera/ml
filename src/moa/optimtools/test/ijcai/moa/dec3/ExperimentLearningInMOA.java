package moa.optimtools.test.ijcai.moa.dec3;

public class ExperimentLearningInMOA {

	private static String problem = "Dec3"; // "RoyalRoad","OneMax","Trap5","Dec3",
	private static int maxGen = 1000;
	static int[] problemSizes = { 30 }; //, 360};
//	static int[] populationSizes = { 10, 50, 250, 1500, 10000 };
	static int[] populationSizes = {1000}; //;, 24000};
	static int[] MNValues = {8};
//	static int[] MNValues = { 2, 4, 8, problemSizes[0] - 1 };
	static int times = 1;
	private static Integer isingInstance = 1;

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		if (args != null  && args.length >= 4 && args[0].equals("-problem")
				&& args[2].equals("-problemSize")) {
			if (args[0] != null) {
				if (args[0].equals("-problem")) {
					problem = args[1];
				}
			}

			if (args[2].equals("-problemSize")) {
				problemSizes = new int[1];
				problemSizes[0] = new Integer(args[3]);
			}

			if (args.length > 4 && args[4].equals("-isingInstance")) {
				isingInstance = new Integer(args[5]);
			}
		}

		String[] techniques;
		if (problem.equals("OneMax") || problem.equals("Trap5")
				|| problem.equals("Dec3")) {
			techniques = new String[3];
			techniques[0] = "ToFind";
//			techniques[1] = "ToFindGSMN";
//			techniques[1] = "ToFindIBMAPHC-GSMN";
			techniques[1] = "ToFindIBMAPHC-EMPTY";
//			techniques[1] = "ToFindIBMAPHC-MI";
			techniques[2] = problem;
		} else {
			techniques = new String[2];
			techniques[0] = "ToFind";
			techniques[1] = "ToFindIBMAPHC-EMPTY";
		}

		int k=0;
		for (int problemSize : problemSizes) {
			int populationSize = populationSizes[k];
			for (int MN : MNValues) {
				for (String technique : techniques) {
					String[] args2 = { "-verbose", "2", "-seed", "123456",
							"-problemSize", "" + problemSize, "-MN",
							"" + MN, "-populationSize",
							"" + populationSize, "-times", "" + times,
							"-technique", "" + technique, "-problem",
							"" + problem, "-maxGen", "" + maxGen, 
							"-isingInstance", ""+isingInstance };

					TestMOAStructureLearning.main(args2);
				}
			}
			++k;
		}
	}
}
