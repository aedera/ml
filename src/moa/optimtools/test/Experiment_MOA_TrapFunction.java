package moa.optimtools.test;

import java.util.Date;
import java.util.Random;

import moa.optimtools.algorithms.moa.MOA;
import moa.optimtools.problems.FitnessFunction;
import moa.optimtools.problems.trap.Trap;
import moa.utility.Mathutil;


/*
 * Run MOA x times and print Average and Stdev for the runs
 * */

public class Experiment_MOA_TrapFunction {
	  public static void main(String args[]) {	
		    
			//initilaise new Trap function
//			int probSize = 360;							// Size of the trap problem 
			int probSize = 30;							// Size of the trap problem 

			int orderSize = 5;							// Order of the blocks in trap problem
		    
		    boolean permute = true;
		    
		    FitnessFunction <Boolean> trap5FitFunc = new Trap(probSize, orderSize, permute);
		    System.out.println("Trap function initilaised.");
		    
		    
		    
		    //initilaise MOA with Trap function
		    
		    int solLength = probSize;					//length of the binary solution  - equals to the problem size for the 3deceptive function
		    int maxGen = 1000;							//Maximum Generation - number of generations allowed
		    int popSize = 600;							//population size
//		    int popSize = 24000;							//population size
		    int ss = (int)(popSize*0.5);				//selection size - number of best solution to be selected from the population
		    int eliteSol = (int)(popSize*0.5);			//Elitism - number of best solution to be transfered to next generation
		    double cr = 0.5;							//Cooling Rate - used for linear temperature (T=1/cr*g) in Gibbs sampling (g is current generation), if CR is set to 0.0 then simply conditional probabilities are used without estimating boltzman probability.
		    int it = 4;									//Iteration coeficient - coefficint controling number of random walk allowed during sampling a solution which is equal to it*sl
		    Random rand = new Random();					//random seed - usefull when repeating the same experiment - set to a number for repeating same result 
		    String structureType = "ToFind";			//Type of the structure - this can also be "trap5" or "3Deceptive", in which case the known structure will be used
			String statType = "CrossEntropy"; 			//Set which statistic test to use to find the structure of the Markov Network, possible values = "CrossEntropy","ChiSquare", "JointEqMargProd", "LinkageDetection" 
		    int maxNeighbours = 4;						//Maximum Neighbour - maximum munber of neighbours allowed during structure learning process, only used when st = "ToFind"
		    //int maxNeighbours = 4;						//Maximum Neighbour - maximum munber of neighbours allowed during structure learning process, only used when st = "ToFind"
		    double thresholdCoeff = 1.5;				//Threshold Coeffecient - used to weight the mean of MI matrix to be used as the threshold for independency, only usefull when st = "ToFind"			
		    double optFit = probSize;					//Optimal fitness - if known, can be used to stop the iteration once optimal solution is found
		    boolean optFitKnown = true;					//Flag to tell whether optimal solution is known and sould the algorithm stop, set to false if you do not provide the optFit or want to run the algorithm till maxGen any way   

		    		           		    
		    int times = 10;								//number times the experiment should be run
		    double bestFitness[] = new double[times];	//space to store the best fitness in each run of the algorithm 
		    double fitEval[] = new double[times];		//space to store the number of fitness evaluation done by each run of the algorithm 
		    
		    for(int i=0; i<times; i++) {
		    	System.out.println("Running experiment no "+(i+1)+" at "+new Date().toString());
			    MOA trap5MOA = new MOA(trap5FitFunc, solLength, popSize, ss, cr, it, maxGen, eliteSol, rand, optFitKnown, optFit, structureType, statType, maxNeighbours, thresholdCoeff);

			    try {
					trap5MOA.runAlgorithm();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		    	
		    	System.out.println("Best solution : " + trap5MOA.getResults().toStringBestSol()+"\n");
		    	bestFitness[i] = trap5MOA.getResults().getBestSolFitness();			//store best fitness of this run 
		    	fitEval[i] = trap5MOA.getResults().getFitEval();					//store number of fitness evaluation of this run
		     }
		    System.out.println("Expetiment statistics over "+times+ " run:");
		    
		    System.out.print("\nmean fitness\t"+ Mathutil.findMean(bestFitness));
		    System.out.print("\nstdev \t"+ Mathutil.findStDev(bestFitness));
		    System.out.print("\nmax fitness\t"+ Mathutil.findMax(bestFitness) + "\n");

		    System.out.print("\nmean evaluation\t"+ Mathutil.findMean(fitEval));
		    System.out.print("\nstdev \t"+ Mathutil.findStDev(fitEval));
		    System.out.print("\nmax evaluation\t"+ Mathutil.findMax(fitEval) + "\n");
		    
	  }
}
