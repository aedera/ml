package moa.optimtools.test;

import java.util.Random;

import moa.optimtools.algorithms.deumd.DEUMd;
import moa.optimtools.problems.FitnessFunction;
import moa.optimtools.problems.trap.Trap;


public class TestDEUMdOnTrap5 {

  public static void main(String args[]) {

	// parameters
    int solLength = 60;
    int popSize = 2000;
    boolean optFitnessKnown = true;
    double optFitness = solLength; 
    int maxGen = 1000;
    int eliteSol = 2;
    int ss = 20;
    double tc = 0.05;

    System.out.println("\nStarting Trap5 with DEUMd");
    
    // Create new instance of trap fitness function with block size of 5 
    // dependency is permuted so that bits in block are all over the 
    // solution and not tightly located
    int problemSize = solLength;
    int blockSize = 5;
    boolean permuted  = true;
    FitnessFunction <Boolean> t5FitFunc = new Trap(problemSize, blockSize, permuted);
    
    //Create new instance of DEUMd algorithm for Trap        
    DEUMd t5DEUMd = new DEUMd(t5FitFunc, solLength, popSize, ss, tc, 
    		maxGen, eliteSol, new Random(), optFitnessKnown, optFitness);
    
    //Run DEUMd    
    try {
		t5DEUMd.runAlgorithm();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    System.out.println(t5DEUMd.getResults().toString());
  }

}
