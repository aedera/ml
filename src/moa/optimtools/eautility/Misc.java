package moa.optimtools.eautility;

public class Misc {

	
	  //Normalaises fitness to all positive usefull when the fitness could be negative
	  //takes a sorted population and normalises the fitness so that all fitness will be positive
	  public static void normalaiseFitnessToAllPositive(Population spop){
		  int popSize = spop.popSize;
	   double diff = 0;
	   if (spop.solutions[popSize-1].getFitness()<=0 && spop.solutions[0].getFitness()<=0)  {
	     diff = spop.solutions[0].getFitness()+spop.solutions[popSize-1].getFitness();
	     for (int i = popSize-1; i>=0; i--) spop.solutions[i].setFitness(spop.solutions[i].getFitness() - diff);
	   }
	   else if (spop.solutions[popSize-1].getFitness()<=0 && spop.solutions[0].getFitness()>0) {
	     diff = spop.solutions[0].getFitness()-spop.solutions[popSize-1].getFitness();
	     for (int i = popSize-1; i>=0; i--) spop.solutions[i].setFitness(spop.solutions[i].getFitness() + diff);
	   }
	   else if (spop.solutions[popSize-1].getFitness()>0 && spop.solutions[0].getFitness()<=0) {
	     diff = spop.solutions[popSize-1].getFitness()-spop.solutions[0].getFitness();
	     for (int i = popSize-1; i>=0; i--) spop.solutions[i].setFitness(spop.solutions[i].getFitness() + diff);
	   }
	  }
	  
	  
	  
}
