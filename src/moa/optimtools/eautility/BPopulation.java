package moa.optimtools.eautility;

import java.util.Random;

public class BPopulation extends Population <Boolean>{

	public BPopulation(int popSize, int solLength, Random rand)
	  {
	    this.popSize = popSize;
	    solutions = new BSolution[popSize];
	    for (int i=0; i<popSize; i++) solutions[i] = new BSolution(solLength, rand);
	  }
	
}
