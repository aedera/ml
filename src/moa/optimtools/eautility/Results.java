package moa.optimtools.eautility;

public class Results {
  
  public boolean optFitnessKnown;
  private double optFitness;			
  private Solution bestSol;
  private Solution finalSol;
  private int fitEval;  
  private int numGen;

  public Results(boolean optFitnessKnown, double optFitness, Solution bestSol, Solution finalSol, int fitEval, int numGen) {
	this.optFitnessKnown = optFitnessKnown;
	this.optFitness = optFitness;
    this.bestSol = bestSol;
    this.finalSol = finalSol;
    this.fitEval = fitEval;
    this.numGen = numGen;
  }

  public void setOptFitness(double optFitness){
	    this.optFitness = optFitness;
	  }
  
  public void setOptFitnessKnown(boolean optFitnessKnown){
	    this.optFitnessKnown = optFitnessKnown;
	  }

  public void setBestSol(Solution bestSol){
    this.bestSol = bestSol;
  }

  public void setFinalSol(Solution finalSol){
   this.finalSol = finalSol;
  }

  public void setFitEval(int fitEval){
	   this.fitEval = fitEval;
  }

  public void setNumGen(int numGen){
	   this.numGen = numGen;
  }

  public double getOptFitness(){
	    return this.optFitness;
	  }

  public boolean getOptFitnessKnown(){
	    return this.optFitnessKnown;
	  }

  public Solution getBestSol(){
    return this.bestSol;
  }

  public Solution getFinalSol(){
   return this.finalSol;
  }
  
  public int getNumGen(){
	    return this.numGen;
  }

  public int getFitEval(){
	    return this.fitEval;
  }

  public double getBestSolFitness(){
	    return this.bestSol.getFitness();
  }

  public double getFinalSolFintess(){
	   return this.finalSol.getFitness();
  }

  public String toStringBestSol(){	  
	  return bestSol.toString();
  }
  
  public String toStringFinalSol(){	  
	  return finalSol.toString();
  }

  public String toString(){	  
	  String ss = "";
	  if (optFitnessKnown){ 
		  ss += "Known Optimum Fitness: "+optFitness+"\n";
		  ss += "Found Best Fitness   : "+bestSol.getFitness()+"\n";
	  }		  
	  ss += "Best  Solution : "+bestSol.toString()+"\n";
	  ss += "Final Solution : "+finalSol.toString()+"\n";	  
	  ss += "Fitness evaluation : "+ fitEval+"\t";	  
	  ss += "Generation : "+ numGen;	  
	  
	  return ss;
  }
  

}
