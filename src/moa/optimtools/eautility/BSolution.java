package moa.optimtools.eautility;

import java.util.Random;

public class BSolution extends Solution <Boolean>{
		
	public BSolution(int length, Random rand) {
		this.length = length;
		this.alleles = new Boolean[length];
		for (int i = 0; i < this.length; i++) alleles[i] = rand.nextBoolean();
	}
	
	public BSolution(Solution <Boolean> sol){
		this.length = sol.length;
	    this.alleles = new Boolean[this.length];
	    for( int i=0; i< this.length; i++ )
	      this.alleles[i] = sol.alleles[i];
	    this.fitness = sol.fitness;
	}
	
	/*Implementation of mutation for boolean solution*/
	public void mutate(double mp, Random rand){
	    for( int i=0; i< this.getLength(); i++)
	      if (rand.nextDouble() < mp) alleles[i] = !alleles[i];
	}

	/*Implementation of mutation of a single allele aspecified by pos */
	public void mutateAllele(int pos, Random rand){
		alleles[pos] = !alleles[pos];
	}

	/*Implementation of random solution for boolean solution*/	
	public void randSolution(int len, Random rand){
		this.length = len;
		this.alleles = new Boolean[this.length];
		for( int i=0; i< this.length; i++ ) alleles[i] = rand.nextBoolean();
	}

	/*Implementation of sampling for boolean solution - should be taken off*/
	public void sample(double pv[], Random rand){
	    for (int i=0; i<this.length; i++)
	      if (rand.nextDouble() < pv[i]) alleles[i]= true;
	        else alleles[i] = false;
	}

	/*Implementation of toString for boolean solution*/
	public String toString(){
	    String retString="";
	    for (int i=0; i<this.length; i++)
	      if (alleles[i]) retString +=  "1";
	      else retString +=  "0";
	    retString += "\t->\t" + fitness ;
	    return (retString);
	}

	/*Implementation of print for boolean solution*/
	public void print(){
	    System.out.println(toString());
	}

}
