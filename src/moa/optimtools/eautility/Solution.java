package moa.optimtools.eautility;

import java.util.Random;

import moa.optimtools.problems.FitnessFunction;


public abstract class Solution <T>{

  protected int length;
  protected T alleles[];
  protected double fitness;

  
  public boolean isSameAs(Solution <T> sol, int from, int to){
		for( int i=from; i<to; i++ )
			if (sol.alleles[i] != alleles[i]) return false;
		return true;
	  }

  public boolean isSameAs(Solution <T> sol){
	  return isSameAs(sol, 0, length);
	}
  
  public void copyTo(Solution <T> sol){
	sol.length = length;
	for( int i=0; i< length; i++)
		sol.setAllele(i, alleles[i]);
	sol.setFitness(fitness);
  }

  public void partCopyTo(Solution <T> sol, int from, int to){
	for( int i=from; i<to; i++ )
		sol.alleles[i] = alleles[i];	  
  }

  public void swapFromTo(Solution <T> sol2, int from, int to){
	T tmpAllele;
	for( int i=from; i<to; i++ ){
		tmpAllele = this.alleles[i];
		this.alleles[i] = sol2.alleles[i];
		sol2.alleles[i] = tmpAllele;
	}
  }

  public void swapAllWithProb(Solution <T> sol2, double prob, Random rand){
		T tmpAllele;
		for( int i=0; i<length; i++ ){
			if (rand.nextDouble()>prob) {
				tmpAllele = this.alleles[i];
				this.alleles[i] = sol2.alleles[i];
				sol2.alleles[i] = tmpAllele;
			}
		}	  
  }

  public int getLength()
  {
    return length;
  }

  public void setLength(int length)
  {
	this.length = length;
  }

  public double getFitness()
	{
    return fitness;
  }

  public void setFitness(double f) {
    fitness = f;
  }

  public T getAllele(int i){
	  return alleles[i];
  }

  public void setAllele(int pos, T val){
		alleles[pos] = val;
  }

  public T[] getAlleles(){
	  return alleles;
  }

  public void setAlleles(T[] alleles){
	  this.alleles =  alleles;
  }

  public void evaluate(FitnessFunction <T> fitFunc, int curIter) {
	    fitness =  fitFunc.returnFit(this,length,curIter);
  }

  public String getDecodedResults(FitnessFunction <T> fitFunc, int curIter) {
	    return(fitFunc.getDecodedResults(this, length, curIter));
  }

  public abstract void mutate(double mp, Random rand);
  
  public abstract void mutateAllele(int pos, Random rand);

  public abstract void randSolution(int len, Random rand);

  public abstract void sample(double pv[], Random rand); //Here just temporarily, should be taken off since will need table for integer rep and will need something else for float

  public abstract void print();

  public abstract String toString();  

}
