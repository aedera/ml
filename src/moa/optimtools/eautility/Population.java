package moa.optimtools.eautility;
import java.util.Random;

import moa.optimtools.problems.FitnessFunction;

public abstract class Population <T>{
  
  protected int popSize;
  public Solution <T> solutions[];  //may should be private

  
  public void copyTo(Population <T> pop){
    for (int i=0; i<popSize; i++)
      this.solutions[i].copyTo(pop.solutions[i]);
    pop.popSize = this.popSize;
  }

  //can go to general utility class
  public void sort()
  {    
	T ta;				//temporary allele
	double tFitness;	//temporary fitness
		
    for (int i=0; i<popSize-1; i++){      
      //find the position for maximum solution in the population
      int max = i;
      for (int j=i+1; j<popSize; j++) if (solutions[max].getFitness() < solutions[j].getFitness()) max = j;
      
      //swap the alleles 
      for (int k=0; k<solutions[i].length; k++){
    	  ta = solutions[i].getAllele(k);
    	  solutions[i].setAllele(k, solutions[max].getAllele(k));
    	  solutions[max].setAllele(k, ta);    	  
      }
      
      //swap the fitness
      tFitness = solutions[i].fitness;
      solutions[i].fitness = solutions[max].fitness;
      solutions[max].fitness = tFitness;
    }
  }

  public void sample(double pv[], Random rand)
  {
    for (int i=0; i<popSize; i++)
      solutions[i].sample(pv, rand);
  }

  public void evaluate(FitnessFunction <T> fitFunc, int curIter, int from)
  {
    for (int i=from; i<popSize; i++)
      solutions[i].evaluate(fitFunc, curIter);
  }

  public double getAvgFit(){
    double avgFit=0;
    for (int i=0; i<popSize; i++) avgFit += solutions[i].getFitness();
    avgFit =  avgFit/popSize;
    return (avgFit);
  }

  public double getSumFit(){
    double sumFit=0;
    for (int i=0; i<popSize; i++) sumFit += solutions[i].getFitness();
    return (sumFit);
  }

  public void print()
  {
    for (int i=0; i<popSize; i++)
      solutions[i].print();
      System.out.println("");
  }

  public String toString()
  {
    String retString="";
    for (int i=0; i<popSize; i++)
      retString += solutions[i].toString() + "\n";
    return (retString);
  }

}
