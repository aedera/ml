package moa;

public class ExperimentMOA {

	private static String problem = "Trap5";
	private static int maxGen = 100;
	// static int[] problemSizes = { 16};
	static int[] problemSizes = { 30, 90, 180, 240, 360 };
	// static int[] populationSizes = { 100};
	static int[] populationSizes = { 600, 1200, 1800, 2000, 2400 };
	static int[] MNValues = {16};
	private static Integer isingInstance = 1;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String repetition = "1";
		String[] techniques = {"ToFind", "ToFindIBMAPHC-MI", "ToFindIBMAPHC-EMPTY"};

		for (int MN : MNValues) {
			int k = 0;
			for (int problemSize : problemSizes) {
				for (String technique : techniques) {
				if(MN == Integer.MAX_VALUE){
					MN = problemSize -1 ;
				}
				int populationSize = populationSizes[k];
				String[] args2 = { "-verbose", "2", "-seed", repetition,
						"-problemSize", "" + problemSize, "-MN", "" + MN,
						"-populationSize", "" + populationSize, "-times",
						"" + 1, "-technique", "" + technique, "-problem",
						"" + problem, "-maxGen", "" + maxGen, "-isingInstance",
						"" + isingInstance, "-repetition", repetition };

				TestMOAStructureLearning.main(args2);
				}
			}
			++k;
		}
	}
}
