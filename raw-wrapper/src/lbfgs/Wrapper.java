package lbfgs;

public class Wrapper {
    private long LBFGSPtr = 0;
    public native long create();
    public native int optimize(long ptr, int size, double[] x, double f,
			       double[] g, boolean orthant, double C);

    static { System.loadLibrary("lbfgs"); }

    public Wrapper() {
	LBFGSPtr = create();
    }

    public int
	run(int n, double[] x, double f,
	    double[] g, boolean orthant, double c) {
	return optimize(LBFGSPtr, n, x, f, g, orthant, c);
    }
}