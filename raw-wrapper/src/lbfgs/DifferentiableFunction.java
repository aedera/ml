package lbfgs;

public interface DifferentiableFunction {
    /**
     * double[0][0]: function value
     * double[1]: gradient value
     */
    public double[][] getValues(double[] point);
}