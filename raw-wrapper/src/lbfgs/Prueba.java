package lbfgs;

public class Prueba {
    private static Minimizer minimizer;
    private static FunctionTest function;

    public static void main(String[] args) {
	double[] x = { 3 };
	function = new FunctionTest();
	minimizer = new Minimizer();

	try { minimizer.run(function, x); }
	catch (Exception e) { System.exit(1); }

	System.out.println("X: " + java.util.Arrays.toString(x));
    }
}


