package lbfgs;

public class Minimizer {
    public int maxItr = 100000;
    public double threshold = 0.0001;
    public double old = 1e+37;
    public double cConverge = 3; // 3 is ad-hoc
    public enum Regularization { L1, L2 };
    public double constReg = 0.0;
    private Wrapper wrapper_;
    private boolean type_;

    public Minimizer() { this(Regularization.L2); }

    public Minimizer(Regularization reg) {
	wrapper_ = new Wrapper();
	type_ = reg == Regularization.L1 ? true : false;
    }

    public double[][]
	run(DifferentiableFunction f, double[] x) throws Exception {
	int converge = 0;
	double diff = 0;
	double[][] v = null;
	double ret = 0;

	for (int i = 0; i < maxItr; i++) {
	    v = f.getValues(x);

	    diff = (i == 0 ? 1.0 : Math.abs(old - v[0][0]) / old);
	    old = v[0][0];

	    converge = diff < threshold ? converge + 1 : 0;

	    if (i > maxItr || converge == cConverge)  break;

	    ret = wrapper_.run(x.length, x, v[0][0], v[1], type_, constReg);

	    if (ret <= 0) {
		if (ret == 0) return v;

		throw new Exception("Error in line search.");
	    }
	}

	return v;
    }
}