package lbfgs;

public class FunctionTest implements DifferentiableFunction {

    public double[][] getValues(double[] point) {
	return new double[][] {
	    new double[] { computeFunction(point) },
	    computeGradient(point)
	};
    }

    public double computeFunction(final double[] x) {
	return (x[0] * x[0]) + x[0];
    }

    public double[] computeGradient(final double[] point) {
	double[] gradient = new double[point.length];

	for (int i = 0; i < point.length; i++) {
	    gradient[i] = computeGradient(i, point);
	}

	return gradient;
    }

    public double computeGradient(int i, final double[] x) {
	return 2*x[i] +1;
    }
}